/*=========================================================================================
    File Name: commaN.js
    Description: class가 commaN인 td들에 대해, 천 단위 콤마를 찍는 javaScript.
    ----------------------------------------------------------------------------------------
    Version: 1.0
    Author: Jo Yeseul
==========================================================================================*/
function commaNum(num) {
		
	console.log("commaNum함수 실행");
	
	var len, point, str;
	
	num = num + ""; 				//받은 숫자를 String화
	point = num.length % 3; 		//받은 숫자를 3으로 나눴을 때 나머지
	len = num.length;
	console.log(num + ", " + point + ", " + len);
	
	str = num.substring(0, point);	//받은 숫자에서, 살릴 제일 앞 부분
	
	while(point < len) {
		if(str != "") str += ",";
		
		str += num.substring(point, point + 3);
		point += 3;
	}
	return str;
};
	
$(document).ready(function(){
	if($(".commaN")) {
		
		for(let i = 0; i < $(".commaN").length; i++) {
			
			let changeStr = commaNum($(".commaN").eq(i).text());
			
			$(".commaN").eq(i).text(changeStr);
		}
	}
});