/* 제약조건 확인 */
function checkValue() {
	
	/* 영업기회명을 입력안했을 경우 작동 */
	if($("#businessOpportunityName").val().length == 0) {
		alert("영업기회명을 입력해주세요.");
		$("#businessOpportunityName").focus();
		return false;
	}
	
	/* 고객을 선택하지 않았을 경우 작동 */
	if($("#clientName").val().length == 0) {
		alert("고객을 선택해주세요");
		$("#clientName").focus();
		return false;
	}
	
	/* 영업기회제품을 선택하지 않았을 경우 작동 */
	if($("#expectedSales").val().length == 0) {
		alert("영업기회품목을 선택해주세요");
		return false;
	}
	
	/* 매출구분을 선택하지 않았을 경우 작동 */
	if($("#salesDivision").val() == '선택안함') {
		alert("매출구분을 선택해주세요");
		$("#salesDivision").focus();
		return false;
	}
	
	/* 사업유형을 선택하지 않았을 경우 작동 */
	if($("#businessType").val() == '선택안함') {
		alert("사업유형을 선택해주세요");
		$("#businessType").focus();
		return false;
	}
	
	/* 사업유형을 선택하지 않았을 경우 작동 */
	if($("#businessStartDate").length == 0) {
		alert("영업시작일을 선택해주세요");
		$("#businessStartDate").focus();
		return false;
	}
	
	/* 사업유형을 선택하지 않았을 경우 작동 */
	if($("#businessEndDate").length == 0) {
		alert("영업종료일을 선택해주세요");
		$("#businessEndDate").focus();
		return false;
	}
	
	/* 담당자을 선택하지 않았을 경우 작동 */
	if($("#name").val().length == 0) {
		alert("담당자를 선택해주세요");
		$("#name").focus();
		return false;
	}
	
	/* 인지경로를 선택하지 않았을 경우 작동 */
	if($("#cognitivePathway").val() == '선택안함') {
		alert("인지경로를 선택해주세요");
		$("#cognitivePathway").focus();
		return false;
	}
	
	/* 결재권자를 선택하지 않았을 경우 작동 */
	if($("#approverName").val().length == 0) {
		alert("결재권자를 선택해주세요");
		$("#approverName").focus();
		return false;
	}
	
}

/* 고객를 선택하기 위한 모달창 */
function selectClientList() {
		
	/* id가 modalOpen인 a태그를 클릭되어 모달창이 열림 */
	$("a[id=modalOpen]").click();
	
	/* ajax통신을 통해서 고객들의 정보를 조회 */
	$.ajax({
		url: "/businessOpportunity/client",
		type: "get",
		success: function(data) {

			/* id가 clientTbody인 것을 변수로 저장 */
			const $table = $("#clientTbody");
			
			/* $table안에 내용을 비움 */
			$table.html("");
			
			/* data의 길이만큼 반복 */
			for(let index in data) {
				
				/* tr을 만듬 */
				$tr = $("<tr>");
			
				/* hidden타입의 input태그를 만들고 고객번호를 값으로 지정함 */
				$clientNoInput = $("<input>").attr("type", "hidden").val(data[index].clientNo);
				
				/* td를 만들고 text값으로 고객명, 고객사명, 작성일자를 담음 */
				$clientNameTd = $("<td>").text(data[index].clientName);
				$clientCompanyNameTd = $("<td>").text(data[index].clientCompanyName);
				$writeDateTd = $("<td>").text(data[index].writeDate);
				
				/* 위에서 만든 td를 tr에 담음 */
				$tr.append($clientNoInput);
				$tr.append($clientNameTd);
				$tr.append($clientCompanyNameTd);
				$tr.append($writeDateTd);
				
				/* 위에서 만든 tr를 $table에 담음 */
				$table.append($tr);
			}
		},
		error: function(xhr) {
			console.log(xhr);
		}
	});
	
	/* 모달창안에서 id가 clientTbody인 것을 클릭시 작동 */
	$("#clientTbody").click(function() {
		
		/* id가 clientTbody를 인덱스에 따라 저장 */
		var tbody = $(this);
		
		/* tbody의 자식노드인 tr을 저장 */
		var tr = tbody.children();
		
		/* tr 클릭시 작동 */
		tr.click(function() {
			
			/* 인덱스에 따른 tr을 tr2로 저장 */
			var tr2 = $(this);
			
			/* 고객사명, 고객명, 고객번호 저장 */
			var clientNo = tr2.children().eq(0).val();	
			var clientName = tr2.children().eq(1).text();	
			var clientCompanyName = tr2.children().eq(2).text();	

			/* 위에서 저장한 값을 넣어줌 */
			$("input[name=clientNo]").attr("value", clientNo);
			$("input[name=clientName]").attr("value", clientName);			
			$("input[name=clientCompanyName]").attr("value", clientCompanyName);
			
			/* id가 btn-close인 a태그가 클릭되어 모달창을 닫음 */
			$("a[id=btn-close]").click();
		});
	});
				
}

/* 담당자를 선택하기 위한 모달창 */
function selectUserList() {
		
	/* id가 modalOpen2인 a태그가 클릭되어 모달창이 열림 */
	$("a[id=modalOpen2]").click();
	
	/* ajax통신을 통해서 담당자들의 정보를 조회 */
	$.ajax({
		url: "/businessOpportunity/user",
		type: "get",
		success: function(data) {

			/* id가 userTbody인 것을 변수로 저장 */
			const $table = $("#userTbody");
			
			/* $table안에 내용을 비움 */
			$table.html("");
			
			/* data의 길이만큼 반복 */
			for(let index in data) {
				
				/* tr을 만듬 */
				$tr = $("<tr>");
			
				/* hidden타입의 input태그를 만들고 담당자번호를 값으로 지정함 */
				$userNoInput = $("<input>").attr("type", "hidden").val(data[index].userNo);
				
				/* td를 만들고 text값으로 사용자의 이름, 부서, 직책을 담음 */
				$nameTd = $("<td>").text(data[index].name);
				$deptNameTd = $("<td>").text(data[index].deptName);
				$positionTd = $("<td>").text(data[index].position);
			
				/* 위에서 만든 td를 tr에 담음 */
				$tr.append($userNoInput);
				$tr.append($nameTd);
				$tr.append($deptNameTd);
				$tr.append($positionTd);
				
				/* 위에서 만든 tr를 $table에 담음 */
				$table.append($tr);
			}
		},
		error: function(xhr) {
			console.log(xhr);
		}
	});
	
	/* 모달창안에서 id가 userTbody인 것을 클릭시 작동 */
	$("#userTbody").click(function() {
		
		/*  id가 userTbody를 인덱스에 따라 저장 */
		var tbody = $(this);
		
		/* tbody의 자식노드인 tr을 저장 */
		var tr = tbody.children();
		
		/* tr 클릭시 작동 */
		tr.click(function() {
			
			/* 인덱스에 따른 tr을 tr2로 저장 */
			var tr2 = $(this);
			
			/* 담당자번호, 담당자이름 저장 */
			var userNo = tr2.children().eq(0).val();	
			var name = tr2.children().eq(1).text();	

			/* 위에서 저장한 값을 넣어줌 */
			$("input[name=userNo]").attr("value", userNo);
			$("input[name=name]").attr("value", name);
			
			/* id가 btn-close인 a태그가 클릭되어 모달창을 닫음 */
			$("a[id=btn-close]").click();
		});
	});
				
}

$(function() {
	
	/* 해당 아이디를 가진 태그를 클릭시 작동 */
	$(document).on("click", "#addProduct, #quantity, #totalSupplyPrice, #suggestedSupplyPrice, #taxAmount, #sum", function() {
		
		/* id가 modalOpen3인 a태그가 클릭되어 모달창이 열림 */
		$("a[id=modalOpen3]").click();
		
		/* ajax통신을 통해서 상품들의 정보를 조회 */
		$.ajax({
			url: "/businessOpportunity/product",
			type: "get",
			success: function(data) {
				
				/* id가 productTbody인 것을 변수로 저장 */
				const $table = $("#productTbody");
				
				/* $table안에 내용을 비움 */
				$table.html("");
				 
				/* data의 길이만큼 반복 */
				for(let index in data) {
					
					/* tr을 만듬 */
					$tr = $("<tr>");
					
					/* td를 만들고 text값으로 상품번호, 상품명, 가격, 규격, 단위를 담음 */
					$productNoTd = $("<td>").text(data[index].productNo);
					$productNameTd = $("<td>").text(data[index].productName);
					$priceTd = $("<td>").text(data[index].price);
					$unitTd = $("<td>").text(data[index].unit);
					$standardTd = $("<td>").text(data[index].standard);
					
					/* 위에서 만든 td를 tr에 담음 */
					$tr.append($productNoTd);
					$tr.append($productNameTd);
					$tr.append($priceTd);
					$tr.append($unitTd);
					$tr.append($standardTd);
					
					/* 위에서 만든 tr를 $table에 담음 */
					$table.append($tr);
				}
			},
			error: function(xhr) {
				console.log(xhr);
			}
		});
		
		
	});
	
	/* 모달창안에서 id가 productTbody인 것을 클릭시 작동 */
	$("#productTbody").click(function() {
		
		/*  id가 userTbody를 인덱스에 따라 저장 */
		var tbody = $(this);
		
		/* tbody의 자식노드인 tr을 저장 */
		var tr = tbody.children();
		
		/* tr 클릭시 작동 */
		tr.click(function() {
			
			/* ajax로 불러온 값을 변수에 저장 */
			var tr2 = $(this);
			var productNo = tr2.children().eq(0).text();
			var productName = tr2.children().eq(1).text();
			var price = tr2.children().eq(2).text();
			var unit = tr2.children().eq(3).text();
			var standard = tr2.children().eq(4).text();
			
			/* 테이블 생성 구문 */
			var html = "";
			
			html += '<div class="whole">';
			html += '<div class="nameText">';
			html += '<input type="hidden" id="productNo" name="productNo"> <br>';
			html += '<input class="productName" type="text" id="productName" name="productName" readonly> <br>';
			html += '<a id="deleteProduct" class="deleteProduct">삭제</a> <br>';
			html += '</div>';
			html += '<table id="productTable">';
			html += '<thead>';
			html += '<tr>';
			html += '<th> 단가 </th>';
			html += '<td> <input type="text" class="price" id="price" name="price" readonly> </td>';
			html += '<th> 수량 </th>';
			html += '<td> <input type="text" class="quantity" id="quantity2" name="quantity2"> </td>';
			html += '</tr>';
			html += '</thead>';
			html += '<tbody>';
			html += '<tr>';
			html += '<th> 할인 </th>';
			html += '<td> <input type="text" id="discount" name="discount"> </td>';
			html += '<th> 포장수량/규격 </th>';
			html += '<td> <input type="text" id="unit" name="unit" readonly> </td>';
			html += '</tr>';
			html += '<tr>';
			html += '<th> 판매단가 </th>';
			html += '<td> <input type="text" id="salePrice" name="salePrice" readonly> </td>';
			html += '<th> 제안금액 </th>';
			html += '<td> <input type="text" class="totalPrice" id="totalPrice" name="totalPrice" readonly> </td>';
			html += '</tr>';
			html += '</table>';
			html += '<br>';
			html += '</div>';

			/* div에 테이블 생성 */
			$("#tableDiv").append(html);
			
			/* 생성된 테이블에 ajax로 불러온 값을 넣음 */
			/* last()를 안쓰면 생성된 테이블들의 값이 모두 같아짐 */
			$("input[id=productNo]").last().attr("value", productNo);
			$("input[id=productName]").last().attr("value", productName);
			$("input[id=price]").last().attr("value", price);
			$("input[id=quantity2]").last().attr("value", 1);
			$("input[id=discount]").last().attr("value", 0);
			$("input[id=unit]").last().attr("value", unit + " / " + standard);
			$("input[id=salePrice]").last().attr("value", price);
			$("input[id=totalPrice]").last().attr("value", price);
			
			/* 동적으로 생성된 테이블에 입력된 값을 더해서 전체공급가액,제안공급가액, 세액,합계를 구해서 값을 넣음 */
			var totalPrices = document.getElementsByClassName("totalPrice");
			
			var sum = 0;

			for(let i = 0; i < totalPrices.length; i++) {
				sum += totalPrices[i].value * 1;
			}
			
			/* 위해서 구한 값을 input태그에 넣음 */
			$("input[id=expectedSales]").attr("value", sum);
			
			/* 모달창을 닫는다 */
			$("a[id=btn-close]").click();
		});
	});
	
	/* 동적으로 생성된 테이블에서는 일반적인 이벤트가 적용되지 않아 on을 사용 */
	/* 삭제버튼 클릭시 작동하며 예상매출금액 수정 */
	$(document).on("click", "#deleteProduct", function() {
		
		/* 동적으로 만든 div 삭제 */
		$(this).parent().parent().remove();
		
		/* 동적으로 생성된 테이블에 입력된 값을 더해서 예상매출을 구해서 값을 넣음 */
		var totalPrices = document.getElementsByClassName("totalPrice");
	
		var sum = 0;

		for(let i = 0; i < totalPrices.length; i++) {
			sum += totalPrices[i].value * 1;
		}
		
		/* 위해서 구한 값을 input태그에 넣음 */
		$("input[id=expectedSales]").attr("value", sum);
		
		/* 상품들을 모두 삭제하였을 경우 초기화 */
		if(sum == 0) {
			$("input[id=expectedSales]").attr("value", "");
		}
			
	})
	
	/* 동적으로 생성된 테이블에서는 일반적인 이벤트가 적용되지 않아 on을 사용함 */
	/* 수량이 입력되면 제안금액을 수정 */
	$(document).on("keyup paste change", "#quantity2", function() {
		var quantity = $(this).val();
		var salePrice = $(this).parent().parent().parent().parent()
						.children().eq(1)
						.children().eq(1)
						.children().eq(1).children().val();
		var totalPrice = $(this).parent().parent().parent().parent()
						.children().eq(1)
						.children().eq(1)
						.children().eq(3).children().val(quantity * salePrice);
		
		/* 동적으로 생성된 테이블에 입력된 값을 더해서 예상매출을 구해서 값을 넣음 */
		var totalPrices = document.getElementsByClassName("totalPrice");
			
			var sum = 0;

			for(let i = 0; i < totalPrices.length; i++) {
				sum += totalPrices[i].value * 1;
			}
			
			/* 위해서 구한 값을 input태그에 넣는다 */
			$("input[id=expectedSales]").attr("value", sum);
	});
	
	/* 할인이 입력되면 판매단가와 제안금액이 수정된다 */
	$(document).on("keyup paste change", "#discount", function() {
		var discount = $(this).val();
		var price = $(this).parent().parent().parent().parent()
					.children().eq(0)
					.children().children().eq(1).children().val();
		var salePrice1 = (price - (price * (discount / 100)));
		var salePrice = $(this).parent().parent().parent()
						.children().eq(1)
						.children().eq(1).children().val(salePrice1);
		var salePrice = $(this).parent().parent().parent()
						.children().eq(1)
						.children().eq(1).children().val();
		var quantity = $(this).parent().parent().parent().parent()
						.children().eq(0)
						.children().children().eq(3).children().val();
		var totalPrice1 = salePrice * quantity;
		var totalPrice = $(this).parent().parent().parent()
						.children().eq(1)
						.children().eq(3).children().val(totalPrice1);
		
		/* 동적으로 생성된 테이블에 입력된 값을 더해서 예상매출을 구해서 값을 넣음 */
		var totalPrices = document.getElementsByClassName("totalPrice");
			
			var sum = 0;

			for(let i = 0; i < totalPrices.length; i++) {
				sum += totalPrices[i].value * 1;
			}
			
			/* 위해서 구한 값을 input태그에 넣음 */
			$("input[id=expectedSales]").attr("value", sum);
	});
		
	
})	

$(function() {
		
	/* 해당 아이디를 가진 태그를 클릭시 작동 */	
	$(document).on("click", "#addApply", function() {	
		
	/* id가 modalOpen4인 a태그를 클릭이 되어 모달창이 열림 */
	$("a[id=modalOpen4]").click();
	
	/* ajax통신을 통해서 지원인력 대상자들의 정보를 조회 */
	$.ajax({
		url: "/businessOpportunity/user",
		type: "get",
		success: function(data) {

			/* id가 userTbody인 것을 변수로 저장 */
			const $table = $("#userTbody2");
			
			/* $table안에 내용을 비움 */
			$table.html("");
			
			/* data의 길이만큼 반복 */
			for(let index in data) {
				
				/* tr을 만듬 */
				$tr = $("<tr>");
			
				/* hidden타입의 input태그를 만들고 담당자번호를 값으로 지정함 */
				$userNoInput = $("<input>").attr("type", "hidden").val(data[index].userNo);
				
				/* td를 만들고 text값으로 사용자의 이름, 부서, 직책을 담음 */
				$nameTd = $("<td>").text(data[index].name);
				$deptNameTd = $("<td>").text(data[index].deptName);
				$positionTd = $("<td>").text(data[index].position);
			
				/* 위에서 만든 td를 tr에 담음 */
				$tr.append($userNoInput);
				$tr.append($nameTd);
				$tr.append($deptNameTd);
				$tr.append($positionTd);
				
				/* 위에서 만든 tr를 $table에 담음 */
				$table.append($tr);
			}
		},
		error: function(xhr) {
			console.log(xhr);
		}
	});
	
	})
	
	/* 모달창안에서 id가 userTbody2인 것을 클릭시 작동 */
	$("#userTbody2").click(function() {
		
		/*  id가 userTbody2를 인덱스에 따라 저장 */
		var tbody = $(this);
		
		/* tbody의 자식노드인 tr을 저장 */
		var tr = tbody.children();
				
		/* tr 클릭시 작동 */
		tr.click(function() {
			
			/* 인덱스에 따른 tr을 tr2로 저장 */
			var tr2 = $(this);

			/* tr2의 자식의 첫번째 값의 value를 userNo으로 저장 */
			var userNo = tr2.children().eq(0).val();
			
			/* tr2의 자식의 두번째 값의 text를 name으로 저장 */
			var name = tr2.children().eq(1).text();		
			
			/* tr2의 자식의 세번째 값의 text를 dept로 저장 */		
			var dept = tr2.children().eq(2).text();		
			
			/* tr2의 자식의 네번째 값의 text를 position로 저장 */			
			var position = tr2.children().eq(3).text();		
			
			/* 테이블 생성 구문 */
			var html = "";
			
			html += '<div>';
			html += '<div>';
			html += '<div class="apply_title">';
			html += '<div class="apply_title_text1">';
			html += '</div>';
			html += '<div class="apply_title_text2">';
			html += '</div>';
			html += '<div class="apply_title_text3">';
			html += '</div>';
			html += '<a class="apply_delete">삭제';
			html += '</a>';
			html += '</div>';
			html += '<div class="apply_block">';
			html += '<div class="apply_block01">';
			html += '<div class="apply_block01_1">역할구분';
			html += '</div>';
			html += '</div>';
			html += '<div class="apply_block02">';
			html += '<select id="roleDivision" name="roleDivision" class="apply_block02_select">';
			html += '<option value="선택안함">선택하세요</option>';
			html += '<option value="영업">영업</option>';
			html += '<option value="기술지원">기술지원</option>';
			html += '<option value="기타">기타</option>';
			html += '</select>';
			html += '</div>';
			html += '</div>';
			html += '</div>';
			html += '<div class="apply_line">';
			html += '</div>';
			html += '<input type="hidden" name="userNo2">';
			html += '</div>';
			html += '</div>';
	
			/* div에 테이블 생성 */
			$("#apply_wrap").append(html);
			
			/* 생성된 테이블에 ajax로 불러온 값을 넣음 */
			/* last()를 안쓰면 생성된 테이블들의 값이 모두 같아짐 */
			$(".apply_title_text1").last().text(name);
			$(".apply_title_text2").last().text(dept);
			$(".apply_title_text3").last().text(position);
		
			/* name이 userNo2인 input태그에 value값으로 userNo을 저장 */
			$("input[name=userNo2]").last().attr("value", userNo);
			
			/* id가 btn-close인 a태그가 클릭되어 모달창을 닫음 */
			$("a[id=btn-close]").click();
		
	});
	
	});
	
	/* 삭제버튼 클릭시 작동 */
	$(document).on("click", ".apply_delete", function() {
		
		/* 최상단의 div를 삭제 */
		$(this).parent().parent().parent().remove();
				
	})
				


})

/* 결재권자를 선택하기 위한 모달창  */
function selectApproverList() {
	
		/* id가 modalOpen5인 a태그가 클릭되어 모달창이 열림 */
		$("a[id=modalOpen5]").click();
	
		/* ajax통신을 통해서 결재권자들의 정보를 조회 */
		$.ajax({
			url: "/businessOpportunity/approver",
			type: "get",
			success: function(data) {
				
				/* id가 approverTbody인 것을 변수로 저장 */
				const $table = $("#approverTbody");
				
				/* $table안에 내용을 비움 */
				$table.html("");
				
				/* data의 길이만큼 반복 */
				for(let index in data) {
					$tr = $("<tr>");
					
					/* hidden타입의 input태그를 만들고 결재권자 번호를 값으로 지정함 */
					$approverNoInput = $("<input>").attr("type", "hidden").val(data[index].approverNo);
						
					/* td를 만들고 text값으로 결재권자의 이름, 부서, 직책을 담음 */
					$nameTd = $("<td>").text(data[index].approverName);
					$deptNameTd = $("<td>").text(data[index].deptName);
					$positionTd = $("<td>").text(data[index].position);
					
					/* 위에서 만든 td를 tr에 담음 */
					$tr.append($approverNoInput);
					$tr.append($nameTd);
					$tr.append($deptNameTd);
					$tr.append($positionTd);
					
					/* 위에서 만든 tr를 $table에 담음 */
					$table.append($tr);
				}
			},
			error: function(xhr) {
				console.log(xhr);
			}
		});
				
		/* 모달창안에서 id가 approverTbody인 것을 클릭시 작동 */
		$("#approverTbody").click(function() {
			
			/*  id가 approverTbody를 인덱스에 따라 저장 */
			var tbody = $(this);
			
			/* tbody의 자식노드인 tr을 저장 */
			var tr = tbody.children();
			
			/* tr 클릭시 작동 */
			tr.click(function() {
				
				/* 인덱스에 따른 tr을 tr2로 저장 */
				var tr2 = $(this);
								
				/* 결재권자번호, 결재권자이름을 저장 */
				var no = tr2.children().eq(0).val();
				var name = tr2.children().eq(1).text();

				/* 위에서 저장한 값을 넣어줌 */
				$("input[id=approverNo]").attr("value", no);
				$("input[id=approverName]").attr("value", name);
				
				/* id가 btn-close인 a태그가 클릭되어 모달창을 닫음 */
				$("a[id=btn-close]").click();
			});
		});
	
}
