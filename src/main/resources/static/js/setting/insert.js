/* 회사정보 추가 */
function insertCompany() {
	
	var insertCompanyInfo = {
		
		companyName: $("#companyName").val(),
		companyId: $("#companyId").val(),
		corporationNo: $("#corporationNo").val(),
		businessNo: $("#businessNo").val(),
		ceo: $("#ceo").val(),
		tel: $("#tel").val(),
		fax: $("#fax").val(),
		zipCode: $("#zipCode").val(),
		address: $("#address").val(),
		detailedAddress: $("#detailedAddress").val(),
		
	};
	
	$.ajax({
		url: "/setting/insert",
		type: "post",
		data: insertCompanyInfo,
		success: function(data) {
			
			if(data > 0) {
		
			alert("회사 정보가 정상적으로 추가되었습니다.");
			location.href="/setting";
					
			} else {
				alert("회사 정보 추가 실패!");
			}
			
		},
		error: function(xhr) {
			alert("회사 정보 추가 에러 발생!");
			console.log(xhr);
		}
	});
	
}