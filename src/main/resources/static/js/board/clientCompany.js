$(function() {
	
	/* whole 클래스를 가지고있는 모든 태그를 가져옴 */
	var whole = document.getElementsByClassName("whole");
	
	/* board_color 클래스를 가지고 있는 모든 클래스를 가져옴 */    
	var color = document.getElementsByClassName("board_color");
    
    /* 이중 반복문을 통해서 보드의 배경색을 정해줌 */
    for(index in color) {
		for(index in whole){
			if(color[index].value == 'GRAY') {				
				 whole[index].style.backgroundColor = '#E6E8E9';				
			}
			if(color[index].value == 'GREEN') {				
				 whole[index].style.backgroundColor = '#80B335';				
			}
			if(color[index].value == 'YELLOW') {				
				 whole[index].style.backgroundColor = '#EDD747';				
			}
			if(color[index].value == 'ORANGE') {				
				 whole[index].style.backgroundColor = '#F2A340';				
			}
			if(color[index].value == 'RED') {				
				 whole[index].style.backgroundColor = '#DA644F';				
			}
			if(color[index].value == 'MAUVE') {				
				 whole[index].style.backgroundColor = '#B87BDA';				
			}
			
		}
	}


})

/* 고객사명 클릭시 상세보기화면으로 이동 */
function selectClientCompany(div) {
	
	var clientCompanyNo = div.parentNode.children[0].value;	//고객사번호
	
	location.href="/clientCompany/select?clientCompanyNo=" + clientCompanyNo;
}

/* x 버튼 클릭시 */
function xBtn() {
	
	location.reload(true);
	
}

/* 점 3개있는 이미지 클릭시 */
function dotOpen(img) {
	
	/* id가 modalOpen인 모달창이 열림 */
	$("a[id=modalOpen]").click();
	
	var clientCompanyNo = img.parentNode.parentNode.children[0].value;	//고객사번호
	var boardNo = img.parentNode.parentNode.children[1].value;			//보드번호
	
	/* 고객사번호와 보드번호를 넣어줄 공간을 정함 */
	const $btn01 = $(".btn01");
	
	/* input태그를 hidden타입으로 value에 clientCompanyNo, boardNo를 넣어서 만듬 */
	$clientCompanyNoInput = $("<input>").attr("type", "hidden").val(clientCompanyNo);
	$boardNoInput = $("<input>").attr("type", "hidden").val(boardNo);
	
	/* 위에서 만든 태그를 위에서 지정한 공간에 넣어줌 */
	$btn01.append($clientCompanyNoInput);
	$btn01.append($boardNoInput);
	
}

/* 고객사를 다른 보드에 복사시킬 때 selectbox에 나올 값 정하기 위해 실행 */
$(function() {
	
	/* 보드명 조회 */
	$.ajax({
		url: "/board/clientCompany/boardName",
		success: function(data) {
			
			/* id가 boardName인 selectbox를 변수로 저장 */
			const $boardName = $("#boardName");
			
			/* $boardName안에 내용을 비움 */
			$boardName.html("");
			
			/* data의 길이 만큼 반복 */
			for(let index in data) {
				
				/* 고객사인 boardName은 모든 고객사가 속해있기 때문에 의미가 없어서 다른과같이 value와 text를 정함 */
				if(data[index].boardName == '고객사') {
					$boardName.append($("<option>").val("").text("선택하세요"));
				}
				
				/* boardName이 고객사가 아닐경우 selecbox 옵션에 value와 text를 넣어줌 */
				if(data[index].boardName != '고객사') {
					$boardName.append($("<option>").val(data[index].boardNo).text(data[index].boardName));
				}
				
			
			}
		},
		error: function(xhr) {
			console.log(xhr);
		}
	})
			
})

/* 보드 복사 */
function copy(btn) {
	
	/* selectbox에서 정한 값을 가져옴 */
	var boardNo = $("#boardName").val();						//보드번호
	var clientCompanyNo = btn.parentNode.children[2].value;		//고객사 번호

	$.ajax({
		url: "/board/clientCompany/copy",
		type: 'POST',
		data: { boardNo : boardNo,
			   clientCompanyNo : clientCompanyNo },	
		success: function(data) {
			console.log(data);
			location.reload(true);
		},
		error: function(xhr) {
			console.log(xhr);
			alert("선택하지 않으셨거나, 이미 복사가 되어있습니다.");
			location.reload(true);
		}
	})
	
}

/* 보드에서 고객사 삭제 */
function remove(btn) {
	
	var clientCompanyNo = btn.parentNode.children[2].value;	//고객사번호
	var boardNo = btn.parentNode.children[3].value;			//보드번호
	
	/* 보드번호가 BRD1가 아닐경우만 작동  */
	if(boardNo != 'BRD1') {
	
	$.ajax({
		url: "/board/clientCompany/delete",
		type: 'POST',
		data: { boardNo : boardNo,
			   clientCompanyNo : clientCompanyNo },	
		success: function(data) {
			console.log(data);
			location.reload(true);
		},
		error: function(xhr) {
			console.log(xhr);
			location.reload(true);
		}
	})
	
	/* 보드번호가 BRD1인 것은 기본적으로 있어야하는 것이기 때문에 제거할 수 없음  */
	} else {
		alert("메인 보드는 삭제할 수 없습니다.")
		location.reload(true);
	}
	
}

/* 보드명 추가하기 위해 작동 */
function addBoard() {
	
	/* id가 modalOpen2인 모덜창이 열림 */
	$("a[id=modalOpen2]").click();
	
	
}

/* 보드명 추가 */
function insertBoard() {
	
	/* id가 boardText input태그에 작성한 값을 저장 */
	var boardName = $("#boardText").val();	//보드명
	
	$.ajax({
		url: "/board/clientCompany/insert",
		type: 'POST',
		data: { boardName : boardName },	
		success: function(data) {
			console.log(data);
			location.reload(true);
			
		},
		error: function(xhr) {
			console.log(xhr);
			location.reload(true);
			
		}
	})
	
}

/*  */
function updateBoard(img) {
		
	var boardNo = img.parentNode.parentNode.parentNode.parentNode.children[1].value;	//보드번호
	
	/* 보드번호가 BRD1가 아닐경우만 작동  */
	if(boardNo != 'BRD1') {
		
		/* id가 modalOpen3인 모달창이 열림 */
		$("a[id=modalOpen3]").click();
		
		/* 저장할 공간을 지정 */
		const $updateHeader = $(".updateHeader");
	
		/* input태그를 hidden타입으로 value에 boardNo를 넣어서 만듬 */
		$boardNoInput = $("<input>").attr("type", "hidden").val(boardNo);
		
		/* 저장공간에 넣어줌 */
		$updateHeader.append($boardNoInput);
	
	/* 보드번호가 BRD1인 것은 기본적인 것이기 때문에 수정할 수 없음  */
	} else {		
		alert("메인보드는 수정할 수 없습니다.");	
	}
}

/* 보드명 수정 클릭 시 */
function updateBoardName(div) {
	
	var boardNo = div.parentNode.children[7].value;	//보드번호
	
	/* id가 modalOpen4인 모달창이 열림 */
	$("a[id=modalOpen4]").click();
	
	/* 저장할 공간을 지정 */
	const $btn01 = $(".btn01");
    
    /* input태그를 hidden타입으로 value에 boardNo를 넣어서 만듬 */
	$boardNoInput = $("<input>").attr("type", "hidden").val(boardNo);
	
	/* 저장공간에 넣어줌 */
	$btn01.append($boardNoInput);
	
}

/* 수정할 보드명을 입력하고 수정버튼 클릭 시 */
function updateBoardName2(btn) {
	
	/* 입력한 보드명을 가져옴 */
	var boardName = $("#boardName2").val();			//보드명
	var boardNo = btn.parentNode.children[2].value;	//보드번호
	
	$.ajax({
		url: "/board/updateName",
		type: 'POST',
		data: { boardNo : boardNo,
		        boardName : boardName },	
		success: function(data) {
			console.log(data);
			location.reload(true);
		},
		error: function(xhr) {
			console.log(xhr);
			location.reload(true);
		}
	})
	
}

/* 색상 수정 클릭 시 */
function updateBoardColor(div) {
	
	var boardNo = div.parentNode.children[7].value;	//보드번호
	
	/* id가 modalOpen5인 모달창이 열림 */			
	$("a[id=modalOpen5]").click();
	
	/* 저장할 공간을 지정 */
	const $updateColor = $(".updateColor");

	 /* input태그를 hidden타입으로 value에 boardNo를 넣어서 만듬 */
	$boardNoInput = $("<input>").attr("type", "hidden").val(boardNo);
	
	/* 저장공간에 넣어줌 */
	$updateColor.append($boardNoInput);

}

/* 회색을 선택했을 시 */
function updateGray(img) {
	
	var boardNo = img.parentNode.children[6].value;	//보드번호
	
	/* color명 지정 */
	var color = 'GRAY';
	
	$.ajax({
		url: "/board/updateColor",
		type: 'POST',
		data: { boardNo : boardNo,
		        color : color },	
		success: function(data) {
			console.log(data);
			location.reload(true);
			
		},
		error: function(xhr) {
			console.log(xhr);
			location.reload(true);
		
		}
	})
	
}

/* 초록을 선택했을 시 */
function updateGreen(img) {
	
	/* 보드번호를 가져옴 */
	var boardNo = img.parentNode.children[6].value;
	
	/* color명 지정 */
	var color = 'GREEN';
	
	$.ajax({
		url: "/board/updateColor",
		type: 'POST',
		data: { boardNo : boardNo,
		        color : color },	
		success: function(data) {
			console.log(data);
			location.reload(true);
			
		},
		error: function(xhr) {
			console.log(xhr);
			location.reload(true);
		
		}
	})
	
}

/* 노란색을 선택했을 시 */
function updateYellow(img) {
	
	var boardNo = img.parentNode.children[6].value;	//보드번호
	
	/* color명 지정 */
	var color = 'YELLOW';
	
	$.ajax({
		url: "/board/updateColor",
		type: 'POST',
		data: { boardNo : boardNo,
		        color : color },	
		success: function(data) {
			console.log(data);
			location.reload(true);
			
		},
		error: function(xhr) {
			console.log(xhr);
			location.reload(true);
			
		}
	})
	
}

/* 주황색을 선택했을 시 */
function updateOrange(img) {
	
	var boardNo = img.parentNode.children[6].value;	//보드번호
	
	/* color명 지정 */
	var color = 'ORANGE';
	
	$.ajax({
		url: "/board/updateColor",
		type: 'POST',
		data: { boardNo : boardNo,
		        color : color },	
		success: function(data) {
			console.log(data);
			location.reload(true);
			
		},
		error: function(xhr) {
			console.log(xhr);
			location.reload(true);
			
		}
	})
	
}

/* 빨간색을 선택했을 시 */
function updateRed(img) {
	
	var boardNo = img.parentNode.children[6].value;	//보드번호
	
	/* color명 지정 */
	var color = 'RED';
	
	$.ajax({
		url: "/board/updateColor",
		type: 'POST',
		data: { boardNo : boardNo,
		        color : color },	
		success: function(data) {
			console.log(data);
			location.reload(true);
			
		},
		error: function(xhr) {
			console.log(xhr);
			location.reload(true);
			
		}
	})
	
}

/* 연보라색을 선택했을 시 */
function updateMauve(img) {
	
	var boardNo = img.parentNode.children[6].value;	//보드번호
	
	/* color명 지정 */
	var color = 'MAUVE';
	
	$.ajax({
		url: "/board/updateColor",
		type: 'POST',
		data: { boardNo : boardNo,
		        color : color },	
		success: function(data) {
			console.log(data);
			location.reload(true);
			
		},
		error: function(xhr) {
			console.log(xhr);
			location.reload(true);
			
		}
	})
	
}

/* 보드 수정에서 삭제 클릭 시 */
function deleteBoard(div) {
		
	var boardNo = div.parentNode.children[7].value;	//보드번호
	
	$.ajax({
		url: "/board/deleteBoard",
		type: 'POST',
		data: { boardNo : boardNo },	
		success: function(data) {
			console.log(data);
			location.reload(true);
		},
		error: function(xhr) {
			console.log(xhr);
			location.reload(true);
		}
	})
	
}