/* 공통으로 사용할 link */
const link = "/proceedings/list";

/* 검색조건이 없을 때 이전버튼 클릭 시 작동 */
function preButtonAction() {
	
	/* html에서 input태그에 hidden타입으로 controller로부터 받아온 값을 value를 저장해놓았음. 그 값을 script에서 저장 */
	var currentPage = $("#currentPageByPaging").val();
	
	location.href = link + "?currentPage=" + (currentPage - 1);	
}

/* 검색조건이 없을 때 다음버튼 클릭 시 작동 */
function nextButtonAction() {
	
	/* html에서 input태그에 hidden타입으로 controller로부터 받아온 값을 value를 저장해놓았음. 그 값을 script에서 저장 */
	var currentPage = $("#currentPageByPaging").val();
	
	/* currentPage변수는 현재 문자열이기 때문에 숫자로 변환함 */
	currentPage *= 1;

	location.href = link + "?currentPage=" + (currentPage + 1);	
}

/* 검색조건이 없을 때 숫자 클릭 시 작동 */
function pageButtonAction(text) {
		
    location.href = link + "?currentPage=" + text;     
}

/* 검색조건이 있을 때 이전버튼 클릭 시 작동 */
function searchPreButtonAction() {
	
	/* html에서 input태그에 hidden타입으로 controller로부터 받아온 값을 value를 저장해놓았음. 그 값을 script에서 저장 */
	var currentPage = $("#currentPageByPaging").val();				//현재페이지
	var clientCompanyName = $("#clientCompanyNameByPaging").val();	//고객사명
	var proceedingsName = $("#proceedingsNameByPaging").val();		//회의록명
	var clientName = $("#clientNameByPaging").val();				//고객명

	var selectSearch = null;	//검색조건
	var searchValue = null;		//검색텍스트
	
	if(clientName != "") {
		selectSearch = "고객";
		searchValue = clientName;
	} else if(clientCompanyName != "") {
		selectSearch = "고객사";
		searchValue = clientCompanyName;
	} else if(proceedingsName != "") {
		selectSearch = "회의록";
		searchValue = proceedingsName;
	}
	
	location.href = link + "?currentPage=" +  (currentPage - 1) + "&selectSearch=" + selectSearch + "&searchValue=" + searchValue;
}

/* 검색조건이 있을 때 다음버튼 클릭 시 작동 */
function searchNextButtonAction() {
	
	/* html에서 input태그에 hidden타입으로 controller로부터 받아온 값을 value를 저장해놓았음. 그 값을 script에서 저장 */
	var currentPage = $("#currentPageByPaging").val();
	
	/* currentPage변수는 현재 문자열이기 때문에 숫자로 변환함 */
	currentPage *= 1;
	var clientCompanyName = $("#clientCompanyNameByPaging").val();	//고객사명
	var proceedingsName = $("#proceedingsNameByPaging").val();		//회의록명
	var clientName = $("#clientNameByPaging").val();				//고객명

	var selectSearch = null;	//검색조건
	var searchValue = null;		//검색텍스트
	
	if(clientName != "") {
		selectSearch = "고객";
		searchValue = clientName;
	} else if(clientCompanyName != "") {
		selectSearch = "고객사";
		searchValue = clientCompanyName;
	} else if(proceedingsName != "") {
		selectSearch = "회의록";
		searchValue = proceedingsName;
	}

	location.href = link + "?currentPage=" +  (currentPage + 1) + "&selectSearch=" + selectSearch + "&searchValue=" + searchValue;
}

/* 검색조건이 있을 때 숫자 클릭 시 작동 */
function searchPageButtonAction(text) {
	  
	/* html에서 input태그에 hidden타입으로 controller로부터 받아온 값을 value를 저장해놓았음. 그 값을 script에서 저장 */  
	var clientCompanyName = $("#clientCompanyNameByPaging").val();	//고객사명
	var proceedingsName = $("#proceedingsNameByPaging").val();		//회의록명
	var clientName = $("#clientNameByPaging").val();				//고객명

	var selectSearch = null;	//검색조건
	var searchValue = null;		//검색텍스트
	
	if(clientName != "") {
		selectSearch = "고객";
		searchValue = clientName;
	} else if(clientCompanyName != "") {
		selectSearch = "고객사";
		searchValue = clientCompanyName;
	} else if(proceedingsName != "") {
		selectSearch = "회의록";
		searchValue = proceedingsName;
	}
	 
   location.href = link + "?currentPage=" +  text + "&selectSearch=" + selectSearch + "&searchValue=" + searchValue; 		
}

/* 상세보기 이동 */
function selectProceedings(div) {
					
	const proceedingsNo = div.children[0].value;	//회의록 번호
	
	location.href = "/proceedings/select?proceedingsNo=" + proceedingsNo;
	
} 