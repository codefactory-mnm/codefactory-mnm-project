$(document).ready(function(){
	
	/* attachments인 클래스를 클릭하면 작동  */
	$(".attachments").on("click", function(){
				
		var fileNo = $(this).parent().children().last().val();	//파일번호
	
		location.href="/proceedings/fileDownload?fileNo=" + fileNo;
		
	});


});

/* 회의록 수정페이지로 이동 */
function update() {
	
	var proceedingsNo = $("#proceedingsNo").val();	//회의록 번호
	
	location.href = "/proceedings/update?proceedingsNo=" + proceedingsNo;
}

/* 회의록 삭제 */
function remove() {
		
	var proceedingsNo = $("#proceedingsNo").val();	//회의록 번호
	
	$.ajax({
			url: "/proceedings/delete",
			data: {proceedingsNo : proceedingsNo},		          
			type: 'POST',
			success: function(data) {
				if(data > 0) {				
					alert("회의록 삭제를 성공하셨습니다.");
					location.href = "/proceedings/list";
				} else {
					alert("회의록 삭제를 실패하셨습니다.")
				}
			},
			error: function(xhr) {
				console.log(xhr);
				alert("에러발생");
			}
		})
	
}	