$(function () {
	
	var phonePattern = /^01([0|1|6|7|8|9])-?([0-9]{3,4})-?([0-9]{4})$/;	// 휴대폰번호 정규표현식
	
	var userInfo = {
		name: $("#name").val(),
		phone: $("#phone").val(),
		question: $("#question").val(),
		answer: $("#answer").val()
	} 
	
	$("userIdChk").click(function() {
		
		if(phonePattern.test(userInfo.phone) === true) {
			
			$.ajax({
				url: "/recoverIdResult",
				type: "post",
				data: userInfo,
				success: function(data) {
					
				console.log(data);
					
/*				if(data.equals("")) {
					data = 0;
				}*/
			
				if(data > 0) {
			
					/*alert('조회하신 사원님의 아이디는 '+ data +' 입니다.');*/
					location.href="/member/login";
						
				} else {
					alert("아이디 찾기 실패!");
					history.back();
					/*location.href="/member/login";*/
				}
				
				},
				error: function(xhr) {
					alert("아이디 찾기 에러 발생!");
					console.log(xhr);
				}
				
			});
			
		} else {
			return;
		}

	});
	
});