/* 주소 입력 */
function ExecPostCode() {
    new daum.Postcode({
        oncomplete: function(data) {

            /* 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져옴*/
            if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                addr = data.roadAddress;
            } else { // 사용자가 지번 주소를 선택했을 경우(J)
                addr = data.jibunAddress;
            }

            /* 우편번호와 주소 정보를 해당 필드에 넣음 */
            document.getElementById('zipCode').value = data.zonecode;
            document.getElementById("address").value = addr;
            
            /* 커서를 상세주소 필드로 이동 */
            document.getElementById("detailedAddress").focus();
        }
    }).open();
}