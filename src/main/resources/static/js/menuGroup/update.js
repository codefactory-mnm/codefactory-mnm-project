/* 메뉴 그롭 수정 */
function updateDept() {
	
	var updateMenuGroupInfo = {
		
		menuNo: $("#detailMenuNo").val(),
		menuName: $("#detailMenuName").val(),
		menuURL: $("#detailMenuURL").val(),
		outputOrder: $("#detailOutputOrder").val(),
		delYN: $('input:radio[name="detailDelYN"]:checked').val(),
		division: $("#detailDivision").val(),
		refMenuName: $("#detailRefMenuName").val()
	};
	
	$.ajax({
		url: "/menuGroup/update",
		type: "post",
		data: updateMenuGroupInfo,
		success: function(data) {
			
			if(data > 0) {
				alert("메뉴가 정상적으로 수정되었습니다.");
				
					
			} else {
				alert("메뉴 수정 실패!");
			}
			
		},
		error: function(xhr) {
			alert("메뉴 수정 에러 발생!");
			console.log(xhr);
		}
	});
	
	
}