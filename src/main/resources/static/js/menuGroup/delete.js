/* 메뉴그룹 삭제 처리 */
function withdrawMenuGroup() {
	
	if(confirm("정말 메뉴를 삭제 처리하시겠습니까?")) {
		
		if($("input:checkbox[name=menuChk]:checked").length <= 0) {
			alert("선택한 메뉴가 없습니다.");
			
			return;
		}
		
		var list = [];	// menuNo를 넘길 list 생성
		
		// name이 menuChk인 체크박스에서 체크된 요소 선택
		$("input:checkbox[name=menuChk]:checked").each(function() {
			
		    var checkVal = $(this).val(); // 체크된 값의 value값 가져오기
		    
		    list.push(checkVal);
		    
		});
		
		// menuNo(문자열) 체크박스 값들(배열)을 key/value 형태로 담는다.
    	var checkData = { "checkArray": list };
		    	    
		$.ajax({
			
	        url: "/menuGroup/delete",
	        type:"post",
	        data: checkData,
	        success: function(data) {	// data로 1개 응답받음
	        	if(data > 0) {
		
					alert("메뉴가 정상적으로 삭제 처리되었습니다.");
					location.href="/menuGroup";
					
				} else {
					alert("메뉴 삭제 처리 실패!");
				}
			},
			error: function(xhr) {
				alert("메뉴 삭제 에러 발생!");
				console.log(xhr);
				
			}
	        
    	});

     } else {
		alert("메뉴 삭제 처리를 취소하셨습니다.");
		
	}
		
} 