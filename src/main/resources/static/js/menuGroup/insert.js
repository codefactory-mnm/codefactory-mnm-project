/* 메뉴 그룹 추가 */
function insertMenuGroup() {
	
	var insertMenuGroupInfo = {
		
		menuName: $("#menuName").val(),
		outputOrder: $("#outputOrder").val(),
		division: $("#division option:selected").text(),
		refMenuName: $("#refMenuName").val(),
		menuURL: $("#menuURL").val()
		
	};
	
		$.ajax({
		url: "/menuGroup/insertMenuGroup",
		type: "post",
		data: insertMenuGroupInfo,
		success: function(data) {
			
			if(data > 0) {
		
			alert("메뉴가 정상적으로 추가되었습니다.");
			location.href="/menuGroup";
					
			} else {
				alert("메뉴 추가 실패!");
			}
			
		},
		error: function(xhr) {
			alert("메뉴 추가 에러 발생!");
			console.log(xhr);
		}
	});
	
}

/* 권한 그룹 추가 */
function insertAuthorityGroup() {
	
	var insertAuthroityGroupInfo = {
		authorityName: $("#authorityName").val()
	};
	
		$.ajax({
		url: "/menuGroup/insertAuthorityGroup",
		type: "post",
		data: insertAuthroityGroupInfo,
		success: function(data) {
			
			if(data > 0) {
		
			alert("권한 그룹이 정상적으로 추가되었습니다.");
			location.href="/menuGroup";
					
			} else {
				alert("권한 그룹 추가 실패!");
			}
			
		},
		error: function(xhr) {
			alert("권한 그룹 추가 에러 발생!");
			console.log(xhr);
		}
	});
	
}