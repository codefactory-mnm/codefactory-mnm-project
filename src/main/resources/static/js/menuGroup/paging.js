/* 이전버튼 클릭 시 작동 */
function preButtonAction() {
	
	var prePage = $("#currentPageByPaging").val() - 1;
	
	if($("#authorityOption option:selected").val() != "") {
	var value = { authority: $("#authorityOption option:checked").text(),
	              currentPage: prePage
	            }; 
	} else {
		return;
		
	}
	
	$(".menuGroup").html("");		// 요청 시, 메뉴 그룹 리스트 비우기
	$(".menuGroupPaging").html("");	// 요청 시, 페이징 버튼 비우기
		
	$.ajax({
			url: "/menuGroup/authorityAndMenuChk",
			type: "get",
			data: value,
			success: function(data) {
				
				var menuGroupList = data.menuGroupList;
			    
			    var pageNo = data.selectCriteria.pageNo;
			    var startPage = data.selectCriteria.startPage;
			    var endPage = data.selectCriteria.endPage;
			    var maxPage =data.selectCriteria.maxPage;
			    var currentPage = data.menuGroupSearch.currentPage;
				
				// 권한별 메뉴 그룹 리스트 출력
				$.each(menuGroupList, function(i, val) {
					$('.menuGroup')
						.append('<tr><td><input type="checkbox" class="input-chk" id="" name="menuChk" value="'+ val.menuNo +'"></td><td>' 
						+ val.division + '</td><td>' 
						+ val.menuNo + '</td><td>' 
						+ val.menuName + '</td><td>' 
						+ val.menuURL + '</td><td>' 
						+ val.outputOrder + '</td><td>' 
						+ val.delYN + '</td><td>' 
						+ val.refMenuName + '</td><td><button class="btn btn-outline-blue float-md-center" id="menuGroupDetailCheck" value="'
						+ val.menuNo + '" data-toggle="modal" data-target="#menuGroupModal">상세보기</button></td></tr>');
				});
				
				/* 페이징 처리 */
				// prevPage 버튼 처리
				if(pageNo <= 1) {
					$('.menuGroupPaging').append('<button class="page_btn1" type="button" disabled style="margin-right: 5px;"><img src="/images/common/prevPage.png"/></button>');
				} else {
					$('.menuGroupPaging').append('<button class="page_btn1" type="button" id="prevPage" onclick="preButtonAction();" style="margin-right: 5px;"><img src="/images/common/prevPage.png"/></button>');
				}
				
				// 페이지 숫자 버튼 처리
				for(var i = startPage; i <= endPage;  i++) {
					if(i == pageNo) {
						$('.menuGroupPaging').append('<button class="page_btn2" disabled style="margin-right: 5px;">'+ i +'</button>');
					} else {
						$('.menuGroupPaging').append('<button class="page_btn2" onclick="pageButtonAction(this.innerText);" style="margin-right: 5px;">'+ i +'</button>');
					}
				}
				
				// nextPage 버튼 처리
				if(pageNo >= maxPage) {
					$('.menuGroupPaging').append('<button class="page_btn3" type="button" disabled style="margin-right: 5px;"><img src="/images/common/nextPage.png"/></button>');
				} else {
					$('.menuGroupPaging').append('<button class="page_btn3" type="button" id="nextPage" onclick="nextButtonAction();" style="margin-right: 5px;"><img src="/images/common/nextPage.png"/></button>');
				}
				
				$('.menuGroupPaging').append('<input type="hidden" id="currentPageByPaging" name="currentPageByPaging" value="'+ currentPage +'">');
				$('.menuGroupPaging').append('<br><br><br>');
					
			},
			error: function(xhr) {
				alert("메뉴 그룹 변경 에러 발생!");
				console.log(xhr);
			}
			
		});
	
}

/* 다음버튼 클릭 시 작동 */
function nextButtonAction() {
	
	var nextPage = $("#currentPageByPaging").val();
	
	/* nextPage변수는 현재 문자열이기 때문에 숫자로 변환후에 +1 을 해준다. */
	nextPage *= 1;
	nextPage = nextPage + 1;
	
	
	if($("#authorityOption option:selected").val() != "") {
	var value = { authority: $("#authorityOption option:checked").text(),
	              currentPage: nextPage
	            }; 
	} else {
		return;
		
	}
	
	$(".menuGroup").html("");		// 요청 시, 메뉴 그룹 리스트 비우기
	$(".menuGroupPaging").html("");	// 요청 시, 페이징 버튼 비우기
		
	$.ajax({
			url: "/menuGroup/authorityAndMenuChk",
			type: "get",
			data: value,
			success: function(data) {
				
				var menuGroupList = data.menuGroupList;
			    
			    var pageNo = data.selectCriteria.pageNo;
			    var startPage = data.selectCriteria.startPage;
			    var endPage = data.selectCriteria.endPage;
			    var maxPage =data.selectCriteria.maxPage;
			    var currentPage = data.menuGroupSearch.currentPage;
				
				// 권한별 메뉴 그룹 리스트 출력
				$.each(menuGroupList, function(i, val) {
					$('.menuGroup')
						.append('<tr><td><input type="checkbox" class="input-chk" id="" name="menuChk" value="'+ val.menuNo +'"></td><td>' 
						+ val.division + '</td><td>' 
						+ val.menuNo + '</td><td>' 
						+ val.menuName + '</td><td>' 
						+ val.menuURL + '</td><td>' 
						+ val.outputOrder + '</td><td>' 
						+ val.delYN + '</td><td>' 
						+ val.refMenuName + '</td><td><button class="btn btn-outline-blue float-md-center" id="menuGroupDetailCheck" value="'
						+ val.menuNo + '" data-toggle="modal" data-target="#menuGroupModal">상세보기</button></td></tr>');
				});
				
				/* 페이징 처리 */
				// prevPage 버튼 처리
				if(pageNo <= 1) {
					$('.menuGroupPaging').append('<button class="page_btn1" type="button" disabled style="margin-right: 5px;"><img src="/images/common/prevPage.png"/></button>');
				} else {
					$('.menuGroupPaging').append('<button class="page_btn1" type="button" id="prevPage" onclick="preButtonAction();" style="margin-right: 5px;"><img src="/images/common/prevPage.png"/></button>');
				}
				
				// 페이지 숫자 버튼 처리
				for(var i = startPage; i <= endPage;  i++) {
					if(i == pageNo) {
						$('.menuGroupPaging').append('<button class="page_btn2" disabled style="margin-right: 5px;">'+ i +'</button>');
					} else {
						$('.menuGroupPaging').append('<button class="page_btn2" onclick="pageButtonAction(this.innerText);" style="margin-right: 5px;">'+ i +'</button>');
					}
				}
				
				// nextPage 버튼 처리
				if(pageNo >= maxPage) {
					$('.menuGroupPaging').append('<button class="page_btn3" type="button" disabled style="margin-right: 5px;"><img src="/images/common/nextPage.png"/></button>');
				} else {
					$('.menuGroupPaging').append('<button class="page_btn3" type="button" id="nextPage" onclick="nextButtonAction();" style="margin-right: 5px;"><img src="/images/common/nextPage.png"/></button>');
				}
				
				$('.menuGroupPaging').append('<input type="hidden" id="currentPageByPaging" name="currentPageByPaging" value="'+ currentPage +'">');
				$('.menuGroupPaging').append('<br><br><br>');
					
			},
			error: function(xhr) {
				alert("메뉴 그룹 변경 에러 발생!");
				console.log(xhr);
			}
			
		});
	
}

/* 숫자 클릭 시 작동 */
function pageButtonAction(text) {
	
	if($("#authorityOption option:selected").val() != "") {
		var value = { authority: $("#authorityOption option:checked").text(),
		              currentPage: text
		            }; 
	} else {
		return;
		
	}
	
	$(".menuGroup").html("");		// 요청 시, 메뉴 그룹 리스트 비우기
	$(".menuGroupPaging").html("");	// 요청 시, 페이징 버튼 비우기
		
	$.ajax({
			url: "/menuGroup/authorityAndMenuChk",
			type: "get",
			data: value,
			success: function(data) {
				
				var menuGroupList = data.menuGroupList;
			    
			    var pageNo = data.selectCriteria.pageNo;
			    var startPage = data.selectCriteria.startPage;
			    var endPage = data.selectCriteria.endPage;
			    var maxPage =data.selectCriteria.maxPage;
			    var currentPage = data.menuGroupSearch.currentPage;
				
				// 권한별 메뉴 그룹 리스트 출력
				$.each(menuGroupList, function(i, val) {
					$('.menuGroup')
						.append('<tr><td><input type="checkbox" class="input-chk" id="" name="menuChk" value="'+ val.menuNo +'"></td><td>' 
						+ val.division + '</td><td>' 
						+ val.menuNo + '</td><td>' 
						+ val.menuName + '</td><td>' 
						+ val.menuURL + '</td><td>' 
						+ val.outputOrder + '</td><td>' 
						+ val.delYN + '</td><td>' 
						+ val.refMenuName + '</td><td><button class="btn btn-outline-blue float-md-center" id="menuGroupDetailCheck" value="'
						+ val.menuNo + '" data-toggle="modal" data-target="#menuGroupModal">상세보기</button></td></tr>');
				});
				
				/* 페이징 처리 */
				// prevPage 버튼 처리
				if(pageNo <= 1) {
					$('.menuGroupPaging').append('<button class="page_btn1" type="button" disabled style="margin-right: 5px;"><img src="/images/common/prevPage.png"/></button>');
				} else {
					$('.menuGroupPaging').append('<button class="page_btn1" type="button" id="prevPage" onclick="preButtonAction();" style="margin-right: 5px;"><img src="/images/common/prevPage.png"/></button>');
				}
				
				// 페이지 숫자 버튼 처리
				for(var i = startPage; i <= endPage;  i++) {
					if(i == pageNo) {
						$('.menuGroupPaging').append('<button class="page_btn2" disabled style="margin-right: 5px;">'+ i +'</button>');
					} else {
						$('.menuGroupPaging').append('<button class="page_btn2" onclick="pageButtonAction(this.innerText);" style="margin-right: 5px;">'+ i +'</button>');
					}
				}
				
				// nextPage 버튼 처리
				if(pageNo >= maxPage) {
					$('.menuGroupPaging').append('<button class="page_btn3" type="button" disabled style="margin-right: 5px;"><img src="/images/common/nextPage.png"/></button>');
				} else {
					$('.menuGroupPaging').append('<button class="page_btn3" type="button" id="nextPage" onclick="nextButtonAction();" style="margin-right: 5px;"><img src="/images/common/nextPage.png"/></button>');
				}
				
				$('.menuGroupPaging').append('<input type="hidden" id="currentPageByPaging" name="currentPageByPaging" value="'+ currentPage +'">');
				$('.menuGroupPaging').append('<br><br><br>');
					
			},
			error: function(xhr) {
				alert("메뉴 그룹 변경 에러 발생!");
				console.log(xhr);
			}
			
		});
        
}