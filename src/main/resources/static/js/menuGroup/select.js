$(function() {
	//조직 등록 이력 최상단 체크박스 클릭
    $("#menuGroupCheckAll").click(function() {
	
        //클릭되었으면
        if($("#menuGroupCheckAll").prop("checked")) {
	
            //input태그의 name이 userNo인 태그들을 찾아서 checked 옵션을 true로 정의
            $("input[name=menuChk]").prop("checked", true);
            
            //클릭이 안되있으면
        } else {
	
            //input태그의 name이 userNo인 태그들을 찾아서 checked 옵션을 false로 정의
            $("input[name=menuChk]").prop("checked", false);
        }
        
    });
	
});


$(function() {
	        // 메뉴 상세보기 클릭
    //$('#menuGroupDetailCheck').click(function(){
	$(document).on("click", "#menuGroupDetailCheck", function() {
	
		var mgdc = $(this).val();
		var param = {"menuNo": mgdc};
		
		$.ajax({
			url: "/menuGroup/detailCheck",
			type: "get",
			data: param,
			success: function(data) {
				
				document.getElementById("detailMenuNo").value = data.menuNo;
				document.getElementById("detailMenuName").value = data.menuName;
				document.getElementById("detailMenuURL").value = data.menuURL;
				document.getElementById("detailOutputOrder").value = data.outputOrder;
				document.getElementById("detailDivision").value = data.division;
				document.getElementById("detailRefMenuName").value = data.refMenuName;
				
				if(data.delYN == 'Y') {
					$("#detailDelY").prop('checked', true);
					$("#detailDelN").prop('checked', false);
				} else if(data.delYN == 'N') {
					$("#detailDelY").prop('checked', false);
					$("#detailDelN").prop('checked', true);
				}
				
				
			},
			error: function(xhr) {
				alert(" 상세보기 에러 발생!");
				console.log(xhr);
			}
			
		});
 
    });
});

	
	
// 권한 체크 시 메뉴 그룹 리스트 불러오는 JS
$(function() {
	
	
	$("#authorityOption").change(function() {
		
		if($("#authorityOption option:selected").val() != "") {
			var value = { authority: $("#authorityOption option:checked").text(),
			              currentPage: $("#currentPage").val()
			            }; 
		} else {
			return;
			
		}
		
		$(".menuGroup").html("");	// 비우기
		$(".menuGroupPaging").html("");	// 비우기
		
		$.ajax({
			url: "/menuGroup/authorityAndMenuChk",
			type: "get",
			data: value,
			success: function(data) {
				
				var menuGroupList = data.menuGroupList;
			    
			    var pageNo = data.selectCriteria.pageNo;
			    var startPage = data.selectCriteria.startPage;
			    var endPage = data.selectCriteria.endPage;
			    var maxPage =data.selectCriteria.maxPage;
			    var currentPage = data.menuGroupSearch.currentPage;
				
				// 권한별 메뉴 그룹 리스트 출력
				$.each(menuGroupList, function(i, val) {
					$('.menuGroup')
						.append('<tr><td><input type="checkbox" class="input-chk" id="" name="menuChk" value="'+ val.menuNo +'"></td><td>' 
						+ val.division + '</td><td>' 
						+ val.menuNo + '</td><td>' 
						+ val.menuName + '</td><td>' 
						+ val.menuURL + '</td><td>' 
						+ val.outputOrder + '</td><td>' 
						+ val.delYN + '</td><td>' 
						+ val.refMenuName + '</td><td><button class="btn btn-outline-blue float-md-center" id="menuGroupDetailCheck" value="'
						+ val.menuNo + '" data-toggle="modal" data-target="#menuGroupModal">상세보기</button></td></tr>');
				});
				
				/* 페이징 처리 */
				// prevPage 버튼 처리
				if(pageNo <= 1) {
					$('.menuGroupPaging').append('<button class="page_btn1" type="button" disabled style="margin-right: 5px;"><img src="/images/common/prevPage.png"/></button>');
				} else {
					$('.menuGroupPaging').append('<button class="page_btn1" type="button" id="prevPage" onclick="preButtonAction();" style="margin-right: 5px;"><img src="/images/common/prevPage.png"/></button>');
				}
				
				// 페이지 숫자 버튼 처리
				for(var i = startPage; i <= endPage;  i++) {
					if(i == pageNo) {
						$('.menuGroupPaging').append('<button class="page_btn2" disabled style="margin-right: 5px;">'+ i +'</button>');
					} else {
						$('.menuGroupPaging').append('<button class="page_btn2" onclick="pageButtonAction(this.innerText);" style="margin-right: 5px;">'+ i +'</button>');
					}
				}
				
				// nextPage 버튼 처리
				if(pageNo >= maxPage) {
					$('.menuGroupPaging').append('<button class="page_btn3" type="button" disabled style="margin-right: 5px;"><img src="/images/common/nextPage.png"/></button>');
				} else {
					$('.menuGroupPaging').append('<button class="page_btn3" type="button" id="nextPage" onclick="nextButtonAction();" style="margin-right: 5px;"><img src="/images/common/nextPage.png"/></button>');
				}
				
				$('.menuGroupPaging').append('<input type="hidden" id="currentPageByPaging" name="currentPageByPaging" value="'+ currentPage +'">');
				$('.menuGroupPaging').append('<br><br><br>');
					
			},
			error: function(xhr) {
				alert("메뉴 그룹 변경 에러 발생!");
				console.log(xhr);
			}
			
		});
		
	});
	
});

