/* 공통으로 사용할 link */
const link = "/organization";

/* 조직 현황 이전버튼 클릭 시 작동 */
function organizationPreButtonAction() {
	
	/* html에서 input태그에 hidden타입으로 controller로부터 받아온 값을 value를 저장해놓았음. 그 값을 script에서 저장 */
	var currentPage = $("#organizationCurrentPageByPaging").val();
	
	/* 경로 이동 */
	location.href = link + "?organizationCurrentPage=" + (currentPage - 1);
	
}

/* 조직 현황 다음버튼 클릭 시 작동 */
function organizationNextButtonAction() {
	
	/* html에서 input태그에 hidden타입으로 controller로부터 받아온 값을 value를 저장해놓았음. 그 값을 script에서 저장 */
	var currentPage = $("#organizationCurrentPageByPaging").val();
	
	/* currentPage변수는 현재 문자열이기 때문에 숫자로 변환함 */
	currentPage *= 1;

	/* 경로 이동 */
	location.href = link + "?organizationCurrentPage=" + (currentPage + 1);
	
}

/* 조직 현황 숫자 클릭 시 작동 */
function organizationPageButtonAction(text) {
		
	/* 경로 이동 */
    location.href = link + "?organizationCurrentPage=" + text;
        
}



/* 조직 변경 이력 이전버튼 클릭 시 작동 */
function changeHistoryPreButtonAction() {
	
	/* html에서 input태그에 hidden타입으로 controller로부터 받아온 값을 value를 저장해놓았음. 그 값을 script에서 저장 */
	var currentPage = $("#changeHistoryCurrentPageByPaging").val();
	
	/* 경로 이동 */
	location.href = link + "?changeHistoryCurrentPage=" + (currentPage - 1);
	
}

/* 조직 변경 이력 다음버튼 클릭 시 작동 */
function changeHistoryNextButtonAction() {
	
	/* html에서 input태그에 hidden타입으로 controller로부터 받아온 값을 value를 저장해놓았음. 그 값을 script에서 저장 */
	var currentPage = $("#organizationCurrentPageByPaging").val();
	
	/* currentPage변수는 현재 문자열이기 때문에 숫자로 변환함 */
	currentPage *= 1;

	/* 경로 이동 */
	location.href = link + "?changeHistoryCurrentPage=" + (currentPage + 1);
	
}

/* 조직 변경 이력 숫자 클릭 시 작동 */
function changeHistoryPageButtonAction(text) {
		
	/* 경로 이동 */
    location.href = link + "?changeHistoryCurrentPage=" + text;
        
}
