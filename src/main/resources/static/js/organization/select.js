$(function() {
	//조직 등록 이력 최상단 체크박스 클릭
    $("#enrollmentCheckAll").click(function(){
	
        //클릭되었으면
        if($("#enrollmentCheckAll").prop("checked")){
	
            //input태그의 name이 userNo인 태그들을 찾아서 checked 옵션을 true로 정의
            $("input[name=enrollDeptNo]").prop("checked", true);
            
            //클릭이 안되있으면
        }else{
	
            //input태그의 name이 userNo인 태그들을 찾아서 checked 옵션을 false로 정의
            $("input[name=enrollDeptNo]").prop("checked", false);
        }
        
    });
    
    //조직 변경 이력 최상단 체크박스 클릭
    $("#changeCheckAll").click(function(){
	
        //클릭되었으면
        if($("#changeCheckAll").prop("checked")){
	
            //input태그의 name이 userNo인 태그들을 찾아서 checked 옵션을 true로 정의
            $("input[name=changeDeptNo]").prop("checked", true);
            
            //클릭이 안되있으면
        }else{
	
            //input태그의 name이 userNo인 태그들을 찾아서 checked 옵션을 false로 정의
            $("input[name=changeDeptNo]").prop("checked", false);
        }
        
    });
    
    
    // 조직 등록 상세보기 클릭시 작동
    $('.enrollViewDetailCheck').click(function(){
 
		var evdc = $(this).val();
		var param = {"deptNo": evdc};
		
		$.ajax({
			url: "/organization/enrollViewDetail",
			type: "get",
			data: param,
			success: function(data) {
				
				document.getElementById("enrollRefDeptName").value = data.refDeptName;
				document.getElementById("enrollDate").value = data.enrollDate;
				document.getElementById("enrollDeptNo").value = data.deptNo;
				document.getElementById("enrollDeptName").value = data.deptName;
				document.getElementById("enrollDeptHead").value = data.deptHead;
				document.getElementById("enrollSortOrder").value = data.sortOrder;
				document.getElementById("enrollNote").value = data.enrollNote;
				
			},
			error: function(xhr) {
				alert(" 상세보기 에러 발생!");
				console.log(xhr);
			}
			
		});
 
    });
    
    // 조직 변경 상세보기 클릭시 작동
    $('.changeHistoryViewDetailCheck').click(function(){
 
		var evdc = $(this).val();
		var param = {"deptNo": evdc};
		
		$.ajax({
			url: "/organization/changeHistoryViewDetail",
			type: "get",
			data: param,
			success: function(data) {
				
				document.getElementById("changeDate").value = data.changeDate;
				document.getElementById("changeRefDept").value = data.refDeptName;
				document.getElementById("changeDeptName").value = data.deptName;
				document.getElementById("changeDeptNo").value = data.deptNo;
				document.getElementById("changeSortOrder").value = data.sortOrder;
				document.getElementById("changeDeptHead").value = data.deptHead;
				document.getElementById("modifier").value = data.modifier;
				document.getElementById("changeHistoryNote").value = data.changeHistoryNote;
				
			},
			error: function(xhr) {
				alert(" 상세보기 에러 발생!");
				console.log(xhr);
			}
			
		});
 
    });
    
});