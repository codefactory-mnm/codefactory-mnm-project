/* 조직 수정 */
function updateDept() {
	
	var updateDeptInfo = {
		
		deptNo: $("#enrollDeptNo").val(),
		deptName: $("#enrollDeptName").val(),
		deptHead: $("#enrollDeptHead").val(),
		refDeptName: $("#enrollRefDeptName").val(),
		sortOrder: $("#enrollSortOrder").val(),
		enrollNote: $("#enrollNote").val()
	};
	
	$.ajax({
		url: "/organization/update",
		type: "post",
		data: updateDeptInfo,
		success: function(data) {
			
			if(data > 0) {
				alert("부서가 정상적으로 수정되었습니다.");
				location.href="/organization";
					
			} else {
				alert("부서 수정 실패!");
			}
			
		},
		error: function(xhr) {
			alert("부서 수정 에러 발생!");
			console.log(xhr);
		}
	});
	
	
}