/* 조직 추가 */
function insertDept() {
	
	var insertDeptInfo = {
		
		deptName: $("#deptName").val(),
		deptHead: $("#deptHead").val(),
		refDeptName: $("#refDeptName").val(),
		sortOrder: $("#sortOrder").val(),
		enrollNote: $("#insertNote").val()
		
	};
	
		$.ajax({
		url: "/organization/insert",
		type: "post",
		data: insertDeptInfo,
		success: function(data) {
			
			if(data > 0) {
		
			alert("부서가 정상적으로 추가되었습니다.");
			location.href="/organization";
					
			} else {
				alert("부서 추가 실패!");
			}
			
		},
		error: function(xhr) {
			alert("부서 추가 에러 발생!");
			console.log(xhr);
		}
	});
	
}