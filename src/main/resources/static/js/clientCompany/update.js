/* 주소 입력 */
function execPostCode() {
	
    new daum.Postcode({
        oncomplete: function(data) {

            /* 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져옴*/
            if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                addr = data.roadAddress;
            } else { // 사용자가 지번 주소를 선택했을 경우(J)
                addr = data.jibunAddress;
            }

            /* 우편번호와 주소 정보를 해당 필드에 넣음 */
            document.getElementById('zipCode').value = data.zonecode;
            document.getElementById("address").value = addr;
            
            /* 커서를 상세주소 필드로 이동 */
            document.getElementById("detailedAddress").focus();
        }
    }).open();
}

/* 제약조건 확인 */
function checkValue() {
	
	/* 고객사명을 입력안했을 경우 작동 */
	if($("#clientCompanyName").val().length == 0) {
		alert("고객사명을 입력해주세요.");
		$("#clientCompanyName").focus();
		return false;
	}
	
	/* 구분을 선택하지 않았을 경우 작동 */
	if($("#division").val() == '선택안함') {
		alert("구분을 선택해주세요");
		$("#division").focus();
		return false;
	}
	
	/* 등급을 선택하지 않았을 경우 작동 */
	if($("#grade").val() == '선택안함') {
		alert("등급을 선택해주세요");
		$("#grade").focus();
		return false;
	}
	
	/* 진행상태를 선택하지 않았을 경우 작동 */
	if($("#progressStatus").val() == '선택안함') {
		alert("진행상태를 선택해주세요");
		$("#progressStatus").focus();
		return false;
	}

	
}

/* 첨부파일 삭제 */
function deleteFile(button) {
	
	var fileNo = button.parentNode.children[2].value;	//파일번호
	
	$.ajax({
			url: "/clientCompany/file/delete",
			data: {fileNo : fileNo},	          
			type: 'POST',
			success: function(data) {
				if(data > 0) {				
					alert("파일 삭제를 성공하셨습니다.");
					location.reload(true);
				} else {
					alert("파일 삭제를 실패하였습니다.")
				}
			},
			error: function(xhr) {
				console.log(xhr);
				alert("에러발생");
			}
		})
	
}	