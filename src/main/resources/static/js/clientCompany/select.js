$(function() {
		
		
		var clientCompanyNo = "clientCompanyNo=" + $("#clientCompanyNo").val();	//고객사번호
		
		/* ajax통신을 통해서 의견 조회 */
		$.ajax({
			url: "/clientCompany/opinion/select",
			data: clientCompanyNo,
			success: function(data) {
				
				/* id가 opinionList인 div를 저장 */
				const $opinionList= $("#opinionList");
				
				/* data의 길이 만큼 반복 */
				for(let index in data) {
					
					/* 첫번째 큰 div 만듬 */			
					$opinionListFirstDiv = $("<div>");
					
					/* 첫번째 div에 2개의 div를 만들고 작성자 이름과 작성날짜를 담음 */
					$opinionListFirstDivFirstDiv = $("<div>").text(data[index].name);
					$opinionListFirstDivSecondDiv = $("<div>").text(data[index].writeDate);
					
					/* 첫번째 div에 담음 */
					$opinionListFirstDiv.append($opinionListFirstDivFirstDiv);
					$opinionListFirstDiv.append($opinionListFirstDivSecondDiv);
					
					/* 미리만들어 놓은 opinionList 변수에 첫번째 div를 담음*/
					$opinionList.append($opinionListFirstDiv);
					
					/* 디자인적 요소를 추가하기 위해서 클래스 추가 */
					$opinionListFirstDiv.addClass("opinion_stored_text");
					$opinionListFirstDivSecondDiv.addClass("opinion_stored_text4");
					
					/* 두번째 큰 div 만듬 */	
					$opinionListSecondDiv = $("<div>");
					
					/* 두번째 div에 5개의 div를 만들고 내용을 담고 삭제 시 필요한 div를 만듬
					   input태그에 hidden타입으로 작성자번호, 로그인한 사용자의 번호, 의견번호를 담음 */
					$opinionListSecondDivFirstDiv = $("<div>").text(data[index].opinionContent);
					$opinionListSecondDivSecondDiv = $("<div>").text("삭제");
					$opinionListSecondDivThirdDiv = $("<input>").attr("id", "opinionUserNo").attr("type", "hidden").val(data[index].userNo);
					$opinionListSecondDivForthDiv = $("<input>").attr("id", "loginUserNo").attr("type", "hidden").val(data[index].loginUserNo);
					$opinionListSecondDivFifthDiv = $("<input>").attr("id", "opinionNo").attr("type", "hidden").val(data[index].opinionNo);
					
					/* 두번째 div에 담음 */
					$opinionListSecondDiv.append($opinionListSecondDivFirstDiv);
					$opinionListSecondDiv.append($opinionListSecondDivSecondDiv);
					$opinionListSecondDiv.append($opinionListSecondDivThirdDiv);
					$opinionListSecondDiv.append($opinionListSecondDivForthDiv);
					$opinionListSecondDiv.append($opinionListSecondDivFifthDiv);
					
					/* 미리만들어 놓은 opinionList 변수에 두번째 div를 담음*/
					$opinionList.append($opinionListSecondDiv);
					
					/* 디자인적 요소를 추가하기 위해서 클래스 추가 */
					$opinionListSecondDiv.addClass("opinion_stored_text2");
					$opinionListSecondDivFirstDiv.addClass("opinion_stored_text5");
					$opinionListSecondDivSecondDiv.addClass("opinion_stored_text6");
					
					/* 디자인적 요소를 추가하기 위해서 3,4번 째 div를 만들고 클래스를 추가하고 미리만들어 놓은 opinionList 변수에 담음 */
					$opinionListThirdDiv = $("<div>");
					$opinionListForthDiv = $("<div>");
					$opinionList.append($opinionListThirdDiv);
					$opinionList.append($opinionListForthDiv);
					$opinionListThirdDiv.addClass("opinion_line");
					$opinionListForthDiv.addClass("restArea2");
					
				}
				
				/* 클래스가 opinion_stored_text6인 태그를 클릭시 작동 (의견삭제) */
				$(".opinion_stored_text6").on("click", function() {
					
					/* 작성자 번호, 로그인된 사용자 번호, 의견번호 저장 */
					var userNo = $(this).parent().children("#opinionUserNo").val();
					var loginUserNo = $(this).parent().children("#loginUserNo").val();	
					var opinionNo = "opinionNo=" + $(this).parent().children().last().val();					
					
					/* 작성자 번호와 로그인된 사용자 번호가 같을 때 의견삭제 기능 작동 */
					if(userNo == loginUserNo) {
											
					$.ajax({
						url: "/clientCompany/opinion/delete",
						type:"POST",
						data: opinionNo,
						success: function(data) {
							if(data > 0) {
								alert("의견이 정상적으로 삭제되었습니다.");
								location.reload(true);
							} else {
								alert("의견 삭제에 실패하셨습니다.");
							}	
							
						},
						error: function(xhr) {
							console.log(xhr);	``
							alert("에러 발생");
						}
					})
					
					} else {
						alert("본인이 작성한 의견만 삭제할 수 있습니다.");
					}
					
				})
				
			},
			error: function(xhr) {
				console.log(xhr);
			}
		});
			
		
		
	});

/* 의견작성 기능 작동 */	
function insertOpinion() {
		
		var opinionContent = $("#opinion_textarea").val();	//의견 작성내용
		var clientCompanyNo = $("#clientCompanyNo").val();	//영업기회번호
		
		$.ajax({
			url: "/clientCompany/opinion/insert",
			data: {opinionContent : opinionContent,
			       clientCompanyNo : clientCompanyNo },			          
			type: 'POST',
			success: function(data) {
				if(data > 0) {				
					alert("의견작성을 성공하셨습니다.");
					location.reload(true);
				} else {
					alert("의견작성을 실패하였습니다.")
				}
			},
			error: function(xhr) {
				console.log(xhr);
				alert("에러발생");
			}
		})
		
		
	}
	
/* 사용자 이미지를 클릭하면 고객상세보기 페이지로 이동 */	
function selectClient(img) {
	
	var clientNo = img.parentNode.children[1].value;	//고객번호
	
	location.href = "/client/select?clientNo=" + clientNo;

}

/* 고객사 수정페이지로 이동 */
function update() {
	
	var clientCompanyNo = $("#clientCompanyNo").val();	//고객사번호
	
	location.href = "/clientCompany/update?clientCompanyNo=" + clientCompanyNo;
	
}

$(document).ready(function(){
	
	/* attachments인 클래스를 클릭하면 작동  */
	$(".attachments").on("click", function(){
								
		var fileNo = $(this).parent().children().last().val();	//파일번호

		location.href="/clientCompany/fileDownload?fileNo=" + fileNo;
		
	});


});

/* 고객사 삭제 */
function remove() {
	
	var clientCompanyNo = $("#clientCompanyNo").val();	//고객사번호
	
	$.ajax({
			url: "/clientCompany/delete",
			data: {clientCompanyNo : clientCompanyNo},		          
			type: 'POST',
			success: function(data) {
				if(data > 0) {				
					alert("고객사 삭제를 성공하셨습니다.");
					location.href = "/clientCompany/list";
				} else {
					alert("고객사 삭제를 실패하셨습니다.")
				}
			},
			error: function(xhr) {
				console.log(xhr);
				alert("에러발생");
			}
		})
	
}