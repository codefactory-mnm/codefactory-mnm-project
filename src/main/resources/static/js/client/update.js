/* 제약조건 확인 */
function checkValue() {
	
	/* 고객명을 입력안했을 경우 작동 */
	if($("#clientName").val().length == 0) {
		alert("고객명을 입력해주세요.");
		$("#clientName").focus();
		return false;
	}
	
	/* 고객사를 선택하지 않았을 경우 작동 */
	if($("#clientCompanyName").val().length == 0) {
		alert("고객사를 선택해주세요.");
		$("#clientCompanyName").focus();
		return false;
	}
	
	/* 휴대번호를 입력안했을 경우 작동 */
	if($("#phone").val().length == 0) {
		alert("휴대번호를 입력해주세요.");
		$("#phone").focus();
		return false;
	}
	
	/* 이메일을 입력안했을 경우 작동 */
	if($("#email").val().length == 0) {
		alert("이메일을 입력해주세요.");
		$("#email").focus();
		return false;
	}

	/* 등급을 선택하지 않았을 경우 작동 */
	if($("#grade").val() == '선택안함') {
		alert("등급을 선택해주세요");
		$("#grade").focus();
		return false;
	}
	
	/* 담당자를 선택하지 않았을 경우 작동 */
	if($("#name").val().length == 0) {
		alert("담당자를 선택해주세요.");
		$("#name").focus();
		return false;
	}

	
}

/* 고객사를 선택하기 위한 모달창 */
function selectClientCompanyList() {
		
	/* id가 modalOpen인 a태그를 클릭되어 모달창이 열림 */
	$("a[id=modalOpen]").click();
	
	/* ajax통신을 통해서 고객사들의 정보를 조회 */
	$.ajax({
		url: "/client/clientCompany",
		type: "get",
		success: function(data) {

			/* id가 clientCompanyTbody인 것을 변수로 저장 */
			const $table = $("#clientCompanyTbody");
			
			/* $table안에 내용을 비움 */
			$table.html("");
			
			/* data의 길이만큼 반복 */
			for(let index in data) {
				
				/* tr을 만듬 */
				$tr = $("<tr>");
			
				/* hidden타입의 input태그를 만들고 고객사번호를 값으로 지정함 */
				$clientCompanyNoInput = $("<input>").attr("type", "hidden").val(data[index].clientCompanyNo);
				
				/* td를 만들고 text값으로 고객사 이름, 구분, 주소, 상세주소, 작성일자을 담음 */
				$clientCompanyNameTd = $("<td>").text(data[index].clientCompanyName);
				$divisionTd = $("<td>").text(data[index].division);
				$addressTd = $("<td>").text(data[index].address);
				$detailedAddressTd = $("<td>").text(data[index].detailedAddress);
				$writeDateTd = $("<td>").text(data[index].writeDate);
				
				/* 위에서 만든 td를 tr에 담음 */
				$tr.append($clientCompanyNoInput);
				$tr.append($clientCompanyNameTd);
				$tr.append($divisionTd);
				$tr.append($addressTd);
				$tr.append($detailedAddressTd);
				$tr.append($writeDateTd);
				
				/* 위에서 만든 tr를 $table에 담음 */
				$table.append($tr);
			}
		},
		error: function(xhr) {
			console.log(xhr);
		}
	});
	
	/* 모달창안에서 id가 userTbody인 것을 클릭시 작동 */
	$("#clientCompanyTbody").click(function() {
		
		/*  id가 clientCompanyTbody를 인덱스에 따라 저장 */
		var tbody = $(this);
		
		/* tbody의 자식노드인 tr을 저장 */
		var tr = tbody.children();
		
		/* tr 클릭시 작동 */
		tr.click(function() {
			
			/* 인덱스에 따른 tr을 tr2로 저장 */
			var tr2 = $(this);
			
			/* 고객사번호, 고객사명을 저장 */
			var clientCompanyNo = tr2.children().eq(0).val();	
			var clientCompanyName = tr2.children().eq(1).text();	

			/* 위에서 저장한 값을 넣어줌 */
			$("input[name=clientCompanyNo]").attr("value", clientCompanyNo);
			$("input[name=clientCompanyName]").attr("value", clientCompanyName);
			
			/* id가 btn-close인 a태그가 클릭되어 모달창을 닫음 */
			$("a[id=btn-close]").click();
		});
	});
				
}

/* 담당자를 선택하기 위한 모달창 */
function selectUserList() {
		
	/* id가 modalOpen2인 a태그를 클릭 시 모달창이 열림 */
	$("a[id=modalOpen2]").click();
	
	/* ajax통신을 통해서 담당자들의 정보를 조회 */
	$.ajax({
		url: "/client/user",
		type: "get",
		success: function(data) {

			/* id가 userTbody인 것을 변수로 저장 */
			const $table = $("#userTbody");
			
			/* $table안에 내용을 비움 */
			$table.html("");
			
			/* data의 길이만큼 반복 */
			for(let index in data) {
				
				/* tr을 만듬 */
				$tr = $("<tr>");
			
				/* hidden타입의 input태그를 만들고 담당자번호를 값으로 지정함 */
				$userNoInput = $("<input>").attr("type", "hidden").val(data[index].userNo);
				
				/* td를 만들고 text값으로 사용자의 이름, 부서, 직책을 담음 */
				$nameTd = $("<td>").text(data[index].name);
				$deptNameTd = $("<td>").text(data[index].deptName);
				$positionTd = $("<td>").text(data[index].position);
			
				/* 위에서 만든 td를 tr에 담음 */
				$tr.append($userNoInput);
				$tr.append($nameTd);
				$tr.append($deptNameTd);
				$tr.append($positionTd);
				
				/* 위에서 만든 tr를 $table에 담음 */
				$table.append($tr);
			}
		},
		error: function(xhr) {
			console.log(xhr);
		}
	});
	
	/* 모달창안에서 id가 userTbody인 것을 클릭시 작동 */
	$("#userTbody").click(function() {
		
		/*  id가 userTbody를 인덱스에 따라 저장 */
		var tbody = $(this);
		
		/* tbody의 자식노드인 tr을 저장 */
		var tr = tbody.children();
		
		/* tr 클릭시 작동 */
		tr.click(function() {
			
			/* 인덱스에 따른 tr을 tr2로 저장 */
			var tr2 = $(this);
			
			/* 담당자 이름, 담당자 번호를 저장 */
			var userNo = tr2.children().eq(0).val();	
			var name = tr2.children().eq(1).text();	

			/* 위에서 저장한 값을 넣어줌 */
			$("input[name=userNo]").attr("value", userNo);
			$("input[name=name]").attr("value", name);
			
			/* id가 btn-close인 a태그를 클릭시 작동하여 모달창을 닫음 */
			$("a[id=btn-close]").click();
		});
	});
				
}

/* 고객 상세조회 페이지로 돌아가기 */
function back() {
	
	var clientNo = $("#clientNo").val();	//고객번호
	
	location.href = "/client/select?clientNo=" + clientNo;
	
}

/* 첨부파일 삭제 */
function deleteFile(button) {
	
	var fileNo = button.parentNode.children[2].value;	//파일번호
	
	$.ajax({
			url: "/client/file/delete",
			data: {fileNo : fileNo},	          
			type: 'POST',
			success: function(data) {
				if(data > 0) {				
					alert("파일 삭제를 성공하셨습니다.");
					location.reload(true);
				} else {
					alert("파일 삭제를 실패하였습니다.")
				}
			},
			error: function(xhr) {
				console.log(xhr);
				alert("에러발생");
			}
		})
	
}	

