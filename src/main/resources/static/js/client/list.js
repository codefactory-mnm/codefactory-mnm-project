/* 검색조건에서 담당자 입력을 위한 모달창 */
function openModal() {
				
	/* id가 modalOpen인 a태그를 클릭되어 모달창이 열림 */
	$("a[id=modalOpen]").click();
	
	/* ajax통신을 통해서 담당자들의 정보를 조회 */
	$.ajax({
		url: "/client/user",
		type: "get",
		success: function(data) {
			
			/* id가 userTbody인 것을 변수로 저장 */
			const $table = $("#userTbody");
			
			/* $table안에 내용을 비움 */
			$table.html("");
			
			/* data의 길이만큼 반복 */
			for(let index in data) {
				
				/* tr을 만듬 */
				$tr = $("<tr>");
				
				/* hidden타입의 input태그를 만들고 담당자번호를 값으로 지정함 */
				$userNoInput = $("<input>").attr("type", "hidden").val(data[index].userNo);
				
				/* td를 만들고 text값으로 사용자의 이름을 담음 */
				$nameTd = $("<td>").text(data[index].name);
				
				/* td를 만들고 text값으로 사용자의 부서을 담음 */
				$deptNameTd = $("<td>").text(data[index].deptName);
				
				/* td를 만들고 text값으로 사용자의 직위을 담음 */
				$positionTd = $("<td>").text(data[index].position);
				
				/* 위에서 만든 td를 tr에 담음 */
				$tr.append($userNoInput);
				$tr.append($nameTd);
				$tr.append($deptNameTd);
				$tr.append($positionTd);
				
				/* 위에서 만든 tr를 $table에 담음 */
				$table.append($tr);
			}
		},
		error: function(xhr) {
			console.log(xhr);
		}
	});

	
	/* 모달창안에서 id가 userTbody인 것을 클릭시 작동 */
	$("#userTbody").click(function() {
		
		/*  id가 userTbody를 인덱스에 따라 저장 */
		var tbody = $(this);
		
		/* tbody의 자식노드인 tr을 저장 */
		var tr = tbody.children();
		
		/* tr 클릭시 작동 */
		tr.click(function() {
			
			/* 인덱스에 따른 tr을 tr2로 저장 */
			var tr2 = $(this);
			
			/* 담당자 이름, 담당자 번호를 저장 */
			var userNo = tr2.children().eq(0).val();
			var name = tr2.children().eq(1).text();				
			
			/* 위에서 저장한 값을 넣어줌 */
			$("input[id=name]").attr("value", name);
			$("input[name=userNo]").attr("value", userNo);
			
			/* id가 btn-close인 a태그가 클릭되어 모달창을 닫음 */
			$("a[id=btn-close]").click();
		});
	});
	
}

/* 해당고객의 영업활동 내역을 보여주는 모덜창 */
function businessActivity(button) {
											
	/* id가 modalOpen2인 a태그를 클릭이 되어 모달창이 열림 */
	$("a[id=modalOpen2]").click();
	
	/* 고객번호를 가져옴 */
	var clientNo = button.parentNode.parentNode.children[0].children[1].children[0].value;
	
	/* ajax통신을 통해서 고객별 영업활동 내역을 가져옴 */
	$.ajax({
		url: "/client/businessAcitivity",
		data: {clientNo : clientNo},	
		type: "get",
		success: function(data) {
			
			/* id가 businessActivityTbody인 것을 변수로 저장 */
			const $table = $("#businessActivityTbody");
			
			/* $table안에 내용을 비움 */
			$table.html("");
			
			/* data의 길이만큼 반복 */
			for(let index in data) {
				
				/* tr을 만듬 */
				$tr = $("<tr>");
				
				/* hidden타입의 input태그를 만들고 고객번호를 값으로 지정함 */
				$businessActivityNoInput = $("<input>").attr("type", "hidden").val(data[index].businessActivityNo);
				
				/* td를 만들고 text값으로 영업활동일자, 영업활동분류, 영업활동목적, 영업기회명을 담음 */
				$activityDateTd = $("<td>").text(data[index].activityDate);
				$activityClassificationTd = $("<td>").text(data[index].activityClassification);
				$activityPurposeTd = $("<td>").text(data[index].activityPurpose);				
				$businessOpportunityNameTd = $("<td>").text(data[index].businessOpportunityName);
				
				/* 위에서 만든 td를 tr에 담음 */
				$tr.append($businessActivityNoInput);
				$tr.append($activityDateTd);
				$tr.append($activityClassificationTd);
				$tr.append($activityPurposeTd);
				$tr.append($businessOpportunityNameTd);
				
				/* 위에서 만든 tr를 $table에 담음 */
				$table.append($tr);
			}
		},
		error: function(xhr) {
			console.log(xhr);
		}
	});

	
	/* 모달창안에서 id가 businessActivityTbody인 것을 클릭시 작동 */
	$("#businessActivityTbody").click(function() {
		
		/*  id가 businessActivityTbody를 인덱스에 따라 저장 */
		var tbody = $(this);
		
		/* tbody의 자식노드인 tr을 저장 */
		var tr = tbody.children();
		
		/* tr 클릭시 작동 */
		tr.click(function() {
			
			/* 인덱스에 따른 tr을 tr2로 저장 */
			var tr2 = $(this);
			
			/* 영업활동번호 저장 */
			var businessActivityNo = tr2.children().eq(0).val();	

			location.href = "/businessActivity/detail?businessActivityNo=" + businessActivityNo;
		});
	
	})
}

/* 공통으로 사용할 link */
const link = "/client/list";

/* 검색조건이 없을 때 이전버튼 클릭 시 작동 */
function preButtonAction() {
	
	/* html에서 input태그에 hidden타입으로 controller로부터 받아온 값을 value를 저장해놓았음. 그 값을 script에서 저장 */
	var currentPage = $("#currentPageByPaging").val();
	
	location.href = link + "?currentPage=" + (currentPage - 1);
	
}

/* 검색조건이 없을 때 다음버튼 클릭 시 작동 */
function nextButtonAction() {
	
	/* html에서 input태그에 hidden타입으로 controller로부터 받아온 값을 value를 저장해놓았음. 그 값을 script에서 저장 */
	var currentPage = $("#currentPageByPaging").val();
	
	/* currentPage변수는 현재 문자열이기 때문에 숫자로 변환함 */
	currentPage *= 1;

	location.href = link + "?currentPage=" + (currentPage + 1);
	
}

/* 검색조건이 없을 때 숫자 클릭 시 작동 */
function pageButtonAction(text) {
		
    location.href = link + "?currentPage=" + text;
        
}

/* 검색조건이 있을 때 이전버튼 클릭 시 작동 */
function searchPreButtonAction() {
	
	/* html에서 input태그에 hidden타입으로 controller로부터 받아온 값을 value를 저장해놓았음. 그 값을 script에서 저장 */
	var currentPage = $("#currentPageByPaging").val();				//현재페이지
	var clientName = $("#clientNameByPaging").val();				//고객명
	var clientCompanyName = $("#clientCompanyNameByPaging").val();	//고객사명
	var phone = $("#phoneByPaging").val();							//휴대폰번호
	var email = $("#emailByPaging").val();							//이메일
	var userNo = $("#userNoByPaging").val();						//담당자번호
	
	var selectSearch = null;	//검색조건
	var searchValue = null;		//검색텍스트
	
	if(clientName != "") {
		selectSearch = "고객";
		searchValue = clientName;
	} else if(clientCompanyName != "") {
		selectSearch = "고객사";
		searchValue = clientCompanyName;
	} else if(phone != "") {
		selectSearch = "휴대폰번호";
		searchValue = phone;
	} else if(email != "") {
		selectSearch = "이메일";
		searchValue = email;
	}
	
	location.href = link + "?currentPage=" +  (currentPage - 1) + "&selectSearch=" + selectSearch + "&searchValue=" + searchValue + "&userNo=" + userNo;
}

/* 검색조건이 있을 때 다음버튼 클릭 시 작동 */
function searchNextButtonAction() {
	
	/* html에서 input태그에 hidden타입으로 controller로부터 받아온 값을 value를 저장해놓았음. 그 값을 script에서 저장 */
	var currentPage = $("#currentPageByPaging").val();	//현재페이지
	
	/* currentPage변수는 현재 문자열이기 때문에 숫자로 변환함 */
	currentPage *= 1;
	
	var clientName = $("#clientNameByPaging").val();				//고객명
	var clientCompanyName = $("#clientCompanyNameByPaging").val();	//고객사명
	var phone = $("#phoneByPaging").val();							//휴대폰번호
	var email = $("#emailByPaging").val();							//이메일
	var userNo = $("#userNoByPaging").val();						//담당자번호
	
	var selectSearch = null;	//검색조건
	var searchValue = null;		//검색텍스트
	
	if(clientName != "") {
		selectSearch = "고객";
		searchValue = clientName;
	} else if(clientCompanyName != "") {
		selectSearch = "고객사";
		searchValue = clientCompanyName;
	} else if(phone != "") {
		selectSearch = "휴대폰번호";
		searchValue = phone;
	} else if(email != "") {
		selectSearch = "이메일";
		searchValue = email;
	}
	
	location.href = link + "?currentPage=" +  (currentPage + 1) + "&selectSearch=" + selectSearch + "&searchValue=" + searchValue + "&userNo=" + userNo;
}

/* 검색조건이 있을 때 숫자 클릭 시 작동 */
function searchPageButtonAction(text) {
	  
	/* html에서 input태그에 hidden타입으로 controller로부터 받아온 값을 value를 저장해놓았음. 그 값을 script에서 저장 */  
	var clientName = $("#clientNameByPaging").val();				//고객명
	var clientCompanyName = $("#clientCompanyNameByPaging").val();	//고객사명
	var phone = $("#phoneByPaging").val();							//휴대폰번호
	var email = $("#emailByPaging").val();							//이메일
	var userNo = $("#userNoByPaging").val();						//담당자번호

	var selectSearch = null;	//검색조건
	var searchValue = null;		//검색텍스트
	
	if(clientName != "") {
		selectSearch = "고객";
		searchValue = clientName;
	} else if(clientCompanyName != "") {
		selectSearch = "고객사";
		searchValue = clientCompanyName;
	} else if(phone != "") {
		selectSearch = "휴대폰번호";
		searchValue = phone;
	} else if(email != "") {
		selectSearch = "이메일";
		searchValue = email;
	}
	
   location.href = link + "?currentPage=" +  text + "&selectSearch=" + selectSearch + "&searchValue=" + searchValue + "&userNo=" + userNo;
   		
}


function selectClient(div) {
					
	const clientNo = div.children[1].children[0].value;
	
	location.href = "/client/select?clientNo=" + clientNo;
	
} 