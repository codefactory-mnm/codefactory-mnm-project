/* 공통으로 사용할 link */
const link = "/userInfo";

/* 이전버튼 클릭 시 작동 */
function preButtonAction() {
	
	/* html에서 input태그에 hidden타입으로 controller로부터 받아온 값을 value를 저장해놓았음. 그 값을 script에서 저장 */
	var currentPage = $("#currentPageByPaging").val();
	
	/* 경로 이동 */
	location.href = link + "?currentPage=" + (currentPage - 1);
	
}

/* 다음버튼 클릭 시 작동 */
function nextButtonAction() {
	
	/* html에서 input태그에 hidden타입으로 controller로부터 받아온 값을 value를 저장해놓았음. 그 값을 script에서 저장 */
	var currentPage = $("#currentPageByPaging").val();
	
	/* currentPage변수는 현재 문자열이기 때문에 숫자로 변환함 */
	currentPage *= 1;

	/* 경로 이동 */
	location.href = link + "?currentPage=" + (currentPage + 1);
	
}

/* 숫자 클릭 시 작동 */
function pageButtonAction(text) {
		
	/* 경로 이동 */
    location.href = link + "?currentPage=" + text;
        
}