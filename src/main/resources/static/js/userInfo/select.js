/* 주소 입력 */
function userExecPostCode() {
    new daum.Postcode({
        oncomplete: function(data) {

            /* 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져옴*/
            if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                addr = data.roadAddress;
            } else { // 사용자가 지번 주소를 선택했을 경우(J)
                addr = data.jibunAddress;
            }

            /* 우편번호와 주소 정보를 해당 필드에 넣음 */
            document.getElementById('userZipCode').value = data.zonecode;
            document.getElementById("userAddress").value = addr;
            
            /* 커서를 상세주소 필드로 이동 */
            document.getElementById("userDetailedAddress").focus();
        }
    }).open();
}



$(function() {
	
	//최상단 체크박스 클릭
    $("#checkall").click(function(){
	
        //클릭되었으면
        if($("#checkall").prop("checked")){
	
            //input태그의 name이 userNo인 태그들을 찾아서 checked 옵션을 true로 정의
            $("input[name=userNo]").prop("checked", true);
            
            //클릭이 안되있으면
        }else{
	
            //input태그의 name이 userNo인 태그들을 찾아서 checked 옵션을 false로 정의
            $("input[name=userNo]").prop("checked", false);
        }
        
    });
    
    $("#authorityDetail").html("");
	
	$.ajax({
		url: "/userInfo/authorityChk",
		type: "get",
		success: function(data) {
			
			$.each(data, function(index, val) {
				$("#authorityDetail").append('<p><input type="checkbox" name="authorityChk" id="' + val.authorityNo + '" value="' + val.authorityNo + '"/> <label for="authority'+ index +'">' + val.authorityName.substring(5)  + '</label></p>');

			});
			
		},
		error: function(xhr) {
			alert("권한 체크 에러 발생!");
			console.log(xhr);
		}
	});
    
    // 사용자의 상세보기 클릭시 작동
    $(document).on("click", '.viewDetailCheck', function() {
	
		$("#userCompanyName").html("");
		$("#userDeptName").html("");
		
		$.ajax({
			url: "/userInfo/companyChk",
			type: "get",
			success: function(data) {
				
				$.each(data, function(index, val) {
					$("#userCompanyName").append('<option value="'+ val.companyNo +'">'+ val.companyName +'</option>');
	
				});
				
			},
			error: function(xhr) {
				alert("회사 체크 에러 발생!");
				console.log(xhr);
			}
		});
	
		$.ajax({
			url: "/userInfo/deptChk",
			type: "get",
			success: function(data) {
				
				$.each(data, function(index, val) {
					$("#userDeptName").append('<option value="'+ val.deptNo +'">'+ val.deptName +'</option>');
	
				});
				
			},
			error: function(xhr) {
				alert("부서 체크 에러 발생!");
				console.log(xhr);
			}
		});
	
		$('input:checkbox[name="authorityChk"]').attr("checked", false);
 
		var vdc = $(this).val();
		var param = {"userNo": vdc};
		
		$.ajax({
			url: "/userInfo/viewDetail",
			type: "get",
			data: param,
			success: function(data) {
				
				document.getElementById("userEnrollDate").value = data.enrollDate;
				document.getElementById("userName").value = data.name;
				document.getElementById("userId").value = data.id;
				document.getElementById("userEmail").value = data.email;
				/*document.getElementById("userPwd").value = (data.pwd).substring(0, 10);*/
				
				document.getElementById("userPhone").value = data.phone;
				document.getElementById("userBirthday").value = data.birthday;
				document.getElementById("userLandlineTel").value = data.landlineTel;
				document.getElementById("userJoinDate").value = data.joinDate;
				document.getElementById("userQuestion").value = data.question;
				
				document.getElementById("userAnswer").value = data.answer;
				
				for(var i = 0; i < $("#userCompanyName option").length; i++) {
					console.log($('#userCompanyName option:eq('+ i +')').val());
					if( $('#userCompanyName option:eq('+ i +')').val() == data.companyNo) {
						$('#userCompanyName option:eq('+ i +')').attr("selected", "selected");
					}
				}
				
				for(var i = 0; i < $("#userDeptName option").length; i++) {
					console.log($('#userDeptName option:eq('+ i +')').val());
					if( $('#userDeptName option:eq('+ i +')').val() == data.deptNo) {
						$('#userDeptName option:eq('+ i +')').attr("selected", "selected");
					}
				}

				document.getElementById("userPosition").value = data.position;
				document.getElementById("userJob").value = data.job;

				for (let i in data.authorityNo) {    
					$('input:checkbox[id="' + data.authorityNo[i] + '"]').attr("checked", true);
				}
				
				document.getElementById("userZipCode").value = data.zipCode;
				document.getElementById("userAddress").value = data.address;
				document.getElementById("userDetailedAddress").value = data.detailedAddress;
				
				if(data.withdrawYN == 'Y') {
					$("#withdrawY").prop('checked', true);
					$("#withdrawN").prop('checked', false);
				} else if(data.withdrawYN == 'N') {
					$("#withdrawN").prop('checked', true);
					$("#withdrawY").prop('checked', false);
				}
				
			},
			error: function(xhr) {
				alert("사용자 상세보기 에러 발생!");
				console.log(xhr);
			}
			
		});
 
    });

});

















