/* 주소 입력 */
function execPostCode() {
    new daum.Postcode({
        oncomplete: function(data) {

            /* 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져옴*/
            if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                addr = data.roadAddress;
            } else { // 사용자가 지번 주소를 선택했을 경우(J)
                addr = data.jibunAddress;
            }

            /* 우편번호와 주소 정보를 해당 필드에 넣음 */
            document.getElementById('zipCode').value = data.zonecode;
            document.getElementById("address").value = addr;
            
            /* 커서를 상세주소 필드로 이동 */
            document.getElementById("detailedAddress").focus();
        }
    }).open();
}


/* 아이디 중복확인 js */
function DuplicateCheckId() {
	
	var idCheck = $("#id").val();
	var param = {"id": idCheck};
	
	$.ajax({
		url: "/userInfo/idCheck",
		type: "post",
		data: param,
		dataType: "text",
		success: function(data) {
			
			var resultId = data;
			
			if(idCheck == ""){
				$("#id-null").show();
				$("#id-success").hide(); 
				$("#id-danger").hide();
				
			} else if(resultId === idCheck) {
				$("#id-danger").show();
				$("#id-success").hide();
				$("#id-null").hide();
				
			} else {
				$("#id-success").show();
				$("#id-danger").hide();
				$("#id-null").hide();
			}
			
		},
		error: function(xhr) {
			alert("아이디를 입력하세요.");
			console.log(xhr);
		}
	});
}

// 권한 selectbox 동적 생성하기
function insertFunc() {
	
	$("#authority").html("");
	
	$("#companyName").html("");
	$("#companyName").append('<option selected="selected">선택</option>');
	
	$("#deptName").html("");
	$("#deptName").append('<option selected="selected">선택</option>');
	
	$.ajax({
		url: "/userInfo/authorityChk",
		type: "get",
		success: function(data) {
			
			$.each(data, function(index, val) {
				$("#authority").append('<p><input type="checkbox" name="authority" id="authority' + index + '" value="' + val.authorityNo + '"/> <label for="authority'+ index +'">' + val.authorityName.substring(5) + '</label></p>');

			});
			
		},
		error: function(xhr) {
			alert("권한 체크 에러 발생!");
			console.log(xhr);
		}
	});
	
	$.ajax({
		url: "/userInfo/companyChk",
		type: "get",
		success: function(data) {
			
			$.each(data, function(index, val) {
				$("#companyName").append('<option value="'+ val.companyNo +'">'+ val.companyName +'</option>');

			});
			
		},
		error: function(xhr) {
			alert("회사 체크 에러 발생!");
			console.log(xhr);
		}
	});
	
	$.ajax({
		url: "/userInfo/deptChk",
		type: "get",
		success: function(data) {
			
			$.each(data, function(index, val) {
				$("#deptName").append('<option value="'+ val.deptNo +'">'+ val.deptName +'</option>');

			});
			
		},
		error: function(xhr) {
			alert("부서 체크 에러 발생!");
			console.log(xhr);
		}
	});
	
}



/* 아이디, 비밀번호, 비밀번호 확인 보이기, 숨기기 전용 js*/
$(function() { 
	
	$("input").keyup(function() {
		 
		var id = $("#id").val();
		
		var pwd1 = $("#pwd").val(); 
		var pwd2 = $("#pwdCheck").val();
		
		if(id == "") {
			$("#id-danger").hide();
			$("#id-success").hide();
			$("#id-null").hide();
		}
		 
		if(pwd1 != "" || pwd2 != "") { 
			if(pwd1 == pwd2) { 
				$("#alert-success").show(); 
				$("#alert-danger").hide(); 
				$("#insertSubmit").removeAttr("disabled"); 
				
			} else { 
				$("#alert-success").hide(); 
				$("#alert-danger").show(); 
				$("#insertSubmit").attr("disabled", "disabled");
				 
			} 
			
		} else if(pwd1 == "" && pwd2 == ""){
			$("#alert-success").hide(); 
			$("#alert-danger").hide(); 
		}
	}); 
});

/* 사용자 정보 추가 js*/
function insertUser() {
	
	// 권한 추가
	var authorityArr = [];
	
	$("input[name=authority]:checked").each(function(){
		var chk = $(this).val();
	    authorityArr.push(chk);
	})
	
	/*alert(authorityArr);*/

	var insertUserInfo = {
		
		id: $("#id").val(),
		name: $("#name").val(),
		email: $("#email").val(),
		pwd: $("#pwd").val(),
		pwdCheck: $("#pwdCheck").val(),
		phone: $("#phone").val(),
		birthday: $("#birthday").val(),
		landlineTel: $("#landlineTel").val(),
		joinDate: $("#joinDate").val(),
		question: $("#question").val(),
		answer: $("#answer").val(),
		companyNo: $("#companyName").val(),
		deptNo: $("#deptName").val(),
		position: $("#position").val(),
		job: $("#job").val(),
		zipCode: $("#zipCode").val(),
		address: $("#address").val(),
		detailedAddress: $("#detailedAddress").val(),
		authorityNo: authorityArr
		
	};
	
	$.ajax({
		url: "/userInfo/insert",
		type: "post",
		data: insertUserInfo,
		success: function(data) {
			
			if(data > 0) {
		
			alert("사용자 정보가 정상적으로 추가되었습니다.");
			location.href="/userInfo";
					
			} else {
				alert("사용자 정보 추가 실패!");
			}
			
		},
		error: function(xhr) {
			alert("사용자 정보 추가 에러 발생!");
			console.log(xhr);
		}
	});
	
}


