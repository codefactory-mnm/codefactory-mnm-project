/* 사용자 정보 삭제 처리 */
function withdrawUser() {
	
	if(confirm("정말 사용자를 탈퇴 처리하시겠습니까?")) {
		/*alert("삭제를 선택하셨습니다.");*/
		
		if($("input:checkbox[name=userNo]:checked").length <= 0) {
			alert("선택한 사용자가 없습니다.");
			
			return;
		}
		
		var list = [];	// userNo를 넘길 list 생성
		
		// name이 number인 체크박스에서 체크된 요소 선택
		$("input:checkbox[name=userNo]:checked").each(function() {
			
		    var checkVal = $(this).val(); // 체크된 값의 value값 가져오기
		    
		    list.push(checkVal);
		    
		});
		
		// userNO(문자열)와 체크박스 값들(배열)을 key/value 형태로 담는다.
    	var checkData = { "checkArray": list };
		    	    
		$.ajax({
			
	        url: "/userInfo/delete",
	        type:"post",
	        data: checkData,
	        success: function(data) {	// data로 1개 응답받음
	        	if(data > 0) {
		
					alert("사용자 정보가 정상적으로 탈퇴 처리되었습니다.");
					location.href="/userInfo";
					
				} else {
					alert("탈퇴 처리 실패!");
				}
			},
			error: function(xhr) {
				alert("탈퇴 에러 발생!");
				console.log(xhr);
				
			}
	        
    	});

     } else {
		alert("사용자 탈퇴를 취소하셨습니다.");
		
	}
		
} 
	
