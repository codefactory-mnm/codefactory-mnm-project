/* 비밀번호, 비밀번호 확인 보이기, 숨기기 전용 js*/
$(function() { 
	
	$("input").keyup(function() {
		 
		var pwd1 = $("#userPwd").val(); 
		var pwd2 = $("#userPwdCheck").val();
		
		if(pwd1 != "" || pwd2 != "") { 
			if(pwd1 == pwd2) { 
				$("#pwdCheck-success").show(); 
				$("#pwdCheck-danger").hide(); 
				$("#updateSubmit").removeAttr("disabled"); 
				
			} else { 
				$("#pwdCheck-success").hide(); 
				$("#pwdCheck-danger").show(); 
				$("#updateSubmit").attr("disabled", "disabled");
				 
			} 
			
		} else if(pwd1 == "" && pwd2 == ""){
			$("#pwdCheck-success").hide(); 
			$("#pwdCheck-danger").hide(); 
		}
	}); 
});


/* 사용자 정보 수정 */
function updateUser() {
	
	/* 전체 Checked Values 값을 담을 배열 */
	var userAuthority = [];
	
	/* 전체 Checked Values */
	var values = "";
	
	$('input[name="authorityChk"]:checked').each(function() {
	    values = $(this).val();
	    userAuthority.push(values);
	});
	
	var updateUserInfo = {
		
	withdrawYN: $('input:radio[name="withdrawYN"]:checked').val(),
	id: $("#userId").val(),
	name: $("#userName").val(),
	email: $("#userEmail").val(),
	pwd: $("#userPwd").val(),
	phone: $("#userPhone").val(),
	landlineTel: $("#userLandlineTel").val(),
	joinDate: $("#userJoinDate").val(),
	question: $("#userQuestion").val(),
	answer: $("#userAnswer").val(),
	CompanyName: $("#userCompanyName").val(),
	deptName: $("#userDeptName").val(),
	position: $("#userPosition").val(),
	job: $("#userJob").val(),
	zipCode: $("#userZipCode").val(),
	address: $("#userAddress").val(),
	detailedAddress: $("#userDetailedAddress").val(),
	userAuthorityList: userAuthority
	
	};

	$.ajax({
		url: "/userInfo/update",
		type: "post",
		data: updateUserInfo,
		success: function(data) {
			
			if(data > 0) {
		
			alert("사용자 정보가 정상적으로 수정되었습니다.");
			location.href="/userInfo";
					
			} else {
				alert("사용자 정보 수정 실패!");
			}
			
		},
		error: function(xhr) {
			alert("사용자 정보 수정 에러 발생!");
			console.log(xhr);
		}
	});
	
	
}