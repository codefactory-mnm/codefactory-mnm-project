package com.codefactory.mnm.organization.controller;

import java.security.Principal;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.organization.model.dto.ChangeHistoryDTO;
import com.codefactory.mnm.organization.model.dto.ChangeHistorySearchDTO;
import com.codefactory.mnm.organization.model.dto.OrganizationDTO;
import com.codefactory.mnm.organization.model.dto.OrganizationSearchDTO;
import com.codefactory.mnm.organization.model.service.OrganizationService;

@Controller
@RequestMapping("organization")
public class OrganizationController {
	
	private OrganizationService organizationService;

	/* OrganizationService 의존성 추입 */
	@Autowired
	public OrganizationController(OrganizationService organizationService) {
		this.organizationService = organizationService;
	}

	/* 조직 현황, 조직 변경이력 조회 */
	@GetMapping
	public ModelAndView organizationPage(ModelAndView mv, 
			@RequestParam(defaultValue = "1") String organizationCurrentPage,
			@RequestParam(defaultValue = "1") String changeHistoryCurrentPage) {
		
		OrganizationSearchDTO organizationSearch = new OrganizationSearchDTO();
		ChangeHistorySearchDTO changeHistorySearch = new ChangeHistorySearchDTO();
		
		organizationSearch.setCurrentPage(organizationCurrentPage);
		changeHistorySearch.setCurrentPage(changeHistoryCurrentPage);
		
		Map<String, Object> map = organizationService.selectOrganization(organizationSearch, changeHistorySearch);

		List<OrganizationDTO> organizationList = (List<OrganizationDTO>) map.get("organizationList");
		List<ChangeHistoryDTO> changeHistoryList = (List<ChangeHistoryDTO>) map.get("changeHistoryList");
		
		SelectCriteria organizationSelectCriteria = (SelectCriteria) map.get("organizationSelectCriteria");								//페이징 처리		
		SelectCriteria changeHistorySelectCriteria = (SelectCriteria) map.get("changeHistorySelectCriteria");							//페이징 처리
		
		int organizationSelectTotalCount = (int) map.get("organizationTotalCount");		
		int changeHistorySelectTotalCount = (int) map.get("changeHistoryTotalCount");		
		
//		List<OrganizationDTO> organizationList = organizationService.organizationEnrollList(currentPage);
//		List<ChangeHistoryDTO> changeHistoryList = organizationService.changeHistoryList(currentPage);
		
		mv.addObject("organizationList", organizationList);
		mv.addObject("changeHistoryList", changeHistoryList);
		
		mv.addObject("organizationSelectCriteria", organizationSelectCriteria);
		mv.addObject("changeHistorySelectCriteria", changeHistorySelectCriteria);
		
		mv.addObject("organizationSearch", organizationSearch);
		mv.addObject("changeHistorySearch", changeHistorySearch);
		
		mv.addObject("organizationSelectTotalCount", organizationSelectTotalCount);
		mv.addObject("changeHistorySelectTotalCount", changeHistorySelectTotalCount);
		
		mv.setViewName("organization/organization");

		return mv;
	}
	
	/* 조직 현황 상세보기 */
	@GetMapping(value = "enrollViewDetail", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public OrganizationDTO selectEnrollDetail(String deptNo) {
		
		OrganizationDTO enrollDetail = organizationService.selectEnrollDetail(deptNo);
		
		return enrollDetail;
	}
	
	/* 조직 변경 이력 상세보기 */
	@GetMapping(value = "changeHistoryViewDetail", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ChangeHistoryDTO selectChangeHistoryDetail(String deptNo) {
		
		ChangeHistoryDTO changeHistory  = organizationService.selectChangeHistoryDetail(deptNo);
		
		return changeHistory;
	}
	
	/* 조직 추가 */
	@PostMapping(value = "insert", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int insertDept(OrganizationDTO dept) {
		
		int result = organizationService.insertDept(dept);
		
		return result;
	}
	
	/* 조직 삭제 처리 */
	@PostMapping(value = "delete", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int deleteDept(@RequestParam(value = "checkArray[]") List<String> deptNoList) {
		
		int result = organizationService.deleteDept(deptNoList);
		
		return result;
	}
	
	/* 조직 정보 변경 */
	@PostMapping(value = "update", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int updateDept(OrganizationDTO dept, ChangeHistoryDTO changeHistoryDept, Principal pcp) {
		
		String userId = pcp.getName();
		
		changeHistoryDept.setModifier(userId);
		
		int result = organizationService.updateDept(dept, changeHistoryDept);
		
		return result;
	}

}
