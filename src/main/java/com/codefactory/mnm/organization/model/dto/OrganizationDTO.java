package com.codefactory.mnm.organization.model.dto;

import java.sql.Date;

public class OrganizationDTO {
	
	private java.sql.Date enrollDate;		//등록일
	private String deptNo;					//부서번호
	private String deptName;				//부서명
	private String deptHead;				//부서장
	private String refDeptName;				//상위부서
	private String sortOrder;				//정렬순서
	private String enrollNote;				//비고
	
	public OrganizationDTO() {}

	public OrganizationDTO(Date enrollDate, String deptNo, String deptName, String deptHead, String refDeptName,
			String sortOrder, String enrollNote) {
		super();
		this.enrollDate = enrollDate;
		this.deptNo = deptNo;
		this.deptName = deptName;
		this.deptHead = deptHead;
		this.refDeptName = refDeptName;
		this.sortOrder = sortOrder;
		this.enrollNote = enrollNote;
	}

	public java.sql.Date getEnrollDate() {
		return enrollDate;
	}

	public void setEnrollDate(java.sql.Date enrollDate) {
		this.enrollDate = enrollDate;
	}

	public String getDeptNo() {
		return deptNo;
	}

	public void setDeptNo(String deptNo) {
		this.deptNo = deptNo;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getDeptHead() {
		return deptHead;
	}

	public void setDeptHead(String deptHead) {
		this.deptHead = deptHead;
	}

	public String getRefDeptName() {
		return refDeptName;
	}

	public void setRefDeptName(String refDeptName) {
		this.refDeptName = refDeptName;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getEnrollNote() {
		return enrollNote;
	}

	public void setEnrollNote(String enrollNote) {
		this.enrollNote = enrollNote;
	}

	@Override
	public String toString() {
		return "OrganizationDTO [enrollDate=" + enrollDate + ", deptNo=" + deptNo + ", deptName=" + deptName
				+ ", deptHead=" + deptHead + ", refDeptName=" + refDeptName + ", sortOrder=" + sortOrder
				+ ", enrollNote=" + enrollNote + "]";
	}
	
}
