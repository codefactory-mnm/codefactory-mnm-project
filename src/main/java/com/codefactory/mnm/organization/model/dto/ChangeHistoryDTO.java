package com.codefactory.mnm.organization.model.dto;

import java.sql.Date;

public class ChangeHistoryDTO {
	
	private String modifier;				//변경자
	private java.sql.Date changeDate;		//변경일
	private String deptNo;					//부서번호
	private String deptName;				//부서명
	private String deptHead;				//부서장
	private String refDeptName;				//상위부서
	private String sortOrder;				//정렬순서
	private String changeHistoryNote;		//비고
	
	public ChangeHistoryDTO() {}
	
	public ChangeHistoryDTO(String modifier, Date changeDate, String deptNo, String deptName, String deptHead,
			String refDeptName, String sortOrder, String changeHistoryNote) {
		super();
		this.modifier = modifier;
		this.changeDate = changeDate;
		this.deptNo = deptNo;
		this.deptName = deptName;
		this.deptHead = deptHead;
		this.refDeptName = refDeptName;
		this.sortOrder = sortOrder;
		this.changeHistoryNote = changeHistoryNote;
	}



	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public java.sql.Date getChangeDate() {
		return changeDate;
	}

	public void setChangeDate(java.sql.Date changeDate) {
		this.changeDate = changeDate;
	}

	public String getDeptNo() {
		return deptNo;
	}

	public void setDeptNo(String deptNo) {
		this.deptNo = deptNo;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getDeptHead() {
		return deptHead;
	}

	public void setDeptHead(String deptHead) {
		this.deptHead = deptHead;
	}

	public String getRefDeptName() {
		return refDeptName;
	}

	public void setRefDeptName(String refDeptName) {
		this.refDeptName = refDeptName;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getChangeHistoryNote() {
		return changeHistoryNote;
	}

	public void setChangeHistoryNote(String changeHistoryNote) {
		this.changeHistoryNote = changeHistoryNote;
	}

	@Override
	public String toString() {
		return "ChangeHistoryDTO [modifier=" + modifier + ", changeDate=" + changeDate + ", deptNo=" + deptNo
				+ ", deptName=" + deptName + ", deptHead=" + deptHead + ", refDeptName=" + refDeptName + ", sortOrder="
				+ sortOrder + ", changeHistoryNote=" + changeHistoryNote + "]";
	}
	
}
