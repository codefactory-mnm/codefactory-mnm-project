package com.codefactory.mnm.organization.model.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.codefactory.mnm.common.Pagenation;
import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.organization.model.dao.OrganizationDAO;
import com.codefactory.mnm.organization.model.dto.ChangeHistoryDTO;
import com.codefactory.mnm.organization.model.dto.ChangeHistorySearchDTO;
import com.codefactory.mnm.organization.model.dto.OrganizationDTO;
import com.codefactory.mnm.organization.model.dto.OrganizationSearchDTO;

@Service
public class OrganizationServiceImpl implements OrganizationService {
	
	private OrganizationDAO organizationDAO;
	
	@Autowired
	public OrganizationServiceImpl(OrganizationDAO organizationDAO) {
		this.organizationDAO = organizationDAO;
	}

	// 조직 현황 상세보기
	@Override
	public OrganizationDTO selectEnrollDetail(String deptNo) {
		
		return organizationDAO.selectEnrollDetail(deptNo);
	}

	// 조직 변경이력 상세보기
	@Override
	public ChangeHistoryDTO selectChangeHistoryDetail(String deptNo) {
		
		return organizationDAO.selectChangeHistoryDetail(deptNo);
	}

	// 조직 추가
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertDept(OrganizationDTO dept) {
		
		return organizationDAO.insertDept(dept);
	}

	// 조직 삭제 처리
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int deleteDept(List<String> deptNoList) {
		
		return organizationDAO.deleteDept(deptNoList);
	}

	// 조직 수정
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int updateDept(OrganizationDTO dept, ChangeHistoryDTO changeHistoryDept) {
		
		changeHistoryDept.setDeptNo(dept.getDeptNo());
		changeHistoryDept.setDeptName(dept.getDeptName());
		changeHistoryDept.setDeptHead(dept.getDeptHead());
		changeHistoryDept.setRefDeptName(dept.getRefDeptName());
		changeHistoryDept.setSortOrder(dept.getSortOrder());
		changeHistoryDept.setChangeHistoryNote(dept.getEnrollNote());
		
		int result = 0;									//결과 반환
		
		// 조직 현황 update하고 조직 변경 이력 추가
		result += organizationDAO.updateDept(dept);
		result += organizationDAO.insertChangeHistory(changeHistoryDept);
		
		return result;
	}

	// 조직 현황, 조직 변경이력 조회
	@Override
	public Map<String, Object> selectOrganization(OrganizationSearchDTO organizationSearch, ChangeHistorySearchDTO changeHistorySearch) {
		
		/* 첫 요청페이지 */
		int organizationPageNo = 1;
		int changeHistoryPageNo = 1;
		
		/* 요청 페이지가 0이하면 1로 바꿔줌 */
		if(organizationPageNo <= 0) {
			organizationPageNo = 1;
		}
		
		if(changeHistoryPageNo <= 0) {
			changeHistoryPageNo = 1;
		}
		
		int limit = 5;
		
		int buttonAmount = 5;
		
		/* 전체 게시물 수, 검색조건이 있다면 검색조건에 맞는 게시물 수 */
		int organizationTotalCount = organizationDAO.organizationSelectTotalCount(); 
		int changeHistoryTotalCount = organizationDAO.changeHistorySelectTotalCount();
		
		/* view에서 전달받은 요청페이지*/
		String organizationCurrentPage = organizationSearch.getCurrentPage();
		String changeHistoryCurrentPage = changeHistorySearch.getCurrentPage();
		
		
		/* view에서 전달받은 요청페이지를 pageNo에 저장 */
		if(organizationCurrentPage != null && !"".equals(organizationCurrentPage)) {
			organizationPageNo = Integer.parseInt(organizationCurrentPage);
		}
		
		/* view에서 전달받은 요청페이지를 pageNo에 저장 */
		if(changeHistoryCurrentPage != null && !"".equals(changeHistoryCurrentPage)) {
			changeHistoryPageNo = Integer.parseInt(changeHistoryCurrentPage);
		}
		
		/* 설정한 변수에 따른 페이징조건 */
		SelectCriteria organizationSelectCriteria = Pagenation.getSelectCriteria(organizationPageNo, organizationTotalCount, limit, buttonAmount);
		SelectCriteria changeHistorySelectCriteria = Pagenation.getSelectCriteria(changeHistoryPageNo, changeHistoryTotalCount, limit, buttonAmount);
		
		Map<String, Object> map = new HashMap<>();

		organizationSearch.setSelectCriteria(organizationSelectCriteria);
		changeHistorySearch.setSelectCriteria(changeHistorySelectCriteria);
		
		List<OrganizationDTO> organizationList = organizationDAO.organizationEnrollList(organizationSearch);
		List<ChangeHistoryDTO> changeHistoryList = organizationDAO.changeHistoryList(changeHistorySearch);
		
		map.put("organizationList", organizationList);
		map.put("changeHistoryList", changeHistoryList);
		
		map.put("organizationSelectCriteria", organizationSelectCriteria);
		map.put("changeHistorySelectCriteria", changeHistorySelectCriteria);
		
		map.put("organizationTotalCount", organizationTotalCount);
		map.put("changeHistoryTotalCount", changeHistoryTotalCount);
		
		return map;
	}

}
