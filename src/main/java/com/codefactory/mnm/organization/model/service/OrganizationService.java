package com.codefactory.mnm.organization.model.service;

import java.util.List;
import java.util.Map;

import com.codefactory.mnm.organization.model.dto.ChangeHistoryDTO;
import com.codefactory.mnm.organization.model.dto.ChangeHistorySearchDTO;
import com.codefactory.mnm.organization.model.dto.OrganizationDTO;
import com.codefactory.mnm.organization.model.dto.OrganizationSearchDTO;

public interface OrganizationService {
	
	// 조직 현황, 조직 변경이력 조회
	public Map<String, Object> selectOrganization(OrganizationSearchDTO organizationSearch, ChangeHistorySearchDTO changeHistorySearch);

	public OrganizationDTO selectEnrollDetail(String deptNo);							//조직 현황 상세보기
	
	public ChangeHistoryDTO selectChangeHistoryDetail(String deptNo);					//조직 변경이력 상세보기

	public int insertDept(OrganizationDTO dept);										//조직 추가

	public int deleteDept(List<String> deptNoList);										//조직 삭제 처리

	public int updateDept(OrganizationDTO dept, ChangeHistoryDTO changeHistoryDept);	//조직 수정




}
