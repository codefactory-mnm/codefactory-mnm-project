package com.codefactory.mnm.organization.model.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.codefactory.mnm.organization.model.dto.ChangeHistoryDTO;
import com.codefactory.mnm.organization.model.dto.ChangeHistorySearchDTO;
import com.codefactory.mnm.organization.model.dto.OrganizationDTO;
import com.codefactory.mnm.organization.model.dto.OrganizationSearchDTO;

@Mapper
public interface OrganizationDAO {

	List<OrganizationDTO> organizationEnrollList(OrganizationSearchDTO organizationSearch);		// 조직 현황 조회
	
	List<ChangeHistoryDTO> changeHistoryList(ChangeHistorySearchDTO changeHistorySearch);		// 조직 변경이력 조회

	OrganizationDTO selectEnrollDetail(String deptNo);											// 조직 현황 상세보기
	
	ChangeHistoryDTO selectChangeHistoryDetail(String deptNo);									// 조직 변경이력 상세보기

	int insertDept(OrganizationDTO dept);														// 조직 추가

	int deleteDept(List<String> deptNoList);													// 조직 삭제처리

	int updateDept(OrganizationDTO dept);														// 조직 수정

	int insertChangeHistory(ChangeHistoryDTO changeHistoryDept);								// 조직 변경이력 추가

	int organizationSelectTotalCount();															// 조직 현황 게시물 수 조회

	int changeHistorySelectTotalCount();														// 조직 변경이력 게시물 수 조회



}
