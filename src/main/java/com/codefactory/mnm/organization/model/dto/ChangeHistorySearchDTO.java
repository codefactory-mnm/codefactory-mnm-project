package com.codefactory.mnm.organization.model.dto;

import com.codefactory.mnm.common.SelectCriteria;

public class ChangeHistorySearchDTO implements java.io.Serializable {
	
	private String currentPage;					//현재페이지
	private SelectCriteria selectCriteria;		//페이징처리
	
	public ChangeHistorySearchDTO() {}

	public ChangeHistorySearchDTO(String currentPage, SelectCriteria selectCriteria) {
		super();
		this.currentPage = currentPage;
		this.selectCriteria = selectCriteria;
	}

	public String getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(String currentPage) {
		this.currentPage = currentPage;
	}

	public SelectCriteria getSelectCriteria() {
		return selectCriteria;
	}

	public void setSelectCriteria(SelectCriteria selectCriteria) {
		this.selectCriteria = selectCriteria;
	}

	@Override
	public String toString() {
		return "ChangeHistorySearchDTO [currentPage=" + currentPage + ", selectCriteria=" + selectCriteria + "]";
	}
	
}
