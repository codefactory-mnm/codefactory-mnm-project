package com.codefactory.mnm.proposal.model.dto;

import java.sql.Date;

public class ProposalDTO implements java.io.Serializable {

	private String proposalNo;						//제안 번호
	private String proposalName;					//제안명
	private String content;							//제안내용
	private java.sql.Date requestDate;				//요청일
	private java.sql.Date startDate;				//제안기간 시작일
	private java.sql.Date endDate;					//제안기간 종료일
	private java.sql.Date submitDate;				//제출일
	private java.sql.Date releaseDate;				//발표일
	private String note;							//비고
	private String clientNo;						//고객번호
	private String clientName;						//고객명
	private String clientCompanyNo;					//고객사번호
	private String clientCompanyName;				//고객사명
	private String businessOpportunityNo;			//영업기회번호
	private String businessOpportunityName;			//영업기회명
	private String userNo;							//담당자번호
	private String name;							//담당자명
	private java.sql.Date writeDate;				//작성일자
	private String delYn;							//삭제여부
	private String fileName;						//파일명
	private String fileOriginalName;				//원본파일명
	private String fileNo;							//파일번호
	private String filePath;						//파일경로
	
	public ProposalDTO() {}

	public ProposalDTO(String proposalNo, String proposalName, String content, Date requestDate, Date startDate,
			Date endDate, Date submitDate, Date releaseDate, String note, String clientNo, String clientName,
			String clientCompanyNo, String clientCompanyName, String businessOpportunityNo,
			String businessOpportunityName, String userNo, String name, Date writeDate, String delYn, String fileName,
			String fileOriginalName, String fileNo, String filePath) {
		super();
		this.proposalNo = proposalNo;
		this.proposalName = proposalName;
		this.content = content;
		this.requestDate = requestDate;
		this.startDate = startDate;
		this.endDate = endDate;
		this.submitDate = submitDate;
		this.releaseDate = releaseDate;
		this.note = note;
		this.clientNo = clientNo;
		this.clientName = clientName;
		this.clientCompanyNo = clientCompanyNo;
		this.clientCompanyName = clientCompanyName;
		this.businessOpportunityNo = businessOpportunityNo;
		this.businessOpportunityName = businessOpportunityName;
		this.userNo = userNo;
		this.name = name;
		this.writeDate = writeDate;
		this.delYn = delYn;
		this.fileName = fileName;
		this.fileOriginalName = fileOriginalName;
		this.fileNo = fileNo;
		this.filePath = filePath;
	}

	public String getProposalNo() {
		return proposalNo;
	}

	public String getProposalName() {
		return proposalName;
	}

	public String getContent() {
		return content;
	}

	public java.sql.Date getRequestDate() {
		return requestDate;
	}

	public java.sql.Date getStartDate() {
		return startDate;
	}

	public java.sql.Date getEndDate() {
		return endDate;
	}

	public java.sql.Date getSubmitDate() {
		return submitDate;
	}

	public java.sql.Date getReleaseDate() {
		return releaseDate;
	}

	public String getNote() {
		return note;
	}

	public String getClientNo() {
		return clientNo;
	}

	public String getClientName() {
		return clientName;
	}

	public String getClientCompanyNo() {
		return clientCompanyNo;
	}

	public String getClientCompanyName() {
		return clientCompanyName;
	}

	public String getBusinessOpportunityNo() {
		return businessOpportunityNo;
	}

	public String getBusinessOpportunityName() {
		return businessOpportunityName;
	}

	public String getUserNo() {
		return userNo;
	}

	public String getName() {
		return name;
	}

	public java.sql.Date getWriteDate() {
		return writeDate;
	}

	public String getDelYn() {
		return delYn;
	}

	public String getFileName() {
		return fileName;
	}

	public String getFileOriginalName() {
		return fileOriginalName;
	}

	public String getFileNo() {
		return fileNo;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setProposalNo(String proposalNo) {
		this.proposalNo = proposalNo;
	}

	public void setProposalName(String proposalName) {
		this.proposalName = proposalName;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setRequestDate(java.sql.Date requestDate) {
		this.requestDate = requestDate;
	}

	public void setStartDate(java.sql.Date startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(java.sql.Date endDate) {
		this.endDate = endDate;
	}

	public void setSubmitDate(java.sql.Date submitDate) {
		this.submitDate = submitDate;
	}

	public void setReleaseDate(java.sql.Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public void setClientNo(String clientNo) {
		this.clientNo = clientNo;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public void setClientCompanyNo(String clientCompanyNo) {
		this.clientCompanyNo = clientCompanyNo;
	}

	public void setClientCompanyName(String clientCompanyName) {
		this.clientCompanyName = clientCompanyName;
	}

	public void setBusinessOpportunityNo(String businessOpportunityNo) {
		this.businessOpportunityNo = businessOpportunityNo;
	}

	public void setBusinessOpportunityName(String businessOpportunityName) {
		this.businessOpportunityName = businessOpportunityName;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}

	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setFileOriginalName(String fileOriginalName) {
		this.fileOriginalName = fileOriginalName;
	}

	public void setFileNo(String fileNo) {
		this.fileNo = fileNo;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	@Override
	public String toString() {
		return "ProposalDTO [proposalNo=" + proposalNo + ", proposalName=" + proposalName + ", content=" + content
				+ ", requestDate=" + requestDate + ", startDate=" + startDate + ", endDate=" + endDate + ", submitDate="
				+ submitDate + ", releaseDate=" + releaseDate + ", note=" + note + ", clientNo=" + clientNo
				+ ", clientName=" + clientName + ", clientCompanyNo=" + clientCompanyNo + ", clientCompanyName="
				+ clientCompanyName + ", businessOpportunityNo=" + businessOpportunityNo + ", businessOpportunityName="
				+ businessOpportunityName + ", userNo=" + userNo + ", name=" + name + ", writeDate=" + writeDate
				+ ", delYn=" + delYn + ", fileName=" + fileName + ", fileOriginalName=" + fileOriginalName + ", fileNo="
				+ fileNo + ", filePath=" + filePath + "]";
	}

	
}
