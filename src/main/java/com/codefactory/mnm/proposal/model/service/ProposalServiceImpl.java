package com.codefactory.mnm.proposal.model.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyAttachmentDTO;
import com.codefactory.mnm.common.Pagenation;
import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.contract.model.dto.ContractDTO;
import com.codefactory.mnm.estimate.model.dto.DeptListDTO;
import com.codefactory.mnm.proposal.model.dao.ProposalDAO;
import com.codefactory.mnm.proposal.model.dto.ProposalAttachmentDTO;
import com.codefactory.mnm.proposal.model.dto.ProposalDTO;
import com.codefactory.mnm.proposal.model.dto.ProposalInsertDTO;
import com.codefactory.mnm.proposal.model.dto.ProposalSearchDTO;

@Service
public class ProposalServiceImpl implements ProposalService {

	private final ProposalDAO proposalDAO;
	
	@Autowired
	public ProposalServiceImpl(ProposalDAO proposalDAO) {
		
		this.proposalDAO = proposalDAO;
	}
	
	/* 제안 등록 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertProposal(ProposalInsertDTO proposalInsertDTO) {

		/* controller에서 받아온 정보를 꺼낸다. */
		ProposalDTO proposalDTO = proposalInsertDTO.getProposalDTO();
		List<ProposalAttachmentDTO> files = proposalInsertDTO.getFiles();
		
		/* 제안 작성내용을 DB에 저장한다. */
		int result1 = proposalDAO.insertProposal(proposalDTO);
		
		/* 파일첨부정보를 DB에 저장한 횟수를 저장한다. */
		int result2 = 0;	
		if(files != null) {
			for(ProposalAttachmentDTO proposalAttachmentDTO : files) {
				result2 += proposalDAO.insertProposalAttachment(proposalAttachmentDTO);
			}	
		}
		
		/* 파일첨부가 없는 경우 : 제안을 정상적으로 등록하였을 경우 result = 1을 반환
		 * 파일첨부가 있는 경우 : 제안, 첨부파일을 정상적으로 등록하였을 경우 result = 1을 반환
		 */
		int result = 0;
		if(result1 == 1  &&  (files == null || result2 == files.size())) {
			result = 1;
		}
		
		return result;
	}

	/* 제안 목록 조회 */
	@Override
	public Map<String, Object> selectProposalList(ProposalSearchDTO proposalSearchDTO) {

		/* 첫 요청페이지 */
		int pageNo = 1;		
		
		/* 요청 페이지가 0이하면 1로 바꿔줌 */
		if(pageNo <= 0) {
			pageNo = 1;
		}
		
		/* 최대 페이지 */
		int limit = 10;
		
		/* 최대 버튼 갯수 */
		int buttonAmount = 5;
		
		/* 전체 게시물 수, 검색조건이 있다면 검색조건에 맞는 게시물 수 */
		int totalCount = proposalDAO.selectTotalCount(proposalSearchDTO);

		/* view에서 전달받은 요청페이지 */
		String currentPage = proposalSearchDTO.getCurrentPage();
		
		/* view에서 전달받은 요청페이지를 pageNo에 저장 */
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		/* 설정한 변수에 따른 페이징조건 */
		SelectCriteria selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount);
				
		/* 조회 결과를 담을 Map 생성 */
		Map<String, Object> map = new HashMap<>();
		
		proposalSearchDTO.setSelectCriteria(selectCriteria);
		
		/* 제안 목록 조회 */
		List<ProposalDTO> proposalList = proposalDAO.selectProposalList(proposalSearchDTO);
		
		/* 부서 조회 */
		List<DeptListDTO> deptList = proposalDAO.selectDeptList();
		
		map.put("totalCount", totalCount);
		map.put("selectCriteria", selectCriteria);
		map.put("proposalList", proposalList);
		map.put("deptList", deptList);
		
		return map;
	}

	/* 제안 상세조회 */
	@Override
	public Map<String, Object> selectProposal(String proposalNo) {

		Map<String, Object> map = new HashMap<>();
		
		ProposalDTO oneProposal = proposalDAO.selectProposal(proposalNo);
		List<ContractDTO> fileList = proposalDAO.selectFileList(proposalNo);
		
		map.put("oneProposal", oneProposal);
		map.put("fileList", fileList);
		
		return map;
	}

	/* 파일 정보 조회 */
	@Override
	public ProposalAttachmentDTO selectFile(String fileNo) {

		return proposalDAO.selectFile(fileNo);
	}

	/* 제안 삭제 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int deleteProposal(String proposalNo) {

		return proposalDAO.deleteProposal(proposalNo);
	}

	/* 제안 수정 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int updateProposal(ProposalInsertDTO proposalInsertDTO) {

		/* controller에서 받아온 정보를 꺼낸다. */
		ProposalDTO proposalDTO = proposalInsertDTO.getProposalDTO();
		List<ProposalAttachmentDTO> files = proposalInsertDTO.getFiles();
		
		/* 제안 작성내용을 DB에 저장한다. */
		int result1 = proposalDAO.updateProposal(proposalDTO);
		
		String proposalNo = proposalDTO.getProposalNo();
		
		/* 파일첨부정보를 DB에 저장한 횟수를 저장한다. */
		int result2 = 0;	
		if(files != null) {
			for(ProposalAttachmentDTO proposalAttachmentDTO : files) {
				proposalAttachmentDTO.setProposalNo(proposalNo);
				result2 += proposalDAO.updateProposalAttachment(proposalAttachmentDTO);
			}	
		}
		
		/* 파일첨부가 없는 경우 : 제안을 정상적으로 등록하였을 경우 result = 1을 반환
		 * 파일첨부가 있는 경우 : 제안, 첨부파일을 정상적으로 등록하였을 경우 result = 1을 반환
		 */
		int result = 0;
		if(result1 == 1  &&  (files == null || result2 == files.size())) {
			result = 1;
		}
		
		return result;
	}

	/* 첨부파일 삭제 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public Map<String, Object> deleteFile(String fileNo) {

		Map<String, Object> map = new HashMap<>();
		
		ProposalAttachmentDTO file = proposalDAO.selectFile(fileNo);		//첨부파일정보				
		int result = proposalDAO.deleteFile(fileNo);						//첨부파일 DB에서 삭제 결과
		
		/* map에 담아서 controller에 전달 */
		map.put("file", file);
		map.put("result", result);
		
		return map;
	}

}
