package com.codefactory.mnm.proposal.model.dto;

public class ProposalAttachmentDTO implements java.io.Serializable {

	private String fileNo;				//파일번호
	private String fileName;			//파일명
	private String fileOriginalName;	//실제파일명
	private String filePath;			//경로
	private String division;			//구분
	private String proposalNo;			//제안번호
	
	public ProposalAttachmentDTO() {}

	public ProposalAttachmentDTO(String fileNo, String fileName, String fileOriginalName, String filePath,
			String division, String proposalNo) {
		super();
		this.fileNo = fileNo;
		this.fileName = fileName;
		this.fileOriginalName = fileOriginalName;
		this.filePath = filePath;
		this.division = division;
		this.proposalNo = proposalNo;
	}

	public String getFileNo() {
		return fileNo;
	}

	public String getFileName() {
		return fileName;
	}

	public String getFileOriginalName() {
		return fileOriginalName;
	}

	public String getFilePath() {
		return filePath;
	}

	public String getDivision() {
		return division;
	}

	public String getProposalNo() {
		return proposalNo;
	}

	public void setFileNo(String fileNo) {
		this.fileNo = fileNo;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setFileOriginalName(String fileOriginalName) {
		this.fileOriginalName = fileOriginalName;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public void setProposalNo(String proposalNo) {
		this.proposalNo = proposalNo;
	}

	@Override
	public String toString() {
		return "ProposalAttachmentDTO [fileNo=" + fileNo + ", fileName=" + fileName + ", fileOriginalName="
				+ fileOriginalName + ", filePath=" + filePath + ", division=" + division + ", proposalNo=" + proposalNo
				+ "]";
	}
	
	
}
