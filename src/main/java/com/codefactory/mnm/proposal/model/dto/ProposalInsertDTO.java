package com.codefactory.mnm.proposal.model.dto;

import java.util.List;

public class ProposalInsertDTO implements java.io.Serializable {

	private ProposalDTO proposalDTO;				//제안 DTO
	private List<ProposalAttachmentDTO> files;		//제안 파일첨부 리스트
	
	public ProposalInsertDTO() {}

	public ProposalInsertDTO(ProposalDTO proposalDTO, List<ProposalAttachmentDTO> files) {
		super();
		this.proposalDTO = proposalDTO;
		this.files = files;
	}

	public ProposalDTO getProposalDTO() {
		return proposalDTO;
	}

	public List<ProposalAttachmentDTO> getFiles() {
		return files;
	}

	public void setProposalDTO(ProposalDTO proposalDTO) {
		this.proposalDTO = proposalDTO;
	}

	public void setFiles(List<ProposalAttachmentDTO> files) {
		this.files = files;
	}

	@Override
	public String toString() {
		return "ProposalInsertDTO [proposalDTO=" + proposalDTO + ", files=" + files + "]";
	}
	
	
}
