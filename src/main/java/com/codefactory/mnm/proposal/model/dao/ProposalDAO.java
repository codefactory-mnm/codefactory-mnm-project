package com.codefactory.mnm.proposal.model.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.codefactory.mnm.contract.model.dto.ContractDTO;
import com.codefactory.mnm.estimate.model.dto.DeptListDTO;
import com.codefactory.mnm.proposal.model.dto.ProposalAttachmentDTO;
import com.codefactory.mnm.proposal.model.dto.ProposalDTO;
import com.codefactory.mnm.proposal.model.dto.ProposalSearchDTO;

@Mapper
public interface ProposalDAO {

	int insertProposal(ProposalDTO proposalDTO);

	int insertProposalAttachment(ProposalAttachmentDTO proposalAttachmentDTO);

	int selectTotalCount(ProposalSearchDTO proposalSearchDTO);

	List<ProposalDTO> selectProposalList(ProposalSearchDTO proposalSearchDTO);

	List<DeptListDTO> selectDeptList();

	ProposalDTO selectProposal(String proposalNo);

	List<ContractDTO> selectFileList(String proposalNo);

	ProposalAttachmentDTO selectFile(String fileNo);

	int deleteProposal(String proposalNo);

	int updateProposal(ProposalDTO proposalDTO);

	int updateProposalAttachment(ProposalAttachmentDTO proposalAttachmentDTO);

	int deleteFile(String fileNo);

}
