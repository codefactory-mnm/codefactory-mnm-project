package com.codefactory.mnm.proposal.model.service;

import java.util.Map;

import com.codefactory.mnm.proposal.model.dto.ProposalAttachmentDTO;
import com.codefactory.mnm.proposal.model.dto.ProposalInsertDTO;
import com.codefactory.mnm.proposal.model.dto.ProposalSearchDTO;

public interface ProposalService {

	int insertProposal(ProposalInsertDTO proposalInsertDTO);

	Map<String, Object> selectProposalList(ProposalSearchDTO proposalSearchDTO);

	Map<String, Object> selectProposal(String proposalNo);

	ProposalAttachmentDTO selectFile(String fileNo);

	int deleteProposal(String proposalNo);

	int updateProposal(ProposalInsertDTO proposalInsertDTO);

	Map<String, Object> deleteFile(String fileNo);

}
