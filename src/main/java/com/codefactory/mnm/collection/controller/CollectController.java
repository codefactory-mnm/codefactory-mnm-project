package com.codefactory.mnm.collection.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.codefactory.mnm.collection.model.dto.CollectDTO;
import com.codefactory.mnm.collection.model.dto.CollectSearchDTO;
import com.codefactory.mnm.collection.model.service.CollectService;
import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.estimate.model.dto.DeptListDTO;

@Controller
@RequestMapping("/collect")
public class CollectController {
   
   private final CollectService collectService;
   
   @Autowired
   public CollectController(CollectService collectService) {
      
      this.collectService = collectService;
   }
   
   /* 수금 목록 조회 */
   @GetMapping("/list")
   public ModelAndView selectCollectList(ModelAndView mv,
         @RequestParam(defaultValue = "") String division,
         @RequestParam(defaultValue = "") String searchName,
         @RequestParam(defaultValue = "") String deptName,
         @RequestParam(defaultValue = "") String name,
         @RequestParam(defaultValue = "1") String currentPage) {
      
      System.out.println("division : " + division);
      System.out.println("searchName : " + searchName);
      System.out.println("deptName : " + deptName);
      System.out.println("name : " + name);
      System.out.println("currentPage : " + currentPage);
      
      /* 처음 매출 리스트를 조회하거나 또는 검색조건을 설정하지 않았을 때를 대비 */
      if(division.equals("")) { // 검색조건 중 구분을 선택하지 않았을 경우
         division = null;
      }
      
      if(searchName.equals("")) { // 검색조건 중 고객사를 입력하지 않았을 경우
         searchName = null;
      }
      
      if(deptName.equals("")) { // 검색조건 중 부서를 선택하지 않았을 경우
         deptName = null;
      }
      
      if(name.equals("")) { // 검색조건 중 이름을 선택하지 않았을 경우
         name = null;
      }
      
      /* 검색조건을 담는다. 검색조건이 없을 경우 null로 담김. */
      CollectSearchDTO collectSearchDTO = new CollectSearchDTO();
      collectSearchDTO.setDivision(division);
      collectSearchDTO.setSearchName(searchName);
      collectSearchDTO.setDeptName(deptName);
      collectSearchDTO.setName(name);
      collectSearchDTO.setCurrentPage(currentPage);
      
      System.out.println("collectSearchDTO : " + collectSearchDTO);
      
      /* 검색조건을 service로 보내주고, 값을 map으로 전달받음 */
      Map<String, Object> map = collectService.selectCollectList(collectSearchDTO);
      
      /* db에서 가져온 값을 꺼낸다. */
      List<CollectDTO> collectList = (List<CollectDTO>) map.get("collectList");
      List<DeptListDTO> deptList = (List<DeptListDTO>) map.get("deptList");
      SelectCriteria selectCriteria = (SelectCriteria) map.get("selectCriteria");
      int totalCount = (int) map.get("totalCount");
      System.out.println("collectList : " + collectList);
      System.out.println("deptList : " + deptList);
      System.out.println("selectCriteria : " + selectCriteria);
      
      /* db에서 조회해온 결과를 담는다 */
      mv.addObject("collectList", collectList);
      mv.addObject("deptList", deptList);
      mv.addObject("selectCriteria", selectCriteria);
      mv.addObject("totalCount", totalCount);
      mv.addObject("collectSearchDTO", collectSearchDTO);
      mv.setViewName("collection/list");
      
      return mv;
   }
   
   /* 수금 등록 페이지 연결 */
   @GetMapping("/insert")
   public String insertConsult() {
      
      return "collection/insert";
   }
   
   /* 수금 등록 */
   @PostMapping("/insert")
   public ModelAndView insertCollect(ModelAndView mv, CollectDTO collectDTO, HttpServletRequest request, RedirectAttributes rttr) {

      System.out.println("collectDTO : " + collectDTO); 
      
      int result = collectService.insertCollect(collectDTO);
      
      if(result > 0) {
         rttr.addFlashAttribute("message", "수금 등록을 성공하였습니다.");
      } else {
         rttr.addFlashAttribute("message", "수금 등록을 실패하였습니다.");
      }
      
      /* 수금 등록 후 연결할 페이지 */
      mv.setViewName("redirect:/collect/list");
      
      return mv;
   }
   
   /* 수금 상세조회 */
   @GetMapping("/detail")
   public ModelAndView selectCollect(ModelAndView mv, @RequestParam String collectionNo) {
      
      System.out.println("collectionNo : " + collectionNo);
      
      /* Map을 생성하여 서비스에서 값을 리턴 받는다*/
      CollectDTO collectDTO = collectService.selectCollect(collectionNo);
      
      /* map에서 꺼낸 값들을 담는다 */
      mv.addObject("collectDTO", collectDTO);
      mv.setViewName("collection/detail");
      
      return mv;
   }
   
   /* 수금 삭제 */
   @GetMapping("/delete")
   public ModelAndView deleteCollect(ModelAndView mv, @RequestParam String collectionNo, RedirectAttributes rttr) {

      System.out.println("collectionNo : " + collectionNo);

      /* 견적 번호를 받아서 해당 게시글을 삭제 한다 */
      int result = collectService.deleteCollect(collectionNo);

      if(result > 0) {
         rttr.addFlashAttribute("message", "상담 삭제를 성공하였습니다.");
      } else {
         rttr.addFlashAttribute("message", "상담 삭제를 실패하였습니다.");
      }

      /* 삭제 후 연결할 페이지 */
      mv.setViewName("redirect:/collect/list");

      return mv;
   }
   
   /* 수금 수정 페이지로 이동 */
   @GetMapping("/update")
   public ModelAndView updateCollectPage(ModelAndView mv, @RequestParam String collectionNo) {

      System.out.println("collectionNo : " + collectionNo);
      
      /* Map을 생성하여 서비스에서 값을 리턴 받는다*/
      CollectDTO collectDTO = collectService.selectCollect(collectionNo);
      
      /* map에서 꺼낸 값들을 담는다 */
      mv.addObject("collectDTO", collectDTO);
      mv.setViewName("collection/update");
      
      return mv;
   }
   
   @PostMapping("/update")
   public ModelAndView updateCollect(ModelAndView mv, CollectDTO collectDTO, RedirectAttributes rttr) {
      
      System.out.println("collectDTO : " + collectDTO); 
      
      int result = collectService.updateCollect(collectDTO);
      
      if(result > 0) {
         rttr.addFlashAttribute("message", "수금 수정을 성공하였습니다.");
      } else {
         rttr.addFlashAttribute("message", "수금 수정을 실패하였습니다.");
      }
      
      /* 수금 수정 후 연결할 페이지 */
      mv.setViewName("redirect:/collect/list");
      
      return mv;
   }
}