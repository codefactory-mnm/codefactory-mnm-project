package com.codefactory.mnm.collection.model.service;

import java.util.Map;

import com.codefactory.mnm.collection.model.dto.CollectDTO;
import com.codefactory.mnm.collection.model.dto.CollectSearchDTO;

public interface CollectService {

   /* 수금 목록 조회 */
   Map<String, Object> selectCollectList(CollectSearchDTO collectSearchDTO);

   /* 수금 등록 */
   int insertCollect(CollectDTO collectDTO);

   /* 수금 상세조회 */
   CollectDTO selectCollect(String collectionNo);

   /* 수금 삭제 */
   int deleteCollect(String collectionNo);

   /* 수금 수정 */
   int updateCollect(CollectDTO collectDTO);

}