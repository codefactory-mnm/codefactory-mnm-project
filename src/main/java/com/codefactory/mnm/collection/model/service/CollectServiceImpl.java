package com.codefactory.mnm.collection.model.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.codefactory.mnm.collection.model.dao.CollectDAO;
import com.codefactory.mnm.collection.model.dto.CollectDTO;
import com.codefactory.mnm.collection.model.dto.CollectSearchDTO;
import com.codefactory.mnm.common.Pagenation;
import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.estimate.model.dto.DeptListDTO;

@Service
public class CollectServiceImpl implements CollectService{

private final CollectDAO collectDAO;
   
   @Autowired
   public CollectServiceImpl(CollectDAO collectDAO) {
      
      this.collectDAO = collectDAO;
   }
   
   /* 수금 조회 */
   @Override
   public Map<String, Object> selectCollectList(CollectSearchDTO collectSearchDTO) {
      
      /* 첫 요청페이지 */
      int pageNo = 1;      
      
      /* 요청 페이지가 0이하면 1로 바꿔줌 */
      if(pageNo <= 0) {
         pageNo = 1;
      }
      
      /* 최대 페이지 */
      int limit = 10;
      
      /* 최대 버튼 갯수 */
      int buttonAmount = 5;
      
      /* 전체 게시물 수, 검색조건이 있다면 검색조건에 맞는 게시물 수 */
      int totalCount = collectDAO.selectTotalCount(collectSearchDTO);

      /* view에서 전달받은 요청페이지 */
      String currentPage = collectSearchDTO.getCurrentPage();
      
      /* view에서 전달받은 요청페이지를 pageNo에 저장 */
      if(currentPage != null && !"".equals(currentPage)) {
         pageNo = Integer.parseInt(currentPage);
      }
      
      /* 설정한 변수에 따른 페이징조건 */
      SelectCriteria selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount);
            
      /* 조회 결과를 담을 Map 생성 */
      Map<String, Object> map = new HashMap<>();
      
      collectSearchDTO.setSelectCriteria(selectCriteria);
      
      /* 수금 목록 조회 */
      List<CollectDTO> collectList = collectDAO.selectCollectList(collectSearchDTO);
      
      /* 부서 목록 조회 */
      List<DeptListDTO> deptList = collectDAO.selectDeptList();
      
      map.put("totalCount", totalCount);
      map.put("selectCriteria", selectCriteria);
      map.put("collectList", collectList);
      map.put("deptList", deptList);
      
      return map;
   }

   @Override
   @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
   public int insertCollect(CollectDTO collectDTO) {
      int result1 = collectDAO.insertCollect(collectDTO);
      /* int result2 = collectDAO.insertCollectHistory(collectDTO); */
      
      int result = 0;
      
      if (result1 == 1) {
         result = 1;
      }
      
      return result;
   }

   /* 수금 상세조회 */
   @Override
   public CollectDTO selectCollect(String collectionNo) {
      
      CollectDTO collectDTO = collectDAO.selectCollect(collectionNo);
      
      return collectDTO;
   }

   /* 수금 삭제 */
   @Override
   @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
   public int deleteCollect(String collectionNo) {

      int result = collectDAO.deleteCollect(collectionNo);
      
      return result;
   }

   /* 수금 수정 */
   @Override
   @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
   public int updateCollect(CollectDTO collectDTO) {
      
      int result = collectDAO.updateCollect(collectDTO);
      
      return result;
   }

}