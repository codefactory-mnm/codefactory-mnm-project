package com.codefactory.mnm.collection.model.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.codefactory.mnm.collection.model.dto.CollectDTO;
import com.codefactory.mnm.collection.model.dto.CollectSearchDTO;
import com.codefactory.mnm.estimate.model.dto.DeptListDTO;

@Mapper
public interface CollectDAO {

   /* 전체 게시물 수, 검색조건이 있다면 검색조건에 맞는 게시물 수 */
   int selectTotalCount(CollectSearchDTO collectSearchDTO);

   /* 수금 목록 조회 */
   List<CollectDTO> selectCollectList(CollectSearchDTO collectSearchDTO);

   /* 부서 목록 조회 */
   List<DeptListDTO> selectDeptList();

   /* 수금 등록 */
   int insertCollect(CollectDTO collectDTO);

   /* 수금 상세조회 */
   CollectDTO selectCollect(String collectionNo);

   /* 수금 삭제 */
   int deleteCollect(String collectionNo);

   /* 수금 수정 */
   int updateCollect(CollectDTO collectDTO);

   /* 수금 이력 등록 */
   /* int insertCollectHistory(CollectDTO collectDTO); */

}