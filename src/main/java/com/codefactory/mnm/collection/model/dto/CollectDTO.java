package com.codefactory.mnm.collection.model.dto;

import java.sql.Date;

public class CollectDTO {
   
   private String collectionNo;         			//수금번호
   private java.sql.Date transactionDate;   		//거래일자
   private String transactionWay;         			//거래방법
   private String currentReceiptAmount;  			//현 수납 금액
   private String totalAmont;            			//총 수납 금액
   private String taxBill;               			//세금계산서
   private String note;               				//비고
   private java.sql.Date writeDate;      			//작성일자
   private String fullPaymentYn;         			//완납여부
   private String contractNo;            			//계약번호
   private String contractName;         			//계약명
   private String clientNo;            				//고객번호
   private String clientName;            			//고객이름
   private String userNo;               			//담당자번호
   private String name;               				//담당자이름
   private String clientCompanyName;      			//고객사명
   private String clientCompanyNo;         			//고객사번호
   private String delYn;               				//삭제여부
   
   public CollectDTO() {}

   public CollectDTO(String collectionNo, Date transactionDate, String transactionWay, String currentReceiptAmount,
         String totalAmont, String taxBill, String note, Date writeDate, String fullPaymentYn, String contractNo,
         String contractName, String clientNo, String clientName, String userNo, String name,
         String clientCompanyName, String clientCompanyNo, String delYn) {
      super();
      this.collectionNo = collectionNo;
      this.transactionDate = transactionDate;
      this.transactionWay = transactionWay;
      this.currentReceiptAmount = currentReceiptAmount;
      this.totalAmont = totalAmont;
      this.taxBill = taxBill;
      this.note = note;
      this.writeDate = writeDate;
      this.fullPaymentYn = fullPaymentYn;
      this.contractNo = contractNo;
      this.contractName = contractName;
      this.clientNo = clientNo;
      this.clientName = clientName;
      this.userNo = userNo;
      this.name = name;
      this.clientCompanyName = clientCompanyName;
      this.clientCompanyNo = clientCompanyNo;
      this.delYn = delYn;
   }

   public String getCollectionNo() {
      return collectionNo;
   }

   public void setCollectionNo(String collectionNo) {
      this.collectionNo = collectionNo;
   }

   public java.sql.Date getTransactionDate() {
      return transactionDate;
   }

   public void setTransactionDate(java.sql.Date transactionDate) {
      this.transactionDate = transactionDate;
   }

   public String getTransactionWay() {
      return transactionWay;
   }

   public void setTransactionWay(String transactionWay) {
      this.transactionWay = transactionWay;
   }

   public String getCurrentReceiptAmount() {
      return currentReceiptAmount;
   }

   public void setCurrentReceiptAmount(String currentReceiptAmount) {
      this.currentReceiptAmount = currentReceiptAmount;
   }

   public String getTotalAmont() {
      return totalAmont;
   }

   public void setTotalAmont(String totalAmont) {
      this.totalAmont = totalAmont;
   }

   public String getTaxBill() {
      return taxBill;
   }

   public void setTaxBill(String taxBill) {
      this.taxBill = taxBill;
   }

   public String getNote() {
      return note;
   }

   public void setNote(String note) {
      this.note = note;
   }

   public java.sql.Date getWriteDate() {
      return writeDate;
   }

   public void setWriteDate(java.sql.Date writeDate) {
      this.writeDate = writeDate;
   }

   public String getFullPaymentYn() {
      return fullPaymentYn;
   }

   public void setFullPaymentYn(String fullPaymentYn) {
      this.fullPaymentYn = fullPaymentYn;
   }

   public String getContractNo() {
      return contractNo;
   }

   public void setContractNo(String contractNo) {
      this.contractNo = contractNo;
   }

   public String getContractName() {
      return contractName;
   }

   public void setContractName(String contractName) {
      this.contractName = contractName;
   }

   public String getClientNo() {
      return clientNo;
   }

   public void setClientNo(String clientNo) {
      this.clientNo = clientNo;
   }

   public String getClientName() {
      return clientName;
   }

   public void setClientName(String clientName) {
      this.clientName = clientName;
   }

   public String getUserNo() {
      return userNo;
   }

   public void setUserNo(String userNo) {
      this.userNo = userNo;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getClientCompanyName() {
      return clientCompanyName;
   }

   public void setClientCompanyName(String clientCompanyName) {
      this.clientCompanyName = clientCompanyName;
   }

   public String getClientCompanyNo() {
      return clientCompanyNo;
   }

   public void setClientCompanyNo(String clientCompanyNo) {
      this.clientCompanyNo = clientCompanyNo;
   }

   public String getDelYn() {
      return delYn;
   }

   public void setDelYn(String delYn) {
      this.delYn = delYn;
   }

   @Override
   public String toString() {
      return "CollectDTO [collectionNo=" + collectionNo + ", transactionDate=" + transactionDate + ", transactionWay="
            + transactionWay + ", currentReceiptAmount=" + currentReceiptAmount + ", totalAmont=" + totalAmont
            + ", taxBill=" + taxBill + ", note=" + note + ", writeDate=" + writeDate + ", fullPaymentYn="
            + fullPaymentYn + ", contractNo=" + contractNo + ", contractName=" + contractName + ", clientNo="
            + clientNo + ", clientName=" + clientName + ", userNo=" + userNo + ", name=" + name
            + ", clientCompanyName=" + clientCompanyName + ", clientCompanyNo=" + clientCompanyNo + ", delYn="
            + delYn + "]";
   }

   
   
}