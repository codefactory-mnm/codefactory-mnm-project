package com.codefactory.mnm.main.model.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.codefactory.mnm.businessactivity.model.dto.BusinessActivityListDTO;

@Mapper
public interface MainDAO {

	List<BusinessActivityListDTO> selectBusinessActivityListToday(String responsibility);

}
