package com.codefactory.mnm.main.model.service;

import java.time.LocalDate;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.codefactory.mnm.average.model.dao.AverageBusinessDAO;
import com.codefactory.mnm.average.model.dao.AverageCustomDAO;
import com.codefactory.mnm.average.model.dao.AverageSalesDAO;
import com.codefactory.mnm.average.model.dto.AverageBusinessOpportunityDTO;
import com.codefactory.mnm.average.model.dto.AverageClientDTO;
import com.codefactory.mnm.average.model.dto.AverageCompanyDTO;
import com.codefactory.mnm.average.model.dto.AverageCustomSearchDTO;
import com.codefactory.mnm.average.model.dto.AveragePotentialClientDTO;
import com.codefactory.mnm.average.model.dto.AverageSalesSearchDTO;
import com.codefactory.mnm.average.model.service.AverageCustomService;
import com.codefactory.mnm.businessactivity.model.dao.BusinessActivityDAO;
import com.codefactory.mnm.businessactivity.model.dto.BusinessActivityListDTO;
import com.codefactory.mnm.businessactivity.model.dto.BusinessActivitySearchDTO;
import com.codefactory.mnm.businessactivity.model.service.BusinessActivityService;
import com.codefactory.mnm.goal.model.dao.GoalDAO;
import com.codefactory.mnm.main.model.dao.MainDAO;

@Service("mainService")
public class MainServiceImpl implements MainService {
	
	private final AverageCustomDAO averageCustomDAO;
	private final AverageBusinessDAO averageBusinessDAO;
	private final AverageSalesDAO averageSalesDAO;
	private final GoalDAO goalDAO;
	private final MainDAO mainDAO;
	
	@Autowired
	public MainServiceImpl(AverageCustomDAO averageCustomDAO,
			AverageBusinessDAO averageBusinessDAO,
			AverageSalesDAO averageSalesDAO,
			GoalDAO goalDAO, 
			MainDAO mainDAO) {
		this.averageCustomDAO = averageCustomDAO;
		this.averageBusinessDAO = averageBusinessDAO;
		this.averageSalesDAO = averageSalesDAO;
		this.goalDAO = goalDAO;
		this.mainDAO = mainDAO;
	}

	@Override
	public Map<String, Object> selectMainContents(Map<String, Object> searchCriteria) {
		
		/* 받은 검색조건 꺼내기 */
		String todayDate = (String) searchCriteria.get("todayDate");
		String responsibility = (String) searchCriteria.get("responsibility");
		List<AverageSalesSearchDTO> searchList = (List<AverageSalesSearchDTO>)searchCriteria.get("averageSalesSearchDTOList");
		
		/* 쿼리문에 검색조건으로 사용할 날짜변수 선언 */
		LocalDate startLocalDate = YearMonth.now().atDay(1); 		//오늘이 속한 달의 첫째 날
		LocalDate endLocalDate = YearMonth.now().atEndOfMonth(); 		//오늘이 속한 달의 마지막 날
		
		java.sql.Date startDateSql = java.sql.Date.valueOf(startLocalDate);
		java.sql.Date endDateSql = java.sql.Date.valueOf(endLocalDate);
		
		/* 검색조건을 DTO에 담는다 */
		AverageCustomSearchDTO averageCustomSearchDTO = new AverageCustomSearchDTO();
		averageCustomSearchDTO.setStartDate(startDateSql);
		averageCustomSearchDTO.setEndDate(endDateSql);
		averageCustomSearchDTO.setResponsibility(responsibility);
		
		/* 1. 신규고객(고객사, 고객, 잠재고객) 표에 필요한 정보 담기 */
		/* '그래프' 탭의 신규고객 표 내용은 당월로 조건이 고정되어있다. 
		 * 오늘을 기준으로 당월, 전월, 전전월 값을 가져온다. */
		List<Integer> tableCustomList = averageCustomDAO.selectGraphCustomList(averageCustomSearchDTO);
		
		/* 3개월 평균을 내고 List에 넣어준다. */
		for(int i = 0; i < 3; i++) {
			
			int totalThreeMonth = 0;
			for(int j = 0; j < 3; j++) {
				totalThreeMonth += tableCustomList.get(i * 3 + j);
			}
			System.out.println("totalThreeMonth : " + totalThreeMonth);
			
			int threeMonthAverage = (totalThreeMonth / 3);
			
			tableCustomList.remove(i * 3 + 2);
			tableCustomList.add(i * 3 + 2, threeMonthAverage);
		}
		
		/* 당월만의 신규고객 리스트를 만든다. */
		List<Integer> badgeCustomThisMonthList = new ArrayList<>();
		badgeCustomThisMonthList.add(tableCustomList.get(0));	//당월 고객사
		badgeCustomThisMonthList.add(tableCustomList.get(3));	//당월 고객
		badgeCustomThisMonthList.add(tableCustomList.get(6));	//당월 잠재고객

		
		/* 2. 영업기회 중 사업유형별 그래프, 인지종류별 그래프에 맞는 값들을 가져오기 */
		/* 영업기회 중 검색기준에 맞는 목록들을 리스트로 가져온다. */
		List<AverageBusinessOpportunityDTO> boList = averageBusinessDAO.selectBusinessOpportunityList(averageCustomSearchDTO);
		
		/* 영업기회 중 오늘을 기준으로 당월, 전월, 전전월 값을 가져온다 */
		List<Integer> tableBusinessOpportunityList = averageBusinessDAO.selectGraphBOList(averageCustomSearchDTO);
		
		
		/* 사업유형(BUSINESS_TYPE) 별 count */
		int civilianType = 0;
		int officialType = 0;
		for(int i = 0; i < boList.size(); i++) {
			if(boList.get(i).getBusinessType().equals("민수사업")) {
				civilianType++;
			}
			
			if(boList.get(i).getBusinessType().equals("관공사업")) {
				officialType++;
			}
		}
		
		/* 인지경로(COGNITIVE_PATHWAY) 별 count */
		int acquaintance = 0;
		int newsPaper = 0;
		int internet = 0;
		int preexistence = 0;
		int etc = 0;
		for(int i = 0; i < boList.size(); i++) {
			if(boList.get(i).getCognitivePathway().equals("지인소개")) {
				acquaintance++;
			}
			
			if(boList.get(i).getCognitivePathway().equals("신문")) {
				newsPaper++;
			}
			
			if(boList.get(i).getCognitivePathway().equals("인터넷검색")) {
				internet++;
			}
			
			if(boList.get(i).getCognitivePathway().equals("기존고객")) {
				preexistence++;
			}
			
			if(boList.get(i).getCognitivePathway().equals("기타")) {
				etc++;
			}
		}
		
		/* 3. 매출관련 통계 */
		/* 매출 중 기간이 맞는 수량리스트 구하기 */
		List<Integer> quantityList = new ArrayList<>();
		for(int i = 0; i < searchList.size(); i++) {
			Integer quantity = averageSalesDAO.selectQuantityList(searchList.get(i));
			
			if(quantity == null) {
				quantity = 0;
			}
			
			quantityList.add(quantity);
		}
		
		/* 수량 합계, 평균, 최댓값 구하기 */
		int quantitySum = 0;
		double quantityAvg = 0.0;
		int quantityMax = 0;
		for(int i = 0; i < quantityList.size(); i++) {
			quantitySum += quantityList.get(i);
			
			if(quantityList.get(i) > quantityMax) {
				quantityMax = quantityList.get(i);
			}
		}
		
		if(quantitySum != 0) {
			quantityAvg = quantitySum / 12;
		}
		
		/* 매출 중 기간이 맞는 총액 리스트 구하기 */
		List<Integer> sumList = new ArrayList<>();
		for(int i = 0; i < searchList.size(); i++) {
			Integer sum = averageSalesDAO.selectSumList(searchList.get(i));
			
			if(sum == null) {
				sum = 0;
			}
			
			sumList.add(sum);
		}
		
		/* 총액 합계, 평균, 최댓값 구하기 */
		int sumSum = 0;
		double sumAvg = 0.0;
		int sumMax = 0;
		for(int i = 0; i <sumList.size(); i++) {
			sumSum += sumList.get(i);
			
			if(sumList.get(i) > sumMax) {
				sumMax = sumList.get(i);
			}
		}
		
		if(sumSum != 0) {
			sumAvg = sumSum / 12;
		}
		
		/* 목표 중 기간이 맞는 총액 리스트 구하기 */
		List<Integer> goalList = new ArrayList<>();
		for(int i = 0; i < searchList.size(); i++) {
			Integer goalByGroup = averageSalesDAO.selectGoalListByGroup(searchList.get(i));
			
			if(goalByGroup == null) {
				goalByGroup = 0;
			}
			
			goalList.add(goalByGroup);
		}
		
		/* 4. 영업활동 목록 가져오기
		 * 담당자(USER_NO)가 나고, 날짜(ACTIVITY_DATE)가 오늘인 영업활동만 가져온다. */
		List<BusinessActivityListDTO> baListToday = mainDAO.selectBusinessActivityListToday(responsibility);
		
		/* Controller 로 보내는 map */
		Map<String, Object> map = new HashMap<>();
		map.put("startLocalDate", startLocalDate);
		
		map.put("tableCustomList", tableCustomList);
		map.put("badgeCustomThisMonthList", badgeCustomThisMonthList);
		
		map.put("tableBusinessOpportunityList", tableBusinessOpportunityList);
		map.put("civilianType", civilianType);
		map.put("officialType", officialType);
		map.put("acquaintance", acquaintance);
		map.put("newsPaper", newsPaper);
		map.put("internet", internet);
		map.put("preexistence", preexistence);
		map.put("etc", etc);
		
		map.put("quantitySum", quantitySum);
		map.put("quantityAvg", quantityAvg);
		map.put("quantityMax", quantityMax);
		
		map.put("sumList", sumList);
		map.put("sumSum", sumSum);
		map.put("sumAvg", sumAvg);
		map.put("sumMax", sumMax);
		map.put("goalList", goalList);
		
		map.put("baListToday", baListToday);
		
		return map;
	}

}
