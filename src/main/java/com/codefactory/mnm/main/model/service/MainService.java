package com.codefactory.mnm.main.model.service;

import java.util.Map;

public interface MainService {

	Map<String, Object> selectMainContents(Map<String, Object> searchCriteria);

}
