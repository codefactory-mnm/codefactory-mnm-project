package com.codefactory.mnm.main.controller;

import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.codefactory.mnm.average.model.dto.AverageBusinessOpportunityDTO;
import com.codefactory.mnm.average.model.dto.AverageSalesSearchDTO;
import com.codefactory.mnm.businessactivity.model.dto.BusinessActivityListDTO;
import com.codefactory.mnm.main.model.service.MainService;
import com.codefactory.mnm.member.model.dto.UserImpl;

@Controller
public class MainController {
	
	private final MainService mainService;
	
	@Autowired
	public MainController(MainService mainService) {
		this.mainService = mainService;
	}
	
	/* main 페이지에 필요한 것들 가져오기 */
	@GetMapping("/main")
	public ModelAndView main(ModelAndView mv, 
			@AuthenticationPrincipal UserImpl userImpl) {
		
		/* 오늘날짜 추출 */
		LocalDate currentDate = LocalDate.now();
		String todayDate = currentDate.format(DateTimeFormatter.ofPattern("yyyyMMdd"));
		String todayYear = todayDate.substring(0, 4);
		
		/* 현재 로그인한 유저의 userNo 받기 */
		String responsibility = userImpl.getUserNo();
		
		/* 매출검색에 필요한 월을 담기. 
		 * main이므로 주어진 조건없이, 오늘을 기준으로 검색한다. */
		List<AverageSalesSearchDTO> averageSalesSearchDTOList = new ArrayList<>();
		java.sql.Date searchStartDate = null;
		java.sql.Date searchEndDate = null;
		
		for(int i = 1; i < 13; i++) {
			AverageSalesSearchDTO searchDTO = new AverageSalesSearchDTO();
			
			if(i < 9) {																	//10보다 작은 월일 경우.
				String getMonthStartDate = todayYear + "-0" + i + "-01";							//i월 첫날
				String getNextMonthStartDate = todayYear + "-0" + (i + 1) + "-01";				//i+1월의 첫날
				
				LocalDate getNextMonthStartDateLoc = LocalDate.parse(getNextMonthStartDate);
				LocalDate getMonthEndDate = getNextMonthStartDateLoc.minusDays(1);			//i월의 마지막 날짜
				
				searchStartDate = Date.valueOf(getMonthStartDate);
				searchEndDate = Date.valueOf(getMonthEndDate);
				
				/* 검색조건 DTO에 담아준다 */
				searchDTO.setMonthStartDate(searchStartDate);
				searchDTO.setMonthLastDate(searchEndDate);
				
				averageSalesSearchDTOList.add(searchDTO);
				
			} else if (i < 12) {															//10보다 큰 월일 경우.
				String getMonthStartDate = todayYear + "-" + i + "-01";							//i월 첫날
				String getNextMonthStartDate = todayYear + "-" + (i + 1) + "-01";				//i+1월의 첫날
				
				LocalDate getNextMonthStartDateLoc = LocalDate.parse(getNextMonthStartDate);
				LocalDate getMonthEndDate = getNextMonthStartDateLoc.minusDays(1);			//i월의 마지막 날짜
				
				searchStartDate = Date.valueOf(getMonthStartDate);
				searchEndDate = Date.valueOf(getMonthEndDate);
				
				/* 검색조건 DTO에 담아준다 */
				searchDTO.setMonthStartDate(searchStartDate);
				searchDTO.setMonthLastDate(searchEndDate);
				
				averageSalesSearchDTOList.add(searchDTO);
				
			} else {														//12월의 경우
				String getMonthStartDate = todayYear + "-" + i + "-01";							//i월 첫날
				String getMonthEndtDate = todayYear + "-" + i + "-31";							//i월의 마지막 날짜
				
				searchStartDate = Date.valueOf(getMonthStartDate);
				searchEndDate = Date.valueOf(getMonthEndtDate);
				
				/* 검색조건 DTO에 담아준다 */
				searchDTO.setMonthStartDate(searchStartDate);
				searchDTO.setMonthLastDate(searchEndDate);
				
				averageSalesSearchDTOList.add(searchDTO);
			}
		}
		
		/* 검색조건을 Map에 담기 */
		Map<String, Object> searchCriteria = new HashMap<>();
		searchCriteria.put("todayDate", todayDate);
		searchCriteria.put("responsibility", responsibility);
		searchCriteria.put("averageSalesSearchDTOList", averageSalesSearchDTOList);
		
		/* 필요한 값들 불러오기 */
		Map<String, Object> map = mainService.selectMainContents(searchCriteria);
		
		LocalDate startLocalDate = (LocalDate)map.get("startLocalDate");
		
		List<Integer> badgeCustomThisMonthList = (List<Integer>)map.get("badgeCustomThisMonthList");
		List<String> tableCustomList = (List<String>)map.get("tableCustomList");
		
		List<Integer> tableBusinessOpportunityList = (List<Integer>)map.get("tableBusinessOpportunityList");
		
		int civilianType = (int)map.get("civilianType");
		int officialType = (int)map.get("officialType");
		
		int acquaintance = (int)map.get("acquaintance");
		int newsPaper = (int)map.get("newsPaper");
		int internet = (int)map.get("internet");
		int preexistence = (int)map.get("preexistence");
		int etc = (int)map.get("etc");
		
		int quantitySum = (int)map.get("quantitySum");
		double quantityAvg = (double)map.get("quantityAvg");
		int quantityMax = (int)map.get("quantityMax");
		int sumSum = (int)map.get("sumSum");
		double sumAvg = (double)map.get("sumAvg");
		int sumMax = (int)map.get("sumMax");
		
		List<BusinessActivityListDTO> baListToday = (List<BusinessActivityListDTO>)map.get("baListToday");
		
		List<Integer> sumList = (List<Integer>)map.get("sumList");
		List<Integer> goalList = (List<Integer>)map.get("goalList");
		
		/* 넘길 값 add */
		mv.addObject("startLocalDate", startLocalDate);
		
		mv.addObject("badgeCustomThisMonthList", badgeCustomThisMonthList);
		mv.addObject("tableCustomList", tableCustomList);
		
		mv.addObject("tableBusinessOpportunityList", tableBusinessOpportunityList);
		mv.addObject("civilianType", civilianType);				//민수사업
		mv.addObject("officialType", officialType);				//관공사업
		
		mv.addObject("acquaintance", acquaintance);				//지인소개
		mv.addObject("newsPaper", newsPaper);					//신문
		mv.addObject("internet", internet);						//인터넷검색
		mv.addObject("preexistence", preexistence);				//기존고객
		mv.addObject("etc", etc);								//기타
		
		mv.addObject("quantitySum", quantitySum);				//수량 합계
		mv.addObject("quantityAvg", quantityAvg);				//수량 평균
		mv.addObject("quantityMax", quantityMax);				//수량 최댓값
		mv.addObject("sumSum", sumSum);							//총액 합계
		mv.addObject("sumAvg", sumAvg);							//총액 평균
		mv.addObject("sumMax", sumMax);							//총액 최댓값
		
		mv.addObject("baListToday", baListToday);				//
		
		mv.addObject("sumList", sumList);						//
		mv.addObject("goalList", goalList);						//
		
		mv.setViewName("main/main");
		
		return mv;
	}
	
	/* /로 리다이렉트만 해도 메인으로 가게끔 한다. */
	@PostMapping("/main")
	public String redirectMain() {
		return "redirect:/main";
	}

}
