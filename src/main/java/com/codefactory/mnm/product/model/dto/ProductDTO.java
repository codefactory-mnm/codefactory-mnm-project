package com.codefactory.mnm.product.model.dto;

import java.sql.Date;

public class ProductDTO {

	private String productNo;				//상품번호
	private String productName;				//상품명
	private String price;					//판매가
	private String briefDescription;		//간략설명
	private String productLink;				//상품링크
	private String tax;						//과세유무
	private String unit;					//단위
	private String standard;				//규격
	private java.sql.Date enrollDate;		//등록일자
	private String userNo;					//사용자번호
	private String name;					//담당자 이름
	private String delYn;					//삭제여부
	
	public ProductDTO() {}

	public ProductDTO(String productNo, String productName, String price, String briefDescription, String productLink,
			String tax, String unit, String standard, Date enrollDate, String userNo, String name, String delYn) {
		super();
		this.productNo = productNo;
		this.productName = productName;
		this.price = price;
		this.briefDescription = briefDescription;
		this.productLink = productLink;
		this.tax = tax;
		this.unit = unit;
		this.standard = standard;
		this.enrollDate = enrollDate;
		this.userNo = userNo;
		this.name = name;
		this.delYn = delYn;
	}

	public String getProductNo() {
		return productNo;
	}

	public String getProductName() {
		return productName;
	}

	public String getPrice() {
		return price;
	}

	public String getBriefDescription() {
		return briefDescription;
	}

	public String getProductLink() {
		return productLink;
	}

	public String getTax() {
		return tax;
	}

	public String getUnit() {
		return unit;
	}

	public String getStandard() {
		return standard;
	}

	public java.sql.Date getEnrollDate() {
		return enrollDate;
	}

	public String getUserNo() {
		return userNo;
	}

	public String getName() {
		return name;
	}

	public String getDelYn() {
		return delYn;
	}

	public void setProductNo(String productNo) {
		this.productNo = productNo;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public void setBriefDescription(String briefDescription) {
		this.briefDescription = briefDescription;
	}

	public void setProductLink(String productLink) {
		this.productLink = productLink;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public void setEnrollDate(java.sql.Date enrollDate) {
		this.enrollDate = enrollDate;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}

	@Override
	public String toString() {
		return "ProductDTO [productNo=" + productNo + ", productName=" + productName + ", price=" + price
				+ ", briefDescription=" + briefDescription + ", productLink=" + productLink + ", tax=" + tax + ", unit="
				+ unit + ", standard=" + standard + ", enrollDate=" + enrollDate + ", userNo=" + userNo + ", name="
				+ name + ", delYn=" + delYn + "]";
	}

	
	
}
