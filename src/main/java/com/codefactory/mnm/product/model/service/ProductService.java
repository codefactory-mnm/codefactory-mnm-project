package com.codefactory.mnm.product.model.service;

import java.util.Map;

import com.codefactory.mnm.product.model.dto.ProductDTO;
import com.codefactory.mnm.product.model.dto.ProductSearchDTO;

public interface ProductService {
	
	/* 상품 조회 */
	Map<String, Object> selectProductList(ProductSearchDTO productSearchDTO);

	/* 상품 등록 */
	int insertProduct(ProductDTO productDTO);

	/* 상품 상세조회 */
	ProductDTO selectProduct(String productNo);

	/* 상품 삭제 */
	int deleteProduct(String productNo);

	/* 상품 수정 */
	int updateProduct(ProductDTO productDTO);

}
