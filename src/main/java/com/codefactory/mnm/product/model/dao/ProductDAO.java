package com.codefactory.mnm.product.model.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.codefactory.mnm.estimate.model.dto.DeptListDTO;
import com.codefactory.mnm.product.model.dto.ProductDTO;
import com.codefactory.mnm.product.model.dto.ProductSearchDTO;

@Mapper
public interface ProductDAO {

	/* 전체 게시물 수, 검색조건이 있다면 검색조건에 맞는 게시물 수 */
	int selectTotalCount(ProductSearchDTO productSearchDTO);

	/* 상품 목록 조회 */
	List<ProductDTO> selectProductList(ProductSearchDTO productSearchDTO);

	/* 부서 목록 조회 */
	List<DeptListDTO> selectDeptList();

	/* 상품 등록 */
	int insertProduct(ProductDTO productDTO);

	/* 상품 이력 등록 */
	int insertProductHistory(ProductDTO productDTO);

	/* 상품 상세조회 */
	ProductDTO selectProduct(String productNo);

	/* 상품 삭제 */
	int deleteProduct(String productNo);

	/* 상품 수정 */
	int updateConsult(ProductDTO productDTO);

	/* 상품 수정 이력 등록 */
	int insertConsultHistoryByUpdate(ProductDTO productDTO);

	

}
