package com.codefactory.mnm.product.model.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.codefactory.mnm.common.Pagenation;
import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.estimate.model.dto.DeptListDTO;
import com.codefactory.mnm.product.model.dao.ProductDAO;
import com.codefactory.mnm.product.model.dto.ProductDTO;
import com.codefactory.mnm.product.model.dto.ProductSearchDTO;

@Service
public class ProductServiceImpl implements ProductService{

private final ProductDAO productDAO;
	
	@Autowired
	public ProductServiceImpl(ProductDAO productDAO) {
		
		this.productDAO = productDAO;
	}

	/* 상품 조회 */
	@Override
	public Map<String, Object> selectProductList(ProductSearchDTO productSearchDTO) {
		
		/* 첫 요청페이지 */
		int pageNo = 1;		
		
		/* 요청 페이지가 0이하면 1로 바꿔줌 */
		if(pageNo <= 0) {
			pageNo = 1;
		}
		
		/* 최대 페이지 */
		int limit = 10;
		
		/* 최대 버튼 갯수 */
		int buttonAmount = 5;
		
		/* 전체 게시물 수, 검색조건이 있다면 검색조건에 맞는 게시물 수 */
		int totalCount = productDAO.selectTotalCount(productSearchDTO);

		/* view에서 전달받은 요청페이지 */
		String currentPage = productSearchDTO.getCurrentPage();
		
		/* view에서 전달받은 요청페이지를 pageNo에 저장 */
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		/* 설정한 변수에 따른 페이징조건 */
		SelectCriteria selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount);
				
		/* 조회 결과를 담을 Map 생성 */
		Map<String, Object> map = new HashMap<>();
		
		productSearchDTO.setSelectCriteria(selectCriteria);
		
		/* 상품 목록 조회 */
		List<ProductDTO> productList = productDAO.selectProductList(productSearchDTO);
		
		/* 부서 목록 조회 */
		List<DeptListDTO> deptList = productDAO.selectDeptList();
		
		map.put("totalCount", totalCount);
		map.put("selectCriteria", selectCriteria);
		map.put("productList", productList);
		map.put("deptList", deptList);
		
		return map;
	}

	
	/* 상품 등록 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertProduct(ProductDTO productDTO) {
		
		int result1 = productDAO.insertProduct(productDTO);
		int result2 = productDAO.insertProductHistory(productDTO);
		
		int result = 0;
		
		if (result1 == 1 && result2 == 1) {
			result = 1;
		}
		
		return result;
	}

	/* 상품 상세조회 */
	@Override
	public ProductDTO selectProduct(String productNo) {
		
		ProductDTO productDTO = productDAO.selectProduct(productNo);
		
		return productDTO;
	}

	/* 상품 삭제 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int deleteProduct(String productNo) {
		
		int result = productDAO.deleteProduct(productNo);
		
		return result;
	}

	/* 상품 수정 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int updateProduct(ProductDTO productDTO) {
		
		int result1 = productDAO.updateConsult(productDTO);
		int result2 = productDAO.insertConsultHistoryByUpdate(productDTO);
		
		int result = 0;
		
		if (result1 == 1 && result2 == 1) {
			result = 1;
		}
		
		return result;
	}
	
}
