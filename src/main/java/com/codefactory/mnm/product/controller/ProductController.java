package com.codefactory.mnm.product.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.consulting.model.dto.ConsultDTO;
import com.codefactory.mnm.estimate.model.dto.DeptListDTO;
import com.codefactory.mnm.member.model.dto.UserImpl;
import com.codefactory.mnm.product.model.dto.ProductDTO;
import com.codefactory.mnm.product.model.dto.ProductSearchDTO;
import com.codefactory.mnm.product.model.service.ProductService;

@Controller
@RequestMapping("/product")
public class ProductController {
	
	private final ProductService productService;
	
	@Autowired
	public ProductController(ProductService productService) {
		
		this.productService = productService;
	}
	
	/* 상품 목록 조회 */
	   @GetMapping("/list")
	   public ModelAndView selectProductList(ModelAndView mv,
	         @RequestParam(defaultValue = "") String division,
	         @RequestParam(defaultValue = "") String searchName,
	         @RequestParam(defaultValue = "") String deptName,
	         @RequestParam(defaultValue = "") String name,
	         @RequestParam(defaultValue = "1") String currentPage) {
	      
	      System.out.println("division : " + division);
	      System.out.println("searchName : " + searchName);
	      System.out.println("deptName : " + deptName);
	      System.out.println("name : " + name);
	      System.out.println("currentPage : " + currentPage);
	      
	      /* 처음 매출 리스트를 조회하거나 또는 검색조건을 설정하지 않았을 때를 대비 */
	      if(division.equals("")) { // 검색조건 중 구분을 선택하지 않았을 경우
	         division = null;
	      }
	      
	      if(searchName.equals("")) { // 검색조건 중 고객사를 입력하지 않았을 경우
	         searchName = null;
	      }
	      
	      if(deptName.equals("")) { // 검색조건 중 부서를 선택하지 않았을 경우
	         deptName = null;
	      }
	      
	      if(name.equals("")) { // 검색조건 중 이름을 선택하지 않았을 경우
	         name = null;
	      }
	      
	      /* 검색조건을 담는다. 검색조건이 없을 경우 null로 담김. */
	      ProductSearchDTO productSearchDTO = new ProductSearchDTO();
	      productSearchDTO.setDivision(division);
	      productSearchDTO.setSearchName(searchName);
	      productSearchDTO.setDeptName(deptName);
	      productSearchDTO.setName(name);
	      productSearchDTO.setCurrentPage(currentPage);
	      
	      System.out.println("productSearchDTO : " + productSearchDTO);
	      
	      /* 검색조건을 service로 보내주고, 값을 map으로 전달받음 */
	      Map<String, Object> map = productService.selectProductList(productSearchDTO);
	      
	      /* db에서 가져온 값을 꺼낸다. */
	      List<ProductDTO> productList = (List<ProductDTO>) map.get("productList");
	      List<DeptListDTO> deptList = (List<DeptListDTO>) map.get("deptList");
	      SelectCriteria selectCriteria = (SelectCriteria) map.get("selectCriteria");
	      int totalCount = (int) map.get("totalCount");
	      System.out.println("productList : " + productList);
	      System.out.println("deptList : " + deptList);
	      System.out.println("selectCriteria : " + selectCriteria);
	      
	      /* db에서 조회해온 결과를 담는다 */
	      mv.addObject("productList", productList);
	      mv.addObject("deptList", deptList);
	      mv.addObject("selectCriteria", selectCriteria);
	      mv.addObject("totalCount", totalCount);
	      mv.addObject("productSearchDTO", productSearchDTO);
	      mv.setViewName("product/list");
	      
	      return mv;
	   }
	   
	   /* 상품 등록 페이지 연결 */
	   @GetMapping("/insert")
	   public String insertProduct() {
	      
	      return "product/insert";
	   }
	   
	   /* 상품 등록 */
	   @PostMapping("/insert")
	   public ModelAndView insertProduct(ModelAndView mv, ProductDTO productDTO, HttpServletRequest request, RedirectAttributes rttr, @AuthenticationPrincipal UserImpl customUser) {

	      System.out.println("productDTO : " + productDTO); 
	      
	      productDTO.setUserNo(customUser.getUserNo());
	      
	      int result = productService.insertProduct(productDTO);
	      
	      if(result > 0) {
	         rttr.addFlashAttribute("message", "상품 등록을 성공하였습니다.");
	      } else {
	         rttr.addFlashAttribute("message", "상품 등록을 실패하였습니다.");
	      }
	      
	      /* 상품 등록 후 연결할 페이지 */
	      mv.setViewName("redirect:/product/list");
	      
	      return mv;
	   }
		
	   /* 상품 상세조회 */
	   @GetMapping("/detail")
	   public ModelAndView selectProduct(ModelAndView mv, @RequestParam String productNo) {
	      
	      System.out.println("productNo : " + productNo);
	      
	      /* Map을 생성하여 서비스에서 값을 리턴 받는다*/
	      ProductDTO productDTO = productService.selectProduct(productNo);
	      
	      /* map에서 꺼낸 값들을 담는다 */
	      mv.addObject("productDTO", productDTO);
	      mv.setViewName("product/detail");
	      
	      return mv;
	   }
	   
	   /* 상품 삭제 */
	   @GetMapping("/delete")
	   public ModelAndView deleteProduct(ModelAndView mv, @RequestParam String productNo, RedirectAttributes rttr) {

	      System.out.println("productNo : " + productNo);

	      /* 상담 번호를 받아서 해당 게시글을 삭제 한다 */
	      int result = productService.deleteProduct(productNo);

	      if(result > 0) {
	         rttr.addFlashAttribute("message", "상품 삭제를 성공하였습니다.");
	      } else {
	         rttr.addFlashAttribute("message", "상품 삭제를 실패하였습니다.");
	      }

	      /* 삭제 후 연결할 페이지 */
	      mv.setViewName("redirect:/product/list");

	      return mv;
	   }
	   
	   /* 상품 수정 페이지로 이동 */
	   @GetMapping("/update")
	   public ModelAndView updateProductPage(ModelAndView mv, @RequestParam String productNo) {

	      System.out.println("productNo : " + productNo);
	      
	      /* Map을 생성하여 서비스에서 값을 리턴 받는다*/
	      ProductDTO productDTO = productService.selectProduct(productNo);
	      
	      /* map에서 꺼낸 값들을 담는다 */
	      mv.addObject("productDTO", productDTO);
	      mv.setViewName("product/update");
	      
	      return mv;
	   }
	   
	   @PostMapping("/update")
	   public ModelAndView updateProduct(ModelAndView mv, ProductDTO productDTO, RedirectAttributes rttr) {
	      
	      System.out.println("productDTO : " + productDTO); 
	      
	      int result = productService.updateProduct(productDTO);
	      
	      if(result > 0) {
	         rttr.addFlashAttribute("message", "상품 수정을 성공하였습니다.");
	      } else {
	         rttr.addFlashAttribute("message", "상품 수정을 실패하였습니다.");
	      }
	      
	      /* 상품 수정 후 연결할 페이지 */
	      mv.setViewName("redirect:/product/list");
	      
	      return mv;
	   }
}
