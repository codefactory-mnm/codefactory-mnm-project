package com.codefactory.mnm.userinfo.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.userinfo.model.dto.AuthorityDTO;
import com.codefactory.mnm.userinfo.model.dto.CompanyDTO;
import com.codefactory.mnm.userinfo.model.dto.DepartmentDTO;
import com.codefactory.mnm.userinfo.model.dto.InsertUserDTO;
import com.codefactory.mnm.userinfo.model.dto.UpdateUserDTO;
import com.codefactory.mnm.userinfo.model.dto.UserDetailDTO;
import com.codefactory.mnm.userinfo.model.dto.UserInfoDTO;
import com.codefactory.mnm.userinfo.model.dto.UserInfoSearchDTO;
import com.codefactory.mnm.userinfo.model.service.UserInfoService;

@Controller
@RequestMapping("userInfo")
public class UserInfoController {

	private UserInfoService userInfoService;
	
	/* UserInfoService 의존성 주입 */
	@Autowired
	public UserInfoController(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}
	
	/* userInfo 페이지 조회 메소드 */
	@GetMapping
	public ModelAndView userInfoPage(ModelAndView mv,
			@RequestParam(defaultValue = "1") String currentPage) {

		UserInfoSearchDTO userInfoSearch = new UserInfoSearchDTO();
		
		userInfoSearch.setCurrentPage(currentPage);
		
		Map<String, Object> map = userInfoService.selectUserInfo(userInfoSearch);
		
		List<UserInfoDTO> userInfoList = (List<UserInfoDTO>) map.get("userInfoList");
		SelectCriteria selectCriteria = (SelectCriteria) map.get("selectCriteria");
		int totalCount = (int) map.get("totalCount");
		
		mv.addObject("userInfoList", userInfoList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("userInfoSearch", userInfoSearch);
		mv.addObject("totalCount", totalCount);
		
		mv.setViewName("userInfo/userInfo");

		return mv;
	}
	
	/* 전체 권한 정보 조회 메소드 */
	@GetMapping(value = "authorityChk", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<AuthorityDTO> selectAuthority() {
		
		List<AuthorityDTO> authorityList = userInfoService.selectAuthorityList();
		
		return authorityList;
	}
	
	/* 전체 회사 정보 조회 메소드 */
	@GetMapping(value = "companyChk", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<CompanyDTO> selectCompany() {
		
		List<CompanyDTO> companyList = userInfoService.selectCompanyList();
		
		return companyList;
	}
	
	/* 전체 부서 정보 조회 메소드 */
	@GetMapping(value = "deptChk", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<DepartmentDTO> selectDepartment() {
		
		List<DepartmentDTO> departmentyList = userInfoService.selectDepartmentList();
		
		return departmentyList;
	}
	
	/* 아이디 중복 여부 검사용 메소드 */
	@PostMapping(value = "idCheck", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public String userIdCheck(@RequestParam("id") String userId) {
		
		String resultId = userInfoService.userIdCheck(userId);
		
		return resultId;

	}

	/* 사용자 정보 insert용 메소드 */
	@PostMapping(value = "insert", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int insertUser(InsertUserDTO user) {
		
		int result = userInfoService.insertUser(user);

		return result;
	}

	/* 사용자 정보 '삭제' 상태로 변경 메소드 */
	@PostMapping(value = "delete", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int withdrawUser(@RequestParam(value = "checkArray[]") List<String> userNoList) {

		int result = userInfoService.withdrawUser(userNoList);

		return result;
	}
	
	
	/* 상세보기 내역 조회 */
	@GetMapping(value = "viewDetail", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public UserDetailDTO selectUserDetailList(String userNo) {
		
		UserDetailDTO userDetail = userInfoService.selectUserDetailList(userNo);
		
		return userDetail;
	}
	
	/* 사용자 정보 수정 */
	@PostMapping(value = "update", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int UpdateUserInfo(UpdateUserDTO userInfo) {
		
		int result = userInfoService.updateUser(userInfo);
		
		return result;
	}

}
