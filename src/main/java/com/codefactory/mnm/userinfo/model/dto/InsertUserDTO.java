package com.codefactory.mnm.userinfo.model.dto;

import java.sql.Date;
import java.util.List;

public class InsertUserDTO {

	private String companyNo;				//회사번호
	private List<String> authorityNo;		//권한번호
	private String name;					//이름
	private String id;						//아이디
	private String pwd;						//비밀번호
	private String pwdCheck;				//비밀번호확인용
	private String phone;					//전화번호
	private String landlineTel;				//유선번호
	private String email;					//이메일
	private java.sql.Date birthday;			//생년월일
	private String question;				//질문
	private String answer;					//답변
	private String zipCode;					//우편번호
	private String address;					//주소
	private String detailedAddress;			//상세주소
	private String deptNo;					//부서번호
	private java.sql.Date joinDate;			//입사일
	private java.sql.Date resignationDate;	//퇴사일
	private String part;					//역할
	private String job;						//직책
	private String position;				//직위
	
	public InsertUserDTO() {}

	public InsertUserDTO(String companyNo, List<String> authorityNo, String name, String id, String pwd,
			String pwdCheck, String phone, String landlineTel, String email, Date birthday, String question,
			String answer, String zipCode, String address, String detailedAddress, String deptNo, Date joinDate,
			Date resignationDate, String part, String job, String position) {
		super();
		this.companyNo = companyNo;
		this.authorityNo = authorityNo;
		this.name = name;
		this.id = id;
		this.pwd = pwd;
		this.pwdCheck = pwdCheck;
		this.phone = phone;
		this.landlineTel = landlineTel;
		this.email = email;
		this.birthday = birthday;
		this.question = question;
		this.answer = answer;
		this.zipCode = zipCode;
		this.address = address;
		this.detailedAddress = detailedAddress;
		this.deptNo = deptNo;
		this.joinDate = joinDate;
		this.resignationDate = resignationDate;
		this.part = part;
		this.job = job;
		this.position = position;
	}

	public String getCompanyNo() {
		return companyNo;
	}

	public void setCompanyNo(String companyNo) {
		this.companyNo = companyNo;
	}

	public List<String> getAuthorityNo() {
		return authorityNo;
	}

	public void setAuthorityNo(List<String> authorityNo) {
		this.authorityNo = authorityNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getPwdCheck() {
		return pwdCheck;
	}

	public void setPwdCheck(String pwdCheck) {
		this.pwdCheck = pwdCheck;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getLandlineTel() {
		return landlineTel;
	}

	public void setLandlineTel(String landlineTel) {
		this.landlineTel = landlineTel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public java.sql.Date getBirthday() {
		return birthday;
	}

	public void setBirthday(java.sql.Date birthday) {
		this.birthday = birthday;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDetailedAddress() {
		return detailedAddress;
	}

	public void setDetailedAddress(String detailedAddress) {
		this.detailedAddress = detailedAddress;
	}

	public String getDeptNo() {
		return deptNo;
	}

	public void setDeptNo(String deptNo) {
		this.deptNo = deptNo;
	}

	public java.sql.Date getJoinDate() {
		return joinDate;
	}

	public void setJoinDate(java.sql.Date joinDate) {
		this.joinDate = joinDate;
	}

	public java.sql.Date getResignationDate() {
		return resignationDate;
	}

	public void setResignationDate(java.sql.Date resignationDate) {
		this.resignationDate = resignationDate;
	}

	public String getPart() {
		return part;
	}

	public void setPart(String part) {
		this.part = part;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	@Override
	public String toString() {
		return "InsertUserDTO [companyNo=" + companyNo + ", authorityNo=" + authorityNo + ", name=" + name + ", id="
				+ id + ", pwd=" + pwd + ", pwdCheck=" + pwdCheck + ", phone=" + phone + ", landlineTel=" + landlineTel
				+ ", email=" + email + ", birthday=" + birthday + ", question=" + question + ", answer=" + answer
				+ ", zipCode=" + zipCode + ", address=" + address + ", detailedAddress=" + detailedAddress + ", deptNo="
				+ deptNo + ", joinDate=" + joinDate + ", resignationDate=" + resignationDate + ", part=" + part
				+ ", job=" + job + ", position=" + position + "]";
	}
	

}
