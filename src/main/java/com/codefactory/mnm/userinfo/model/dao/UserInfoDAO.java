package com.codefactory.mnm.userinfo.model.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.codefactory.mnm.userinfo.model.dto.AuthorityDTO;
import com.codefactory.mnm.userinfo.model.dto.CompanyDTO;
import com.codefactory.mnm.userinfo.model.dto.DepartmentDTO;
import com.codefactory.mnm.userinfo.model.dto.InsertUserDTO;
import com.codefactory.mnm.userinfo.model.dto.UpdateUserDTO;
import com.codefactory.mnm.userinfo.model.dto.UserDetailDTO;
import com.codefactory.mnm.userinfo.model.dto.UserInfoDTO;
import com.codefactory.mnm.userinfo.model.dto.UserInfoSearchDTO;

@Mapper
public interface UserInfoDAO {

	List<UserInfoDTO> selectAllUserInfo(UserInfoSearchDTO userInfoSearch);	// 사용자 정보 리스트 조회
	
	String selectUserId(String userId);										// 사용자 id 중복검사용 조회
	
	int insertUser(InsertUserDTO user);										// 사용자 정보 추가
	
	int insertAuthority(InsertUserDTO user, List<String> list);				// 사용자 권한 정보 추가

	int withdrawUser(Map<String, List<String>> userNoMap);					// 사용자 정보 삭제 처리

	UserDetailDTO selectUserDetailList(String userNo);						// 사용자 정보 상세보기 리스트 조회
	
	int updateUserInfo(UpdateUserDTO user);									// 사용자 정보 수정 

	List<AuthorityDTO> selectAuthorityList();								// 권한 전체 조회
	
	List<String> userAuthorityList(String userNo);							// 사용자 정보 권한 조회

	int selectTotalCount();													// 사용자 정보 게시물 수 조회

	int deleteUserAuthority(UpdateUserDTO user);							// 사용자 권한 삭제

	int insertUserAuthority(UpdateUserDTO user, List<String> list);			// 사용자 권한 추가

	List<CompanyDTO> selectCompanyList();									// 회사 정보 전체 조회 

	List<DepartmentDTO> selectDepartmentList();								// 부서 정보 전체 조회

}
