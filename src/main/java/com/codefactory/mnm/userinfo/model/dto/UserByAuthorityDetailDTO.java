package com.codefactory.mnm.userinfo.model.dto;

public class UserByAuthorityDetailDTO {
	
	private String userNo;				//사용자번호
	private String authorityNo;			//권한번호
	
	public UserByAuthorityDetailDTO() {}

	public UserByAuthorityDetailDTO(String userNo, String authorityNo) {
		super();
		this.userNo = userNo;
		this.authorityNo = authorityNo;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getAuthorityNo() {
		return authorityNo;
	}

	public void setAuthorityNo(String authorityNo) {
		this.authorityNo = authorityNo;
	}

	@Override
	public String toString() {
		return "UserByAuthorityDetailDTO [userNo=" + userNo + ", authorityNo=" + authorityNo + "]";
	}
	

}
