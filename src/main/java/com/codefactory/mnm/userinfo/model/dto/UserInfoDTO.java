package com.codefactory.mnm.userinfo.model.dto;

public class UserInfoDTO {
	
	private String no;					//사용자번호
	private String name;				//이름
	private String id;					//id
	private String pwd;					//비밀번호
	private String withdrawYN;			//삭제여부
	private String joinDate;			//시작일
	private String resignationDate;		//종료일
	private long employeeNo;			//사원번호
	private String deptName;			//부서명
	private String job;					//직책
	private String position;			//직위
	private String email;				//메일
	private String landlineTel;			//유선번호
	private String phone;				//휴대전화번호
	
	public UserInfoDTO() {}

	public UserInfoDTO(String no, String name, String id, String pwd, String withdrawYN, String joinDate,
			String resignationDate, long employeeNo, String deptName, String job, String position, String email,
			String landlineTel, String phone) {
		super();
		this.no = no;
		this.name = name;
		this.id = id;
		this.pwd = pwd;
		this.withdrawYN = withdrawYN;
		this.joinDate = joinDate;
		this.resignationDate = resignationDate;
		this.employeeNo = employeeNo;
		this.deptName = deptName;
		this.job = job;
		this.position = position;
		this.email = email;
		this.landlineTel = landlineTel;
		this.phone = phone;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getWithdrawYN() {
		return withdrawYN;
	}

	public void setWithdrawYN(String withdrawYN) {
		this.withdrawYN = withdrawYN;
	}

	public String getJoinDate() {
		return joinDate;
	}

	public void setJoinDate(String joinDate) {
		this.joinDate = joinDate;
	}

	public String getResignationDate() {
		return resignationDate;
	}

	public void setResignationDate(String resignationDate) {
		this.resignationDate = resignationDate;
	}

	public long getEmployeeNo() {
		return employeeNo;
	}

	public void setEmployeeNo(long employeeNo) {
		this.employeeNo = employeeNo;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLandlineTel() {
		return landlineTel;
	}

	public void setLandlineTel(String landlineTel) {
		this.landlineTel = landlineTel;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return "UserInfoDTO [no=" + no + ", name=" + name + ", id=" + id + ", pwd=" + pwd + ", withdrawYN=" + withdrawYN
				+ ", joinDate=" + joinDate + ", resignationDate=" + resignationDate + ", employeeNo=" + employeeNo
				+ ", deptName=" + deptName + ", job=" + job + ", position=" + position + ", email=" + email
				+ ", landlineTel=" + landlineTel + ", phone=" + phone + "]";
	}

}
