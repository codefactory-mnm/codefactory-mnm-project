package com.codefactory.mnm.userinfo.model.dto;

public class CompanyDTO {
	
	private String companyNo;			//회사번호
	private String companyName;			//회사명
	
	public CompanyDTO() {}

	public CompanyDTO(String companyNo, String companyName) {
		super();
		this.companyNo = companyNo;
		this.companyName = companyName;
	}

	public String getCompanyNo() {
		return companyNo;
	}

	public void setCompanyNo(String companyNo) {
		this.companyNo = companyNo;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@Override
	public String toString() {
		return "CompanyDTO [companyNo=" + companyNo + ", companyName=" + companyName + "]";
	}

}
