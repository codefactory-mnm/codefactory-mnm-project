package com.codefactory.mnm.userinfo.model.service;

import java.util.List;
import java.util.Map;

import com.codefactory.mnm.userinfo.model.dto.AuthorityDTO;
import com.codefactory.mnm.userinfo.model.dto.CompanyDTO;
import com.codefactory.mnm.userinfo.model.dto.DepartmentDTO;
import com.codefactory.mnm.userinfo.model.dto.InsertUserDTO;
import com.codefactory.mnm.userinfo.model.dto.UpdateUserDTO;
import com.codefactory.mnm.userinfo.model.dto.UserDetailDTO;
import com.codefactory.mnm.userinfo.model.dto.UserInfoSearchDTO;

public interface UserInfoService {
	
	public Map<String, Object> selectUserInfo(UserInfoSearchDTO userInfoSearch);	// 사용자 정보 조회

	public int insertUser(InsertUserDTO user);										// 사용자 정보 추가
	
	public int withdrawUser(List<String> userNoList);								// 사용자 정보 삭제 처리

	public String userIdCheck(String userId);										// 사용자 ID 중복검사 체크
	
	public List<AuthorityDTO> selectAuthorityList();								// 권한 정보 전체 조회

	public UserDetailDTO selectUserDetailList(String userNo);						// 사용자 정보 상세보기
	
	public int updateUser(UpdateUserDTO user);										// 사용자 정보 수정

	public List<CompanyDTO> selectCompanyList();									// 회사 정보 전체 조회

	public List<DepartmentDTO> selectDepartmentList();								// 부서 정보 전체 조회


}
