package com.codefactory.mnm.userinfo.model.dto;

import java.util.List;

import com.codefactory.mnm.member.model.dto.AccessibleMenuByAuthorityDTO;

public class AuthorityDTO {
	
	private String authorityNo;		//권한정보
	private String authorityName;	//권한명
	
	private List<AccessibleMenuByAuthorityDTO> accessibleMenuByAuthorityList;		//권한별접근가능메뉴 리스트
	
	public AuthorityDTO() {}

	public AuthorityDTO(String authorityNo, String authorityName,
			List<AccessibleMenuByAuthorityDTO> accessibleMenuByAuthorityList) {
		super();
		this.authorityNo = authorityNo;
		this.authorityName = authorityName;
		this.accessibleMenuByAuthorityList = accessibleMenuByAuthorityList;
	}

	public String getAuthorityNo() {
		return authorityNo;
	}

	public void setAuthorityNo(String authorityNo) {
		this.authorityNo = authorityNo;
	}

	public String getAuthorityName() {
		return authorityName;
	}

	public void setAuthorityName(String authorityName) {
		this.authorityName = authorityName;
	}

	public List<AccessibleMenuByAuthorityDTO> getAccessibleMenuByAuthorityList() {
		return accessibleMenuByAuthorityList;
	}

	public void setAccessibleMenuByAuthorityList(List<AccessibleMenuByAuthorityDTO> accessibleMenuByAuthorityList) {
		this.accessibleMenuByAuthorityList = accessibleMenuByAuthorityList;
	}

	@Override
	public String toString() {
		return "AuthorityDTO [authorityNo=" + authorityNo + ", authorityName=" + authorityName
				+ ", accessibleMenuByAuthorityList=" + accessibleMenuByAuthorityList + "]";
	}
	
}
