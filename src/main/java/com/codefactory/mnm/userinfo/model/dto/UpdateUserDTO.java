package com.codefactory.mnm.userinfo.model.dto;

import java.sql.Date;
import java.util.List;

public class UpdateUserDTO {
	
	private String withdrawYN;
	private String id;
	private String name;
	private String email;
	private String pwd;
	private String phone;
	private String landlineTel;
	private java.sql.Date joinDate;
	private String question;
	private String answer;
	private String CompanyName;
	private String deptName;
	private String job;
	private String position;
	private String zipCode;
	private String address;
	private String detailedAddress;
	
	private List<String> userAuthorityList;
	
	public UpdateUserDTO() {}

	public UpdateUserDTO(String withdrawYN, String id, String name, String email, String pwd, String phone,
			String landlineTel, Date joinDate, String question, String answer, String companyName, String deptName,
			String job, String position, String zipCode, String address, String detailedAddress,
			List<String> userAuthorityList) {
		super();
		this.withdrawYN = withdrawYN;
		this.id = id;
		this.name = name;
		this.email = email;
		this.pwd = pwd;
		this.phone = phone;
		this.landlineTel = landlineTel;
		this.joinDate = joinDate;
		this.question = question;
		this.answer = answer;
		CompanyName = companyName;
		this.deptName = deptName;
		this.job = job;
		this.position = position;
		this.zipCode = zipCode;
		this.address = address;
		this.detailedAddress = detailedAddress;
		this.userAuthorityList = userAuthorityList;
	}

	public String getWithdrawYN() {
		return withdrawYN;
	}

	public void setWithdrawYN(String withdrawYN) {
		this.withdrawYN = withdrawYN;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getLandlineTel() {
		return landlineTel;
	}

	public void setLandlineTel(String landlineTel) {
		this.landlineTel = landlineTel;
	}

	public java.sql.Date getJoinDate() {
		return joinDate;
	}

	public void setJoinDate(java.sql.Date joinDate) {
		this.joinDate = joinDate;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getCompanyName() {
		return CompanyName;
	}

	public void setCompanyName(String companyName) {
		CompanyName = companyName;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDetailedAddress() {
		return detailedAddress;
	}

	public void setDetailedAddress(String detailedAddress) {
		this.detailedAddress = detailedAddress;
	}

	public List<String> getUserAuthorityList() {
		return userAuthorityList;
	}

	public void setUserAuthorityList(List<String> userAuthorityList) {
		this.userAuthorityList = userAuthorityList;
	}

	@Override
	public String toString() {
		return "UpdateUserDTO [withdrawYN=" + withdrawYN + ", id=" + id + ", name=" + name + ", email=" + email
				+ ", pwd=" + pwd + ", phone=" + phone + ", landlineTel=" + landlineTel + ", joinDate=" + joinDate
				+ ", question=" + question + ", answer=" + answer + ", CompanyName=" + CompanyName + ", deptName="
				+ deptName + ", job=" + job + ", position=" + position + ", zipCode=" + zipCode + ", address=" + address
				+ ", detailedAddress=" + detailedAddress + ", userAuthorityList=" + userAuthorityList + "]";
	}

}
