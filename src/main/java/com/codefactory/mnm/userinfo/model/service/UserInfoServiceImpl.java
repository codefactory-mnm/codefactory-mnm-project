package com.codefactory.mnm.userinfo.model.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.codefactory.mnm.common.Pagenation;
import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.userinfo.model.dao.UserInfoDAO;
import com.codefactory.mnm.userinfo.model.dto.AuthorityDTO;
import com.codefactory.mnm.userinfo.model.dto.CompanyDTO;
import com.codefactory.mnm.userinfo.model.dto.DepartmentDTO;
import com.codefactory.mnm.userinfo.model.dto.InsertUserDTO;
import com.codefactory.mnm.userinfo.model.dto.UpdateUserDTO;
import com.codefactory.mnm.userinfo.model.dto.UserDetailDTO;
import com.codefactory.mnm.userinfo.model.dto.UserInfoDTO;
import com.codefactory.mnm.userinfo.model.dto.UserInfoSearchDTO;

@Service
public class UserInfoServiceImpl implements UserInfoService {
	
	private UserInfoDAO userInfoDAO;
	
	@Autowired
	public UserInfoServiceImpl(UserInfoDAO userInfoDAO) {
		this.userInfoDAO = userInfoDAO;
	}
	
	/* 사용자 정보 전체 조회 */
	@Override
	public Map<String, Object> selectUserInfo(UserInfoSearchDTO userInfoSearch) {
		
		/* 첫 요청페이지 */
		int pageNo = 1;
		
		/* 요청 페이지가 0이하면 1로 바꿔줌 */
		if(pageNo <= 0) {
			pageNo = 1;
		}
		
		int limit = 10;
		
		int buttonAmount = 5;
		
		/* 전체 게시물 수, 검색조건이 있다면 검색조건에 맞는 게시물 수 */
		int totalCount = userInfoDAO.selectTotalCount(); 
		
		/* view에서 전달받은 요청페이지*/
		String currentPage = userInfoSearch.getCurrentPage();
		
		/* view에서 전달받은 요청페이지를 pageNo에 저장 */
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		/* 설정한 변수에 따른 페이징조건 */
		SelectCriteria selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount);
		
		Map<String, Object> map = new HashMap<>();

		userInfoSearch.setSelectCriteria(selectCriteria);
		
		List<UserInfoDTO> userInfoList = userInfoDAO.selectAllUserInfo(userInfoSearch);
		
		map.put("userInfoList", userInfoList);
		map.put("selectCriteria", selectCriteria);
		map.put("totalCount", totalCount);
		
		return map;
	}
	
	/* 사용자 ID 중복검사 체크 */
	@Override
	public String userIdCheck(String userId) {
		
		String id = userInfoDAO.selectUserId(userId);
		
		return id;
	}
	
	/* 권한 정보 전체 조회 */
	@Override
	public List<AuthorityDTO> selectAuthorityList() {
		
		return userInfoDAO.selectAuthorityList();
	}
	
	/* 회사 정보 전체 조회 */
	@Override
	public List<CompanyDTO> selectCompanyList() {
		
		return userInfoDAO.selectCompanyList();
	}
	
	/* 부서 정보 전체 조회 */
	@Override
	public List<DepartmentDTO> selectDepartmentList() {
		
		return userInfoDAO.selectDepartmentList();
	}

	/* 사용자 정보 추가 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertUser(InsertUserDTO user) {
		
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();	// 암호를 해시시키기 위해 객체 생성

		String pwd = encoder.encode(user.getPwd());		// 입력한 비밀번호 암호화
		
		user.setPwd(pwd);								// 암호화된 비밀번호 set
		
		int result = 0;		// insert 처리 결과 여부
		
		result += userInfoDAO.insertUser(user);		// DB 로직 처리
		result += userInfoDAO.insertAuthority(user, user.getAuthorityNo());
		
		return result;
	}

	/* 사용자 정보 삭제 처리 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int withdrawUser(List<String> userNoList) {
		
		Map<String, List<String>> userNoMap = new HashMap<>();
		userNoMap.put("userNoList", userNoList);
		
		int result = userInfoDAO.withdrawUser(userNoMap);
		
		return result;
	}

	/* 사용자 정보 상세보기 */
	@Override
	public UserDetailDTO selectUserDetailList(String userNo) {
		
		UserDetailDTO userDetailList = userInfoDAO.selectUserDetailList(userNo);
		
		List<String> userAuthorityList = userInfoDAO.userAuthorityList(userNo);
		
		userDetailList.setAuthorityNo(userAuthorityList);
		
		return userDetailList;
	}

	/* 사용자 정보 수정 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int updateUser(UpdateUserDTO user) {
		
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();	// 암호를 해시시키기 위해 객체 생성

		String pwd = encoder.encode(user.getPwd());		// 입력한 비밀번호 암호화
		
		user.setPwd(pwd);								// 암호화된 비밀번호 넣어주기
		
		int result = 0; 							 	// update 처리 결과 여부 
				
		result += userInfoDAO.updateUserInfo(user);		// DB 로직 처리

		if(user.getUserAuthorityList().size() >= 1) {	// 사용자 권한 확인 후 delete 
			result += userInfoDAO.deleteUserAuthority(user);
		}
		
		// 체크한 사용자 권한 insert 
		result += userInfoDAO.insertUserAuthority(user, user.getUserAuthorityList());
		
		return result;
	}

}












