package com.codefactory.mnm.mypage.model.dto;

public class MyInfoDTO {
	
	private String name;				//이름
	private String id;					//아이디
	private String pwd;					//비밀번호
	private String phone;				//전화번호
	private String email;				//이메일
	private String birthday;			//생년월일
	private String question;			//질문
	private String answer;				//답변
	private String zipCode;				//우편번호
	private String address;				//주소
	private String detailedAddress;		//상세주소
	private String employeeNo;			//사원번호
	private String position;			//직급
	private String deptName;			//부서
	
	public MyInfoDTO() {}

	public MyInfoDTO(String name, String id, String pwd, String phone, String email, String birthday, String question,
			String answer, String zipCode, String address, String detailedAddress, String employeeNo, String position,
			String deptName) {
		super();
		this.name = name;
		this.id = id;
		this.pwd = pwd;
		this.phone = phone;
		this.email = email;
		this.birthday = birthday;
		this.question = question;
		this.answer = answer;
		this.zipCode = zipCode;
		this.address = address;
		this.detailedAddress = detailedAddress;
		this.employeeNo = employeeNo;
		this.position = position;
		this.deptName = deptName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDetailedAddress() {
		return detailedAddress;
	}

	public void setDetailedAddress(String detailedAddress) {
		this.detailedAddress = detailedAddress;
	}

	public String getEmployeeNo() {
		return employeeNo;
	}

	public void setEmployeeNo(String employeeNo) {
		this.employeeNo = employeeNo;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	@Override
	public String toString() {
		return "MyInfoDTO [name=" + name + ", id=" + id + ", pwd=" + pwd + ", phone=" + phone + ", email=" + email
				+ ", birthday=" + birthday + ", question=" + question + ", answer=" + answer + ", zipCode=" + zipCode
				+ ", address=" + address + ", detailedAddress=" + detailedAddress + ", employeeNo=" + employeeNo
				+ ", position=" + position + ", deptName=" + deptName + "]";
	}

}
