package com.codefactory.mnm.mypage.model.service;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.codefactory.mnm.mypage.model.dao.MyPageDAO;
import com.codefactory.mnm.mypage.model.dto.ChangePasswordDTO;
import com.codefactory.mnm.mypage.model.dto.MyInfoDTO;

@Service
public class MyPageServiceImpl implements MyPageService{
	
	private MyPageDAO mypageDAO;
	
	@Autowired
	public MyPageServiceImpl(MyPageDAO mypageDAO) {
		this.mypageDAO = mypageDAO;
	}

	/* 내정보 조회 */
	@Override
	public MyInfoDTO MyPageInfo(String userId) {
		
		MyInfoDTO myInfo = mypageDAO.MyInfoById(userId);
		
		return myInfo;
	}

	/* 내정보 수정 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int updateMyinfo(String userId, MyInfoDTO myInfo) {
		
		return mypageDAO.updateMyInfo(userId, myInfo);
	}

	/* 비밀번호 변경 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int updatePassword(HttpServletResponse response, String userId, ChangePasswordDTO cPwd) throws IOException {
		
		response.setContentType("text/html; charset=UTF-8");
		
		PrintWriter out = response.getWriter();
		
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();	// 암호를 해시시키기 위해 객체 생성
		
		String userPwd = mypageDAO.readPwd(userId);			//DB에 저장된 비밀번호
		String currentPwd = cPwd.getPwd();					//입력한 현재 비밀번호
		String newPwd = cPwd.getNewPwd();					//입력한 새 비밀번호
		String newPwdCheck = cPwd.getNewPwdCheck();			//입력한 새 비밀번호 확인
		
		int result = 0;		// 비밀번호 변경 처리할 결과값
		
		// 평문 패스워드와 암호화 패스워드를 비교했을때, 같은 패스워드일 경우
		// 같으면 true, 다르면 false
		if(!encoder.matches(currentPwd, userPwd)) {
			
			if(currentPwd == "") {
				out.println("<script>");
				out.println("alert('현재 비밀번호를 입력해주세요.');");
				out.println("history.go(-1);");
				out.println("</script>");
				
				out.close();
			}
			
			out.println("<script>");
			out.println("alert('현재 비밀번호가 다릅니다. 다시 입력해주세요.');");
			out.println("history.go(-1);");
			out.println("</script>");
			
			out.close();
			
		} else if(newPwd == "" || newPwdCheck == "") {
			out.println("<script>");
			out.println("alert('새 비밀번호를 입력해주세요.');");
			out.println("history.go(-1);");
			out.println("</script>");
			
			out.close();
			
		} else if(!newPwd.equals(newPwdCheck)) {
			out.println("<script>");
			out.println("alert('새 비밀번호가 일치하지 않습니다. 다시 입력해주세요.');");
			out.println("history.go(-1);");
			out.println("</script>");
			
			out.close();
			
		} else {
			// 새 비밀번호 암호화 처리
			newPwd = encoder.encode(cPwd.getNewPwd());
			
			// update
			result = mypageDAO.updatePwd(userId, newPwd);

		}
		
		return result;
		
	}


}
