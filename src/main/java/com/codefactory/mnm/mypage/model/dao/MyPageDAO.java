package com.codefactory.mnm.mypage.model.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.codefactory.mnm.member.model.dto.UserDTO;
import com.codefactory.mnm.mypage.model.dto.ChangePasswordDTO;
import com.codefactory.mnm.mypage.model.dto.MyInfoDTO;

@Mapper
public interface MyPageDAO {

	MyInfoDTO MyInfoById(String userId); 									// 로그인한 사원의 아이디를 이용하여 내정보 조회

	int updateMyInfo(@Param("userId") String userId, MyInfoDTO myInfo);		// 내정보 수정

	String readPwd(String userId);											// 현재 사용자의 비밀번호 조회

	int updatePwd(String userId, String newPwd);							// 비밀번호 변경
	
}
