package com.codefactory.mnm.mypage.model.dto;

public class ChangePasswordDTO {
	
	private String pwd;			//현재비밀번호
	private String newPwd;		//새 비밀번호	
	private String newPwdCheck;	//새 비밀번호 확인
	
	public ChangePasswordDTO() {}

	public ChangePasswordDTO(String pwd, String newPwd, String newPwdCheck) {
		super();
		this.pwd = pwd;
		this.newPwd = newPwd;
		this.newPwdCheck = newPwdCheck;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getNewPwd() {
		return newPwd;
	}

	public void setNewPwd(String newPwd) {
		this.newPwd = newPwd;
	}

	public String getNewPwdCheck() {
		return newPwdCheck;
	}

	public void setNewPwdCheck(String newPwdCheck) {
		this.newPwdCheck = newPwdCheck;
	}

	@Override
	public String toString() {
		return "ChangePasswordDTO [pwd=" + pwd + ", newPwd=" + newPwd + ", newPwdCheck=" + newPwdCheck + "]";
	}
	
}
