package com.codefactory.mnm.mypage.model.service;

import java.io.IOException;
import java.security.Principal;

import javax.servlet.http.HttpServletResponse;

import com.codefactory.mnm.mypage.model.dto.ChangePasswordDTO;
import com.codefactory.mnm.mypage.model.dto.MyInfoDTO;

public interface MyPageService {

	public MyInfoDTO MyPageInfo(String userId);																			// 내정보 조회
	
	public int updateMyinfo(String userId, MyInfoDTO myInfo);															// 내정조 수정

	public int updatePassword(HttpServletResponse response, String userId, ChangePasswordDTO cPwd) throws IOException;	// 비밀번호 변경
	
}
