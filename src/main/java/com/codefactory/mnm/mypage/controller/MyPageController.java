package com.codefactory.mnm.mypage.controller;

import java.io.IOException;
import java.security.Principal;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.codefactory.mnm.mypage.model.dto.ChangePasswordDTO;
import com.codefactory.mnm.mypage.model.dto.MyInfoDTO;
import com.codefactory.mnm.mypage.model.service.MyPageService;

@Controller
@RequestMapping("mypage")
public class MyPageController {
	
	private MyPageService mypageService;
	
	@Autowired
	public MyPageController(MyPageService mypageService) {
		this.mypageService = mypageService;
	}

	/* 내정보 화면으로 이동해서 내정보 보여주기 */
	@GetMapping("myinfo")
	public ModelAndView myInfoPage(ModelAndView mv, Principal pcp) {
		
		String userId = pcp.getName();		// user01
		
		MyInfoDTO myInfo = mypageService.MyPageInfo(userId);
		
		mv.addObject("myInfo", myInfo);
		mv.setViewName("mypage/myinfo");
		
		return mv;
	}
	
	/* 내 정보 변경 */
	@PostMapping("myInfo/update")
	public ModelAndView updateMyInfo(ModelAndView mv, Principal pcp, MyInfoDTO myInfo, RedirectAttributes rttr) {
		
		String userId = pcp.getName();
		
		System.out.println(userId);
		System.out.println(myInfo);
		
		int result = mypageService.updateMyinfo(userId, myInfo);
		
		if(result > 0) {
			rttr.addFlashAttribute("successMessage", "내 정보가 정상적으로 저장되었습니다.");	
		} else {
			rttr.addFlashAttribute("failMessage", "내 정보가 저장되지 않았습니다. 다시 시도해주세요.");	
		}
		
		mv.setViewName("redirect:/mypage/myinfo");
		
		return mv;
	}
	
	/* 비밀번호 변경 페이지 */
	@GetMapping("myInfo/changePassword")
	public String changePasswordPage() {
		
        return "mypage/changePassword";
    }
	
	/* 비밀번호 변경 로직 */
	@PostMapping("myInfo/changePassword/update")
	public ModelAndView updatePassword(ModelAndView mv, Principal pcp, ChangePasswordDTO cPwd, RedirectAttributes rttr, HttpServletResponse response) throws IOException {
		
		String userId = pcp.getName();
		
		int result = mypageService.updatePassword(response, userId, cPwd);
		
		if(result > 0) {
			rttr.addFlashAttribute("successUpdatePasswordMessage", "비밀번호가 정상적으로 변경되었습니다.");
		} else {
			rttr.addFlashAttribute("failUpdatePasswordMessage", "비밀번호가 변경되지 않았습니다. 다시 시도해주세요.");
		}
		
		mv.setViewName("redirect:/mypage/myinfo");
		
		return mv;
	}
	
}
