package com.codefactory.mnm.member.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.codefactory.mnm.member.model.dto.RecoverIdDTO;
import com.codefactory.mnm.member.model.dto.UserDTO;
import com.codefactory.mnm.member.model.service.UserManagementService;

@Controller
@RequestMapping("/*")
public class UserManagementController {
	
	private UserManagementService userMamagementService;
	
	/* controller 의존성 추가 */
	@Autowired
	public UserManagementController(UserManagementService userMamagementService) {
		this.userMamagementService = userMamagementService;
	}
	
	/* 로그인 화면 이동 */
	@GetMapping
	public String memberLoginForm() {
		return "member/login";
	}
	
	/* 로그인 실패시, 화면 이동 */
	@GetMapping("fail")
	public String loginFail() {
		return "member/login-fail";
	}
	
	/* 아이디 찾기 화면 이동 */
	@GetMapping("recover-id")
	public String recoverId() {
		return "member/recover-id";
	}
	
	/* 아이디 찾기 결과 화면 이동 */
	@PostMapping("recoverIdResult")
	public ModelAndView recoverIdResult(ModelAndView mv, RecoverIdDTO userInfo) {
		
		String userId = userMamagementService.recoverUserById(userInfo);
		
		if(userId == null) {
			mv.addObject("failMessage", "사용자 정보를 잘못 입력하셨습니다. 다시 입력해주세요.");
		} else {
			mv.addObject("userId", userId);
		}
		
		mv.setViewName("member/login");
		
		return mv;
	}
	
	/* 비밀번호 찾기 */
	@GetMapping("recover-password")
	public String recoverPassword() throws Exception {
		return "member/recover-password";
	}
	
	/* 비밀번호 찾기 결과 */
	@PostMapping("recoverPasswordResult")
	public void recoverPasswordResult(ModelAndView mv, @ModelAttribute UserDTO userInfo, HttpServletResponse response) throws Exception {
		
		userMamagementService.recoverPasswordResult(response, userInfo);

	}
	
	/* 로그인 가이드 팝업창 */
	@GetMapping("loginGuidePopup")
	public String loginPopup() throws Exception {
		return "member/loginGuidePopup";
	}

}
