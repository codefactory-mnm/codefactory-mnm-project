package com.codefactory.mnm.member.model.dto;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

//import 주의
public class UserImpl extends User {

	private String userNo;							//사용자번호
	private String companyNo;						//회사번호
	private long employeeNo;						//사원번호
	private String id;								//아이디
	private String pwd;								//비밀번호
	private String name;							//이름
	private String phone;							//휴대폰번호
	private String landLineTel;						//유선전화번호
	private String email;							//이메일
	private java.sql.Date birthday;					//생년월일
	private String question;						//질문
	private String answer;							//답변
	private String zipCode;							//우편번호
	private String address;							//주소
	private String detailedAddress;					//상세주소
	private String deptNo;							//부서번호
	private java.sql.Date joinDate;					//입사일
	private java.sql.Date resignationDate;			//퇴사일
	private String withdrawYN;						//탈퇴여부
	private java.sql.Date enrollDate;				//등록일자
	private String part;							//역할
	private String job;								//직책
	private String position;						//직위
	
	private List<UserByAuthorityDTO> userByAuthorityList;	//회원별권한 리스트
	
	//생성자 반드시 생성해야 함
	public UserImpl(String username, String password, String userNo,
			Collection<? extends GrantedAuthority> authorities) {
		super(username, password, authorities);
		this.userNo = userNo;
		
	}
	
	public void setDetails(UserDTO user) {
		this.userNo = user.getUserNo();
		this.companyNo = user.getCompanyNo();
		this.employeeNo = user.getEmployeeNo();
		this.id = user.getId();
		this.pwd = user.getPwd();
		this.name = user.getName();
		this.phone = user.getPhone();
		this.landLineTel = user.getLandLineTel();
		this.email = user.getEmail();
		this.birthday = user.getBirthday();
		this.question = user.getQuestion();
		this.answer = user.getAnswer();
		this.zipCode = user.getZipCode();
		this.address = user.getAddress();
		this.detailedAddress = user.getDetailedAddress();
		this.deptNo = user.getDeptNo();
		this.joinDate = user.getJoinDate();
		this.resignationDate = user.getResignationDate();
		this.withdrawYN = user.getWithdrawYN();
		this.enrollDate = user.getEnrollDate();
		this.part = user.getPart();
		this.job = user.getJob();
		this.position = user.getPosition();
		this.detailedAddress = user.getDetailedAddress();
	}

	/* getter 들만 추가. 여기에서는 값을 꺼내 쓸 일만 있기 때문 */
	public String getUserNo() {
		return userNo;
	}

	public String getCompanyNo() {
		return companyNo;
	}

	public long getEmployeeNo() {
		return employeeNo;
	}

	public String getId() {
		return id;
	}

	public String getPwd() {
		return pwd;
	}

	public String getName() {
		return name;
	}

	public String getPhone() {
		return phone;
	}

	public String getLandLineTel() {
		return landLineTel;
	}

	public String getEmail() {
		return email;
	}

	public java.sql.Date getBirthday() {
		return birthday;
	}

	public String getQuestion() {
		return question;
	}

	public String getAnswer() {
		return answer;
	}

	public String getZipCode() {
		return zipCode;
	}

	public String getAddress() {
		return address;
	}

	public String getDetailedAddress() {
		return detailedAddress;
	}

	public String getDeptNo() {
		return deptNo;
	}

	public java.sql.Date getJoinDate() {
		return joinDate;
	}

	public java.sql.Date getResignationDate() {
		return resignationDate;
	}

	public String getWithdrawYN() {
		return withdrawYN;
	}

	public java.sql.Date getEnrollDate() {
		return enrollDate;
	}

	public String getPart() {
		return part;
	}

	public String getJob() {
		return job;
	}

	public String getPosition() {
		return position;
	}

	public List<UserByAuthorityDTO> getUserByAuthorityList() {
		return userByAuthorityList;
	}
	

}
