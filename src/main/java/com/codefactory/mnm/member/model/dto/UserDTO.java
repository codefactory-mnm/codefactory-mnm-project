package com.codefactory.mnm.member.model.dto;

import java.sql.Date;
import java.util.List;

public class UserDTO {
	
	private String userNo;							//사용자번호
	private String companyNo;						//회사번호
	private long employeeNo;						//사원번호
	private String id;								//아이디
	private String pwd;								//비밀번호
	private String name;							//이름
	private String phone;							//휴대폰번호
	private String landLineTel;						//유선전화번호
	private String email;							//이메일
	private java.sql.Date birthday;					//생년월일
	private String question;						//질문
	private String answer;							//답변
	private String zipCode;							//우편번호
	private String address;							//주소
	private String detailedAddress;					//상세주소
	private String deptNo;							//부서번호
	private java.sql.Date joinDate;					//입사일
	private java.sql.Date resignationDate;			//퇴사일
	private String withdrawYN;						//탈퇴여부
	private java.sql.Date enrollDate;				//등록일자
	private String part;							//역할
	private String job;								//직책
	private String position;						//직위
	
	private List<UserByAuthorityDTO> userByAuthorityList;	//회원별권한 리스트
	
	public UserDTO() {}

	public UserDTO(String userNo, String companyNo, long employeeNo, String id, String pwd, String name, String phone,
			String landLineTel, String email, Date birthday, String question, String answer, String zipCode,
			String address, String detailedAddress, String deptNo, Date joinDate, Date resignationDate,
			String withdrawYN, Date enrollDate, String part, String job, String position,
			List<UserByAuthorityDTO> userByAuthorityList) {
		super();
		this.userNo = userNo;
		this.companyNo = companyNo;
		this.employeeNo = employeeNo;
		this.id = id;
		this.pwd = pwd;
		this.name = name;
		this.phone = phone;
		this.landLineTel = landLineTel;
		this.email = email;
		this.birthday = birthday;
		this.question = question;
		this.answer = answer;
		this.zipCode = zipCode;
		this.address = address;
		this.detailedAddress = detailedAddress;
		this.deptNo = deptNo;
		this.joinDate = joinDate;
		this.resignationDate = resignationDate;
		this.withdrawYN = withdrawYN;
		this.enrollDate = enrollDate;
		this.part = part;
		this.job = job;
		this.position = position;
		this.userByAuthorityList = userByAuthorityList;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getCompanyNo() {
		return companyNo;
	}

	public void setCompanyNo(String companyNo) {
		this.companyNo = companyNo;
	}

	public long getEmployeeNo() {
		return employeeNo;
	}

	public void setEmployeeNo(long employeeNo) {
		this.employeeNo = employeeNo;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getLandLineTel() {
		return landLineTel;
	}

	public void setLandLineTel(String landLineTel) {
		this.landLineTel = landLineTel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public java.sql.Date getBirthday() {
		return birthday;
	}

	public void setBirthday(java.sql.Date birthday) {
		this.birthday = birthday;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDetailedAddress() {
		return detailedAddress;
	}

	public void setDetailedAddress(String detailedAddress) {
		this.detailedAddress = detailedAddress;
	}

	public String getDeptNo() {
		return deptNo;
	}

	public void setDeptNo(String deptNo) {
		this.deptNo = deptNo;
	}

	public java.sql.Date getJoinDate() {
		return joinDate;
	}

	public void setJoinDate(java.sql.Date joinDate) {
		this.joinDate = joinDate;
	}

	public java.sql.Date getResignationDate() {
		return resignationDate;
	}

	public void setResignationDate(java.sql.Date resignationDate) {
		this.resignationDate = resignationDate;
	}

	public String getWithdrawYN() {
		return withdrawYN;
	}

	public void setWithdrawYN(String withdrawYN) {
		this.withdrawYN = withdrawYN;
	}

	public java.sql.Date getEnrollDate() {
		return enrollDate;
	}

	public void setEnrollDate(java.sql.Date enrollDate) {
		this.enrollDate = enrollDate;
	}

	public String getPart() {
		return part;
	}

	public void setPart(String part) {
		this.part = part;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public List<UserByAuthorityDTO> getUserByAuthorityList() {
		return userByAuthorityList;
	}

	public void setUserByAuthorityList(List<UserByAuthorityDTO> userByAuthorityList) {
		this.userByAuthorityList = userByAuthorityList;
	}

	@Override
	public String toString() {
		return "UserDTO [userNo=" + userNo + ", companyNo=" + companyNo + ", employeeNo=" + employeeNo + ", id=" + id
				+ ", pwd=" + pwd + ", name=" + name + ", phone=" + phone + ", landLineTel=" + landLineTel + ", email="
				+ email + ", birthday=" + birthday + ", question=" + question + ", answer=" + answer + ", zipCode="
				+ zipCode + ", address=" + address + ", detailedAddress=" + detailedAddress + ", deptNo=" + deptNo
				+ ", joinDate=" + joinDate + ", resignationDate=" + resignationDate + ", withdrawYN=" + withdrawYN
				+ ", enrollDate=" + enrollDate + ", part=" + part + ", job=" + job + ", position=" + position
				+ ", userByAuthorityList=" + userByAuthorityList + "]";
	}

}
