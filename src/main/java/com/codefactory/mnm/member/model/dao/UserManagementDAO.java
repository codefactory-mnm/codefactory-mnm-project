package com.codefactory.mnm.member.model.dao;

import org.apache.ibatis.annotations.Mapper;

import com.codefactory.mnm.member.model.dto.RecoverIdDTO;
import com.codefactory.mnm.member.model.dto.UserDTO;

@Mapper
public interface UserManagementDAO {
	
	UserDTO findUserById(String userId);
	
	String recoverUserById(RecoverIdDTO userInfo);

	UserDTO readUser(String userId); // 사원의 아이디를 이용하여 정보 추출

	Object idCheck(String userId);	// 사원 아이디 있는지 체크

	void updatePassword(UserDTO userInfo);	// 비밀번호 변경 처리

}
