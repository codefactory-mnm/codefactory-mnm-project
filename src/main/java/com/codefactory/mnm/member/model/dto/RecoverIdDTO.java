package com.codefactory.mnm.member.model.dto;

public class RecoverIdDTO {
	
	private String name;
	private String phone;
	private String question;
	private String answer;
	
	public RecoverIdDTO() {}

	public RecoverIdDTO(String name, String phone, String question, String answer) {
		super();
		this.name = name;
		this.phone = phone;
		this.question = question;
		this.answer = answer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	@Override
	public String toString() {
		return "RecoverIdDTO [name=" + name + ", phone=" + phone + ", question=" + question + ", answer=" + answer
				+ "]";
	}

}
