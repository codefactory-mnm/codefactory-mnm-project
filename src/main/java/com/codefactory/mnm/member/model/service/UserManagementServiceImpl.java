package com.codefactory.mnm.member.model.service;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.codefactory.mnm.member.model.dao.UserManagementDAO;
import com.codefactory.mnm.member.model.dto.RecoverIdDTO;
import com.codefactory.mnm.member.model.dto.UserByAuthorityDTO;
import com.codefactory.mnm.member.model.dto.UserDTO;
import com.codefactory.mnm.member.model.dto.UserImpl;
import com.codefactory.mnm.userinfo.model.dto.AuthorityDTO;

@Service
public class UserManagementServiceImpl implements UserManagementService {

	private UserManagementDAO userManagementDAO;
	
	@Autowired
	public UserManagementServiceImpl(UserManagementDAO userManagementDAO) {
		this.userManagementDAO = userManagementDAO;
	}
	
	@Autowired
	private JavaMailSender javaMailSender;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		UserDTO user = userManagementDAO.findUserById(username);
		
		/* 만일 맞는 사용자가 없는 경우, 빈 DTO를 하나 만들어준다 */
		if(user == null) {
			user = new UserDTO();
		}
		
		/* 권한 부여 */
		List<GrantedAuthority> authorities = new ArrayList<>();
		if(user.getUserByAuthorityList() != null) {
			List<UserByAuthorityDTO> roleList = user.getUserByAuthorityList();
			
			for(int i = 0; i < roleList.size(); i++) {
				AuthorityDTO authority = roleList.get(i).getAuthority();
				
				if(authority != null) {
					authorities.add(new SimpleGrantedAuthority(authority.getAuthorityName()));
				}
			}
		}
		
		/* UserImp은 User객체를 상속받은 객체. 담고싶은 정보를 담는다. */
		UserImpl userInfo = new UserImpl(user.getId(), user.getPwd(), user.getUserNo() ,authorities);
		userInfo.setDetails(user);
		
		return userInfo;
	}
	
	
	@Override
	public String recoverUserById(RecoverIdDTO userInfo) {
		
		String userId = userManagementDAO.recoverUserById(userInfo);
		
		return userId;
	}

	// 비밀번호 찾기
	@Override
	public void recoverPasswordResult(HttpServletResponse response, UserDTO userInfo) throws Exception {
		
		response.setContentType("text/html; charset=UTF-8");
		
		// 사원의 아이디를 이용하여 사용자 정보 추출
		UserDTO user = userManagementDAO.readUser(userInfo.getId());
		
		// 사원의 아이디를 이용하여 DB에 사용자 ID가 유효한지 체크
		Object userIdChk = userManagementDAO.idCheck(userInfo.getId());
		
		PrintWriter out = response.getWriter();
		
		// 사원 아이디가 존재하지 않을 경우
		if(userIdChk == null) {
			out.println("<script>");
			out.println("alert('등록되지 않은 아이디 입니다.');");
			out.println("history.go(-1);");
			out.println("</script>");
			
			out.close();
		
		}
		
		// 사원의 이메일이 틀린 경우
		else if(!userInfo.getEmail().equals(user.getEmail())) {
			out.println("<script>");
			out.println("alert('등록되지 않은 이메일 입니다.');");
			out.println("history.go(-1);");
			out.println("</script>");
			out.close();
		
			
		} else { //사원의 아이디, 이메일이 정상적으로 있는 경우
			
			// 비밀번호 변경 메일 발송
			sendEmail(userInfo, "recoverPassword");
			
			out.println("<script>");
			out.println("alert('이메일로 임시 비밀번호를 발송하였습니다.');");
			out.println("history.go(-1);");
			out.println("</script>");
			out.close();
			
		}
		
	}

	// 비밀번호 찾기 이메일 발송
	@Override
	public void sendEmail(UserDTO userInfo, String div) {
		
		// 사용자 아이디
		String userId = userInfo.getId();
		
		// 이메일 제목, 내용
		String subject = "";
		String msg = "";
		
		// 임시 비밀번호 생성
		StringBuffer tempPwd = new StringBuffer();
		Random rnd = new Random();

		for(int i = 0; i < 10; i++) {
		
		   int rIndex = rnd.nextInt(3);
		     
		   switch(rIndex) {
		   case 0: tempPwd.append((char) ((int) (rnd.nextInt(26)) + 97)); break;
		   case 1: tempPwd.append((char) ((int) (rnd.nextInt(26)) + 65)); break;
		   case 2: tempPwd.append((rnd.nextInt(10))); break;
		   }
		
		}
		
		if(div.equals("recoverPassword")) {
			subject = "M&M 임시 비밀번호 입니다.";
			msg += userId + " 님의 임시 비밀번호 입니다. 로그인 이후 비밀번호를 변경하여 사용하세요. \n";
			msg += "임시 비밀번호 : ";
			msg += tempPwd;
			
		}
		
		// 임시 비밀번호 암호화 처리
		String tempPwdEncry= new BCryptPasswordEncoder().encode(tempPwd);
		
		// 암호화된 임시 비밀번호 set
		userInfo.setPwd(tempPwdEncry);	
		
		// 비밀번호 변경 처리
		userManagementDAO.updatePassword(userInfo);
		
		// 받는 사람 E-Mail 주소 지정하기
		String mail = userInfo.getEmail();
		
		try {
			
			SimpleMailMessage email = new SimpleMailMessage();
			
			email.setTo(mail);					//받는사람
			email.setSubject(subject);			//제목
			email.setText(msg);					//메시지 내용
			
			javaMailSender.send(email);
			
		} catch (Exception e) {
			System.out.println("메일발송 실패 : " + e);
			
		}
	}
}
