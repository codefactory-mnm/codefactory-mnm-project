package com.codefactory.mnm.member.model.dto;

public class CommonMenuDTO {
	
	private String menuNo;			//메뉴번호
	private String menuName;		//메뉴명
	private String menuURL;			//메뉴URL
	private int outputOrder;		//출력순서
	private String delYN;			//삭제여부
	private String division;		//구분
	private String refMenuNo;		//상위메뉴번호
	
	public CommonMenuDTO() {}
	
	public CommonMenuDTO(String menuNo, String menuName, String menuURL, int outputOrder, String delYN, String division,
			String refMenuNo) {
		super();
		this.menuNo = menuNo;
		this.menuName = menuName;
		this.menuURL = menuURL;
		this.outputOrder = outputOrder;
		this.delYN = delYN;
		this.division = division;
		this.refMenuNo = refMenuNo;
	}

	public String getMenuNo() {
		return menuNo;
	}

	public void setMenuNo(String menuNo) {
		this.menuNo = menuNo;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getMenuURL() {
		return menuURL;
	}

	public void setMenuURL(String menuURL) {
		this.menuURL = menuURL;
	}

	public int getOutputOrder() {
		return outputOrder;
	}

	public void setOutputOrder(int outputOrder) {
		this.outputOrder = outputOrder;
	}

	public String getDelYN() {
		return delYN;
	}

	public void setDelYN(String delYN) {
		this.delYN = delYN;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getRefMenuNo() {
		return refMenuNo;
	}

	public void setRefMenuNo(String refMenuNo) {
		this.refMenuNo = refMenuNo;
	}

	@Override
	public String toString() {
		return "CommonMenuDTO [menuNo=" + menuNo + ", menuName=" + menuName + ", menuURL=" + menuURL + ", outputOrder="
				+ outputOrder + ", delYN=" + delYN + ", division=" + division + ", refMenuNo=" + refMenuNo + "]";
	}

}
