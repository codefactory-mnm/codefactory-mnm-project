package com.codefactory.mnm.member.model.dto;

import com.codefactory.mnm.userinfo.model.dto.AuthorityDTO;

public class UserByAuthorityDTO {
	
	private String userNo;				//사용자번호
	private String authorityNo;			//권한번호
	
	private AuthorityDTO authority;		//권한 정보
	
	public UserByAuthorityDTO() {}

	public UserByAuthorityDTO(String userNo, String authorityNo, AuthorityDTO authority) {
		super();
		this.userNo = userNo;
		this.authorityNo = authorityNo;
		this.authority = authority;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getAuthorityNo() {
		return authorityNo;
	}

	public void setAuthorityNo(String authorityNo) {
		this.authorityNo = authorityNo;
	}

	public AuthorityDTO getAuthority() {
		return authority;
	}

	public void setAuthority(AuthorityDTO authority) {
		this.authority = authority;
	}

	@Override
	public String toString() {
		return "UserByAuthorityDTO [userNo=" + userNo + ", authorityNo=" + authorityNo + ", authority=" + authority
				+ "]";
	}

}
