package com.codefactory.mnm.member.model.dto;

public class AccessibleMenuByAuthorityDTO {
	
	private String authorityNo;			//권한번호
	private String menuNo;				//메뉴번호
	
	private CommonMenuDTO commonMenu;	//메뉴 정보
	
	public AccessibleMenuByAuthorityDTO() {};

	public AccessibleMenuByAuthorityDTO(String authorityNo, String menuNo, CommonMenuDTO commonMenu) {
		super();
		this.authorityNo = authorityNo;
		this.menuNo = menuNo;
		this.commonMenu = commonMenu;
	}

	public String getAuthorityNo() {
		return authorityNo;
	}

	public void setAuthorityNo(String authorityNo) {
		this.authorityNo = authorityNo;
	}

	public String getMenuNo() {
		return menuNo;
	}

	public void setMenuNo(String menuNo) {
		this.menuNo = menuNo;
	}

	public CommonMenuDTO getCommonMenu() {
		return commonMenu;
	}

	public void setCommonMenu(CommonMenuDTO commonMenu) {
		this.commonMenu = commonMenu;
	}

	@Override
	public String toString() {
		return "AccessibleMenuByAuthorityDTO [authorityNo=" + authorityNo + ", menuNo=" + menuNo + ", commonMenu="
				+ commonMenu + "]";
	}
	
}
