package com.codefactory.mnm.member.model.service;

import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.codefactory.mnm.member.model.dto.RecoverIdDTO;
import com.codefactory.mnm.member.model.dto.UserDTO;

public interface UserManagementService extends UserDetailsService {

	// 아이디 찾기
	String recoverUserById(RecoverIdDTO userInfo);

	// 비밀번호 찾기
	public void recoverPasswordResult(HttpServletResponse response, UserDTO userInfo) throws Exception;

	// 이메일 발송
	public void sendEmail(UserDTO user, String div);

}
