package com.codefactory.mnm.menuGroup.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.menuGroup.model.dto.MenuGroupDTO;
import com.codefactory.mnm.menuGroup.model.dto.MenuGroupSearchDTO;
import com.codefactory.mnm.menuGroup.model.service.MenuGroupService;
import com.codefactory.mnm.userinfo.model.dto.AuthorityDTO;

@Controller
@RequestMapping("menuGroup")
public class MenuGroupController {
	
	private MenuGroupService menuGroupService;
	
	
	@Autowired
	public MenuGroupController(MenuGroupService menuGroupService) {
		this.menuGroupService = menuGroupService; 
	}
	
	/* 권한그룹 조회 */
	@GetMapping
	public ModelAndView menuGroupPage(ModelAndView mv) {
		
		List<AuthorityDTO> authorityGroup = menuGroupService.selectAuthorityGroup(); 
		
		mv.addObject("authorityGroup", authorityGroup);
		mv.setViewName("menuGroup/menuGroup");
		
		return mv;
	}
	
	/* 메뉴그룹 조회 */
	@GetMapping(value = "authorityAndMenuChk", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public Map<String, Object> selectMenuGroup(@RequestParam("authority") String authority, 
			@RequestParam("currentPage") String currentPage) {
		
		MenuGroupSearchDTO menuGroupSearch = new MenuGroupSearchDTO();
		
		menuGroupSearch.setCurrentPage(currentPage);
		
		Map<String, Object> map1 = menuGroupService.selectMenuGroup(authority, menuGroupSearch);
		Map<String, Object> map2 = new HashMap<String, Object>();
		
		List<MenuGroupDTO> menuGroupList = (List<MenuGroupDTO>) map1.get("menuGroupList");
		SelectCriteria selectCriteria = (SelectCriteria) map1.get("selectCriteria");
		int totalCount = (int) map1.get("totalCount");
		
		map2.put("menuGroupList", menuGroupList);
		map2.put("selectCriteria", selectCriteria);
		map2.put("menuGroupSearch", menuGroupSearch);
		map2.put("totalCount", totalCount);
		
		return map2;
	}
	
	/* 메뉴그룹 상세보기 조회 */
	@GetMapping(value = "detailCheck", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public MenuGroupDTO selectMenuDetail(@RequestParam("menuNo") String menuNo) {
		
		MenuGroupDTO menuDetail = menuGroupService.selectMenuDetail(menuNo);
		
		return menuDetail;
	}
	
	/* 메뉴그룹 추가 */
	@PostMapping(value = "insertMenuGroup", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int insertMenuGroup(MenuGroupDTO menu) {
		
		int result = menuGroupService.insertMenuGroup(menu);
		
		return result;
	}
	
	/* 권한그룹 추가 */
	@PostMapping(value = "insertAuthorityGroup", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int insertAuthorityGroup(@RequestParam("authorityName") String authority) {
		
		int result = menuGroupService.insertAuthorityGroup(authority);
		
		return result;
	}

	/* 메뉴그룹 삭제 처리 */
	@PostMapping(value = "delete", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int withdrawMenuGroup(@RequestParam(value = "checkArray[]") List<String> menuNoList) {
		
		int result = menuGroupService.withdrawMenuGroup(menuNoList);
		
		return result;
		
	}
	
	/* 메뉴그룹 수정 */
	@PostMapping(value = "update", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int updateMenuGroup(MenuGroupDTO menu) {
		
		int result = menuGroupService.updateMenuGroup(menu);
	
		return result;
	}
	
}
