package com.codefactory.mnm.menuGroup.model.service;

import java.util.List;
import java.util.Map;

import com.codefactory.mnm.menuGroup.model.dto.MenuGroupDTO;
import com.codefactory.mnm.menuGroup.model.dto.MenuGroupSearchDTO;
import com.codefactory.mnm.userinfo.model.dto.AuthorityDTO;

public interface MenuGroupService {
	
	public Map<String, Object> selectMenuGroup(String authority, MenuGroupSearchDTO menuGroupSearch);	// 메뉴그룹 리스트 조회

	public int insertMenuGroup(MenuGroupDTO menu);														// 메뉴그룹 추가
	
	public List<AuthorityDTO> selectAuthorityGroup();													// 권한그룹 조회 
	
	public int insertAuthorityGroup(String authority);													// 권한그룹 추가

	public int withdrawMenuGroup(List<String> menuNoList);												// 메뉴그룹 삭제처리
	
	public int updateMenuGroup(MenuGroupDTO menu);														// 메뉴그룹 수정

	public MenuGroupDTO selectMenuDetail(String menuNo);												// 메뉴그룹 상세조회

}
