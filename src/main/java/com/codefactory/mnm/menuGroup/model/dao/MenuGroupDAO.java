package com.codefactory.mnm.menuGroup.model.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.codefactory.mnm.menuGroup.model.dto.MenuGroupDTO;
import com.codefactory.mnm.menuGroup.model.dto.MenuGroupSearchDTO;
import com.codefactory.mnm.userinfo.model.dto.AuthorityDTO;

@Mapper
public interface MenuGroupDAO {

	List<MenuGroupDTO> selectMenuGroup(String authority, MenuGroupSearchDTO menuGroupSearch);		// 메뉴그룹 리스트 조회
	
	List<AuthorityDTO> selectAuthorityGroup();														// 권한그룹 리스트 조회
	
	MenuGroupDTO selectMenuDetail(String menuNo);													// 메뉴그룹 상세보기 조회

	int insertMenuGroup(MenuGroupDTO menu);															// 메뉴그룹 추가
	
	int insertAuthorityGroup(String authority);														// 권한그룹 추가

	int withdrawMenuGroup(List<String> menuNoList);													// 메뉴그룹 삭제 처리

	int selectTotalCount(String authority);															// 권한에 따른 메뉴그룹 수 조회

	int updateMenuGroup(MenuGroupDTO menu);															// 메뉴그룹 수정

}
