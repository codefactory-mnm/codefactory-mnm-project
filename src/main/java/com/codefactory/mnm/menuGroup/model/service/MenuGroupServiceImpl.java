package com.codefactory.mnm.menuGroup.model.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.codefactory.mnm.common.Pagenation;
import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.menuGroup.model.dao.MenuGroupDAO;
import com.codefactory.mnm.menuGroup.model.dto.MenuGroupDTO;
import com.codefactory.mnm.menuGroup.model.dto.MenuGroupSearchDTO;
import com.codefactory.mnm.userinfo.model.dto.AuthorityDTO;

@Service
public class MenuGroupServiceImpl implements MenuGroupService {
	
	private MenuGroupDAO mapper;
	
	@Autowired
	public MenuGroupServiceImpl(MenuGroupDAO menuGroupDAO) {
		this.mapper = menuGroupDAO;
	}
	
	/* 메뉴그룹 리스트 조회 */
	@Override
	public Map<String, Object> selectMenuGroup(String authority, MenuGroupSearchDTO menuGroupSearch) {
		
		/* 첫 요청페이지 */
		int pageNo = 1;
		
		/* 요청 페이지가 0이하면 1로 바꿔줌 */
		if(pageNo <= 0) {
			pageNo = 1;
		}
		
		int limit = 5;
		
		int buttonAmount = 5;
		
		/* 전체 게시물 수, 검색조건이 있다면 검색조건에 맞는 게시물 수 */
		int totalCount = mapper.selectTotalCount(authority); 
		
		/* view에서 전달받은 요청페이지*/
		String currentPage = menuGroupSearch.getCurrentPage();
		
		/* view에서 전달받은 요청페이지를 pageNo에 저장 */
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		/* 설정한 변수에 따른 페이징조건 */
		SelectCriteria selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount);
		
		Map<String, Object> map = new HashMap<>();
		
		menuGroupSearch.setSelectCriteria(selectCriteria);
		
		List<MenuGroupDTO> menuGroupList = mapper.selectMenuGroup(authority, menuGroupSearch);
		
		for(MenuGroupDTO menu : menuGroupList) {
			if(menu.getRefMenuName() == null) {
				menu.setRefMenuName("없음");
			}
		}
		
		map.put("menuGroupList", menuGroupList);
		map.put("selectCriteria", selectCriteria);
		map.put("totalCount", totalCount);
		
		return map;
	}
	
	/* 권한그룹 조회 */
	@Override
	public List<AuthorityDTO> selectAuthorityGroup() {
		
		return mapper.selectAuthorityGroup();
	}
	
	/* 메뉴그룹 상세조회 */
	@Override
	public MenuGroupDTO selectMenuDetail(String menuNo) {
		
		return mapper.selectMenuDetail(menuNo);
	}

	/* 메뉴그룹 추가 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertMenuGroup(MenuGroupDTO menu) {
		
		return mapper.insertMenuGroup(menu);
	}
	
	/* 권한그룹 추가 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertAuthorityGroup(String authority) {
		
		return mapper.insertAuthorityGroup(authority);
	}

	/* 메뉴그룹 삭제처리 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int withdrawMenuGroup(List<String> menuNoList) {
		
		return mapper.withdrawMenuGroup(menuNoList);
	}

	/* 메뉴그룹 수정 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int updateMenuGroup(MenuGroupDTO menu) {
		
		int result= mapper.updateMenuGroup(menu);
		
		return result;
	}

}
