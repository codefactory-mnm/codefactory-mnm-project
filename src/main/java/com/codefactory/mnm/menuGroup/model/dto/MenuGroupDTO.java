package com.codefactory.mnm.menuGroup.model.dto;

public class MenuGroupDTO {
	
	private String menuNo;
	private String menuName;
	private String menuURL;
	private String outputOrder;
	private String delYN;
	private String division;
	private String refMenuName;
	
	public MenuGroupDTO() {}

	public MenuGroupDTO(String menuNo, String menuName, String menuURL, String outputOrder, String delYN,
			String division, String refMenuName) {
		super();
		this.menuNo = menuNo;
		this.menuName = menuName;
		this.menuURL = menuURL;
		this.outputOrder = outputOrder;
		this.delYN = delYN;
		this.division = division;
		this.refMenuName = refMenuName;
	}

	public String getMenuNo() {
		return menuNo;
	}

	public void setMenuNo(String menuNo) {
		this.menuNo = menuNo;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getMenuURL() {
		return menuURL;
	}

	public void setMenuURL(String menuURL) {
		this.menuURL = menuURL;
	}

	public String getOutputOrder() {
		return outputOrder;
	}

	public void setOutputOrder(String outputOrder) {
		this.outputOrder = outputOrder;
	}

	public String getDelYN() {
		return delYN;
	}

	public void setDelYN(String delYN) {
		this.delYN = delYN;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getRefMenuName() {
		return refMenuName;
	}

	public void setRefMenuName(String refMenuName) {
		this.refMenuName = refMenuName;
	}

	@Override
	public String toString() {
		return "MenuGroupDTO [menuNo=" + menuNo + ", menuName=" + menuName + ", menuURL=" + menuURL + ", outputOrder="
				+ outputOrder + ", delYN=" + delYN + ", division=" + division + ", refMenuName=" + refMenuName + "]";
	}

}
