package com.codefactory.mnm.clientsupport.model.dto;

public class ClientSupportProductDTO implements java.io.Serializable {

	private String clientSupportNo;			//고객지원번호
	private String productNo;				//상품번호
	private String productNote;				//비고
	private String unit;					//포장수량
	private String standard;				//규격
	private String productName;				//상품명
	
	public ClientSupportProductDTO() {}

	public ClientSupportProductDTO(String clientSupportNo, String productNo, String productNote, String unit,
			String standard, String productName) {
		super();
		this.clientSupportNo = clientSupportNo;
		this.productNo = productNo;
		this.productNote = productNote;
		this.unit = unit;
		this.standard = standard;
		this.productName = productName;
	}

	public String getClientSupportNo() {
		return clientSupportNo;
	}

	public String getProductNo() {
		return productNo;
	}

	public String getProductNote() {
		return productNote;
	}

	public String getUnit() {
		return unit;
	}

	public String getStandard() {
		return standard;
	}

	public String getProductName() {
		return productName;
	}

	public void setClientSupportNo(String clientSupportNo) {
		this.clientSupportNo = clientSupportNo;
	}

	public void setProductNo(String productNo) {
		this.productNo = productNo;
	}

	public void setProductNote(String productNote) {
		this.productNote = productNote;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Override
	public String toString() {
		return "ClientSupportProductDTO [clientSupportNo=" + clientSupportNo + ", productNo=" + productNo
				+ ", productNote=" + productNote + ", unit=" + unit + ", standard=" + standard + ", productName="
				+ productName + "]";
	}

	
}
