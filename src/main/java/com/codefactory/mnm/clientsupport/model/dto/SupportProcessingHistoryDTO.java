package com.codefactory.mnm.clientsupport.model.dto;

import java.sql.Date;

public class SupportProcessingHistoryDTO {

	private String processingHistoryNo;			//처리내역번호
	private String processingDivision2;			//처리구분
	private String processingContent;			//처리내용
	private java.sql.Date processingDate;		//처리날짜
	private String userNo;						//담당자번호
	private String clientSupportNo;				//고객지원번호
	private String name;						//담당자명
	
	public SupportProcessingHistoryDTO() {}

	public SupportProcessingHistoryDTO(String processingHistoryNo, String processingDivision2, String processingContent,
			Date processingDate, String userNo, String clientSupportNo, String name) {
		super();
		this.processingHistoryNo = processingHistoryNo;
		this.processingDivision2 = processingDivision2;
		this.processingContent = processingContent;
		this.processingDate = processingDate;
		this.userNo = userNo;
		this.clientSupportNo = clientSupportNo;
		this.name = name;
	}

	public String getProcessingHistoryNo() {
		return processingHistoryNo;
	}

	public String getProcessingDivision2() {
		return processingDivision2;
	}

	public String getProcessingContent() {
		return processingContent;
	}

	public java.sql.Date getProcessingDate() {
		return processingDate;
	}

	public String getUserNo() {
		return userNo;
	}

	public String getClientSupportNo() {
		return clientSupportNo;
	}

	public String getName() {
		return name;
	}

	public void setProcessingHistoryNo(String processingHistoryNo) {
		this.processingHistoryNo = processingHistoryNo;
	}

	public void setProcessingDivision2(String processingDivision2) {
		this.processingDivision2 = processingDivision2;
	}

	public void setProcessingContent(String processingContent) {
		this.processingContent = processingContent;
	}

	public void setProcessingDate(java.sql.Date processingDate) {
		this.processingDate = processingDate;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public void setClientSupportNo(String clientSupportNo) {
		this.clientSupportNo = clientSupportNo;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "SupportProcessingHistoryDTO [processingHistoryNo=" + processingHistoryNo + ", processingDivision2="
				+ processingDivision2 + ", processingContent=" + processingContent + ", processingDate="
				+ processingDate + ", userNo=" + userNo + ", clientSupportNo=" + clientSupportNo + ", name=" + name
				+ "]";
	}

	
}
