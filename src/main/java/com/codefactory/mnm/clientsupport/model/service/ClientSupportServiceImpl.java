package com.codefactory.mnm.clientsupport.model.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.codefactory.mnm.clientsupport.model.dao.ClientSupportDAO;
import com.codefactory.mnm.clientsupport.model.dto.ClientSupportAttachmentDTO;
import com.codefactory.mnm.clientsupport.model.dto.ClientSupportDTO;
import com.codefactory.mnm.clientsupport.model.dto.ClientSupportInsertDTO;
import com.codefactory.mnm.clientsupport.model.dto.ClientSupportProductDTO;
import com.codefactory.mnm.clientsupport.model.dto.ClientSupportSearchDTO;
import com.codefactory.mnm.clientsupport.model.dto.SupportProcessingHistoryDTO;
import com.codefactory.mnm.common.Pagenation;
import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.contract.model.dto.ContractDTO;
import com.codefactory.mnm.contract.model.dto.ContractProductDTO;
import com.codefactory.mnm.estimate.model.dto.DeptListDTO;
import com.codefactory.mnm.sales.model.dto.SalesAttachmentDTO;
import com.codefactory.mnm.sales.model.dto.SalesDTO;
import com.codefactory.mnm.sales.model.dto.SalesProductDTO;

@Service
public class ClientSupportServiceImpl implements ClientSupportService {

	private final ClientSupportDAO clientSupportDAO;
	
	@Autowired
	public ClientSupportServiceImpl(ClientSupportDAO clientSupportDAO) {
		
		this.clientSupportDAO = clientSupportDAO;
	}
	
	/* 고객지원 등록 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertClientSupport(ClientSupportInsertDTO clientSupportInsertDTO) {
		
		/* controller에서 받아온 정보를 꺼낸다. */
		ClientSupportDTO clientSupportDTO = clientSupportInsertDTO.getClientSupportDTO();
		List<ClientSupportAttachmentDTO> files = clientSupportInsertDTO.getFiles();
		List<ClientSupportProductDTO> clientSupportProductList = clientSupportInsertDTO.getClientSupportProductList();
		SupportProcessingHistoryDTO supportProcessingHistoryDTO = clientSupportInsertDTO.getSupportProcessingHistoryDTO();
		
		/* 고객지원 작성내용을 DB에 저장한다. */
		int result1 = clientSupportDAO.insertClientSupport(clientSupportDTO);
		
		int result2 = 0;
		/* 고객지원 품목을 등록한다 */
		for(int i = 0; i < clientSupportProductList.size(); i++) {
			
			ClientSupportProductDTO salesProduct = clientSupportProductList.get(i);
			result2 = clientSupportDAO.insertClientSupportProduct(salesProduct);
		}
		
		/* 파일첨부정보를 DB에 저장한 횟수를 저장한다. */
		int result3 = 0;	
		if(files != null) {
			for(ClientSupportAttachmentDTO clientSupportAttachmentDTO : files) {
				result3 += clientSupportDAO.insertClientSupportAttachment(clientSupportAttachmentDTO);
			}	
		}
		
		/* 고객지원 처리내역 등록 */
		int result4 = clientSupportDAO.insertSupportProcessingHistory(supportProcessingHistoryDTO);
		
		/* 파일첨부가 없는 경우 : 고객지원, 품목을 정상적으로 등록하였을 경우 result = 1을 반환
		 * 파일첨부가 있는 경우 : 고객지원, 품목, 첨부파일을 정상적으로 등록하였을 경우 result = 1을 반환
		 */
		int result = 0;
		if(result1 == 1 && result2 == 1 && result4 == 1 && (files == null || result3 == files.size())) {
			result = 1;
		}
		
		return result;
	}

	/* 고객지원 조회 */
	@Override
	public Map<String, Object> selectClientSupportList(ClientSupportSearchDTO clientSupportSearchDTO) {

		/* 첫 요청페이지 */
		int pageNo = 1;		
		
		/* 요청 페이지가 0이하면 1로 바꿔줌 */
		if(pageNo <= 0) {
			pageNo = 1;
		}
		
		/* 최대 페이지 */
		int limit = 10;
		
		/* 최대 버튼 갯수 */
		int buttonAmount = 5;
		
		/* 전체 게시물 수, 검색조건이 있다면 검색조건에 맞는 게시물 수 */
		int totalCount = clientSupportDAO.selectTotalCount(clientSupportSearchDTO);

		/* view에서 전달받은 요청페이지 */
		String currentPage = clientSupportSearchDTO.getCurrentPage();
		
		/* view에서 전달받은 요청페이지를 pageNo에 저장 */
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		/* 설정한 변수에 따른 페이징조건 */
		SelectCriteria selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount);
				
		/* 조회 결과를 담을 Map 생성 */
		Map<String, Object> map = new HashMap<>();
		
		clientSupportSearchDTO.setSelectCriteria(selectCriteria);
		
		/* 고객지원 조회 */
		List<ClientSupportDTO> clientSupportList = clientSupportDAO.selectClientSupportList(clientSupportSearchDTO);
		
		/* 부서 조회 */
		List<DeptListDTO> deptList = clientSupportDAO.selectDeptList();
		
		map.put("totalCount", totalCount);
		map.put("selectCriteria", selectCriteria);
		map.put("clientSupportList", clientSupportList);
		map.put("deptList", deptList);
		
		return map;
	}

	/* 고객지원 상세 조회 */
	@Override
	public Map<String, Object> selectClientSupport(String clientSupportNo) {

		Map<String, Object> map = new HashMap<>();
		
		ClientSupportDTO oneClientSupport = clientSupportDAO.selectClientSupport(clientSupportNo);
		List<ClientSupportAttachmentDTO> fileList = clientSupportDAO.selectFileList(clientSupportNo);
		List<ClientSupportProductDTO> productList = clientSupportDAO.selectClientSupportProductList(clientSupportNo);
		List<SupportProcessingHistoryDTO> supportProcessingHistoryList = clientSupportDAO.selectSupportProcessingHistory(clientSupportNo);
				
		map.put("oneClientSupport", oneClientSupport);
		map.put("fileList", fileList);
		map.put("productList", productList);
		map.put("supportProcessingHistoryList", supportProcessingHistoryList);
		
		return map;
	}

	/* 고객지원 삭제 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int deleteClientSupport(String clientSupportNo) {

		return clientSupportDAO.deleteClientSupport(clientSupportNo);
	}

	/* 첨부파일 다운로드 */
	@Override
	public ClientSupportAttachmentDTO selectFile(String clientSupportFileNo) {

		return clientSupportDAO.selectFile(clientSupportFileNo);
	}

	/* 첨부파일 삭제 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public Map<String, Object> deleteFile(String clientSupportFileNo) {

		Map<String, Object> map = new HashMap<>();
		
		ClientSupportAttachmentDTO file = clientSupportDAO.selectFile(clientSupportFileNo);		//첨부파일정보				
		int result = clientSupportDAO.deleteFile(clientSupportFileNo);							//첨부파일 DB에서 삭제 결과
		
		/* map에 담아서 controller에 전달 */
		map.put("file", file);
		map.put("result", result);
		
		return map;
	}

	/* 고객지원 수정 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int updateClientSupport(ClientSupportInsertDTO clientSupportInsertDTO) {

		/* controller에서 받아온 정보를 꺼낸다. */
		ClientSupportDTO clientSupportDTO = clientSupportInsertDTO.getClientSupportDTO();
		List<ClientSupportAttachmentDTO> files = clientSupportInsertDTO.getFiles();
		List<ClientSupportProductDTO> clientSupportProductLists = clientSupportInsertDTO.getClientSupportProductList();
		SupportProcessingHistoryDTO supportProcessingHistoryDTO = clientSupportInsertDTO.getSupportProcessingHistoryDTO();
		
		/* 고객지원 품목을 삭제한다 */
		int result1 = clientSupportDAO.deleteClientSupportProduct(clientSupportDTO);
		
		/* 고객지원 작성내용을 DB에 저장한다. */
		int result2 = clientSupportDAO.updateClientSupport(clientSupportDTO);
		
		String clientSupportNo = clientSupportDTO.getClientSupportNo();
		
		int result3 = 0;
		/* 고객지원 품목을 등록한다 */
		for(int i = 0; i < clientSupportProductLists.size(); i++) {
			
			ClientSupportProductDTO clientSupportProductDTO = clientSupportProductLists.get(i);
			clientSupportProductDTO.setClientSupportNo(clientSupportNo);
			result3 = clientSupportDAO.updateClientSupportProduct(clientSupportProductDTO);
		}
		
		/* 파일첨부정보를 DB에 저장한 횟수를 저장한다. */
		int result4 = 0;	
		if(files != null) {
			for(ClientSupportAttachmentDTO clientSupportAttachmentDTO : files) {
				clientSupportAttachmentDTO.setClientSupportNo(clientSupportNo);
				result4 += clientSupportDAO.updateClientSupportAttachment(clientSupportAttachmentDTO);
			}	
		}
		
		/* 고객지원 처리내역을 등록 한다 */
		int result5 = 0;
		if(supportProcessingHistoryDTO.getProcessingContent() != null) {
			supportProcessingHistoryDTO.setClientSupportNo(clientSupportNo);
			result5 = clientSupportDAO.updateSupportProcessingHistory(supportProcessingHistoryDTO);
		}
		
		
		/* 파일첨부가 없는 경우 : 고객지원을 정상적으로 등록하였을 경우 result = 1을 반환
		 * 파일첨부가 있는 경우 : 고객지원, 첨부파일을 정상적으로 등록하였을 경우 result = 1을 반환
		 */
		System.out.println("result1 : " + result1);
		System.out.println("result2 : " + result2);
		System.out.println("result3 : " + result3);
		System.out.println("result4 : " + result4);
		int result = 0;
		if(supportProcessingHistoryDTO.getProcessingContent() != null) {
			if(result1 > 0  && result2 > 0 && result3 > 0 && result5 > 0 && (files == null || result4 == files.size())) {
				result = 1;
			}
		} else {
			if(result1 > 0  && result2 > 0 && result3 > 0 && (files == null || result4 == files.size())) {
				result = 1;
			}
		}
		
		return result;
	}

}
