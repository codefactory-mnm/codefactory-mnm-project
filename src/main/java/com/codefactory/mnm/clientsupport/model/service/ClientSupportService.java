package com.codefactory.mnm.clientsupport.model.service;

import java.util.Map;

import com.codefactory.mnm.clientsupport.model.dto.ClientSupportAttachmentDTO;
import com.codefactory.mnm.clientsupport.model.dto.ClientSupportInsertDTO;
import com.codefactory.mnm.clientsupport.model.dto.ClientSupportSearchDTO;

public interface ClientSupportService {

	int insertClientSupport(ClientSupportInsertDTO clientSupportInsertDTO);

	Map<String, Object> selectClientSupportList(ClientSupportSearchDTO clientSupportSearchDTO);

	Map<String, Object> selectClientSupport(String clientSupportNo);

	int deleteClientSupport(String clientSupportNo);

	ClientSupportAttachmentDTO selectFile(String clientSupportFileNo);

	Map<String, Object> deleteFile(String clientSupportFileNo);

	int updateClientSupport(ClientSupportInsertDTO clientSupportInsertDTO);

}
