package com.codefactory.mnm.clientsupport.model.dto;

public class ClientSupportAttachmentDTO {

	private String clientSupportFileNo;				//파일번호
	private String fileName;						//파일명
	private String fileOriginalName;				//실제파일명
	private String filePath;						//경로
	private String clientSupportNo;					//고객지원번호
	
	public ClientSupportAttachmentDTO() {}

	public ClientSupportAttachmentDTO(String clientSupportFileNo, String fileName, String fileOriginalName,
			String filePath, String clientSupportNo) {
		super();
		this.clientSupportFileNo = clientSupportFileNo;
		this.fileName = fileName;
		this.fileOriginalName = fileOriginalName;
		this.filePath = filePath;
		this.clientSupportNo = clientSupportNo;
	}

	public String getClientSupportFileNo() {
		return clientSupportFileNo;
	}

	public String getFileName() {
		return fileName;
	}

	public String getFileOriginalName() {
		return fileOriginalName;
	}

	public String getFilePath() {
		return filePath;
	}

	public String getClientSupportNo() {
		return clientSupportNo;
	}

	public void setClientSupportFileNo(String clientSupportFileNo) {
		this.clientSupportFileNo = clientSupportFileNo;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setFileOriginalName(String fileOriginalName) {
		this.fileOriginalName = fileOriginalName;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public void setClientSupportNo(String clientSupportNo) {
		this.clientSupportNo = clientSupportNo;
	}

	@Override
	public String toString() {
		return "ClientSupportAttachmentDTO [clientSupportFileNo=" + clientSupportFileNo + ", fileName=" + fileName
				+ ", fileOriginalName=" + fileOriginalName + ", filePath=" + filePath + ", clientSupportNo="
				+ clientSupportNo + "]";
	}

	
}
