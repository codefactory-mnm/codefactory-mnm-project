package com.codefactory.mnm.clientsupport.model.dto;

import java.util.List;

public class ClientSupportInsertDTO implements java.io.Serializable {

	private ClientSupportDTO clientSupportDTO;							//고객지원 DTO
	private List<ClientSupportAttachmentDTO> files;						//고객지원 첨부파일 리스트
	private List<ClientSupportProductDTO> clientSupportProductList;		//고객지원 품목 리스트
	private SupportProcessingHistoryDTO supportProcessingHistoryDTO;	//고객지원 처리내역 DTO
	
	public ClientSupportInsertDTO() {}

	public ClientSupportInsertDTO(ClientSupportDTO clientSupportDTO, List<ClientSupportAttachmentDTO> files,
			List<ClientSupportProductDTO> clientSupportProductList,
			SupportProcessingHistoryDTO supportProcessingHistoryDTO) {
		super();
		this.clientSupportDTO = clientSupportDTO;
		this.files = files;
		this.clientSupportProductList = clientSupportProductList;
		this.supportProcessingHistoryDTO = supportProcessingHistoryDTO;
	}

	public ClientSupportDTO getClientSupportDTO() {
		return clientSupportDTO;
	}

	public List<ClientSupportAttachmentDTO> getFiles() {
		return files;
	}

	public List<ClientSupportProductDTO> getClientSupportProductList() {
		return clientSupportProductList;
	}

	public SupportProcessingHistoryDTO getSupportProcessingHistoryDTO() {
		return supportProcessingHistoryDTO;
	}

	public void setClientSupportDTO(ClientSupportDTO clientSupportDTO) {
		this.clientSupportDTO = clientSupportDTO;
	}

	public void setFiles(List<ClientSupportAttachmentDTO> files) {
		this.files = files;
	}

	public void setClientSupportProductList(List<ClientSupportProductDTO> clientSupportProductList) {
		this.clientSupportProductList = clientSupportProductList;
	}

	public void setSupportProcessingHistoryDTO(SupportProcessingHistoryDTO supportProcessingHistoryDTO) {
		this.supportProcessingHistoryDTO = supportProcessingHistoryDTO;
	}

	@Override
	public String toString() {
		return "ClientSupportInsertDTO [clientSupportDTO=" + clientSupportDTO + ", files=" + files
				+ ", clientSupportProductList=" + clientSupportProductList + ", supportProcessingHistoryDTO="
				+ supportProcessingHistoryDTO + "]";
	}

	
}
