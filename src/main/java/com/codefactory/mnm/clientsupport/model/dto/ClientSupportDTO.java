package com.codefactory.mnm.clientsupport.model.dto;

import java.sql.Date;

public class ClientSupportDTO implements java.io.Serializable {

	private String clientSupportNo;						//고객지원번호
	private String title;								//제목
	private java.sql.Date requestDate;					//요청일
	private java.sql.Date completeRequestDate;			//완료요청일
	private String importance;							//중요도
	private String clientNo;							//고객번호
	private String receptionistNo;						//접수자번호
	private String receiptWay;							//접수방법
	private java.sql.Date writeDate;					//작성일자
	private String delYn;								//삭제여부
	private String userNo;								//담당자번호
	private String processingDivision;					//처리구분
	private String clientCompanyName;					//고객사명
	private String clientName;							//고객명
	private String receptionistName;					//접수자명
	private String name;								//담당자명
	private String completeDate;						//완료일
	
	public ClientSupportDTO() {}

	public ClientSupportDTO(String clientSupportNo, String title, Date requestDate, Date completeRequestDate,
			String importance, String clientNo, String receptionistNo, String receiptWay, Date writeDate, String delYn,
			String userNo, String processingDivision, String clientCompanyName, String clientName,
			String receptionistName, String name, String completeDate) {
		super();
		this.clientSupportNo = clientSupportNo;
		this.title = title;
		this.requestDate = requestDate;
		this.completeRequestDate = completeRequestDate;
		this.importance = importance;
		this.clientNo = clientNo;
		this.receptionistNo = receptionistNo;
		this.receiptWay = receiptWay;
		this.writeDate = writeDate;
		this.delYn = delYn;
		this.userNo = userNo;
		this.processingDivision = processingDivision;
		this.clientCompanyName = clientCompanyName;
		this.clientName = clientName;
		this.receptionistName = receptionistName;
		this.name = name;
		this.completeDate = completeDate;
	}

	public String getClientSupportNo() {
		return clientSupportNo;
	}

	public String getTitle() {
		return title;
	}

	public java.sql.Date getRequestDate() {
		return requestDate;
	}

	public java.sql.Date getCompleteRequestDate() {
		return completeRequestDate;
	}

	public String getImportance() {
		return importance;
	}

	public String getClientNo() {
		return clientNo;
	}

	public String getReceptionistNo() {
		return receptionistNo;
	}

	public String getReceiptWay() {
		return receiptWay;
	}

	public java.sql.Date getWriteDate() {
		return writeDate;
	}

	public String getDelYn() {
		return delYn;
	}

	public String getUserNo() {
		return userNo;
	}

	public String getProcessingDivision() {
		return processingDivision;
	}

	public String getClientCompanyName() {
		return clientCompanyName;
	}

	public String getClientName() {
		return clientName;
	}

	public String getReceptionistName() {
		return receptionistName;
	}

	public String getName() {
		return name;
	}

	public String getCompleteDate() {
		return completeDate;
	}

	public void setClientSupportNo(String clientSupportNo) {
		this.clientSupportNo = clientSupportNo;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setRequestDate(java.sql.Date requestDate) {
		this.requestDate = requestDate;
	}

	public void setCompleteRequestDate(java.sql.Date completeRequestDate) {
		this.completeRequestDate = completeRequestDate;
	}

	public void setImportance(String importance) {
		this.importance = importance;
	}

	public void setClientNo(String clientNo) {
		this.clientNo = clientNo;
	}

	public void setReceptionistNo(String receptionistNo) {
		this.receptionistNo = receptionistNo;
	}

	public void setReceiptWay(String receiptWay) {
		this.receiptWay = receiptWay;
	}

	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}

	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public void setProcessingDivision(String processingDivision) {
		this.processingDivision = processingDivision;
	}

	public void setClientCompanyName(String clientCompanyName) {
		this.clientCompanyName = clientCompanyName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public void setReceptionistName(String receptionistName) {
		this.receptionistName = receptionistName;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCompleteDate(String completeDate) {
		this.completeDate = completeDate;
	}

	@Override
	public String toString() {
		return "ClientSupportDTO [clientSupportNo=" + clientSupportNo + ", title=" + title + ", requestDate="
				+ requestDate + ", completeRequestDate=" + completeRequestDate + ", importance=" + importance
				+ ", clientNo=" + clientNo + ", receptionistNo=" + receptionistNo + ", receiptWay=" + receiptWay
				+ ", writeDate=" + writeDate + ", delYn=" + delYn + ", userNo=" + userNo + ", processingDivision="
				+ processingDivision + ", clientCompanyName=" + clientCompanyName + ", clientName=" + clientName
				+ ", receptionistName=" + receptionistName + ", name=" + name + ", completeDate=" + completeDate + "]";
	}

	
}
