package com.codefactory.mnm.clientsupport.model.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.codefactory.mnm.clientsupport.model.dto.ClientSupportAttachmentDTO;
import com.codefactory.mnm.clientsupport.model.dto.ClientSupportDTO;
import com.codefactory.mnm.clientsupport.model.dto.ClientSupportProductDTO;
import com.codefactory.mnm.clientsupport.model.dto.ClientSupportSearchDTO;
import com.codefactory.mnm.clientsupport.model.dto.SupportProcessingHistoryDTO;
import com.codefactory.mnm.estimate.model.dto.DeptListDTO;

@Mapper
public interface ClientSupportDAO {

	int insertClientSupport(ClientSupportDTO clientSupportDTO);

	int insertClientSupportProduct(ClientSupportProductDTO salesProduct);

	int insertClientSupportAttachment(ClientSupportAttachmentDTO clientSupportAttachmentDTO);

	int insertSupportProcessingHistory(SupportProcessingHistoryDTO supportProcessingHistoryDTO);

	int selectTotalCount(ClientSupportSearchDTO clientSupportSearchDTO);

	List<ClientSupportDTO> selectClientSupportList(ClientSupportSearchDTO clientSupportSearchDTO);

	List<DeptListDTO> selectDeptList();

	ClientSupportDTO selectClientSupport(String clientSupportNo);

	List<ClientSupportAttachmentDTO> selectFileList(String clientSupportNo);

	List<ClientSupportProductDTO> selectClientSupportProductList(String clientSupportNo);

	List<SupportProcessingHistoryDTO> selectSupportProcessingHistory(String clientSupportNo);

	int deleteClientSupport(String clientSupportNo);

	ClientSupportAttachmentDTO selectFile(String clientSupportFileNo);

	int deleteFile(String clientSupportFileNo);

	int deleteClientSupportProduct(ClientSupportDTO clientSupportDTO);

	int updateClientSupport(ClientSupportDTO clientSupportDTO);

	int updateClientSupportProduct(ClientSupportProductDTO clientSupportProductDTO);

	int updateClientSupportAttachment(ClientSupportAttachmentDTO clientSupportAttachmentDTO);

	int updateSupportProcessingHistory(SupportProcessingHistoryDTO supportProcessingHistoryDTO);

}
