package com.codefactory.mnm.clientsupport.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.codefactory.mnm.clientsupport.model.dto.ClientSupportAttachmentDTO;
import com.codefactory.mnm.clientsupport.model.dto.ClientSupportDTO;
import com.codefactory.mnm.clientsupport.model.dto.ClientSupportInsertDTO;
import com.codefactory.mnm.clientsupport.model.dto.ClientSupportProductDTO;
import com.codefactory.mnm.clientsupport.model.dto.ClientSupportSearchDTO;
import com.codefactory.mnm.clientsupport.model.dto.SupportProcessingHistoryDTO;
import com.codefactory.mnm.clientsupport.model.service.ClientSupportService;
import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.estimate.model.dto.DeptListDTO;

@Controller
@RequestMapping("/business")
public class ClientsupportController {

	private final ClientSupportService clientSupportService;
	
	@Autowired
	public ClientsupportController(ClientSupportService clientSupportService) {
		
		this.clientSupportService = clientSupportService;
	}
	
	@GetMapping("/clientsupport/insert")
	public String clientSupportInsertPage() {
		
		return "clientsupport/insert";
	}
	
	/* 고객지원 등록 */
	@PostMapping("/clientsupport/insert")
	public ModelAndView insertClientSupport(ModelAndView mv, ClientSupportDTO clientSupportDTO, SupportProcessingHistoryDTO supportProcessingHistoryDTO,
			List<MultipartFile> multiFiles, HttpServletRequest request, RedirectAttributes rttr,
			String[] productNo, String[] unit, String[] standard, String[] productNote) {

		/* 고객지원 품목을 담을 리스스와 DTO 생성 */
		List<ClientSupportProductDTO> clientSupportProductList = new ArrayList<>();
		
		/* 고객지원 품목이 있을시 동작 */
		if(productNo.length > 0) {
			
			/* 고객지원 품목 갯수 만큼 동작 */
			for(int i =0; i < productNo.length; i++) {
				
				/* 고객지원품목을 DTO에 담는다 */
				ClientSupportProductDTO clientSupportProductDTO = new ClientSupportProductDTO();
				clientSupportProductDTO.setProductNo(productNo[i]);
				clientSupportProductDTO.setUnit(unit[i]);
				clientSupportProductDTO.setStandard(standard[i]);
				clientSupportProductDTO.setProductNote(productNote[i]);
				
				/* 고객지원 품목을 담은 DTO를 리스트에 담는다 */
				clientSupportProductList.add(clientSupportProductDTO);
			}
		}
		
		/* 고객지원 정보 입력과 동시에 파일첨부도 같이 등록한 경우 */
		if(multiFiles.size() > 0 && !multiFiles.get(0).getOriginalFilename().equals("")) {

			String root = request.getSession().getServletContext().getRealPath("/");	// 첨부파일 저장경로(절대경로)
			String filePath = root + "static/upload/";									// 첨부파일 저장경로(최종경로)

			/* 첨부파일 저장경로 (최종경로) 파일이 존재 하지 않을 경우 생성 */
			File mkdir = new File(filePath);
			if(!mkdir.exists()) {
				mkdir.mkdirs();
			}

			/* 다중 첨부파일이기 때문에, 여러 개의 파일을 담기 위해 List<ClientSupportAttachmentDTO> 타입으로 생성 */
			List<ClientSupportAttachmentDTO> files = new ArrayList<>();

			/* 다중 첨부파일은 반복문을 통해서, 미리 선언한 files에 담음  */
			for(int i = 0; i < multiFiles.size(); i++) {
				String fileOriginalName = multiFiles.get(i).getOriginalFilename();			// 원본파일명
				String ext = fileOriginalName.substring(fileOriginalName.lastIndexOf("."));	// 확장자명
				String fileName = UUID.randomUUID().toString().replace("-", "") + ext;		// 저장파일명

				/* 고객지원파일첨부DTO인 file에 정보를 담은 후 리스트인 files에 담음 */
				ClientSupportAttachmentDTO file = new ClientSupportAttachmentDTO();			
				file.setFileName(fileName);
				file.setFileOriginalName(fileOriginalName);
				file.setFilePath(filePath);

				files.add(file);
			}
			
			/* 고객지원 등록시 필요한 정보를 clientSupportInsertDTO에 담는다 */
			ClientSupportInsertDTO clientSupportInsertDTO = new ClientSupportInsertDTO();
			clientSupportInsertDTO.setClientSupportDTO(clientSupportDTO);
			clientSupportInsertDTO.setFiles(files);
			clientSupportInsertDTO.setClientSupportProductList(clientSupportProductList);
			clientSupportInsertDTO.setSupportProcessingHistoryDTO(supportProcessingHistoryDTO);
			
			/* 서비스로 salesInsertDTO를 보낸다 */
			int result = clientSupportService.insertClientSupport(clientSupportInsertDTO);
			
			/* DB에 등록을 성공하였을 경우 */
			if(result > 0 ) {
					
					/* 파일 저장 */
					try {
						for(int i = 0; i < multiFiles.size(); i++) {
							
							ClientSupportAttachmentDTO file = files.get(i);			
							multiFiles.get(i).transferTo(new File(filePath + "\\" + file.getFileName()));
							
						}
						
						rttr.addFlashAttribute("message", "고객지원 등록을 성공하였습니다.");
						
						/* 파일 저장을 실패하였을 경우, 저장했던 파일을 삭제
						 * 트랜젝션처리를 통해서 DB에 입력된 정보도 삭제됨 */
					} catch (Exception e) {
						e.printStackTrace();
						
						for(int i = 0; i < multiFiles.size(); i++) {
							
							ClientSupportAttachmentDTO file = files.get(i); 
							new File(filePath + "\\" + file.getFileName()).delete(); 
							
						}
						
						rttr.addFlashAttribute("message", "고객지원 등록 후 파일 업로드에 실패하였습니다.");
						
					}
			
			/* DB에 고객지원 등록을 실패하였을 경우 */
			} else {
				
				rttr.addFlashAttribute("message", "고객지원 등록을 실패하였습니다.");	
				
			}

		
		/* 파일첨부를 하지 않고, 고객지원 등록을 하는 경우 */
		} else {
			/* 고객지원 등록시 필요한 정보를 clientSupportInsertDTO에 담는다 */
			ClientSupportInsertDTO clientSupportInsertDTO = new ClientSupportInsertDTO();
			clientSupportInsertDTO.setClientSupportDTO(clientSupportDTO);
			clientSupportInsertDTO.setClientSupportProductList(clientSupportProductList);
			clientSupportInsertDTO.setSupportProcessingHistoryDTO(supportProcessingHistoryDTO);
			
			/* 서비스로 salesInsertDTO를 전달한다 */
			int result = clientSupportService.insertClientSupport(clientSupportInsertDTO);

				if(result > 0) {
					rttr.addFlashAttribute("message", "고객지원 등록을 성공하였습니다.");
				} else {
					rttr.addFlashAttribute("message", "고객지원 등록을 실패하였습니다.");
				}

		}
		
		/* 고객지원 등록 후 연결할 페이지 */
		mv.setViewName("redirect:/business/clientsupport/list");
		
		return mv;
	}
	
	/* 고객지원 목록 조회 */
	@GetMapping("/clientsupport/list")
	public ModelAndView selectClientSupportList(ModelAndView mv,
			@RequestParam(defaultValue = "") String division,
			@RequestParam(defaultValue = "") String searchName,
			@RequestParam(defaultValue = "") String deptName,
			@RequestParam(defaultValue = "") String name,
			@RequestParam(defaultValue = "1") String currentPage) {
		
		/* 처음 고객지원 리스트를 조회하거나 또는 검색조건을 설정하지 않았을 때를 대비 */
		if(division.equals("")) { // 검색조건 중 구분을 선택하지 않았을 경우
			division = null;
		}
		
		if(searchName.equals("")) { // 검색조건 중 고객사를 입력하지 않았을 경우
			searchName = null;
		}
		
		if(deptName.equals("")) { // 검색조건 중 부서를 선택하지 않았을 경우
			deptName = null;
		}
		
		if(name.equals("")) { // 검색조건 중 이름을 선택하지 않았을 경우
			name = null;
		}
		
		/* 검색조건을 담는다. 검색조건이 없을 경우 null로 담김. */
		ClientSupportSearchDTO clientSupportSearchDTO = new ClientSupportSearchDTO();
		clientSupportSearchDTO.setDivision(division);
		clientSupportSearchDTO.setSearchName(searchName);
		clientSupportSearchDTO.setDeptName(deptName);
		clientSupportSearchDTO.setName(name);
		clientSupportSearchDTO.setCurrentPage(currentPage);
		
		/* 검색조건을 searvice로 보내주고, 값을 map으로 전달받음 */
		Map<String, Object> map = clientSupportService.selectClientSupportList(clientSupportSearchDTO);
		
		/* db에서 가져온 값을 꺼낸다. */
		List<ClientSupportDTO> clientSupportList = (List<ClientSupportDTO>) map.get("clientSupportList");
		List<DeptListDTO> deptList = (List<DeptListDTO>) map.get("deptList");
		SelectCriteria selectCriteria = (SelectCriteria) map.get("selectCriteria");
		int totalCount = (int) map.get("totalCount");
		
		/* db에서 조회해온 결과를 담는다 */
		mv.addObject("clientSupportList", clientSupportList);
		mv.addObject("deptList", deptList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("totalCount", totalCount);
		mv.addObject("clientSupportSearchDTO", clientSupportSearchDTO);
		mv.setViewName("clientsupport/list");
		
		return mv;
	}
	
	/* 고객지원 상세조회 */
	@GetMapping("/clientsupport/detail")
	public ModelAndView selectClientSupport(ModelAndView mv, @RequestParam String clientSupportNo) {
		
		/* Map을 생성하여 서비스에서 값을 리턴 받는다*/
		Map<String, Object> map = clientSupportService.selectClientSupport(clientSupportNo);
		
		/* map에 담은 값을 꺼낸다 */
		ClientSupportDTO oneClientSupport = (ClientSupportDTO) map.get("oneClientSupport");
		List<ClientSupportAttachmentDTO> fileList = (List<ClientSupportAttachmentDTO>) map.get("fileList");
		List<ClientSupportProductDTO> productList = (List<ClientSupportProductDTO>) map.get("productList");
		List<SupportProcessingHistoryDTO> supportProcessingHistoryList = (List<SupportProcessingHistoryDTO>) map.get("supportProcessingHistoryList");

		String completeDate = oneClientSupport.getCompleteDate();
		if(completeDate != null) {
			String completeDate2 = completeDate.substring(0, 10);
			oneClientSupport.setCompleteDate(completeDate2);
		} else {
			oneClientSupport.setCompleteDate("연도-월-일");
		}
		
		/* map에서 꺼낸 값들을 담는다 */
		mv.addObject("oneClientSupport", oneClientSupport);
		mv.addObject("fileList", fileList);
		mv.addObject("productList", productList);
		mv.addObject("supportProcessingHistoryList", supportProcessingHistoryList);
		mv.setViewName("clientsupport/detail");
		
		return mv;
	}
	
	/* 고객지원 삭제 */
	@GetMapping("/clientsupport/delete")
	public ModelAndView deleteClientsupport(ModelAndView mv, @RequestParam String clientSupportNo, RedirectAttributes rttr) {
		
		/* 고객지원 번호를 받아서 해당 게시글을 삭제 한다 */
		int result = clientSupportService.deleteClientSupport(clientSupportNo);
		
		if(result > 0) {
			rttr.addFlashAttribute("message", "고객지원 삭제를 성공하였습니다.");
		} else {
			rttr.addFlashAttribute("message", "고객지원 삭제를 실패하였습니다.");
		}
		
		/* 삭제 후 연결할 페이지 */
		mv.setViewName("redirect:/business/clientsupport/list");
		
		return mv;
	}
	
	/* 첨부파일 다운로드 */
	@GetMapping("/clientsupport/download")
	@ResponseBody
	public void download(HttpServletResponse response, String clientSupportFileNo) throws IOException {
		
		/* 전달받은 fileNo로 파일 정보 조회 */
		ClientSupportAttachmentDTO clientSupportAttachmentDTO = clientSupportService.selectFile(clientSupportFileNo);
		String fileOriginalName = clientSupportAttachmentDTO.getFileOriginalName();
		String filePath = clientSupportAttachmentDTO.getFilePath();
		String fileName = clientSupportAttachmentDTO.getFileName();
		
		String saveFileName = filePath + fileName;
		
		File file = new File(saveFileName);
		long fileLength = file.length();
		
		/* 파일 정보들을 가지고 reponse의 Header에 세팅 */
		response.setHeader("Content-Disposition", "attachment; filename=\"" + URLEncoder.encode(fileOriginalName,"UTF-8") + "\";");
        response.setHeader("Content-Transfer-Encoding", "binary");
        response.setHeader("Content-Type", "application/octet-stream");
        response.setHeader("Content-Length", "" + fileLength);
        response.setHeader("Pragma", "no-cache;");
        response.setHeader("Expires", "-1;");

		/* saveFileName을 파라미터로 넣어 inputStream 객체를 만들고 
        response에서 파일을 내보낼 OutputStream을 가져와서 */
        FileInputStream fis = new FileInputStream(saveFileName);
        OutputStream out = response.getOutputStream();
        
    	/* 파일 읽을 만큼 크기의 buffer를 생성한 후 */
        int readCount = 0;
        byte[] buffer = new byte[1024];
        
        /* outputStream에 씌워준다 */
        while((readCount = fis.read(buffer)) != -1){
                out.write(buffer,0,readCount);
        }
        /* 스트림 close */
        fis.close();
        out.close();
        
	}
	
	/* 고객지원 수정페이지로 이동 */
	@GetMapping("/clientsupport/update")
	public ModelAndView updateClientSupportPage(ModelAndView mv, @RequestParam String clientSupportNo) {
		
		/* Map을 생성하여 서비스에서 값을 리턴 받는다*/
		Map<String, Object> map = clientSupportService.selectClientSupport(clientSupportNo);
		
		/* map에 담은 값을 꺼낸다 */
		ClientSupportDTO oneClientSupport = (ClientSupportDTO) map.get("oneClientSupport");
		List<ClientSupportAttachmentDTO> fileList = (List<ClientSupportAttachmentDTO>) map.get("fileList");
		List<ClientSupportProductDTO> productList = (List<ClientSupportProductDTO>) map.get("productList");
		List<SupportProcessingHistoryDTO> supportProcessingHistoryList = (List<SupportProcessingHistoryDTO>) map.get("supportProcessingHistoryList");

		/* map에서 꺼낸 값들을 담는다 */
		mv.addObject("oneClientSupport", oneClientSupport);
		mv.addObject("fileList", fileList);
		mv.addObject("productList", productList);
		mv.addObject("supportProcessingHistoryList", supportProcessingHistoryList);
		mv.setViewName("clientsupport/update");
		
		return mv;
	}
	
	/* ajax를 통해 파일 삭제 */
	@PostMapping(value="/clientsupport/file/delete", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int deleteFile(String clientSupportFileNo, HttpServletRequest request) {

		String root = request.getSession().getServletContext().getRealPath("/");	//첨부파일 저장경로(절대경로)
		String filePath = root + "static/upload/";									//첨부파일 저장경로(최종경로)

		/* 파일번호를 service에 전달하고 map으로 값을 전달받음 */
		Map<String, Object> map = clientSupportService.deleteFile(clientSupportFileNo);	

		ClientSupportAttachmentDTO file = (ClientSupportAttachmentDTO) map.get("file");		//첨부파일정보
		int result = (int) map.get("result");												//첨부파일 DB에서 삭제 결과

		/* 첨부파일을 DB에서 성공적으로 삭제되었을 경우 저장된파일도 삭제 */
		if(result > 0 ) {

			new File(filePath + "\\" + file.getFileName()).delete(); 		

		}

		/* 화면으로 전달 */
		return result;

	}
	
	/* 고객지원 수정 */
	@PostMapping("/clientsupport/update")
	public ModelAndView updateClientSupport(ModelAndView mv, ClientSupportDTO clientSupportDTO, SupportProcessingHistoryDTO supportProcessingHistoryDTO,
			List<MultipartFile> multiFiles, HttpServletRequest request, RedirectAttributes rttr,
			String[] productNo, String[] unit, String[] standard, String[] productNote) {

		/* 고객지원 품목을 담을 리스스와 DTO 생성 */
		List<ClientSupportProductDTO> clientSupportProductList = new ArrayList<>();
		
		/* 고객지원 품목이 있을시 동작 */
		if(productNo.length > 0) {
			
			/* 고객지원 품목 갯수 만큼 동작 */
			for(int i =0; i < productNo.length; i++) {
				
				/* 고객지원품목을 DTO에 담는다 */
				ClientSupportProductDTO clientSupportProductDTO = new ClientSupportProductDTO();
				clientSupportProductDTO.setProductNo(productNo[i]);
				clientSupportProductDTO.setUnit(unit[i]);
				clientSupportProductDTO.setStandard(standard[i]);
				clientSupportProductDTO.setProductNote(productNote[i]);
				
				/* 고객지원 품목을 담은 DTO를 리스트에 담는다 */
				clientSupportProductList.add(clientSupportProductDTO);
			}
		}
		
		/* 고객지원 정보 입력과 동시에 파일첨부도 같이 등록한 경우 */
		if(multiFiles.size() > 0 && !multiFiles.get(0).getOriginalFilename().equals("")) {

			String root = request.getSession().getServletContext().getRealPath("/");	// 첨부파일 저장경로(절대경로)
			String filePath = root + "static/upload/";									// 첨부파일 저장경로(최종경로)

			/* 첨부파일 저장경로 (최종경로) 파일이 존재 하지 않을 경우 생성 */
			File mkdir = new File(filePath);
			if(!mkdir.exists()) {
				mkdir.mkdirs();
			}

			/* 다중 첨부파일이기 때문에, 여러 개의 파일을 담기 위해 List<ClientSupportAttachmentDTO> 타입으로 생성 */
			List<ClientSupportAttachmentDTO> files = new ArrayList<>();

			/* 다중 첨부파일은 반복문을 통해서, 미리 선언한 files에 담음  */
			for(int i = 0; i < multiFiles.size(); i++) {
				String fileOriginalName = multiFiles.get(i).getOriginalFilename();			// 원본파일명
				String ext = fileOriginalName.substring(fileOriginalName.lastIndexOf("."));	// 확장자명
				String fileName = UUID.randomUUID().toString().replace("-", "") + ext;		// 저장파일명

				/* 고객지원파일첨부DTO인 file에 정보를 담은 후 리스트인 files에 담음 */
				ClientSupportAttachmentDTO file = new ClientSupportAttachmentDTO();			
				file.setFileName(fileName);
				file.setFileOriginalName(fileOriginalName);
				file.setFilePath(filePath);

				files.add(file);
			}
			
			/* 고객지원 등록시 필요한 정보를 clientSupportInsertDTO에 담는다 */
			ClientSupportInsertDTO clientSupportInsertDTO = new ClientSupportInsertDTO();
			clientSupportInsertDTO.setClientSupportDTO(clientSupportDTO);
			clientSupportInsertDTO.setFiles(files);
			clientSupportInsertDTO.setClientSupportProductList(clientSupportProductList);
			clientSupportInsertDTO.setSupportProcessingHistoryDTO(supportProcessingHistoryDTO);
			
			/* 서비스로 salesInsertDTO를 보낸다 */
			int result = clientSupportService.updateClientSupport(clientSupportInsertDTO);
			
			/* DB에 등록을 성공하였을 경우 */
			if(result > 0 ) {
					
					/* 파일 저장 */
					try {
						for(int i = 0; i < multiFiles.size(); i++) {
							
							ClientSupportAttachmentDTO file = files.get(i);			
							multiFiles.get(i).transferTo(new File(filePath + "\\" + file.getFileName()));
							
						}
						
						rttr.addFlashAttribute("message", "고객지원 수정을 성공하였습니다.");
						
						/* 파일 저장을 실패하였을 경우, 저장했던 파일을 삭제
						 * 트랜젝션처리를 통해서 DB에 입력된 정보도 삭제됨 */
					} catch (Exception e) {
						e.printStackTrace();
						
						for(int i = 0; i < multiFiles.size(); i++) {
							
							ClientSupportAttachmentDTO file = files.get(i); 
							new File(filePath + "\\" + file.getFileName()).delete(); 
							
						}
						
						rttr.addFlashAttribute("message", "고객지원 수정 후 파일 업로드에 실패하였습니다.");
						
					}
			
			/* DB에 고객지원 등록을 실패하였을 경우 */
			} else {
				
				rttr.addFlashAttribute("message", "고객지원 수정을 실패하였습니다.");	
				
			}

		
		/* 파일첨부를 하지 않고, 고객지원 등록을 하는 경우 */
		} else {
			/* 고객지원 등록시 필요한 정보를 clientSupportInsertDTO에 담는다 */
			ClientSupportInsertDTO clientSupportInsertDTO = new ClientSupportInsertDTO();
			clientSupportInsertDTO.setClientSupportDTO(clientSupportDTO);
			clientSupportInsertDTO.setClientSupportProductList(clientSupportProductList);
			clientSupportInsertDTO.setSupportProcessingHistoryDTO(supportProcessingHistoryDTO);
			
			/* 서비스로 salesInsertDTO를 전달한다 */
			int result = clientSupportService.updateClientSupport(clientSupportInsertDTO);

				if(result > 0) {
					rttr.addFlashAttribute("message", "고객지원 수정을 성공하였습니다.");
				} else {
					rttr.addFlashAttribute("message", "고객지원 수정을 실패하였습니다.");
				}

		}
		
		/* 고객지원 등록 후 연결할 페이지 */
		mv.setViewName("redirect:/business/clientsupport/list");
		
		return mv;
	}
}
