package com.codefactory.mnm.estimate.model.dto;

public class ApproverListDTO implements java.io.Serializable {

	private String approverNo;					//결재권자번호
	private String approverName;				//결재권자명
	private String authorityNo;					//권한번호
	private String authorityName;				//권한명
	private String deptName;					//부서
	private String position;					//직위
	
	public ApproverListDTO() {}

	public ApproverListDTO(String approverNo, String approverName, String authorityNo, String authorityName,
			String deptName, String position) {
		super();
		this.approverNo = approverNo;
		this.approverName = approverName;
		this.authorityNo = authorityNo;
		this.authorityName = authorityName;
		this.deptName = deptName;
		this.position = position;
	}

	public String getApproverNo() {
		return approverNo;
	}

	public String getApproverName() {
		return approverName;
	}

	public String getAuthorityNo() {
		return authorityNo;
	}

	public String getAuthorityName() {
		return authorityName;
	}

	public String getDeptName() {
		return deptName;
	}

	public String getPosition() {
		return position;
	}

	public void setApproverNo(String approverNo) {
		this.approverNo = approverNo;
	}

	public void setApproverName(String approverName) {
		this.approverName = approverName;
	}

	public void setAuthorityNo(String authorityNo) {
		this.authorityNo = authorityNo;
	}

	public void setAuthorityName(String authorityName) {
		this.authorityName = authorityName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	@Override
	public String toString() {
		return "ApproverListDTO [approverNo=" + approverNo + ", approverName=" + approverName + ", authorityNo="
				+ authorityNo + ", authorityName=" + authorityName + ", deptName=" + deptName + ", position=" + position
				+ "]";
	}

	
}
