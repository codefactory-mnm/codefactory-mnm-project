package com.codefactory.mnm.estimate.model.dto;

import java.sql.Date;

public class EstimateDTO implements java.io.Serializable {

	private String estimateNo;					//견적번호
	private String estimateName;				//견적명
	private java.sql.Date estimateDate;			//견적일
	private int quantity;						//수량
	private String totalSupplyPrice;			//전체공급가액
	private String suggestedSupplyPrice;		//제안공급가액
	private String taxAmount;					//세액
	private String sum;							//합계
	private String vat;							//부가세
	private String note;						//비고
	private String clientNo;					//고객번호
	private String delYn;						//삭제여부
	private String userNo;						//담당자번호
	private java.sql.Date writeDate;			//작성일자
	private String businessOpportunityNo;		//영업기회번호
	
	public EstimateDTO() {}

	public EstimateDTO(String estimateNo, String estimateName, Date estimateDate, int quantity, String totalSupplyPrice,
			String suggestedSupplyPrice, String taxAmount, String sum, String vat, String note, String clientNo,
			String delYn, String userNo, Date writeDate, String businessOpportunityNo) {
		super();
		this.estimateNo = estimateNo;
		this.estimateName = estimateName;
		this.estimateDate = estimateDate;
		this.quantity = quantity;
		this.totalSupplyPrice = totalSupplyPrice;
		this.suggestedSupplyPrice = suggestedSupplyPrice;
		this.taxAmount = taxAmount;
		this.sum = sum;
		this.vat = vat;
		this.note = note;
		this.clientNo = clientNo;
		this.delYn = delYn;
		this.userNo = userNo;
		this.writeDate = writeDate;
		this.businessOpportunityNo = businessOpportunityNo;
	}

	public String getEstimateNo() {
		return estimateNo;
	}

	public String getEstimateName() {
		return estimateName;
	}

	public java.sql.Date getEstimateDate() {
		return estimateDate;
	}

	public int getQuantity() {
		return quantity;
	}

	public String getTotalSupplyPrice() {
		return totalSupplyPrice;
	}

	public String getSuggestedSupplyPrice() {
		return suggestedSupplyPrice;
	}

	public String getTaxAmount() {
		return taxAmount;
	}

	public String getSum() {
		return sum;
	}

	public String getVat() {
		return vat;
	}

	public String getNote() {
		return note;
	}

	public String getClientNo() {
		return clientNo;
	}

	public String getDelYn() {
		return delYn;
	}

	public String getUserNo() {
		return userNo;
	}

	public java.sql.Date getWriteDate() {
		return writeDate;
	}

	public String getBusinessOpportunityNo() {
		return businessOpportunityNo;
	}

	public void setEstimateNo(String estimateNo) {
		this.estimateNo = estimateNo;
	}

	public void setEstimateName(String estimateName) {
		this.estimateName = estimateName;
	}

	public void setEstimateDate(java.sql.Date estimateDate) {
		this.estimateDate = estimateDate;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public void setTotalSupplyPrice(String totalSupplyPrice) {
		this.totalSupplyPrice = totalSupplyPrice;
	}

	public void setSuggestedSupplyPrice(String suggestedSupplyPrice) {
		this.suggestedSupplyPrice = suggestedSupplyPrice;
	}

	public void setTaxAmount(String taxAmount) {
		this.taxAmount = taxAmount;
	}

	public void setSum(String sum) {
		this.sum = sum;
	}

	public void setVat(String vat) {
		this.vat = vat;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public void setClientNo(String clientNo) {
		this.clientNo = clientNo;
	}

	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}

	public void setBusinessOpportunityNo(String businessOpportunityNo) {
		this.businessOpportunityNo = businessOpportunityNo;
	}

	@Override
	public String toString() {
		return "EstimateDTO [estimateNo=" + estimateNo + ", estimateName=" + estimateName + ", estimateDate="
				+ estimateDate + ", quantity=" + quantity + ", totalSupplyPrice=" + totalSupplyPrice
				+ ", suggestedSupplyPrice=" + suggestedSupplyPrice + ", taxAmount=" + taxAmount + ", sum=" + sum
				+ ", vat=" + vat + ", note=" + note + ", clientNo=" + clientNo + ", delYn=" + delYn + ", userNo="
				+ userNo + ", writeDate=" + writeDate + ", businessOpportunityNo=" + businessOpportunityNo + "]";
	}
	
	
	
}
