package com.codefactory.mnm.estimate.model.dto;

public class DeptListDTO implements java.io.Serializable {

	private String deptNo;				//부서번호
	private String deptName;			//부서명
	private String refDeptNo;			//상위부서번호
	private int sortOrder;				//정렬순서
	
	public DeptListDTO() {}

	public DeptListDTO(String deptNo, String deptName, String refDeptNo, int sortOrder) {
		super();
		this.deptNo = deptNo;
		this.deptName = deptName;
		this.refDeptNo = refDeptNo;
		this.sortOrder = sortOrder;
	}

	public String getDeptNo() {
		return deptNo;
	}

	public String getDeptName() {
		return deptName;
	}

	public String getRefDeptNo() {
		return refDeptNo;
	}

	public int getSortOrder() {
		return sortOrder;
	}

	public void setDeptNo(String deptNo) {
		this.deptNo = deptNo;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public void setRefDeptNo(String refDeptNo) {
		this.refDeptNo = refDeptNo;
	}

	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}

	@Override
	public String toString() {
		return "DeptListDTO [deptNo=" + deptNo + ", deptName=" + deptName + ", refDeptNo=" + refDeptNo + ", sortOrder="
				+ sortOrder + "]";
	}
	
	
	
}
