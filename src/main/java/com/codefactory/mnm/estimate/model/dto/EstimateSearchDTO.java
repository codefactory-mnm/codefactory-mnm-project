package com.codefactory.mnm.estimate.model.dto;

import com.codefactory.mnm.common.SelectCriteria;

public class EstimateSearchDTO implements java.io.Serializable {

	private String division;					//구분
	private String searchName;					//검색어
	private String deptName;					//담당자 부서
	private String name;						//담당자이름
	private String currentPage;					//현재페이지
	private SelectCriteria selectCriteria;		//페이징처리
	
	public EstimateSearchDTO() {}

	public EstimateSearchDTO(String division, String searchName, String deptName, String name, String currentPage,
			SelectCriteria selectCriteria) {
		super();
		this.division = division;
		this.searchName = searchName;
		this.deptName = deptName;
		this.name = name;
		this.currentPage = currentPage;
		this.selectCriteria = selectCriteria;
	}

	public String getDivision() {
		return division;
	}

	public String getSearchName() {
		return searchName;
	}

	public String getDeptName() {
		return deptName;
	}

	public String getName() {
		return name;
	}

	public String getCurrentPage() {
		return currentPage;
	}

	public SelectCriteria getSelectCriteria() {
		return selectCriteria;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public void setSearchName(String searchName) {
		this.searchName = searchName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCurrentPage(String currentPage) {
		this.currentPage = currentPage;
	}

	public void setSelectCriteria(SelectCriteria selectCriteria) {
		this.selectCriteria = selectCriteria;
	}

	@Override
	public String toString() {
		return "EstimateSearchDTO [division=" + division + ", searchName=" + searchName + ", deptName=" + deptName
				+ ", name=" + name + ", currentPage=" + currentPage + ", selectCriteria=" + selectCriteria + "]";
	}
	
	
}
