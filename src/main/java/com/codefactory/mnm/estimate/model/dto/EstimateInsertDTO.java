package com.codefactory.mnm.estimate.model.dto;

import java.util.List;

public class EstimateInsertDTO implements java.io.Serializable {

	private EstimateDTO estimateDTO;					//견적DTO
	private ApprovalListDTO approvalListDTO;			//결재DTO
	private List<EstimateAttachmentDTO> files;			//견적 첨부파일 List
	private List<ProductListDTO> productList;			//견적 제품 List
	
	public EstimateInsertDTO() {}

	public EstimateInsertDTO(EstimateDTO estimateDTO, ApprovalListDTO approvalListDTO,
			List<EstimateAttachmentDTO> files, List<ProductListDTO> productList) {
		super();
		this.estimateDTO = estimateDTO;
		this.approvalListDTO = approvalListDTO;
		this.files = files;
		this.productList = productList;
	}

	public EstimateDTO getEstimateDTO() {
		return estimateDTO;
	}

	public ApprovalListDTO getApprovalListDTO() {
		return approvalListDTO;
	}

	public List<EstimateAttachmentDTO> getFiles() {
		return files;
	}

	public List<ProductListDTO> getProductList() {
		return productList;
	}

	public void setEstimateDTO(EstimateDTO estimateDTO) {
		this.estimateDTO = estimateDTO;
	}

	public void setApprovalListDTO(ApprovalListDTO approvalListDTO) {
		this.approvalListDTO = approvalListDTO;
	}

	public void setFiles(List<EstimateAttachmentDTO> files) {
		this.files = files;
	}

	public void setProductList(List<ProductListDTO> productList) {
		this.productList = productList;
	}

	@Override
	public String toString() {
		return "EstimateInsertDTO [estimateDTO=" + estimateDTO + ", approvalListDTO=" + approvalListDTO + ", files="
				+ files + ", productList=" + productList + "]";
	}

	
	
}
