package com.codefactory.mnm.estimate.model.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.codefactory.mnm.common.Pagenation;
import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.estimate.model.dao.EstimateDAO;
import com.codefactory.mnm.estimate.model.dto.ApprovalListDTO;
import com.codefactory.mnm.estimate.model.dto.ApproverListDTO;
import com.codefactory.mnm.estimate.model.dto.BusinessOpportunityListDTO;
import com.codefactory.mnm.estimate.model.dto.ClientListDTO;
import com.codefactory.mnm.estimate.model.dto.CompanyListDTO;
import com.codefactory.mnm.estimate.model.dto.DeptListDTO;
import com.codefactory.mnm.estimate.model.dto.EstimateAttachmentDTO;
import com.codefactory.mnm.estimate.model.dto.EstimateDTO;
import com.codefactory.mnm.estimate.model.dto.EstimateInsertDTO;
import com.codefactory.mnm.estimate.model.dto.EstimateListDTO;
import com.codefactory.mnm.estimate.model.dto.EstimateSearchDTO;
import com.codefactory.mnm.estimate.model.dto.ManagerListDTO;
import com.codefactory.mnm.estimate.model.dto.OpinionDTO;
import com.codefactory.mnm.estimate.model.dto.ProductListDTO;
import com.codefactory.mnm.estimate.model.dto.UserListDTO;
import com.codefactory.mnm.sales.model.dto.SalesProductDTO;

@Service
public class EstimateServiceImpl implements EstimateService {

	private final EstimateDAO estimateDAO;
	
	@Autowired
	public EstimateServiceImpl(EstimateDAO estimateDAO) {
		
		this.estimateDAO = estimateDAO;
	}
	
	/* 고객사 조회 */
	@Override
	public List<CompanyListDTO> selectCompanyList() {

		return estimateDAO.selectCompanyList();
	}
	
	/* 고객 조회 */
	@Override
	public List<ClientListDTO> selectClientList() {

		return estimateDAO.selectClientList();
	}

	/* 영업기회 조회 */
	@Override
	public List<BusinessOpportunityListDTO> selectBusinessOpportunityList() {

		return estimateDAO.selectBusinessOpportunityList();
	}

	/* 담당자 조회 */
	@Override
	public List<UserListDTO> selectUserList() {

		return estimateDAO.selectUserList();
	}

	/* 상품 조회 */
	@Override
	public List<ProductListDTO> selectProductList() {

		return estimateDAO.selectProductList();
	}

	/* 견적 리스트 조회 */
	@Override
	public Map<String, Object> selectEstimateList(EstimateSearchDTO estimateSearchDTO) {

		/* 첫 요청페이지 */
		int pageNo = 1;		
		
		/* 요청 페이지가 0이하면 1로 바꿔줌 */
		if(pageNo <= 0) {
			pageNo = 1;
		}
		
		/* 최대 페이지 */
		int limit = 10;
		
		/* 최대 버튼 갯수 */
		int buttonAmount = 5;
		
		/* 전체 게시물 수, 검색조건이 있다면 검색조건에 맞는 게시물 수 */
		int totalCount = estimateDAO.selectTotalCount(estimateSearchDTO);

		/* view에서 전달받은 요청페이지 */
		String currentPage = estimateSearchDTO.getCurrentPage();
		
		/* view에서 전달받은 요청페이지를 pageNo에 저장 */
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		/* 설정한 변수에 따른 페이징조건 */
		SelectCriteria selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount);
				
		/* 조회 결과를 담을 Map 생성 */
		Map<String, Object> map = new HashMap<>();
		
		estimateSearchDTO.setSelectCriteria(selectCriteria);
		
		/* 견적 조회 */
		List<EstimateListDTO> estimateList = estimateDAO.selectEstimateList(estimateSearchDTO);
		
		/* 부서 조회 */
		List<DeptListDTO> deptList = estimateDAO.selectDeptList();
		
		map.put("estimateList", estimateList);
		map.put("deptList", deptList);
		map.put("selectCriteria", selectCriteria);
		map.put("totalCount", totalCount);
		
		return map;
	}

	/* 결재권자 조회 */
	@Override
	public List<ApproverListDTO> selectApproverList() {

		return estimateDAO.selectApproverList();
	}

	/* 견적 등록 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertEstimate(EstimateInsertDTO estimateInsertDTO) {

		/* controller에서 받아온 정보를 꺼낸다. */
		EstimateDTO estimateDTO = estimateInsertDTO.getEstimateDTO();
		ApprovalListDTO approvalListDTO = estimateInsertDTO.getApprovalListDTO();
		List<EstimateAttachmentDTO> files = estimateInsertDTO.getFiles();
		List<ProductListDTO> productList = estimateInsertDTO.getProductList();
		
		/* 견적 작성내용을 DB에 저장한다. */
		int result1 = estimateDAO.insertEstimate(estimateDTO); 			
		
		/* 결재 내용을 DB에 저장한다. */
		int result2 = estimateDAO.insertApproval(approvalListDTO);
		
		/* 결재 변경이력을 DB에 저장한다 */
		int result3 = estimateDAO.insertApprovalHistory();
		
		/* 파일첨부정보를 DB에 저장한 횟수를 저장한다. */
		int result4 = 0;	
		if(files != null) {
			for(EstimateAttachmentDTO estimateAttachmentDTO : files) {
				result4 += estimateDAO.insertEstimateAttachment(estimateAttachmentDTO);
			}	
		}
		
		int result5 = 0;
		/* 견적 품목을 등록한다 */
		for(int i = 0; i < productList.size(); i++) {
			
			ProductListDTO productListDTO = productList.get(i);
			result5 = estimateDAO.insertEstimateProduct(productListDTO);
		}
		
		/* 파일첨부가 없는 경우 : 견적, 결재를 정상적으로 등록하였을 경우 result = 1을 반환
		 * 파일첨부가 있는 경우 : 견적, 결재, 첨부파일을 정상적으로 등록하였을 경우 result = 1을 반환
		 */
		int result = 0;
		if(result1 > 0 && result2 > 0 && result3 > 0 && result5 > 0 && (files == null || result4 == files.size())) {
			result = 1;
		}
		
		return result;
	}

	/* 부서별 담당자 조회*/
	@Override
	public List<ManagerListDTO> selectManagerList(String deptName) {

		return estimateDAO.selectManagerList(deptName);
	}

	/* 견적 상세 조회 */
	@Override
	public Map<String, Object> selectEstimate(String estimateNo) {

		Map<String, Object> map = new HashMap<>();
		
		EstimateListDTO oneEstimate = estimateDAO.selectEstimate(estimateNo);
		String approver = estimateDAO.selectApprover(estimateNo);
		List<EstimateListDTO> fileList = estimateDAO.selectFileList(estimateNo);
		List<ProductListDTO> productList = estimateDAO.selectEstimateProductList(estimateNo);
		
		map.put("oneEstimate", oneEstimate);
		map.put("approver", approver);
		map.put("fileList", fileList);
		map.put("productList", productList);
		
		return map;
	}

	/* 견적 삭제 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int deleteEstimate(String estimateNo) {

		int result = estimateDAO.deleteEstimate(estimateNo);
		
		return result;
	}

	/* 첨부파일 정보 조회 */
	@Override
	public EstimateAttachmentDTO selectFile(String fileNo) {
		
		EstimateAttachmentDTO file = estimateDAO.selectFile(fileNo);
		
		return file;
	}

	/* 견적 상세 조회 시 ajax를 통해 의견 조회 */
	@Override
	public List<OpinionDTO> selectOpinionList(String estimateNo) {

		return estimateDAO.selectOpinionList(estimateNo);
	}

	/* 의견 작성 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertOpinion(String estimateNo, String opinionContent, String id) {

		/* 로그인된 사용자의 번호, 이름 조회하여 저장 */
		OpinionDTO opinion = estimateDAO.selectUserNoAndName(id);
		
		/* 고객사번호, 의견내용, 아이디를 저장 */
		opinion.setEstimateNo(estimateNo);
		opinion.setOpinionContent(opinionContent);
		opinion.setId(id);
		
		/* 의견작성에 필요한 정보를 전달하고 결과를 전달받음 */
		return estimateDAO.insertOpinion(opinion);
	}

	/* 의견 삭제 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int deleteOpinion(String opinionNo) {

		return estimateDAO.deleteOpinion(opinionNo);
	}

}
