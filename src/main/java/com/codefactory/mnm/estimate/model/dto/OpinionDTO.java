package com.codefactory.mnm.estimate.model.dto;

public class OpinionDTO implements java.io.Serializable {

	private String opinionNo;				//의견번호
	private String opinionContent;			//의견내용
	private String name;					//작성자 이름
	private String writeDate;				//작성일자
	private String estimateNo;				//견적번호
	private String id;						//아이디
	private String userNo;					//사용자번호
	private String loginUserNo;				//로그인된 사용자번호
	
	public OpinionDTO () {}

	public OpinionDTO(String opinionNo, String opinionContent, String name, String writeDate, String estimateNo,
			String id, String userNo, String loginUserNo) {
		super();
		this.opinionNo = opinionNo;
		this.opinionContent = opinionContent;
		this.name = name;
		this.writeDate = writeDate;
		this.estimateNo = estimateNo;
		this.id = id;
		this.userNo = userNo;
		this.loginUserNo = loginUserNo;
	}

	public String getOpinionNo() {
		return opinionNo;
	}

	public String getOpinionContent() {
		return opinionContent;
	}

	public String getName() {
		return name;
	}

	public String getWriteDate() {
		return writeDate;
	}

	public String getEstimateNo() {
		return estimateNo;
	}

	public String getId() {
		return id;
	}

	public String getUserNo() {
		return userNo;
	}

	public String getLoginUserNo() {
		return loginUserNo;
	}

	public void setOpinionNo(String opinionNo) {
		this.opinionNo = opinionNo;
	}

	public void setOpinionContent(String opinionContent) {
		this.opinionContent = opinionContent;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setWriteDate(String writeDate) {
		this.writeDate = writeDate;
	}

	public void setEstimateNo(String estimateNo) {
		this.estimateNo = estimateNo;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public void setLoginUserNo(String loginUserNo) {
		this.loginUserNo = loginUserNo;
	}

	@Override
	public String toString() {
		return "OpinionDTO [opinionNo=" + opinionNo + ", opinionContent=" + opinionContent + ", name=" + name
				+ ", writeDate=" + writeDate + ", estimateNo=" + estimateNo + ", id=" + id + ", userNo=" + userNo
				+ ", loginUserNo=" + loginUserNo + "]";
	}

	
}
