package com.codefactory.mnm.estimate.model.service;

import java.util.List;
import java.util.Map;

import com.codefactory.mnm.estimate.model.dto.ApproverListDTO;
import com.codefactory.mnm.estimate.model.dto.BusinessOpportunityListDTO;
import com.codefactory.mnm.estimate.model.dto.ClientListDTO;
import com.codefactory.mnm.estimate.model.dto.CompanyListDTO;
import com.codefactory.mnm.estimate.model.dto.EstimateAttachmentDTO;
import com.codefactory.mnm.estimate.model.dto.EstimateInsertDTO;
import com.codefactory.mnm.estimate.model.dto.EstimateSearchDTO;
import com.codefactory.mnm.estimate.model.dto.ManagerListDTO;
import com.codefactory.mnm.estimate.model.dto.OpinionDTO;
import com.codefactory.mnm.estimate.model.dto.ProductListDTO;
import com.codefactory.mnm.estimate.model.dto.UserListDTO;

public interface EstimateService {

	List<CompanyListDTO> selectCompanyList();

	List<ClientListDTO> selectClientList();

	List<BusinessOpportunityListDTO> selectBusinessOpportunityList();

	List<UserListDTO> selectUserList();

	List<ProductListDTO> selectProductList();

	Map<String, Object> selectEstimateList(EstimateSearchDTO estimateSearchDTO);

	List<ApproverListDTO> selectApproverList();

	int insertEstimate(EstimateInsertDTO estimateInsertDTO);

	List<ManagerListDTO> selectManagerList(String deptName);

	Map<String, Object> selectEstimate(String estimateNo);

	int deleteEstimate(String estimateNo);

	EstimateAttachmentDTO selectFile(String fileNo);

	List<OpinionDTO> selectOpinionList(String estimateNo);

	int insertOpinion(String estimateNo, String opinionContent, String id);

	int deleteOpinion(String opinionNo);


}
