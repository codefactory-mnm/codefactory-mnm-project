package com.codefactory.mnm.estimate.model.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.codefactory.mnm.estimate.model.dto.ApprovalListDTO;
import com.codefactory.mnm.estimate.model.dto.ApproverListDTO;
import com.codefactory.mnm.estimate.model.dto.BusinessOpportunityListDTO;
import com.codefactory.mnm.estimate.model.dto.ClientListDTO;
import com.codefactory.mnm.estimate.model.dto.CompanyListDTO;
import com.codefactory.mnm.estimate.model.dto.DeptListDTO;
import com.codefactory.mnm.estimate.model.dto.EstimateAttachmentDTO;
import com.codefactory.mnm.estimate.model.dto.EstimateDTO;
import com.codefactory.mnm.estimate.model.dto.EstimateListDTO;
import com.codefactory.mnm.estimate.model.dto.EstimateSearchDTO;
import com.codefactory.mnm.estimate.model.dto.ManagerListDTO;
import com.codefactory.mnm.estimate.model.dto.OpinionDTO;
import com.codefactory.mnm.estimate.model.dto.ProductListDTO;
import com.codefactory.mnm.estimate.model.dto.UserListDTO;

@Mapper
public interface EstimateDAO {

	List<CompanyListDTO> selectCompanyList();

	List<ClientListDTO> selectClientList();

	List<BusinessOpportunityListDTO> selectBusinessOpportunityList();

	List<UserListDTO> selectUserList();

	List<ProductListDTO> selectProductList();

	List<EstimateListDTO> selectEstimateList(EstimateSearchDTO estimateSearchDTO);

	int selectEstimateListCount();

	List<ApproverListDTO> selectApproverList();
	
	int insertEstimate(EstimateDTO estimateDTO);

	int insertApproval(ApprovalListDTO approvalListDTO);

	int insertApprovalHistory();
	
	int insertEstimateAttachment(EstimateAttachmentDTO estimateAttachmentDTO);

	List<DeptListDTO> selectDeptList();

	List<ManagerListDTO> selectManagerList(String deptName);

	int selectTotalCount(EstimateSearchDTO estimateSearchDTO);

	int insertEstimateProduct(ProductListDTO productListDTO);

	EstimateListDTO selectEstimate(String estimateNo);

	String selectApprover(String estimateNo);

	int deleteEstimate(String estimateNo);

	List<EstimateListDTO> selectFileList(String estimateNo);

	List<ProductListDTO> selectEstimateProductList(String estimateNo);

	EstimateAttachmentDTO selectFile(String fileNo);

	List<OpinionDTO> selectOpinionList(String estimateNo);

	OpinionDTO selectUserNoAndName(String id);

	int insertOpinion(OpinionDTO opinion);

	int deleteOpinion(String opinionNo);




}
