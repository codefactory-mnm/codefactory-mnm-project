package com.codefactory.mnm.estimate.model.dto;

public class BusinessOpportunityListDTO implements java.io.Serializable {

	private String businessOpportunityNo;			//영업기회번호
	private String businessOpportunityName;			//영업기회명
	private String clientNo;						//고객번호
	private String clientName;						//고객명
	private String clientCompanyNo;					//고객사번호
	private String clientCompanyName;				//고객사명
	
	public BusinessOpportunityListDTO() {}

	public BusinessOpportunityListDTO(String businessOpportunityNo, String businessOpportunityName, String clientNo,
			String clientName, String clientCompanyNo, String clientCompanyName) {
		super();
		this.businessOpportunityNo = businessOpportunityNo;
		this.businessOpportunityName = businessOpportunityName;
		this.clientNo = clientNo;
		this.clientName = clientName;
		this.clientCompanyNo = clientCompanyNo;
		this.clientCompanyName = clientCompanyName;
	}

	public String getBusinessOpportunityNo() {
		return businessOpportunityNo;
	}

	public String getBusinessOpportunityName() {
		return businessOpportunityName;
	}

	public String getClientNo() {
		return clientNo;
	}

	public String getClientName() {
		return clientName;
	}

	public String getClientCompanyNo() {
		return clientCompanyNo;
	}

	public String getClientCompanyName() {
		return clientCompanyName;
	}

	public void setBusinessOpportunityNo(String businessOpportunityNo) {
		this.businessOpportunityNo = businessOpportunityNo;
	}

	public void setBusinessOpportunityName(String businessOpportunityName) {
		this.businessOpportunityName = businessOpportunityName;
	}

	public void setClientNo(String clientNo) {
		this.clientNo = clientNo;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public void setClientCompanyNo(String clientCompanyNo) {
		this.clientCompanyNo = clientCompanyNo;
	}

	public void setClientCompanyName(String clientCompanyName) {
		this.clientCompanyName = clientCompanyName;
	}

	@Override
	public String toString() {
		return "BusinessOpportunityListDTO [businessOpportunityNo=" + businessOpportunityNo
				+ ", businessOpportunityName=" + businessOpportunityName + ", clientNo=" + clientNo + ", clientName="
				+ clientName + ", clientCompanyNo=" + clientCompanyNo + ", clientCompanyName=" + clientCompanyName
				+ "]";
	}

	
}
