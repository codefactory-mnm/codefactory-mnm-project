package com.codefactory.mnm.estimate.model.dto;

public class UserListDTO implements java.io.Serializable {

	private String userNo;				//사용자번호
	private String name;				//사용자명
	private String deptName;			//부서
	private String position;			//직위
	
	public UserListDTO() {}

	public UserListDTO(String userNo, String name, String deptName, String position) {
		super();
		this.userNo = userNo;
		this.name = name;
		this.deptName = deptName;
		this.position = position;
	}

	public String getUserNo() {
		return userNo;
	}

	public String getName() {
		return name;
	}

	public String getDeptName() {
		return deptName;
	}

	public String getPosition() {
		return position;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	@Override
	public String toString() {
		return "UserListDTO [userNo=" + userNo + ", name=" + name + ", deptName=" + deptName + ", position=" + position
				+ "]";
	}

	
}
