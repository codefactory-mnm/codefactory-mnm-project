package com.codefactory.mnm.estimate.model.dto;

public class ManagerListDTO implements java.io.Serializable {

	private String userNo;					//담당자번호
	private String name;					//담당자명
	private String deptNo;					//부서번호
	private String deptName;				//부서명
	private String position;				//직위
	
	public ManagerListDTO() {}

	public ManagerListDTO(String userNo, String name, String deptNo, String deptName, String position) {
		super();
		this.userNo = userNo;
		this.name = name;
		this.deptNo = deptNo;
		this.deptName = deptName;
		this.position = position;
	}

	public String getUserNo() {
		return userNo;
	}

	public String getName() {
		return name;
	}

	public String getDeptNo() {
		return deptNo;
	}

	public String getDeptName() {
		return deptName;
	}

	public String getPosition() {
		return position;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDeptNo(String deptNo) {
		this.deptNo = deptNo;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	@Override
	public String toString() {
		return "ManagerListDTO [userNo=" + userNo + ", name=" + name + ", deptNo=" + deptNo + ", deptName=" + deptName
				+ ", position=" + position + "]";
	}

	
}
