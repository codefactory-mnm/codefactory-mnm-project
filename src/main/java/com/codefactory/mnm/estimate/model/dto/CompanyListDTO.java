package com.codefactory.mnm.estimate.model.dto;

import java.sql.Date;

public class CompanyListDTO implements java.io.Serializable {

	private String clientCompanyNo;				//고객사번호
	private String clientCompanyName;			//고객명
	private String division;					//구분
	private String address;						//주소
	private String detailedAddress;				//상세주소
	private java.sql.Date writeDate;			//등록일
	
	public CompanyListDTO() {}

	public CompanyListDTO(String clientCompanyNo, String clientCompanyName, String division, String address,
			String detailedAddress, Date writeDate) {
		super();
		this.clientCompanyNo = clientCompanyNo;
		this.clientCompanyName = clientCompanyName;
		this.division = division;
		this.address = address;
		this.detailedAddress = detailedAddress;
		this.writeDate = writeDate;
	}

	public String getClientCompanyNo() {
		return clientCompanyNo;
	}

	public String getClientCompanyName() {
		return clientCompanyName;
	}

	public String getDivision() {
		return division;
	}

	public String getAddress() {
		return address;
	}

	public String getDetailedAddress() {
		return detailedAddress;
	}

	public java.sql.Date getWriteDate() {
		return writeDate;
	}

	public void setClientCompanyNo(String clientCompanyNo) {
		this.clientCompanyNo = clientCompanyNo;
	}

	public void setClientCompanyName(String clientCompanyName) {
		this.clientCompanyName = clientCompanyName;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setDetailedAddress(String detailedAddress) {
		this.detailedAddress = detailedAddress;
	}

	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}

	@Override
	public String toString() {
		return "CompanyListDTO [clientCompanyNo=" + clientCompanyNo + ", clientCompanyName=" + clientCompanyName
				+ ", division=" + division + ", address=" + address + ", detailedAddress=" + detailedAddress
				+ ", writeDate=" + writeDate + "]";
	}

	
	
}
