package com.codefactory.mnm.estimate.model.dto;

import java.sql.Date;

public class ClientListDTO implements java.io.Serializable {

	private String clientNo;					//고객번호
	private String clientName;					//고객명
	private String clientCompanyNo;				//고객사번호
	private String clientCompanyName;			//고객명
	private java.sql.Date writeDate;			//작성일자
	
	public ClientListDTO() {}

	public ClientListDTO(String clientNo, String clientName, String clientCompanyNo, String clientCompanyName,
			Date writeDate) {
		super();
		this.clientNo = clientNo;
		this.clientName = clientName;
		this.clientCompanyNo = clientCompanyNo;
		this.clientCompanyName = clientCompanyName;
		this.writeDate = writeDate;
	}

	public String getClientNo() {
		return clientNo;
	}

	public String getClientName() {
		return clientName;
	}

	public String getClientCompanyNo() {
		return clientCompanyNo;
	}

	public String getClientCompanyName() {
		return clientCompanyName;
	}

	public java.sql.Date getWriteDate() {
		return writeDate;
	}

	public void setClientNo(String clientNo) {
		this.clientNo = clientNo;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public void setClientCompanyNo(String clientCompanyNo) {
		this.clientCompanyNo = clientCompanyNo;
	}

	public void setClientCompanyName(String clientCompanyName) {
		this.clientCompanyName = clientCompanyName;
	}

	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}

	@Override
	public String toString() {
		return "ClientListDTO [clientNo=" + clientNo + ", clientName=" + clientName + ", clientCompanyNo="
				+ clientCompanyNo + ", clientCompanyName=" + clientCompanyName + ", writeDate=" + writeDate + "]";
	}

	
}
