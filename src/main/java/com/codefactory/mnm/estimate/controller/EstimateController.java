package com.codefactory.mnm.estimate.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.estimate.model.dto.ApprovalListDTO;
import com.codefactory.mnm.estimate.model.dto.ApproverListDTO;
import com.codefactory.mnm.estimate.model.dto.BusinessOpportunityListDTO;
import com.codefactory.mnm.estimate.model.dto.ClientListDTO;
import com.codefactory.mnm.estimate.model.dto.CompanyListDTO;
import com.codefactory.mnm.estimate.model.dto.DeptListDTO;
import com.codefactory.mnm.estimate.model.dto.EstimateAttachmentDTO;
import com.codefactory.mnm.estimate.model.dto.EstimateDTO;
import com.codefactory.mnm.estimate.model.dto.EstimateInsertDTO;
import com.codefactory.mnm.estimate.model.dto.EstimateListDTO;
import com.codefactory.mnm.estimate.model.dto.EstimateSearchDTO;
import com.codefactory.mnm.estimate.model.dto.ManagerListDTO;
import com.codefactory.mnm.estimate.model.dto.OpinionDTO;
import com.codefactory.mnm.estimate.model.dto.ProductListDTO;
import com.codefactory.mnm.estimate.model.dto.UserListDTO;
import com.codefactory.mnm.estimate.model.service.EstimateService;
import com.codefactory.mnm.member.model.dto.UserImpl;
import com.codefactory.mnm.sales.model.dto.SalesProductDTO;

@Controller
@RequestMapping("/business")
public class EstimateController {
	
	private final EstimateService estimateService;
	
	@Autowired
	public EstimateController(EstimateService estimateService) {
		
		this.estimateService = estimateService;
	}
	
	/* 견적 목록 조회 (검색조건을 가져온다) */
	@GetMapping("/estimate/list")
	public ModelAndView selectEstimate(ModelAndView mv,
			@RequestParam(defaultValue = "") String division,
			@RequestParam(defaultValue = "") String searchName,
			@RequestParam(defaultValue = "") String deptName,
			@RequestParam(defaultValue = "") String name,
			@RequestParam(defaultValue = "1") String currentPage) {
		
		/* 처음 견적 리스트를 조회하거나 또는 검색조건을 설정하지 않았을 때를 대비 */
		if(division.equals("")) { // 검색조건 중 구분을 선택하지 않았을 경우
			division = null;
		}
		
		if(searchName.equals("")) { // 검색조건 중 고객사를 입력하지 않았을 경우
			searchName = null;
		}
		
		if(deptName.equals("")) { // 검색조건 중 부서를 선택하지 않았을 경우
			deptName = null;
		}
		
		if(name.equals("")) { // 검색조건 중 이름을 선택하지 않았을 경우
			name = null;
		}
		
		/* 검색조건을 담는다. 검색조건이 없을 경우 null로 담김. */
		EstimateSearchDTO estimateSearchDTO = new EstimateSearchDTO();
		estimateSearchDTO.setDivision(division);
		estimateSearchDTO.setSearchName(searchName);
		estimateSearchDTO.setDeptName(deptName);
		estimateSearchDTO.setName(name);
		estimateSearchDTO.setCurrentPage(currentPage);
		
		/* 검색조건을 searvice로 보내주고, 값을 map으로 전달받음 */
		Map<String, Object> map = estimateService.selectEstimateList(estimateSearchDTO);
		
		/* db에서 가져온 값을 꺼낸다. */
		List<EstimateListDTO> estimateList = (List<EstimateListDTO>) map.get("estimateList");
		List<DeptListDTO> deptList = (List<DeptListDTO>) map.get("deptList");
		SelectCriteria selectCriteria = (SelectCriteria) map.get("selectCriteria");
		int totalCount = (int) map.get("totalCount");
		
		/* db에서 조회해온 결과를 담는다 */
		mv.addObject("estimateList", estimateList);
		mv.addObject("deptList", deptList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("totalCount", totalCount);
		mv.addObject("estimateSearchDTO", estimateSearchDTO);
		mv.setViewName("estimate/list");
		
		return mv;
	}
	
	/* get방식 요청으로 insert.html로 페이지 연결 */
	@GetMapping("/estimate/insert")
	public String insertEstimate() {
		
		return "estimate/insert";
	}
	
	/* 견적 작성 
	 * 견적 등록에 필요한 정보 매개변수로 전달 받는다 */
	@PostMapping("/estimate/insert")
	public ModelAndView insertEstimate(ModelAndView mv,String[] productNo, int[] quantity2, int[] discount, int[] salePrice, int[] totalPrice,
			EstimateDTO estimateDTO, ApprovalListDTO approvalListDTO, 
			List<MultipartFile> multiFiles, HttpServletRequest request, RedirectAttributes rttr) {

		List<ProductListDTO> productList = new ArrayList<>();
		
		/* 견적 품목이 있을시 동작 */
		if(productNo.length > 0) {
			
			/* 견적 품목 갯수 만큼 동작 */
			for(int i =0; i < productNo.length; i++) {
				
				/* 견적품목을 DTO에 담는다 */
				ProductListDTO productListDTO = new ProductListDTO();
				productListDTO.setProductNo(productNo[i]);
				productListDTO.setQuantity2(quantity2[i]);
				productListDTO.setDiscount(discount[i]);
				productListDTO.setSalePrice(salePrice[i]);
				productListDTO.setTotalPrice(totalPrice[i]);
				
				/* 견적 품목을 담은 DTO를 리스트에 담는다 */
				productList.add(productListDTO);
			}
		}
		
		/* 견적 정보 입력과 동시에 파일첨부도 같이 등록한 경우 */
		if(multiFiles.size() > 0 && !multiFiles.get(0).getOriginalFilename().equals("")) {

			String root = request.getSession().getServletContext().getRealPath("/");	// 첨부파일 저장경로(절대경로)
			String filePath = root + "static/upload/";									// 첨부파일 저장경로(최종경로)

			/* 첨부파일 저장경로 (최종경로) 파일이 존재 하지 않을 경우 생성 */
			File mkdir = new File(filePath);
			if(!mkdir.exists()) {
				mkdir.mkdirs();
			}

			/* 다중 첨부파일이기 때문에, 여러 개의 파일을 담기 위해 List<EstimateAttachmentDTO> 타입으로 생성 */
			List<EstimateAttachmentDTO> files = new ArrayList<>();

			/* 다중 첨부파일은 반복문을 통해서, 미리 선언한 files에 담음  */
			for(int i = 0; i < multiFiles.size(); i++) {
				String fileOriginalName = multiFiles.get(i).getOriginalFilename();			// 원본파일명
				String ext = fileOriginalName.substring(fileOriginalName.lastIndexOf("."));	// 확장자명
				String fileName = UUID.randomUUID().toString().replace("-", "") + ext;		// 저장파일명

				/* 견적파일첨부DTO인 file에 정보를 담은 후 리스트인 files에 담음 */
				EstimateAttachmentDTO file = new EstimateAttachmentDTO();			
				file.setFileName(fileName);
				file.setFileOriginalName(fileOriginalName);
				file.setFilePath(filePath);

				files.add(file);
			}
			
			/* 견적 등록시 필요한 정보를 estimateInsertDTO에 담는다 */
			EstimateInsertDTO estimateInsertDTO = new EstimateInsertDTO();
			estimateInsertDTO.setEstimateDTO(estimateDTO);
			estimateInsertDTO.setApprovalListDTO(approvalListDTO);
			estimateInsertDTO.setFiles(files);
			estimateInsertDTO.setProductList(productList);
			
			/* 서비스로 estimateInsertDTO를 보낸다 */
			int result = estimateService.insertEstimate(estimateInsertDTO);
			
			/* DB에 등록을 성공하였을 경우 */
			if(result > 0 ) {
				
				/* 파일 저장 */
				try {
					for(int i = 0; i < multiFiles.size(); i++) {
						
						EstimateAttachmentDTO file = files.get(i);			
						multiFiles.get(i).transferTo(new File(filePath + "\\" + file.getFileName()));
						
					}
					
					rttr.addFlashAttribute("message", "견적 등록을 성공하였습니다.");
					
					/* 파일 저장을 실패하였을 경우, 저장했던 파일을 삭제
					 * 트랜젝션처리를 통해서 DB에 입력된 정보도 삭제됨 */
				} catch (Exception e) {
					e.printStackTrace();
					
					for(int i = 0; i < multiFiles.size(); i++) {
						
						EstimateAttachmentDTO file = files.get(i); 
						new File(filePath + "\\" + file.getFileName()).delete(); 
						
					}
					
					rttr.addFlashAttribute("message", "견적 등록 후 파일 업로드에 실패하였습니다.");
					
				}
			
			/* DB에 견적 등록을 실패하였을 경우 */
			} else {
				
				rttr.addFlashAttribute("message", "견적 등록을 실패하였습니다.");	
				
			}

		
		/* 파일첨부를 하지 않고, 견적 등록을 하는 경우 */
		} else {
			/* 견적 등록시 필요한 정보를 estimateInsertDTO에 담는다 */
			EstimateInsertDTO estimateInsertDTO = new EstimateInsertDTO();
			estimateInsertDTO.setEstimateDTO(estimateDTO);
			estimateInsertDTO.setApprovalListDTO(approvalListDTO);
			estimateInsertDTO.setProductList(productList);
			
			/* 서비스로 estimateInsertDTO를 전달한다 */
			int result = estimateService.insertEstimate(estimateInsertDTO);

			if(result > 0) {
				rttr.addFlashAttribute("message", "견적 등록을 성공하였습니다.");
				
			} else {
				rttr.addFlashAttribute("message", "견적 등록을 실패하였습니다.");
				
			}

		}
		
		/* 견적 등록 후 연결할 페이지 */
		mv.setViewName("redirect:/business/estimate/list");
		
		return mv;
	}
	
	/* 견적 상세조회 */
	@GetMapping("/estimate/detail")
	public ModelAndView selectEstimate(ModelAndView mv, @RequestParam String estimateNo) {
		
		/* Map을 생성하여 서비스에서 값을 리턴 받는다*/
		Map<String, Object> map = estimateService.selectEstimate(estimateNo);
		
		/* map에 담은 값을 꺼낸다 */
		EstimateListDTO oneEstimate = (EstimateListDTO) map.get("oneEstimate");
		String approver = (String) map.get("approver");
		List<EstimateListDTO> fileList = (List<EstimateListDTO>) map.get("fileList");
		List<ProductListDTO> productList = (List<ProductListDTO>) map.get("productList");
		
		/* map에서 꺼낸 값들을 담는다 */
		mv.addObject("oneEstimate", oneEstimate);
		mv.addObject("approver", approver);
		mv.addObject("fileList", fileList);
		mv.addObject("productList", productList);
		mv.setViewName("estimate/detail");
		
		return mv;
	}
	
	/* 견적 삭제 */
	@GetMapping("/estimate/delete")
	public ModelAndView deleteEstimate(ModelAndView mv, @RequestParam String estimateNo, RedirectAttributes rttr) {
		
		/* 견적 번호를 받아서 해당 게시글을 삭제 한다 */
		int result = estimateService.deleteEstimate(estimateNo);
		
		if(result > 0) {
			rttr.addFlashAttribute("message", "견적 삭제를 성공하였습니다.");
		} else {
			rttr.addFlashAttribute("message", "견적 삭제를 실패하였습니다.");
		}
		
		/* 삭제 후 연결할 페이지 */
		mv.setViewName("redirect:/business/estimate/list");
		
		return mv;
	}
	
	/* 견적 등록시 고객사 모달창에 띄울 정보 가져오기 */
	@GetMapping(value="/estimate/company", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<CompanyListDTO> selectCompanyList(Model model) {
		
		List<CompanyListDTO> companyList = estimateService.selectCompanyList();
		
		model.addAttribute("companyList", companyList);
		
		return companyList;
	}
	
	/* 견적 등록시 고객 모달창에 띄울 정보 가져오기 */
	@GetMapping(value="/estimate/client", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<ClientListDTO> selectClientList(Model model) {
		
		List<ClientListDTO> clientList = estimateService.selectClientList();
		
		model.addAttribute("clientList", clientList);
		
		return clientList;
	}
	
	/* 견적 등록시 영업기회 모달창에 띄울 정보 가져오기 */
	@GetMapping(value="/estimate/businessOpportunity", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<BusinessOpportunityListDTO> selectBusinessOpportunityList(Model model) {
		
		List<BusinessOpportunityListDTO> businessOpportunityList = estimateService.selectBusinessOpportunityList();
		
		model.addAttribute("businessOpportunityList", businessOpportunityList);
		
		return businessOpportunityList;
	}
	
	/* 견적 등록시 담당자 모달창에 띄울 정보 가져오기 */
	@GetMapping(value="/estimate/user", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<UserListDTO> selectUserList(Model model) {
		
		List<UserListDTO> userList = estimateService.selectUserList();
		
		model.addAttribute("userList", userList);
		
		return userList;
	}
	
	/* 견적 등록시 견적 품목 모달창에 띄울 정보 가져오기 */
	@GetMapping(value="/estimate/product", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<ProductListDTO> selectProductList(Model model) {
		
		List<ProductListDTO> productList = estimateService.selectProductList();
		
		model.addAttribute("productList", productList);
		
		return productList;
	}
	
	/* 견적 등록시 결재권자 모달창에 띄울 정보 가져오기 */
	@GetMapping(value="/estimate/approver", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<ApproverListDTO> selectApproverList(Model model) {
		
		List<ApproverListDTO> approverList = estimateService.selectApproverList();
		
		model.addAttribute("approverList", approverList);
		
		return approverList;
	}
	
	/* 견적 목록에서 담당자 모달창에 띄울 정보 가져오기 */
	@GetMapping(value="/estimate/manager", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<ManagerListDTO> selectManagerList(Model model, @RequestParam String deptName) {
		
		List<ManagerListDTO> managerList = estimateService.selectManagerList(deptName);
		
		model.addAttribute("managerList", managerList);
		
		return managerList;
	}
	
	/* 첨부파일 다운로드 */
	@GetMapping("/estimate/download")
	@ResponseBody
	public void download(HttpServletResponse response, String fileNo) throws IOException {
		
		/* 전달받은 fileNo로 파일 정보 조회 */
		EstimateAttachmentDTO estimateAttachmentDTO = estimateService.selectFile(fileNo);
		String fileOriginalName = estimateAttachmentDTO.getFileOriginalName();
		String fileName = estimateAttachmentDTO.getFileName();
		String filePath = estimateAttachmentDTO.getFilePath();
		
		String saveFileName = filePath + fileName;
		
		File file = new File(saveFileName);
		long fileLength = file.length();
		
		/* 파일 정보들을 가지고 reponse의 Header에 세팅 */
		response.setHeader("Content-Disposition", "attachment; filename=\"" + URLEncoder.encode(fileOriginalName,"UTF-8") + "\";");
        response.setHeader("Content-Transfer-Encoding", "binary");
        response.setHeader("Content-Type", "application/octet-stream");
        response.setHeader("Content-Length", "" + fileLength);
        response.setHeader("Pragma", "no-cache;");
        response.setHeader("Expires", "-1;");

		/* saveFileName을 파라미터로 넣어 inputStream 객체를 만들고 
        response에서 파일을 내보낼 OutputStream을 가져와서 */
        FileInputStream fis = new FileInputStream(saveFileName);
        OutputStream out = response.getOutputStream();
        
    	/* 파일 읽을 만큼 크기의 buffer를 생성한 후 */
        int readCount = 0;
        byte[] buffer = new byte[1024];
        
        /* outputStream에 씌워준다 */
        while((readCount = fis.read(buffer)) != -1){
                out.write(buffer,0,readCount);
        }
        /* 스트림 close */
        fis.close();
        out.close();
        
	}
	
	/* 견적 상세 조회 시 ajax를 통해 의견 조회 */
	@GetMapping(value="/estimate/opinion/select", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<OpinionDTO> selectOpinionList(String estimateNo, @AuthenticationPrincipal UserImpl customUser) {
		
		/* 상세조회할 견적번호를 service로 전달하고 의견리스트를 전달받음 */
		List<OpinionDTO> selectOpinionList = estimateService.selectOpinionList(estimateNo);
		
		/* 의견삭제시 작성자와 로그인된사용자가 같은지 확인하기 위해서 사용자번호를 담음 */
		for(int i = 0; i < selectOpinionList.size(); i++) {

			selectOpinionList.get(i).setLoginUserNo(customUser.getUserNo());

		}

		/* 화면으로 전달 */
		return selectOpinionList;

	}
	
	/* ajax를 통해 의견 작성 */
	@PostMapping(value="/estimate/opinion/insert", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int insertOpinion(String estimateNo, String opinionContent, Principal pcp) {

		String id = pcp.getName();	//세션에 담겨있는 사용자 아이디

		/* 견적번호, 의견내용, 사용자아이디를 전달하고 결과를 전달 받음 */
		int result = estimateService.insertOpinion(estimateNo, opinionContent, id);

		/* 화면으로 전달 */
		return result;

	}
	
	/* ajax를 통해 의견 삭제 */
	@PostMapping(value="/estimate/opinion/delete", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int deleteOpinion(String opinionNo) {

		/* 의견번호를 전달하고 결과를 전달 받음 */
		int result = estimateService.deleteOpinion(opinionNo);		

		/* 화면으로 전달 */
		return result;

	}
	
}
