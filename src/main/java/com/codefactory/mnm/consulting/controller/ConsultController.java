package com.codefactory.mnm.consulting.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.consulting.model.dto.ConsultDTO;
import com.codefactory.mnm.consulting.model.dto.ConsultSearchDTO;
import com.codefactory.mnm.consulting.model.service.ConsultService;
import com.codefactory.mnm.estimate.model.dto.DeptListDTO;

@Controller
@RequestMapping("/consult")
public class ConsultController {
   
   private final ConsultService consultService;
   
   @Autowired
   public ConsultController(ConsultService consultService) {
      
      this.consultService = consultService;
   }

   /* 상담 목록 조회 */
   @GetMapping("/list")
   public ModelAndView selectConsultList(ModelAndView mv,
         @RequestParam(defaultValue = "") String division,
         @RequestParam(defaultValue = "") String searchName,
         @RequestParam(defaultValue = "") String deptName,
         @RequestParam(defaultValue = "") String name,
         @RequestParam(defaultValue = "1") String currentPage) {
      
      System.out.println("division : " + division);
      System.out.println("searchName : " + searchName);
      System.out.println("deptName : " + deptName);
      System.out.println("name : " + name);
      System.out.println("currentPage : " + currentPage);
      
      /* 처음 매출 리스트를 조회하거나 또는 검색조건을 설정하지 않았을 때를 대비 */
      if(division.equals("")) { // 검색조건 중 구분을 선택하지 않았을 경우
         division = null;
      }
      
      if(searchName.equals("")) { // 검색조건 중 고객사를 입력하지 않았을 경우
         searchName = null;
      }
      
      if(deptName.equals("")) { // 검색조건 중 부서를 선택하지 않았을 경우
         deptName = null;
      }
      
      if(name.equals("")) { // 검색조건 중 이름을 선택하지 않았을 경우
         name = null;
      }
      
      /* 검색조건을 담는다. 검색조건이 없을 경우 null로 담김. */
      ConsultSearchDTO consultSearchDTO = new ConsultSearchDTO();
      consultSearchDTO.setDivision(division);
      consultSearchDTO.setSearchName(searchName);
      consultSearchDTO.setDeptName(deptName);
      consultSearchDTO.setName(name);
      consultSearchDTO.setCurrentPage(currentPage);
      
      System.out.println("consultSearchDTO : " + consultSearchDTO);
      
      /* 검색조건을 service로 보내주고, 값을 map으로 전달받음 */
      Map<String, Object> map = consultService.selectConsultList(consultSearchDTO);
      
      /* db에서 가져온 값을 꺼낸다. */
      List<ConsultDTO> consultList = (List<ConsultDTO>) map.get("consultList");
      List<DeptListDTO> deptList = (List<DeptListDTO>) map.get("deptList");
      SelectCriteria selectCriteria = (SelectCriteria) map.get("selectCriteria");
      int totalCount = (int) map.get("totalCount");
      System.out.println("consultList : " + consultList);
      System.out.println("deptList : " + deptList);
      
      /* db에서 조회해온 결과를 담는다 */
      mv.addObject("consultList", consultList);
      mv.addObject("deptList", deptList);
      mv.addObject("selectCriteria", selectCriteria);
      mv.addObject("totalCount", totalCount);
      mv.addObject("consultSearchDTO", consultSearchDTO);
      mv.setViewName("consulting/list");
      
      return mv;
   }
   
   /* 상담 등록 페이지 연결 */
   @GetMapping("/insert")
   public String insertConsult() {
      
      return "consulting/insert";
   }
   
   /* 상담 등록 */
   @PostMapping("/insert")
   public ModelAndView insertConsult(ModelAndView mv, ConsultDTO consultDTO, HttpServletRequest request, RedirectAttributes rttr) {

      System.out.println("consultDTO : " + consultDTO); 
      
      int result = consultService.insertConsult(consultDTO);
      
      if(result > 0) {
         rttr.addFlashAttribute("message", "상담 등록을 성공하였습니다.");
      } else {
         rttr.addFlashAttribute("message", "상담 등록을 실패하였습니다.");
      }
      
      /* 상담 등록 후 연결할 페이지 */
      mv.setViewName("redirect:/consult/list");
      
      return mv;
   }
   
   /* 상담 상세조회 */
   @GetMapping("/detail")
   public ModelAndView selectConsult(ModelAndView mv, @RequestParam String consultingNo) {
      
      System.out.println("consultingNo : " + consultingNo);
      
      /* Map을 생성하여 서비스에서 값을 리턴 받는다*/
      ConsultDTO consultDTO = consultService.selectConsult(consultingNo);
      
      /* map에서 꺼낸 값들을 담는다 */
      mv.addObject("consultDTO", consultDTO);
      mv.setViewName("consulting/detail");
      
      return mv;
   }
   
   /* 상담 삭제 */
   @GetMapping("/delete")
   public ModelAndView deleteConsult(ModelAndView mv, @RequestParam String consultingNo, RedirectAttributes rttr) {

      System.out.println("consultingNo : " + consultingNo);

      /* 상담 번호를 받아서 해당 게시글을 삭제 한다 */
      int result = consultService.deleteConsult(consultingNo);

      if(result > 0) {
         rttr.addFlashAttribute("message", "상담 삭제를 성공하였습니다.");
      } else {
         rttr.addFlashAttribute("message", "상담 삭제를 실패하였습니다.");
      }

      /* 삭제 후 연결할 페이지 */
      mv.setViewName("redirect:/consult/list");

      return mv;
   }
   
   /* 상담 수정 페이지로 이동 */
   @GetMapping("/update")
   public ModelAndView updateConsultPage(ModelAndView mv, @RequestParam String consultingNo) {

      System.out.println("consultingNo : " + consultingNo);
      
      /* Map을 생성하여 서비스에서 값을 리턴 받는다*/
      ConsultDTO consultDTO = consultService.selectConsult(consultingNo);
      
      /* map에서 꺼낸 값들을 담는다 */
      mv.addObject("consultDTO", consultDTO);
      mv.setViewName("consulting/update");
      
      return mv;
   }
   
   @PostMapping("/update")
   public ModelAndView updateConsult(ModelAndView mv, ConsultDTO consultDTO, RedirectAttributes rttr) {
      
      System.out.println("consultDTO : " + consultDTO); 
      
      int result = consultService.updateConsult(consultDTO);
      
      if(result > 0) {
         rttr.addFlashAttribute("message", "상담 수정을 성공하였습니다.");
      } else {
         rttr.addFlashAttribute("message", "상담 수정을 실패하였습니다.");
      }
      
      /* 상담 수정 후 연결할 페이지 */
      mv.setViewName("redirect:/consult/list");
      
      return mv;
   }
   
}