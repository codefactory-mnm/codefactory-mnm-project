package com.codefactory.mnm.consulting.model.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.codefactory.mnm.common.Pagenation;
import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.consulting.model.dao.ConsultDAO;
import com.codefactory.mnm.consulting.model.dto.ConsultDTO;
import com.codefactory.mnm.consulting.model.dto.ConsultSearchDTO;
import com.codefactory.mnm.estimate.model.dto.DeptListDTO;

@Service
public class ConsultServiceImpl implements ConsultService {

	private final ConsultDAO consultDAO;
	
	@Autowired
	public ConsultServiceImpl(ConsultDAO consultDAO) {
		
		this.consultDAO = consultDAO;
	}
	
	/* 상담 조회 */
	@Override
	public Map<String, Object> selectConsultList(ConsultSearchDTO consultSearchDTO) {
		
		/* 첫 요청페이지 */
		int pageNo = 1;		
		
		/* 요청 페이지가 0이하면 1로 바꿔줌 */
		if(pageNo <= 0) {
			pageNo = 1;
		}
		
		/* 최대 페이지 */
		int limit = 10;
		
		/* 최대 버튼 갯수 */
		int buttonAmount = 5;
		
		/* 전체 게시물 수, 검색조건이 있다면 검색조건에 맞는 게시물 수 */
		int totalCount = consultDAO.selectTotalCount(consultSearchDTO);

		/* view에서 전달받은 요청페이지 */
		String currentPage = consultSearchDTO.getCurrentPage();
		
		/* view에서 전달받은 요청페이지를 pageNo에 저장 */
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		/* 설정한 변수에 따른 페이징조건 */
		SelectCriteria selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount);
				
		/* 조회 결과를 담을 Map 생성 */
		Map<String, Object> map = new HashMap<>();
		
		consultSearchDTO.setSelectCriteria(selectCriteria);
		
		/* 상담 목록 조회 */
		List<ConsultDTO> consultList = consultDAO.selectConsultList(consultSearchDTO);
		
		/* 부서 목록 조회 */
		List<DeptListDTO> deptList = consultDAO.selectDeptList();
		
		map.put("totalCount", totalCount);
		map.put("selectCriteria", selectCriteria);
		map.put("consultList", consultList);
		map.put("deptList", deptList);
		
		return map;
	}

	/* 상담 등록 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertConsult(ConsultDTO consultDTO) {
		
		int result1 = consultDAO.insertConsult(consultDTO);
		int result2 = consultDAO.insertConsultHistory(consultDTO);
		
		int result = 0;
		
		if (result1 == 1 && result2 == 1) {
			result = 1;
		}
		
		return result;
	}

	/* 상담 상세조회 */
	@Override
	public ConsultDTO selectConsult(String consultingNo) {
		
		ConsultDTO consultDTO = consultDAO.selectConsult(consultingNo);
		
		return consultDTO;
	}

	/* 상담 삭제 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int deleteConsult(String consultingNo) {
		
		int result = consultDAO.deleteConsult(consultingNo);
		
		return result;
	}

	/* 상담 수정 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int updateConsult(ConsultDTO consultDTO) {
		
		int result1 = consultDAO.updateConsult(consultDTO);
		int result2 = consultDAO.insertConsultHistoryByUpdate(consultDTO);
		
		int result = 0;
		
		if (result1 == 1 && result2 == 1) {
			result = 1;
		}
		
		return result;
	}
	
}
