package com.codefactory.mnm.consulting.model.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.codefactory.mnm.consulting.model.dto.ConsultDTO;
import com.codefactory.mnm.consulting.model.dto.ConsultSearchDTO;
import com.codefactory.mnm.estimate.model.dto.DeptListDTO;

@Mapper
public interface ConsultDAO {

	/* 전체 게시물 수, 검색조건이 있다면 검색조건에 맞는 게시물 수 */
	int selectTotalCount(ConsultSearchDTO consultSearchDTO);

	/* 부서 목록 조회 */
	List<DeptListDTO> selectDeptList();

	/* 상담 목록 조회 */
	List<ConsultDTO> selectConsultList(ConsultSearchDTO consultSearchDTO);

	int insertConsult(ConsultDTO consultDTO);

	int insertConsultHistory(ConsultDTO consultDTO);

	ConsultDTO selectConsult(String consultingNo);

	int deleteConsult(String consultingNo);

	int updateConsult(ConsultDTO consultDTO);

	int insertConsultHistoryByUpdate(ConsultDTO consultDTO);

}
