package com.codefactory.mnm.consulting.model.dto;

import java.sql.Date;

public class ConsultDTO {
	
	private String consultingNo;			//상담번호
	private String title;					//상담제목
	private java.sql.Date consultingDate; 	//상담일자
	private String consultingStatus;		//상담상태
	private String consultingWay;			//상담방법
	private String consultingContent;		//상담내용
	private String userNo;					//담당자번호
	private String name;					//담당자 이름
	private String clientNo;				//고객번호
	private String clientName;				//고객이름
	private java.sql.Date writeDate;		//작성일자
	private String delYn;					//삭제여부
	
	public ConsultDTO() {}

	public ConsultDTO(String consultingNo, String title, Date consultingDate, String consultingStatus,
			String consultingWay, String consultingContent, String userNo, String name, String clientNo,
			String clientName, Date writeDate, String delYn) {
		super();
		this.consultingNo = consultingNo;
		this.title = title;
		this.consultingDate = consultingDate;
		this.consultingStatus = consultingStatus;
		this.consultingWay = consultingWay;
		this.consultingContent = consultingContent;
		this.userNo = userNo;
		this.name = name;
		this.clientNo = clientNo;
		this.clientName = clientName;
		this.writeDate = writeDate;
		this.delYn = delYn;
	}

	public String getConsultingNo() {
		return consultingNo;
	}

	public String getTitle() {
		return title;
	}

	public java.sql.Date getConsultingDate() {
		return consultingDate;
	}

	public String getConsultingStatus() {
		return consultingStatus;
	}

	public String getConsultingWay() {
		return consultingWay;
	}

	public String getConsultingContent() {
		return consultingContent;
	}

	public String getUserNo() {
		return userNo;
	}

	public String getName() {
		return name;
	}

	public String getClientNo() {
		return clientNo;
	}

	public String getClientName() {
		return clientName;
	}

	public java.sql.Date getWriteDate() {
		return writeDate;
	}

	public String getDelYn() {
		return delYn;
	}

	public void setConsultingNo(String consultingNo) {
		this.consultingNo = consultingNo;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setConsultingDate(java.sql.Date consultingDate) {
		this.consultingDate = consultingDate;
	}

	public void setConsultingStatus(String consultingStatus) {
		this.consultingStatus = consultingStatus;
	}

	public void setConsultingWay(String consultingWay) {
		this.consultingWay = consultingWay;
	}

	public void setConsultingContent(String consultingContent) {
		this.consultingContent = consultingContent;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setClientNo(String clientNo) {
		this.clientNo = clientNo;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}

	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}

	@Override
	public String toString() {
		return "ConsultDTO [consultingNo=" + consultingNo + ", title=" + title + ", consultingDate=" + consultingDate
				+ ", consultingStatus=" + consultingStatus + ", consultingWay=" + consultingWay + ", consultingContent="
				+ consultingContent + ", userNo=" + userNo + ", name=" + name + ", clientNo=" + clientNo
				+ ", clientName=" + clientName + ", writeDate=" + writeDate + ", delYn=" + delYn + "]";
	}

	
	
}
