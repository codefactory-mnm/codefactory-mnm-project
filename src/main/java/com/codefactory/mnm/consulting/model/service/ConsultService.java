package com.codefactory.mnm.consulting.model.service;

import java.util.Map;

import com.codefactory.mnm.consulting.model.dto.ConsultDTO;
import com.codefactory.mnm.consulting.model.dto.ConsultSearchDTO;

public interface ConsultService {

	Map<String, Object> selectConsultList(ConsultSearchDTO consultSearchDTO);

	int insertConsult(ConsultDTO consultDTO);

	ConsultDTO selectConsult(String consultingNo);

	int deleteConsult(String consultingNo);

	int updateConsult(ConsultDTO consultDTO);

}
