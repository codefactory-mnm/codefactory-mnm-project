package com.codefactory.mnm.sales.model.service;

import java.util.List;
import java.util.Map;

import com.codefactory.mnm.contract.model.dto.OpinionDTO;
import com.codefactory.mnm.sales.model.dto.ContractListDTO;
import com.codefactory.mnm.sales.model.dto.SalesAttachmentDTO;
import com.codefactory.mnm.sales.model.dto.SalesInsertDTO;
import com.codefactory.mnm.sales.model.dto.SalesProductDTO;
import com.codefactory.mnm.sales.model.dto.SalesSearchDTO;

public interface SalesService {

	Map<String, Object> selectSalesList(SalesSearchDTO salesSearchDTO);

	List<ContractListDTO> selectContractList();

	int insertSales(SalesInsertDTO salesInsertDTO);

	Map<String, Object> selectSales(String salesNo);

	int deleteSales(String salesNo);

	SalesAttachmentDTO selectFile(String fileNo);

	Map<String, Object> deleteFile(String fileNo);

	int updateSales(SalesInsertDTO salesInsertDTO);


}
