package com.codefactory.mnm.sales.model.dto;

import java.sql.Date;
import java.util.List;

public class SalesProductDTO implements java.io.Serializable {

	private String productNo;							//상품번호
	private String productName;							//상품명
	private String price;								//단가
	private int quantity2;								//수량
	private String productSupplyPrice;					//공급가액
	private String salesCycle;							//매출주기
	private java.sql.Date productDeliverydate;			//납품예정일
	private String productNote;							//비고
	private String sumPrice;							//단가합계
	private String sumQuantity;							//총수량
	private String sumTotalPrice;						//공급가액합계
	private String salesNo;								//매출 번호
	private List<SalesProductDTO> productList;
	
	public SalesProductDTO() {}

	public SalesProductDTO(String productNo, String productName, String price, int quantity2, String productSupplyPrice,
			String salesCycle, Date productDeliverydate, String productNote, String sumPrice, String sumQuantity,
			String sumTotalPrice, String salesNo, List<SalesProductDTO> productList) {
		super();
		this.productNo = productNo;
		this.productName = productName;
		this.price = price;
		this.quantity2 = quantity2;
		this.productSupplyPrice = productSupplyPrice;
		this.salesCycle = salesCycle;
		this.productDeliverydate = productDeliverydate;
		this.productNote = productNote;
		this.sumPrice = sumPrice;
		this.sumQuantity = sumQuantity;
		this.sumTotalPrice = sumTotalPrice;
		this.salesNo = salesNo;
		this.productList = productList;
	}

	public String getProductNo() {
		return productNo;
	}

	public String getProductName() {
		return productName;
	}

	public String getPrice() {
		return price;
	}

	public int getQuantity2() {
		return quantity2;
	}

	public String getProductSupplyPrice() {
		return productSupplyPrice;
	}

	public String getSalesCycle() {
		return salesCycle;
	}

	public java.sql.Date getProductDeliverydate() {
		return productDeliverydate;
	}

	public String getProductNote() {
		return productNote;
	}

	public String getSumPrice() {
		return sumPrice;
	}

	public String getSumQuantity() {
		return sumQuantity;
	}

	public String getSumTotalPrice() {
		return sumTotalPrice;
	}

	public String getSalesNo() {
		return salesNo;
	}

	public List<SalesProductDTO> getProductList() {
		return productList;
	}

	public void setProductNo(String productNo) {
		this.productNo = productNo;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public void setQuantity2(int quantity2) {
		this.quantity2 = quantity2;
	}

	public void setProductSupplyPrice(String productSupplyPrice) {
		this.productSupplyPrice = productSupplyPrice;
	}

	public void setSalesCycle(String salesCycle) {
		this.salesCycle = salesCycle;
	}

	public void setProductDeliverydate(java.sql.Date productDeliverydate) {
		this.productDeliverydate = productDeliverydate;
	}

	public void setProductNote(String productNote) {
		this.productNote = productNote;
	}

	public void setSumPrice(String sumPrice) {
		this.sumPrice = sumPrice;
	}

	public void setSumQuantity(String sumQuantity) {
		this.sumQuantity = sumQuantity;
	}

	public void setSumTotalPrice(String sumTotalPrice) {
		this.sumTotalPrice = sumTotalPrice;
	}

	public void setSalesNo(String salesNo) {
		this.salesNo = salesNo;
	}

	public void setProductList(List<SalesProductDTO> productList) {
		this.productList = productList;
	}

	@Override
	public String toString() {
		return "SalesProductDTO [productNo=" + productNo + ", productName=" + productName + ", price=" + price
				+ ", quantity2=" + quantity2 + ", productSupplyPrice=" + productSupplyPrice + ", salesCycle="
				+ salesCycle + ", productDeliverydate=" + productDeliverydate + ", productNote=" + productNote
				+ ", sumPrice=" + sumPrice + ", sumQuantity=" + sumQuantity + ", sumTotalPrice=" + sumTotalPrice
				+ ", salesNo=" + salesNo + ", productList=" + productList + "]";
	}

	

	

	
}
