package com.codefactory.mnm.sales.model.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.codefactory.mnm.common.Pagenation;
import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.contract.model.dto.ContractProductDTO;
import com.codefactory.mnm.estimate.model.dto.DeptListDTO;
import com.codefactory.mnm.proposal.model.dto.ProposalAttachmentDTO;
import com.codefactory.mnm.proposal.model.dto.ProposalDTO;
import com.codefactory.mnm.sales.model.dao.SalesDAO;
import com.codefactory.mnm.sales.model.dto.ContractListDTO;
import com.codefactory.mnm.sales.model.dto.SalesAttachmentDTO;
import com.codefactory.mnm.sales.model.dto.SalesDTO;
import com.codefactory.mnm.sales.model.dto.SalesInsertDTO;
import com.codefactory.mnm.sales.model.dto.SalesProductDTO;
import com.codefactory.mnm.sales.model.dto.SalesSearchDTO;

@Service
public class SalesServiceImpl implements SalesService {

	private final SalesDAO salesDAO;
	
	@Autowired
	public SalesServiceImpl(SalesDAO salesDAO) {
		
		this.salesDAO = salesDAO;
	}
	
	/* 매출 조회 */
	@Override
	public Map<String, Object> selectSalesList(SalesSearchDTO salesSearchDTO) {
		
		/* 첫 요청페이지 */
		int pageNo = 1;		
		
		/* 요청 페이지가 0이하면 1로 바꿔줌 */
		if(pageNo <= 0) {
			pageNo = 1;
		}
		
		/* 최대 페이지 */
		int limit = 10;
		
		/* 최대 버튼 갯수 */
		int buttonAmount = 5;
		
		/* 전체 게시물 수, 검색조건이 있다면 검색조건에 맞는 게시물 수 */
		int totalCount = salesDAO.selectTotalCount(salesSearchDTO);

		/* view에서 전달받은 요청페이지 */
		String currentPage = salesSearchDTO.getCurrentPage();
		
		/* view에서 전달받은 요청페이지를 pageNo에 저장 */
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		/* 설정한 변수에 따른 페이징조건 */
		SelectCriteria selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount);
				
		/* 조회 결과를 담을 Map 생성 */
		Map<String, Object> map = new HashMap<>();
		
		salesSearchDTO.setSelectCriteria(selectCriteria);
		
		/* 매출 목록 조회 */
		List<SalesDTO> salesList = salesDAO.selectSalesList(salesSearchDTO);
		
		/* 부서 목록 조회 */
		List<DeptListDTO> deptList = salesDAO.selectDeptList();
		
		map.put("totalCount", totalCount);
		map.put("selectCriteria", selectCriteria);
		map.put("salesList", salesList);
		map.put("deptList", deptList);
		
		return map;
	}

	/* 계약 등록 페이지에서 견적 모달 정보 조회 */
	@Override
	public List<ContractListDTO> selectContractList() {

		return salesDAO.selectContractList();
	}

	/* 매출 등록 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertSales(SalesInsertDTO salesInsertDTO) {

		/* controller에서 받아온 정보를 꺼낸다. */
		SalesDTO salesDTO = salesInsertDTO.getSalesDTO();
		List<SalesAttachmentDTO> files = salesInsertDTO.getFiles();
		List<SalesProductDTO> salesProductLists = salesInsertDTO.getSalesProductList();
		
		/* 매출 작성내용을 DB에 저장한다. */
		int result1 = salesDAO.insertSales(salesDTO);
		
		int result2 = 0;
		/* 매출 품목을 등록한다 */
		for(int i = 0; i < salesProductLists.size(); i++) {
			
			SalesProductDTO salesProductList = salesProductLists.get(i);
			result2 = salesDAO.insertSalesProduct(salesProductList);
		}
		
		/* 파일첨부정보를 DB에 저장한 횟수를 저장한다. */
		int result3 = 0;	
		if(files != null) {
			for(SalesAttachmentDTO salesAttachmentDTO : files) {
				result3 += salesDAO.insertSalesAttachment(salesAttachmentDTO);
			}	
		}
		
		/* 파일첨부가 없는 경우 : 계약, 결재를 정상적으로 등록하였을 경우 result = 1을 반환
		 * 파일첨부가 있는 경우 : 계약, 결재, 첨부파일을 정상적으로 등록하였을 경우 result = 1을 반환
		 */
		int result = 0;
		if(result1 > 0 && result2 > 0 && (files == null || result3 == files.size())) {
			result = 1;
		}
		
		return result;
	}

	/* 매출 상세조회 */
	@Override
	public Map<String, Object> selectSales(String salesNo) {

		Map<String, Object> map = new HashMap<>();
		
		SalesDTO oneSales = salesDAO.selectSales(salesNo);
		List<SalesAttachmentDTO> fileList = salesDAO.selectFileList(salesNo);
		List<SalesProductDTO> productList = salesDAO.selectSalesProductList(salesNo);
		
		map.put("oneSales", oneSales);
		map.put("fileList", fileList);
		map.put("productList", productList);
		
		return map;
	}

	/* 매출 삭제 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int deleteSales(String salesNo) {

		return salesDAO.deleteSales(salesNo);
	}

	/* 매출 첨부파일 조회 */
	@Override
	public SalesAttachmentDTO selectFile(String fileNo) {

		return salesDAO.selectFile(fileNo);
	}

	/* 매출 첨부파일 삭제 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public Map<String, Object> deleteFile(String fileNo) {

		Map<String, Object> map = new HashMap<>();
		
		SalesAttachmentDTO file = salesDAO.selectFile(fileNo);			//첨부파일정보				
		int result = salesDAO.deleteFile(fileNo);						//첨부파일 DB에서 삭제 결과
		
		/* map에 담아서 controller에 전달 */
		map.put("file", file);
		map.put("result", result);
		
		return map;
	}

	/* 매출 수정 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int updateSales(SalesInsertDTO salesInsertDTO) {

		/* controller에서 받아온 정보를 꺼낸다. */
		SalesDTO salesDTO = salesInsertDTO.getSalesDTO();
		List<SalesAttachmentDTO> files = salesInsertDTO.getFiles();
		List<SalesProductDTO> salesProductLists = salesInsertDTO.getSalesProductList();
		
		/* 매출 품목을 삭제한다 */
		int result1 = salesDAO.deleteSalesProduct(salesDTO);
		
		/* 제안 작성내용을 DB에 저장한다. */
		int result2 = salesDAO.updateSales(salesDTO);
		
		String salseNo = salesDTO.getSalesNo();
		
		int result3 = 0;
		/* 매출 품목을 등록한다 */
		for(int i = 0; i < salesProductLists.size(); i++) {
			
			SalesProductDTO salesProductDTO = salesProductLists.get(i);
			salesProductDTO.setSalesNo(salseNo);
			result3 = salesDAO.updateSalesProduct(salesProductDTO);
		}
		String salesNo = salesDTO.getSalesNo();
		
		/* 파일첨부정보를 DB에 저장한 횟수를 저장한다. */
		int result4 = 0;	
		if(files != null) {
			for(SalesAttachmentDTO salesAttachmentDTO : files) {
				salesAttachmentDTO.setSalesNo(salesNo);
				result4 += salesDAO.updateSalesAttachment(salesAttachmentDTO);
			}	
		}
		
		/* 파일첨부가 없는 경우 : 제안을 정상적으로 등록하였을 경우 result = 1을 반환
		 * 파일첨부가 있는 경우 : 제안, 첨부파일을 정상적으로 등록하였을 경우 result = 1을 반환
		 */
		System.out.println("result1 : " + result1);
		System.out.println("result2 : " + result2);
		System.out.println("result3 : " + result3);
		System.out.println("result3 : " + result4);
		int result = 0;
		if(result1 == 1  && result2 == 1 && result3 == 1 && (files == null || result4 == files.size())) {
			result = 1;
		}
		
		return result;
	}

}
