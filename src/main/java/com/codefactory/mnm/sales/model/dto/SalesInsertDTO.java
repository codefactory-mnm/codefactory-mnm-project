package com.codefactory.mnm.sales.model.dto;

import java.util.List;

public class SalesInsertDTO {

	private SalesDTO salesDTO;					//매출 DTO
	List<SalesAttachmentDTO> files;				//매출 첨부파일 리스트
	List<SalesProductDTO> salesProductList;		//매출품목 리스트
	
	public SalesInsertDTO() {}

	public SalesInsertDTO(SalesDTO salesDTO, List<SalesAttachmentDTO> files, List<SalesProductDTO> salesProductList) {
		super();
		this.salesDTO = salesDTO;
		this.files = files;
		this.salesProductList = salesProductList;
	}

	public SalesDTO getSalesDTO() {
		return salesDTO;
	}

	public List<SalesAttachmentDTO> getFiles() {
		return files;
	}

	public List<SalesProductDTO> getSalesProductList() {
		return salesProductList;
	}

	public void setSalesDTO(SalesDTO salesDTO) {
		this.salesDTO = salesDTO;
	}

	public void setFiles(List<SalesAttachmentDTO> files) {
		this.files = files;
	}

	public void setSalesProductList(List<SalesProductDTO> salesProductList) {
		this.salesProductList = salesProductList;
	}

	@Override
	public String toString() {
		return "SalesInsertDTO [salesDTO=" + salesDTO + ", files=" + files + ", salesProductList=" + salesProductList
				+ "]";
	}

	
	
	
}
