package com.codefactory.mnm.sales.model.dto;

public class SalesAttachmentDTO {

	private String fileNo;				//파일번호
	private String fileName;			//파일명
	private String fileOriginalName;	//실제파일명
	private String filePath;			//경로
	private String division;			//구분
	private String salesNo;				//매출번호
	
	public SalesAttachmentDTO() {}

	public SalesAttachmentDTO(String fileNo, String fileName, String fileOriginalName, String filePath, String division,
			String salesNo) {
		super();
		this.fileNo = fileNo;
		this.fileName = fileName;
		this.fileOriginalName = fileOriginalName;
		this.filePath = filePath;
		this.division = division;
		this.salesNo = salesNo;
	}

	public String getFileNo() {
		return fileNo;
	}

	public String getFileName() {
		return fileName;
	}

	public String getFileOriginalName() {
		return fileOriginalName;
	}

	public String getFilePath() {
		return filePath;
	}

	public String getDivision() {
		return division;
	}

	public String getSalesNo() {
		return salesNo;
	}

	public void setFileNo(String fileNo) {
		this.fileNo = fileNo;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setFileOriginalName(String fileOriginalName) {
		this.fileOriginalName = fileOriginalName;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public void setSalesNo(String salesNo) {
		this.salesNo = salesNo;
	}

	@Override
	public String toString() {
		return "SalesAttachmentDTO [fileNo=" + fileNo + ", fileName=" + fileName + ", fileOriginalName="
				+ fileOriginalName + ", filePath=" + filePath + ", division=" + division + ", salesNo=" + salesNo + "]";
	}
	
	
}
