package com.codefactory.mnm.sales.model.dto;

import java.sql.Date;

public class SalesDTO {

	private String salesNo;								//매출번호
	private java.sql.Date releaseDate;					//출고일
	private int quantity;								//수량
	private java.sql.Date expectedDeliveryDate;			//입고예정일
	private String note;								//비고
	private String clientNo;							//고객번호
	private String businessType;						//사업유형
	private java.sql.Date writeDate;					//작성일자
	private String userNo;								//담당자번호
	private String vat;									//부가세
	private String delYn;								//삭제여부
	private String contractNo;							//계약번호
	private int supplyPrice;							//공급가액
	private int taxAmount;								//세액
	private int sum;									//합계
	private String clientName;							//고객명
	private String clientCompanyNo;						//고객사번호
	private String clientCompanyName;					//고객사명
	private String businessOpportunityNo;				//영업기회번호
	private String businessOpportunityName;				//영업기회명
	private String name;								//담당자명
	private String contractName;						//계약명
	private String estimateNo;							//견적번호
	private String estimateName;						//견적명
	
	public SalesDTO() {}

	public SalesDTO(String salesNo, Date releaseDate, int quantity, Date expectedDeliveryDate, String note,
			String clientNo, String businessType, Date writeDate, String userNo, String vat, String delYn,
			String contractNo, int supplyPrice, int taxAmount, int sum, String clientName, String clientCompanyNo,
			String clientCompanyName, String businessOpportunityNo, String businessOpportunityName, String name,
			String contractName, String estimateNo, String estimateName) {
		super();
		this.salesNo = salesNo;
		this.releaseDate = releaseDate;
		this.quantity = quantity;
		this.expectedDeliveryDate = expectedDeliveryDate;
		this.note = note;
		this.clientNo = clientNo;
		this.businessType = businessType;
		this.writeDate = writeDate;
		this.userNo = userNo;
		this.vat = vat;
		this.delYn = delYn;
		this.contractNo = contractNo;
		this.supplyPrice = supplyPrice;
		this.taxAmount = taxAmount;
		this.sum = sum;
		this.clientName = clientName;
		this.clientCompanyNo = clientCompanyNo;
		this.clientCompanyName = clientCompanyName;
		this.businessOpportunityNo = businessOpportunityNo;
		this.businessOpportunityName = businessOpportunityName;
		this.name = name;
		this.contractName = contractName;
		this.estimateNo = estimateNo;
		this.estimateName = estimateName;
	}

	public String getSalesNo() {
		return salesNo;
	}

	public java.sql.Date getReleaseDate() {
		return releaseDate;
	}

	public int getQuantity() {
		return quantity;
	}

	public java.sql.Date getExpectedDeliveryDate() {
		return expectedDeliveryDate;
	}

	public String getNote() {
		return note;
	}

	public String getClientNo() {
		return clientNo;
	}

	public String getBusinessType() {
		return businessType;
	}

	public java.sql.Date getWriteDate() {
		return writeDate;
	}

	public String getUserNo() {
		return userNo;
	}

	public String getVat() {
		return vat;
	}

	public String getDelYn() {
		return delYn;
	}

	public String getContractNo() {
		return contractNo;
	}

	public int getSupplyPrice() {
		return supplyPrice;
	}

	public int getTaxAmount() {
		return taxAmount;
	}

	public int getSum() {
		return sum;
	}

	public String getClientName() {
		return clientName;
	}

	public String getClientCompanyNo() {
		return clientCompanyNo;
	}

	public String getClientCompanyName() {
		return clientCompanyName;
	}

	public String getBusinessOpportunityNo() {
		return businessOpportunityNo;
	}

	public String getBusinessOpportunityName() {
		return businessOpportunityName;
	}

	public String getName() {
		return name;
	}

	public String getContractName() {
		return contractName;
	}

	public String getEstimateNo() {
		return estimateNo;
	}

	public String getEstimateName() {
		return estimateName;
	}

	public void setSalesNo(String salesNo) {
		this.salesNo = salesNo;
	}

	public void setReleaseDate(java.sql.Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public void setExpectedDeliveryDate(java.sql.Date expectedDeliveryDate) {
		this.expectedDeliveryDate = expectedDeliveryDate;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public void setClientNo(String clientNo) {
		this.clientNo = clientNo;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public void setVat(String vat) {
		this.vat = vat;
	}

	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}

	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	public void setSupplyPrice(int supplyPrice) {
		this.supplyPrice = supplyPrice;
	}

	public void setTaxAmount(int taxAmount) {
		this.taxAmount = taxAmount;
	}

	public void setSum(int sum) {
		this.sum = sum;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public void setClientCompanyNo(String clientCompanyNo) {
		this.clientCompanyNo = clientCompanyNo;
	}

	public void setClientCompanyName(String clientCompanyName) {
		this.clientCompanyName = clientCompanyName;
	}

	public void setBusinessOpportunityNo(String businessOpportunityNo) {
		this.businessOpportunityNo = businessOpportunityNo;
	}

	public void setBusinessOpportunityName(String businessOpportunityName) {
		this.businessOpportunityName = businessOpportunityName;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setContractName(String contractName) {
		this.contractName = contractName;
	}

	public void setEstimateNo(String estimateNo) {
		this.estimateNo = estimateNo;
	}

	public void setEstimateName(String estimateName) {
		this.estimateName = estimateName;
	}

	@Override
	public String toString() {
		return "SalesDTO [salesNo=" + salesNo + ", releaseDate=" + releaseDate + ", quantity=" + quantity
				+ ", expectedDeliveryDate=" + expectedDeliveryDate + ", note=" + note + ", clientNo=" + clientNo
				+ ", businessType=" + businessType + ", writeDate=" + writeDate + ", userNo=" + userNo + ", vat=" + vat
				+ ", delYn=" + delYn + ", contractNo=" + contractNo + ", supplyPrice=" + supplyPrice + ", taxAmount="
				+ taxAmount + ", sum=" + sum + ", clientName=" + clientName + ", clientCompanyNo=" + clientCompanyNo
				+ ", clientCompanyName=" + clientCompanyName + ", businessOpportunityNo=" + businessOpportunityNo
				+ ", businessOpportunityName=" + businessOpportunityName + ", name=" + name + ", contractName="
				+ contractName + ", estimateNo=" + estimateNo + ", estimateName=" + estimateName + "]";
	}
	
	
}
