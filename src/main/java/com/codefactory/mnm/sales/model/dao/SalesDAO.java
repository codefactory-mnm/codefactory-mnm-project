package com.codefactory.mnm.sales.model.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.codefactory.mnm.contract.model.dto.ContractDTO;
import com.codefactory.mnm.contract.model.dto.ContractProductDTO;
import com.codefactory.mnm.contract.model.dto.OpinionDTO;
import com.codefactory.mnm.estimate.model.dto.DeptListDTO;
import com.codefactory.mnm.sales.model.dto.ContractListDTO;
import com.codefactory.mnm.sales.model.dto.SalesAttachmentDTO;
import com.codefactory.mnm.sales.model.dto.SalesDTO;
import com.codefactory.mnm.sales.model.dto.SalesProductDTO;
import com.codefactory.mnm.sales.model.dto.SalesSearchDTO;

@Mapper
public interface SalesDAO {

	int selectTotalCount(SalesSearchDTO salesSearchDTO);

	List<SalesDTO> selectSalesList(SalesSearchDTO salesSearchDTO);

	List<DeptListDTO> selectDeptList();

	List<ContractListDTO> selectContractList();

	int insertSales(SalesDTO salesDTO);

	int insertSalesProduct(SalesProductDTO salesProductList);

	int insertSalesAttachment(SalesAttachmentDTO salesAttachmentDTO);

	SalesDTO selectSales(String salesNo);

	List<SalesAttachmentDTO> selectFileList(String salesNo);

	List<SalesProductDTO> selectSalesProductList(String salesNo);

	List<OpinionDTO> selectOpinionList(String salesNo);

	int deleteSales(String salesNo);

	SalesAttachmentDTO selectFile(String fileNo);

	int deleteFile(String fileNo);

	int updateSales(SalesDTO salesDTO);

	int updateSalesAttachment(SalesAttachmentDTO salesAttachmentDTO);

	int deleteSalesProduct(SalesDTO salesDTO);

	int updateSalesProduct(SalesProductDTO salesProductDTO);

}
