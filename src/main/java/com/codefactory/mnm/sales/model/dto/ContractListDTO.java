package com.codefactory.mnm.sales.model.dto;

public class ContractListDTO implements java.io.Serializable {

	private String contractNo;						//계약번호
	private String contractName;					//계약명
	private String estimateNo;						//견적번호
	private String estimateName;					//견적명
	private String businessOpportunityNo;			//영업기회번호
	private String businessOpportunityName;			//영업기회명
	private String clientNo;						//고객번호
	private String clientName;						//고객명
	private String clientCompanyNo;					//고객사번호
	private String clientCompanyName;				//고객사명
	
	public ContractListDTO() {}

	public ContractListDTO(String contractNo, String contractName, String estimateNo, String estimateName,
			String businessOpportunityNo, String businessOpportunityName, String clientNo, String clientName,
			String clientCompanyNo, String clientCompanyName) {
		super();
		this.contractNo = contractNo;
		this.contractName = contractName;
		this.estimateNo = estimateNo;
		this.estimateName = estimateName;
		this.businessOpportunityNo = businessOpportunityNo;
		this.businessOpportunityName = businessOpportunityName;
		this.clientNo = clientNo;
		this.clientName = clientName;
		this.clientCompanyNo = clientCompanyNo;
		this.clientCompanyName = clientCompanyName;
	}

	public String getContractNo() {
		return contractNo;
	}

	public String getContractName() {
		return contractName;
	}

	public String getEstimateNo() {
		return estimateNo;
	}

	public String getEstimateName() {
		return estimateName;
	}

	public String getBusinessOpportunityNo() {
		return businessOpportunityNo;
	}

	public String getBusinessOpportunityName() {
		return businessOpportunityName;
	}

	public String getClientNo() {
		return clientNo;
	}

	public String getClientName() {
		return clientName;
	}

	public String getClientCompanyNo() {
		return clientCompanyNo;
	}

	public String getClientCompanyName() {
		return clientCompanyName;
	}

	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	public void setContractName(String contractName) {
		this.contractName = contractName;
	}

	public void setEstimateNo(String estimateNo) {
		this.estimateNo = estimateNo;
	}

	public void setEstimateName(String estimateName) {
		this.estimateName = estimateName;
	}

	public void setBusinessOpportunityNo(String businessOpportunityNo) {
		this.businessOpportunityNo = businessOpportunityNo;
	}

	public void setBusinessOpportunityName(String businessOpportunityName) {
		this.businessOpportunityName = businessOpportunityName;
	}

	public void setClientNo(String clientNo) {
		this.clientNo = clientNo;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public void setClientCompanyNo(String clientCompanyNo) {
		this.clientCompanyNo = clientCompanyNo;
	}

	public void setClientCompanyName(String clientCompanyName) {
		this.clientCompanyName = clientCompanyName;
	}

	@Override
	public String toString() {
		return "ContractListDTO [contractNo=" + contractNo + ", contractName=" + contractName + ", estimateNo="
				+ estimateNo + ", estimateName=" + estimateName + ", businessOpportunityNo=" + businessOpportunityNo
				+ ", businessOpportunityName=" + businessOpportunityName + ", clientNo=" + clientNo + ", clientName="
				+ clientName + ", clientCompanyNo=" + clientCompanyNo + ", clientCompanyName=" + clientCompanyName
				+ "]";
	}
	
	
}
