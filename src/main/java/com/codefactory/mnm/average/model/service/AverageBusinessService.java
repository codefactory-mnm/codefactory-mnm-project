package com.codefactory.mnm.average.model.service;

import java.util.Map;

import com.codefactory.mnm.average.model.dto.AverageCustomSearchDTO;

public interface AverageBusinessService {

	Map<String, Object> selectbusinessOpportunityList(AverageCustomSearchDTO averageCustomSearchDTO);

	Map<String, Object> selectbusinessActivityList(AverageCustomSearchDTO averageCustomSearchDTO);

}
