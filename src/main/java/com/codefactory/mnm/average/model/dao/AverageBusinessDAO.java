package com.codefactory.mnm.average.model.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.codefactory.mnm.average.model.dto.AverageBusinessActivityDTO;
import com.codefactory.mnm.average.model.dto.AverageBusinessOpportunityDTO;
import com.codefactory.mnm.average.model.dto.AverageCustomSearchDTO;

@Mapper
public interface AverageBusinessDAO {

	List<AverageBusinessOpportunityDTO> selectBusinessOpportunityList(AverageCustomSearchDTO averageCustomSearchDTO);
	
	List<Integer> selectGraphBOList(AverageCustomSearchDTO averageCustomSearchDTO);

	List<AverageBusinessActivityDTO> selectBusinessActivityList(AverageCustomSearchDTO averageCustomSearchDTO);

	List<Integer> selectTableBAList(AverageCustomSearchDTO averageCustomSearchDTO);

}
