package com.codefactory.mnm.average.model.dto;

import java.sql.Date;

public class AveragePotentialClientDTO {
	
	private String potentialClientNo;			//잠재고객번호
	private String name;						//이름
	private String clientCompany;				//고객사
	private String department;					//부서
	private String position;					//직책
	private String contactDivision;				//접촉구분
	private String contactState;				//접촉상태
	private String phone;						//휴대전화번호
	private String landlineTel;					//유선전화번호
	private String mail;						//메일
	private String fax;							//팩스번호
	private String zipCode;						//우편번호
	private String address;						//주소
	private String detailedAddress;				//상세주소
	private String note;						//비고
	private String userNo;						//작성자번호
	private String userName;					//작성자이름
	private java.sql.Date writeDate;			//작성일자
	private String grade;						//등급
	
	public AveragePotentialClientDTO() {}

	public AveragePotentialClientDTO(String potentialClientNo, String name, String clientCompany, String department,
			String position, String contactDivision, String contactState, String phone, String landlineTel, String mail,
			String fax, String zipCode, String address, String detailedAddress, String note, String userNo,
			String userName, Date writeDate, String grade) {
		super();
		this.potentialClientNo = potentialClientNo;
		this.name = name;
		this.clientCompany = clientCompany;
		this.department = department;
		this.position = position;
		this.contactDivision = contactDivision;
		this.contactState = contactState;
		this.phone = phone;
		this.landlineTel = landlineTel;
		this.mail = mail;
		this.fax = fax;
		this.zipCode = zipCode;
		this.address = address;
		this.detailedAddress = detailedAddress;
		this.note = note;
		this.userNo = userNo;
		this.userName = userName;
		this.writeDate = writeDate;
		this.grade = grade;
	}

	public String getPotentialClientNo() {
		return potentialClientNo;
	}

	public void setPotentialClientNo(String potentialClientNo) {
		this.potentialClientNo = potentialClientNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClientCompany() {
		return clientCompany;
	}

	public void setClientCompany(String clientCompany) {
		this.clientCompany = clientCompany;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getContactDivision() {
		return contactDivision;
	}

	public void setContactDivision(String contactDivision) {
		this.contactDivision = contactDivision;
	}

	public String getContactState() {
		return contactState;
	}

	public void setContactState(String contactState) {
		this.contactState = contactState;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getLandlineTel() {
		return landlineTel;
	}

	public void setLandlineTel(String landlineTel) {
		this.landlineTel = landlineTel;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDetailedAddress() {
		return detailedAddress;
	}

	public void setDetailedAddress(String detailedAddress) {
		this.detailedAddress = detailedAddress;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public java.sql.Date getWriteDate() {
		return writeDate;
	}

	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	@Override
	public String toString() {
		return "AveragePotentialClientDTO [potentialClientNo=" + potentialClientNo + ", name=" + name
				+ ", clientCompany=" + clientCompany + ", department=" + department + ", position=" + position
				+ ", contactDivision=" + contactDivision + ", contactState=" + contactState + ", phone=" + phone
				+ ", landlineTel=" + landlineTel + ", mail=" + mail + ", fax=" + fax + ", zipCode=" + zipCode
				+ ", address=" + address + ", detailedAddress=" + detailedAddress + ", note=" + note + ", userNo="
				+ userNo + ", userName=" + userName + ", writeDate=" + writeDate + ", grade=" + grade + "]";
	}

	
	
}
