package com.codefactory.mnm.average.model.dto;

import java.sql.Date;
import java.util.List;
import java.util.Map;

public class AverageCompanyDTO {
	
	private String clientCompanyNo;			//고객사번호
	private String clientCompanyName;		//고객사명
	private String grade;					//등급
	private String progressStatus;			//진행상태
	private String sales;					//매출
	private String businessNo;				//사업자번호
	private String landlineTel;				//유선전화번호
	private String fax;						//팩스번호
	private String website;					//웹사이트
	private String zipCode;					//우편번호
	private String address;					//주소
	private String detailedAddress;			//상세주소
	private String note;					//비고
	private String division;				//구분
	private java.sql.Date writeDate;		//작성일자
	private String employeeNo;				//사원수
	
	private List<Map<String, String>> userNameList;		//담당자
	private Map<String, String> userNameMap;
	private String userNameStr;							//담당자 Str(view 출력용)
	
	public AverageCompanyDTO() {}

	public AverageCompanyDTO(String clientCompanyNo, String clientCompanyName, String grade, String progressStatus,
			String sales, String businessNo, String landlineTel, String fax, String website, String zipCode,
			String address, String detailedAddress, String note, String division, Date writeDate, String employeeNo,
			List<Map<String, String>> userNameList, Map<String, String> userNameMap, String userNameStr) {
		super();
		this.clientCompanyNo = clientCompanyNo;
		this.clientCompanyName = clientCompanyName;
		this.grade = grade;
		this.progressStatus = progressStatus;
		this.sales = sales;
		this.businessNo = businessNo;
		this.landlineTel = landlineTel;
		this.fax = fax;
		this.website = website;
		this.zipCode = zipCode;
		this.address = address;
		this.detailedAddress = detailedAddress;
		this.note = note;
		this.division = division;
		this.writeDate = writeDate;
		this.employeeNo = employeeNo;
		this.userNameList = userNameList;
		this.userNameMap = userNameMap;
		this.userNameStr = userNameStr;
	}

	public String getClientCompanyNo() {
		return clientCompanyNo;
	}

	public void setClientCompanyNo(String clientCompanyNo) {
		this.clientCompanyNo = clientCompanyNo;
	}

	public String getClientCompanyName() {
		return clientCompanyName;
	}

	public void setClientCompanyName(String clientCompanyName) {
		this.clientCompanyName = clientCompanyName;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getProgressStatus() {
		return progressStatus;
	}

	public void setProgressStatus(String progressStatus) {
		this.progressStatus = progressStatus;
	}

	public String getSales() {
		return sales;
	}

	public void setSales(String sales) {
		this.sales = sales;
	}

	public String getBusinessNo() {
		return businessNo;
	}

	public void setBusinessNo(String businessNo) {
		this.businessNo = businessNo;
	}

	public String getLandlineTel() {
		return landlineTel;
	}

	public void setLandlineTel(String landlineTel) {
		this.landlineTel = landlineTel;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDetailedAddress() {
		return detailedAddress;
	}

	public void setDetailedAddress(String detailedAddress) {
		this.detailedAddress = detailedAddress;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public java.sql.Date getWriteDate() {
		return writeDate;
	}

	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}

	public String getEmployeeNo() {
		return employeeNo;
	}

	public void setEmployeeNo(String employeeNo) {
		this.employeeNo = employeeNo;
	}

	public List<Map<String, String>> getUserNameList() {
		return userNameList;
	}

	public void setUserNameList(List<Map<String, String>> userNameList) {
		this.userNameList = userNameList;
	}

	public Map<String, String> getUserNameMap() {
		return userNameMap;
	}

	public void setUserNameMap(Map<String, String> userNameMap) {
		this.userNameMap = userNameMap;
	}

	public String getUserNameStr() {
		return userNameStr;
	}

	public void setUserNameStr(String userNameStr) {
		this.userNameStr = userNameStr;
	}

	@Override
	public String toString() {
		return "AverageCompanyDTO [clientCompanyNo=" + clientCompanyNo + ", clientCompanyName=" + clientCompanyName
				+ ", grade=" + grade + ", progressStatus=" + progressStatus + ", sales=" + sales + ", businessNo="
				+ businessNo + ", landlineTel=" + landlineTel + ", fax=" + fax + ", website=" + website + ", zipCode="
				+ zipCode + ", address=" + address + ", detailedAddress=" + detailedAddress + ", note=" + note
				+ ", division=" + division + ", writeDate=" + writeDate + ", employeeNo=" + employeeNo
				+ ", userNameList=" + userNameList + ", userNameMap=" + userNameMap + ", userNameStr=" + userNameStr
				+ "]";
	}

	

}
