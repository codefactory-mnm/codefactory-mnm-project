package com.codefactory.mnm.average.model.dto;

import java.sql.Date;

import org.springframework.web.bind.annotation.RequestParam;

public class AverageCustomSearchDTO {
	
	private java.sql.Date startDate;
	private java.sql.Date endDate;
	private String deptNo;
	private String responsibility;
	private String receptionist;
	private String preResponsibility;
	
	public AverageCustomSearchDTO() {}

	public AverageCustomSearchDTO(Date startDate, Date endDate, String deptNo, String responsibility,
			String receptionist, String preResponsibility) {
		super();
		this.startDate = startDate;
		this.endDate = endDate;
		this.deptNo = deptNo;
		this.responsibility = responsibility;
		this.receptionist = receptionist;
		this.preResponsibility = preResponsibility;
	}

	public java.sql.Date getStartDate() {
		return startDate;
	}

	public void setStartDate(java.sql.Date startDate) {
		this.startDate = startDate;
	}

	public java.sql.Date getEndDate() {
		return endDate;
	}

	public void setEndDate(java.sql.Date endDate) {
		this.endDate = endDate;
	}

	public String getDeptNo() {
		return deptNo;
	}

	public void setDeptNo(String deptNo) {
		this.deptNo = deptNo;
	}

	public String getResponsibility() {
		return responsibility;
	}

	public void setResponsibility(String responsibility) {
		this.responsibility = responsibility;
	}

	public String getReceptionist() {
		return receptionist;
	}

	public void setReceptionist(String receptionist) {
		this.receptionist = receptionist;
	}

	public String getPreResponsibility() {
		return preResponsibility;
	}

	public void setPreResponsibility(String preResponsibility) {
		this.preResponsibility = preResponsibility;
	}

	@Override
	public String toString() {
		return "AverageCustomSearchDTO [startDate=" + startDate + ", endDate=" + endDate + ", deptNo=" + deptNo
				+ ", responsibility=" + responsibility + ", receptionist=" + receptionist + ", preResponsibility="
				+ preResponsibility + "]";
	}

}
