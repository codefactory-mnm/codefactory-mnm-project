package com.codefactory.mnm.average.model.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.codefactory.mnm.average.model.dao.AverageBusinessDAO;
import com.codefactory.mnm.average.model.dto.AverageBusinessActivityDTO;
import com.codefactory.mnm.average.model.dto.AverageBusinessOpportunityDTO;
import com.codefactory.mnm.average.model.dto.AverageCustomSearchDTO;
import com.codefactory.mnm.goal.model.dao.GoalDAO;
import com.codefactory.mnm.goal.model.dto.DepartmentDTO;
import com.codefactory.mnm.goal.model.dto.UserDTO;

@Service("averageBusinessService")
public class AverageBusinessServiceImpl implements AverageBusinessService{
	
	private final AverageBusinessDAO averageBusinessDAO;
	private final GoalDAO goalDAO;
	
	@Autowired
	public AverageBusinessServiceImpl(AverageBusinessDAO averageBusinessDAO, GoalDAO goalDAO) {
		this.averageBusinessDAO = averageBusinessDAO;
		this.goalDAO = goalDAO;
	}
	
	@Override
	public Map<String, Object> selectbusinessOpportunityList(AverageCustomSearchDTO averageCustomSearchDTO) {
		
		/* 1. 검색용 select box */
		/* 부서 List로 불러오기 */
		List<DepartmentDTO> deptList = goalDAO.findAllDept();
		
		/* 직원 List로 불러오기 */
		List<UserDTO> userList = goalDAO.findAllUser();
		
		/* 2. 출력용 내용 불러오기 */
		/* 영업기회 중 검색기준에 맞는 목록들을 리스트로 가져온다. */
		List<AverageBusinessOpportunityDTO> boList = averageBusinessDAO.selectBusinessOpportunityList(averageCustomSearchDTO);
		
		/* 영업기회 중 오늘을 기준으로 당월, 전월, 전전월 값을 가져온다 */
		List<Integer> graphBusinessOpportunityList = averageBusinessDAO.selectGraphBOList(averageCustomSearchDTO);
		
		/* 3개월 평균을 내고 List에 넣어준다. */
		int totalThreeMonth = 0;
		for(int i = 0; i < 3; i++) {
			
			totalThreeMonth += graphBusinessOpportunityList.get(i);
			
			int threeMonthAverage = (totalThreeMonth / 3);
			
			graphBusinessOpportunityList.remove(2);
			graphBusinessOpportunityList.add(2, threeMonthAverage);
		}
		
		/* 사업유형(BUSINESS_TYPE) 별 count */
		int civilianType = 0;
		int officialType = 0;
		for(int i = 0; i < boList.size(); i++) {
			if(boList.get(i).getBusinessType().equals("민수사업")) {
				civilianType++;
			}
			
			if(boList.get(i).getBusinessType().equals("관공사업")) {
				officialType++;
			}
		}
		
		/* 매출구분(SALES_DIVISION) 별 count */
		int productType = 0;
		int commissionType = 0;
		for(int i = 0; i < boList.size(); i++) {
			if(boList.get(i).getSalesDivision().equals("상품매출")) {
				productType++;
			}
			
			if(boList.get(i).getSalesDivision().equals("커미션매출")) {
				commissionType++;
			}
		}
		
		/* 인지경로(COGNITIVE_PATHWAY) 별 count */
		int acquaintance = 0;
		int newsPaper = 0;
		int internet = 0;
		int preexistence = 0;
		int etc = 0;
		for(int i = 0; i < boList.size(); i++) {
			if(boList.get(i).getCognitivePathway().equals("지인소개")) {
				acquaintance++;
			}
			
			if(boList.get(i).getCognitivePathway().equals("신문")) {
				newsPaper++;
			}
			
			if(boList.get(i).getCognitivePathway().equals("인터넷검색")) {
				internet++;
			}
			
			if(boList.get(i).getCognitivePathway().equals("기존고객")) {
				preexistence++;
			}
			
			if(boList.get(i).getCognitivePathway().equals("기타")) {
				etc++;
			}
		}
		
		/* Controller 로 보내는 map */
		Map<String, Object> map = new HashMap<>();
		map.put("deptList", deptList);
		map.put("userList", userList);
		
		map.put("boList", boList);
		map.put("graphBusinessOpportunityList", graphBusinessOpportunityList);
		
		map.put("civilianType", civilianType);
		map.put("officialType", officialType);
		map.put("productType", productType);
		map.put("commissionType", commissionType);
		
		map.put("acquaintance", acquaintance);
		map.put("newsPaper", newsPaper);
		map.put("internet", internet);
		map.put("preexistence", preexistence);
		map.put("etc", etc);
		
		return map;
	}

	@Override
	public Map<String, Object> selectbusinessActivityList(AverageCustomSearchDTO averageCustomSearchDTO) {
		
		/* 1. 검색용 select box */
		/* 부서 List로 불러오기 */
		List<DepartmentDTO> deptList = goalDAO.findAllDept();
		
		/* 직원 List로 불러오기 */
		List<UserDTO> userList = goalDAO.findAllUser();
		
		/* 2. 출력용 내용 불러오기 */
		/* 영업활동 중 검색기준에 맞는 목록들을 리스트로 가져온다. */
		List<AverageBusinessActivityDTO> baList = averageBusinessDAO.selectBusinessActivityList(averageCustomSearchDTO);
		
		/* 영업활동 중 오늘을 기준으로 당월, 전월, 전전월 값을 가져온다 */
		List<Integer> tableBusinessActivityList = averageBusinessDAO.selectTableBAList(averageCustomSearchDTO);
		System.out.println("tableBusinessActivityList : " + tableBusinessActivityList);
		
		/* 3개월 평균을 내고 List에 넣어준다. */
		int totalThreeMonth = 0;
		for(int i = 0; i < 3; i++) {
			
			totalThreeMonth += tableBusinessActivityList.get(i);
			
			int threeMonthAverage = (totalThreeMonth / 3);
			
			tableBusinessActivityList.remove(2);
			tableBusinessActivityList.add(2, threeMonthAverage);
		}
		
		/* 활동분류(ACTIVITY_CLASSIFICATION) 별 count */
		int visitType = 0;
		int telephoneType = 0;
		if(baList != null) {
			for(int i = 0; i < baList.size(); i++) {
				if(baList.get(i).getActivityClassification().equals("방문")) {
					visitType++;
				}
				
				if(baList.get(i).getActivityClassification().equals("전화")) {
					telephoneType++;
				}
			}
		}
		
		/* 활동목적(ACTIVITY_PURPOSE) 별 count */
		int greetingType = 0;
		int salesType = 0;
		if(baList != null) {
			for(int i = 0; i < baList.size(); i++) {
				if(baList.get(i).getActivityPurpose().equals("인사")) {
					greetingType++;
				}
				
				if(baList.get(i).getActivityPurpose().equals("영업")) {
					salesType++;
				}
			}
		}
		
		/* Controller 로 보내는 map */
		Map<String, Object> map = new HashMap<>();
		map.put("deptList", deptList);
		map.put("userList", userList);
		
		map.put("baList", baList);
		map.put("tableBusinessActivityList", tableBusinessActivityList);
		
		map.put("visitType", visitType);
		map.put("telephoneType", telephoneType);
		map.put("greetingType", greetingType);
		map.put("salesType", salesType);
		
		return map;
	}
	
}
