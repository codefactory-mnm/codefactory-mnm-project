package com.codefactory.mnm.average.model.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.codefactory.mnm.average.model.dto.AverageProductDTO;
import com.codefactory.mnm.average.model.dto.AverageSalesSearchDTO;
import com.codefactory.mnm.goal.model.dto.UserDTO;

@Mapper
public interface AverageSalesDAO {

	Integer selectQuantityList(AverageSalesSearchDTO averageSalesSearchDTO);

	Integer selectSumList(AverageSalesSearchDTO averageSalesSearchDTO);

	List<AverageProductDTO> findAllProduct();
	
	Integer selectSumListByGroup(AverageSalesSearchDTO averageSalesSearchDTO);

	Integer selectGoalListByGroup(AverageSalesSearchDTO averageSalesSearchDTO);

	List<UserDTO> findUser(String responsibility);

	Integer selectSalesRepresentative(AverageSalesSearchDTO averageSalesSearchDTO);

}
