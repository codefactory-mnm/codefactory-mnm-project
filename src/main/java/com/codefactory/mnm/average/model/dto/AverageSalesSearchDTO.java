package com.codefactory.mnm.average.model.dto;

import java.sql.Date;

public class AverageSalesSearchDTO {
	
	private java.sql.Date monthStartDate;
	private java.sql.Date monthLastDate;
	private String deptName;
	private String productNo;
	private String responsibility;
	private int year;
	private int month;
	
	public AverageSalesSearchDTO() {}

	public AverageSalesSearchDTO(Date monthStartDate, Date monthLastDate, String deptName, String productNo,
			String responsibility, int year, int month) {
		super();
		this.monthStartDate = monthStartDate;
		this.monthLastDate = monthLastDate;
		this.deptName = deptName;
		this.productNo = productNo;
		this.responsibility = responsibility;
		this.year = year;
		this.month = month;
	}

	public java.sql.Date getMonthStartDate() {
		return monthStartDate;
	}

	public void setMonthStartDate(java.sql.Date monthStartDate) {
		this.monthStartDate = monthStartDate;
	}

	public java.sql.Date getMonthLastDate() {
		return monthLastDate;
	}

	public void setMonthLastDate(java.sql.Date monthLastDate) {
		this.monthLastDate = monthLastDate;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getProductNo() {
		return productNo;
	}

	public void setProductNo(String productNo) {
		this.productNo = productNo;
	}

	public String getResponsibility() {
		return responsibility;
	}

	public void setResponsibility(String responsibility) {
		this.responsibility = responsibility;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	@Override
	public String toString() {
		return "AverageSalesSearchDTO [monthStartDate=" + monthStartDate + ", monthLastDate=" + monthLastDate
				+ ", deptName=" + deptName + ", productNo=" + productNo + ", responsibility=" + responsibility
				+ ", year=" + year + ", month=" + month + "]";
	}

	
}