package com.codefactory.mnm.average.model.dto;

import java.sql.Date;
import java.util.List;

import org.apache.naming.java.javaURLContextFactory;

public class AverageClientSupportDTO {
	
	private String clientSupportNo;							//고객지원번호
	private String title;									//제목
	private java.sql.Date requestDate;						//요청일
	private java.sql.Date completeRequestDate;				//완료요철일
	private java.sql.Date completeDate;
	private java.sql.Date elapsedTime;						//경과일
	private String importance;								//중요도
	private String clientNo;								//고객번호
	private String receptionistNo;							//접수자번호
	private String receptionistName;						//접수자명
	private String receiptWay;								//접수방법
	private java.sql.Date writeDate;						//작성일자
	private String userNo;									//담당자번호
	private String userName;								//담당자번호
	private String processingDivision;						//처리구분
	
	private List<AverageCSHistroyDTO> csHistroyList; 		//고객지원처리내역
	
	public AverageClientSupportDTO() {}

	public AverageClientSupportDTO(String clientSupportNo, String title, Date requestDate, Date completeRequestDate,
			Date completeDate, Date elapsedTime, String importance, String clientNo, String receptionistNo,
			String receptionistName, String receiptWay, Date writeDate, String userNo, String userName,
			String processingDivision, List<AverageCSHistroyDTO> csHistroyList) {
		super();
		this.clientSupportNo = clientSupportNo;
		this.title = title;
		this.requestDate = requestDate;
		this.completeRequestDate = completeRequestDate;
		this.completeDate = completeDate;
		this.elapsedTime = elapsedTime;
		this.importance = importance;
		this.clientNo = clientNo;
		this.receptionistNo = receptionistNo;
		this.receptionistName = receptionistName;
		this.receiptWay = receiptWay;
		this.writeDate = writeDate;
		this.userNo = userNo;
		this.userName = userName;
		this.processingDivision = processingDivision;
		this.csHistroyList = csHistroyList;
	}

	public String getClientSupportNo() {
		return clientSupportNo;
	}

	public void setClientSupportNo(String clientSupportNo) {
		this.clientSupportNo = clientSupportNo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public java.sql.Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(java.sql.Date requestDate) {
		this.requestDate = requestDate;
	}

	public java.sql.Date getCompleteRequestDate() {
		return completeRequestDate;
	}

	public void setCompleteRequestDate(java.sql.Date completeRequestDate) {
		this.completeRequestDate = completeRequestDate;
	}

	public java.sql.Date getCompleteDate() {
		return completeDate;
	}

	public void setCompleteDate(java.sql.Date completeDate) {
		this.completeDate = completeDate;
	}

	public java.sql.Date getElapsedTime() {
		return elapsedTime;
	}

	public void setElapsedTime(java.sql.Date elapsedTime) {
		this.elapsedTime = elapsedTime;
	}

	public String getImportance() {
		return importance;
	}

	public void setImportance(String importance) {
		this.importance = importance;
	}

	public String getClientNo() {
		return clientNo;
	}

	public void setClientNo(String clientNo) {
		this.clientNo = clientNo;
	}

	public String getReceptionistNo() {
		return receptionistNo;
	}

	public void setReceptionistNo(String receptionistNo) {
		this.receptionistNo = receptionistNo;
	}

	public String getReceptionistName() {
		return receptionistName;
	}

	public void setReceptionistName(String receptionistName) {
		this.receptionistName = receptionistName;
	}

	public String getReceiptWay() {
		return receiptWay;
	}

	public void setReceiptWay(String receiptWay) {
		this.receiptWay = receiptWay;
	}

	public java.sql.Date getWriteDate() {
		return writeDate;
	}

	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getProcessingDivision() {
		return processingDivision;
	}

	public void setProcessingDivision(String processingDivision) {
		this.processingDivision = processingDivision;
	}

	public List<AverageCSHistroyDTO> getCsHistroyList() {
		return csHistroyList;
	}

	public void setCsHistroyList(List<AverageCSHistroyDTO> csHistroyList) {
		this.csHistroyList = csHistroyList;
	}

	@Override
	public String toString() {
		return "AverageClientSupportDTO [clientSupportNo=" + clientSupportNo + ", title=" + title + ", requestDate="
				+ requestDate + ", completeRequestDate=" + completeRequestDate + ", completeDate=" + completeDate
				+ ", elapsedTime=" + elapsedTime + ", importance=" + importance + ", clientNo=" + clientNo
				+ ", receptionistNo=" + receptionistNo + ", receptionistName=" + receptionistName + ", receiptWay="
				+ receiptWay + ", writeDate=" + writeDate + ", userNo=" + userNo + ", userName=" + userName
				+ ", processingDivision=" + processingDivision + ", csHistroyList=" + csHistroyList + "]";
	}

	
	
}
