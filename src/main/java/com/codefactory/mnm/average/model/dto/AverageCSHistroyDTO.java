package com.codefactory.mnm.average.model.dto;

import java.sql.Date;

public class AverageCSHistroyDTO {
	
	private String processingHistoryNo;			//처리내역번호
	private String processingDivision; 			//처리구분
	private String processingContent;			//처리내용
	private java.sql.Date processingDate; 		//처리날짜
	private String userNo;						//담당자
	private String userName;					//담당자 명
	private String clientSupportNo;				//고객지원번호
	
	public AverageCSHistroyDTO() {}

	public AverageCSHistroyDTO(String processingHistoryNo, String processingDivision, String processingContent,
			Date processingDate, String userNo, String userName, String clientSupportNo) {
		super();
		this.processingHistoryNo = processingHistoryNo;
		this.processingDivision = processingDivision;
		this.processingContent = processingContent;
		this.processingDate = processingDate;
		this.userNo = userNo;
		this.userName = userName;
		this.clientSupportNo = clientSupportNo;
	}

	public String getProcessingHistoryNo() {
		return processingHistoryNo;
	}

	public void setProcessingHistoryNo(String processingHistoryNo) {
		this.processingHistoryNo = processingHistoryNo;
	}

	public String getProcessingDivision() {
		return processingDivision;
	}

	public void setProcessingDivision(String processingDivision) {
		this.processingDivision = processingDivision;
	}

	public String getProcessingContent() {
		return processingContent;
	}

	public void setProcessingContent(String processingContent) {
		this.processingContent = processingContent;
	}

	public java.sql.Date getProcessingDate() {
		return processingDate;
	}

	public void setProcessingDate(java.sql.Date processingDate) {
		this.processingDate = processingDate;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getClientSupportNo() {
		return clientSupportNo;
	}

	public void setClientSupportNo(String clientSupportNo) {
		this.clientSupportNo = clientSupportNo;
	}

	@Override
	public String toString() {
		return "AverageCSHistroyDTO [processingHistoryNo=" + processingHistoryNo + ", processingDivision="
				+ processingDivision + ", processingContent=" + processingContent + ", processingDate=" + processingDate
				+ ", userNo=" + userNo + ", userName=" + userName + ", clientSupportNo=" + clientSupportNo + "]";
	}

}
