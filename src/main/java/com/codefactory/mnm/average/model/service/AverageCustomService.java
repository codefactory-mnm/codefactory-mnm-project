package com.codefactory.mnm.average.model.service;

import java.util.Map;

import com.codefactory.mnm.average.model.dto.AverageCustomSearchDTO;

public interface AverageCustomService {

	Map<String, Object> selectCustomList(AverageCustomSearchDTO averageCustomSearchDTO);

	Map<String, Object> selectClientSupportList(AverageCustomSearchDTO averageCustomSearchDTO);

}
