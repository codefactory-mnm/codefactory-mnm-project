package com.codefactory.mnm.average.model.dto;

import java.sql.Date;

public class AverageClientDTO {
	
	private String clientNo;			//고객번호
	private String clientName;			//고객명
	private String dept;				//부서
	private String job;					//직책
	private String phone;				//휴대폰번호
	private String landlineTel;			//유선전화번호
	private String email;				//메일
	private String keyMan;				//키맨여부
	private String note;				//비고
	private String userNo;				//담당자번호
	private String userName;			//담당자이름
	private java.sql.Date writeDate;			//작성일자
	private String grade;				//등급
	private String clientCompanyNo;		//고객사번호
	
	public AverageClientDTO() {}

	public AverageClientDTO(String clientNo, String clientName, String dept, String job, String phone,
			String landlineTel, String email, String keyMan, String note, String userNo, String userName,
			Date writeDate, String grade, String clientCompanyNo) {
		super();
		this.clientNo = clientNo;
		this.clientName = clientName;
		this.dept = dept;
		this.job = job;
		this.phone = phone;
		this.landlineTel = landlineTel;
		this.email = email;
		this.keyMan = keyMan;
		this.note = note;
		this.userNo = userNo;
		this.userName = userName;
		this.writeDate = writeDate;
		this.grade = grade;
		this.clientCompanyNo = clientCompanyNo;
	}

	public String getClientNo() {
		return clientNo;
	}

	public void setClientNo(String clientNo) {
		this.clientNo = clientNo;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getLandlineTel() {
		return landlineTel;
	}

	public void setLandlineTel(String landlineTel) {
		this.landlineTel = landlineTel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getKeyMan() {
		return keyMan;
	}

	public void setKeyMan(String keyMan) {
		this.keyMan = keyMan;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public java.sql.Date getWriteDate() {
		return writeDate;
	}

	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getClientCompanyNo() {
		return clientCompanyNo;
	}

	public void setClientCompanyNo(String clientCompanyNo) {
		this.clientCompanyNo = clientCompanyNo;
	}

	@Override
	public String toString() {
		return "AverageClientDTO [clientNo=" + clientNo + ", clientName=" + clientName + ", dept=" + dept + ", job="
				+ job + ", phone=" + phone + ", landlineTel=" + landlineTel + ", email=" + email + ", keyMan=" + keyMan
				+ ", note=" + note + ", userNo=" + userNo + ", userName=" + userName + ", writeDate=" + writeDate
				+ ", grade=" + grade + ", clientCompanyNo=" + clientCompanyNo + "]";
	}

	
	

}
