package com.codefactory.mnm.average.model.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.codefactory.mnm.average.model.dao.AverageBusinessDAO;
import com.codefactory.mnm.average.model.dao.AverageCustomDAO;
import com.codefactory.mnm.average.model.dao.AverageSalesDAO;
import com.codefactory.mnm.average.model.dto.AverageCustomSearchDTO;
import com.codefactory.mnm.average.model.dto.AverageSalesSearchDTO;
import com.codefactory.mnm.goal.model.dao.GoalDAO;
import com.codefactory.mnm.goal.model.dto.DepartmentDTO;
import com.codefactory.mnm.goal.model.dto.UserDTO;

@Service("averageMainService")
public class AverageMainServiceImpl implements AverageMainService{

	private final AverageCustomDAO averageCustomDAO; 
	private final AverageBusinessDAO averageBusinessDAO;
	private final AverageSalesDAO averageSalesDAO;
	private final GoalDAO goalDAO;
	
	@Autowired
	public AverageMainServiceImpl(AverageCustomDAO averageCustomDAO, 
			AverageBusinessDAO averageBusinessDAO,
			AverageSalesDAO averageSalesDAO,
			GoalDAO goalDAO) {
		this.averageCustomDAO = averageCustomDAO;
		this.averageBusinessDAO = averageBusinessDAO;
		this.averageSalesDAO = averageSalesDAO;
		this.goalDAO = goalDAO;
	}
	
	@Override
	public Map<String, Object> selectAverageMainList(Map<String, Object> searchCriteria) {
		
		/* 받은 검색조건 꺼내기 */
		List<AverageSalesSearchDTO> searchList = (List<AverageSalesSearchDTO>) searchCriteria.get("averageSalesSearchDTOList");
		AverageCustomSearchDTO averageCustomSearchDTO = (AverageCustomSearchDTO) searchCriteria.get("averageCustomSearchDTO");
		
		/* 1. 검색용 select box */
		/* 부서 List로 불러오기 */
		List<DepartmentDTO> deptList = goalDAO.findAllDept();
		
		/* 직원 List로 불러오기 */
		List<UserDTO> userList = goalDAO.findAllUser();
		
		/* '그래프' 탭의 신규고객 표 내용은 당월로 조건이 고정되어있다. 
		 * 오늘을 기준으로 당월, 전월, 전전월 값을 가져온다. */
		List<Integer> graphCustomList = averageCustomDAO.selectGraphCustomList(averageCustomSearchDTO);
		
		/* 당월만의 신규고객 리스트를 만든다. */
		List<Integer> badgeCustomThisMonthList = new ArrayList<>();
		badgeCustomThisMonthList.add(graphCustomList.get(0));	//당월 고객사
		badgeCustomThisMonthList.add(graphCustomList.get(3));	//당월 고객
		badgeCustomThisMonthList.add(graphCustomList.get(6));	//당월 잠재고객
		
		/* 매출 중 기간이 맞는 수량리스트 구하기 */
		List<Integer> quantityList = new ArrayList<>();
		for(int i = 0; i < searchList.size(); i++) {
			Integer quantity = averageSalesDAO.selectQuantityList(searchList.get(i));
			
			if(quantity == null) {
				quantity = 0;
			}
			
			quantityList.add(quantity);
		}
		
		/* 수량 합계, 평균, 최댓값 구하기 */
		int quantitySum = 0;
		double quantityAvg = 0.0;
		int quantityMax = 0;
		for(int i = 0; i < quantityList.size(); i++) {
			quantitySum += quantityList.get(i);
			
			if(quantityList.get(i) > quantityMax) {
				quantityMax = quantityList.get(i);
			}
		}
		
		if(quantitySum != 0) {
			quantityAvg = quantitySum / 12;
		}
		
		/* 매출 중 기간이 맞는 총액 리스트 구하기 */
		List<Integer> sumList = new ArrayList<>();
		for(int i = 0; i < searchList.size(); i++) {
			Integer sum = averageSalesDAO.selectSumList(searchList.get(i));
			
			if(sum == null) {
				sum = 0;
			}
			
			sumList.add(sum);
		}
		
		/* 총액 합계, 평균, 최댓값 구하기 */
		int sumSum = 0;
		double sumAvg = 0.0;
		int sumMax = 0;
		for(int i = 0; i <sumList.size(); i++) {
			sumSum += sumList.get(i);
			
			if(sumList.get(i) > sumMax) {
				sumMax = sumList.get(i);
			}
		}
		
		if(sumSum != 0) {
			sumAvg = sumSum / 12;
		}
		
		/* 영업기회 중 오늘을 기준으로 당월, 전월, 전전월 값을 가져온다 */
		List<Integer> tableBusinessOpportunityList = averageBusinessDAO.selectGraphBOList(averageCustomSearchDTO);
		
		/* 고객지원 월별 갯수 : 당월, 전월, 전전월, 3개월 평균 */
		List<Integer> tableClientSupportList = averageCustomDAO.selectGraphClientSupportList(averageCustomSearchDTO);
		
		int totalThreeMonth = 0;
			
		totalThreeMonth += tableClientSupportList.get(0);
		totalThreeMonth += tableClientSupportList.get(1);
		totalThreeMonth += tableClientSupportList.get(2);
		
		int threeMonthAverage = (totalThreeMonth / 3);			//3개월 평균을 내고 List에 넣어준다.
		
		tableClientSupportList.remove(2);
		tableClientSupportList.add(2, threeMonthAverage);
		
		/* Controller 로 보내는 map */
		Map<String, Object> map = new HashMap<>();
		map.put("deptList", deptList);
		map.put("userList", userList);
		
		map.put("badgeCustomThisMonthList", badgeCustomThisMonthList);
		
		map.put("quantitySum", quantitySum);
		map.put("quantityAvg", quantityAvg);
		map.put("quantityMax", quantityMax);
		
		map.put("sumSum", sumSum);
		map.put("sumAvg", sumAvg);
		map.put("sumMax", sumMax);
		
		map.put("tableBusinessOpportunityList", tableBusinessOpportunityList);
		
		map.put("graphClientSupportList", tableClientSupportList);
		
		return map;
	}

}
