package com.codefactory.mnm.average.model.service;

import java.util.List;
import java.util.Map;

import com.codefactory.mnm.average.model.dto.AverageSalesSearchDTO;

public interface AverageSalesService {

	Map<String, Object> selectSalesList(List<AverageSalesSearchDTO> averageSalesSearchDTOList);

	Map<String, Object> selectSalesGroupList(List<AverageSalesSearchDTO> averageSalesSearchDTOList);

	Map<String, Object> selectSalesRepresentativeList(List<AverageSalesSearchDTO> averageSalesSearchDTOList);

}
