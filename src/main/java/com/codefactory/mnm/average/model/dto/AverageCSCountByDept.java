package com.codefactory.mnm.average.model.dto;

public class AverageCSCountByDept {
	
	private String deptNo;
	private String deptName;
	private int totalCount;
	
	public AverageCSCountByDept() {}

	public AverageCSCountByDept(String deptNo, String deptName, int totalCount) {
		super();
		this.deptNo = deptNo;
		this.deptName = deptName;
		this.totalCount = totalCount;
	}

	public String getDeptNo() {
		return deptNo;
	}

	public void setDeptNo(String deptNo) {
		this.deptNo = deptNo;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	@Override
	public String toString() {
		return "AverageCSCountByDept [deptNo=" + deptNo + ", deptName=" + deptName + ", totalCount=" + totalCount + "]";
	}
	
	

}
