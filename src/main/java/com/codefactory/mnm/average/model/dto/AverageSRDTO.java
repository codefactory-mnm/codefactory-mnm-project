package com.codefactory.mnm.average.model.dto;

import java.util.List;
import java.util.Map;

public class AverageSRDTO {
	
	private String userNo;
	private String name;
	private List<Integer> salesByResponsibility;			//일반적인 담당자별 판매 List
	private Map<String, Integer> sumMap;					//총 합계 판매 Map
	private int sum;
	
	public AverageSRDTO() {}

	public AverageSRDTO(String userNo, String name, List<Integer> salesByResponsibility, Map<String, Integer> sumMap,
			int sum) {
		super();
		this.userNo = userNo;
		this.name = name;
		this.salesByResponsibility = salesByResponsibility;
		this.sumMap = sumMap;
		this.sum = sum;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Integer> getSalesByResponsibility() {
		return salesByResponsibility;
	}

	public void setSalesByResponsibility(List<Integer> salesByResponsibility) {
		this.salesByResponsibility = salesByResponsibility;
	}

	public Map<String, Integer> getSumMap() {
		return sumMap;
	}

	public void setSumMap(Map<String, Integer> sumMap) {
		this.sumMap = sumMap;
	}

	public int getSum() {
		return sum;
	}

	public void setSum(int sum) {
		this.sum = sum;
	}

	@Override
	public String toString() {
		return "AverageSRDTO [userNo=" + userNo + ", name=" + name + ", salesByResponsibility=" + salesByResponsibility
				+ ", sumMap=" + sumMap + ", sum=" + sum + "]";
	}

	

	
}
