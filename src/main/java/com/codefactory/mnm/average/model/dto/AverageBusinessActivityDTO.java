package com.codefactory.mnm.average.model.dto;

import java.sql.Date;

public class AverageBusinessActivityDTO {
	
	private String businessActivityNo;			//영업활동번호
	private String activityClassification;		//활동분류
	private String activityPurpose;				//활동목적
	private java.sql.Date activityDate;			//날짜
	private String activityStartTime;			//활동시작시간
	private String activityEndTime;				//활동종료시간
	private String completeYn;					//완료여부
	private String planContent;					//계획내용
	private String activityContent;				//활동내용
	private String userNo;						//담당자번호
	private String userName;					//담당자명
	private java.sql.Date writeDate;			//작성일자
	private String businessOpportunityNo;		//영업기회번호
	
	public AverageBusinessActivityDTO() {}

	public AverageBusinessActivityDTO(String businessActivityNo, String activityClassification, String activityPurpose,
			Date activityDate, String activityStartTime, String activityEndTime, String completeYn, String planContent,
			String activityContent, String userNo, String userName, Date writeDate, String businessOpportunityNo) {
		super();
		this.businessActivityNo = businessActivityNo;
		this.activityClassification = activityClassification;
		this.activityPurpose = activityPurpose;
		this.activityDate = activityDate;
		this.activityStartTime = activityStartTime;
		this.activityEndTime = activityEndTime;
		this.completeYn = completeYn;
		this.planContent = planContent;
		this.activityContent = activityContent;
		this.userNo = userNo;
		this.userName = userName;
		this.writeDate = writeDate;
		this.businessOpportunityNo = businessOpportunityNo;
	}

	public String getBusinessActivityNo() {
		return businessActivityNo;
	}

	public void setBusinessActivityNo(String businessActivityNo) {
		this.businessActivityNo = businessActivityNo;
	}

	public String getActivityClassification() {
		return activityClassification;
	}

	public void setActivityClassification(String activityClassification) {
		this.activityClassification = activityClassification;
	}

	public String getActivityPurpose() {
		return activityPurpose;
	}

	public void setActivityPurpose(String activityPurpose) {
		this.activityPurpose = activityPurpose;
	}

	public java.sql.Date getActivityDate() {
		return activityDate;
	}

	public void setActivityDate(java.sql.Date activityDate) {
		this.activityDate = activityDate;
	}

	public String getActivityStartTime() {
		return activityStartTime;
	}

	public void setActivityStartTime(String activityStartTime) {
		this.activityStartTime = activityStartTime;
	}

	public String getActivityEndTime() {
		return activityEndTime;
	}

	public void setActivityEndTime(String activityEndTime) {
		this.activityEndTime = activityEndTime;
	}

	public String getCompleteYn() {
		return completeYn;
	}

	public void setCompleteYn(String completeYn) {
		this.completeYn = completeYn;
	}

	public String getPlanContent() {
		return planContent;
	}

	public void setPlanContent(String planContent) {
		this.planContent = planContent;
	}

	public String getActivityContent() {
		return activityContent;
	}

	public void setActivityContent(String activityContent) {
		this.activityContent = activityContent;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public java.sql.Date getWriteDate() {
		return writeDate;
	}

	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}

	public String getBusinessOpportunityNo() {
		return businessOpportunityNo;
	}

	public void setBusinessOpportunityNo(String businessOpportunityNo) {
		this.businessOpportunityNo = businessOpportunityNo;
	}

	@Override
	public String toString() {
		return "AverageBusinessActivityDTO [businessActivityNo=" + businessActivityNo + ", activityClassification="
				+ activityClassification + ", activityPurpose=" + activityPurpose + ", activityDate=" + activityDate
				+ ", activityStartTime=" + activityStartTime + ", activityEndTime=" + activityEndTime + ", completeYn="
				+ completeYn + ", planContent=" + planContent + ", activityContent=" + activityContent + ", userNo="
				+ userNo + ", userName=" + userName + ", writeDate=" + writeDate + ", businessOpportunityNo="
				+ businessOpportunityNo + "]";
	}

	
	
}
