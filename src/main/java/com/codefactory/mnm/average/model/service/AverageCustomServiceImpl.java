package com.codefactory.mnm.average.model.service;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.codefactory.mnm.average.model.dao.AverageCustomDAO;
import com.codefactory.mnm.average.model.dto.AverageClientDTO;
import com.codefactory.mnm.average.model.dto.AverageClientSupportDTO;
import com.codefactory.mnm.average.model.dto.AverageCompanyDTO;
import com.codefactory.mnm.average.model.dto.AverageCustomSearchDTO;
import com.codefactory.mnm.average.model.dto.AveragePotentialClientDTO;
import com.codefactory.mnm.goal.model.dao.GoalDAO;
import com.codefactory.mnm.goal.model.dto.DepartmentDTO;
import com.codefactory.mnm.goal.model.dto.UserDTO;

@Service("averageCustomService")
public class AverageCustomServiceImpl implements AverageCustomService {
	
	private final AverageCustomDAO averageCustomDAO; 
	private final GoalDAO goalDAO;
	
	@Autowired
	public AverageCustomServiceImpl(AverageCustomDAO averageCustomDAO, GoalDAO goalDAO) {
		this.averageCustomDAO = averageCustomDAO;
		this.goalDAO = goalDAO;
	}

	@Override
	public Map<String, Object> selectCustomList(AverageCustomSearchDTO averageCustomSearchDTO) {
		
		/* 1. 검색용 select box */
		/* 부서 List로 불러오기 */
		List<DepartmentDTO> deptList = goalDAO.findAllDept();
		
		/* 직원 List로 불러오기 */
		List<UserDTO> userList = goalDAO.findAllUser();
		
		/* 2. 출력용 내용 불러오기*/
		/* '고객사' 탭. 
		 * 고객사 중 기준에 맞는 전체 고객사 리스트를 가져온다. */
		List<AverageCompanyDTO> companyList = averageCustomDAO.selectCompanyList(averageCustomSearchDTO);
		
		/* '고객' 탭. 
		 * 고객 중 검색조건에 해당하는 리스트를 가져온다. */
		List<AverageClientDTO> clientList = averageCustomDAO.selectClientList(averageCustomSearchDTO);
		
		/* '잠재고객' 탭. 
		 * 잠재고객 중 검색조건에 해당하는 리스트를 가져온다. */
		List<AveragePotentialClientDTO> potentialClientList = averageCustomDAO.selectPotentialClientList(averageCustomSearchDTO);
		
		/* 가져온 고객리스트를 바탕으로 고객사에 담당자를 넣어준다. */
		for(int i = 0; i < companyList.size(); i++) {
			
			/* 중복되는 담당자는 제거하기위해 Map을 생성하고, List에 담아준다.
			 * 예컨대, C1의 clientCompanyNo가 CC1 인 경우, 
			 * C1의 담당자인 "U2" : "주찬빈"라는 엔트리를 하나 만들어서 Map에 넣어줌 */
			List<Map<String, String>> clientResponsibilityList = new ArrayList<>();
			Map<String, String> responsibilityMap = new HashMap<>();
			
			for (int j = 0; j < clientList.size(); j++) {
				
				if(clientList.get(j).getClientCompanyNo().equals(companyList.get(i).getClientCompanyNo())) {
					responsibilityMap.put(clientList.get(j).getUserNo(), clientList.get(j).getUserName());
				}
				
			}
			
			/* 중복없이 담은 담당자들을 String으로 담아준다 */
			String userNameStr = "";
			Iterator<String> keys = responsibilityMap.keySet().iterator();
			while(keys.hasNext()) {
				String key = keys.next();
				userNameStr += responsibilityMap.get(key) + " ";
			}
			
			clientResponsibilityList.add(responsibilityMap);
			companyList.get(i).setUserNameMap(responsibilityMap);
			companyList.get(i).setUserNameList(clientResponsibilityList);
			companyList.get(i).setUserNameStr(userNameStr);
			
		}
		
		/* '그래프' 탭의 신규고객 표 내용은 당월로 조건이 고정되어있다. 
		 * 오늘을 기준으로 당월, 전월, 전전월 값을 가져온다. */
		List<Integer> graphCustomList = averageCustomDAO.selectGraphCustomList(averageCustomSearchDTO);
		
		/* 3개월 평균을 내고 List에 넣어준다. */
		for(int i = 0; i < 3; i++) {
			
			int totalThreeMonth = 0;
			for(int j = 0; j < 3; j++) {
				totalThreeMonth += graphCustomList.get(i * 3 + j);
			}
			
			int threeMonthAverage = (totalThreeMonth / 3);
			
			graphCustomList.remove(i * 3 + 2);
			graphCustomList.add(i * 3 + 2, threeMonthAverage);
		}
		
		/* 고객등급에 따라서 센다. */
		List<Integer> customByGrade = new ArrayList<>();
		int sGradeCustom = 0;
		int aGradeCustom = 0;
		int bGradeCustom = 0;
		int cGradeCustom = 0;
		int dGradeCustom = 0;
		int unGradedCustom = 0;
		
		for(int i = 0; i < clientList.size(); i++) {
			
			switch(clientList.get(i).getGrade()) {
			case "S" : sGradeCustom++; break;
			case "A" : aGradeCustom++; break;
			case "B" : bGradeCustom++; break;
			case "C" : cGradeCustom++; break;
			case "D" : dGradeCustom++; break;
			default : unGradedCustom++; break;
			}
			
		}
		
		customByGrade.add(sGradeCustom);
		customByGrade.add(aGradeCustom);
		customByGrade.add(bGradeCustom);
		customByGrade.add(cGradeCustom);
		customByGrade.add(dGradeCustom);
		customByGrade.add(unGradedCustom);
		
		/* Controller 로 보내는 map */
		Map<String, Object> map = new HashMap<>();
		map.put("deptList", deptList);
		map.put("userList", userList);
		
		map.put("companyList", companyList);
		map.put("clientList", clientList);
		map.put("potentialClientList", potentialClientList);
		map.put("companyCountNo", companyList.size());
		map.put("clientCountNo", clientList.size());
		map.put("potentialClientCountNo", potentialClientList.size());
		
		map.put("graphCustomList", graphCustomList);
		map.put("customByGrade", customByGrade);
		
		return map;
	}

	@Override
	public Map<String, Object> selectClientSupportList(AverageCustomSearchDTO averageCustomSearchDTO) {
		
		/* 1. 검색용 select box */
		/* 부서 List로 불러오기 */
		List<DepartmentDTO> deptList = goalDAO.findAllDept();
		
		/* 직원 List로 불러오기 */
		List<UserDTO> userList = goalDAO.findAllUser();
		
		/* 2. 출력용 내용 불러오기 */
		/* 고객지원 리스트로 가져오기 */
		List<AverageClientSupportDTO> csList = averageCustomDAO.selectClientSupportList(averageCustomSearchDTO);
		
		/* 고객지원 월별 갯수 : 당월, 전월, 전전월, 3개월 평균 */
		List<Integer> graphClientSupportList = averageCustomDAO.selectGraphClientSupportList(averageCustomSearchDTO);
		
		int totalThreeMonth = 0;
			
		totalThreeMonth += graphClientSupportList.get(0);
		totalThreeMonth += graphClientSupportList.get(1);
		totalThreeMonth += graphClientSupportList.get(2);
		
		int threeMonthAverage = (totalThreeMonth / 3);			//3개월 평균을 내고 List에 넣어준다.
		
		graphClientSupportList.remove(2);
		graphClientSupportList.add(2, threeMonthAverage);
		
		/* 고객지원처리상태(PROCESSING_DIVISION)에 따라 분류
		 * 접수처리 완, 진행중, 완료건 */
		List<Integer> CSByProcess = new ArrayList<>();
		int proceedingCount = 0;
		int completeCount = 0;
		int elseCount = 0;
		
		for(int i = 0; i < csList.size(); i++) {
			
			switch(csList.get(i).getProcessingDivision()) {
			case "처리중" : proceedingCount++; break;
			case "완료" : completeCount++; break;
			default : elseCount++; break;
			}
			
		}
		CSByProcess.add(proceedingCount);
		CSByProcess.add(completeCount);
		CSByProcess.add(elseCount);
		
		/* Controller 로 보내는 map */
		Map<String, Object> map = new HashMap<>();
		map.put("deptList", deptList);
		map.put("userList", userList);
		
		map.put("csList", csList);
		map.put("graphClientSupportList", graphClientSupportList);
		map.put("CSByProcess", CSByProcess);
		
		return map;
	}

}
