package com.codefactory.mnm.average.model.dto;

public class AverageProductDTO {
	
	private String productNo;
	private String productName;
	
	public AverageProductDTO() {}

	public AverageProductDTO(String productNo, String productName) {
		super();
		this.productNo = productNo;
		this.productName = productName;
	}

	public String getProductNo() {
		return productNo;
	}

	public void setProductNo(String productNo) {
		this.productNo = productNo;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Override
	public String toString() {
		return "AverageProductDTO [productNo=" + productNo + ", productName=" + productName + "]";
	}
	
}
