package com.codefactory.mnm.average.model.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.codefactory.mnm.average.model.dao.AverageSalesDAO;
import com.codefactory.mnm.average.model.dto.AverageProductDTO;
import com.codefactory.mnm.average.model.dto.AverageSRDTO;
import com.codefactory.mnm.average.model.dto.AverageSalesSearchDTO;
import com.codefactory.mnm.goal.model.dao.GoalDAO;
import com.codefactory.mnm.goal.model.dto.DepartmentDTO;
import com.codefactory.mnm.goal.model.dto.UserDTO;

@Service("averageSalesService")
public class AverageSalesServiceImpl implements AverageSalesService {
	
	private final AverageSalesDAO averageSalesDAO;
	private final GoalDAO goalDAO;
	
	@Autowired
	public AverageSalesServiceImpl(AverageSalesDAO averageSalesDAO, GoalDAO goalDAO) {
		this.averageSalesDAO = averageSalesDAO;
		this.goalDAO = goalDAO;
	}

	@Override
	public Map<String, Object> selectSalesList(List<AverageSalesSearchDTO> searchList) {
		
		/* 매출 중 기간이 맞는 수량리스트 구하기 */
		List<Integer> quantityList = new ArrayList<>();
		for(int i = 0; i < searchList.size(); i++) {
			Integer quantity = averageSalesDAO.selectQuantityList(searchList.get(i));
			
			if(quantity == null) {
				quantity = 0;
			}
			
			quantityList.add(quantity);
		}
		
		/* 수량 합계, 평균, 최댓값 구하기 */
		int quantitySum = 0;
		double quantityAvg = 0.0;
		int quantityMax = 0;
		for(int i = 0; i < quantityList.size(); i++) {
			quantitySum += quantityList.get(i);
			
			if(quantityList.get(i) > quantityMax) {
				quantityMax = quantityList.get(i);
			}
		}
		
		if(quantitySum != 0) {
			quantityAvg = quantitySum / 12;
		}
		
		/* 매출 중 기간이 맞는 총액 리스트 구하기 */
		List<Integer> sumList = new ArrayList<>();
		for(int i = 0; i < searchList.size(); i++) {
			Integer sum = averageSalesDAO.selectSumList(searchList.get(i));
			
			if(sum == null) {
				sum = 0;
			}
			
			sumList.add(sum);
		}
		
		/* 총액 합계, 평균, 최댓값 구하기 */
		int sumSum = 0;
		double sumAvg = 0.0;
		int sumMax = 0;
		for(int i = 0; i <sumList.size(); i++) {
			sumSum += sumList.get(i);
			
			if(sumList.get(i) > sumMax) {
				sumMax = sumList.get(i);
			}
		}
		
		if(sumSum != 0) {
			sumAvg = sumSum / 12;
		}
		
		/* Controller 로 보내는 map */
		Map<String, Object> map = new HashMap<>();
		map.put("quantityList", quantityList);
		map.put("quantitySum", quantitySum);
		map.put("quantityAvg", quantityAvg);
		map.put("quantityMax", quantityMax);
		
		map.put("sumList", sumList);
		map.put("sumSum", sumSum);
		map.put("sumAvg", sumAvg);
		map.put("sumMax", sumMax);
		
		return map;
	}
	

	@Override
	public Map<String, Object> selectSalesGroupList(List<AverageSalesSearchDTO> searchList) {
		
		/* 1. 검색용 select box */
		/* 부서 List로 불러오기 */
		List<DepartmentDTO> deptList = goalDAO.findAllDept();
		
		/* 상품 List로 불러오기 */
		List<AverageProductDTO> productList = averageSalesDAO.findAllProduct();
		
		/* 2. 출력용 내용 가져오기 */
		/* 매출 중 기간이 맞는 총액 리스트 구하기 */
		List<Integer> sumListByGroup = new ArrayList<>();
		for(int i = 0; i < searchList.size(); i++) {
			Integer sumByGroup = averageSalesDAO.selectSumListByGroup(searchList.get(i));
			
			if(sumByGroup == null) {
				sumByGroup = 0;
			}
			
			sumListByGroup.add(sumByGroup);
		}
		
		/* 목표 중 기간이 맞는 총액 리스트 구하기 */
		List<Integer> goalListByGroup = new ArrayList<>();
		for(int i = 0; i < searchList.size(); i++) {
			Integer goalByGroup = averageSalesDAO.selectGoalListByGroup(searchList.get(i));
			
			if(goalByGroup == null) {
				goalByGroup = 0;
			}
			
			goalListByGroup.add(goalByGroup);
		}
		
		/* Controller 로 보내는 map */
		Map<String, Object> map = new HashMap<>();
		map.put("deptList", deptList);
		map.put("productList", productList);
		
		map.put("sumListByGroup", sumListByGroup);
		map.put("goalListByGroup", goalListByGroup);
		
		return map;
	}

	@Override
	public Map<String, Object> selectSalesRepresentativeList(List<AverageSalesSearchDTO> searchDTOList) {
		
		/* 검색용 select box에 넣을 전체 직원 List */
		List<UserDTO> AllUserList = goalDAO.findAllUser();
		
		/* 담당자는 모든 DTO마다 똑같을 것이므로, 임의번째 DTO에서 담당자를 가져온다 */
		String responsibility = searchDTOList.get(0).getResponsibility();	
		
		/* 담당자 리스트 가져오기 
		 * 기준이 있다면, 그에 해당하는 담당자를, 없다면 전체를 가져온다 */
		List<UserDTO> userList = averageSalesDAO.findUser(responsibility);
		
		/* userList에서 가져온 담당자마다 Sales DTO를 가져오고, 해당 DTO와 정보들을 srListList에 담는다 */
		List<AverageSRDTO> srListList = new ArrayList<>();
		for(int i = 0; i < userList.size(); i++) {
			
			AverageSRDTO averageSRDTO = new AverageSRDTO();
			
			/* userList별로 담아줄 월별 salesList를 만든다 */
			List<Integer> salesByResponsibilityList = new ArrayList<>();
			
			int sum = 0;
			
			/* 해당 담당자 리스트를 기준 삼아서 판매현황을 List로 가져온다 
			 * (12달이므로, searchDTOList의 길이는 11이다) */
			for(int j = 0; j < searchDTOList.size(); j++) {
				
				/* 검색조건의 담당자번호를, 가져온 담당자리스트의 UserNo로 바꿔준다 */
				searchDTOList.get(j).setResponsibility(userList.get(i).getUserNo());
				
				Integer salesByResponsibility = averageSalesDAO.selectSalesRepresentative(searchDTOList.get(j));
				
				if(salesByResponsibility == null) {
					salesByResponsibility = 0;
				}
				
				salesByResponsibilityList.add(salesByResponsibility);
				
				sum += salesByResponsibility;
			}
			
			averageSRDTO.setUserNo(userList.get(i).getUserNo());
			averageSRDTO.setName(userList.get(i).getName());
			averageSRDTO.setSalesByResponsibility(salesByResponsibilityList);
			averageSRDTO.setSum(sum);
			
			srListList.add(averageSRDTO);
		}
		
		/* 차트로 그릴 총합계 List를 만든다. */
		AverageSRDTO averageAllSumSRDTO = new AverageSRDTO();
		
		Map<String, Integer> sumMap = new LinkedHashMap();
		
		int totalSum = 0;
		for(int j = 0; j < 12; j++) {
			int sum = 0;
			
			for(int i = 0; i < srListList.size(); i++) {
				sum += srListList.get(i).getSalesByResponsibility().get(j);
			}
			
			sumMap.put(j + "", sum);
			
			totalSum += sum;
		}
		averageAllSumSRDTO.setName("합계");
		averageAllSumSRDTO.setSumMap(sumMap);
		averageAllSumSRDTO.setSum(totalSum);
		
		/* Controller 로 보내는 map */
		Map<String, Object> map = new HashMap<>();
		map.put("AllUserList", AllUserList);
		
		map.put("srListList", srListList);
		map.put("averageAllSumSRDTO", averageAllSumSRDTO);
		
		return map;
	}	

}
