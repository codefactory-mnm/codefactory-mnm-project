package com.codefactory.mnm.average.model.service;

import java.util.Map;

import com.codefactory.mnm.average.model.dto.AverageCustomSearchDTO;

public interface AverageMainService {

	Map<String, Object> selectAverageMainList(Map<String, Object> searchCriteria);

}
