package com.codefactory.mnm.average.model.dto;

import java.sql.Date;

public class AverageBusinessOpportunityDTO {
	
	private String businessOpportunityNo;		//영업기회번호
	private String businessOpportunityName;		//영업기회명
	private String expectedSales;				//예상매출
	private String businessType;				//사업유형
	private java.sql.Date businessStartDate; 	//영업시작일
	private java.sql.Date businessEndDate;		//영업종료일
	private String note;						//비고
	private String salesDivision;				//매출구분
	private String cognitivePathway;			//인지경로
	private String userNo;						//담당자번호
	private String userName;					//담당자명
	private String clientNo;					//고객번호
	private String clientName;					//고객명
	private java.sql.Date writeDate;			//작성일자
	
	public AverageBusinessOpportunityDTO() {}

	public AverageBusinessOpportunityDTO(String businessOpportunityNo, String businessOpportunityName,
			String expectedSales, String businessType, Date businessStartDate, Date businessEndDate, String note,
			String salesDivision, String cognitivePathway, String userNo, String userName, String clientNo,
			String clientName, Date writeDate) {
		super();
		this.businessOpportunityNo = businessOpportunityNo;
		this.businessOpportunityName = businessOpportunityName;
		this.expectedSales = expectedSales;
		this.businessType = businessType;
		this.businessStartDate = businessStartDate;
		this.businessEndDate = businessEndDate;
		this.note = note;
		this.salesDivision = salesDivision;
		this.cognitivePathway = cognitivePathway;
		this.userNo = userNo;
		this.userName = userName;
		this.clientNo = clientNo;
		this.clientName = clientName;
		this.writeDate = writeDate;
	}

	public String getBusinessOpportunityNo() {
		return businessOpportunityNo;
	}

	public void setBusinessOpportunityNo(String businessOpportunityNo) {
		this.businessOpportunityNo = businessOpportunityNo;
	}

	public String getBusinessOpportunityName() {
		return businessOpportunityName;
	}

	public void setBusinessOpportunityName(String businessOpportunityName) {
		this.businessOpportunityName = businessOpportunityName;
	}

	public String getExpectedSales() {
		return expectedSales;
	}

	public void setExpectedSales(String expectedSales) {
		this.expectedSales = expectedSales;
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public java.sql.Date getBusinessStartDate() {
		return businessStartDate;
	}

	public void setBusinessStartDate(java.sql.Date businessStartDate) {
		this.businessStartDate = businessStartDate;
	}

	public java.sql.Date getBusinessEndDate() {
		return businessEndDate;
	}

	public void setBusinessEndDate(java.sql.Date businessEndDate) {
		this.businessEndDate = businessEndDate;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getSalesDivision() {
		return salesDivision;
	}

	public void setSalesDivision(String salesDivision) {
		this.salesDivision = salesDivision;
	}

	public String getCognitivePathway() {
		return cognitivePathway;
	}

	public void setCognitivePathway(String cognitivePathway) {
		this.cognitivePathway = cognitivePathway;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getClientNo() {
		return clientNo;
	}

	public void setClientNo(String clientNo) {
		this.clientNo = clientNo;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public java.sql.Date getWriteDate() {
		return writeDate;
	}

	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}

	@Override
	public String toString() {
		return "AverageBusinessOpportunityDTO [businessOpportunityNo=" + businessOpportunityNo
				+ ", businessOpportunityName=" + businessOpportunityName + ", expectedSales=" + expectedSales
				+ ", businessType=" + businessType + ", businessStartDate=" + businessStartDate + ", businessEndDate="
				+ businessEndDate + ", note=" + note + ", salesDivision=" + salesDivision + ", cognitivePathway="
				+ cognitivePathway + ", userNo=" + userNo + ", userName=" + userName + ", clientNo=" + clientNo
				+ ", clientName=" + clientName + ", writeDate=" + writeDate + "]";
	}


}
