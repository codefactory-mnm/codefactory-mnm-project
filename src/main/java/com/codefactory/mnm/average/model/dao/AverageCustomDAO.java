package com.codefactory.mnm.average.model.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.codefactory.mnm.average.model.dto.AverageClientDTO;
import com.codefactory.mnm.average.model.dto.AverageClientSupportDTO;
import com.codefactory.mnm.average.model.dto.AverageCompanyDTO;
import com.codefactory.mnm.average.model.dto.AverageCustomSearchDTO;
import com.codefactory.mnm.average.model.dto.AveragePotentialClientDTO;
import com.codefactory.mnm.goal.model.dto.DepartmentDTO;
import com.codefactory.mnm.goal.model.dto.UserDTO;

@Mapper
public interface AverageCustomDAO {
	
	List<Integer> selectGraphCustomList(AverageCustomSearchDTO averageCustomSearchDTO);
	
	List<AverageCompanyDTO> selectCompanyList(AverageCustomSearchDTO averageCustomSearchDTO);

	List<AverageClientDTO> selectClientList(AverageCustomSearchDTO averageCustomSearchDTO);

	List<AveragePotentialClientDTO> selectPotentialClientList(AverageCustomSearchDTO averageCustomSearchDTO);

	List<AverageClientSupportDTO> selectClientSupportList(AverageCustomSearchDTO averageCustomSearchDTO);

	List<Integer> selectGraphClientSupportList(AverageCustomSearchDTO averageCustomSearchDTO);

}
