package com.codefactory.mnm.average.controller;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.codefactory.mnm.average.model.dto.AverageBusinessActivityDTO;
import com.codefactory.mnm.average.model.dto.AverageBusinessOpportunityDTO;
import com.codefactory.mnm.average.model.dto.AverageCustomSearchDTO;
import com.codefactory.mnm.average.model.service.AverageBusinessService;
import com.codefactory.mnm.goal.model.dto.DepartmentDTO;
import com.codefactory.mnm.goal.model.dto.UserDTO;
import com.codefactory.mnm.member.model.dto.UserImpl;

@Controller
@RequestMapping("/average")
public class AverageBusinessController {
	
	private AverageBusinessService averageBusinessService;
	
	@Autowired
	public AverageBusinessController(AverageBusinessService averageBusinessService) {
		this.averageBusinessService = averageBusinessService;
	}
	
	@GetMapping("/businessOpportunity")
	public ModelAndView selectbusinessOpportunityAverage(ModelAndView mv, 
			@AuthenticationPrincipal UserImpl userImpl, 
			@RequestParam(defaultValue = "") String startDate,
			@RequestParam(defaultValue = "") String endDate,
			@RequestParam(defaultValue = "") String deptName,
			@RequestParam(defaultValue = "") String responsibility) { 
		
		/* 오늘날짜의 연도 추출 */
		LocalDate currentDate = LocalDate.now();
		String todayDate = currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String[] todaySplit = todayDate.split("-");
		
		LocalDate startLocalDate = YearMonth.now().atDay(1); 		//오늘이 속한 달의 첫째 날
		
		/* 쿼리문에 검색조건으로 사용할 날짜변수 선언
		 * 날짜를 입력하지않으면 모든갯수를 다 가져온다. */
		java.sql.Date startDateSql = null;
		java.sql.Date endDateSql = null;
		
		/* 검색조건을 넣어준다 */
		if(!startDate.equals("")) { 				// 검색조건 중 시작일자를 입력한 경우
			startDateSql = java.sql.Date.valueOf(startDate);
		}

		if(!endDate.equals("")) { 					// 검색조건 중 종료일자를 입력한 경우
			endDateSql = java.sql.Date.valueOf(endDate);
		}
		
		if(deptName.equals("")) { 					// 검색조건 중 부서명을 선택하지 않았을 경우
			deptName = null;
		}

		if(responsibility.equals("")) { 			// 검색조건 중 담당자를 선택하지 않았을 경우
			responsibility = null;
		}
		
		/* 검색조건을 DTO에 담는다 */
		AverageCustomSearchDTO averageCustomSearchDTO = new AverageCustomSearchDTO();
		averageCustomSearchDTO.setStartDate(startDateSql);
		averageCustomSearchDTO.setEndDate(endDateSql);
		averageCustomSearchDTO.setDeptNo(deptName);
		averageCustomSearchDTO.setResponsibility(responsibility);
		
		//검색조건을 searvice로 보내주고, 값을 map으로 전달 
		Map<String, Object> map = averageBusinessService.selectbusinessOpportunityList(averageCustomSearchDTO);
		
		List<DepartmentDTO> deptList = (List<DepartmentDTO>)map.get("deptList"); 	//검색용으로 쓸 부서리스트
		List<UserDTO> userList = (List<UserDTO>)map.get("userList");				//검색용으로 쓸 직원리스트
		
		List<AverageBusinessOpportunityDTO> businessOpportunityList = (List<AverageBusinessOpportunityDTO>)map.get("boList");	
		List<Integer> graphBusinessOpportunityList = (List<Integer>)map.get("graphBusinessOpportunityList");
		
		int civilianType = (int)map.get("civilianType");							//민수사업
		int officialType = (int)map.get("officialType");							//관공사업
		int productType = (int)map.get("productType");								//상품매출
		int commissionType = (int)map.get("commissionType");						//커미션매출
		
		int acquaintance = (int)map.get("acquaintance");							//지인소개
		int newsPaper = (int)map.get("newsPaper");									//신문
		int internet = (int)map.get("internet");									//인터넷검색
		int preexistence = (int)map.get("preexistence");							//기존고객
		int etc = (int)map.get("etc");												//기타
		
		/* mv로 넘길 값 add */
		mv.addObject("startLocalDate", startLocalDate);
		mv.addObject("deptList", deptList);
		mv.addObject("userList", userList);
		
		mv.addObject("businessOpportunityList", businessOpportunityList);
		mv.addObject("graphBusinessOpportunityList", graphBusinessOpportunityList);
		
		mv.addObject("civilianType", civilianType);
		mv.addObject("officialType", officialType);
		mv.addObject("productType", productType);
		mv.addObject("commissionType", commissionType);
		
		mv.addObject("acquaintance", acquaintance);	
		mv.addObject("newsPaper", newsPaper);		
		mv.addObject("internet", internet);		
		mv.addObject("preexistence", preexistence);
		mv.addObject("etc", etc);
		
		mv.setViewName("average/businessOpportunity");
		
		return mv;
	}
	
	@GetMapping("/businessActivity")
	public ModelAndView selectbusinessActivityAverage(ModelAndView mv, 
			@AuthenticationPrincipal UserImpl userImpl, 
			@RequestParam(defaultValue = "") String startDate,
			@RequestParam(defaultValue = "") String endDate,
			@RequestParam(defaultValue = "") String deptName,
			@RequestParam(defaultValue = "") String responsibility) { 
		
		/* 오늘날짜의 연도 추출 */
		LocalDate currentDate = LocalDate.now();
		String todayDate = currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String[] todaySplit = todayDate.split("-");
		
		LocalDate startLocalDate = YearMonth.now().atDay(1); 		//오늘이 속한 달의 첫째 날
		
		/* 쿼리문에 검색조건으로 사용할 날짜변수 선언
		 * 날짜를 입력하지않으면 모든갯수를 다 가져온다. */
		java.sql.Date startDateSql = null;
		java.sql.Date endDateSql = null;
		
		/* 검색조건을 넣어준다 */
		if(!startDate.equals("")) { 								// 검색조건 중 시작일자를 입력한 경우
			startDateSql = java.sql.Date.valueOf(startDate);
		}

		if(!endDate.equals("")) { 									// 검색조건 중 종료일자를 입력한 경우
			endDateSql = java.sql.Date.valueOf(endDate);
		}
		
		if(deptName.equals("")) { 									// 검색조건 중 부서명을 선택하지 않았을 경우
			deptName = null;
		}

		if(responsibility.equals("")) { 							// 검색조건 중 담당자를 선택하지 않았을 경우
			responsibility = null;
		}
		
		/* 검색조건을 DTO에 담는다 */
		AverageCustomSearchDTO averageCustomSearchDTO = new AverageCustomSearchDTO();
		averageCustomSearchDTO.setStartDate(startDateSql);
		averageCustomSearchDTO.setEndDate(endDateSql);
		averageCustomSearchDTO.setDeptNo(deptName);
		averageCustomSearchDTO.setResponsibility(responsibility);
		
		//검색조건을 searvice로 보내주고, 값을 map으로 전달 
		Map<String, Object> map = averageBusinessService.selectbusinessActivityList(averageCustomSearchDTO);
		
		List<DepartmentDTO> deptList = (List<DepartmentDTO>)map.get("deptList"); 	//검색용으로 쓸 부서리스트
		List<UserDTO> userList = (List<UserDTO>)map.get("userList");				//검색용으로 쓸 직원리스트
		
		List<AverageBusinessActivityDTO> businessActivityList = (List<AverageBusinessActivityDTO>)map.get("baList");
		List<Integer> tableBusinessActivityList = (List<Integer>)map.get("tableBusinessActivityList");
		
		int visitType = (int)map.get("visitType");									//방문
		int telephoneType = (int)map.get("telephoneType");							//전화
		int greetingType = (int)map.get("greetingType");							//인사
		int salesType = (int)map.get("salesType");									//영업
		
		mv.addObject("startLocalDate", startLocalDate);
		mv.addObject("deptList", deptList);
		mv.addObject("userList", userList);
		
		mv.addObject("businessActivityList", businessActivityList);
		mv.addObject("tableBusinessActivityList", tableBusinessActivityList);
		
		mv.addObject("visitType", visitType);
		mv.addObject("telephoneType", telephoneType);
		mv.addObject("greetingType", greetingType);
		mv.addObject("salesType", salesType);
		
		mv.setViewName("average/businessActivity");
		
		return mv;
	}
	
	
}
