package com.codefactory.mnm.average.controller;

import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.codefactory.mnm.average.model.dto.AverageClientSupportDTO;
import com.codefactory.mnm.average.model.dto.AverageProductDTO;
import com.codefactory.mnm.average.model.dto.AverageSRDTO;
import com.codefactory.mnm.average.model.dto.AverageSalesSearchDTO;
import com.codefactory.mnm.average.model.service.AverageSalesService;
import com.codefactory.mnm.goal.model.dto.DepartmentDTO;
import com.codefactory.mnm.goal.model.dto.UserDTO;
import com.codefactory.mnm.member.model.dto.UserImpl;

@Controller
@RequestMapping("/average")
public class AverageSalesController {

	private AverageSalesService averageSalesService;
	
	@Autowired
	public AverageSalesController(AverageSalesService averageSalesService) {
		this.averageSalesService = averageSalesService;
	}
	
	/* 판매 통계 */
	@GetMapping("/sales")
	public ModelAndView selectSalesAverage(ModelAndView mv,
			@RequestParam(defaultValue = "") String year) {
		
		/* 오늘날짜의 연도 추출 */
		LocalDate currentDate = LocalDate.now();
		String todayDate = currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String[] todaySplit = todayDate.split("-");
		
		int todayYear = Integer.parseInt(todaySplit[0]);
		
		/* 3개년치 해를 리스트에 담기 */
		List<Integer> yearList = new ArrayList<>();
		for(int i = 0; i < 3; i++) {
			yearList.add(todayYear - i);
		}
		
		/* 검색조건을 넣어준다 
		 * 매출발생시점은 '출고일'이다. */
		List<AverageSalesSearchDTO> averageSalesSearchDTOList = new ArrayList<>();
		java.sql.Date searchStartDate = null;
		java.sql.Date searchEndDate = null;
		
		/* 검색연도를 입력한 경우 */
		if(!year.equals("")) {
			
			for(int i = 1; i < 13; i++) {
				AverageSalesSearchDTO searchDTO = new AverageSalesSearchDTO();
				
				/* 10보다 작은 월일 경우 */
				if(i < 9) {																		
					String getMonthStartDate = year + "-0" + i + "-01";							//i월 첫날
					String getNextMonthStartDate = year + "-0" + (i + 1) + "-01";				//i+1월의 첫날
					
					LocalDate getNextMonthStartDateLoc = LocalDate.parse(getNextMonthStartDate);
					LocalDate getMonthEndDate = getNextMonthStartDateLoc.minusDays(1);			//i월의 마지막 날짜
					
					searchStartDate = Date.valueOf(getMonthStartDate);
					searchEndDate = Date.valueOf(getMonthEndDate);
					
					/* 검색조건 DTO에 담아준다 */
					searchDTO.setMonthStartDate(searchStartDate);
					searchDTO.setMonthLastDate(searchEndDate);
					
					averageSalesSearchDTOList.add(searchDTO);
					
				/* 10보다 큰 월일 경우 */
				} else if (i < 12) {
					String getMonthStartDate = year + "-" + i + "-01";							//i월 첫날
					String getNextMonthStartDate = year + "-" + (i + 1) + "-01";				//i+1월의 첫날
					
					LocalDate getNextMonthStartDateLoc = LocalDate.parse(getNextMonthStartDate);
					LocalDate getMonthEndDate = getNextMonthStartDateLoc.minusDays(1);			//i월의 마지막 날짜
					
					searchStartDate = Date.valueOf(getMonthStartDate);
					searchEndDate = Date.valueOf(getMonthEndDate);
					
					/* 검색조건 DTO에 담아준다 */
					searchDTO.setMonthStartDate(searchStartDate);
					searchDTO.setMonthLastDate(searchEndDate);
					
					averageSalesSearchDTOList.add(searchDTO);
				
				/* 12월의 경우 */
				} else {
					String getMonthStartDate = year + "-" + i + "-01";							//i월 첫날
					String getMonthEndtDate = year + "-" + i + "-31";							//i월의 마지막 날짜
					
					searchStartDate = Date.valueOf(getMonthStartDate);
					searchEndDate = Date.valueOf(getMonthEndtDate);
					
					/* 검색조건 DTO에 담아준다 */
					searchDTO.setMonthStartDate(searchStartDate);
					searchDTO.setMonthLastDate(searchEndDate);
					
					averageSalesSearchDTOList.add(searchDTO);
				}
			}
		
		/* 검색연도를 입력하지 않은 경우 */
		} else {	 	
			
			for(int i = 1; i < 13; i++) {
				AverageSalesSearchDTO searchDTO = new AverageSalesSearchDTO();
				
				if(i < 9) {																			//10보다 작은 월일 경우.
					String getMonthStartDate = todayYear + "-0" + i + "-01";						//i월 첫날
					String getNextMonthStartDate = todayYear + "-0" + (i + 1) + "-01";				//i+1월의 첫날
					
					LocalDate getNextMonthStartDateLoc = LocalDate.parse(getNextMonthStartDate);
					LocalDate getMonthEndDate = getNextMonthStartDateLoc.minusDays(1);				//i월의 마지막 날짜
					
					searchStartDate = Date.valueOf(getMonthStartDate);
					searchEndDate = Date.valueOf(getMonthEndDate);
					
					/* 검색조건 DTO에 담아준다 */
					searchDTO.setMonthStartDate(searchStartDate);
					searchDTO.setMonthLastDate(searchEndDate);
					
					averageSalesSearchDTOList.add(searchDTO);
					
				} else if (i < 12) {																//10보다 큰 월일 경우.
					String getMonthStartDate = todayYear + "-" + i + "-01";							//i월 첫날
					String getNextMonthStartDate = todayYear + "-" + (i + 1) + "-01";				//i+1월의 첫날
					
					LocalDate getNextMonthStartDateLoc = LocalDate.parse(getNextMonthStartDate);
					LocalDate getMonthEndDate = getNextMonthStartDateLoc.minusDays(1);				//i월의 마지막 날짜
					
					searchStartDate = Date.valueOf(getMonthStartDate);
					searchEndDate = Date.valueOf(getMonthEndDate);
					
					/* 검색조건 DTO에 담아준다 */
					searchDTO.setMonthStartDate(searchStartDate);
					searchDTO.setMonthLastDate(searchEndDate);
					
					averageSalesSearchDTOList.add(searchDTO);
				
				/* 12월의 경우 */
				} else {
					String getMonthStartDate = todayYear + "-" + i + "-01";							//i월 첫날
					String getMonthEndtDate = todayYear + "-" + i + "-31";							//i월의 마지막 날짜
					
					searchStartDate = Date.valueOf(getMonthStartDate);
					searchEndDate = Date.valueOf(getMonthEndtDate);
					
					/* 검색조건 DTO에 담아준다 */
					searchDTO.setMonthStartDate(searchStartDate);
					searchDTO.setMonthLastDate(searchEndDate);
					
					averageSalesSearchDTOList.add(searchDTO);
				}
			}
		
		}
		
		/* 검색조건을 searvice로 보내주고, 값을 map으로 전달 */ 
		Map<String, Object> map = averageSalesService.selectSalesList(averageSalesSearchDTOList);
		
		List<Integer> quantityList = (List<Integer>)map.get("quantityList");
		int quantitySum = (int)map.get("quantitySum");
		double quantityAvg = (double)map.get("quantityAvg");
		int quantityMax = (int)map.get("quantityMax");
		List<Integer> sumList = (List<Integer>)map.get("sumList");
		int sumSum = (int)map.get("sumSum");
		double sumAvg = (double)map.get("sumAvg");
		int sumMax = (int)map.get("sumMax");
		
		/* 넘길 값 add */		
		if(!year.equals("")) { 								//검색조건으로 설정한 게 있는 경우
			mv.addObject("year", year);
		} else {
			mv.addObject("year", todayYear);
		}
		
		mv.addObject("yearList", yearList);
		
		mv.addObject("quantityList", quantityList);
		mv.addObject("quantitySum", quantitySum);
		mv.addObject("quantityAvg", quantityAvg);
		mv.addObject("quantityMax", quantityMax);
		mv.addObject("sumList", sumList);
		mv.addObject("sumSum", sumSum);
		mv.addObject("sumAvg", sumAvg);
		mv.addObject("sumMax", sumMax);
		
		mv.setViewName("average/sales");
		
		return mv;
	}
	
	@GetMapping("/salesGroup")
	public ModelAndView selectSalesGroupAverage(ModelAndView mv, 
			@AuthenticationPrincipal UserImpl userImpl, 
			@RequestParam(defaultValue = "") String year,
			@RequestParam(defaultValue = "") String deptName,
			@RequestParam(defaultValue = "") String productNo) {
		
		if(deptName.equals("")) { 				// 검색조건 중 부서를 선택하지 않았을 경우
			deptName = null;
		}
		
		if(productNo.equals("")) { 				// 검색조건 중 상품을 선택하지 않았을 경우
			productNo = null;
		}
		
		/* 오늘날짜의 연도 추출 */
		LocalDate currentDate = LocalDate.now();
		String todayDate = currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String[] todaySplit = todayDate.split("-");
		
		int todayYear = Integer.parseInt(todaySplit[0]);
		
		/* 3개년치 해를 리스트에 담기 */
		List<Integer> yearList = new ArrayList<>();
		for(int i = 0; i < 3; i++) {
			yearList.add(todayYear - i);
		}
		
		/* 검색조건을 넣어준다 
		 * 매출발생시점은 '출고일'이다. */
		List<AverageSalesSearchDTO> averageSalesSearchDTOList = new ArrayList<>();
		java.sql.Date searchStartDate = null;
		java.sql.Date searchEndDate = null;
		
		/* 검색연도를 입력한 경우 */
		if(!year.equals("")) { 								
			
			for(int i = 1; i < 13; i++) {
				AverageSalesSearchDTO searchDTO = new AverageSalesSearchDTO();
				
				/* 10보다 작은 월일 경우 */
				if(i < 9) {																	
					String getMonthStartDate = year + "-0" + i + "-01";								//i월 첫날
					String getNextMonthStartDate = year + "-0" + (i + 1) + "-01";					//i+1월의 첫날
					
					LocalDate getNextMonthStartDateLoc = LocalDate.parse(getNextMonthStartDate);
					LocalDate getMonthEndDate = getNextMonthStartDateLoc.minusDays(1);				//i월의 마지막 날짜
					
					searchStartDate = Date.valueOf(getMonthStartDate);
					searchEndDate = Date.valueOf(getMonthEndDate);
					
					/* 검색조건 DTO에 담아준다 */
					searchDTO.setMonthStartDate(searchStartDate);
					searchDTO.setMonthLastDate(searchEndDate);
					searchDTO.setDeptName(deptName);
					searchDTO.setProductNo(productNo);
					searchDTO.setYear(Integer.parseInt(year));
					searchDTO.setMonth(i);
					
					averageSalesSearchDTOList.add(searchDTO);
				
				/* 10보다 큰 월일 경우 */
				} else if (i < 12) {																
					String getMonthStartDate = year + "-" + i + "-01";								//i월 첫날
					String getNextMonthStartDate = year + "-" + (i + 1) + "-01";					//i+1월의 첫날
					System.out.println(i + " + 1월의 첫 날 : " + getNextMonthStartDate);
					
					LocalDate getNextMonthStartDateLoc = LocalDate.parse(getNextMonthStartDate);
					LocalDate getMonthEndDate = getNextMonthStartDateLoc.minusDays(1);				//i월의 마지막 날짜
					
					searchStartDate = Date.valueOf(getMonthStartDate);
					searchEndDate = Date.valueOf(getMonthEndDate);
					
					/* 검색조건 DTO에 담아준다 */
					searchDTO.setMonthStartDate(searchStartDate);
					searchDTO.setMonthLastDate(searchEndDate);
					searchDTO.setDeptName(deptName);
					searchDTO.setProductNo(productNo);
					searchDTO.setYear(Integer.parseInt(year));
					searchDTO.setMonth(i);
					
					averageSalesSearchDTOList.add(searchDTO);
					
				/* 12월의 경우 */
				} else {
					String getMonthStartDate = year + "-" + i + "-01";							//i월 첫날
					String getMonthEndtDate = year + "-" + i + "-31";							//i월의 마지막 날짜
					
					searchStartDate = Date.valueOf(getMonthStartDate);
					searchEndDate = Date.valueOf(getMonthEndtDate);
					
					/* 검색조건 DTO에 담아준다 */
					searchDTO.setMonthStartDate(searchStartDate);
					searchDTO.setMonthLastDate(searchEndDate);
					searchDTO.setDeptName(deptName);
					searchDTO.setProductNo(productNo);
					searchDTO.setYear(Integer.parseInt(year));
					searchDTO.setMonth(i);
					
					averageSalesSearchDTOList.add(searchDTO);
				}
			}
		
		/* 검색연도를 입력하지 않은 경우 */
		} else {	 										
			
			for(int i = 1; i < 13; i++) {
				AverageSalesSearchDTO searchDTO = new AverageSalesSearchDTO();
				
				/* 10보다 작은 월일 경우 */
				if(i < 9) {																	
					String getMonthStartDate = todayYear + "-0" + i + "-01";						//i월 첫날
					String getNextMonthStartDate = todayYear + "-0" + (i + 1) + "-01";				//i+1월의 첫날
					
					LocalDate getNextMonthStartDateLoc = LocalDate.parse(getNextMonthStartDate);
					LocalDate getMonthEndDate = getNextMonthStartDateLoc.minusDays(1);				//i월의 마지막 날짜
					
					searchStartDate = Date.valueOf(getMonthStartDate);
					searchEndDate = Date.valueOf(getMonthEndDate);
					
					/* 검색조건 DTO에 담아준다 */
					searchDTO.setMonthStartDate(searchStartDate);
					searchDTO.setMonthLastDate(searchEndDate);
					searchDTO.setDeptName(deptName);
					searchDTO.setProductNo(productNo);
					searchDTO.setYear(todayYear);
					searchDTO.setMonth(i);
					
					averageSalesSearchDTOList.add(searchDTO);
					
				/* 10보다 큰 월일 경우 */
				} else if (i < 12) {															
					String getMonthStartDate = todayYear + "-" + i + "-01";							//i월 첫날
					String getNextMonthStartDate = todayYear + "-" + (i + 1) + "-01";				//i+1월의 첫날
					System.out.println(i + " + 1)월의 첫 날 : " + getNextMonthStartDate);
					
					LocalDate getNextMonthStartDateLoc = LocalDate.parse(getNextMonthStartDate);
					LocalDate getMonthEndDate = getNextMonthStartDateLoc.minusDays(1);				//i월의 마지막 날짜
					
					searchStartDate = Date.valueOf(getMonthStartDate);
					searchEndDate = Date.valueOf(getMonthEndDate);
					
					/* 검색조건 DTO에 담아준다 */
					searchDTO.setMonthStartDate(searchStartDate);
					searchDTO.setMonthLastDate(searchEndDate);
					searchDTO.setDeptName(deptName);
					searchDTO.setProductNo(productNo);
					searchDTO.setYear(todayYear);
					searchDTO.setMonth(i);
					
					averageSalesSearchDTOList.add(searchDTO);
					
				/* 12월의 경우 */
				} else {
					String getMonthStartDate = todayYear + "-" + i + "-01";							//i월 첫날
					String getMonthEndtDate = todayYear + "-" + i + "-31";							//i월의 마지막 날짜
					
					searchStartDate = Date.valueOf(getMonthStartDate);
					searchEndDate = Date.valueOf(getMonthEndtDate);
					
					/* 검색조건 DTO에 담아준다 */
					searchDTO.setMonthStartDate(searchStartDate);
					searchDTO.setMonthLastDate(searchEndDate);
					searchDTO.setDeptName(deptName);
					searchDTO.setProductNo(productNo);
					searchDTO.setYear(todayYear);
					searchDTO.setMonth(i);
					
					averageSalesSearchDTOList.add(searchDTO);
				}
			}
		}
		
		//검색조건을 searvice로 보내주고, 값을 map으로 전달 
		Map<String, Object> map = averageSalesService.selectSalesGroupList(averageSalesSearchDTOList);
		
		List<DepartmentDTO> deptList = (List<DepartmentDTO>)map.get("deptList");
		List<AverageProductDTO> productList = (List<AverageProductDTO>)map.get("productList");
		
		List<Integer> sumListByGroup = (List<Integer>)map.get("sumListByGroup");
		List<Integer> goalListByGroup = (List<Integer>)map.get("goalListByGroup");
		
		/* 넘길 값 add */		
		if(!year.equals("")) { 									//그래프에서 표시해줄 검색기준 연도
			mv.addObject("year", year);
		} else {
			mv.addObject("year", todayYear);
		}
		
		mv.addObject("deptName", deptName);
		
		mv.addObject("yearList", yearList);
		mv.addObject("deptList", deptList);
		mv.addObject("productList", productList);
		
		mv.addObject("sumListByGroup", sumListByGroup);
		mv.addObject("goalListByGroup", goalListByGroup);
		
		mv.setViewName("average/salesGroup");
		
		return mv;
	}
	
	@GetMapping("/salesRepresentative")
	public ModelAndView selectSalesRepresentativeAverage(ModelAndView mv, 
			@AuthenticationPrincipal UserImpl userImpl, 
			@RequestParam(defaultValue = "") String year,
			@RequestParam(defaultValue = "") String responsibility) {
		
		if(responsibility.equals("")) { 				// 검색조건 중 담당자를 선택하지 않았을 경우
			responsibility = null;
		}
		
		/* 오늘날짜의 연도 추출 */
		LocalDate currentDate = LocalDate.now();
		String todayDate = currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String[] todaySplit = todayDate.split("-");
		
		int todayYear = Integer.parseInt(todaySplit[0]);
		
		/* 3개년치 해를 리스트에 담기 */
		List<Integer> yearList = new ArrayList<>();
		for(int i = 0; i < 3; i++) {
			yearList.add(todayYear - i);
		}
		
		/* 검색조건을 넣어준다 
		 * 매출발생시점은 '출고일'이다. */
		List<AverageSalesSearchDTO> averageSalesSearchDTOList = new ArrayList<>();
		java.sql.Date searchStartDate = null;
		java.sql.Date searchEndDate = null;
		
		/* 검색연도를 입력한 경우 */
		if(!year.equals("")) { 							
			
			for(int i = 1; i < 13; i++) {
				AverageSalesSearchDTO searchDTO = new AverageSalesSearchDTO();
				
				/* 10보다 작은 월일 경우 */
				if(i < 9) {																	
					String getMonthStartDate = year + "-0" + i + "-01";								//i월 첫날
					String getNextMonthStartDate = year + "-0" + (i + 1) + "-01";					//i+1월의 첫날
					
					LocalDate getNextMonthStartDateLoc = LocalDate.parse(getNextMonthStartDate);
					LocalDate getMonthEndDate = getNextMonthStartDateLoc.minusDays(1);				//i월의 마지막 날짜
					
					searchStartDate = Date.valueOf(getMonthStartDate);
					searchEndDate = Date.valueOf(getMonthEndDate);
					
					/* 검색조건 DTO에 담아준다 */
					searchDTO.setMonthStartDate(searchStartDate);
					searchDTO.setMonthLastDate(searchEndDate);
					searchDTO.setResponsibility(responsibility);
					
					averageSalesSearchDTOList.add(searchDTO);
					
				/* 10보다 큰 월일 경우 */
				} else if (i < 12) {															
					String getMonthStartDate = year + "-" + i + "-01";								//i월 첫날
					String getNextMonthStartDate = year + "-" + (i + 1) + "-01";					//i+1월의 첫날
					
					LocalDate getNextMonthStartDateLoc = LocalDate.parse(getNextMonthStartDate);
					LocalDate getMonthEndDate = getNextMonthStartDateLoc.minusDays(1);				//i월의 마지막 날짜
					
					searchStartDate = Date.valueOf(getMonthStartDate);
					searchEndDate = Date.valueOf(getMonthEndDate);
					
					/* 검색조건 DTO에 담아준다 */
					searchDTO.setMonthStartDate(searchStartDate);
					searchDTO.setMonthLastDate(searchEndDate);
					searchDTO.setResponsibility(responsibility);
					
					averageSalesSearchDTOList.add(searchDTO);
					
				/* 12월의 경우 */
				} else {																
					String getMonthStartDate = year + "-" + i + "-01";								//i월 첫날
					String getMonthEndtDate = year + "-" + i + "-31";								//i월의 마지막 날짜
					
					searchStartDate = Date.valueOf(getMonthStartDate);
					searchEndDate = Date.valueOf(getMonthEndtDate);
					
					/* 검색조건 DTO에 담아준다 */
					searchDTO.setMonthStartDate(searchStartDate);
					searchDTO.setMonthLastDate(searchEndDate);
					searchDTO.setResponsibility(responsibility);
					
					averageSalesSearchDTOList.add(searchDTO);
				}
			}
			
		/* 검색연도를 입력하지 않은 경우 */
		} else {	 										
			
			for(int i = 1; i < 13; i++) {
				AverageSalesSearchDTO searchDTO = new AverageSalesSearchDTO();
				
				/* 10보다 작은 월일 경우 */
				if(i < 9) {																	
					String getMonthStartDate = todayYear + "-0" + i + "-01";							//i월 첫날
					String getNextMonthStartDate = todayYear + "-0" + (i + 1) + "-01";					//i+1월의 첫날
					
					LocalDate getNextMonthStartDateLoc = LocalDate.parse(getNextMonthStartDate);
					LocalDate getMonthEndDate = getNextMonthStartDateLoc.minusDays(1);					//i월의 마지막 날짜
					
					searchStartDate = Date.valueOf(getMonthStartDate);
					searchEndDate = Date.valueOf(getMonthEndDate);
					
					/* 검색조건 DTO에 담아준다 */
					searchDTO.setMonthStartDate(searchStartDate);
					searchDTO.setMonthLastDate(searchEndDate);
					searchDTO.setResponsibility(responsibility);
					
					averageSalesSearchDTOList.add(searchDTO);
					
				/* 10보다 큰 월일 경우 */
				} else if (i < 12) {															
					String getMonthStartDate = todayYear + "-" + i + "-01";								//i월 첫날
					String getNextMonthStartDate = todayYear + "-" + (i + 1) + "-01";					//i+1월의 첫날
					
					LocalDate getNextMonthStartDateLoc = LocalDate.parse(getNextMonthStartDate);
					LocalDate getMonthEndDate = getNextMonthStartDateLoc.minusDays(1);					//i월의 마지막 날짜
					
					searchStartDate = Date.valueOf(getMonthStartDate);
					searchEndDate = Date.valueOf(getMonthEndDate);
					
					/* 검색조건 DTO에 담아준다 */
					searchDTO.setMonthStartDate(searchStartDate);
					searchDTO.setMonthLastDate(searchEndDate);
					searchDTO.setResponsibility(responsibility);
					
					averageSalesSearchDTOList.add(searchDTO);
					
				/* 12월의 경우 */
				} else {	
					String getMonthStartDate = todayYear + "-" + i + "-01";							//i월 첫날
					String getMonthEndtDate = todayYear + "-" + i + "-31";							//i월의 마지막 날짜
					
					searchStartDate = Date.valueOf(getMonthStartDate);
					searchEndDate = Date.valueOf(getMonthEndtDate);
					
					/* 검색조건 DTO에 담아준다 */
					searchDTO.setMonthStartDate(searchStartDate);
					searchDTO.setMonthLastDate(searchEndDate);
					searchDTO.setResponsibility(responsibility);
					
					averageSalesSearchDTOList.add(searchDTO);
				}
			}
		
		}
		
		/* 검색조건을 searvice로 보내주고, 값을 map으로 전달 */ 
		Map<String, Object> map = averageSalesService.selectSalesRepresentativeList(averageSalesSearchDTOList);
		
		List<UserDTO> AllUserList = (List<UserDTO>)map.get("AllUserList");
		List<AverageSRDTO> srListList = (List<AverageSRDTO>)map.get("srListList");
		AverageSRDTO averageAllSumSRDTO = (AverageSRDTO)map.get("averageAllSumSRDTO");
		
		/* 넘길 값 add */	
		mv.addObject("yearList", yearList);									//select 태그에서 사용할 연도들
		mv.addObject("userList", AllUserList);								//select 태그에서 사용할 담당자들
		
		if(year.equals("")) {
			mv.addObject("year", todayYear);								//그래프에서 표시해줄 검색기준 연도
		} else {
			mv.addObject("year", year);
		}
		
		if(responsibility == null) {
			mv.addObject("responsibility", responsibility);					//그래프에서 표시해줄 검색기준 담당자
		} else {
			mv.addObject("responsibility", srListList.get(0).getName());
		}
		
		mv.addObject("srListList", srListList);								//표에서 사용할 판매량 List 
		mv.addObject("averageAllSumSRDTO", averageAllSumSRDTO);				//표에서 사용할 판매량 총액 DTO
		
		mv.setViewName("average/salesRepresentative");
		
		return mv;
	}
}
