package com.codefactory.mnm.average.controller;

import java.sql.Date;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.codefactory.mnm.average.model.dto.AverageCustomSearchDTO;
import com.codefactory.mnm.average.model.dto.AverageSalesSearchDTO;
import com.codefactory.mnm.average.model.service.AverageMainService;
import com.codefactory.mnm.goal.model.dto.DepartmentDTO;
import com.codefactory.mnm.goal.model.dto.UserDTO;
import com.codefactory.mnm.member.model.dto.UserImpl;

@Controller
@RequestMapping("/average")
public class AverageMainController {
	
	private AverageMainService averageMainService;
	
	@Autowired
	public AverageMainController(AverageMainService averageMainService) {
		this.averageMainService = averageMainService;
	}
	
	@GetMapping("/main")
	public ModelAndView mainController(ModelAndView mv, 
			@AuthenticationPrincipal UserImpl userImpl, 
			@RequestParam(defaultValue = "") String startDate,
			@RequestParam(defaultValue = "") String endDate,
			@RequestParam(defaultValue = "") String deptName,
			@RequestParam(defaultValue = "") String responsibility) {
		
		/* 오늘날짜 추출 */
		LocalDate currentDate = LocalDate.now();
		String todayDate = currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String todayYear = todayDate.substring(0, 4);
		
		/* 쿼리문에 검색조건으로 사용할 날짜변수 선언
		 * 날짜를 입력하지않으면 모든갯수를 다 가져온다. */
		java.sql.Date startDateSql = null;
		java.sql.Date endDateSql = null;
		
		/* 검색조건을 넣어준다 */
		if(!startDate.equals("")) { 					// 검색조건 중 시작일자를 입력한 경우
			startDateSql = java.sql.Date.valueOf(startDate);
		}

		if(!endDate.equals("")) { 						// 검색조건 중 종료일자를 입력한 경우
			endDateSql = java.sql.Date.valueOf(endDate);
		}
		
		if(deptName.equals("")) { 						// 검색조건 중 부서명을 선택하지 않았을 경우
			deptName = null;
		}

		if(responsibility.equals("")) { 				// 검색조건 중 담당자를 선택하지 않았을 경우
			responsibility = userImpl.getUserNo();		// 현재 로그인한 유저로 검색
		}
		
		/* 매출검색에 필요한 월을 담기. 
		 * main이므로 주어진 조건없이, 오늘을 기준으로 검색한다. */
		List<AverageSalesSearchDTO> averageSalesSearchDTOList = new ArrayList<>();
		java.sql.Date searchStartDate = null;
		java.sql.Date searchEndDate = null;
		
		for(int i = 1; i < 13; i++) {
			AverageSalesSearchDTO searchDTO = new AverageSalesSearchDTO();
			
			if(i < 9) {																	//10보다 작은 월일 경우.
				String getMonthStartDate = todayYear + "-0" + i + "-01";							//i월 첫날
				String getNextMonthStartDate = todayYear + "-0" + (i + 1) + "-01";				//i+1월의 첫날
				
				LocalDate getNextMonthStartDateLoc = LocalDate.parse(getNextMonthStartDate);
				LocalDate getMonthEndDate = getNextMonthStartDateLoc.minusDays(1);			//i월의 마지막 날짜
				
				searchStartDate = Date.valueOf(getMonthStartDate);
				searchEndDate = Date.valueOf(getMonthEndDate);
				
				/* 검색조건 DTO에 담아준다 */
				searchDTO.setMonthStartDate(searchStartDate);
				searchDTO.setMonthLastDate(searchEndDate);
				searchDTO.setResponsibility(responsibility);
				searchDTO.setDeptName(deptName);
				
				averageSalesSearchDTOList.add(searchDTO);
				
			} else if (i < 12) {															//10보다 큰 월일 경우.
				String getMonthStartDate = todayYear + "-" + i + "-01";							//i월 첫날
				String getNextMonthStartDate = todayYear + "-" + (i + 1) + "-01";				//i+1월의 첫날
				
				LocalDate getNextMonthStartDateLoc = LocalDate.parse(getNextMonthStartDate);
				LocalDate getMonthEndDate = getNextMonthStartDateLoc.minusDays(1);			//i월의 마지막 날짜
				
				searchStartDate = Date.valueOf(getMonthStartDate);
				searchEndDate = Date.valueOf(getMonthEndDate);
				
				/* 검색조건 DTO에 담아준다 */
				searchDTO.setMonthStartDate(searchStartDate);
				searchDTO.setMonthLastDate(searchEndDate);
				searchDTO.setResponsibility(responsibility);
				searchDTO.setDeptName(deptName);
				
				averageSalesSearchDTOList.add(searchDTO);
				
			} else {														//12월의 경우
				String getMonthStartDate = todayYear + "-" + i + "-01";							//i월 첫날
				String getMonthEndtDate = todayYear + "-" + i + "-31";							//i월의 마지막 날짜
				
				searchStartDate = Date.valueOf(getMonthStartDate);
				searchEndDate = Date.valueOf(getMonthEndtDate);
				
				/* 검색조건 DTO에 담아준다 */
				searchDTO.setMonthStartDate(searchStartDate);
				searchDTO.setMonthLastDate(searchEndDate);
				searchDTO.setResponsibility(responsibility);
				searchDTO.setDeptName(deptName);
				
				averageSalesSearchDTOList.add(searchDTO);
			}
		}
		
		/* 검색조건을 DTO에 담는다 */
		AverageCustomSearchDTO averageCustomSearchDTO = new AverageCustomSearchDTO();
		averageCustomSearchDTO.setStartDate(startDateSql);
		averageCustomSearchDTO.setEndDate(endDateSql);
		averageCustomSearchDTO.setDeptNo(deptName);
		averageCustomSearchDTO.setResponsibility(responsibility);
		
		/* 검색조건을 Map에 담기 */
		Map<String, Object> searchCriteria = new HashMap<>();
		searchCriteria.put("averageSalesSearchDTOList", averageSalesSearchDTOList);
		searchCriteria.put("averageCustomSearchDTO", averageCustomSearchDTO);
		
		//검색조건을 searvice로 보내주고, 값을 map으로 전달 
		Map<String, Object> map = averageMainService.selectAverageMainList(searchCriteria);
		
		LocalDate startLocalDate = (LocalDate) map.get("startLocalDate");
		List<DepartmentDTO> deptList = (List<DepartmentDTO>) map.get("deptList"); 	//검색용으로 쓸 부서리스트
		List<UserDTO> userList = (List<UserDTO>) map.get("userList");				//검색용으로 쓸 담당자리스트
		
		List<Integer> badgeCustomThisMonthList = (List<Integer>) map.get("badgeCustomThisMonthList");
		
		int quantitySum = (int)map.get("quantitySum");
		double quantityAvg = (double)map.get("quantityAvg");
		int quantityMax = (int)map.get("quantityMax");
		int sumSum = (int)map.get("sumSum");
		double sumAvg = (double)map.get("sumAvg");
		int sumMax = (int)map.get("sumMax");
		
		List<Integer> tableBusinessOpportunityList = (List<Integer>)map.get("tableBusinessOpportunityList");
		
		List<Integer> tableClientSupportList = (List<Integer>)map.get("graphClientSupportList");
		
		mv.addObject("startLocalDate", todayDate);
		mv.addObject("deptList", deptList);
		mv.addObject("userList", userList);
		
		mv.addObject("badgeCustomThisMonthList", badgeCustomThisMonthList);
		
		mv.addObject("quantitySum", quantitySum);				//수량 합계
		mv.addObject("quantityAvg", quantityAvg);				//수량 평균
		mv.addObject("quantityMax", quantityMax);				//수량 최댓값
		mv.addObject("sumSum", sumSum);							//총액 합계
		mv.addObject("sumAvg", sumAvg);							//총액 평균
		mv.addObject("sumMax", sumMax);							//총액 최댓값
		
		mv.addObject("tableBusinessOpportunityList", tableBusinessOpportunityList);
		
		mv.addObject("tableClientSupportList", tableClientSupportList);
		
		mv.setViewName("average/main");
		
		return mv;
	}

}
