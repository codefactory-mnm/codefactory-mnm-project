package com.codefactory.mnm.average.controller;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.codefactory.mnm.average.model.dto.AverageClientDTO;
import com.codefactory.mnm.average.model.dto.AverageClientSupportDTO;
import com.codefactory.mnm.average.model.dto.AverageCompanyDTO;
import com.codefactory.mnm.average.model.dto.AverageCustomSearchDTO;
import com.codefactory.mnm.average.model.dto.AveragePotentialClientDTO;
import com.codefactory.mnm.average.model.service.AverageCustomService;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyCountDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyListDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyManagerDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanySalesDTO;
import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.goal.model.dto.DepartmentDTO;
import com.codefactory.mnm.goal.model.dto.UserDTO;
import com.codefactory.mnm.member.model.dto.UserImpl;

@Controller
@RequestMapping("/average")
public class AverageCustomController {

	private AverageCustomService averageCustomService;

	@Autowired
	public AverageCustomController(AverageCustomService averageCustomService) {
		this.averageCustomService = averageCustomService;
	}

	@GetMapping("/custom")
	public ModelAndView selectCustomAverage(ModelAndView mv, 
			@AuthenticationPrincipal UserImpl userImpl, 
			@RequestParam(defaultValue = "") String startDate,
			@RequestParam(defaultValue = "") String endDate,
			@RequestParam(defaultValue = "") String deptName,
			@RequestParam(defaultValue = "") String responsibility) {
		
		/* 오늘날짜의 연도 추출 */
		LocalDate currentDate = LocalDate.now();
		String todayDate = currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String[] todaySplit = todayDate.split("-");
		
		LocalDate startLocalDate = YearMonth.now().atDay(1); 		//오늘이 속한 달의 첫째 날
		
		/* 쿼리문에 검색조건으로 사용할 날짜변수 선언
		 * 날짜를 입력하지않으면 모든갯수를 다 가져온다. */
		java.sql.Date startDateSql = null;
		java.sql.Date endDateSql = null;
		
		/* 검색조건을 넣어준다 */
		if(!startDate.equals("")) { 				// 검색조건 중 시작일자를 입력한 경우
			startDateSql = java.sql.Date.valueOf(startDate);
		}

		if(!endDate.equals("")) { 					// 검색조건 중 종료일자를 입력한 경우
			endDateSql = java.sql.Date.valueOf(endDate);
		}
		
		if(deptName.equals("")) { 					// 검색조건 중 부서명을 선택하지 않았을 경우
			deptName = null;
		}

		if(responsibility.equals("")) { 			// 검색조건 중 담당자를 선택하지 않았을 경우
			responsibility = null;
		}
		
		/* 검색조건을 DTO에 담는다 */
		AverageCustomSearchDTO averageCustomSearchDTO = new AverageCustomSearchDTO();
		averageCustomSearchDTO.setStartDate(startDateSql);
		averageCustomSearchDTO.setEndDate(endDateSql);
		averageCustomSearchDTO.setDeptNo(deptName);
		averageCustomSearchDTO.setResponsibility(responsibility);
		
		//검색조건을 service로 보내주고, 값을 map으로 전달 
		Map<String, Object> map = averageCustomService.selectCustomList(averageCustomSearchDTO);
		
		List<DepartmentDTO> deptList = (List<DepartmentDTO>)map.get("deptList"); 	//검색용으로 쓸 부서리스트
		List<UserDTO> userList = (List<UserDTO>)map.get("userList");				//검색용으로 쓸 직원리스트
		List<AverageCompanyDTO> companyList = (List<AverageCompanyDTO>)map.get("companyList");
		List<AverageClientDTO> clientList = (List<AverageClientDTO>)map.get("clientList");
		List<AveragePotentialClientDTO> potentialClientList = (List<AveragePotentialClientDTO>)map.get("potentialClientList");
		
		int companyCountNo = (Integer)map.get("companyCountNo");					//고객사 수
		int clientCountNo = (Integer)map.get("clientCountNo");						//고객 수
		int potentialClientCountNo = (Integer)map.get("potentialClientCountNo");	//잠재고객 수
		
		List<String> graphCustomList = (List<String>)map.get("graphCustomList");
		List<Integer> customByGrade = (List<Integer>)map.get("customByGrade");
		
		mv.addObject("startLocalDate", startLocalDate);
		mv.addObject("deptList", deptList);
		mv.addObject("userList", userList);
		
		mv.addObject("companyList", companyList);
		mv.addObject("clientList", clientList);
		mv.addObject("potentialClientList", potentialClientList);
		
		mv.addObject("companyCountNo", companyCountNo);
		mv.addObject("clientCountNo", clientCountNo);
		mv.addObject("potentialClientCountNo", potentialClientCountNo);
		
		mv.addObject("graphCustomList", graphCustomList);
		mv.addObject("customByGrade", customByGrade);
		mv.setViewName("average/custom");

		return mv;
	}
	
	@GetMapping("/clientSupport")
	public ModelAndView selectClientSupportAverage(ModelAndView mv, 
			@AuthenticationPrincipal UserImpl userImpl, 
			@RequestParam(defaultValue = "") String startDate,
			@RequestParam(defaultValue = "") String endDate,
			@RequestParam(defaultValue = "") String deptName,
			@RequestParam(defaultValue = "") String responsibility,
			@RequestParam(defaultValue = "") String receptionist,
			@RequestParam(defaultValue = "") String preResponsibility) {
		
		/* 오늘날짜의 연도 추출 <-할필요 없다. 쿼리문에서 처리해준다. */
		LocalDate currentDate = LocalDate.now();
		String todayDate = currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String[] todaySplit = todayDate.split("-");
		
		LocalDate startLocalDate = YearMonth.now().atDay(1); 		//오늘이 속한 달의 첫째 날
		
		/* 쿼리문에 검색조건으로 사용할 날짜변수 선언
		 * 날짜를 입력하지않으면 모든갯수를 다 가져온다. */
		java.sql.Date startDateSql = null;
		java.sql.Date endDateSql = null;
		
		/* 검색조건을 넣어준다 */
		if(!startDate.equals("")) { 					// 검색조건 중 시작일자를 입력한 경우
			startDateSql = java.sql.Date.valueOf(startDate);
		}

		if(!endDate.equals("")) { 						// 검색조건 중 종료일자를 입력한 경우
			endDateSql = java.sql.Date.valueOf(endDate);
		}
		
		if(deptName.equals("")) { 						// 검색조건 중 부서명을 선택하지 않았을 경우
			deptName = null;
		}

		if(responsibility.equals("")) { 				// 검색조건 중 담당자를 선택하지 않았을 경우
			responsibility = null;
		}
		
		if(receptionist.equals("")) { 					// 검색조건 중 접수자를 선택하지 않았을 경우
			receptionist = null;
		}
		
		if(preResponsibility.equals("")) { 				// 검색조건 중 이전담당자를 선택하지 않았을 경우
			preResponsibility = null;
		}
		
		/* 검색조건을 DTO에 담는다 */
		AverageCustomSearchDTO averageCustomSearchDTO = new AverageCustomSearchDTO();
		averageCustomSearchDTO.setStartDate(startDateSql);
		averageCustomSearchDTO.setEndDate(endDateSql);
		averageCustomSearchDTO.setDeptNo(deptName);
		averageCustomSearchDTO.setResponsibility(responsibility);
		averageCustomSearchDTO.setReceptionist(receptionist);
		averageCustomSearchDTO.setPreResponsibility(preResponsibility);
		
		//검색조건을 searvice로 보내주고, 값을 map으로 전달 
		Map<String, Object> map = averageCustomService.selectClientSupportList(averageCustomSearchDTO);
		
		List<DepartmentDTO> deptList = (List<DepartmentDTO>)map.get("deptList"); 	//검색용으로 쓸 부서리스트
		List<UserDTO> userList = (List<UserDTO>)map.get("userList");				//검색용으로 쓸 직원리스트
		
		List<AverageClientSupportDTO> clientSupportList = (List<AverageClientSupportDTO>)map.get("csList");
		List<Integer> graphClientSupportList = (List<Integer>)map.get("graphClientSupportList");
		List<Integer> CSByProcess = (List<Integer>)map.get("CSByProcess");
		
		mv.addObject("startLocalDate", startLocalDate);
		mv.addObject("deptList", deptList);
		mv.addObject("userList", userList);
		
		mv.addObject("clientSupportList", clientSupportList);
		mv.addObject("graphClientSupportList", graphClientSupportList);
		mv.addObject("CSByProcess", CSByProcess);
		
		mv.setViewName("average/clientSupport");
		
		return mv;
	}

}
