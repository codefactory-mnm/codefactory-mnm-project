package com.codefactory.mnm.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.codefactory.mnm")
public class CodefactoryMnmProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodefactoryMnmProjectApplication.class, args);
		
		
	}

}
