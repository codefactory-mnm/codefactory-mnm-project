package com.codefactory.mnm.configuration.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CustomErrorController implements ErrorController{
	
	/* 에러페이지 설정 */
	@GetMapping("/error")
    public String handleError(HttpServletRequest request) {
		
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        
        if (status != null) {
        	
            int statusCode = Integer.valueOf(status.toString());
            
            if (statusCode == HttpStatus.BAD_REQUEST.value()) {
                return "error/400";
            }
            
            if (statusCode == HttpStatus.FORBIDDEN.value()) {
                return "error/403";
            }
            
            if (statusCode == HttpStatus.NOT_FOUND.value()) {
                return "error/404";
            }
            
            if (statusCode == HttpStatus.METHOD_NOT_ALLOWED.value()) {
                return "error/405";
            }
            
            if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
                return "error/500";
            }
        }
        
        return "error";
        
    }
	
}
