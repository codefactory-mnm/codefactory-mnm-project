package com.codefactory.mnm.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.codefactory.mnm.member.model.service.UserManagementService;

@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class SpringSecurityConfiguration extends WebSecurityConfigurerAdapter{
	
	private UserManagementService userManagementService;
	
	@Autowired
	public SpringSecurityConfiguration(UserManagementService userManagementService) {
		this.userManagementService = userManagementService;
	}
	
	/* 사용할 인코더를 Bean으로 등록 */
	@Bean
	public PasswordEncoder passwordEncoder() {
		
		return new BCryptPasswordEncoder();
	}
	
	/* WebSecurityConfigurerAdapter를 상속받아서 override로 구현 */
	@Override
	public void configure(WebSecurity web) {
		
		/* 권한체크없이 접속할 수 있는(security의 설정을 무시할) url 주소들을 설정 */
		 web.ignoring().antMatchers("/css/**", "/js/**", "/images/**", "/lib/**");
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http.csrf().disable()
			.authorizeRequests()
				.antMatchers("/main/**").authenticated()	//main 하위는 로그인 필요
				.antMatchers("/main/main").hasAnyRole("ADMIN", "SALES", "APPROVER")
				.antMatchers("/admin/**").hasRole("ADMIN")
				.anyRequest().permitAll()	    //그 밖의 요청은 다 허가함
			.and()
				.formLogin()
				.loginPage("/member/login")	    //login page는 이 경로로 요청
				.successForwardUrl("/main")
				.failureUrl("/fail")
			.and()
				.logout()
				.logoutRequestMatcher(new AntPathRequestMatcher("/member/logout"))
				.deleteCookies("JSESSIONID")	//"JSESSIONID"라는 쿠키를 삭제
				.invalidateHttpSession(true) 	//세선의 모든정보 만료
				.logoutSuccessUrl("/") 			//성공시 여기로 강제 리다이렉트
			.and()
				.exceptionHandling()			//권한 없으면
				.accessDeniedPage("/");         //deny 페이지 쪽으로 넘어간다
		
	}
	
	/* 권한 획득할 때 인증할 로직을 설정 */
	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		
		/* 사용할 비즈니스로직과 그때 쓸 암호화방식을 설정 */ 
		auth.userDetailsService(userManagementService).passwordEncoder(passwordEncoder());
	}
	
	

}
