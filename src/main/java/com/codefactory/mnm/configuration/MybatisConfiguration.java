package com.codefactory.mnm.configuration;

import org.apache.ibatis.annotations.Mapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan(basePackages = "com.codefactory.mnm", annotationClass = Mapper.class)
public class MybatisConfiguration {

}
