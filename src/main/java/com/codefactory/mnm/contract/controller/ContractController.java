package com.codefactory.mnm.contract.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.contract.model.dto.ContractApprovalDTO;
import com.codefactory.mnm.contract.model.dto.ContractAttachmentDTO;
import com.codefactory.mnm.contract.model.dto.ContractDTO;
import com.codefactory.mnm.contract.model.dto.ContractInsertDTO;
import com.codefactory.mnm.contract.model.dto.ContractProductDTO;
import com.codefactory.mnm.contract.model.dto.ContractSearchDTO;
import com.codefactory.mnm.contract.model.dto.EstimateListDTO;
import com.codefactory.mnm.contract.model.dto.OpinionDTO;
import com.codefactory.mnm.contract.model.service.ContractService;
import com.codefactory.mnm.estimate.model.dto.DeptListDTO;
import com.codefactory.mnm.member.model.dto.UserImpl;

@Controller
@RequestMapping("/business")
public class ContractController {

	private final ContractService contractService;
	
	@Autowired
	public ContractController(ContractService contractService) {
		
		this.contractService = contractService;
	}
	
	/* 계약 목록 조회 */
	@GetMapping("/contract/list")
	public ModelAndView selectContract(ModelAndView mv,
			@RequestParam(defaultValue = "") String division,
			@RequestParam(defaultValue = "") String searchName,
			@RequestParam(defaultValue = "") String deptName,
			@RequestParam(defaultValue = "") String name,
			@RequestParam(defaultValue = "1") String currentPage) {
		
		/* 처음 계약 리스트를 조회하거나 또는 검색조건을 설정하지 않았을 때를 대비 */
		if(division.equals("")) { // 검색조건 중 구분을 선택하지 않았을 경우
			division = null;
		}
		
		if(searchName.equals("")) { // 검색조건 중 검색어를 입력하지 않았을 경우
			searchName = null;
		}
		
		if(deptName.equals("")) { // 검색조건 중 부서를 선택하지 않았을 경우
			deptName = null;
		}
		
		if(name.equals("")) { // 검색조건 중 이름을 선택하지 않았을 경우
			name = null;
		}
		
		/* 검색조건을 담는다. 검색조건이 없을 경우 null로 담김. */
		ContractSearchDTO contractSearchDTO = new ContractSearchDTO();
		contractSearchDTO.setDivision(division);
		contractSearchDTO.setSearchName(searchName);
		contractSearchDTO.setDeptName(deptName);
		contractSearchDTO.setName(name);
		contractSearchDTO.setCurrentPage(currentPage);
		
		/* 검색조건을 searvice로 보내주고, 값을 map으로 전달받음 */
		Map<String, Object> map = contractService.selectContractList(contractSearchDTO);
		
		/* db에서 가져온 값을 꺼낸다. */
		List<ContractDTO> contractList = (List<ContractDTO>) map.get("contractList");
		List<DeptListDTO> deptList = (List<DeptListDTO>) map.get("deptList");
		SelectCriteria selectCriteria = (SelectCriteria) map.get("selectCriteria");
		int totalCount = (int) map.get("totalCount");
		
		/* db에서 조회해온 결과를 담는다 */
		mv.addObject("contractList", contractList);
		mv.addObject("deptList", deptList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("totalCount", totalCount);
		mv.addObject("contractSearchDTO", contractSearchDTO);
		mv.setViewName("contract/list");
		
		return mv;
	}
	
	/* 계약 등록 페이지로 이동 */
	@GetMapping("/contract/insert")
	public String insertContract() {
		
		return "contract/insert";
	}
	
	/* 계약 등록 */
	@PostMapping("/contract/insert")
	public ModelAndView insertContract(ModelAndView mv, ContractDTO contractDTO, ContractApprovalDTO contractApprovalDTO, 
			List<MultipartFile> multiFiles, HttpServletRequest request, RedirectAttributes rttr,
			String[] productNo, int[] quantity, String[] price, String[] salesCycle, java.sql.Date[] expectationReleaseDate,
			String[] totalPrice, String[] productNote, String sumPrice, String sumQuantity, String sumTotalPrice) {

		/* 계약 정보 입력과 동시에 파일첨부도 같이 등록한 경우 */
		if(multiFiles.size() > 0 && !multiFiles.get(0).getOriginalFilename().equals("")) {

			String root = request.getSession().getServletContext().getRealPath("/");	// 첨부파일 저장경로(절대경로)
			String filePath = root + "static/upload/";									// 첨부파일 저장경로(최종경로)

			/* 첨부파일 저장경로 (최종경로) 파일이 존재 하지 않을 경우 생성 */
			File mkdir = new File(filePath);
			if(!mkdir.exists()) {
				mkdir.mkdirs();
			}

			/* 다중 첨부파일이기 때문에, 여러 개의 파일을 담기 위해 List<ContractAttachmentDTO> 타입으로 생성 */
			List<ContractAttachmentDTO> files = new ArrayList<>();

			/* 다중 첨부파일은 반복문을 통해서, 미리 선언한 files에 담음  */
			for(int i = 0; i < multiFiles.size(); i++) {
				String fileOriginalName = multiFiles.get(i).getOriginalFilename();			// 원본파일명
				String ext = fileOriginalName.substring(fileOriginalName.lastIndexOf("."));	// 확장자명
				String fileName = UUID.randomUUID().toString().replace("-", "") + ext;		// 저장파일명

				/* 계약파일첨부DTO인 file에 정보를 담은 후 리스트인 files에 담음 */
				ContractAttachmentDTO file = new ContractAttachmentDTO();			
				file.setFileName(fileName);
				file.setFileOriginalName(fileOriginalName);
				file.setFilePath(filePath);

				files.add(file);
			}
			
			/* 계약 등록시 필요한 정보를 contractInsertDTO에 담는다 */
			ContractInsertDTO contractInsertDTO = new ContractInsertDTO();
			contractInsertDTO.setContractDTO(contractDTO);
			contractInsertDTO.setContractApprovalDTO(contractApprovalDTO);
			contractInsertDTO.setFiles(files);
			
			/* 서비스로 contractInsertDTO를 보낸다 */
			int result = contractService.insertContract(contractInsertDTO);
			
			/* DB에 등록을 성공하였을 경우 */
			if(result > 0 ) {
				
				/* 계약 품목 등록 */
				ContractProductDTO contractProductDTO = new ContractProductDTO();
				
				/* 입력한 품목 갯수 만큼 for으로 등록한다 */
				for(int i =0; i < productNo.length; i++) {
					contractProductDTO.setProductNo(productNo[i]);
					contractProductDTO.setQuantity(quantity[i]);
					contractProductDTO.setPrice(price[i]);
					contractProductDTO.setSalesCycle(salesCycle[i]);
					contractProductDTO.setExpectationReleaseDate(expectationReleaseDate[i]);
					contractProductDTO.setTotalPrice(totalPrice[i]);
					contractProductDTO.setProductNote(productNote[i]);
					contractProductDTO.setSumPrice(sumPrice);
					contractProductDTO.setSumQuantity(sumQuantity);
					contractProductDTO.setSumTotalPrice(sumTotalPrice);
					
					result = contractService.insertContractProduct(contractProductDTO);
					
					if(result > 0) {
						rttr.addFlashAttribute("message", "계약 등록을 성공하였습니다.");
					} else {
						rttr.addFlashAttribute("message", "계약 품목등록을 실패하였습니다.");
						break;
					}
				}
				if(result > 0) {
					
					/* 파일 저장 */
					try {
						for(int i = 0; i < multiFiles.size(); i++) {
							
							ContractAttachmentDTO file = files.get(i);			
							multiFiles.get(i).transferTo(new File(filePath + "\\" + file.getFileName()));
							
						}
						
						rttr.addFlashAttribute("message", "계약 등록을 성공하였습니다.");
						
						/* 파일 저장을 실패하였을 경우, 저장했던 파일을 삭제
						 * 트랜젝션처리를 통해서 DB에 입력된 정보도 삭제됨 */
					} catch (Exception e) {
						e.printStackTrace();
						
						for(int i = 0; i < multiFiles.size(); i++) {
							
							ContractAttachmentDTO file = files.get(i); 
							new File(filePath + "\\" + file.getFileName()).delete(); 
							
						}
						
						rttr.addFlashAttribute("message", "계약 등록 후 파일 업로드에 실패하였습니다.");
						
					}
				}
			
			/* DB에 계약 등록을 실패하였을 경우 */
			} else {
				
				rttr.addFlashAttribute("message", "계약 등록을 실패하였습니다.");	
				
			}

		
		/* 파일첨부를 하지 않고, 계약 등록을 하는 경우 */
		} else {
			/* 계약 등록시 필요한 정보를 contractInsertDTO에 담는다 */
			ContractInsertDTO contractInsertDTO = new ContractInsertDTO();
			contractInsertDTO.setContractDTO(contractDTO);
			contractInsertDTO.setContractApprovalDTO(contractApprovalDTO);
			
			/* 서비스로 contractInsertDTO를 전달한다 */
			int result = contractService.insertContract(contractInsertDTO);

			if(result > 0) {
				
				/* 계약 품목 등록 */
				ContractProductDTO contractProductDTO = new ContractProductDTO();
				
				/* 계약 품목 갯수 만큼 for문으로 등록한다 */
				for(int i =0; i < productNo.length; i++) {
					contractProductDTO.setProductNo(productNo[i]);
					contractProductDTO.setQuantity(quantity[i]);
					contractProductDTO.setPrice(price[i]);
					contractProductDTO.setSalesCycle(salesCycle[i]);
					contractProductDTO.setExpectationReleaseDate(expectationReleaseDate[i]);
					contractProductDTO.setTotalPrice(totalPrice[i]);
					contractProductDTO.setProductNote(productNote[i]);
					contractProductDTO.setSumPrice(sumPrice);
					contractProductDTO.setSumQuantity(sumQuantity);
					contractProductDTO.setSumTotalPrice(sumTotalPrice);
					
					result = contractService.insertContractProduct(contractProductDTO);
					
					if(result > 0) {
						rttr.addFlashAttribute("message", "계약 등록을 성공하였습니다.");
					} else {
						break;
					}
				}
				
				if(result > 0) {
					rttr.addFlashAttribute("message", "계약 등록을 성공하였습니다.");
				} else {
					rttr.addFlashAttribute("message", "계약 품목등록을 실패하였습니다.");
				}
				
			} else {
				
				rttr.addFlashAttribute("message", "계약 등록을 실패하였습니다.");
				
			}
			

		}
		
		/* 계약 등록 후 연결할 페이지 */
		mv.setViewName("redirect:/business/contract/list");
		
		return mv;
	}
	
	/* 계약 등록시 견적 모달창에 띄울 정보 가져오기 */
	@GetMapping(value="/contract/estimate", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<EstimateListDTO> selectApproverList(Model model) {
		
		List<EstimateListDTO> estimateList = contractService.selectEstimateList();
		
		model.addAttribute("estimateList", estimateList);
		
		return estimateList;
	}
	
	/* 계약 상세조회 */
	@GetMapping("/contract/detail")
	public ModelAndView selectContract(ModelAndView mv, @RequestParam String contractNo) {
		
		/* Map을 생성하여 서비스에서 값을 리턴 받는다*/
		Map<String, Object> map = contractService.selectContract(contractNo);
		
		/* map에 담은 값을 꺼낸다 */
		ContractDTO oneContract = (ContractDTO) map.get("oneContract");
		String approver = (String) map.get("approver");
		List<ContractDTO> fileList = (List<ContractDTO>) map.get("fileList");
		List<ContractProductDTO> productList = (List<ContractProductDTO>) map.get("productList");
		
		/* map에서 꺼낸 값들을 담는다 */
		mv.addObject("oneContract", oneContract);
		mv.addObject("approver", approver);
		mv.addObject("fileList", fileList);
		mv.addObject("productList", productList);
		mv.setViewName("contract/detail");
		
		return mv;
	}
	
	/* 첨부파일 다운로드 */
	@GetMapping("/contract/download")
	@ResponseBody
	public void download(HttpServletResponse response, String fileNo) throws IOException {
		
		/* 전달받은 fileNo로 파일 정보 조회 */
		ContractAttachmentDTO contractAttachmentDTO = contractService.selectFile(fileNo);
		String fileOriginalName = contractAttachmentDTO.getFileOriginalName();
		String filePath = contractAttachmentDTO.getFilePath();
		String fileName = contractAttachmentDTO.getFileName();
		
		String saveFileName = filePath + fileName;
		
		File file = new File(saveFileName);
		long fileLength = file.length();
		
		/* 파일 정보들을 가지고 reponse의 Header에 세팅 */
		response.setHeader("Content-Disposition", "attachment; filename=\"" + URLEncoder.encode(fileOriginalName,"UTF-8") + "\";");
        response.setHeader("Content-Transfer-Encoding", "binary");
        response.setHeader("Content-Type", "application/octet-stream");
        response.setHeader("Content-Length", "" + fileLength);
        response.setHeader("Pragma", "no-cache;");
        response.setHeader("Expires", "-1;");

		/* saveFileName을 파라미터로 넣어 inputStream 객체를 만들고 
        response에서 파일을 내보낼 OutputStream을 가져와서 */
        FileInputStream fis = new FileInputStream(saveFileName);
        OutputStream out = response.getOutputStream();
        
    	/* 파일 읽을 만큼 크기의 buffer를 생성한 후 */
        int readCount = 0;
        byte[] buffer = new byte[1024];
        
        /* outputStream에 씌워준다 */
        while((readCount = fis.read(buffer)) != -1){
                out.write(buffer,0,readCount);
        }
        /* 스트림 close */
        fis.close();
        out.close();
        
	}
	
	/* 계약 상세 조회 시 ajax를 통해 의견 조회 */
	@GetMapping(value="/contract/opinion/select", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<OpinionDTO> selectOpinionList(String contractNo, @AuthenticationPrincipal UserImpl customUser) {
		
		/* 상세조회할 견적번호를 service로 전달하고 의견리스트를 전달받음 */
		List<OpinionDTO> selectOpinionList = contractService.selectOpinionList(contractNo);
		
		/* 의견삭제시 작성자와 로그인된사용자가 같은지 확인하기 위해서 사용자번호를 담음 */
		for(int i = 0; i < selectOpinionList.size(); i++) {

			selectOpinionList.get(i).setLoginUserNo(customUser.getUserNo());

		}

		/* 화면으로 전달 */
		return selectOpinionList;

	}
	
	/* ajax를 통해 의견 작성 */
	@PostMapping(value="/contract/opinion/insert", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int insertOpinion(String contractNo, String opinionContent, Principal pcp) {

		String id = pcp.getName();	//세션에 담겨있는 사용자 아이디

		/* 견적번호, 의견내용, 사용자아이디를 전달하고 결과를 전달 받음 */
		int result = contractService.insertOpinion(contractNo, opinionContent, id);

		/* 화면으로 전달 */
		return result;

	}
	
	/* ajax를 통해 의견 삭제 */
	@PostMapping(value="/contract/opinion/delete", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int deleteOpinion(String opinionNo) {

		/* 의견번호를 전달하고 결과를 전달 받음 */
		int result = contractService.deleteOpinion(opinionNo);		

		/* 화면으로 전달 */
		return result;

	}
	
	/* 계약 수정*/
	@GetMapping("/contract/update")
	public ModelAndView updateContract(ModelAndView mv, String contractNo) {
		
		mv.setViewName("contract/update");
		
		return mv;
	}
	
	/* 계약 삭제 */
	@GetMapping("/contract/delete")
	public ModelAndView deleteContract(ModelAndView mv, @RequestParam String contractNo, RedirectAttributes rttr) {
		
		/* 계약 번호를 받아서 해당 게시글을 삭제 한다 */
		int result = contractService.deleteContract(contractNo);
		
		if(result > 0) {
			rttr.addFlashAttribute("message", "계약 삭제를 성공하였습니다.");
		} else {
			rttr.addFlashAttribute("message", "계약 삭제를 실패하였습니다.");
		}
		
		/* 삭제 후 연결할 페이지 */
		mv.setViewName("redirect:/business/contract/list");
		
		return mv;
	}
}
