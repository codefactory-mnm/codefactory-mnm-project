package com.codefactory.mnm.contract.model.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.codefactory.mnm.contract.model.dto.ContractApprovalDTO;
import com.codefactory.mnm.contract.model.dto.ContractAttachmentDTO;
import com.codefactory.mnm.contract.model.dto.ContractDTO;
import com.codefactory.mnm.contract.model.dto.ContractProductDTO;
import com.codefactory.mnm.contract.model.dto.ContractSearchDTO;
import com.codefactory.mnm.contract.model.dto.EstimateListDTO;
import com.codefactory.mnm.contract.model.dto.OpinionDTO;
import com.codefactory.mnm.estimate.model.dto.DeptListDTO;

@Mapper
public interface ContractDAO {

	int insertContract(ContractDTO contractDTO);

	int insertContractApproval(ContractApprovalDTO contractApprovalDTO);

	int insertContractApprovalHistory();

	int insertContractAttachment(ContractAttachmentDTO contractAttachmentDTO);

	List<EstimateListDTO> selectEstimateList();

	int insertContractProduct(ContractProductDTO contractProductDTO);

	int selectTotalCount(ContractSearchDTO contractSearchDTO);

	List<ContractDTO> selectContractList(ContractSearchDTO contractSearchDTO);

	List<DeptListDTO> selectDeptList();

	ContractDTO selectContract(String contractNo);

	String selectApprover(String contractNo);

	List<ContractDTO> selectFileList(String contractNo);

	List<ContractProductDTO> selectContractProductList(String contractNo);

	ContractAttachmentDTO selectFile(String fileNo);

	List<OpinionDTO> selectOpinionList(String contractNo);

	OpinionDTO selectUserNoAndName(String id);

	int insertOpinion(OpinionDTO opinion);

	int deleteOpinion(String opinionNo);

	int deleteContract(String contractNo);

}
