package com.codefactory.mnm.contract.model.dto;

import java.util.List;

public class ContractInsertDTO implements java.io.Serializable {

	private ContractDTO contractDTO;						//계약DTO
	private ContractApprovalDTO contractApprovalDTO;		//계약 결재DTO
	private List<ContractAttachmentDTO> files;				//계약 파일첨부 리스트
	
	public ContractInsertDTO() {}

	public ContractInsertDTO(ContractDTO contractDTO, ContractApprovalDTO contractApprovalDTO,
			List<ContractAttachmentDTO> files) {
		super();
		this.contractDTO = contractDTO;
		this.contractApprovalDTO = contractApprovalDTO;
		this.files = files;
	}

	public ContractDTO getContractDTO() {
		return contractDTO;
	}

	public ContractApprovalDTO getContractApprovalDTO() {
		return contractApprovalDTO;
	}

	public List<ContractAttachmentDTO> getFiles() {
		return files;
	}

	public void setContractDTO(ContractDTO contractDTO) {
		this.contractDTO = contractDTO;
	}

	public void setContractApprovalDTO(ContractApprovalDTO contractApprovalDTO) {
		this.contractApprovalDTO = contractApprovalDTO;
	}

	public void setFiles(List<ContractAttachmentDTO> files) {
		this.files = files;
	}

	@Override
	public String toString() {
		return "ContractInsertDTO [contractDTO=" + contractDTO + ", contractApprovalDTO=" + contractApprovalDTO
				+ ", files=" + files + "]";
	}
	
	
}
