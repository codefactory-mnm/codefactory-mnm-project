package com.codefactory.mnm.contract.model.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.codefactory.mnm.common.Pagenation;
import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.contract.model.dao.ContractDAO;
import com.codefactory.mnm.contract.model.dto.ContractApprovalDTO;
import com.codefactory.mnm.contract.model.dto.ContractAttachmentDTO;
import com.codefactory.mnm.contract.model.dto.ContractDTO;
import com.codefactory.mnm.contract.model.dto.ContractInsertDTO;
import com.codefactory.mnm.contract.model.dto.ContractProductDTO;
import com.codefactory.mnm.contract.model.dto.ContractSearchDTO;
import com.codefactory.mnm.contract.model.dto.EstimateListDTO;
import com.codefactory.mnm.contract.model.dto.OpinionDTO;
import com.codefactory.mnm.estimate.model.dto.DeptListDTO;

@Service
public class ContractServiceImpl implements ContractService{

	private final ContractDAO contractDAO;
	
	@Autowired
	public ContractServiceImpl(ContractDAO contractDAO) {
		
		this.contractDAO = contractDAO;
	}
	
	/* 계약 등록 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertContract(ContractInsertDTO contractInsertDTO) {
		
		/* controller에서 받아온 정보를 꺼낸다. */
		ContractDTO contractDTO = contractInsertDTO.getContractDTO();
		ContractApprovalDTO contractApprovalDTO = contractInsertDTO.getContractApprovalDTO();
		List<ContractAttachmentDTO> files = contractInsertDTO.getFiles();
		
		/* 계약 작성내용을 DB에 저장한다. */
		int result1 = contractDAO.insertContract(contractDTO);
		
		/* 계약 결재 내용을 DB에 저장한다. */
		int result2 = contractDAO.insertContractApproval(contractApprovalDTO);
		
		/* 계약 결재 변경이력을 DB에 저장한다 */
		int result3 = contractDAO.insertContractApprovalHistory();
		
		/* 파일첨부정보를 DB에 저장한 횟수를 저장한다. */
		int result4 = 0;	
		if(files != null) {
			for(ContractAttachmentDTO contractAttachmentDTO : files) {
				result4 += contractDAO.insertContractAttachment(contractAttachmentDTO);
			}	
		}
		
		/* 파일첨부가 없는 경우 : 계약, 결재를 정상적으로 등록하였을 경우 result = 1을 반환
		 * 파일첨부가 있는 경우 : 계약, 결재, 첨부파일을 정상적으로 등록하였을 경우 result = 1을 반환
		 */
		int result = 0;
		if(result1 == 1 && result2 == 1 && result3 == 1 &&  (files == null || result4 == files.size())) {
			result = 1;
		}
		
		return result;
	}

	/* 계약 품목 등록 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertContractProduct(ContractProductDTO contractProductDTO) {
		
		return contractDAO.insertContractProduct(contractProductDTO);
	}

	/* 견적 목록 조회 (모달)*/
	@Override
	public List<EstimateListDTO> selectEstimateList() {

		return contractDAO.selectEstimateList();
	}

	/* 계약 목록 조회*/
	@Override
	public Map<String, Object> selectContractList(ContractSearchDTO contractSearchDTO) {

		/* 첫 요청페이지 */
		int pageNo = 1;		
		
		/* 요청 페이지가 0이하면 1로 바꿔줌 */
		if(pageNo <= 0) {
			pageNo = 1;
		}
		
		/* 최대 페이지 */
		int limit = 10;
		
		/* 최대 버튼 갯수 */
		int buttonAmount = 5;
		
		/* 전체 게시물 수, 검색조건이 있다면 검색조건에 맞는 게시물 수 */
		int totalCount = contractDAO.selectTotalCount(contractSearchDTO);

		/* view에서 전달받은 요청페이지 */
		String currentPage = contractSearchDTO.getCurrentPage();
		
		/* view에서 전달받은 요청페이지를 pageNo에 저장 */
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		/* 설정한 변수에 따른 페이징조건 */
		SelectCriteria selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount);
				
		/* 조회 결과를 담을 Map 생성 */
		Map<String, Object> map = new HashMap<>();
		
		contractSearchDTO.setSelectCriteria(selectCriteria);
		
		/* 견적 조회 */
		List<ContractDTO> contractList = contractDAO.selectContractList(contractSearchDTO);
		
		/* 부서 조회 */
		List<DeptListDTO> deptList = contractDAO.selectDeptList();
		
		map.put("totalCount", totalCount);
		map.put("selectCriteria", selectCriteria);
		map.put("contractList", contractList);
		map.put("deptList", deptList);
		
		return map;
	}

	/* 계약 상세조회 */
	@Override
	public Map<String, Object> selectContract(String contractNo) {

		Map<String, Object> map = new HashMap<>();
		
		ContractDTO oneContract = contractDAO.selectContract(contractNo);
		String approver = contractDAO.selectApprover(contractNo);
		List<ContractDTO> fileList = contractDAO.selectFileList(contractNo);
		List<ContractProductDTO> productList = contractDAO.selectContractProductList(contractNo);
		
		map.put("oneContract", oneContract);
		map.put("approver", approver);
		map.put("fileList", fileList);
		map.put("productList", productList);
		
		return map;
	}

	/* 파일정보 조회 */
	@Override
	public ContractAttachmentDTO selectFile(String fileNo) {

		return contractDAO.selectFile(fileNo);
	}

	/* 의견 목록 조회 */
	@Override
	public List<OpinionDTO> selectOpinionList(String contractNo) {
		
		return contractDAO.selectOpinionList(contractNo);
	}

	/* 의견 작성 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertOpinion(String contractNo, String opinionContent, String id) {

		/* 로그인된 사용자의 번호, 이름 조회하여 저장 */
		OpinionDTO opinion = contractDAO.selectUserNoAndName(id);
		
		/* 고객사번호, 의견내용, 아이디를 저장 */
		opinion.setContractNo(contractNo);
		opinion.setOpinionContent(opinionContent);
		opinion.setId(id);
		
		/* 의견작성에 필요한 정보를 전달하고 결과를 전달받음 */
		return contractDAO.insertOpinion(opinion);
	}

	/* 의견 삭제 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int deleteOpinion(String opinionNo) {

		return contractDAO.deleteOpinion(opinionNo);
	}

	/* 계약 삭제 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int deleteContract(String contractNo) {

		return contractDAO.deleteContract(contractNo);
	}

}
