package com.codefactory.mnm.contract.model.service;

import java.util.List;
import java.util.Map;

import com.codefactory.mnm.contract.model.dto.ContractAttachmentDTO;
import com.codefactory.mnm.contract.model.dto.ContractInsertDTO;
import com.codefactory.mnm.contract.model.dto.ContractProductDTO;
import com.codefactory.mnm.contract.model.dto.ContractSearchDTO;
import com.codefactory.mnm.contract.model.dto.EstimateListDTO;
import com.codefactory.mnm.contract.model.dto.OpinionDTO;

public interface ContractService {

	int insertContract(ContractInsertDTO contractInsertDTO);

	int insertContractProduct(ContractProductDTO contractProductDTO);

	List<EstimateListDTO> selectEstimateList();

	Map<String, Object> selectContractList(ContractSearchDTO contractSearchDTO);

	Map<String, Object> selectContract(String contractNo);

	ContractAttachmentDTO selectFile(String fileNo);

	List<OpinionDTO> selectOpinionList(String contractNo);

	int insertOpinion(String contractNo, String opinionContent, String id);

	int deleteOpinion(String opinionNo);

	int deleteContract(String contractNo);

}
