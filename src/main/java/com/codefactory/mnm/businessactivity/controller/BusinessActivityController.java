package com.codefactory.mnm.businessactivity.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.codefactory.mnm.businessactivity.model.dto.BusinessActivityAttachmentsDTO;
import com.codefactory.mnm.businessactivity.model.dto.BusinessActivityCollectionDTO;
import com.codefactory.mnm.businessactivity.model.dto.BusinessActivityListDTO;
import com.codefactory.mnm.businessactivity.model.dto.BusinessActivitySearchDTO;
import com.codefactory.mnm.businessactivity.model.dto.ClientCompanyListDTO;
import com.codefactory.mnm.businessactivity.model.dto.ClientListDTO;
import com.codefactory.mnm.businessactivity.model.dto.DeptListDTO;
import com.codefactory.mnm.businessactivity.model.dto.ManagerListDTO;
import com.codefactory.mnm.businessactivity.model.service.BusinessActivityService;
import com.codefactory.mnm.common.SelectCriteria;

@Controller
@RequestMapping("/businessActivity")
public class BusinessActivityController {

	private BusinessActivityService businessActivityService;

	@Autowired
	public BusinessActivityController(BusinessActivityService businessActivityService) {
		this.businessActivityService = businessActivityService;
	}
	
	/* 영업활동 리스트 조회 */
	@GetMapping("/list")
	public ModelAndView selectBusinessCalendar(ModelAndView mv, 
			@RequestParam(defaultValue = "") String clientCompanyName,
			@RequestParam(defaultValue = "") String clientName,
			@RequestParam(defaultValue = "") String deptName,
			@RequestParam(defaultValue = "") String name,
			@RequestParam(defaultValue = "") @DateTimeFormat(pattern = "yyyyMMdd") String startDate,
			@RequestParam(defaultValue = "") @DateTimeFormat(pattern = "yyyyMMdd") String endDate,
			@RequestParam(defaultValue = "1") String currentPage) {
		
		/* 처음 영업 활동 리스트를 조회하거나 또는 검색조건을 설정하지 않았을 때를 대비 */
		if(clientCompanyName.equals("")) { // 검색조건 중 고객사를 선택하지 않았을 경우
			clientCompanyName = null;
		}
		
		if(clientName.equals("")) { // 검색조건 중 고객을 선택하지 않았을 경우
			clientName = null;
		}
		
		if(deptName.equals("")) { // 검색조건 중 부서를 선택하지 않았을 경우
			deptName = null;
		}
		
		if(name.equals("")) { // 검색조건 중 이름을 선택하지 않았을 경우
			name = null;
		}
		
		if(startDate.equals("")) { // 검색조건 중 시작 날짜를 선택하지 않았을 경우
			startDate = null;
		}
		
		if(endDate.equals("")) { // 검색조건 중 끝 날짜를 선택하지 않았을 경우
			endDate = null;
		}
		
		/* 검색조건을 담는다. 검색조건이 없을 경우 null로 담김. */
		BusinessActivitySearchDTO businessActivitySearch = new BusinessActivitySearchDTO();
		businessActivitySearch.setClientCompanyName(clientCompanyName);
		businessActivitySearch.setClientName(clientName);
		businessActivitySearch.setDeptName(deptName);
		businessActivitySearch.setName(name);
		businessActivitySearch.setStartDate(startDate);
		businessActivitySearch.setEndDate(endDate);
		businessActivitySearch.setCurrentPage(currentPage);
		
		/* 검색조건을 service로 보내주고, 값을 map으로 전달받음 */
		Map<String, Object> map = businessActivityService.selectBusinessActivityList(businessActivitySearch);
		
		
		/* 화면으로 전달 */
		List<BusinessActivityListDTO> businessActivityList = (List<BusinessActivityListDTO>) map.get("businessActivityList");
		List<ClientCompanyListDTO> clientCompanyList = (List<ClientCompanyListDTO>) map.get("clientCompanyList");
		List<ClientListDTO> clientList = (List<ClientListDTO>) map.get("clientList");				
		List<DeptListDTO> deptList = (List<DeptListDTO>) map.get("deptList");
		int businessActivityListCount = (int) map.get("businessActivityListCount");
		SelectCriteria selectCriteria = (SelectCriteria) map.get("selectCriteria");
		int totalCount = (int) map.get("totalCount");
		
		mv.addObject("businessActivityList", businessActivityList);
		mv.addObject("clientCompanyList", clientCompanyList);
		mv.addObject("clientList", clientList);
		mv.addObject("businessActivityListCount", businessActivityListCount);
		mv.addObject("deptList", deptList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("totalCount", totalCount);
		mv.addObject("businessActivitySearch", businessActivitySearch);
		
		mv.setViewName("businessActivity/list");

		return mv;
	}
	
	/* 고객사 리스트 조회 */
	@GetMapping(value="clientCompany", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<ClientCompanyListDTO> selectClientCompanyList() {

		return businessActivityService.selectClientCompanyList();
	}
	
	/* 고객 리스트 조회 */
	@GetMapping(value="client", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<ClientListDTO> selectClientList() {

		return businessActivityService.selectClientList();
	}
	
	/* 부서 리스트 조회 */
	@GetMapping(value="dept", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<DeptListDTO> selectDeptList() {

		return businessActivityService.selectDeptList();
	}
	
	/* 영업 활동 목록에서 담당자 모달창에 띄울 정보 가져오기 */
	@GetMapping(value="/manager", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<ManagerListDTO> selectManagerList(Model model, @RequestParam String deptName) {
		
		List<ManagerListDTO> managerList = businessActivityService.selectManagerList(deptName);
		
		model.addAttribute("managerList", managerList);
		
		return managerList;
	}

	@GetMapping("/insert")
	public void insertBusinessCalendarPage() {}
	
	/* 영업 활동 등록 */
	@PostMapping("/insert")
	public ModelAndView insertBusinessCalendar(ModelAndView mv, BusinessActivityListDTO businessActivity,
			List<MultipartFile> multiFiles, HttpServletRequest request, RedirectAttributes rttr) {

		/* 영업 활동 정보 입력과 동시에 파일첨부도 같이 등록한 경우 */
		if(multiFiles.size() > 0 && !multiFiles.get(0).getOriginalFilename().equals("")) {

			String root = request.getSession().getServletContext().getRealPath("/");	// 첨부파일 저장경로(절대경로)
			String filePath = root + "static/upload/";									// 첨부파일 저장경로(최종경로)
			
			/* 첨부파일 저장경로 (최종경로) 파일이 존재 하지 않을 경우 생성 */
			File mkdir = new File(filePath);
			if(!mkdir.exists()) {
				mkdir.mkdirs();
			}
			
			/* 다중 첨부파일이기 때문에, 여러 개의 파일을 담기 위해 List<BusinessActivityAttachmentsDTO> 타입으로 생성 */
			List<BusinessActivityAttachmentsDTO> files = new ArrayList<>();
			
			/* 다중 첨부파일은 반복문을 통해서, 미리 선언한 files에 담음  */
			for(int i = 0; i < multiFiles.size(); i++) {
				String fileOriginalName = multiFiles.get(i).getOriginalFilename();				// 원본파일명
				String ext = fileOriginalName.substring(fileOriginalName.lastIndexOf("."));		// 확장자명
				String fileName = UUID.randomUUID().toString().replace("-", "") + ext;			// 저장파일명
				
				/* 제안 파일첨부DTO인 file에 정보를 담은 후 리스트인 files에 담음 */
				BusinessActivityAttachmentsDTO file = new BusinessActivityAttachmentsDTO();
				file.setFileName(fileName);
				file.setFileOriginalName(fileOriginalName);
				file.setFilePath(filePath);

				files.add(file);
			}
			
			/* 영업 활동 등록시 필요한 정보를 businessActivityCollection에 담는다 */
			BusinessActivityCollectionDTO businessActivityCollection = new BusinessActivityCollectionDTO();
			businessActivityCollection.setBusinessActivity(businessActivity);
			businessActivityCollection.setFiles(files);
			
			/* 서비스로 businessActivityCollection를 보낸다 */
			int result = businessActivityService.insertBusinessActicity(businessActivityCollection);
			
			/* DB에 등록을 성공하였을 경우 */
			if(result > 0 ) {
				
			/* 파일 저장 */
			try {
				for(int i = 0; i < multiFiles.size(); i++) {

					BusinessActivityAttachmentsDTO file = files.get(i);			
					multiFiles.get(i).transferTo(new File(filePath + "\\" + file.getFileName()));

				}

				rttr.addFlashAttribute("successMessage", "영업 활동 등록에 성공하셨습니다.");
				
				/* 파일 저장을 실패하였을 경우, 저장했던 파일을 삭제
				 * 트랜젝션처리를 통해서 DB에 입력된 정보도 삭제됨 */
			} catch (Exception e) {
				e.printStackTrace();

				/* 실패 시 파일 삭제 */
				for(int i = 0; i < multiFiles.size(); i++) {
					
					BusinessActivityAttachmentsDTO file = files.get(i); 
					new File(filePath + "\\" + file.getFileName()).delete(); 
					
				}

				rttr.addFlashAttribute("successMessage", "영업 활동 등록후 파일 업로드에 실패하셨습니다.");
				
			}

		/* DB에 제안 등록을 실패하였을 경우 */
		} else {
			
			rttr.addFlashAttribute("message", "영업 활동 등록을 실패하였습니다.");	
			
		}
			
		/* 파일첨부를 하지 않고, 제안 등록을 하는 경우 */
		} else {	
			
			/* 영업 활동  등록시 필요한 정보를 businessActivityCollection에 담는다 */
			BusinessActivityCollectionDTO businessActivityCollection = new BusinessActivityCollectionDTO();
			businessActivityCollection.setBusinessActivity(businessActivity);
			
			/* 서비스로 businessActivityCollection를 전달한다 */
			int result = businessActivityService.insertBusinessActicity(businessActivityCollection);
			
			if(result > 0) {
				
				rttr.addFlashAttribute("successMessage", "영업 활동 등록에 성공하셨습니다.");
				
			} else {
				
				rttr.addFlashAttribute("successMessage", "영업 활동 등록에 실패하셨습니다.");
				
			}

		}
		
		/* 영업 등록 등록 후 연결할 페이지 */
		mv.setViewName("redirect:/businessActivity/list");

		return mv;
	}
	
	/* 영업활동 상세조회 */
	@GetMapping("/detail")
	public ModelAndView selectBusinessActivity(ModelAndView mv, @RequestParam String businessActivityNo) {
		
		/* Map을 생성하여 서비스에서 값을 리턴 받는다*/
		Map<String, Object> map = businessActivityService.selectBusinessActivity(businessActivityNo);
		
		/* map에 담은 값을 꺼낸다 */
		BusinessActivityListDTO oneBusinessActivity = (BusinessActivityListDTO) map.get("oneBusinessActivity");
		List<BusinessActivityListDTO> fileList = (List<BusinessActivityListDTO>) map.get("fileList");
		
		/* map에서 꺼낸 값들을 담는다 */
		mv.addObject("oneBusinessActivity", oneBusinessActivity);
		mv.addObject("fileList", fileList);
		mv.setViewName("businessActivity/detail");
		
		return mv;
	}
	
	/* 영업 활동 수정페이지로 이동  */
	@GetMapping("/update")
	public ModelAndView selectBusinessActivityUpdate(ModelAndView mv, @RequestParam String businessActivityNo) {
		
		/* Map을 생성하여 서비스에서 값을 리턴 받는다 */
		Map<String, Object> map = businessActivityService.selectBusinessActivity(businessActivityNo);
		
		/* map에 담은 값을 꺼낸다 */
		BusinessActivityListDTO oneBusinessActivity = (BusinessActivityListDTO) map.get("oneBusinessActivity");
		
		List<BusinessActivityAttachmentsDTO> fileList = (List<BusinessActivityAttachmentsDTO>) map.get("fileList");
	   
		/* map에서 꺼낸 값들을 담는다 */
		mv.addObject("oneBusinessActivity", oneBusinessActivity);
		mv.addObject("fileList", fileList);
		mv.setViewName("businessActivity/update");
		
		return mv;
	}
	
	/* 영업 활동 수정 */
	@PostMapping("/update")
	public ModelAndView updateBusinessActivity(ModelAndView mv, BusinessActivityListDTO businessActivity,
			List<MultipartFile> multiFiles, HttpServletRequest request, RedirectAttributes rttr) {

		
		if (multiFiles == null ) {
			List<MultipartFile> mutifilesData = new ArrayList<>();
			multiFiles = mutifilesData;
		}
		
			/* 영업 활동 정보 입력과 동시에 파일첨부도 같이 등록한 경우 */
		if(multiFiles.size() > 0 && !multiFiles.get(0).getOriginalFilename().equals("")) {
			String root = request.getSession().getServletContext().getRealPath("/");		
			String filePath = root + "static/upload/";
			
			/* 첨부파일 저장경로 (최종경로) 파일이 존재 하지 않을 경우 생성 */
			File mkdir = new File(filePath);
			if(!mkdir.exists()) {
				mkdir.mkdirs();
			}
			
			/* 다중 첨부파일이기 때문에, 여러 개의 파일을 담기 위해 List<BusinessActivityAttachmentsDTO> 타입으로 생성 */
			List<BusinessActivityAttachmentsDTO> files = new ArrayList<>();
			
			/* 다중 첨부파일은 반복문을 통해서, 미리 선언한 files에 담음  */
			for(int i = 0; i < multiFiles.size(); i++) {
				String fileOriginalName = multiFiles.get(i).getOriginalFilename();
				String ext = fileOriginalName.substring(fileOriginalName.lastIndexOf("."));
				String fileName = UUID.randomUUID().toString().replace("-", "") + ext;
				
				/* 공지 파일첨부DTO인 file에 정보를 담은 후 리스트인 files에 담음 */
				BusinessActivityAttachmentsDTO file = new BusinessActivityAttachmentsDTO();
				file.setFileName(fileName);
				file.setFileOriginalName(fileOriginalName);
				file.setFilePath(filePath);

				files.add(file);
			}

			
			/* 공지 등록시 필요한 정보를 BusinessActivityCollectionDTO에 담는다 */
			BusinessActivityCollectionDTO businessActivityCollection = new BusinessActivityCollectionDTO();
			businessActivityCollection.setBusinessActivity(businessActivity);
			businessActivityCollection.setFiles(files);
			
			/* 서비스로 businessNoticeCollection를 보낸다 */
			int result = businessActivityService.updateBusinessActivity(businessActivityCollection);
			
			/* DB에 등록을 성공하였을 경우 */
			if(result > 0 ) {
				
			/* 파일 저장 */
			try {
				for(int i = 0; i < multiFiles.size(); i++) {

					BusinessActivityAttachmentsDTO file = files.get(i);			
					multiFiles.get(i).transferTo(new File(filePath + "\\" + file.getFileName()));

				}

				rttr.addFlashAttribute("successMessage", "영업 활동 수정에 성공하셨습니다.");
				
				/* 파일 저장을 실패하였을 경우, 저장했던 파일을 삭제
				 * 트랜젝션처리를 통해서 DB에 입력된 정보도 삭제됨 */
			} catch (Exception e) {
				e.printStackTrace();

				/* 실패 시 파일 삭제 */
				for(int i = 0; i < multiFiles.size(); i++) {
					
					BusinessActivityAttachmentsDTO file = files.get(i); 
					new File(filePath + "\\" + file.getFileName()).delete(); 
				}

				rttr.addFlashAttribute("successMessage", "영업 활동 등록후 업로드에 실패하셨습니다.");
			}

		/* DB에 제안 등록을 실패하였을 경우 */	
		} else {
			
			rttr.addAttribute("message", "영업 활동 등록에 실패하셨습니다.");
				
		} 
			
		/* 파일첨부를 하지 않고, 영업 활동 등록을 하는 경우 */
		} else {
			/* 영업 공지 등록시 필요한 정보를 businessActivityCollection에 담는다 */
			BusinessActivityCollectionDTO businessActivityCollection = new BusinessActivityCollectionDTO();
			businessActivityCollection.setBusinessActivity(businessActivity);
			
			/* 서비스로 businessActivityCollection를 전달한다 */
			int result = businessActivityService.updateBusinessActivity(businessActivityCollection);

			if(result > 0) {
				rttr.addFlashAttribute("successMessage", "영업 활동 등록에 성공하셨습니다.");
			} else {
				rttr.addFlashAttribute("successMessage", "영업 활동 등록에 실패하셨습니다.");
			}
			
		}
		
		/* 영업 활동 수정 후 연결할 페이지 */
		mv.setViewName("redirect:/businessActivity/list");
		return mv;
	
	}
	
	/* 영업 활동 삭제 */
	@GetMapping("/delete")
	public ModelAndView deleteBusinessNotice(ModelAndView mv, @RequestParam String businessActivityNo,
			RedirectAttributes rttr) {
		 
		/* 공지 번호를 받아서 해당 게시글을 삭제 한다 */
		int result = businessActivityService.deleteBusinessActivity(businessActivityNo);
		
		if(result > 0) {
			rttr.addFlashAttribute("message", "공지 삭제를 성공하였습니다.");
		} else {
			rttr.addFlashAttribute("message", "공지 삭제를 실패하였습니다.");
		}
		
		/* 삭제 후 연결할 페이지 */
		mv.setViewName("redirect:/businessActivity/list");
		
		return mv;
	}
	
	/* 영업 활동 캘린더 */
	@GetMapping("/calendar")
	public ModelAndView businessActivityCalendar(ModelAndView mv,
			@RequestParam(defaultValue = "") String name,
			@RequestParam(defaultValue = "1") String currentPage) {
			
		return mv;
		
	}
	
}



















