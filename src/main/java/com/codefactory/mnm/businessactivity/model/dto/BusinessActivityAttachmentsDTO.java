package com.codefactory.mnm.businessactivity.model.dto;

public class BusinessActivityAttachmentsDTO implements java.io.Serializable{
	
	private String businessFileNo;		//파일번호
	private String fileName;			//파일명
	private String fileOriginalName;	//실제파일명
	private String filePath;			//경로
	private String division;			//구분
	private String businessReportNo;	//영업보고번호
	private String proceedingsNo;		//회의록번호
	private String businessActivityNo;	//영업활동번호
	
	public BusinessActivityAttachmentsDTO() {}

	public BusinessActivityAttachmentsDTO(String businessFileNo, String fileName, String fileOriginalName,
			String filePath, String division, String businessReportNo, String proceedingsNo,
			String businessActivityNo) {
		super();
		this.businessFileNo = businessFileNo;
		this.fileName = fileName;
		this.fileOriginalName = fileOriginalName;
		this.filePath = filePath;
		this.division = division;
		this.businessReportNo = businessReportNo;
		this.proceedingsNo = proceedingsNo;
		this.businessActivityNo = businessActivityNo;
	}

	public String getBusinessFileNo() {
		return businessFileNo;
	}

	public void setBusinessFileNo(String businessFileNo) {
		this.businessFileNo = businessFileNo;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileOriginalName() {
		return fileOriginalName;
	}

	public void setFileOriginalName(String fileOriginalName) {
		this.fileOriginalName = fileOriginalName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getBusinessReportNo() {
		return businessReportNo;
	}

	public void setBusinessReportNo(String businessReportNo) {
		this.businessReportNo = businessReportNo;
	}

	public String getProceedingsNo() {
		return proceedingsNo;
	}

	public void setProceedingsNo(String proceedingsNo) {
		this.proceedingsNo = proceedingsNo;
	}

	public String getBusinessActivityNo() {
		return businessActivityNo;
	}

	public void setBusinessActivityNo(String businessActivityNo) {
		this.businessActivityNo = businessActivityNo;
	}

	@Override
	public String toString() {
		return "BusinessActivityAttachmentsDTO [businessFileNo=" + businessFileNo + ", fileName=" + fileName
				+ ", fileOriginalName=" + fileOriginalName + ", filePath=" + filePath + ", division=" + division
				+ ", businessReportNo=" + businessReportNo + ", proceedingsNo=" + proceedingsNo
				+ ", businessActivityNo=" + businessActivityNo + "]";
	}

}
