package com.codefactory.mnm.businessactivity.model.dto;

import java.sql.Date;

public class ClientCompanyListDTO implements java.io.Serializable {

	private String clientCompanyNo;			//고객사번호
	private String clientCompanyName;		//고객사명
	private String grade;					//등급
	private String progressStatus;			//진행상태
	private String sales;					//매출
	private String employeeNo;				//사원수
	private String businessNo;				//사업자번호
	private String landlineTel;				//유선전화번호
	private String fax;						//팩스번호
	private String website;					//웹사이트
	private String zipCode;					//우편번호
	private String address;					//주소
	private String detailedAddress;			//상세주소
	private String note;					//비고
	private String division;				//구분
	private java.sql.Date writeDate;		//작성일자
	private String delYN;					//삭제여부
	
	public ClientCompanyListDTO() {}

	public ClientCompanyListDTO(String clientCompanyNo, String clientCompanyName, String grade, String progressStatus,
			String sales, String employeeNo, String businessNo, String landlineTel, String fax, String website,
			String zipCode, String address, String detailedAddress, String note, String division, Date writeDate,
			String delYN) {
		super();
		this.clientCompanyNo = clientCompanyNo;
		this.clientCompanyName = clientCompanyName;
		this.grade = grade;
		this.progressStatus = progressStatus;
		this.sales = sales;
		this.employeeNo = employeeNo;
		this.businessNo = businessNo;
		this.landlineTel = landlineTel;
		this.fax = fax;
		this.website = website;
		this.zipCode = zipCode;
		this.address = address;
		this.detailedAddress = detailedAddress;
		this.note = note;
		this.division = division;
		this.writeDate = writeDate;
		this.delYN = delYN;
	}

	public String getClientCompanyNo() {
		return clientCompanyNo;
	}

	public void setClientCompanyNo(String clientCompanyNo) {
		this.clientCompanyNo = clientCompanyNo;
	}

	public String getClientCompanyName() {
		return clientCompanyName;
	}

	public void setClientCompanyName(String clientCompanyName) {
		this.clientCompanyName = clientCompanyName;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getProgressStatus() {
		return progressStatus;
	}

	public void setProgressStatus(String progressStatus) {
		this.progressStatus = progressStatus;
	}

	public String getSales() {
		return sales;
	}

	public void setSales(String sales) {
		this.sales = sales;
	}

	public String getEmployeeNo() {
		return employeeNo;
	}

	public void setEmployeeNo(String employeeNo) {
		this.employeeNo = employeeNo;
	}

	public String getBusinessNo() {
		return businessNo;
	}

	public void setBusinessNo(String businessNo) {
		this.businessNo = businessNo;
	}

	public String getLandlineTel() {
		return landlineTel;
	}

	public void setLandlineTel(String landlineTel) {
		this.landlineTel = landlineTel;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDetailedAddress() {
		return detailedAddress;
	}

	public void setDetailedAddress(String detailedAddress) {
		this.detailedAddress = detailedAddress;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public java.sql.Date getWriteDate() {
		return writeDate;
	}

	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}

	public String getDelYN() {
		return delYN;
	}

	public void setDelYN(String delYN) {
		this.delYN = delYN;
	}

	@Override
	public String toString() {
		return "ClientCompanyListDTO [clientCompanyNo=" + clientCompanyNo + ", clientCompanyName=" + clientCompanyName
				+ ", grade=" + grade + ", progressStatus=" + progressStatus + ", sales=" + sales + ", employeeNo="
				+ employeeNo + ", businessNo=" + businessNo + ", landlineTel=" + landlineTel + ", fax=" + fax
				+ ", website=" + website + ", zipCode=" + zipCode + ", address=" + address + ", detailedAddress="
				+ detailedAddress + ", note=" + note + ", division=" + division + ", writeDate=" + writeDate
				+ ", delYN=" + delYN + "]";
	}

}
