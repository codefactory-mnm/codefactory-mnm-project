package com.codefactory.mnm.businessactivity.model.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.codefactory.mnm.businessactivity.model.dao.BusinessActivityDAO;
import com.codefactory.mnm.businessactivity.model.dto.BusinessActivityAttachmentsDTO;
import com.codefactory.mnm.businessactivity.model.dto.BusinessActivityCollectionDTO;
import com.codefactory.mnm.businessactivity.model.dto.BusinessActivityListDTO;
import com.codefactory.mnm.businessactivity.model.dto.BusinessActivitySearchDTO;
import com.codefactory.mnm.businessactivity.model.dto.ClientCompanyListDTO;
import com.codefactory.mnm.businessactivity.model.dto.ClientListDTO;
import com.codefactory.mnm.businessactivity.model.dto.DeptListDTO;
import com.codefactory.mnm.businessactivity.model.dto.ManagerListDTO;
import com.codefactory.mnm.common.Pagenation;
import com.codefactory.mnm.common.SelectCriteria;

@Service
public class BusinessActivityServiceImpl implements BusinessActivityService{

	private BusinessActivityDAO businessActivityDAO;
	
	@Autowired
	public BusinessActivityServiceImpl(BusinessActivityDAO businessActivityDAO) {
		this.businessActivityDAO = businessActivityDAO;
	}
	
	/* 영업활동 리스트 조회 */
	@Override
	public Map<String, Object> selectBusinessActivityList(BusinessActivitySearchDTO businessActivitySearch) {

		/* 첫 요청페이지 */
		int pageNo = 1;		
		
		/* 요청 페이지가 0이하면 1로 바꿔줌 */
		if(pageNo <= 0) {
			pageNo = 1;
		}
		
		/* 최대 페이지 */
		int limit = 10;
		
		/* 최대 버튼 갯수 */
		int buttonAmount = 5;
		
		/* 전체 게시물 수, 검색조건이 있다면 검색조건에 맞는 게시물 수 */
		int totalCount = businessActivityDAO.selectTotalCount(businessActivitySearch);

		/* view에서 전달받은 요청페이지 */
		String currentPage = businessActivitySearch.getCurrentPage();
		
		/* view에서 전달받은 요청페이지를 pageNo에 저장 */
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		/* 설정한 변수에 따른 페이징조건 */
		SelectCriteria selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount);
				
		/* 조회 결과를 담을 Map 생성 */
		Map<String, Object> map = new HashMap<>();
		
		businessActivitySearch.setSelectCriteria(selectCriteria);
		
		/* 검색 조건에 따른 영업활동 리스트 */
		List<BusinessActivityListDTO> businessActivityList = businessActivityDAO.selectBusinessActivityList(businessActivitySearch);
		
		/* 고객사 조회 */
		List<ClientCompanyListDTO> clientCompanyList = businessActivityDAO.selectClientCompanyList();
		
		/* 고객 조회 */
		List<ClientListDTO> clientList = businessActivityDAO.selectClientList();
		 
		/* 영업 활동 게시물 갯수 조회 */
		int businessActivityListCount = businessActivityDAO.selectBusinessActivityListCount();
		
		/* 부서 조회 */
		List<DeptListDTO> deptList = businessActivityDAO.selectDeptList();
		
		map.put("businessActivityList", businessActivityList);
		map.put("clientCompanyList", clientCompanyList);
		map.put("clientList", clientList);
		map.put("businessActivityListCount", businessActivityListCount);
		map.put("deptList", deptList);
		map.put("selectCriteria", selectCriteria);
		map.put("totalCount", totalCount);
		
		return map;
	}
	
	/* 영업 활동 등록 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertBusinessActicity(BusinessActivityCollectionDTO businessActivityCollection) {
		
		/* controller에서 받아온 정보를 꺼낸다. */
		BusinessActivityListDTO businessActivity = businessActivityCollection.getBusinessActivity();
		List<BusinessActivityAttachmentsDTO> files = businessActivityCollection.getFiles();
		
		/* 영업 활동 작성내용을 DB에 저장한다. */
		int result1 = businessActivityDAO.insertBusinessActivity(businessActivity);
		
		/* 파일첨부정보를 DB에 저장한 횟수를 저장한다. */
		int result2 = 0;
		
		if(files != null) {
			for(BusinessActivityAttachmentsDTO BusinessActivityAttachment : files) {
				result2 += businessActivityDAO.insertBusinessActivityAttachment(BusinessActivityAttachment);
			}	
		}
		
		/* 파일첨부가 없는 경우 : 제안을 정상적으로 등록하였을 경우 result = 1을 반환
		 * 파일첨부가 있는 경우 : 제안, 첨부파일을 정상적으로 등록하였을 경우 result = 1을 반환
		 */
		int result = 0;
		if(result1 == 1 &&  (files == null || result2 == files.size())) {
			result = 1;
		}
		
		return result;
	}
	
	/* 고객사 조회 */
	@Override
	public List<ClientCompanyListDTO> selectClientCompanyList() {

		return businessActivityDAO.selectClientCompanyList();
	}

	/* 고객 조회 */
	@Override
	public List<ClientListDTO> selectClientList() {

		return businessActivityDAO.selectClientList();
	}

	/* 부서 조회 */
	@Override
	public List<DeptListDTO> selectDeptList() {

		return businessActivityDAO.selectDeptList();
	}
	
	/* 담당자 조회 */
	@Override
	public List<ManagerListDTO> selectManagerList(String deptName) {

		return businessActivityDAO.selectManagerList(deptName);
	}
	
	/* 영업 활동 상세 조회 */
	@Override
	public Map<String, Object> selectBusinessActivity(String businessActivityNo) {

		Map<String, Object> map = new HashMap<>();
		
		BusinessActivityListDTO oneBusinessActivity = businessActivityDAO.selectBusinessActivity(businessActivityNo);
		List<BusinessActivityListDTO> fileList = businessActivityDAO.selectFileList(businessActivityNo);
		
		map.put("oneBusinessActivity", oneBusinessActivity);
		map.put("fileList", fileList);
		
		return map;
	}
	
	/* 영업 활동 수정 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int updateBusinessActivity(BusinessActivityCollectionDTO businessActivityCollection) {

		/* controller에서 받아온 정보를 꺼낸다. */
		BusinessActivityListDTO businessActivity = businessActivityCollection.getBusinessActivity();
		
		List<BusinessActivityAttachmentsDTO> files = businessActivityCollection.getFiles();
		
		/* 영업 공지 작성내용을 DB에 저장한다. */
		int result1 = businessActivityDAO.updateBusinessActivity(businessActivity);
		
		String businessActivityNo = businessActivity.getBusinessActivityNo();
		
		/* 파일첨부정보를 DB에 저장한 횟수를 저장한다. */
		int result2 = 0;
		if(files != null) {
			for(BusinessActivityAttachmentsDTO BusinessActivityAttachment : files) {
				BusinessActivityAttachment.setBusinessActivityNo(businessActivityNo);
				result2 += businessActivityDAO.updateBusinessActivityAttachment(BusinessActivityAttachment);
			}	
		}
		
		/* 파일첨부가 없는 경우 : 영업 활동을 정상적으로 등록하였을 경우 result = 1을 반환
		 * 파일첨부가 있는 경우 : 영업 활동, 첨부파일을 정상적으로 등록하였을 경우 result = 1을 반환
		 */
		int result = 0;
		if(result1 == 1 &&  (files == null || result2 == files.size())) {
			result = 1;
		}
		return result;
	}
	
	/* 영업 활동 삭제 */
	@Override
	public int deleteBusinessActivity(String businessActivityNo) {
		
		int result = businessActivityDAO.deleteBusinessActivity(businessActivityNo);
		
		return result;
	}


	
}




















