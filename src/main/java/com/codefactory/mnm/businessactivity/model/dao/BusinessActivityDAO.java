package com.codefactory.mnm.businessactivity.model.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.codefactory.mnm.businessactivity.model.dto.BusinessActivityAttachmentsDTO;
import com.codefactory.mnm.businessactivity.model.dto.BusinessActivityDTO;
import com.codefactory.mnm.businessactivity.model.dto.BusinessActivityListDTO;
import com.codefactory.mnm.businessactivity.model.dto.BusinessActivitySearchDTO;
import com.codefactory.mnm.businessactivity.model.dto.ClientCompanyListDTO;
import com.codefactory.mnm.businessactivity.model.dto.ClientListDTO;
import com.codefactory.mnm.businessactivity.model.dto.DeptListDTO;
import com.codefactory.mnm.businessactivity.model.dto.ManagerListDTO;
import com.codefactory.mnm.estimate.model.dto.EstimateListDTO;

@Mapper
public interface BusinessActivityDAO {

	int insertBusinessActivity(BusinessActivityListDTO businessActivity);

	int insertBusinessActivityAttachment(BusinessActivityAttachmentsDTO businessActivityAttachment);

	List<BusinessActivityListDTO> selectBusinessActivityList(BusinessActivitySearchDTO businessActivitySearch);

	int selectBusinessActivityListCount();

	int selectTotalCount(BusinessActivitySearchDTO businessActivitySearch);

	List<ClientCompanyListDTO> selectClientCompanyList();

	List<ClientListDTO> selectClientList();

	List<DeptListDTO> selectDeptList();

	List<ManagerListDTO> selectManagerList(String deptName);

	BusinessActivityListDTO selectBusinessActivity(String businessActivityNo);

	List<BusinessActivityListDTO> selectFileList(String businessActivityNo);

	int updateBusinessActivity(BusinessActivityListDTO businessActivity);

	int updateBusinessActivityAttachment(BusinessActivityAttachmentsDTO businessActivityAttachment);

	int deleteBusinessActivity(String businessActivityNo);





}
