package com.codefactory.mnm.businessactivity.model.dto;

import java.io.Serializable;

public class DeptListDTO implements Serializable{
	
	private String deptNo;				//부서번호
	private String deptName;			//부서명
	private String refDeptNo;			//상위부서번호
	private int sortOrder;
	
	public DeptListDTO() {}

	public DeptListDTO(String deptNo, String deptName, String refDeptNo, int sortOrder) {
		super();
		this.deptNo = deptNo;
		this.deptName = deptName;
		this.refDeptNo = refDeptNo;
		this.sortOrder = sortOrder;
	}

	public String getDeptNo() {
		return deptNo;
	}

	public void setDeptNo(String deptNo) {
		this.deptNo = deptNo;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getRefDeptNo() {
		return refDeptNo;
	}

	public void setRefDeptNo(String refDeptNo) {
		this.refDeptNo = refDeptNo;
	}

	public int getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}

	@Override
	public String toString() {
		return "DeptListDTO [deptNo=" + deptNo + ", deptName=" + deptName + ", refDeptNo=" + refDeptNo + ", sortOrder="
				+ sortOrder + "]";
	}
	
	
}


