package com.codefactory.mnm.businessactivity.model.dto;

import java.io.Serializable;
import java.sql.Date;

public class BusinessActivityListDTO implements Serializable{
	
	private String businessActivityNo;			//영업 활동 번호
	private String activityClassification;		//활동 분류
	private String activityPurpose;				//활동 목적
	private java.sql.Date activityDate;			//날짜
	private String activityStartTime;			//활동 시작 시간
	private String activityEndTime;				//활동 종료 시간
	private String completeYn;					//완료 여부
	private String planContent;					//계획 내용
	private String activityContent;				//활동 내용
	private String userNo;						//담당자 번호
	private java.sql.Date writeDate;			//작성일자
	private String delYn;						//삭제 여부
	private String businessOpportunityNo;		//영업 기회 번호
	private String clientName;					//고객명
	private String name;						//담당자명
	private String clientCompanyName;			//고객사명
	private String fileName;					//파일명
	private String fileOriginalName;			//원본파일명
	private String businessFileNo;				//파일번호
	private String filePath;					//파일경로
	private String businessOpportunityName;		//영업기회 명
	
	public BusinessActivityListDTO() {}

	public BusinessActivityListDTO(String businessActivityNo, String activityClassification, String activityPurpose,
			Date activityDate, String activityStartTime, String activityEndTime, String completeYn, String planContent,
			String activityContent, String userNo, Date writeDate, String delYn, String businessOpportunityNo,
			String clientName, String name, String clientCompanyName, String fileName, String fileOriginalName,
			String businessFileNo, String filePath, String businessOpportunityName) {
		super();
		this.businessActivityNo = businessActivityNo;
		this.activityClassification = activityClassification;
		this.activityPurpose = activityPurpose;
		this.activityDate = activityDate;
		this.activityStartTime = activityStartTime;
		this.activityEndTime = activityEndTime;
		this.completeYn = completeYn;
		this.planContent = planContent;
		this.activityContent = activityContent;
		this.userNo = userNo;
		this.writeDate = writeDate;
		this.delYn = delYn;
		this.businessOpportunityNo = businessOpportunityNo;
		this.clientName = clientName;
		this.name = name;
		this.clientCompanyName = clientCompanyName;
		this.fileName = fileName;
		this.fileOriginalName = fileOriginalName;
		this.businessFileNo = businessFileNo;
		this.filePath = filePath;
		this.businessOpportunityName = businessOpportunityName;
	}

	public String getBusinessActivityNo() {
		return businessActivityNo;
	}

	public void setBusinessActivityNo(String businessActivityNo) {
		this.businessActivityNo = businessActivityNo;
	}

	public String getActivityClassification() {
		return activityClassification;
	}

	public void setActivityClassification(String activityClassification) {
		this.activityClassification = activityClassification;
	}

	public String getActivityPurpose() {
		return activityPurpose;
	}

	public void setActivityPurpose(String activityPurpose) {
		this.activityPurpose = activityPurpose;
	}

	public java.sql.Date getActivityDate() {
		return activityDate;
	}

	public void setActivityDate(java.sql.Date activityDate) {
		this.activityDate = activityDate;
	}

	public String getActivityStartTime() {
		return activityStartTime;
	}

	public void setActivityStartTime(String activityStartTime) {
		this.activityStartTime = activityStartTime;
	}

	public String getActivityEndTime() {
		return activityEndTime;
	}

	public void setActivityEndTime(String activityEndTime) {
		this.activityEndTime = activityEndTime;
	}

	public String getCompleteYn() {
		return completeYn;
	}

	public void setCompleteYn(String completeYn) {
		this.completeYn = completeYn;
	}

	public String getPlanContent() {
		return planContent;
	}

	public void setPlanContent(String planContent) {
		this.planContent = planContent;
	}

	public String getActivityContent() {
		return activityContent;
	}

	public void setActivityContent(String activityContent) {
		this.activityContent = activityContent;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public java.sql.Date getWriteDate() {
		return writeDate;
	}

	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}

	public String getDelYn() {
		return delYn;
	}

	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}

	public String getBusinessOpportunityNo() {
		return businessOpportunityNo;
	}

	public void setBusinessOpportunityNo(String businessOpportunityNo) {
		this.businessOpportunityNo = businessOpportunityNo;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClientCompanyName() {
		return clientCompanyName;
	}

	public void setClientCompanyName(String clientCompanyName) {
		this.clientCompanyName = clientCompanyName;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileOriginalName() {
		return fileOriginalName;
	}

	public void setFileOriginalName(String fileOriginalName) {
		this.fileOriginalName = fileOriginalName;
	}

	public String getBusinessFileNo() {
		return businessFileNo;
	}

	public void setBusinessFileNo(String businessFileNo) {
		this.businessFileNo = businessFileNo;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getBusinessOpportunityName() {
		return businessOpportunityName;
	}

	public void setBusinessOpportunityName(String businessOpportunityName) {
		this.businessOpportunityName = businessOpportunityName;
	}

	@Override
	public String toString() {
		return "BusinessActivityListDTO [businessActivityNo=" + businessActivityNo + ", activityClassification="
				+ activityClassification + ", activityPurpose=" + activityPurpose + ", activityDate=" + activityDate
				+ ", activityStartTime=" + activityStartTime + ", activityEndTime=" + activityEndTime + ", completeYn="
				+ completeYn + ", planContent=" + planContent + ", activityContent=" + activityContent + ", userNo="
				+ userNo + ", writeDate=" + writeDate + ", delYn=" + delYn + ", businessOpportunityNo="
				+ businessOpportunityNo + ", clientName=" + clientName + ", name=" + name + ", clientCompanyName="
				+ clientCompanyName + ", fileName=" + fileName + ", fileOriginalName=" + fileOriginalName
				+ ", businessFileNo=" + businessFileNo + ", filePath=" + filePath + ", businessOpportunityName="
				+ businessOpportunityName + "]";
	}

	

}
