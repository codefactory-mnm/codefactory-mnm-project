package com.codefactory.mnm.businessactivity.model.dto;

public class BusinessActivityDTO implements java.io.Serializable{
	
	private String businessActivityNo;			//영업 활동 번호
	private String activityClassification;		//활동 분류
	private String activityPurpose;				//활동 목적
	private java.sql.Date activityDate;					//날짜
	private String activityStartTime;				//활동 시작 시간
	private String activityEndTime;				//활동 종료 시간
	private String completeYn;					//완료 여부
	private String planContent;					//계획 내용
	private String activityContent;				//활동 내용
	private String userNo;						//담당자 번호
	private java.sql.Date writeDate;			//작성일자
	private String delYn;						//삭제 여부
	private String businessOpportunityNo;		//영업 기회 번호
	
	public BusinessActivityDTO() {}

	public BusinessActivityDTO(String businessActivityNo, String activityClassification, String activityPurpose,
			java.sql.Date activityDate, String activityStartTime, String activityEndTime, String completeYn,
			String planContent, String activityContent, String userNo, java.sql.Date writeDate, String delYn,
			String businessOpportunityNo) {
		super();
		this.businessActivityNo = businessActivityNo;
		this.activityClassification = activityClassification;
		this.activityPurpose = activityPurpose;
		this.activityDate = activityDate;
		this.activityStartTime = activityStartTime;
		this.activityEndTime = activityEndTime;
		this.completeYn = completeYn;
		this.planContent = planContent;
		this.activityContent = activityContent;
		this.userNo = userNo;
		this.writeDate = writeDate;
		this.delYn = delYn;
		this.businessOpportunityNo = businessOpportunityNo;
	}

	public String getBusinessActivityNo() {
		return businessActivityNo;
	}

	public void setBusinessActivityNo(String businessActivityNo) {
		this.businessActivityNo = businessActivityNo;
	}

	public String getActivityClassification() {
		return activityClassification;
	}

	public void setActivityClassification(String activityClassification) {
		this.activityClassification = activityClassification;
	}

	public String getActivityPurpose() {
		return activityPurpose;
	}

	public void setActivityPurpose(String activityPurpose) {
		this.activityPurpose = activityPurpose;
	}

	public java.sql.Date getActivityDate() {
		return activityDate;
	}

	public void setActivityDate(java.sql.Date activityDate) {
		this.activityDate = activityDate;
	}

	public String getActivityStartTime() {
		return activityStartTime;
	}

	public void setActivityStartTime(String activityStartTime) {
		this.activityStartTime = activityStartTime;
	}

	public String getActivityEndTime() {
		return activityEndTime;
	}

	public void setActivityEndTime(String activityEndTime) {
		this.activityEndTime = activityEndTime;
	}

	public String getCompleteYn() {
		return completeYn;
	}

	public void setCompleteYn(String completeYn) {
		this.completeYn = completeYn;
	}

	public String getPlanContent() {
		return planContent;
	}

	public void setPlanContent(String planContent) {
		this.planContent = planContent;
	}

	public String getActivityContent() {
		return activityContent;
	}

	public void setActivityContent(String activityContent) {
		this.activityContent = activityContent;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public java.sql.Date getWriteDate() {
		return writeDate;
	}

	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}

	public String getDelYn() {
		return delYn;
	}

	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}

	public String getBusinessOpportunityNo() {
		return businessOpportunityNo;
	}

	public void setBusinessOpportunityNo(String businessOpportunityNo) {
		this.businessOpportunityNo = businessOpportunityNo;
	}

	@Override
	public String toString() {
		return "BusinessActivityDTO [businessActivityNo=" + businessActivityNo + ", activityClassification="
				+ activityClassification + ", activityPurpose=" + activityPurpose + ", activityDate=" + activityDate
				+ ", activityStartTime=" + activityStartTime + ", activityEndTime=" + activityEndTime + ", completeYn="
				+ completeYn + ", planContent=" + planContent + ", activityContent=" + activityContent + ", userNo="
				+ userNo + ", writeDate=" + writeDate + ", delYn=" + delYn + ", businessOpportunityNo="
				+ businessOpportunityNo + "]";
	}

	
}
