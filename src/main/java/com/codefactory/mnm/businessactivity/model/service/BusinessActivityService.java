package com.codefactory.mnm.businessactivity.model.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.codefactory.mnm.businessactivity.model.dto.BusinessActivityCollectionDTO;
import com.codefactory.mnm.businessactivity.model.dto.BusinessActivitySearchDTO;
import com.codefactory.mnm.businessactivity.model.dto.ClientCompanyListDTO;
import com.codefactory.mnm.businessactivity.model.dto.ClientListDTO;
import com.codefactory.mnm.businessactivity.model.dto.DeptListDTO;
import com.codefactory.mnm.businessactivity.model.dto.ManagerListDTO;

@Service
public interface BusinessActivityService {

	int insertBusinessActicity(BusinessActivityCollectionDTO businessActivityCollection);

	Map<String, Object> selectBusinessActivityList(BusinessActivitySearchDTO businessActivitySearch);

	List<ClientCompanyListDTO> selectClientCompanyList();

	List<ClientListDTO> selectClientList();

	List<DeptListDTO> selectDeptList();

	List<ManagerListDTO> selectManagerList(String deptName);

	Map<String, Object> selectBusinessActivity(String businessActivityNo);

	int updateBusinessActivity(BusinessActivityCollectionDTO businessActivityCollection);

	int deleteBusinessActivity(String businessActivityNo);

}
