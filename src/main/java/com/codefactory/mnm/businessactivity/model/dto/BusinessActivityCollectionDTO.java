package com.codefactory.mnm.businessactivity.model.dto;

import java.io.Serializable;
import java.util.List;

public class BusinessActivityCollectionDTO implements Serializable{

	public BusinessActivityListDTO businessActivity;
	public List<BusinessActivityAttachmentsDTO> files;
	
	public BusinessActivityCollectionDTO() {}

	public BusinessActivityCollectionDTO(BusinessActivityListDTO businessActivity,
			List<BusinessActivityAttachmentsDTO> files) {
		super();
		this.businessActivity = businessActivity;
		this.files = files;
	}

	public BusinessActivityListDTO getBusinessActivity() {
		return businessActivity;
	}

	public void setBusinessActivity(BusinessActivityListDTO businessActivity) {
		this.businessActivity = businessActivity;
	}

	public List<BusinessActivityAttachmentsDTO> getFiles() {
		return files;
	}

	public void setFiles(List<BusinessActivityAttachmentsDTO> files) {
		this.files = files;
	}

	@Override
	public String toString() {
		return "BusinessActivityCollectionDTO [businessActivity=" + businessActivity + ", files=" + files + "]";
	}

	
}
