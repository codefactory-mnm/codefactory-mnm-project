package com.codefactory.mnm.businessactivity.model.dto;

import com.codefactory.mnm.common.SelectCriteria;

public class BusinessActivitySearchDTO implements java.io.Serializable {
	
	private String BusinessActivityNo;			//영업활동 번호
	private String clientCompanyName;			//고객사
	private String clientName;					//고객
	private String deptName;					//담당자 부서
	private String name;						//담당자이름
	private String userNo;						//
	private String startDate;					//시작 날짜
	private String endDate;						//종료 날짜
	private String currentPage;					//현재페이지
	private SelectCriteria selectCriteria;		//페이징처리
	
	public BusinessActivitySearchDTO() {}

	public BusinessActivitySearchDTO(String businessActivityNo, String clientCompanyName, String clientName,
			String deptName, String name, String userNo, String startDate, String endDate, String currentPage,
			SelectCriteria selectCriteria) {
		super();
		BusinessActivityNo = businessActivityNo;
		this.clientCompanyName = clientCompanyName;
		this.clientName = clientName;
		this.deptName = deptName;
		this.name = name;
		this.userNo = userNo;
		this.startDate = startDate;
		this.endDate = endDate;
		this.currentPage = currentPage;
		this.selectCriteria = selectCriteria;
	}

	public String getBusinessActivityNo() {
		return BusinessActivityNo;
	}

	public void setBusinessActivityNo(String businessActivityNo) {
		BusinessActivityNo = businessActivityNo;
	}

	public String getClientCompanyName() {
		return clientCompanyName;
	}

	public void setClientCompanyName(String clientCompanyName) {
		this.clientCompanyName = clientCompanyName;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(String currentPage) {
		this.currentPage = currentPage;
	}

	public SelectCriteria getSelectCriteria() {
		return selectCriteria;
	}

	public void setSelectCriteria(SelectCriteria selectCriteria) {
		this.selectCriteria = selectCriteria;
	}

	@Override
	public String toString() {
		return "BusinessActivitySearchDTO [BusinessActivityNo=" + BusinessActivityNo + ", clientCompanyName="
				+ clientCompanyName + ", clientName=" + clientName + ", deptName=" + deptName + ", name=" + name
				+ ", userNo=" + userNo + ", startDate=" + startDate + ", endDate=" + endDate + ", currentPage="
				+ currentPage + ", selectCriteria=" + selectCriteria + "]";
	}

	
	
}
