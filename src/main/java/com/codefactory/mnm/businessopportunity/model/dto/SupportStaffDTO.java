package com.codefactory.mnm.businessopportunity.model.dto;

public class SupportStaffDTO implements java.io.Serializable {
	
	private String userNo2;					//지원인력대상자 번호
	private String name;					//지원인력대상자 이름
	private String businessOpportunityNo;	//영업기회 번호
	private String roleDivision;			//역할구분
	private String deptName;				//부서이름
	private String position;				//직위
	
	public SupportStaffDTO() {}

	public SupportStaffDTO(String userNo2, String name, String businessOpportunityNo, String roleDivision,
			String deptName, String position) {
		super();
		this.userNo2 = userNo2;
		this.name = name;
		this.businessOpportunityNo = businessOpportunityNo;
		this.roleDivision = roleDivision;
		this.deptName = deptName;
		this.position = position;
	}

	public String getUserNo2() {
		return userNo2;
	}

	public void setUserNo2(String userNo2) {
		this.userNo2 = userNo2;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBusinessOpportunityNo() {
		return businessOpportunityNo;
	}

	public void setBusinessOpportunityNo(String businessOpportunityNo) {
		this.businessOpportunityNo = businessOpportunityNo;
	}

	public String getRoleDivision() {
		return roleDivision;
	}

	public void setRoleDivision(String roleDivision) {
		this.roleDivision = roleDivision;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	@Override
	public String toString() {
		return "SupportStaffDTO [userNo2=" + userNo2 + ", name=" + name + ", businessOpportunityNo="
				+ businessOpportunityNo + ", roleDivision=" + roleDivision + ", deptName=" + deptName + ", position="
				+ position + "]";
	}

	

	
}
