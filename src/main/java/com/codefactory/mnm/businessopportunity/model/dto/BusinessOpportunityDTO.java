package com.codefactory.mnm.businessopportunity.model.dto;

import java.sql.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class BusinessOpportunityDTO implements java.io.Serializable {
	
	private String businessOpportunityNo;				//영업기회번호
	private String businessOpportunityName;				//영업기회명
	private String expectedSales;						//예상매출
	private String salesDivision;						//매출구분
	private String businessType;						//사업유형
	private java.sql.Date businessStartDate;			//영업시작일
	private java.sql.Date businessEndDate;				//영업종료일
	private String userNo;								//담당자번호
	private String userName;							//담당자이름
	private String clientNo;							//고객번호
	private String cognitivePathway;					//인지경로
	private String note;								//비고
	private java.sql.Date writeDate;					//작성일자		
	private String clientName;							//고객명
	private String clientCompanyName;					//고객사명
	private String approvalStatus;						//결재상태
	private String approvalOpinion;						//결재의견
	
	public BusinessOpportunityDTO() {}

	public BusinessOpportunityDTO(String businessOpportunityNo, String businessOpportunityName, String expectedSales,
			String salesDivision, String businessType, Date businessStartDate, Date businessEndDate, String userNo,
			String userName, String clientNo, String cognitivePathway, String note, Date writeDate, String clientName,
			String clientCompanyName, String approvalStatus, String approvalOpinion) {
		super();
		this.businessOpportunityNo = businessOpportunityNo;
		this.businessOpportunityName = businessOpportunityName;
		this.expectedSales = expectedSales;
		this.salesDivision = salesDivision;
		this.businessType = businessType;
		this.businessStartDate = businessStartDate;
		this.businessEndDate = businessEndDate;
		this.userNo = userNo;
		this.userName = userName;
		this.clientNo = clientNo;
		this.cognitivePathway = cognitivePathway;
		this.note = note;
		this.writeDate = writeDate;
		this.clientName = clientName;
		this.clientCompanyName = clientCompanyName;
		this.approvalStatus = approvalStatus;
		this.approvalOpinion = approvalOpinion;
	}

	public String getBusinessOpportunityNo() {
		return businessOpportunityNo;
	}

	public void setBusinessOpportunityNo(String businessOpportunityNo) {
		this.businessOpportunityNo = businessOpportunityNo;
	}

	public String getBusinessOpportunityName() {
		return businessOpportunityName;
	}

	public void setBusinessOpportunityName(String businessOpportunityName) {
		this.businessOpportunityName = businessOpportunityName;
	}

	public String getExpectedSales() {
		return expectedSales;
	}

	public void setExpectedSales(String expectedSales) {
		this.expectedSales = expectedSales;
	}

	public String getSalesDivision() {
		return salesDivision;
	}

	public void setSalesDivision(String salesDivision) {
		this.salesDivision = salesDivision;
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public java.sql.Date getBusinessStartDate() {
		return businessStartDate;
	}

	public void setBusinessStartDate(java.sql.Date businessStartDate) {
		this.businessStartDate = businessStartDate;
	}

	public java.sql.Date getBusinessEndDate() {
		return businessEndDate;
	}

	public void setBusinessEndDate(java.sql.Date businessEndDate) {
		this.businessEndDate = businessEndDate;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getClientNo() {
		return clientNo;
	}

	public void setClientNo(String clientNo) {
		this.clientNo = clientNo;
	}

	public String getCognitivePathway() {
		return cognitivePathway;
	}

	public void setCognitivePathway(String cognitivePathway) {
		this.cognitivePathway = cognitivePathway;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public java.sql.Date getWriteDate() {
		return writeDate;
	}

	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getClientCompanyName() {
		return clientCompanyName;
	}

	public void setClientCompanyName(String clientCompanyName) {
		this.clientCompanyName = clientCompanyName;
	}

	public String getApprovalStatus() {
		return approvalStatus;
	}

	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public String getApprovalOpinion() {
		return approvalOpinion;
	}

	public void setApprovalOpinion(String approvalOpinion) {
		this.approvalOpinion = approvalOpinion;
	}

	@Override
	public String toString() {
		return "BusinessOpportunityDTO [businessOpportunityNo=" + businessOpportunityNo + ", businessOpportunityName="
				+ businessOpportunityName + ", expectedSales=" + expectedSales + ", salesDivision=" + salesDivision
				+ ", businessType=" + businessType + ", businessStartDate=" + businessStartDate + ", businessEndDate="
				+ businessEndDate + ", userNo=" + userNo + ", userName=" + userName + ", clientNo=" + clientNo
				+ ", cognitivePathway=" + cognitivePathway + ", note=" + note + ", writeDate=" + writeDate
				+ ", clientName=" + clientName + ", clientCompanyName=" + clientCompanyName + ", approvalStatus="
				+ approvalStatus + ", approvalOpinion=" + approvalOpinion + "]";
	}

	
	
}
