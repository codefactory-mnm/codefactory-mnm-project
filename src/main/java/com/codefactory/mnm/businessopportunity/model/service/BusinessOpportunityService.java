package com.codefactory.mnm.businessopportunity.model.service;

import java.util.List;
import java.util.Map;

import com.codefactory.mnm.businessopportunity.model.dto.ApproverListDTO;
import com.codefactory.mnm.businessopportunity.model.dto.BusinessOpportunityAttachmentDTO;
import com.codefactory.mnm.businessopportunity.model.dto.BusinessOpportunityCollectionDTO;
import com.codefactory.mnm.businessopportunity.model.dto.BusinessOpportunitySearchDTO;
import com.codefactory.mnm.businessopportunity.model.dto.ClientDTO;
import com.codefactory.mnm.businessopportunity.model.dto.DepartmentDTO;
import com.codefactory.mnm.businessopportunity.model.dto.OpinionDTO;
import com.codefactory.mnm.businessopportunity.model.dto.ProductListDTO;
import com.codefactory.mnm.businessopportunity.model.dto.UserDTO;

public interface BusinessOpportunityService {

	Map<String, Object> selectBusinessOpportunityList(BusinessOpportunitySearchDTO businessOpportunitySearch);		//영업기회 리스트 조회
	
	List<DepartmentDTO> selectDepartmentList();																		//부서 리스트 조회
	
	Map<String, Object> selectBusinessOpportunity(String businessOpportunityNo);									//영업기회 상세조회
	
	BusinessOpportunityAttachmentDTO selectFile(String fileNo);														//첨부파일 조회
	
	List<OpinionDTO> selectOpinionList(String businessOpportunityNo);												//의견 리스트 조회
	
	int insertOpinion(String businessOpportunityNo, String opinionContent, String id);								//의견 등록
	
	int deleteOpinion(String opinionNo);																			//의견 삭제
	
	List<ClientDTO> selectClientList();																				//고객 리스트 조회
	
	List<UserDTO> selectUserList();																					//담닫자 리스트 조회

	List<ProductListDTO> selectProductList();																		//상품 리스트 조회

	List<ApproverListDTO> selectApproverList();																		//결재권자 리스트 조회
	
	int insertBusinessOpportunity(BusinessOpportunityCollectionDTO businessOpportunityCollection);					//영업기회 등록

	int deleteBusinessOpportunity(String businessOpportunityNo);													//영업기회 삭제











	

}
