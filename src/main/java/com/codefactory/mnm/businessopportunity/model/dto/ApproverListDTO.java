package com.codefactory.mnm.businessopportunity.model.dto;

public class ApproverListDTO implements java.io.Serializable {

	private String approverNo;					//결재권자번호
	private String approverName;				//결재권자명
	private String deptName;					//부서명
	private String position;					//직위
	private String authorityNo;					//권한번호
	private String authorityName;				//권한명
	
	public ApproverListDTO() {}

	public ApproverListDTO(String approverNo, String approverName, String deptName, String position, String authorityNo,
			String authorityName) {
		super();
		this.approverNo = approverNo;
		this.approverName = approverName;
		this.deptName = deptName;
		this.position = position;
		this.authorityNo = authorityNo;
		this.authorityName = authorityName;
	}

	public String getApproverNo() {
		return approverNo;
	}

	public void setApproverNo(String approverNo) {
		this.approverNo = approverNo;
	}

	public String getApproverName() {
		return approverName;
	}

	public void setApproverName(String approverName) {
		this.approverName = approverName;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getAuthorityNo() {
		return authorityNo;
	}

	public void setAuthorityNo(String authorityNo) {
		this.authorityNo = authorityNo;
	}

	public String getAuthorityName() {
		return authorityName;
	}

	public void setAuthorityName(String authorityName) {
		this.authorityName = authorityName;
	}

	@Override
	public String toString() {
		return "ApproverListDTO [approverNo=" + approverNo + ", approverName=" + approverName + ", deptName=" + deptName
				+ ", position=" + position + ", authorityNo=" + authorityNo + ", authorityName=" + authorityName + "]";
	}

	
	
}
