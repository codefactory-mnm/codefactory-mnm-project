package com.codefactory.mnm.businessopportunity.model.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.codefactory.mnm.businessopportunity.model.dto.ApprovalDTO;
import com.codefactory.mnm.businessopportunity.model.dto.ApproverListDTO;
import com.codefactory.mnm.businessopportunity.model.dto.BusinessOpportunityAttachmentDTO;
import com.codefactory.mnm.businessopportunity.model.dto.BusinessOpportunityDTO;
import com.codefactory.mnm.businessopportunity.model.dto.BusinessOpportunitySearchDTO;
import com.codefactory.mnm.businessopportunity.model.dto.ClientDTO;
import com.codefactory.mnm.businessopportunity.model.dto.DepartmentDTO;
import com.codefactory.mnm.businessopportunity.model.dto.OpinionDTO;
import com.codefactory.mnm.businessopportunity.model.dto.ProductListDTO;
import com.codefactory.mnm.businessopportunity.model.dto.SupportStaffDTO;
import com.codefactory.mnm.businessopportunity.model.dto.UserDTO;

@Mapper
public interface BusinessOpportunityDAO {

	int selectTotalCount(BusinessOpportunitySearchDTO businessOpportunitySearch);												//총 게시물 수 조회
	
	List<BusinessOpportunityDTO> selectBusinessOpportunityList(BusinessOpportunitySearchDTO businessOpportunitySearch);			//영업기회 리스트 조회
	
	List<DepartmentDTO> selectDepartmentList();																					//부서 리스트 조회
	
	BusinessOpportunityDTO selectBusinessOpportunity(String businessOpportunityNo);												//영업기회 조회
	
	List<BusinessOpportunityAttachmentDTO> selectBusinessOpportunityAttachmentList(String businessOpportunityNo);				//첨부파일 리스트 조회
	
	List<ProductListDTO> selectProductListBySelect(String businessOpportunityNo);												//상품 리스트 조회
	
	List<SupportStaffDTO> selectSupportStaff(String businessOpportunityNo);														//지원인력 리스트 조회
	
	String selectApprover(String businessOpportunityNo);																		//결재권자 조회
	
	BusinessOpportunityAttachmentDTO selectFile(String fileNo);																	//첨부파일 조회														
	
	List<OpinionDTO> selectOpinionList(String businessOpportunityNo);															//의견 리스트 조회
	
	OpinionDTO selectUserNoAndName(String id);																					//로그인된 사용자의 번호, 이름 조회
	
	int insertOpinion(OpinionDTO opinion);																						//의견 등록
	
	int deleteOpinion(String opinionNo);																						//의견 삭제
	
	List<ClientDTO> selectClientList();																							//고객 리스트 조회

	List<UserDTO> selectUserList();																								//담당자 리스트 조회
																				
	List<ProductListDTO> selectProductList();																					//상품 리스트 조회

	List<ApproverListDTO> selectApproverList();																					//결재권자 리스트 조회
	
	int insertBusinessOpportunity(BusinessOpportunityDTO businessOpportunity);													//영업기회 등록

	int insertBusinessOpportunityAttachment(BusinessOpportunityAttachmentDTO businessOpportunityAttachment);					//첨부파일 등록

	int insertBusinessProduct(ProductListDTO product);																			//상품 등록

	int insertSupportStaff(SupportStaffDTO supportStaff);																		//지원인력 등록

	int insertApproval(ApprovalDTO approval);																					//결재 등록

	int insertApprovalChangeHistory();																							//결재 변경이력 등록

	int insertBoard();																											//영업기회별 보드 등록
	
	int deleteBusinessOpportunity(String businessOpportunityNo);																//영업 기회 삭제


















	


	

}
