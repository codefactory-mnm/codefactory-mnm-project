package com.codefactory.mnm.businessopportunity.model.dto;

public class BusinessOpportunityAttachmentDTO implements java.io.Serializable {

	private String fileNo;						//파일번호
	private String fileName;					//파일명
	private String fileOriginalName;			//실제파일명
	private String filePath;					//경로
	private String division;					//구분
	private String businessOpportunityNo;		//영업기회번호
	
	public BusinessOpportunityAttachmentDTO() {}

	public BusinessOpportunityAttachmentDTO(String fileNo, String fileName, String fileOriginalName, String filePath,
			String division, String businessOpportunityNo) {
		super();
		this.fileNo = fileNo;
		this.fileName = fileName;
		this.fileOriginalName = fileOriginalName;
		this.filePath = filePath;
		this.division = division;
		this.businessOpportunityNo = businessOpportunityNo;
	}

	public String getFileNo() {
		return fileNo;
	}

	public void setFileNo(String fileNo) {
		this.fileNo = fileNo;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileOriginalName() {
		return fileOriginalName;
	}

	public void setFileOriginalName(String fileOriginalName) {
		this.fileOriginalName = fileOriginalName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getBusinessOpportunityNo() {
		return businessOpportunityNo;
	}

	public void setBusinessOpportunityNo(String businessOpportunityNo) {
		this.businessOpportunityNo = businessOpportunityNo;
	}

	@Override
	public String toString() {
		return "BusinessOpportunityAttachmentDTO [fileNo=" + fileNo + ", fileName=" + fileName + ", fileOriginalName="
				+ fileOriginalName + ", filePath=" + filePath + ", division=" + division + ", businessOpportunityNo="
				+ businessOpportunityNo + "]";
	}

	
	
}
