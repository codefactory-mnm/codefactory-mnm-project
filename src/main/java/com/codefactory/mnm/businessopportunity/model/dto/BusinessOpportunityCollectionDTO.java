package com.codefactory.mnm.businessopportunity.model.dto;

import java.util.List;

public class BusinessOpportunityCollectionDTO implements java.io.Serializable {

	private BusinessOpportunityDTO businessOpportunity;			//영업기회
	private List<BusinessOpportunityAttachmentDTO> files;		//첨부파일 리스트
	private List<ProductListDTO> productList;					//상품 리스트
	private ApprovalDTO approval; 								//결재
	private List<SupportStaffDTO> supportStaffList;				//지원인력 리스트
	
	public BusinessOpportunityCollectionDTO() {}

	public BusinessOpportunityCollectionDTO(BusinessOpportunityDTO businessOpportunity,
			List<BusinessOpportunityAttachmentDTO> files, List<ProductListDTO> productList, ApprovalDTO approval,
			List<SupportStaffDTO> supportStaffList) {
		super();
		this.businessOpportunity = businessOpportunity;
		this.files = files;
		this.productList = productList;
		this.approval = approval;
		this.supportStaffList = supportStaffList;
	}

	public BusinessOpportunityDTO getBusinessOpportunity() {
		return businessOpportunity;
	}

	public void setBusinessOpportunity(BusinessOpportunityDTO businessOpportunity) {
		this.businessOpportunity = businessOpportunity;
	}

	public List<BusinessOpportunityAttachmentDTO> getFiles() {
		return files;
	}

	public void setFiles(List<BusinessOpportunityAttachmentDTO> files) {
		this.files = files;
	}

	public List<ProductListDTO> getProductList() {
		return productList;
	}

	public void setProductList(List<ProductListDTO> productList) {
		this.productList = productList;
	}

	public ApprovalDTO getApproval() {
		return approval;
	}

	public void setApproval(ApprovalDTO approval) {
		this.approval = approval;
	}

	public List<SupportStaffDTO> getSupportStaffList() {
		return supportStaffList;
	}

	public void setSupportStaffList(List<SupportStaffDTO> supportStaffList) {
		this.supportStaffList = supportStaffList;
	}

	@Override
	public String toString() {
		return "BusinessOpportunityCollectionDTO [businessOpportunity=" + businessOpportunity + ", files=" + files
				+ ", productList=" + productList + ", approval=" + approval + ", supportStaffList=" + supportStaffList
				+ "]";
	}

	
	
}	
