package com.codefactory.mnm.businessopportunity.model.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.codefactory.mnm.businessopportunity.model.dao.BusinessOpportunityDAO;
import com.codefactory.mnm.businessopportunity.model.dto.ApprovalDTO;
import com.codefactory.mnm.businessopportunity.model.dto.ApproverListDTO;
import com.codefactory.mnm.businessopportunity.model.dto.BusinessOpportunityAttachmentDTO;
import com.codefactory.mnm.businessopportunity.model.dto.BusinessOpportunityCollectionDTO;
import com.codefactory.mnm.businessopportunity.model.dto.BusinessOpportunityDTO;
import com.codefactory.mnm.businessopportunity.model.dto.BusinessOpportunitySearchDTO;
import com.codefactory.mnm.businessopportunity.model.dto.ClientDTO;
import com.codefactory.mnm.businessopportunity.model.dto.DepartmentDTO;
import com.codefactory.mnm.businessopportunity.model.dto.OpinionDTO;
import com.codefactory.mnm.businessopportunity.model.dto.ProductListDTO;
import com.codefactory.mnm.businessopportunity.model.dto.SupportStaffDTO;
import com.codefactory.mnm.businessopportunity.model.dto.UserDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyAttachmentDTO;
import com.codefactory.mnm.common.Pagenation;
import com.codefactory.mnm.common.SelectCriteria;

@Service
public class BusinessOpportunityServiceImpl implements BusinessOpportunityService{

	private final BusinessOpportunityDAO mapper;

	@Autowired
	public BusinessOpportunityServiceImpl(BusinessOpportunityDAO mapper) {
		this.mapper = mapper;
	}
	
	/* 영업기회 리스트 조회 */
	@Override
	public Map<String, Object> selectBusinessOpportunityList(BusinessOpportunitySearchDTO businessOpportunitySearch) {
		
		int pageNo = 1;		
		
		if(pageNo <= 0) {
			pageNo = 1;
		}
		
		int limit = 10;		
		int buttonAmount = 5;
		
		/* 전체 게시물 수, 검색조건이 있다면 검색조건에 맞는 게시물 수 */
		int totalCount = mapper.selectTotalCount(businessOpportunitySearch); 
		
		/* view에서 전달받은 요청페이지*/
		String currentPage = businessOpportunitySearch.getCurrentPage();
		
		/* view에서 전달받은 요청페이지를 pageNo에 저장 */
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		/* 설정한 변수에 따른 페이징조건 */
		SelectCriteria selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount);
		
		businessOpportunitySearch.setSelectCriteria(selectCriteria);	
		
		List<BusinessOpportunityDTO> businessOpportunityList = mapper.selectBusinessOpportunityList(businessOpportunitySearch);			//영업기회 리스트 조회
		
		Map<String, Object> map = new HashMap<>();
		
		map.put("businessOpportunityList", businessOpportunityList);
		map.put("selectCriteria", selectCriteria);
		map.put("totalCount", totalCount);
				
		return map;
	}
	
	/* 부서 리스트 조회 */
	@Override
	public List<DepartmentDTO> selectDepartmentList() {
		
		return mapper.selectDepartmentList();
	}
	
	/* 영업기회 상세조회 */
	@Override
	public Map<String, Object> selectBusinessOpportunity(String businessOpportunityNo) {
		
		Map<String, Object> map = new HashMap<>();
		
		BusinessOpportunityDTO businessOpportunity = mapper.selectBusinessOpportunity(businessOpportunityNo);						//영업기회 조회
		List<BusinessOpportunityAttachmentDTO> files = mapper.selectBusinessOpportunityAttachmentList(businessOpportunityNo);		//영업기회 첨부파일 리스트 조회
		List<ProductListDTO> productList = mapper.selectProductListBySelect(businessOpportunityNo);									//영업기회 상품 리스트 조회
		List<SupportStaffDTO> supportStaff = mapper.selectSupportStaff(businessOpportunityNo);										//영업기회 지원인력 리스트 조회
		String approverName = mapper.selectApprover(businessOpportunityNo);															//영업기회 결재권자 조회
		
		map.put("businessOpportunity", businessOpportunity);
		map.put("files", files);
		map.put("productList", productList);
		map.put("supportStaff", supportStaff);
		map.put("approverName", approverName);
		
		return map;
	}
	
	/* 첨부파일 조회 */
	@Override
	public BusinessOpportunityAttachmentDTO selectFile(String fileNo) {
		
		return mapper.selectFile(fileNo);
	}
	
	/* 의견 리스트 조회 */
	@Override
	public List<OpinionDTO> selectOpinionList(String businessOpportunityNo) {
	
		return mapper.selectOpinionList(businessOpportunityNo);
	}
	
	/* 의견 등록 */
	/* DB에 저장하는 과정이므로 트랜잭션 처리를 해줌 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertOpinion(String businessOpportunityNo, String opinionContent, String id) {
		
		/* 로그인된 사용자의 번호, 이름 조회하여 저장 */
		OpinionDTO opinion = mapper.selectUserNoAndName(id);
		
		/* 영업기회번호, 의견내용, 아이디를 저장 */
		opinion.setBusinessOpportunityNo(businessOpportunityNo);
		opinion.setOpinionContent(opinionContent);
		opinion.setId(id);
		
		return mapper.insertOpinion(opinion);
	}
	
	/* 의견 삭제 */
	/* DB에 저장하는 과정이므로 트랜잭션 처리를 해줌 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int deleteOpinion(String opinionNo) {
		
		return mapper.deleteOpinion(opinionNo);
	}
	
	/* 고객 리스트 조회 */
	@Override
	public List<ClientDTO> selectClientList() {

		return mapper.selectClientList();
	}
	
	/* 담당자 리스트 조회 */
	@Override
	public List<UserDTO> selectUserList() {
		
		return mapper.selectUserList();
	}
	
	/* 상품 리스트 조회 */
	@Override
	public List<ProductListDTO> selectProductList() {

		return mapper.selectProductList();
	}
	
	/* 결재권자 리스트 조회 */
	@Override
	public List<ApproverListDTO> selectApproverList() {
		
		return mapper.selectApproverList();
	}
	
	/* 영업기회 등록 */
	/* DB에 저장하는 과정이므로 트랜잭션 처리를 해줌 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertBusinessOpportunity(BusinessOpportunityCollectionDTO businessOpportunityCollection) {

		BusinessOpportunityDTO businessOpportunity = businessOpportunityCollection.getBusinessOpportunity();
		List<BusinessOpportunityAttachmentDTO> files = businessOpportunityCollection.getFiles();
		List<ProductListDTO> productList = businessOpportunityCollection.getProductList();
		ApprovalDTO approval = businessOpportunityCollection.getApproval();
		List<SupportStaffDTO> supportStaffList = businessOpportunityCollection.getSupportStaffList();

		int result1 = mapper.insertBusinessOpportunity(businessOpportunity);	//영업기회 등록

		int result2 = 0;
		if(files != null) {
			for(BusinessOpportunityAttachmentDTO businessOpportunityAttachment: files) {
				result2 += mapper.insertBusinessOpportunityAttachment(businessOpportunityAttachment);	//첨부파일 등록
			}	
		}
		
		int result3 = 0;
		if(productList != null) {
			for(ProductListDTO product : productList) {
				result3 += mapper.insertBusinessProduct(product);	//상품 리스트 등록
			}
		}
		
		int result4 = 0;
		if(supportStaffList != null) {
			for(SupportStaffDTO supportStaff : supportStaffList) {
				result4 += mapper.insertSupportStaff(supportStaff);		//지원인력 리스트 등록			
			}
			
		}

		int result5 = mapper.insertApproval(approval);	//결재 등록 

		int result6 = mapper.insertApprovalChangeHistory();	//결재변경이력 등록

		int result7 = mapper.insertBoard();	//영업기회별 보드 등록
		
		int result = 0;
		
		/* 모든 조건이 부합할 경우 result를 1로 반환 */
		if(result1 == 1 && result5 == 1 && result6 == 1 && result7 == 1 &&  (files == null || result2 == files.size()) &&
				                                                            (productList == null || result3 == productList.size()) && 
				                                                            (supportStaffList == null || result4 == supportStaffList.size())) {
			result = 1;
		}
		
		return result;
	}

	/* 영업기회 삭제 */
	/* DB에 삭제하는 과정이므로 트랜잭션 처리를 해줌 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int deleteBusinessOpportunity(String businessOpportunityNo) {
			
		return mapper.deleteBusinessOpportunity(businessOpportunityNo);
	}
	

}
