package com.codefactory.mnm.businessopportunity.model.dto;

import java.util.List;

public class ProductListDTO implements java.io.Serializable {

	private String businessOpportunityNo;			//영업기회번호
	private String productNo;						//상품번호
	private int quantity2;							//수량
	private int discount;							//할인
	private String productName;						//상품명
	private String price;							//단가
	private String unit;							//단위
	private String standard;						//규격
	private int salePrice;							//판매단가
	private int totalPrice;							//제안금액
	
	public ProductListDTO() {}

	public ProductListDTO(String businessOpportunityNo, String productNo, int quantity2, int discount,
			String productName, String price, String unit, String standard, int salePrice, int totalPrice) {
		super();
		this.businessOpportunityNo = businessOpportunityNo;
		this.productNo = productNo;
		this.quantity2 = quantity2;
		this.discount = discount;
		this.productName = productName;
		this.price = price;
		this.unit = unit;
		this.standard = standard;
		this.salePrice = salePrice;
		this.totalPrice = totalPrice;
	}

	public String getBusinessOpportunityNo() {
		return businessOpportunityNo;
	}

	public void setBusinessOpportunityNo(String businessOpportunityNo) {
		this.businessOpportunityNo = businessOpportunityNo;
	}

	public String getProductNo() {
		return productNo;
	}

	public void setProductNo(String productNo) {
		this.productNo = productNo;
	}

	public int getQuantity2() {
		return quantity2;
	}

	public void setQuantity2(int quantity2) {
		this.quantity2 = quantity2;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getStandard() {
		return standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public int getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(int salePrice) {
		this.salePrice = salePrice;
	}

	public int getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}

	@Override
	public String toString() {
		return "ProductListDTO [businessOpportunityNo=" + businessOpportunityNo + ", productNo=" + productNo
				+ ", quantity2=" + quantity2 + ", discount=" + discount + ", productName=" + productName + ", price="
				+ price + ", unit=" + unit + ", standard=" + standard + ", salePrice=" + salePrice + ", totalPrice="
				+ totalPrice + "]";
	}

	
}
