package com.codefactory.mnm.businessopportunity.model.dto;

import com.codefactory.mnm.common.SelectCriteria;

public class BusinessOpportunitySearchDTO implements java.io.Serializable {
	
	private String businessOpportunityName;		//영업기회명
	private String clientName;					//고객명
	private String clientCompanyName;			//고객사명
	private String userNo;						//담당자번호
	private String deptName;					//부서명
	private String currentPage;					//현재페이지
	private SelectCriteria selectCriteria;		//페이징처리
	
	public BusinessOpportunitySearchDTO() {}

	public BusinessOpportunitySearchDTO(String businessOpportunityName, String clientName, String clientCompanyName,
			String userNo, String deptName, String currentPage, SelectCriteria selectCriteria) {
		super();
		this.businessOpportunityName = businessOpportunityName;
		this.clientName = clientName;
		this.clientCompanyName = clientCompanyName;
		this.userNo = userNo;
		this.deptName = deptName;
		this.currentPage = currentPage;
		this.selectCriteria = selectCriteria;
	}

	public String getBusinessOpportunityName() {
		return businessOpportunityName;
	}

	public void setBusinessOpportunityName(String businessOpportunityName) {
		this.businessOpportunityName = businessOpportunityName;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getClientCompanyName() {
		return clientCompanyName;
	}

	public void setClientCompanyName(String clientCompanyName) {
		this.clientCompanyName = clientCompanyName;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(String currentPage) {
		this.currentPage = currentPage;
	}

	public SelectCriteria getSelectCriteria() {
		return selectCriteria;
	}

	public void setSelectCriteria(SelectCriteria selectCriteria) {
		this.selectCriteria = selectCriteria;
	}

	@Override
	public String toString() {
		return "BusinessOpportunitySearchDTO [businessOpportunityName=" + businessOpportunityName + ", clientName="
				+ clientName + ", clientCompanyName=" + clientCompanyName + ", userNo=" + userNo + ", deptName="
				+ deptName + ", currentPage=" + currentPage + ", selectCriteria=" + selectCriteria + "]";
	}

	
	
	
}
