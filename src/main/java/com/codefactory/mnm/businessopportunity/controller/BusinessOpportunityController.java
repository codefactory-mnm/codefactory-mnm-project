package com.codefactory.mnm.businessopportunity.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.codefactory.mnm.businessopportunity.model.dto.ApprovalDTO;
import com.codefactory.mnm.businessopportunity.model.dto.ApproverListDTO;
import com.codefactory.mnm.businessopportunity.model.dto.BusinessOpportunityAttachmentDTO;
import com.codefactory.mnm.businessopportunity.model.dto.BusinessOpportunityCollectionDTO;
import com.codefactory.mnm.businessopportunity.model.dto.BusinessOpportunityDTO;
import com.codefactory.mnm.businessopportunity.model.dto.BusinessOpportunitySearchDTO;
import com.codefactory.mnm.businessopportunity.model.dto.ClientDTO;
import com.codefactory.mnm.businessopportunity.model.dto.DepartmentDTO;
import com.codefactory.mnm.businessopportunity.model.dto.OpinionDTO;
import com.codefactory.mnm.businessopportunity.model.dto.ProductListDTO;
import com.codefactory.mnm.businessopportunity.model.dto.SupportStaffDTO;
import com.codefactory.mnm.businessopportunity.model.dto.UserDTO;
import com.codefactory.mnm.businessopportunity.model.service.BusinessOpportunityService;
import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.member.model.dto.UserImpl;


@Controller
@RequestMapping("/businessOpportunity")
public class BusinessOpportunityController {

	private BusinessOpportunityService businessOpportunityService;

	@Autowired
	public BusinessOpportunityController(BusinessOpportunityService businessOpportunityService) {
		this.businessOpportunityService = businessOpportunityService;
	}
	
	/* 영업기회 리스트 조회 */
	@GetMapping("/list")
	public ModelAndView selectBusinessOpportunityList(ModelAndView mv,
			@RequestParam(defaultValue = "") String selectSearch,
			@RequestParam(defaultValue = "") String searchValue,
			@RequestParam(defaultValue = "") String userNo,
			@RequestParam(defaultValue = "1") String currentPage,
			@RequestParam(defaultValue = "") String deptName
			) {
		
		/* 검색조건에서 해당되지 않거나, 검색조건 없이 리스틀 조회할 시 null로 전달하기위해 null로 담는다. */
		String businessOpportunityName = null;
		String clientCompanyName = null;
		String clientName = null;
		
		/* 검색 조건에 따라 값을 설정 */
		if(selectSearch.equals("영업기회")) {					//검색조건 중 영업기회를 선택하였을 경우
			businessOpportunityName = searchValue;
		} else if(selectSearch.equals("고객사")) {			//검색조건 중 고객사를 선택하였을 경우
			clientCompanyName = searchValue;
		} else if(selectSearch.equals("고객")) {				//검색조건 중 고객을 선택하였을 경우
			clientName = searchValue;
		}
		
		if(userNo.equals("")) {								//검색조건 중 이름을 선택하지 않았을 경우
			userNo = null;
		}
		
		if(deptName.equals("")) {							//검색조건 중 부서를 선택하지 않았을 경우
			deptName = null;
		}
		
		BusinessOpportunitySearchDTO businessOpportunitySearch = new BusinessOpportunitySearchDTO();
		businessOpportunitySearch.setBusinessOpportunityName(businessOpportunityName);
		businessOpportunitySearch.setClientCompanyName(clientCompanyName);
		businessOpportunitySearch.setClientName(clientName);
		businessOpportunitySearch.setDeptName(deptName);
		businessOpportunitySearch.setUserNo(userNo);
		businessOpportunitySearch.setCurrentPage(currentPage);

		Map<String, Object> map = businessOpportunityService.selectBusinessOpportunityList(businessOpportunitySearch);
		
		List<BusinessOpportunityDTO> businessOpportunityList = (List<BusinessOpportunityDTO>) map.get("businessOpportunityList"); 		//영업기회 리스트
		SelectCriteria selectCriteria = (SelectCriteria) map.get("selectCriteria");														//페이징처리
		int totalCount = (int) map.get("totalCount");																					//총 게시물 수
		
		mv.addObject("businessOpportunityList", businessOpportunityList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("totalCount", totalCount);
		mv.addObject("businessOpportunitySearch", businessOpportunitySearch);
		mv.setViewName("businessOpportunity/list");
		
		return mv;
	}
	
	/* 부서 리스트 조회 */
	@GetMapping(value="dept", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<DepartmentDTO> selectDepartmentList() {

		return businessOpportunityService.selectDepartmentList();
	}
	
	/* 영업기회 상세 조회 */
	@GetMapping("/select")
	public ModelAndView selectBusinessOpportunity(ModelAndView mv, String businessOpportunityNo) {
		
		Map<String, Object> map = businessOpportunityService.selectBusinessOpportunity(businessOpportunityNo);
		
		BusinessOpportunityDTO businessOpportunity = (BusinessOpportunityDTO) map.get("businessOpportunity");		//영업기회 상세정보
		List<BusinessOpportunityAttachmentDTO> files = (List<BusinessOpportunityAttachmentDTO>) map.get("files");	//영업기회 첨부파일 리스트
		List<ProductListDTO> productList = (List<ProductListDTO>) map.get("productList");							//영업기회 상품 리스트
		List<SupportStaffDTO> supportStaff = (List<SupportStaffDTO>) map.get("supportStaff");						//영업기회 지원인력
		String approverName = (String) map.get("approverName");														//영업기회 결재권자
				
		mv.addObject("businessOpportunity", businessOpportunity);
		mv.addObject("files", files);
		mv.addObject("productList", productList);
		mv.addObject("supportStaff", supportStaff);
		mv.addObject("approverName", approverName);
		mv.setViewName("businessOpportunity/select");
		
		return mv;
	}

	
	/*  파일첨부 다운로드 */
	@GetMapping(value="fileDownload")
	@ResponseBody
	public void fileDownload(String fileNo, HttpServletResponse response) throws Exception {

		/* 파일번호를 service에 전달하고 파일정보를 전달받음 */
		BusinessOpportunityAttachmentDTO businessOpportunityAttachment = businessOpportunityService.selectFile(fileNo);

		String fileName = businessOpportunityAttachment.getFileName();					//저장파일명
		String fileOriginalName = businessOpportunityAttachment.getFileOriginalName();	//원본파일명
		String filePath = businessOpportunityAttachment.getFilePath();					//첨부파일 저장경로(최종경로)
		String saveFileName = filePath + fileName;

		File file = new File(saveFileName);
		long fileLength = file.length();

		/* 파일 정보들을 가지고 reponse의 Header에 세팅 */
		response.setHeader("Content-Disposition", "attachment; filename=\"" + URLEncoder.encode(fileOriginalName,"UTF-8") + "\";");
		response.setHeader("Content-Transfer-Encoding", "binary");
		response.setHeader("Content-Type", "application/octet-stream");
		response.setHeader("Content-Length", "" + fileLength);
		response.setHeader("Pragma", "no-cache;");
		response.setHeader("Expires", "-1;");

		/* saveFileName을 파라미터로 넣어 inputStream 객체를 만들고 
	        response에서 파일을 내보낼 OutputStream을 가져와서 */
		FileInputStream fis = new FileInputStream(saveFileName);
		OutputStream out = response.getOutputStream();

		/* 파일 읽을 만큼 크기의 buffer를 생성한 후 */
		int readCount = 0;
		byte[] buffer = new byte[1024];
		
		/* outputStream에 씌워준다 */
		while((readCount = fis.read(buffer)) != -1){
			out.write(buffer,0,readCount);
		}

		fis.close();
		out.close();

	}
	
	/* 의견 리스트 조회 */
	@GetMapping(value="opinion/select", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<OpinionDTO> selectOpinionList(String businessOpportunityNo, @AuthenticationPrincipal UserImpl customUser) {
		
		List<OpinionDTO> selectOpinionList = businessOpportunityService.selectOpinionList(businessOpportunityNo);
		
		/* 의견삭제시 작성자와 로그인된사용자가 같은지 확인하기 위해서 사용자번호를 담음 */
		for(int i = 0; i < selectOpinionList.size(); i++) {

			selectOpinionList.get(i).setLoginUserNo(customUser.getUserNo());

		}

		return selectOpinionList;

	}
	
	/* 의견 등록 */
	@PostMapping(value="opinion/insert", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int insertOpinion(String businessOpportunityNo, String opinionContent, Principal pcp) {
		
		String id = pcp.getName();	//세션에 담겨있는 사용자 아이디

		return businessOpportunityService.insertOpinion(businessOpportunityNo, opinionContent, id);
	}
	
	/* 의견 삭제 */
	@PostMapping(value="opinion/delete", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int deleteOpinion(String opinionNo) {

		return businessOpportunityService.deleteOpinion(opinionNo);
	}
	
	/* 고객 리스트 조회 */
	@GetMapping(value="client", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<ClientDTO> selectClientList() {

		return businessOpportunityService.selectClientList();
	}

	/* 담당자 리스트 조회 */
	@GetMapping(value="user", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<UserDTO> selectUserList() {

		return businessOpportunityService.selectUserList();
	}

	/* 상품 리스트 조회 */
	@GetMapping(value="product", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<ProductListDTO> selectProductList() {

		return businessOpportunityService.selectProductList();
	}
	
	/* 결재권자 리스트 조회 */
	@GetMapping(value="approver", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<ApproverListDTO> selectApproverList() {

		return businessOpportunityService.selectApproverList();
	}
	
	/* 영업기회 작성화면으로 이동 */
	@GetMapping("/insert")
	public void insertBusinessOpportunityPage() {}
	
	/* 영업기회 등록 */
	@PostMapping("/insert")
	public ModelAndView insertBusinessOpportunity(ModelAndView mv, 
			String[] productNo, 
			int[] quantity2, 
			int[] discount,
			int[] salePrice, 
			int[] totalPrice,
			String[] userNo2,
			String[] roleDivision,
			BusinessOpportunityDTO businessOpportunity, 
			ApprovalDTO approval,
			List<MultipartFile> multiFiles, 
			HttpServletRequest request, 
			RedirectAttributes rttr) {

		List<ProductListDTO> productList = new ArrayList<>();;

		/* 반복문을 통해서 미리 선언한 productList에 당음 */
		for(int i = 0; i < productNo.length; i++) {
			
			ProductListDTO product = new ProductListDTO();
			product.setProductNo(productNo[i]);						//상품번호
			product.setQuantity2(quantity2[i]);						//수량
			product.setDiscount(discount[i]);						//할인
			
			productList.add(product);
		}
	
		/* 지원인력대상자 등록 시에만 값을 담음 */
		List<SupportStaffDTO> supportStaffList = null;		
		if(userNo2 != null) {
			
			supportStaffList = new ArrayList<>();
			
			for(int i = 0; i <userNo2.length; i++) {
				
				SupportStaffDTO supportStaff = new SupportStaffDTO();
				supportStaff.setUserNo2(userNo2[i]);						//지원인력대상자 번호
				supportStaff.setRoleDivision(roleDivision[i]);				//지원인력대상자 역할
				
				supportStaffList.add(supportStaff);				
			}
			
		}

		/* 영업기회 등록과 동시에 파일첨부도 같이 등록한 경우 */
		if(multiFiles.size() > 0 && !multiFiles.get(0).getOriginalFilename().equals("")) {

			String root = request.getSession().getServletContext().getRealPath("/");	// 첨부파일 저장경로(절대경로)
			String filePath = root + "static/upload/";									// 첨부파일 저장경로(최종경로)

			/* 첨부파일 저장경로 (최종경로) 파일이 존재 하지 않을 경우 생성 */
			File mkdir = new File(filePath);
			if(!mkdir.exists()) {
				mkdir.mkdirs();
			}

			List<BusinessOpportunityAttachmentDTO> files = new ArrayList<>();

			/* 다중 첨부파일은 반복문을 통해서, 미리 선언한 files에 담음  */
			for(int i = 0; i < multiFiles.size(); i++) {
				String fileOriginalName = multiFiles.get(i).getOriginalFilename();			// 원본파일명
				String ext = fileOriginalName.substring(fileOriginalName.lastIndexOf("."));	// 확장자명
				String fileName = UUID.randomUUID().toString().replace("-", "") + ext;		// 저장파일명

				/* 영업기회파일첨부DTO인 file에 정보를 담은 후 리스트인 files에 담음 */
				BusinessOpportunityAttachmentDTO file = new BusinessOpportunityAttachmentDTO();			
				file.setFileName(fileName);
				file.setFileOriginalName(fileOriginalName);
				file.setFilePath(filePath);

				files.add(file);
			}
			

			/* 영업기회 등록시 필요한 정보를 BusinessOpportunityCollectionDTO에 담음 */
			BusinessOpportunityCollectionDTO businessOpportunityCollection = new BusinessOpportunityCollectionDTO();
			businessOpportunityCollection.setBusinessOpportunity(businessOpportunity);
			businessOpportunityCollection.setFiles(files);
			businessOpportunityCollection.setApproval(approval);
			businessOpportunityCollection.setProductList(productList);				
			
			/* 지원인력대상자가 있을 때만 값을 담음 */
			if(userNo2 != null) {
				businessOpportunityCollection.setSupportStaffList(supportStaffList);
			}

			int result = businessOpportunityService.insertBusinessOpportunity(businessOpportunityCollection);

			/* DB에 등록을 성공하였을 경우 */
			if(result > 0 ) {

				/* 파일 저장 */
				try {
					for(int i = 0; i < multiFiles.size(); i++) {
						
						BusinessOpportunityAttachmentDTO file = files.get(i);			
						multiFiles.get(i).transferTo(new File(filePath + "\\" + file.getFileName()));
						
					}

					rttr.addFlashAttribute("message", "영업기회 등록을 성공하였습니다.");

					/* 파일 저장을 실패하였을 경우, 저장했던 파일을 삭제
					 * 트랜젝션처리를 통해서 DB에 입력된 정보도 삭제됨 */
				} catch (Exception e) {
					e.printStackTrace();

					for(int i = 0; i < multiFiles.size(); i++) {
						
						BusinessOpportunityAttachmentDTO file = files.get(i); 
						new File(filePath + "\\" + file.getFileName()).delete(); 
						
					}
					rttr.addFlashAttribute("message", "영업기회 등록 후 파일 업로드에 실패하였습니다.");
				}


				/* DB에 영업기회 등록을 실패하였을 경우 */
			} else {
				rttr.addFlashAttribute("message", "영업기회 등록을 실패하였습니다.");	
			}

			/* 파일첨부를 하지 않고, 영업기회 등록을 하는 경우 */
		}  else {			
			BusinessOpportunityCollectionDTO businessOpportunityCollection = new BusinessOpportunityCollectionDTO();
			businessOpportunityCollection.setBusinessOpportunity(businessOpportunity);
			businessOpportunityCollection.setApproval(approval);
			businessOpportunityCollection.setProductList(productList);		
			
			/* 지원인력대상자가 있을 때만 값을 담음 */
			if(userNo2 != null) {
				businessOpportunityCollection.setSupportStaffList(supportStaffList);
			}
			
			int result = businessOpportunityService.insertBusinessOpportunity(businessOpportunityCollection);

			if(result > 0) {				
					rttr.addFlashAttribute("message", "영업기회 등록을 성공하였습니다.");
			} else {
				rttr.addFlashAttribute("message", "영업기회 등록을 실패하였습니다.");
			}
		}

		mv.setViewName("redirect:/businessOpportunity/list");
		return mv;		
	}

	/* 영업기회 삭제 */
	@PostMapping(value="delete", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int deleteBusinessOpportunity(String businessOpportunityNo) {	
		
		return businessOpportunityService.deleteBusinessOpportunity(businessOpportunityNo);
	}

}
