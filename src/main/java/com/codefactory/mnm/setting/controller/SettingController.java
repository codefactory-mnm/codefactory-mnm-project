package com.codefactory.mnm.setting.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.codefactory.mnm.setting.model.dto.SettingDTO;
import com.codefactory.mnm.setting.model.service.SettingService;

@Controller
@RequestMapping("setting")
public class SettingController {
	
	private SettingService settingService;
	
	public SettingController(SettingService settingService) {
		this.settingService = settingService;
	}
	
	/* 일반 설정 페이지 */
	@GetMapping
	public String SettingPage() {
		return "setting/setting";
	}
	
	/* 회사정보 추가 */
	@PostMapping(value = "insert", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int insertCompany(SettingDTO company) {
		
		int result = 0; 
		
		result = settingService.insertCompany(company);
		
		return result;
		
	}
	
}
