package com.codefactory.mnm.setting.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.codefactory.mnm.setting.model.dao.SettingDAO;
import com.codefactory.mnm.setting.model.dto.SettingDTO;

@Service
public class SettingServiceImpl implements SettingService {
	
	private SettingDAO mapper;
	
	@Autowired
	public SettingServiceImpl(SettingDAO mapper) {
		this.mapper = mapper;
	}
	
	/* 회사정보 추가 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertCompany(SettingDTO company) {
		
		return mapper.insertCompany(company);		// DB 로직 처리
	}

}




