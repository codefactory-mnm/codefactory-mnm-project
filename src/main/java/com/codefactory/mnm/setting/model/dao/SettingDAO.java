package com.codefactory.mnm.setting.model.dao;

import org.apache.ibatis.annotations.Mapper;

import com.codefactory.mnm.setting.model.dto.SettingDTO;

@Mapper
public interface SettingDAO {

	/* 회사정보 추가 */
	public int insertCompany(SettingDTO company);

}
