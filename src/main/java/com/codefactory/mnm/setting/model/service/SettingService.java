package com.codefactory.mnm.setting.model.service;

import com.codefactory.mnm.setting.model.dto.SettingDTO;

public interface SettingService {

	/* 회사정보 추가 */
	public int insertCompany(SettingDTO company);
}
