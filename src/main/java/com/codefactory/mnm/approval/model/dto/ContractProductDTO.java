package com.codefactory.mnm.approval.model.dto;

import java.sql.Date;
import java.util.List;

public class ContractProductDTO implements java.io.Serializable {

	private String productNo;							//상품번호
	private String productName;							//상품명
	private String price;								//단가
	private int quantity;								//수량
	private String totalPrice;							//금액
	private String salesCycle;							//매출주기
	private java.sql.Date expectationReleaseDate;		//예상출고일
	private String productNote;							//비고
	private String sumPrice;							//단가합계
	private String sumQuantity;							//총수량
	private String sumTotalPrice;						//금액합계
	private List<ContractProductDTO> productList;
	
	public ContractProductDTO() {}

	public ContractProductDTO(String productNo, String productName, String price, int quantity, String totalPrice,
			String salesCycle, Date expectationReleaseDate, String productNote, String sumPrice, String sumQuantity,
			String sumTotalPrice, List<ContractProductDTO> productList) {
		super();
		this.productNo = productNo;
		this.productName = productName;
		this.price = price;
		this.quantity = quantity;
		this.totalPrice = totalPrice;
		this.salesCycle = salesCycle;
		this.expectationReleaseDate = expectationReleaseDate;
		this.productNote = productNote;
		this.sumPrice = sumPrice;
		this.sumQuantity = sumQuantity;
		this.sumTotalPrice = sumTotalPrice;
		this.productList = productList;
	}

	public String getProductNo() {
		return productNo;
	}

	public String getProductName() {
		return productName;
	}

	public String getPrice() {
		return price;
	}

	public int getQuantity() {
		return quantity;
	}

	public String getTotalPrice() {
		return totalPrice;
	}

	public String getSalesCycle() {
		return salesCycle;
	}

	public java.sql.Date getExpectationReleaseDate() {
		return expectationReleaseDate;
	}

	public String getProductNote() {
		return productNote;
	}

	public String getSumPrice() {
		return sumPrice;
	}

	public String getSumQuantity() {
		return sumQuantity;
	}

	public String getSumTotalPrice() {
		return sumTotalPrice;
	}

	public List<ContractProductDTO> getProductList() {
		return productList;
	}

	public void setProductNo(String productNo) {
		this.productNo = productNo;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}

	public void setSalesCycle(String salesCycle) {
		this.salesCycle = salesCycle;
	}

	public void setExpectationReleaseDate(java.sql.Date expectationReleaseDate) {
		this.expectationReleaseDate = expectationReleaseDate;
	}

	public void setProductNote(String productNote) {
		this.productNote = productNote;
	}

	public void setSumPrice(String sumPrice) {
		this.sumPrice = sumPrice;
	}

	public void setSumQuantity(String sumQuantity) {
		this.sumQuantity = sumQuantity;
	}

	public void setSumTotalPrice(String sumTotalPrice) {
		this.sumTotalPrice = sumTotalPrice;
	}

	public void setProductList(List<ContractProductDTO> productList) {
		this.productList = productList;
	}

	@Override
	public String toString() {
		return "ContractProductDTO [productNo=" + productNo + ", productName=" + productName + ", price=" + price
				+ ", quantity=" + quantity + ", totalPrice=" + totalPrice + ", salesCycle=" + salesCycle
				+ ", expectationReleaseDate=" + expectationReleaseDate + ", productNote=" + productNote + ", sumPrice="
				+ sumPrice + ", sumQuantity=" + sumQuantity + ", sumTotalPrice=" + sumTotalPrice + ", productList="
				+ productList + "]";
	}

	
	
	
}
