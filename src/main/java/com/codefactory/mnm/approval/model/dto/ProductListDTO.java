package com.codefactory.mnm.approval.model.dto;

import java.util.List;

public class ProductListDTO implements java.io.Serializable {

	private String estimateNo;			//견적번호
	private String productNo;			//상품번호
	private int quantity2;				//수량
	private int discount;				//할인
	private String productName;			//상품명
	private String price;				//단가
	private String unit;				//단위
	private String standard;			//규격
	private int salePrice;				//판매단가
	private int totalPrice;				//제안금액
	private List<ProductListDTO> productList;
	
	public ProductListDTO() {}

	public ProductListDTO(String estimateNo, String productNo, int quantity2, int discount, String productName,
			String price, String unit, String standard, int salePrice, int totalPrice,
			List<ProductListDTO> productList) {
		super();
		this.estimateNo = estimateNo;
		this.productNo = productNo;
		this.quantity2 = quantity2;
		this.discount = discount;
		this.productName = productName;
		this.price = price;
		this.unit = unit;
		this.standard = standard;
		this.salePrice = salePrice;
		this.totalPrice = totalPrice;
		this.productList = productList;
	}

	public String getEstimateNo() {
		return estimateNo;
	}

	public String getProductNo() {
		return productNo;
	}

	public int getQuantity2() {
		return quantity2;
	}

	public int getDiscount() {
		return discount;
	}

	public String getProductName() {
		return productName;
	}

	public String getPrice() {
		return price;
	}

	public String getUnit() {
		return unit;
	}

	public String getStandard() {
		return standard;
	}

	public int getSalePrice() {
		return salePrice;
	}

	public int getTotalPrice() {
		return totalPrice;
	}

	public List<ProductListDTO> getProductList() {
		return productList;
	}

	public void setEstimateNo(String estimateNo) {
		this.estimateNo = estimateNo;
	}

	public void setProductNo(String productNo) {
		this.productNo = productNo;
	}

	public void setQuantity2(int quantity2) {
		this.quantity2 = quantity2;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public void setSalePrice(int salePrice) {
		this.salePrice = salePrice;
	}

	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}

	public void setProductList(List<ProductListDTO> productList) {
		this.productList = productList;
	}

	@Override
	public String toString() {
		return "ProductListDTO [estimateNo=" + estimateNo + ", productNo=" + productNo + ", quantity2=" + quantity2
				+ ", discount=" + discount + ", productName=" + productName + ", price=" + price + ", unit=" + unit
				+ ", standard=" + standard + ", salePrice=" + salePrice + ", totalPrice=" + totalPrice
				+ ", productList=" + productList + "]";
	}

	
	
}
