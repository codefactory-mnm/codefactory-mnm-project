package com.codefactory.mnm.approval.model.dto;

public class OpinionDTO implements java.io.Serializable {

	private String opinionNo;				//의견번호
	private String opinionContent;			//의견내용
	private String name;					//작성자 이름
	private String writeDate;				//작성일자
	private String estimateNo;				//견적번호
	private String id;						//아이디
	private String userNo;					//사용자번호
	private String loginUserNo;				//로그인된 사용자번호
	private String contractNo;				//계약번호
	
	public OpinionDTO () {}

	public OpinionDTO(String opinionNo, String opinionContent, String name, String writeDate, String estimateNo,
			String id, String userNo, String loginUserNo, String contractNo) {
		super();
		this.opinionNo = opinionNo;
		this.opinionContent = opinionContent;
		this.name = name;
		this.writeDate = writeDate;
		this.estimateNo = estimateNo;
		this.id = id;
		this.userNo = userNo;
		this.loginUserNo = loginUserNo;
		this.contractNo = contractNo;
	}

	public String getOpinionNo() {
		return opinionNo;
	}

	public void setOpinionNo(String opinionNo) {
		this.opinionNo = opinionNo;
	}

	public String getOpinionContent() {
		return opinionContent;
	}

	public void setOpinionContent(String opinionContent) {
		this.opinionContent = opinionContent;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWriteDate() {
		return writeDate;
	}

	public void setWriteDate(String writeDate) {
		this.writeDate = writeDate;
	}

	public String getEstimateNo() {
		return estimateNo;
	}

	public void setEstimateNo(String estimateNo) {
		this.estimateNo = estimateNo;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getLoginUserNo() {
		return loginUserNo;
	}

	public void setLoginUserNo(String loginUserNo) {
		this.loginUserNo = loginUserNo;
	}

	public String getContractNo() {
		return contractNo;
	}

	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	@Override
	public String toString() {
		return "OpinionDTO [opinionNo=" + opinionNo + ", opinionContent=" + opinionContent + ", name=" + name
				+ ", writeDate=" + writeDate + ", estimateNo=" + estimateNo + ", id=" + id + ", userNo=" + userNo
				+ ", loginUserNo=" + loginUserNo + ", contractNo=" + contractNo + "]";
	}



}
