package com.codefactory.mnm.approval.model.service;

import java.util.List;
import java.util.Map;

import com.codefactory.mnm.approval.model.dto.ApproverListDTO;
import com.codefactory.mnm.approval.model.dto.BusinessOpportunityListDTO;
import com.codefactory.mnm.approval.model.dto.ClientListDTO;
import com.codefactory.mnm.approval.model.dto.CompanyListDTO;
import com.codefactory.mnm.approval.model.dto.EstimateAttachmentDTO;
import com.codefactory.mnm.approval.model.dto.EstimateInsertDTO;
import com.codefactory.mnm.approval.model.dto.EstimateSearchDTO;
import com.codefactory.mnm.approval.model.dto.ManagerListDTO;
import com.codefactory.mnm.approval.model.dto.OpinionDTO;
import com.codefactory.mnm.approval.model.dto.ProductListDTO;
import com.codefactory.mnm.approval.model.dto.UserListDTO;
import com.codefactory.mnm.businessopportunity.model.dto.ApprovalDTO;



public interface ApprovalService {

	List<CompanyListDTO> selectCompanyList();

	List<ClientListDTO> selectClientList();

	List<BusinessOpportunityListDTO> selectBusinessOpportunityList();

	List<UserListDTO> selectUserList();

	List<ProductListDTO> selectProductList();

	Map<String, Object> selectEstimateList(EstimateSearchDTO estimateSearchDTO);

	List<ApproverListDTO> selectApproverList();

	int insertEstimate(EstimateInsertDTO estimateInsertDTO);

	List<ManagerListDTO> selectManagerList(String deptName);

	int insertEstimateProduct(ProductListDTO productListDTO);

	Map<String, Object> selectEstimate(String estimateNo);

	int deleteEstimate(String estimateNo);

	EstimateAttachmentDTO selectFile(String fileNo);

	List<OpinionDTO> selectOpinionList(String estimateNo);

	int insertOpinion(String estimateNo, String opinionContent, String id);

	int deleteOpinion(String opinionNo);

	Map<String, Object> selectWaitEstimateList(EstimateSearchDTO estimateSearchDTO);

	Map<String, Object> selectFinishEstimateList(EstimateSearchDTO estimateSearchDTO);

	int updateEstimateStatus(ApprovalDTO approvalDTO);

}
