package com.codefactory.mnm.approval.model.dto;

public class CompanyListDTO implements java.io.Serializable {

	private String clientCompanyNo;				//고객사번호
	private String clientCompanyName;			//고객명
	
	public CompanyListDTO() {}

	public CompanyListDTO(String clientCompanyNo, String clientCompanyName) {
		super();
		this.clientCompanyNo = clientCompanyNo;
		this.clientCompanyName = clientCompanyName;
	}

	public String getClientCompanyNo() {
		return clientCompanyNo;
	}

	public String getClientCompanyName() {
		return clientCompanyName;
	}

	public void setClientCompanyNo(String clientCompanyNo) {
		this.clientCompanyNo = clientCompanyNo;
	}

	public void setClientCompanyName(String clientCompanyName) {
		this.clientCompanyName = clientCompanyName;
	}

	@Override
	public String toString() {
		return "CompanyListDTO [clientCompanyNo=" + clientCompanyNo + ", clientCompanyName=" + clientCompanyName + "]";
	}
	
	
}
