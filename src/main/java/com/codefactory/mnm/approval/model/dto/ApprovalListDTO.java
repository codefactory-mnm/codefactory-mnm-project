package com.codefactory.mnm.approval.model.dto;

public class ApprovalListDTO implements java.io.Serializable {

	private String approvalNo;					//결재번호
	private String approverNo;					//결재권자번호
	private String approvalStatus;				//결재상태
	private String approvalOpnion;				//결재의견
	private String division;					//구분
	private String businessOpportunityNo;		//영업기회번호
	private String estimateNo;					//견적번호
	private String contractNo;					//계약번호
	private String delYn;						//삭제여부
	
	public ApprovalListDTO() {}

	public ApprovalListDTO(String approvalNo, String approverNo, String approvalStatus, String approvalOpnion,
			String division, String businessOpportunityNo, String estimateNo, String contractNo, String delYn) {
		super();
		this.approvalNo = approvalNo;
		this.approverNo = approverNo;
		this.approvalStatus = approvalStatus;
		this.approvalOpnion = approvalOpnion;
		this.division = division;
		this.businessOpportunityNo = businessOpportunityNo;
		this.estimateNo = estimateNo;
		this.contractNo = contractNo;
		this.delYn = delYn;
	}

	public String getApprovalNo() {
		return approvalNo;
	}

	public String getApproverNo() {
		return approverNo;
	}

	public String getApprovalStatus() {
		return approvalStatus;
	}

	public String getApprovalOpnion() {
		return approvalOpnion;
	}

	public String getDivision() {
		return division;
	}

	public String getBusinessOpportunityNo() {
		return businessOpportunityNo;
	}

	public String getEstimateNo() {
		return estimateNo;
	}

	public String getContractNo() {
		return contractNo;
	}

	public String getDelYn() {
		return delYn;
	}

	public void setApprovalNo(String approvalNo) {
		this.approvalNo = approvalNo;
	}

	public void setApproverNo(String approverNo) {
		this.approverNo = approverNo;
	}

	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public void setApprovalOpnion(String approvalOpnion) {
		this.approvalOpnion = approvalOpnion;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public void setBusinessOpportunityNo(String businessOpportunityNo) {
		this.businessOpportunityNo = businessOpportunityNo;
	}

	public void setEstimateNo(String estimateNo) {
		this.estimateNo = estimateNo;
	}

	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}

	@Override
	public String toString() {
		return "ApprovalListDTO [approvalNo=" + approvalNo + ", approverNo=" + approverNo + ", approvalStatus="
				+ approvalStatus + ", approvalOpnion=" + approvalOpnion + ", division=" + division
				+ ", businessOpportunityNo=" + businessOpportunityNo + ", estimateNo=" + estimateNo + ", contractNo="
				+ contractNo + ", delYn=" + delYn + "]";
	}
	
	
}
