package com.codefactory.mnm.approval.model.dto;

public class UserListDTO implements java.io.Serializable {

	private String userNo;				//사용자번호
	private String name;				//사용자명
	
	public UserListDTO() {}

	public UserListDTO(String userNo, String name) {
		super();
		this.userNo = userNo;
		this.name = name;
	}

	public String getUserNo() {
		return userNo;
	}

	public String getName() {
		return name;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "UserListDTO [userNo=" + userNo + ", name=" + name + "]";
	}
	
	
}
