package com.codefactory.mnm.approval.model.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.codefactory.mnm.approval.model.dto.ApprovalListDTO;
import com.codefactory.mnm.approval.model.dto.ApproverListDTO;
import com.codefactory.mnm.approval.model.dto.BusinessOpportunityListDTO;
import com.codefactory.mnm.approval.model.dto.ClientListDTO;
import com.codefactory.mnm.approval.model.dto.CompanyListDTO;
import com.codefactory.mnm.approval.model.dto.DeptListDTO;
import com.codefactory.mnm.approval.model.dto.EstimateAttachmentDTO;
import com.codefactory.mnm.approval.model.dto.EstimateDTO;
import com.codefactory.mnm.approval.model.dto.EstimateListDTO;
import com.codefactory.mnm.approval.model.dto.EstimateSearchDTO;
import com.codefactory.mnm.approval.model.dto.ManagerListDTO;
import com.codefactory.mnm.approval.model.dto.OpinionDTO;
import com.codefactory.mnm.approval.model.dto.ProductListDTO;
import com.codefactory.mnm.approval.model.dto.UserListDTO;
import com.codefactory.mnm.businessopportunity.model.dto.ApprovalDTO;


@Mapper
public interface ApprovalDAO {

	List<CompanyListDTO> selectCompanyList();

	List<ClientListDTO> selectClientList();

	List<BusinessOpportunityListDTO> selectBusinessOpportunityList();

	List<UserListDTO> selectUserList();

	List<ProductListDTO> selectProductList();

	int selectEstimateListCount();

	List<ApproverListDTO> selectApproverList();
	
	int insertEstimate(EstimateDTO estimateDTO);

	int insertApproval(ApprovalListDTO approvalListDTO);

	int insertApprovalHistory();
	
	int insertEstimateAttachment(EstimateAttachmentDTO estimateAttachmentDTO);

	List<DeptListDTO> selectDeptList();

	List<ManagerListDTO> selectManagerList(String deptName);

	int selectTotalCount(EstimateSearchDTO estimateSearchDTO);

	int insertEstimateProduct(ProductListDTO productListDTO);

	EstimateListDTO selectEstimate(String estimateNo);

	String selectApprover(String estimateNo);

	int deleteEstimate(String estimateNo);

	List<EstimateListDTO> selectFileList(String estimateNo);

	List<ProductListDTO> selectEstimateProductList(String estimateNo);

	EstimateAttachmentDTO selectFile(String fileNo);

	List<OpinionDTO> selectOpinionList(String estimateNo);

	OpinionDTO selectUserNoAndName(String id);

	int insertOpinion(OpinionDTO opinion);

	int deleteOpinion(String opinionNo);

	String selectUserNo(String id);

	List<EstimateListDTO> selectEstimateList(EstimateSearchDTO estimateSearchDTO);

	int updateEstimateStatus(ApprovalDTO approvalDTO);

	int selectRequestTotalCount(EstimateSearchDTO estimateSearchDTO);

	List<EstimateListDTO> selectRequestEstimateList(EstimateSearchDTO estimateSearchDTO);

	int selectFinishTotalCount(EstimateSearchDTO estimateSearchDTO);

	List<EstimateListDTO> selectFinishEstimateList(EstimateSearchDTO estimateSearchDTO);

	int insertEstimateStatus(ApprovalDTO approvalDTO);

	String selectApproveNo(String estimateNo);
}
