package com.codefactory.mnm.approval.model.dto;

import com.codefactory.mnm.common.SelectCriteria;

public class ContractSearchDTO implements java.io.Serializable {

	private String division;					//구분
	private String searchName;					//검색어
	private String deptName;					//담당자 부서
	private String name;						//담당자이름
	private String currentPage;					//현재페이지
	private SelectCriteria selectCriteria;		//페이징처리
	private String id;							//세션에 담겨있는 사용자 아이디
	private String userNo;						//사용자 번호
	
	public ContractSearchDTO() {}

	public ContractSearchDTO(String division, String searchName, String deptName, String name, String currentPage,
			SelectCriteria selectCriteria, String id, String userNo) {
		super();
		this.division = division;
		this.searchName = searchName;
		this.deptName = deptName;
		this.name = name;
		this.currentPage = currentPage;
		this.selectCriteria = selectCriteria;
		this.id = id;
		this.userNo = userNo;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getSearchName() {
		return searchName;
	}

	public void setSearchName(String searchName) {
		this.searchName = searchName;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(String currentPage) {
		this.currentPage = currentPage;
	}

	public SelectCriteria getSelectCriteria() {
		return selectCriteria;
	}

	public void setSelectCriteria(SelectCriteria selectCriteria) {
		this.selectCriteria = selectCriteria;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	@Override
	public String toString() {
		return "ContractSearchDTO [division=" + division + ", searchName=" + searchName + ", deptName=" + deptName
				+ ", name=" + name + ", currentPage=" + currentPage + ", selectCriteria=" + selectCriteria + ", id="
				+ id + ", userNo=" + userNo + "]";
	}
	
	
	
	
}
