package com.codefactory.mnm.approval.model.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.codefactory.mnm.approval.model.dao.ApprovalContractDAO;
import com.codefactory.mnm.approval.model.dto.ContractAttachmentDTO;
import com.codefactory.mnm.approval.model.dto.ContractDTO;
import com.codefactory.mnm.approval.model.dto.ContractProductDTO;
import com.codefactory.mnm.approval.model.dto.ContractSearchDTO;
import com.codefactory.mnm.approval.model.dto.DeptListDTO;
import com.codefactory.mnm.approval.model.dto.OpinionDTO;
import com.codefactory.mnm.common.Pagenation;
import com.codefactory.mnm.common.SelectCriteria;



@Service
public class ApprovalContractServiceImpl implements ApprovalContractService{

private final ApprovalContractDAO approvalContractDAO;
	
	@Autowired
	public ApprovalContractServiceImpl(ApprovalContractDAO approvalContractDAO) {
		
		this.approvalContractDAO = approvalContractDAO;
	}
	
	/* 계약 목록 조회*/
	@Override
	public Map<String, Object> selectContractList(ContractSearchDTO contractSearchDTO) {

		/* 첫 요청페이지 */
		int pageNo = 1;		
		
		/* 요청 페이지가 0이하면 1로 바꿔줌 */
		if(pageNo <= 0) {
			pageNo = 1;
		}
		
		/* 최대 페이지 */
		int limit = 1;
		
		/* 최대 버튼 갯수 */
		int buttonAmount = 5;
		
		/* 사용자 아이디 */
		String id = contractSearchDTO.getId();
		String userNo = approvalContractDAO.selectUserNo(id);
		System.out.println("userNo : " + userNo);
		contractSearchDTO.setUserNo(userNo);
		
		/* 전체 게시물 수, 검색조건이 있다면 검색조건에 맞는 게시물 수 */
		int totalCount = approvalContractDAO.selectTotalCount(contractSearchDTO);

		/* view에서 전달받은 요청페이지 */
		String currentPage = contractSearchDTO.getCurrentPage();
		
		/* view에서 전달받은 요청페이지를 pageNo에 저장 */
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		/* 설정한 변수에 따른 페이징조건 */
		SelectCriteria selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount);
				
		/* 조회 결과를 담을 Map 생성 */
		Map<String, Object> map = new HashMap<>();
		
		contractSearchDTO.setSelectCriteria(selectCriteria);
		
		/* 견적 조회 */
		List<ContractDTO> contractList = approvalContractDAO.selectContractList(contractSearchDTO);
		
		/* 부서 조회 */
		List<DeptListDTO> deptList = approvalContractDAO.selectDeptList();
		
		map.put("totalCount", totalCount);
		map.put("selectCriteria", selectCriteria);
		map.put("contractList", contractList);
		map.put("deptList", deptList);
		
		return map;
	}
	
	/* 계약 상세조회 */
	@Override
	public Map<String, Object> selectContract(String contractNo) {

		Map<String, Object> map = new HashMap<>();
		
		ContractDTO oneContract = approvalContractDAO.selectContract(contractNo);
		String approver = approvalContractDAO.selectApprover(contractNo);
		List<ContractDTO> fileList = approvalContractDAO.selectFileList(contractNo);
		List<ContractProductDTO> productList = approvalContractDAO.selectContractProductList(contractNo);
		
		map.put("oneContract", oneContract);
		map.put("approver", approver);
		map.put("fileList", fileList);
		map.put("productList", productList);
		
		return map;
	}

	/* 파일정보 조회 */
	@Override
	public ContractAttachmentDTO selectFile(String fileNo) {

		return approvalContractDAO.selectFile(fileNo);
	}

	/* 의견 목록 조회 */
	@Override
	public List<OpinionDTO> selectOpinionList(String contractNo) {
		
		return approvalContractDAO.selectOpinionList(contractNo);
	}

	/* 의견 작성 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertOpinion(String contractNo, String opinionContent, String id) {

		/* 로그인된 사용자의 번호, 이름 조회하여 저장 */
		OpinionDTO opinion = approvalContractDAO.selectUserNoAndName(id);
		
		/* 고객사번호, 의견내용, 아이디를 저장 */
		opinion.setContractNo(contractNo);
		opinion.setOpinionContent(opinionContent);
		opinion.setId(id);
		
		/* 의견작성에 필요한 정보를 전달하고 결과를 전달받음 */
		return approvalContractDAO.insertOpinion(opinion);
	}

	/* 의견 삭제 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int deleteOpinion(String opinionNo) {

		return approvalContractDAO.deleteOpinion(opinionNo);
	}

	/* 계약 삭제 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int deleteContract(String contractNo) {

		return approvalContractDAO.deleteContract(contractNo);
	}
	
}
