package com.codefactory.mnm.approval.model.dto;

public class ContractAttachmentDTO implements java.io.Serializable {

	private String fileNo;				//파일번호
	private String fileName;			//파일명
	private String fileOriginalName;	//실제파일명
	private String filePath;			//경로
	private String division;			//구분
	private String contractNo;			//계약번호
	
	public ContractAttachmentDTO() {}

	public ContractAttachmentDTO(String fileNo, String fileName, String fileOriginalName, String filePath,
			String division, String contractNo) {
		super();
		this.fileNo = fileNo;
		this.fileName = fileName;
		this.fileOriginalName = fileOriginalName;
		this.filePath = filePath;
		this.division = division;
		this.contractNo = contractNo;
	}

	public String getFileNo() {
		return fileNo;
	}

	public String getFileName() {
		return fileName;
	}

	public String getFileOriginalName() {
		return fileOriginalName;
	}

	public String getFilePath() {
		return filePath;
	}

	public String getDivision() {
		return division;
	}

	public String getContractNo() {
		return contractNo;
	}

	public void setFileNo(String fileNo) {
		this.fileNo = fileNo;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setFileOriginalName(String fileOriginalName) {
		this.fileOriginalName = fileOriginalName;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	@Override
	public String toString() {
		return "ContractAttachmentDTO [fileNo=" + fileNo + ", fileName=" + fileName + ", fileOriginalName="
				+ fileOriginalName + ", filePath=" + filePath + ", division=" + division + ", contractNo=" + contractNo
				+ "]";
	}
	
	
}
