package com.codefactory.mnm.approval.model.service;

import java.util.List;
import java.util.Map;

import com.codefactory.mnm.approval.model.dto.ContractAttachmentDTO;
import com.codefactory.mnm.approval.model.dto.ContractSearchDTO;
import com.codefactory.mnm.approval.model.dto.OpinionDTO;

public interface ApprovalContractService {

	Map<String, Object> selectContractList(ContractSearchDTO contractSearchDTO);

	Map<String, Object> selectContract(String contractNo);

	List<OpinionDTO> selectOpinionList(String contractNo);

	int insertOpinion(String contractNo, String opinionContent, String id);

	int deleteOpinion(String opinionNo);

	int deleteContract(String contractNo);

	ContractAttachmentDTO selectFile(String fileNo);

}
