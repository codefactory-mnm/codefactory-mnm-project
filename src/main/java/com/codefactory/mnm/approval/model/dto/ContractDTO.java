package com.codefactory.mnm.approval.model.dto;

import java.sql.Date;

public class ContractDTO implements java.io.Serializable {

	private String contractNo;							//계약번호
	private String contractName;						//계약명
	private java.sql.Date contractDate;					//계약일
	private java.sql.Date startDate;					//계약시작일
	private java.sql.Date endDate;						//계약종료일
	private String totalContractAmount;					//총계약금액
	private String vat;									//부가세
	private String paymentTerms;						//대금지급조건
	private int warrantyPeriod;							//하자보증기간
	private String note;								//비고
	private String clientNo;							//고객번호
	private String clientName;							//고객명
	private String clientCompanyNo;						//고객사번호
	private String clientCompanyName;					//고객사명
	private String suspenseReceipt;						//가수금액
	private java.sql.Date writeDate;					//작성일자
	private String delYn;								//삭제여부
	private String businessOpportunityNo;				//영업기회번호
	private String businessOpportunityName;				//영업기회명
	private String estimateNo;							//견적번호
	private String estimateName;						//견적명
	private String userNo;								//담당자번호
	private String name;								//담당자명
	private String approverNo;							//결재권자번호
	private String approverName;						//결재권자명
	private String approvalStatus;						//결재상태
	private String opinionContent;						//결재의견
	private String fileName;							//파일명
	private String fileOriginalName;					//원본파일명
	private String fileNo;								//파일번호
	private String filePath;							//파일경로
	
	public ContractDTO() {}

	public ContractDTO(String contractNo, String contractName, Date contractDate, Date startDate, Date endDate,
			String totalContractAmount, String vat, String paymentTerms, int warrantyPeriod, String note,
			String clientNo, String clientName, String clientCompanyNo, String clientCompanyName,
			String suspenseReceipt, Date writeDate, String delYn, String businessOpportunityNo,
			String businessOpportunityName, String estimateNo, String estimateName, String userNo, String name,
			String approverNo, String approverName, String approvalStatus, String opinionContent, String fileName,
			String fileOriginalName, String fileNo, String filePath) {
		super();
		this.contractNo = contractNo;
		this.contractName = contractName;
		this.contractDate = contractDate;
		this.startDate = startDate;
		this.endDate = endDate;
		this.totalContractAmount = totalContractAmount;
		this.vat = vat;
		this.paymentTerms = paymentTerms;
		this.warrantyPeriod = warrantyPeriod;
		this.note = note;
		this.clientNo = clientNo;
		this.clientName = clientName;
		this.clientCompanyNo = clientCompanyNo;
		this.clientCompanyName = clientCompanyName;
		this.suspenseReceipt = suspenseReceipt;
		this.writeDate = writeDate;
		this.delYn = delYn;
		this.businessOpportunityNo = businessOpportunityNo;
		this.businessOpportunityName = businessOpportunityName;
		this.estimateNo = estimateNo;
		this.estimateName = estimateName;
		this.userNo = userNo;
		this.name = name;
		this.approverNo = approverNo;
		this.approverName = approverName;
		this.approvalStatus = approvalStatus;
		this.opinionContent = opinionContent;
		this.fileName = fileName;
		this.fileOriginalName = fileOriginalName;
		this.fileNo = fileNo;
		this.filePath = filePath;
	}

	public String getContractNo() {
		return contractNo;
	}

	public String getContractName() {
		return contractName;
	}

	public java.sql.Date getContractDate() {
		return contractDate;
	}

	public java.sql.Date getStartDate() {
		return startDate;
	}

	public java.sql.Date getEndDate() {
		return endDate;
	}

	public String getTotalContractAmount() {
		return totalContractAmount;
	}

	public String getVat() {
		return vat;
	}

	public String getPaymentTerms() {
		return paymentTerms;
	}

	public int getWarrantyPeriod() {
		return warrantyPeriod;
	}

	public String getNote() {
		return note;
	}

	public String getClientNo() {
		return clientNo;
	}

	public String getClientName() {
		return clientName;
	}

	public String getClientCompanyNo() {
		return clientCompanyNo;
	}

	public String getClientCompanyName() {
		return clientCompanyName;
	}

	public String getSuspenseReceipt() {
		return suspenseReceipt;
	}

	public java.sql.Date getWriteDate() {
		return writeDate;
	}

	public String getDelYn() {
		return delYn;
	}

	public String getBusinessOpportunityNo() {
		return businessOpportunityNo;
	}

	public String getBusinessOpportunityName() {
		return businessOpportunityName;
	}

	public String getEstimateNo() {
		return estimateNo;
	}

	public String getEstimateName() {
		return estimateName;
	}

	public String getUserNo() {
		return userNo;
	}

	public String getName() {
		return name;
	}

	public String getApproverNo() {
		return approverNo;
	}

	public String getApproverName() {
		return approverName;
	}

	public String getApprovalStatus() {
		return approvalStatus;
	}

	public String getOpinionContent() {
		return opinionContent;
	}

	public String getFileName() {
		return fileName;
	}

	public String getFileOriginalName() {
		return fileOriginalName;
	}

	public String getFileNo() {
		return fileNo;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	public void setContractName(String contractName) {
		this.contractName = contractName;
	}

	public void setContractDate(java.sql.Date contractDate) {
		this.contractDate = contractDate;
	}

	public void setStartDate(java.sql.Date startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(java.sql.Date endDate) {
		this.endDate = endDate;
	}

	public void setTotalContractAmount(String totalContractAmount) {
		this.totalContractAmount = totalContractAmount;
	}

	public void setVat(String vat) {
		this.vat = vat;
	}

	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public void setWarrantyPeriod(int warrantyPeriod) {
		this.warrantyPeriod = warrantyPeriod;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public void setClientNo(String clientNo) {
		this.clientNo = clientNo;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public void setClientCompanyNo(String clientCompanyNo) {
		this.clientCompanyNo = clientCompanyNo;
	}

	public void setClientCompanyName(String clientCompanyName) {
		this.clientCompanyName = clientCompanyName;
	}

	public void setSuspenseReceipt(String suspenseReceipt) {
		this.suspenseReceipt = suspenseReceipt;
	}

	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}

	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}

	public void setBusinessOpportunityNo(String businessOpportunityNo) {
		this.businessOpportunityNo = businessOpportunityNo;
	}

	public void setBusinessOpportunityName(String businessOpportunityName) {
		this.businessOpportunityName = businessOpportunityName;
	}

	public void setEstimateNo(String estimateNo) {
		this.estimateNo = estimateNo;
	}

	public void setEstimateName(String estimateName) {
		this.estimateName = estimateName;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setApproverNo(String approverNo) {
		this.approverNo = approverNo;
	}

	public void setApproverName(String approverName) {
		this.approverName = approverName;
	}

	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public void setOpinionContent(String opinionContent) {
		this.opinionContent = opinionContent;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setFileOriginalName(String fileOriginalName) {
		this.fileOriginalName = fileOriginalName;
	}

	public void setFileNo(String fileNo) {
		this.fileNo = fileNo;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	@Override
	public String toString() {
		return "ContractDTO [contractNo=" + contractNo + ", contractName=" + contractName + ", contractDate="
				+ contractDate + ", startDate=" + startDate + ", endDate=" + endDate + ", totalContractAmount="
				+ totalContractAmount + ", vat=" + vat + ", paymentTerms=" + paymentTerms + ", warrantyPeriod="
				+ warrantyPeriod + ", note=" + note + ", clientNo=" + clientNo + ", clientName=" + clientName
				+ ", clientCompanyNo=" + clientCompanyNo + ", clientCompanyName=" + clientCompanyName
				+ ", suspenseReceipt=" + suspenseReceipt + ", writeDate=" + writeDate + ", delYn=" + delYn
				+ ", businessOpportunityNo=" + businessOpportunityNo + ", businessOpportunityName="
				+ businessOpportunityName + ", estimateNo=" + estimateNo + ", estimateName=" + estimateName
				+ ", userNo=" + userNo + ", name=" + name + ", approverNo=" + approverNo + ", approverName="
				+ approverName + ", approvalStatus=" + approvalStatus + ", opinionContent=" + opinionContent
				+ ", fileName=" + fileName + ", fileOriginalName=" + fileOriginalName + ", fileNo=" + fileNo
				+ ", filePath=" + filePath + "]";
	}

	
}
