package com.codefactory.mnm.approval.model.dto;

import java.sql.Date;

public class ApprovalHistoryListDTO implements java.io.Serializable {

	private String approvalChangeHisNo;			//상태변경 이력번호
	private String approvalStatus;				//결재상태
	private java.sql.Date changeDate;			//변경일자
	private String approvalNo;					//결재번호
	
	public ApprovalHistoryListDTO() {}

	public ApprovalHistoryListDTO(String approvalChangeHisNo, String approvalStatus, Date changeDate,
			String approvalNo) {
		super();
		this.approvalChangeHisNo = approvalChangeHisNo;
		this.approvalStatus = approvalStatus;
		this.changeDate = changeDate;
		this.approvalNo = approvalNo;
	}

	public String getApprovalChangeHisNo() {
		return approvalChangeHisNo;
	}

	public String getApprovalStatus() {
		return approvalStatus;
	}

	public java.sql.Date getChangeDate() {
		return changeDate;
	}

	public String getApprovalNo() {
		return approvalNo;
	}

	public void setApprovalChangeHisNo(String approvalChangeHisNo) {
		this.approvalChangeHisNo = approvalChangeHisNo;
	}

	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public void setChangeDate(java.sql.Date changeDate) {
		this.changeDate = changeDate;
	}

	public void setApprovalNo(String approvalNo) {
		this.approvalNo = approvalNo;
	}

	@Override
	public String toString() {
		return "ApprovalHistoryListDTO [approvalChangeHisNo=" + approvalChangeHisNo + ", approvalStatus="
				+ approvalStatus + ", changeDate=" + changeDate + ", approvalNo=" + approvalNo + "]";
	}
	
	
}
