package com.codefactory.mnm.approval.model.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.codefactory.mnm.approval.model.dto.ContractAttachmentDTO;
import com.codefactory.mnm.approval.model.dto.ContractDTO;
import com.codefactory.mnm.approval.model.dto.ContractProductDTO;
import com.codefactory.mnm.approval.model.dto.ContractSearchDTO;
import com.codefactory.mnm.approval.model.dto.DeptListDTO;
import com.codefactory.mnm.approval.model.dto.OpinionDTO;

@Mapper
public interface ApprovalContractDAO {

	int selectTotalCount(ContractSearchDTO contractSearchDTO);

	List<ContractDTO> selectContractList(ContractSearchDTO contractSearchDTO);

	List<DeptListDTO> selectDeptList();

	String selectUserNo(String id);

	ContractDTO selectContract(String contractNo);

	String selectApprover(String contractNo);

	List<ContractDTO> selectFileList(String contractNo);

	List<ContractProductDTO> selectContractProductList(String contractNo);

	ContractAttachmentDTO selectFile(String fileNo);

	List<OpinionDTO> selectOpinionList(String contractNo);

	OpinionDTO selectUserNoAndName(String id);

	int insertOpinion(OpinionDTO opinion);

	int deleteOpinion(String opinionNo);

	int deleteContract(String contractNo);

}
