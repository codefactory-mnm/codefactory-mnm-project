package com.codefactory.mnm.approval.model.dto;

public class EstimateAttachmentDTO implements java.io.Serializable {

	private String fileNo;				//파일번호
	private String fileName;			//파일명
	private String fileOriginalName;	//실제파일명
	private String filePath;			//경로
	private String division;			//구분
	private String estimateNo;			//견적번호
	
	public EstimateAttachmentDTO() {}

	public EstimateAttachmentDTO(String fileNo, String fileName, String fileOriginalName, String filePath,
			String division, String estimateNo) {
		super();
		this.fileNo = fileNo;
		this.fileName = fileName;
		this.fileOriginalName = fileOriginalName;
		this.filePath = filePath;
		this.division = division;
		this.estimateNo = estimateNo;
	}

	public String getFileNo() {
		return fileNo;
	}

	public String getFileName() {
		return fileName;
	}

	public String getFileOriginalName() {
		return fileOriginalName;
	}

	public String getFilePath() {
		return filePath;
	}

	public String getDivision() {
		return division;
	}

	public String getEstimateNo() {
		return estimateNo;
	}

	public void setFileNo(String fileNo) {
		this.fileNo = fileNo;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setFileOriginalName(String fileOriginalName) {
		this.fileOriginalName = fileOriginalName;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public void setEstimateNo(String estimateNo) {
		this.estimateNo = estimateNo;
	}

	@Override
	public String toString() {
		return "EstimateAttachmentDTO [fileNo=" + fileNo + ", fileName=" + fileName + ", fileOriginalName="
				+ fileOriginalName + ", filePath=" + filePath + ", division=" + division + ", estimateNo=" + estimateNo
				+ "]";
	}
	
	
}
