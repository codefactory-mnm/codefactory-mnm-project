package com.codefactory.mnm.approval.model.dto;

public class EstimateProductDTO implements java.io.Serializable {

	private String estimateNo;				//견적번호
	private String productNo;				//상품번호
	private int quantity;					//수량
	private int discount;					//할인
	
	public EstimateProductDTO() {}

	public EstimateProductDTO(String estimateNo, String productNo, int quantity, int discount) {
		super();
		this.estimateNo = estimateNo;
		this.productNo = productNo;
		this.quantity = quantity;
		this.discount = discount;
	}

	public String getEstimateNo() {
		return estimateNo;
	}

	public String getProductNo() {
		return productNo;
	}

	public int getQuantity() {
		return quantity;
	}

	public int getDiscount() {
		return discount;
	}

	public void setEstimateNo(String estimateNo) {
		this.estimateNo = estimateNo;
	}

	public void setProductNo(String productNo) {
		this.productNo = productNo;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	@Override
	public String toString() {
		return "EstimateProductDTO [estimateNo=" + estimateNo + ", productNo=" + productNo + ", quantity=" + quantity
				+ ", discount=" + discount + "]";
	}
	
	
}
