package com.codefactory.mnm.approval.model.dto;

public class ClientListDTO implements java.io.Serializable {

	private String clientNo;			//고객번호
	private String clientName;			//고객명
	
	public ClientListDTO() {}

	public ClientListDTO(String clientNo, String clientName) {
		super();
		this.clientNo = clientNo;
		this.clientName = clientName;
	}

	public String getClientNo() {
		return clientNo;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientNo(String clientNo) {
		this.clientNo = clientNo;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	@Override
	public String toString() {
		return "ClientListDTO [clientNo=" + clientNo + ", clientName=" + clientName + "]";
	}
	
	
}
