package com.codefactory.mnm.approval.model.dto;

import java.util.List;

public class EstimateInsertDTO implements java.io.Serializable {

	private EstimateDTO estimateDTO;					//견적DTO
	private ApprovalListDTO approvalListDTO;			//결재DTO
	private List<EstimateAttachmentDTO> files;			//견적 첨부파일 List
	
	public EstimateInsertDTO() {}

	public EstimateInsertDTO(EstimateDTO estimateDTO, ApprovalListDTO approvalListDTO,
			List<EstimateAttachmentDTO> files) {
		super();
		this.estimateDTO = estimateDTO;
		this.approvalListDTO = approvalListDTO;
		this.files = files;
	}

	public EstimateDTO getEstimateDTO() {
		return estimateDTO;
	}

	public ApprovalListDTO getApprovalListDTO() {
		return approvalListDTO;
	}

	public List<EstimateAttachmentDTO> getFiles() {
		return files;
	}

	public void setEstimateDTO(EstimateDTO estimateDTO) {
		this.estimateDTO = estimateDTO;
	}

	public void setApprovalListDTO(ApprovalListDTO approvalListDTO) {
		this.approvalListDTO = approvalListDTO;
	}

	public void setFiles(List<EstimateAttachmentDTO> files) {
		this.files = files;
	}

	@Override
	public String toString() {
		return "EstimateInsertDTO [estimateDTO=" + estimateDTO + ", approvalListDTO=" + approvalListDTO + ", files="
				+ files + "]";
	}
	
	
}
