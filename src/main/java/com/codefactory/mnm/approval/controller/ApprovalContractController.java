package com.codefactory.mnm.approval.controller;

import java.security.Principal;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.codefactory.mnm.approval.model.dto.ContractDTO;
import com.codefactory.mnm.approval.model.dto.ContractProductDTO;
import com.codefactory.mnm.approval.model.dto.ContractSearchDTO;
import com.codefactory.mnm.approval.model.dto.DeptListDTO;
import com.codefactory.mnm.approval.model.dto.OpinionDTO;
import com.codefactory.mnm.approval.model.service.ApprovalContractService;
import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.member.model.dto.UserImpl;


@Controller
@RequestMapping("/approval")
public class ApprovalContractController {

private final ApprovalContractService approvalContractService;
	
	@Autowired
	public ApprovalContractController(ApprovalContractService approvalContractService) {
		
		this.approvalContractService = approvalContractService;
	}
	
	/* 계약 목록 조회 */
	@GetMapping("/approvalRequest/contract/list")
	public ModelAndView selectContract(ModelAndView mv,
			@RequestParam(defaultValue = "") String division,
			@RequestParam(defaultValue = "") String searchName,
			@RequestParam(defaultValue = "") String deptName,
			@RequestParam(defaultValue = "") String name,
			@RequestParam(defaultValue = "1") String currentPage,
			Principal pcp) {
		
		String id = pcp.getName();	//세션에 담겨있는 사용자 아이디
		
		System.out.println("division : " + division);
		System.out.println("searchName : " + searchName);
		System.out.println("deptName : " + deptName);
		System.out.println("name : " + name);
		System.out.println("currentPage : " + currentPage);
		
		/* 처음 견적 리스트를 조회하거나 또는 검색조건을 설정하지 않았을 때를 대비 */
		if(division.equals("")) { // 검색조건 중 구분을 선택하지 않았을 경우
			division = null;
		}
		
		if(searchName.equals("")) { // 검색조건 중 고객사를 입력하지 않았을 경우
			searchName = null;
		}
		
		if(deptName.equals("")) { // 검색조건 중 부서를 선택하지 않았을 경우
			deptName = null;
		}
		
		if(name.equals("")) { // 검색조건 중 이름을 선택하지 않았을 경우
			name = null;
		}
		
		/* 검색조건을 담는다. 검색조건이 없을 경우 null로 담김. */
		ContractSearchDTO contractSearchDTO = new ContractSearchDTO();
		contractSearchDTO.setDivision(division);
		contractSearchDTO.setSearchName(searchName);
		contractSearchDTO.setDeptName(deptName);
		contractSearchDTO.setName(name);
		contractSearchDTO.setCurrentPage(currentPage);
		contractSearchDTO.setId(id);

		
		System.out.println("contractSearchDTO : " + contractSearchDTO);
		
		/* 검색조건을 searvice로 보내주고, 값을 map으로 전달받음 */
		Map<String, Object> map = approvalContractService.selectContractList(contractSearchDTO);
		
		/* db에서 가져온 값을 꺼낸다. */
		List<ContractDTO> contractList = (List<ContractDTO>) map.get("contractList");
		List<DeptListDTO> deptList = (List<DeptListDTO>) map.get("deptList");
		SelectCriteria selectCriteria = (SelectCriteria) map.get("selectCriteria");
		int totalCount = (int) map.get("totalCount");
		System.out.println("contractList : " + contractList);
		System.out.println("deptList : " + deptList);
		
		/* db에서 조회해온 결과를 담는다 */
		mv.addObject("contractList", contractList);
		mv.addObject("deptList", deptList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("totalCount", totalCount);
		mv.addObject("contractSearchDTO", contractSearchDTO);
		mv.setViewName("approval/approvalRequest/contract/list");
		
		return mv;
	}
	
	/* 계약 상세조회 */
	@GetMapping("/approvalRequest/contract/detail")
	public ModelAndView selectContract(ModelAndView mv, @RequestParam String contractNo) {
		
		System.out.println("contractNo : " + contractNo);
		
		/* Map을 생성하여 서비스에서 값을 리턴 받는다*/
		Map<String, Object> map = approvalContractService.selectContract(contractNo);
		
		/* map에 담은 값을 꺼낸다 */
		ContractDTO oneContract = (ContractDTO) map.get("oneContract");
		String approver = (String) map.get("approver");
		List<ContractDTO> fileList = (List<ContractDTO>) map.get("fileList");
		List<ContractProductDTO> productList = (List<ContractProductDTO>) map.get("productList");
		System.out.println("견적명 : " + oneContract.getEstimateName());
		System.out.println("oneContract : " + oneContract);
		System.out.println("approver : " + approver);
		for(ContractDTO file : fileList) {
			System.out.println("file : " + file);
		}
		for(ContractProductDTO product : productList) {
			System.out.println("product : " + product);
		}
		
		/* map에서 꺼낸 값들을 담는다 */
		mv.addObject("oneContract", oneContract);
		mv.addObject("approver", approver);
		mv.addObject("fileList", fileList);
		mv.addObject("productList", productList);
		mv.setViewName("approval/approvalRequest/contract/detail");
		
		return mv;
	}
	
	/* 계약 상세 조회 시 ajax를 통해 의견 조회 */
	@GetMapping(value="/contract/opinion/select", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<OpinionDTO> selectOpinionList(String contractNo, @AuthenticationPrincipal UserImpl customUser) {
		
		System.out.println("의견contractNo : " + contractNo);
		/* 상세조회할 견적번호를 service로 전달하고 의견리스트를 전달받음 */
		List<OpinionDTO> selectOpinionList = approvalContractService.selectOpinionList(contractNo);
		
		for(OpinionDTO opn : selectOpinionList) {
			System.out.println("opn : " + opn);
		}
		/* 의견삭제시 작성자와 로그인된사용자가 같은지 확인하기 위해서 사용자번호를 담음 */
		for(int i = 0; i < selectOpinionList.size(); i++) {

			selectOpinionList.get(i).setLoginUserNo(customUser.getUserNo());

		}

		/* 화면으로 전달 */
		return selectOpinionList;

	}
	
	/* ajax를 통해 의견 작성 */
	@PostMapping(value="/contract/opinion/insert", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int insertOpinion(String contractNo, String opinionContent, Principal pcp) {

		String id = pcp.getName();	//세션에 담겨있는 사용자 아이디

		/* 견적번호, 의견내용, 사용자아이디를 전달하고 결과를 전달 받음 */
		int result = approvalContractService.insertOpinion(contractNo, opinionContent, id);

		/* 화면으로 전달 */
		return result;

	}
	
	/* ajax를 통해 의견 삭제 */
	@PostMapping(value="/contract/opinion/delete", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int deleteOpinion(String opinionNo) {

		/* 의견번호를 전달하고 결과를 전달 받음 */
		int result = approvalContractService.deleteOpinion(opinionNo);		

		/* 화면으로 전달 */
		return result;

	}
	
	/* 계약 수정*/
	@GetMapping("/contract/update")
	public ModelAndView updateContract(ModelAndView mv, String contractNo) {
		
		System.out.println("updateContractNo : " + contractNo);
		
		mv.setViewName("contract/update");
		
		return mv;
	}
	
	/* 계약 삭제 */
	@GetMapping("/approvalRequest/contract/delete")
	public ModelAndView deleteContract(ModelAndView mv, @RequestParam String contractNo, RedirectAttributes rttr) {
		
		System.out.println("delContractNo : " + contractNo);
		
		/* 계약 번호를 받아서 해당 게시글을 삭제 한다 */
		int result = approvalContractService.deleteContract(contractNo);
		
		if(result > 0) {
			rttr.addFlashAttribute("message", "계약 삭제를 성공하였습니다.");
		} else {
			rttr.addFlashAttribute("message", "계약 삭제를 실패하였습니다.");
		}
		
		/* 삭제 후 연결할 페이지 */
		mv.setViewName("redirect:/approval/approvalRequest/contract/list");
		
		return mv;
	}
	
}
