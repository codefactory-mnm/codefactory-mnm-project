package com.codefactory.mnm.approval.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.security.Principal;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.codefactory.mnm.approval.model.dto.ApproverListDTO;
import com.codefactory.mnm.approval.model.dto.BusinessOpportunityListDTO;
import com.codefactory.mnm.approval.model.dto.ClientListDTO;
import com.codefactory.mnm.approval.model.dto.CompanyListDTO;
import com.codefactory.mnm.approval.model.dto.DeptListDTO;
import com.codefactory.mnm.approval.model.dto.EstimateAttachmentDTO;
import com.codefactory.mnm.approval.model.dto.EstimateListDTO;
import com.codefactory.mnm.approval.model.dto.EstimateSearchDTO;
import com.codefactory.mnm.approval.model.dto.ManagerListDTO;
import com.codefactory.mnm.approval.model.dto.OpinionDTO;
import com.codefactory.mnm.approval.model.dto.ProductListDTO;
import com.codefactory.mnm.approval.model.dto.UserListDTO;
import com.codefactory.mnm.approval.model.service.ApprovalService;
import com.codefactory.mnm.businessopportunity.model.dto.ApprovalDTO;
import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.member.model.dto.UserImpl;

@Controller
@RequestMapping("/approval")
public class ApprovalController {

	private final ApprovalService approvalService;

	@Autowired
	public ApprovalController(ApprovalService approvalService) {

		this.approvalService = approvalService;
	}

	/* 결재요청함 견적 목록 조회 (검색조건을 가져온다) */
	@GetMapping("/approvalRequest/estimate/list")
	public ModelAndView selectEstimate(ModelAndView mv,
			@RequestParam(defaultValue = "") String division,
			@RequestParam(defaultValue = "") String searchName,
			@RequestParam(defaultValue = "") String deptName,
			@RequestParam(defaultValue = "") String name,
			@RequestParam(defaultValue = "1") String currentPage,
			Principal pcp) {

		String id = pcp.getName();	//세션에 담겨있는 사용자 아이디

		System.out.println("division : " + division);
		System.out.println("searchName : " + searchName);
		System.out.println("deptName : " + deptName);
		System.out.println("name : " + name);
		System.out.println("currentPage : " + currentPage);

		/* 처음 견적 리스트를 조회하거나 또는 검색조건을 설정하지 않았을 때를 대비 */
		if(division.equals("")) { // 검색조건 중 구분을 선택하지 않았을 경우
			division = null;
		}

		if(searchName.equals("")) { // 검색조건 중 고객사를 입력하지 않았을 경우
			searchName = null;
		}

		if(deptName.equals("")) { // 검색조건 중 부서를 선택하지 않았을 경우
			deptName = null;
		}

		if(name.equals("")) { // 검색조건 중 이름을 선택하지 않았을 경우
			name = null;
		}

		/* 검색조건을 담는다. 검색조건이 없을 경우 null로 담김. */
		EstimateSearchDTO estimateSearchDTO = new EstimateSearchDTO();
		estimateSearchDTO.setDivision(division);
		estimateSearchDTO.setSearchName(searchName);
		estimateSearchDTO.setDeptName(deptName);
		estimateSearchDTO.setName(name);
		estimateSearchDTO.setCurrentPage(currentPage);
		estimateSearchDTO.setId(id);

		System.out.println("estimateSearchDTO : " + estimateSearchDTO);

		/* 검색조건을 service로 보내주고, 값을 map으로 전달받음 */
		Map<String, Object> map = approvalService.selectEstimateList(estimateSearchDTO);

		/* db에서 가져온 값을 꺼낸다. */
		List<EstimateListDTO> estimateList = (List<EstimateListDTO>) map.get("estimateList");
		List<DeptListDTO> deptList = (List<DeptListDTO>) map.get("deptList");
		SelectCriteria selectCriteria = (SelectCriteria) map.get("selectCriteria");
		int totalCount = (int) map.get("totalCount");
		System.out.println("estimateList : " + estimateList);
		System.out.println("deptList : " + deptList);

		/* db에서 조회해온 결과를 담는다 */
		mv.addObject("estimateList", estimateList);
		mv.addObject("deptList", deptList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("totalCount", totalCount);
		mv.addObject("estimateSearchDTO", estimateSearchDTO);
		mv.setViewName("approval/approvalRequest/estimate/list");

		return mv;
	}

	/* 결재대기함 견적 목록 조회 (검색조건을 가져온다) */
	@GetMapping("/approvalWait/estimate/list")
	public ModelAndView selectEstimateWait(ModelAndView mv,
			@RequestParam(defaultValue = "") String division,
			@RequestParam(defaultValue = "") String searchName,
			@RequestParam(defaultValue = "") String deptName,
			@RequestParam(defaultValue = "") String name,
			@RequestParam(defaultValue = "1") String currentPage,
			Principal pcp) {

		String id = pcp.getName();	//세션에 담겨있는 사용자 아이디

		System.out.println("division : " + division);
		System.out.println("searchName : " + searchName);
		System.out.println("deptName : " + deptName);
		System.out.println("name : " + name);
		System.out.println("currentPage : " + currentPage);

		/* 처음 견적 리스트를 조회하거나 또는 검색조건을 설정하지 않았을 때를 대비 */
		if(division.equals("")) { // 검색조건 중 구분을 선택하지 않았을 경우
			division = null;
		}

		if(searchName.equals("")) { // 검색조건 중 고객사를 입력하지 않았을 경우
			searchName = null;
		}

		if(deptName.equals("")) { // 검색조건 중 부서를 선택하지 않았을 경우
			deptName = null;
		}

		if(name.equals("")) { // 검색조건 중 이름을 선택하지 않았을 경우
			name = null;
		}

		/* 검색조건을 담는다. 검색조건이 없을 경우 null로 담김. */
		EstimateSearchDTO estimateSearchDTO = new EstimateSearchDTO();
		estimateSearchDTO.setDivision(division);
		estimateSearchDTO.setSearchName(searchName);
		estimateSearchDTO.setDeptName(deptName);
		estimateSearchDTO.setName(name);
		estimateSearchDTO.setCurrentPage(currentPage);
		estimateSearchDTO.setId(id);

		System.out.println("estimateSearchDTO : " + estimateSearchDTO);

		/* 검색조건을 searvice로 보내주고, 값을 map으로 전달받음 */
		Map<String, Object> map = approvalService.selectWaitEstimateList(estimateSearchDTO);

		/* db에서 가져온 값을 꺼낸다. */
		List<EstimateListDTO> estimateList = (List<EstimateListDTO>) map.get("estimateList");
		List<DeptListDTO> deptList = (List<DeptListDTO>) map.get("deptList");
		SelectCriteria selectCriteria = (SelectCriteria) map.get("selectCriteria");
		int totalCount = (int) map.get("totalCount");
		System.out.println("estimateList : " + estimateList);
		System.out.println("deptList : " + deptList);

		/* db에서 조회해온 결과를 담는다 */
		mv.addObject("estimateList", estimateList);
		mv.addObject("deptList", deptList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("totalCount", totalCount);
		mv.addObject("estimateSearchDTO", estimateSearchDTO);
		mv.setViewName("approval/approvalWait/estimate/list");

		return mv;
	}
	
	/* 결재완료함 견적 목록 조회 (검색조건을 가져온다) */
	@GetMapping("/approvalFinish/estimate/list")
	public ModelAndView selectEstimateFinish(ModelAndView mv,
			@RequestParam(defaultValue = "") String division,
			@RequestParam(defaultValue = "") String searchName,
			@RequestParam(defaultValue = "") String deptName,
			@RequestParam(defaultValue = "") String name,
			@RequestParam(defaultValue = "1") String currentPage,
			Principal pcp) {

		String id = pcp.getName();	//세션에 담겨있는 사용자 아이디

		System.out.println("division : " + division);
		System.out.println("searchName : " + searchName);
		System.out.println("deptName : " + deptName);
		System.out.println("name : " + name);
		System.out.println("currentPage : " + currentPage);

		/* 처음 견적 리스트를 조회하거나 또는 검색조건을 설정하지 않았을 때를 대비 */
		if(division.equals("")) { // 검색조건 중 구분을 선택하지 않았을 경우
			division = null;
		}

		if(searchName.equals("")) { // 검색조건 중 고객사를 입력하지 않았을 경우
			searchName = null;
		}

		if(deptName.equals("")) { // 검색조건 중 부서를 선택하지 않았을 경우
			deptName = null;
		}

		if(name.equals("")) { // 검색조건 중 이름을 선택하지 않았을 경우
			name = null;
		}

		/* 검색조건을 담는다. 검색조건이 없을 경우 null로 담김. */
		EstimateSearchDTO estimateSearchDTO = new EstimateSearchDTO();
		estimateSearchDTO.setDivision(division);
		estimateSearchDTO.setSearchName(searchName);
		estimateSearchDTO.setDeptName(deptName);
		estimateSearchDTO.setName(name);
		estimateSearchDTO.setCurrentPage(currentPage);
		estimateSearchDTO.setId(id);

		System.out.println("estimateSearchDTO : " + estimateSearchDTO);

		/* 검색조건을 searvice로 보내주고, 값을 map으로 전달받음 */
		Map<String, Object> map = approvalService.selectFinishEstimateList(estimateSearchDTO);

		/* db에서 가져온 값을 꺼낸다. */
		List<EstimateListDTO> estimateList = (List<EstimateListDTO>) map.get("estimateList");
		List<DeptListDTO> deptList = (List<DeptListDTO>) map.get("deptList");
		SelectCriteria selectCriteria = (SelectCriteria) map.get("selectCriteria");
		int totalCount = (int) map.get("totalCount");
		System.out.println("estimateList : " + estimateList);
		System.out.println("deptList : " + deptList);

		/* db에서 조회해온 결과를 담는다 */
		mv.addObject("estimateList", estimateList);
		mv.addObject("deptList", deptList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("totalCount", totalCount);
		mv.addObject("estimateSearchDTO", estimateSearchDTO);
		mv.setViewName("approval/approvalFinish/estimate/list");

		return mv;
	}

	/* 결재대기함 견적 수정 페이지로 이동 */
	@GetMapping("/approvalWait/estimate/update")
	public ModelAndView updateEstimatePage(ModelAndView mv, @RequestParam String estimateNo) {

		System.out.println("estimateNo : " + estimateNo);

		/* Map을 생성하여 서비스에서 값을 리턴 받는다*/
		Map<String, Object> map = approvalService.selectEstimate(estimateNo);
		
		/* map에 담은 값을 꺼낸다 */
		EstimateListDTO oneEstimate = (EstimateListDTO) map.get("oneEstimate");
		String approver = (String) map.get("approver");
		List<EstimateListDTO> fileList = (List<EstimateListDTO>) map.get("fileList");
		List<ProductListDTO> productList = (List<ProductListDTO>) map.get("productList");

		System.out.println("oneEstimate : " + oneEstimate);
		System.out.println("approver : " + approver);
		for(EstimateListDTO file : fileList) {
			System.out.println("file : " + file);
		}
		for(ProductListDTO product : productList) {
			System.out.println("product : " + product);
		}

		/* map에서 꺼낸 값들을 담는다 */
		mv.addObject("oneEstimate", oneEstimate);
		mv.addObject("approver", approver);
		mv.addObject("fileList", fileList);
		mv.addObject("productList", productList);
		mv.setViewName("approval/approvalWait/estimate/update");

		return mv;
	}
	
	@PostMapping("/approvalWait/estimate/update")
	public ModelAndView updateEstimateStatus(ModelAndView mv, ApprovalDTO approvalDTO, RedirectAttributes rttr) {
		
		int result = approvalService.updateEstimateStatus(approvalDTO);
		
		if(result > 0) {
			rttr.addFlashAttribute("message", "견적 수정을 성공하였습니다.");
		} else {
			rttr.addFlashAttribute("message", "견적 수정을 실패하였습니다.");
		}
		
		mv.setViewName("redirect:/approval/approvalWait/estimate/list");
		
		return mv;
	}

	/* 결재요청함 견적 상세조회 */
	@GetMapping("/approvalRequest/estimate/detail")
	public ModelAndView selectEstimate(ModelAndView mv, @RequestParam String estimateNo) {

		System.out.println("estimateNo : " + estimateNo);

		/* Map을 생성하여 서비스에서 값을 리턴 받는다*/
		Map<String, Object> map = approvalService.selectEstimate(estimateNo);

		/* map에 담은 값을 꺼낸다 */
		EstimateListDTO oneEstimate = (EstimateListDTO) map.get("oneEstimate");
		String approver = (String) map.get("approver");
		List<EstimateListDTO> fileList = (List<EstimateListDTO>) map.get("fileList");
		List<ProductListDTO> productList = (List<ProductListDTO>) map.get("productList");

		System.out.println("oneEstimate : " + oneEstimate);
		System.out.println("approver : " + approver);
		for(EstimateListDTO file : fileList) {
			System.out.println("file : " + file);
		}
		for(ProductListDTO product : productList) {
			System.out.println("product : " + product);
		}

		/* map에서 꺼낸 값들을 담는다 */
		mv.addObject("oneEstimate", oneEstimate);
		mv.addObject("approver", approver);
		mv.addObject("fileList", fileList);
		mv.addObject("productList", productList);
		mv.setViewName("approval/approvalRequest/estimate/detail");

		return mv;
	}
	
	/* 결재대기함 견적 상세조회 */
	@GetMapping("/approvalWait/estimate/detail")
	public ModelAndView selectEstimateWait(ModelAndView mv, @RequestParam String estimateNo) {

		System.out.println("estimateNo : " + estimateNo);

		/* Map을 생성하여 서비스에서 값을 리턴 받는다*/
		Map<String, Object> map = approvalService.selectEstimate(estimateNo);

		/* map에 담은 값을 꺼낸다 */
		EstimateListDTO oneEstimate = (EstimateListDTO) map.get("oneEstimate");
		String approver = (String) map.get("approver");
		List<EstimateListDTO> fileList = (List<EstimateListDTO>) map.get("fileList");
		List<ProductListDTO> productList = (List<ProductListDTO>) map.get("productList");

		System.out.println("oneEstimate : " + oneEstimate);
		System.out.println("approver : " + approver);
		for(EstimateListDTO file : fileList) {
			System.out.println("file : " + file);
		}
		for(ProductListDTO product : productList) {
			System.out.println("product : " + product);
		}

		/* map에서 꺼낸 값들을 담는다 */
		mv.addObject("oneEstimate", oneEstimate);
		mv.addObject("approver", approver);
		mv.addObject("fileList", fileList);
		mv.addObject("productList", productList);
		mv.setViewName("approval/approvalWait/estimate/detail");

		return mv;
	}

	/* 결재 요청함 견적 삭제 */
	@GetMapping("/approvalRequest/estimate/delete")
	public ModelAndView deleteEstimate(ModelAndView mv, @RequestParam String estimateNo, RedirectAttributes rttr) {

		System.out.println("delEstimateNo : " + estimateNo);

		/* 견적 번호를 받아서 해당 게시글을 삭제 한다 */
		int result = approvalService.deleteEstimate(estimateNo);

		if(result > 0) {
			rttr.addFlashAttribute("message", "견적 삭제를 성공하였습니다.");
		} else {
			rttr.addFlashAttribute("message", "견적 삭제를 실패하였습니다.");
		}

		/* 삭제 후 연결할 페이지 */
		mv.setViewName("redirect:/approval/approvalRequest/estimate/list");

		return mv;
	}

	/* 견적 등록시 고객사 모달창에 띄울 정보 가져오기 */
	@GetMapping(value="/estimate/company", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<CompanyListDTO> selectCompanyList(Model model) {

		List<CompanyListDTO> companyList = approvalService.selectCompanyList();

		model.addAttribute("companyList", companyList);

		System.out.println("companyList : " + companyList);

		return companyList;
	}

	/* 견적 등록시 고객 모달창에 띄울 정보 가져오기 */
	@GetMapping(value="/estimate/client", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<ClientListDTO> selectClientList(Model model) {

		List<ClientListDTO> clientList = approvalService.selectClientList();

		model.addAttribute("clientList", clientList);

		System.out.println("clientList : " + clientList);

		return clientList;
	}

	/* 견적 등록시 영업기회 모달창에 띄울 정보 가져오기 */
	@GetMapping(value="/estimate/businessOpportunity", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<BusinessOpportunityListDTO> selectBusinessOpportunityList(Model model) {

		List<BusinessOpportunityListDTO> businessOpportunityList = approvalService.selectBusinessOpportunityList();

		model.addAttribute("businessOpportunityList", businessOpportunityList);

		System.out.println("businessOpportunityList : " + businessOpportunityList);

		return businessOpportunityList;
	}

	/* 견적 등록시 담당자 모달창에 띄울 정보 가져오기 */
	@GetMapping(value="/estimate/user", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<UserListDTO> selectUserList(Model model) {

		List<UserListDTO> userList = approvalService.selectUserList();

		model.addAttribute("userList", userList);

		System.out.println("userList : " + userList);

		return userList;
	}

	/* 견적 등록시 견적 품목 모달창에 띄울 정보 가져오기 */
	@GetMapping(value="/estimate/product", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<ProductListDTO> selectProductList(Model model) {

		List<ProductListDTO> productList = approvalService.selectProductList();

		model.addAttribute("productList", productList);

		System.out.println("productList : " + productList);

		return productList;
	}

	/* 견적 등록시 결재권자 모달창에 띄울 정보 가져오기 */
	@GetMapping(value="/estimate/approver", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<ApproverListDTO> selectApproverList(Model model) {

		List<ApproverListDTO> approverList = approvalService.selectApproverList();

		model.addAttribute("approverList", approverList);

		System.out.println("approverList : " + approverList);

		return approverList;
	}

	/* 견적 목록에서 담당자 모달창에 띄울 정보 가져오기 */
	@GetMapping(value="/estimate/manager", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<ManagerListDTO> selectManagerList(Model model, @RequestParam String deptName) {

		System.out.println("deptName : " + deptName);

		List<ManagerListDTO> managerList = approvalService.selectManagerList(deptName);

		model.addAttribute("managerList", managerList);

		System.out.println("managerList : " + managerList);

		return managerList;
	}

	/* 첨부파일 다운로드 */
	@GetMapping("/estimate/download")
	@ResponseBody
	public void download(HttpServletResponse response, String fileNo) throws IOException {

		System.out.println("fileNo : " + fileNo);

		/* 전달받은 fileNo로 파일 정보 조회 */
		EstimateAttachmentDTO estimateAttachmentDTO = approvalService.selectFile(fileNo);
		String fileOriginalName = estimateAttachmentDTO.getFileOriginalName();
		String fileName = estimateAttachmentDTO.getFileName();
		String filePath = estimateAttachmentDTO.getFilePath();

		String saveFileName = filePath + fileName;

		File file = new File(saveFileName);
		long fileLength = file.length();

		/* 파일 정보들을 가지고 reponse의 Header에 세팅 */
		response.setHeader("Content-Disposition", "attachment; filename=\"" + URLEncoder.encode(fileOriginalName,"UTF-8") + "\";");
		response.setHeader("Content-Transfer-Encoding", "binary");
		response.setHeader("Content-Type", "application/octet-stream");
		response.setHeader("Content-Length", "" + fileLength);
		response.setHeader("Pragma", "no-cache;");
		response.setHeader("Expires", "-1;");

		/* saveFileName을 파라미터로 넣어 inputStream 객체를 만들고 
        response에서 파일을 내보낼 OutputStream을 가져와서 */
		FileInputStream fis = new FileInputStream(saveFileName);
		OutputStream out = response.getOutputStream();

		/* 파일 읽을 만큼 크기의 buffer를 생성한 후 */
		int readCount = 0;
		byte[] buffer = new byte[1024];

		/* outputStream에 씌워준다 */
		while((readCount = fis.read(buffer)) != -1){
			out.write(buffer,0,readCount);
		}
		/* 스트림 close */
		fis.close();
		out.close();

	}

	/* 견적 상세 조회 시 ajax를 통해 의견 조회 */
	@GetMapping(value="/estimate/opinion/select", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<OpinionDTO> selectOpinionList(String estimateNo, @AuthenticationPrincipal UserImpl customUser) {

		System.out.println("estimateNo22 : " + estimateNo);
		/* 상세조회할 견적번호를 service로 전달하고 의견리스트를 전달받음 */
		List<OpinionDTO> selectOpinionList = approvalService.selectOpinionList(estimateNo);

		for(OpinionDTO opn : selectOpinionList) {
			System.out.println("opn : " + opn);
		}
		/* 의견삭제시 작성자와 로그인된사용자가 같은지 확인하기 위해서 사용자번호를 담음 */
		for(int i = 0; i < selectOpinionList.size(); i++) {

			selectOpinionList.get(i).setLoginUserNo(customUser.getUserNo());

		}

		/* 화면으로 전달 */
		return selectOpinionList;

	}

	/* ajax를 통해 의견 작성 */
	@PostMapping(value="/estimate/opinion/insert", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int insertOpinion(String estimateNo, String opinionContent, Principal pcp) {

		String id = pcp.getName();	//세션에 담겨있는 사용자 아이디

		/* 견적번호, 의견내용, 사용자아이디를 전달하고 결과를 전달 받음 */
		int result = approvalService.insertOpinion(estimateNo, opinionContent, id);

		/* 화면으로 전달 */
		return result;

	}

	/* ajax를 통해 의견 삭제 */
	@PostMapping(value="/estimate/opinion/delete", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int deleteOpinion(String opinionNo) {

		/* 의견번호를 전달하고 결과를 전달 받음 */
		int result = approvalService.deleteOpinion(opinionNo);		

		/* 화면으로 전달 */
		return result;

	}

}
