package com.codefactory.mnm.potentialclient.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.estimate.model.dto.DeptListDTO;
import com.codefactory.mnm.potentialclient.model.dto.ContactStateHistoryDTO;
import com.codefactory.mnm.potentialclient.model.dto.PotentialClientDTO;
import com.codefactory.mnm.potentialclient.model.dto.PotentialClientHistoryDTO;
import com.codefactory.mnm.potentialclient.model.dto.PotentialClientInsertDTO;
import com.codefactory.mnm.potentialclient.model.dto.PotentialClientSearchDTO;
import com.codefactory.mnm.potentialclient.model.service.PotentialClientService;

@Controller
@RequestMapping("/business")
public class PotentialclientController {

	private final PotentialClientService potentialClientService;
	
	@Autowired
	public PotentialclientController(PotentialClientService potentialClientService) {
		
		this.potentialClientService = potentialClientService;
	}
	
	/* 잠재고객 등록 페이지로 이동 */
	@GetMapping("/potentialclient/insert")
	public String potentialclientInsertPage() {
		
		return "potentialclient/insert";
	}
	
	/* 잠재고객 등록 */
	@PostMapping("/potentialclient/insert")
	public ModelAndView insertPotentialclient(ModelAndView mv, PotentialClientDTO potentialClientDTO, ContactStateHistoryDTO contactStateHistoryDTO
			, java.sql.Date[] contactDate, String[] contactDivision2, String[] contactContent, RedirectAttributes rttr) {
		
		/* 잠재고객 이력 리스트 생성 */
		List<PotentialClientHistoryDTO> potentialClientHistoryList = new ArrayList<>();
		
		if(contactDivision2 != null) {
			
			for(int i = 0; i < contactDivision2.length; i++) {
				
				/* 잠재고객 이력 DTO에 전달받은 값을 넣고 DTO를 리스트에 담는다 */
				PotentialClientHistoryDTO potentialClientHistoryDTO = new PotentialClientHistoryDTO();
				potentialClientHistoryDTO.setContactDate(contactDate[i]);
				potentialClientHistoryDTO.setContactDivision2(contactDivision2[i]);
				potentialClientHistoryDTO.setContactContent(contactContent[i]);
				
				potentialClientHistoryList.add(potentialClientHistoryDTO);
			}
		}
		
		/* potentialClientInsertDTO에 잠재고객 내용과 이력정보를 담아서 서비스로 전달 */
		PotentialClientInsertDTO potentialClientInsertDTO = new PotentialClientInsertDTO();
		potentialClientInsertDTO.setPotentialClientDTO(potentialClientDTO);
		potentialClientInsertDTO.setContactStateHistoryDTO(contactStateHistoryDTO);
		potentialClientInsertDTO.setPotentialClientHistoryList(potentialClientHistoryList);
		
		int result = potentialClientService.insertPotentialClient(potentialClientInsertDTO);
		
		if(result > 0) {
			rttr.addFlashAttribute("message", "잠재고객 등록을 성공하였습니다.");
		} else {
			rttr.addFlashAttribute("message", "잠재고객 등록을 실패하였습니다.");
		}
		
		mv.setViewName("redirect:/business/potentialclient/list");
		
		return mv;
	}
	
	/* 잠재고객 목록 조회 */
	@GetMapping("/potentialclient/list")
	public ModelAndView selectPotentialclient(ModelAndView mv,
			@RequestParam(defaultValue = "") String division,
			@RequestParam(defaultValue = "") String searchName,
			@RequestParam(defaultValue = "") String deptName,
			@RequestParam(defaultValue = "") String name,
			@RequestParam(defaultValue = "1") String currentPage) {
		
		/* 처음 잠재고객 리스트를 조회하거나 또는 검색조건을 설정하지 않았을 때를 대비 */
		if(division.equals("")) { // 검색조건 중 구분을 선택하지 않았을 경우
			division = null;
		}
		
		if(searchName.equals("")) { // 검색조건 중 검색어를 입력하지 않았을 경우
			searchName = null;
		}
		
		if(deptName.equals("")) { // 검색조건 중 부서를 선택하지 않았을 경우
			deptName = null;
		}
		
		if(name.equals("")) { // 검색조건 중 이름을 선택하지 않았을 경우
			name = null;
		}
		
		/* 검색조건을 담는다. 검색조건이 없을 경우 null로 담김. */
		PotentialClientSearchDTO potentialClientSearchDTO = new PotentialClientSearchDTO();
		potentialClientSearchDTO.setDivision(division);
		potentialClientSearchDTO.setSearchName(searchName);
		potentialClientSearchDTO.setDeptName(deptName);
		potentialClientSearchDTO.setName(name);
		potentialClientSearchDTO.setCurrentPage(currentPage);
		
		/* 검색조건을 searvice로 보내주고, 값을 map으로 전달받음 */
		Map<String, Object> map = potentialClientService.selectPotentialClientList(potentialClientSearchDTO);
		
		/* db에서 가져온 값을 꺼낸다. */
		List<PotentialClientDTO> potentialClientList = (List<PotentialClientDTO>) map.get("potentialClientList");
		List<DeptListDTO> deptList = (List<DeptListDTO>) map.get("deptList");
		SelectCriteria selectCriteria = (SelectCriteria) map.get("selectCriteria");
		int totalCount = (int) map.get("totalCount");
		
		/* db에서 조회해온 결과를 담는다 */
		mv.addObject("potentialClientList", potentialClientList);
		mv.addObject("deptList", deptList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("totalCount", totalCount);
		mv.addObject("potentialClientSearchDTO", potentialClientSearchDTO);
		mv.setViewName("potentialclient/list");
		
		return mv;
	}
	
	/* 잠재고객 상세조회 */
	@GetMapping("/potentialclient/detail")
	public ModelAndView selectPotentialClientDetail(ModelAndView mv, String potentialClientNo, String clientCompany) {
		
		PotentialClientDTO potentialClientDTO = new PotentialClientDTO();
		potentialClientDTO.setPotentialClientNo(potentialClientNo);
		potentialClientDTO.setClientCompany(clientCompany);
		
		/* Map을 생성하여 서비스에서 값을 리턴 받는다*/
		Map<String, Object> map = potentialClientService.selectPotentialClient(potentialClientDTO);
		
		/* map에 담은 값을 꺼낸다 */
		PotentialClientDTO onePotentialClient = (PotentialClientDTO) map.get("onePotentialClient");
		List<PotentialClientHistoryDTO> historyList = (List<PotentialClientHistoryDTO>) map.get("historyList");
		List<PotentialClientDTO> sameCompanyList = (List<PotentialClientDTO>) map.get("sameCompanyList");
		
		/* map에서 꺼낸 값들을 담는다 */
		mv.addObject("onePotentialClient", onePotentialClient);
		mv.addObject("historyList", historyList);
		mv.addObject("sameCompanyList", sameCompanyList);
		mv.setViewName("potentialclient/detail");
		
		return mv;
	}
	
	/* 잠재고객 삭제 */
	@GetMapping("/potentialclient/delete")
	public ModelAndView deletePotentialclient(ModelAndView mv, String potentialClientNo, RedirectAttributes rttr) {
		
		/* 계약 번호를 받아서 해당 게시글을 삭제 한다 */
		int result = potentialClientService.deletePotentialClient(potentialClientNo);
		
		if(result > 0) {
			rttr.addFlashAttribute("message", "잠재고객 삭제를 성공하였습니다.");
		} else {
			rttr.addFlashAttribute("message", "잠재고객 삭제를 실패하였습니다.");
		}
		
		/* 삭제 후 연결할 페이지 */
		mv.setViewName("redirect:/business/potentialclient/list");
		
		return mv;
	}
	
	/* 잠재고객 수정 페이지로 이동 */
	@GetMapping("/potentialclient/update")
	public ModelAndView PotentialClientUpdatePage(ModelAndView mv, String potentialClientNo, String clientCompany) {
		
		PotentialClientDTO potentialClientDTO = new PotentialClientDTO();
		potentialClientDTO.setPotentialClientNo(potentialClientNo);
		potentialClientDTO.setClientCompany(clientCompany);
		
		/* Map을 생성하여 서비스에서 값을 리턴 받는다*/
		Map<String, Object> map = potentialClientService.selectPotentialClient(potentialClientDTO);
		
		/* map에 담은 값을 꺼낸다 */
		PotentialClientDTO onePotentialClient = (PotentialClientDTO) map.get("onePotentialClient");
		List<PotentialClientHistoryDTO> historyList = (List<PotentialClientHistoryDTO>) map.get("historyList");
		List<PotentialClientDTO> sameCompanyList = (List<PotentialClientDTO>) map.get("sameCompanyList");
		
		/* map에서 꺼낸 값들을 담는다 */
		mv.addObject("onePotentialClient", onePotentialClient);
		mv.addObject("historyList", historyList);
		mv.addObject("sameCompanyList", sameCompanyList);
		mv.setViewName("potentialclient/update");
		
		return mv;
	}
	
	/* 잠재고객 수정 */
	@PostMapping("/potentialclient/update")
	public ModelAndView updatetPotentialclient(ModelAndView mv, PotentialClientDTO potentialClientDTO, ContactStateHistoryDTO contactStateHistoryDTO
			, java.sql.Date[] contactDate, String[] contactDivision2, String[] contactContent, RedirectAttributes rttr) {
		
		/* 잠재고객 이력 리스트 생성 */
		List<PotentialClientHistoryDTO> potentialClientHistoryList = new ArrayList<>();
		
		for(int i = 0; i < contactDivision2.length; i++) {
			
			/* 잠재고객 이력 DTO에 전달받은 값을 넣고 DTO를 리스트에 담는다 */
			PotentialClientHistoryDTO potentialClientHistoryDTO = new PotentialClientHistoryDTO();
			potentialClientHistoryDTO.setContactDate(contactDate[i]);
			potentialClientHistoryDTO.setContactDivision2(contactDivision2[i]);
			potentialClientHistoryDTO.setContactContent(contactContent[i]);
			
			potentialClientHistoryList.add(potentialClientHistoryDTO);
		}
		
		/* potentialClientInsertDTO에 잠재고객 내용과 이력정보를 담아서 서비스로 전달 */
		PotentialClientInsertDTO potentialClientInsertDTO = new PotentialClientInsertDTO();
		potentialClientInsertDTO.setPotentialClientDTO(potentialClientDTO);
		potentialClientInsertDTO.setContactStateHistoryDTO(contactStateHistoryDTO);
		potentialClientInsertDTO.setPotentialClientHistoryList(potentialClientHistoryList);
		
		int result = potentialClientService.updatePotentialClient(potentialClientInsertDTO);
		
		if(result > 0) {
			rttr.addFlashAttribute("message", "잠재고객 수정을 성공하였습니다.");
		} else {
			rttr.addFlashAttribute("message", "잠재고객 수정을 실패하였습니다.");
		}
		
		mv.setViewName("redirect:/business/potentialclient/list");
		
		return mv;
	}
}
