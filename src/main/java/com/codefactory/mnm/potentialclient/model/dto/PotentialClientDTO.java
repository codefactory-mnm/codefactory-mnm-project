package com.codefactory.mnm.potentialclient.model.dto;

import java.sql.Date;

public class PotentialClientDTO implements java.io.Serializable {

	private String potentialClientNo;					//잠재고객번호
	private String potentialClientName;					//이름
	private String clientCompany;						//고객사
	private String department;							//부서
	private String position;							//직책
	private String contactDivision;						//접촉구분
	private String contactState;						//접촉상태
	private String phone;								//휴대전화
	private String landlineTel;							//유선번호
	private String mail;								//메일
	private String fax;									//팩스
	private String zipCode;								//우편번호
	private String address;								//주소
	private String detailedAddress;						//상세주소
	private String note;								//비고
	private String userNo;								//담당자번호
	private java.sql.Date writeDate;					//작성일자
	private String grade;								//등급
	private String delYn;								//삭제여부
	private String name;								//담당자이름
	
	public PotentialClientDTO() {}

	public PotentialClientDTO(String potentialClientNo, String potentialClientName, String clientCompany,
			String department, String position, String contactDivision, String contactState, String phone,
			String landlineTel, String mail, String fax, String zipCode, String address, String detailedAddress,
			String note, String userNo, Date writeDate, String grade, String delYn, String name) {
		super();
		this.potentialClientNo = potentialClientNo;
		this.potentialClientName = potentialClientName;
		this.clientCompany = clientCompany;
		this.department = department;
		this.position = position;
		this.contactDivision = contactDivision;
		this.contactState = contactState;
		this.phone = phone;
		this.landlineTel = landlineTel;
		this.mail = mail;
		this.fax = fax;
		this.zipCode = zipCode;
		this.address = address;
		this.detailedAddress = detailedAddress;
		this.note = note;
		this.userNo = userNo;
		this.writeDate = writeDate;
		this.grade = grade;
		this.delYn = delYn;
		this.name = name;
	}

	public String getPotentialClientNo() {
		return potentialClientNo;
	}

	public String getPotentialClientName() {
		return potentialClientName;
	}

	public String getClientCompany() {
		return clientCompany;
	}

	public String getDepartment() {
		return department;
	}

	public String getPosition() {
		return position;
	}

	public String getContactDivision() {
		return contactDivision;
	}

	public String getContactState() {
		return contactState;
	}

	public String getPhone() {
		return phone;
	}

	public String getLandlineTel() {
		return landlineTel;
	}

	public String getMail() {
		return mail;
	}

	public String getFax() {
		return fax;
	}

	public String getZipCode() {
		return zipCode;
	}

	public String getAddress() {
		return address;
	}

	public String getDetailedAddress() {
		return detailedAddress;
	}

	public String getNote() {
		return note;
	}

	public String getUserNo() {
		return userNo;
	}

	public java.sql.Date getWriteDate() {
		return writeDate;
	}

	public String getGrade() {
		return grade;
	}

	public String getDelYn() {
		return delYn;
	}

	public String getName() {
		return name;
	}

	public void setPotentialClientNo(String potentialClientNo) {
		this.potentialClientNo = potentialClientNo;
	}

	public void setPotentialClientName(String potentialClientName) {
		this.potentialClientName = potentialClientName;
	}

	public void setClientCompany(String clientCompany) {
		this.clientCompany = clientCompany;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public void setContactDivision(String contactDivision) {
		this.contactDivision = contactDivision;
	}

	public void setContactState(String contactState) {
		this.contactState = contactState;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setLandlineTel(String landlineTel) {
		this.landlineTel = landlineTel;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setDetailedAddress(String detailedAddress) {
		this.detailedAddress = detailedAddress;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "PotentialClientDTO [potentialClientNo=" + potentialClientNo + ", potentialClientName="
				+ potentialClientName + ", clientCompany=" + clientCompany + ", department=" + department
				+ ", position=" + position + ", contactDivision=" + contactDivision + ", contactState=" + contactState
				+ ", phone=" + phone + ", landlineTel=" + landlineTel + ", mail=" + mail + ", fax=" + fax + ", zipCode="
				+ zipCode + ", address=" + address + ", detailedAddress=" + detailedAddress + ", note=" + note
				+ ", userNo=" + userNo + ", writeDate=" + writeDate + ", grade=" + grade + ", delYn=" + delYn
				+ ", name=" + name + "]";
	}
	
	
}
