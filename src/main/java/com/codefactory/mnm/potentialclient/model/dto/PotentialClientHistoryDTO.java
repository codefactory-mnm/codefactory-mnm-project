package com.codefactory.mnm.potentialclient.model.dto;

import java.sql.Date;

public class PotentialClientHistoryDTO implements java.io.Serializable {

	private String potentialClientHisNo;		//잠재고객이력번호
	private java.sql.Date contactDate;			//접촉일
	private String contactDivision2;				//접촉구분
	private String contactContent;				//접촉내용
	private String potentialClientNo;			//잠재고객번호
	
	public PotentialClientHistoryDTO() {}

	public PotentialClientHistoryDTO(String potentialClientHisNo, Date contactDate, String contactDivision2,
			String contactContent, String potentialClientNo) {
		super();
		this.potentialClientHisNo = potentialClientHisNo;
		this.contactDate = contactDate;
		this.contactDivision2 = contactDivision2;
		this.contactContent = contactContent;
		this.potentialClientNo = potentialClientNo;
	}

	public String getPotentialClientHisNo() {
		return potentialClientHisNo;
	}

	public java.sql.Date getContactDate() {
		return contactDate;
	}

	public String getContactDivision2() {
		return contactDivision2;
	}

	public String getContactContent() {
		return contactContent;
	}

	public String getPotentialClientNo() {
		return potentialClientNo;
	}

	public void setPotentialClientHisNo(String potentialClientHisNo) {
		this.potentialClientHisNo = potentialClientHisNo;
	}

	public void setContactDate(java.sql.Date contactDate) {
		this.contactDate = contactDate;
	}

	public void setContactDivision2(String contactDivision2) {
		this.contactDivision2 = contactDivision2;
	}

	public void setContactContent(String contactContent) {
		this.contactContent = contactContent;
	}

	public void setPotentialClientNo(String potentialClientNo) {
		this.potentialClientNo = potentialClientNo;
	}

	@Override
	public String toString() {
		return "PotentialClientHistoryDTO [potentialClientHisNo=" + potentialClientHisNo + ", contactDate="
				+ contactDate + ", contactDivision2=" + contactDivision2 + ", contactContent=" + contactContent
				+ ", potentialClientNo=" + potentialClientNo + "]";
	}
	
	
}
