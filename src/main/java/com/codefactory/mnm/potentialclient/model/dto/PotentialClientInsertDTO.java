package com.codefactory.mnm.potentialclient.model.dto;

import java.util.List;

public class PotentialClientInsertDTO implements java.io.Serializable {

	private PotentialClientDTO potentialClientDTO;						//잠재고객 DTO
	private ContactStateHistoryDTO contactStateHistoryDTO;				//접촉상태 변경이력 DTO
	private PotentialClientHistoryDTO PotentialClientHistoryDTO;		//잠재고객 이력 DTO
	List<PotentialClientHistoryDTO> potentialClientHistoryList;			//잠재고객 이력 리스트
	
	public PotentialClientInsertDTO() {}

	public PotentialClientInsertDTO(PotentialClientDTO potentialClientDTO,
			ContactStateHistoryDTO contactStateHistoryDTO,
			com.codefactory.mnm.potentialclient.model.dto.PotentialClientHistoryDTO potentialClientHistoryDTO,
			List<com.codefactory.mnm.potentialclient.model.dto.PotentialClientHistoryDTO> potentialClientHistoryList) {
		super();
		this.potentialClientDTO = potentialClientDTO;
		this.contactStateHistoryDTO = contactStateHistoryDTO;
		PotentialClientHistoryDTO = potentialClientHistoryDTO;
		this.potentialClientHistoryList = potentialClientHistoryList;
	}

	public PotentialClientDTO getPotentialClientDTO() {
		return potentialClientDTO;
	}

	public ContactStateHistoryDTO getContactStateHistoryDTO() {
		return contactStateHistoryDTO;
	}

	public PotentialClientHistoryDTO getPotentialClientHistoryDTO() {
		return PotentialClientHistoryDTO;
	}

	public List<PotentialClientHistoryDTO> getPotentialClientHistoryList() {
		return potentialClientHistoryList;
	}

	public void setPotentialClientDTO(PotentialClientDTO potentialClientDTO) {
		this.potentialClientDTO = potentialClientDTO;
	}

	public void setContactStateHistoryDTO(ContactStateHistoryDTO contactStateHistoryDTO) {
		this.contactStateHistoryDTO = contactStateHistoryDTO;
	}

	public void setPotentialClientHistoryDTO(PotentialClientHistoryDTO potentialClientHistoryDTO) {
		PotentialClientHistoryDTO = potentialClientHistoryDTO;
	}

	public void setPotentialClientHistoryList(List<PotentialClientHistoryDTO> potentialClientHistoryList) {
		this.potentialClientHistoryList = potentialClientHistoryList;
	}

	@Override
	public String toString() {
		return "PotentialClientInsertDTO [potentialClientDTO=" + potentialClientDTO + ", contactStateHistoryDTO="
				+ contactStateHistoryDTO + ", PotentialClientHistoryDTO=" + PotentialClientHistoryDTO
				+ ", potentialClientHistoryList=" + potentialClientHistoryList + "]";
	}

	
	
	
}
