package com.codefactory.mnm.potentialclient.model.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.codefactory.mnm.common.Pagenation;
import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.contract.model.dto.ContractDTO;
import com.codefactory.mnm.contract.model.dto.ContractProductDTO;
import com.codefactory.mnm.estimate.model.dto.DeptListDTO;
import com.codefactory.mnm.potentialclient.model.dao.PotentialClientDAO;
import com.codefactory.mnm.potentialclient.model.dto.ContactStateHistoryDTO;
import com.codefactory.mnm.potentialclient.model.dto.PotentialClientDTO;
import com.codefactory.mnm.potentialclient.model.dto.PotentialClientHistoryDTO;
import com.codefactory.mnm.potentialclient.model.dto.PotentialClientInsertDTO;
import com.codefactory.mnm.potentialclient.model.dto.PotentialClientSearchDTO;

@Service
public class PotentialClientServiceImpl implements PotentialClientService {

	private final PotentialClientDAO potentialclientDAO;
	
	@Autowired
	public PotentialClientServiceImpl(PotentialClientDAO potentialclientDAO) {
		this.potentialclientDAO = potentialclientDAO;
	}
	
	/* 잠재고객 등록 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertPotentialClient(PotentialClientInsertDTO potentialClientInsertDTO) {

		PotentialClientDTO potentialClientDTO = potentialClientInsertDTO.getPotentialClientDTO();
		List<PotentialClientHistoryDTO> potentialClientHistoryList = potentialClientInsertDTO.getPotentialClientHistoryList();
		ContactStateHistoryDTO contactStateHistoryDTO = potentialClientInsertDTO.getContactStateHistoryDTO();
		
		/* 잠재고객 등록 */
		int result1 =  potentialclientDAO.insertPotentialClient(potentialClientDTO);
		
		/* 잠재고객 이력 등록 */
		int result2 = 0;
		if(potentialClientHistoryList.size() > 0) {
			for(int i= 0; i < potentialClientHistoryList.size(); i++) {
				PotentialClientHistoryDTO potentialClientHistoryDTO = potentialClientHistoryList.get(i);
				result2 = potentialclientDAO.insertPotentialClientHistory(potentialClientHistoryDTO);
			}
		}
		
		
		/* 잠재고객 접촉상태 변경 이력 등록 */
		int result3 = potentialclientDAO.insertContactStateHistory(contactStateHistoryDTO);
		
		int result = 0;
		
		if(potentialClientHistoryList.size() > 0) {
			if(result1 > 0 && result2 > 0 && result3 > 0) {
				result = 1;
			}
		} else {
			if(result1 > 0 && result3 > 0) {
				result = 1;
			}
		}
		
		
		return result;
	}

	/* 잠재고객 목록 조회 */
	@Override
	public Map<String, Object> selectPotentialClientList(PotentialClientSearchDTO potentialClientSearchDTO) {

		/* 첫 요청페이지 */
		int pageNo = 1;		
		
		/* 요청 페이지가 0이하면 1로 바꿔줌 */
		if(pageNo <= 0) {
			pageNo = 1;
		}
		
		/* 최대 페이지 */
		int limit = 10;
		
		/* 최대 버튼 갯수 */
		int buttonAmount = 5;
		
		/* 전체 게시물 수, 검색조건이 있다면 검색조건에 맞는 게시물 수 */
		int totalCount = potentialclientDAO.selectTotalCount(potentialClientSearchDTO);

		/* view에서 전달받은 요청페이지 */
		String currentPage = potentialClientSearchDTO.getCurrentPage();
		
		/* view에서 전달받은 요청페이지를 pageNo에 저장 */
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		/* 설정한 변수에 따른 페이징조건 */
		SelectCriteria selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount);
				
		/* 조회 결과를 담을 Map 생성 */
		Map<String, Object> map = new HashMap<>();
		
		potentialClientSearchDTO.setSelectCriteria(selectCriteria);
		
		/* 견적 조회 */
		List<ContractDTO> potentialClientList = potentialclientDAO.selectPotentialClientList(potentialClientSearchDTO);
		
		/* 부서 조회 */
		List<DeptListDTO> deptList = potentialclientDAO.selectDeptList();
		
		map.put("totalCount", totalCount);
		map.put("selectCriteria", selectCriteria);
		map.put("potentialClientList", potentialClientList);
		map.put("deptList", deptList);
		
		return map;
	}

	/* 잠재고객 상세조회 */
	@Override
	public Map<String, Object> selectPotentialClient(PotentialClientDTO potentialClientDTO) {

		String potentialclientNo = potentialClientDTO.getPotentialClientNo();
		
		Map<String, Object> map = new HashMap<>();
		
		PotentialClientDTO onePotentialClient = potentialclientDAO.selectPotentialClient(potentialclientNo);
		List<PotentialClientHistoryDTO> historyList = potentialclientDAO.selectPotentialClientHistoryList(potentialclientNo);
		List<PotentialClientDTO> sameCompanyList = potentialclientDAO.selectSameCompanyList(potentialClientDTO);
		
		map.put("onePotentialClient", onePotentialClient);
		map.put("historyList", historyList);
		map.put("sameCompanyList", sameCompanyList);
		
		return map;
	}

	/* 잠재고객 삭제 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int deletePotentialClient(String potentialClientNo) {

		return potentialclientDAO.deletePotentialClient(potentialClientNo);
	}

	/* 잠재고객 수정 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int updatePotentialClient(PotentialClientInsertDTO potentialClientInsertDTO) {

		PotentialClientDTO potentialClientDTO = potentialClientInsertDTO.getPotentialClientDTO();
		List<PotentialClientHistoryDTO> potentialClientHistoryList = potentialClientInsertDTO.getPotentialClientHistoryList();
		ContactStateHistoryDTO contactStateHistoryDTO = potentialClientInsertDTO.getContactStateHistoryDTO();
		String potentialClientNo = potentialClientDTO.getPotentialClientNo();
		
		/*잠재고객 이력 삭제 */
		int result1 = potentialclientDAO.deletePotentialClientHistory(potentialClientNo);
		
		/* 잠재고객 등록 */
		int result2 =  potentialclientDAO.updatePotentialClient(potentialClientDTO);
		
		/* 잠재고객 이력 등록 */
		int result3 = 0;
		for(int i= 0; i < potentialClientHistoryList.size(); i++) {
			PotentialClientHistoryDTO potentialClientHistoryDTO = potentialClientHistoryList.get(i);
			potentialClientHistoryDTO.setPotentialClientNo(potentialClientNo);
			result3 = potentialclientDAO.updatePotentialClientHistory(potentialClientHistoryDTO);
		}
		
		/* 잠재고객 접촉상태 변경 이력 등록 */
		int result4 = potentialclientDAO.updateContactStateHistory(contactStateHistoryDTO);
		
		int result = 0;
		
		if(result1 > 0 && result2 > 0 && result3 > 0 && result4 > 0) {
			result = 1;
		}
		
		return result;
	}

}
