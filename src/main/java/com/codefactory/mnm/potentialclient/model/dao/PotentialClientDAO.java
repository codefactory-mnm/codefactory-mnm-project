package com.codefactory.mnm.potentialclient.model.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.codefactory.mnm.contract.model.dto.ContractDTO;
import com.codefactory.mnm.estimate.model.dto.DeptListDTO;
import com.codefactory.mnm.potentialclient.model.dto.ContactStateHistoryDTO;
import com.codefactory.mnm.potentialclient.model.dto.PotentialClientDTO;
import com.codefactory.mnm.potentialclient.model.dto.PotentialClientHistoryDTO;
import com.codefactory.mnm.potentialclient.model.dto.PotentialClientSearchDTO;

@Mapper
public interface PotentialClientDAO {

	int insertPotentialClient(PotentialClientDTO potentialClientDTO);

	int insertPotentialClientHistory(PotentialClientHistoryDTO potentialClientHistoryDTO);

	int insertContactStateHistory(ContactStateHistoryDTO contactStateHistoryDTO);

	int selectTotalCount(PotentialClientSearchDTO potentialClientSearchDTO);

	List<ContractDTO> selectPotentialClientList(PotentialClientSearchDTO potentialClientSearchDTO);

	List<DeptListDTO> selectDeptList();

	PotentialClientDTO selectPotentialClient(String potentialclientNo);

	List<PotentialClientHistoryDTO> selectPotentialClientHistoryList(String potentialclientNo);

	List<PotentialClientDTO> selectSameCompanyList(PotentialClientDTO potentialClientDTO);

	int deletePotentialClient(String potentialClientNo);

	int deletePotentialClientHistory(String potentialClientNo);

	int updatePotentialClient(PotentialClientDTO potentialClientDTO);

	int updatePotentialClientHistory(PotentialClientHistoryDTO potentialClientHistoryDTO);

	int updateContactStateHistory(ContactStateHistoryDTO contactStateHistoryDTO);

}
