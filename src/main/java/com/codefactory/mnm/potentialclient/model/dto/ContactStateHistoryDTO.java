package com.codefactory.mnm.potentialclient.model.dto;

import java.sql.Date;

public class ContactStateHistoryDTO {

	private String contactStateHisNo;		//접촉상태 이력번호
	private java.sql.Date changeDate;		//변경일자
	private String contactState;			//접촉상태
	private String potentialClientNo;		//잠재고객번호
	
	public ContactStateHistoryDTO() {}

	public ContactStateHistoryDTO(String contactStateHisNo, Date changeDate, String contactState,
			String potentialClientNo) {
		super();
		this.contactStateHisNo = contactStateHisNo;
		this.changeDate = changeDate;
		this.contactState = contactState;
		this.potentialClientNo = potentialClientNo;
	}

	public String getContactStateHisNo() {
		return contactStateHisNo;
	}

	public java.sql.Date getChangeDate() {
		return changeDate;
	}

	public String getContactState() {
		return contactState;
	}

	public String getPotentialClientNo() {
		return potentialClientNo;
	}

	public void setContactStateHisNo(String contactStateHisNo) {
		this.contactStateHisNo = contactStateHisNo;
	}

	public void setChangeDate(java.sql.Date changeDate) {
		this.changeDate = changeDate;
	}

	public void setContactState(String contactState) {
		this.contactState = contactState;
	}

	public void setPotentialClientNo(String potentialClientNo) {
		this.potentialClientNo = potentialClientNo;
	}

	@Override
	public String toString() {
		return "ContactStateHistoryDTO [contactStateHisNo=" + contactStateHisNo + ", changeDate=" + changeDate
				+ ", contactState=" + contactState + ", potentialClientNo=" + potentialClientNo + "]";
	}
	
	
}
