package com.codefactory.mnm.potentialclient.model.service;

import java.util.Map;

import com.codefactory.mnm.potentialclient.model.dto.PotentialClientDTO;
import com.codefactory.mnm.potentialclient.model.dto.PotentialClientInsertDTO;
import com.codefactory.mnm.potentialclient.model.dto.PotentialClientSearchDTO;

public interface PotentialClientService {

	int insertPotentialClient(PotentialClientInsertDTO potentialClientInsertDTO);

	Map<String, Object> selectPotentialClientList(PotentialClientSearchDTO potentialClientSearchDTO);

	Map<String, Object> selectPotentialClient(PotentialClientDTO potentialClientDTO);

	int deletePotentialClient(String potentialClientNo);

	int updatePotentialClient(PotentialClientInsertDTO potentialClientInsertDTO);


}
