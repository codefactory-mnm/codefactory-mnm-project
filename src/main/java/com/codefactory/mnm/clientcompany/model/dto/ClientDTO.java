package com.codefactory.mnm.clientcompany.model.dto;

public class ClientDTO implements java.io.Serializable {	
	
	private String clientNo;				//고객번호
	private String clientName;				//고객명
	private String dept;					//부서
	private String job;						//직위
	private String phone;					//휴대폰번호
	private String landlineTel;				//유선번호
	private String email;					//이메일
	
	public ClientDTO() {}

	public ClientDTO(String clientNo, String clientName, String dept, String job, String phone, String landlineTel,
			String email) {
		super();
		this.clientNo = clientNo;
		this.clientName = clientName;
		this.dept = dept;
		this.job = job;
		this.phone = phone;
		this.landlineTel = landlineTel;
		this.email = email;
	}

	public String getClientNo() {
		return clientNo;
	}

	public void setClientNo(String clientNo) {
		this.clientNo = clientNo;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getLandlineTel() {
		return landlineTel;
	}

	public void setLandlineTel(String landlineTel) {
		this.landlineTel = landlineTel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "ClientDTO [clientNo=" + clientNo + ", clientName=" + clientName + ", dept=" + dept + ", job=" + job
				+ ", phone=" + phone + ", landlineTel=" + landlineTel + ", email=" + email + "]";
	}
	
	
}
