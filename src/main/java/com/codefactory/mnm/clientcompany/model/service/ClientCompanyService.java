package com.codefactory.mnm.clientcompany.model.service;

import java.util.List;
import java.util.Map;

import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyAttachmentDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyCollectionDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanySearchDTO;
import com.codefactory.mnm.clientcompany.model.dto.DepartmentDTO;
import com.codefactory.mnm.clientcompany.model.dto.OpinionDTO;
import com.codefactory.mnm.clientcompany.model.dto.UsersDTO;

public interface ClientCompanyService {

	List<DepartmentDTO> selectDepartmentList();														//부서 리스트 조회
	
	List<UsersDTO> selectUsersList();																//담당자 리스트 조회
	
	Map<String, Object> selectClientCompanyList(ClientCompanySearchDTO clientCompanySearch);		//고객사 리스트 조회
	
	Map<String, Object> selectClientCompany(String clientCompanyNo);								//고객사 상세 조회
	
	List<OpinionDTO> selectOpinionList(String clientCompanyNo);										//의견 리스트 조회
	
	int insertOpinion(String clientCompanyNo, String opinionContent, String id);					//의견 등록
	
	int deleteOpinion(String opinionNo);															//의견 삭제
	
	ClientCompanyAttachmentDTO selectFile(String fileNo);											//첨부파일 조회

	int insertClientComapany(ClientCompanyCollectionDTO clientCompanyCollection);					//고객사 등록

	Map<String, Object> seleteClientCompanyForUpdate(String clientCompanyNo);						//고객사 수정페이지 조회

	Map<String, Object> deleteFile(String fileNo);													//첨부파일 삭제

	int updateClientComapany(ClientCompanyCollectionDTO clientCompanyCollection);					//고객사 수정

	int deleteClientCompany(String clientCompanyNo);												//고객사 삭제			







}
