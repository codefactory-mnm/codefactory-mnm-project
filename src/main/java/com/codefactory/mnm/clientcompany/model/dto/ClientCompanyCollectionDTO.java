package com.codefactory.mnm.clientcompany.model.dto;

import java.util.List;

public class ClientCompanyCollectionDTO implements java.io.Serializable {
	
	private ClientCompanyDTO clientCompany;				//고객사DTO
	private List<ClientCompanyAttachmentDTO> files;		//첨부파일DTO List
	
	public ClientCompanyCollectionDTO() {}

	public ClientCompanyDTO getClientCompany() {
		return clientCompany;
	}

	public void setClientCompany(ClientCompanyDTO clientCompany) {
		this.clientCompany = clientCompany;
	}

	public List<ClientCompanyAttachmentDTO> getFiles() {
		return files;
	}

	public void setFiles(List<ClientCompanyAttachmentDTO> files) {
		this.files = files;
	}

	@Override
	public String toString() {
		return "ClientCompanyCollectionDTO [clientCompany=" + clientCompany + ", files=" + files + "]";
	}
	
	
}
