package com.codefactory.mnm.clientcompany.model.dto;

public class ClientCompanyCountDTO implements java.io.Serializable {

	private String clientCompanyNo;				//고객사번호
	private int businessOpportunityCount;		//영업기회 횟수
	private int businessActivityCount;			//영업활동 횟수
	private int estimateCount;					//견적 횟수
	private int contractCount;					//계약 횟수
	private int clientSupportCount;				//고객지원 횟수
	
	public ClientCompanyCountDTO() {}
	
	public ClientCompanyCountDTO(String clientCompanyNo, int businessOpportunityCount, int businessActivityCount,
			int estimateCount, int contractCount, int clientSupportCount) {
		super();
		this.clientCompanyNo = clientCompanyNo;
		this.businessOpportunityCount = businessOpportunityCount;
		this.businessActivityCount = businessActivityCount;
		this.estimateCount = estimateCount;
		this.contractCount = contractCount;
		this.clientSupportCount = clientSupportCount;
	}

	public String getClientCompanyNo() {
		return clientCompanyNo;
	}

	public void setClientCompanyNo(String clientCompanyNo) {
		this.clientCompanyNo = clientCompanyNo;
	}

	public int getBusinessOpportunityCount() {
		return businessOpportunityCount;
	}

	public void setBusinessOpportunityCount(int businessOpportunityCount) {
		this.businessOpportunityCount = businessOpportunityCount;
	}

	public int getBusinessActivityCount() {
		return businessActivityCount;
	}

	public void setBusinessActivityCount(int businessActivityCount) {
		this.businessActivityCount = businessActivityCount;
	}

	public int getEstimateCount() {
		return estimateCount;
	}

	public void setEstimateCount(int estimateCount) {
		this.estimateCount = estimateCount;
	}

	public int getContractCount() {
		return contractCount;
	}

	public void setContractCount(int contractCount) {
		this.contractCount = contractCount;
	}

	public int getClientSupportCount() {
		return clientSupportCount;
	}

	public void setClientSupportCount(int clientSupportCount) {
		this.clientSupportCount = clientSupportCount;
	}

	@Override
	public String toString() {
		return "ClientCompanyCountDTO [clientCompanyNo=" + clientCompanyNo + ", businessOpportunityCount="
				+ businessOpportunityCount + ", businessActivityCount=" + businessActivityCount + ", estimateCount="
				+ estimateCount + ", contractCount=" + contractCount + ", clientSupportCount=" + clientSupportCount
				+ "]";
	}

}
