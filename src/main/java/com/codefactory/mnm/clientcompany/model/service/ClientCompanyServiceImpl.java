package com.codefactory.mnm.clientcompany.model.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.codefactory.mnm.clientcompany.model.dao.ClientCompanyDAO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyAttachmentDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyCollectionDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyCountDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyListDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyManagerDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanySalesDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanySearchDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientDTO;
import com.codefactory.mnm.clientcompany.model.dto.DepartmentDTO;
import com.codefactory.mnm.clientcompany.model.dto.OpinionDTO;
import com.codefactory.mnm.clientcompany.model.dto.UsersDTO;
import com.codefactory.mnm.common.Pagenation;
import com.codefactory.mnm.common.SelectCriteria;

@Service
public class ClientCompanyServiceImpl implements ClientCompanyService {

	private final ClientCompanyDAO mapper;
	
	@Autowired
	public ClientCompanyServiceImpl(ClientCompanyDAO mapper) {
		this.mapper = mapper;
	}

	/* 부서 리스트 조회 */
	@Override
	public List<DepartmentDTO> selectDepartmentList() {
			
		return mapper.selectDepartmentList();
	}
	
	/* 담당자 리스트 조회 */
	@Override
	public List<UsersDTO> selectUsersList() {
		
		return mapper.selectUsersList();
	}
	
	/* 고객사 리스트 조회 */
	@Override
	public Map<String, Object> selectClientCompanyList(ClientCompanySearchDTO clientCompanySearch) {
		
		int pageNo = 1;		

		if(pageNo <= 0) {
			pageNo = 1;
		}
		
		int limit = 10;		
		int buttonAmount = 5;
		
		/* 전체 게시물 수, 검색조건이 있다면 검색조건에 맞는 게시물 수 */
		int totalCount = mapper.selectTotalCount(clientCompanySearch); 
		
		/* view에서 전달받은 요청페이지*/
		String currentPage = clientCompanySearch.getCurrentPage();
		
		/* view에서 전달받은 요청페이지를 pageNo에 저장 */
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		/* 설정한 변수에 따른 페이징조건 */
		SelectCriteria selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount);
		
		clientCompanySearch.setSelectCriteria(selectCriteria);
		
		/* 상세조회시 필요한 변수이나 리스트조회할 땐 필요없어서 null처리 */
		String clientCompanyNo = null;
		
		List<ClientCompanyListDTO> clientCompanyList = mapper.selectClientCompanyList(clientCompanySearch);			//고객사 리스트 조회
		List<ClientCompanyManagerDTO> clientCompanyManager = mapper.selectClientCompanyManager(clientCompanyNo);	//고객사 담당자 리스트 조회
		List<ClientCompanySalesDTO> clientCompanySales = mapper.selectClientCompanySales();							//고객사 매출 리스트 조회
		List<ClientCompanyCountDTO> clientCompanyCount = mapper.selectClientCompanyCount();							//고객사의 영업기회, 영업활동, 견적, 계약, 고객지원 횟수 리스트 조회
		
		Map<String, Object> map = new HashMap<>();
		
		map.put("clientCompanyList", clientCompanyList);
		map.put("clientCompanyManager", clientCompanyManager);
		map.put("clientCompanySales", clientCompanySales);
		map.put("clientCompanyCount", clientCompanyCount);
		map.put("selectCriteria", selectCriteria);
		map.put("totalCount", totalCount);
		
		return map;
	}
	
	/* 고객사 상세조회 */
	@Override
	public Map<String, Object> selectClientCompany(String clientCompanyNo) {
		
		ClientCompanyDTO clientCompany = mapper.selectClientCompany(clientCompanyNo);											//고객사 정보 조회
		List<ClientCompanyManagerDTO> clientCompanyManager = mapper.selectClientCompanyManager(clientCompanyNo);				//고객사 담당자 리스트 조회
		List<ClientCompanyAttachmentDTO> clientCompanyAttachmentList = mapper.selectClientCompanyAttachment(clientCompanyNo);	//고객사 첨부파일 리스트 조회
		List<ClientDTO> clientList = mapper.selectClient(clientCompanyNo);														//고객사에 해당하는 고객리스트 조회
		
		Map<String, Object> map = new HashMap<>();

		map.put("clientCompany", clientCompany);
		map.put("clientCompanyManager", clientCompanyManager);
		map.put("clientCompanyAttachmentList", clientCompanyAttachmentList);
		map.put("clientList", clientList);
		
		return map;
	}
	
	/* 의견 리스트 조회 */
	@Override
	public List<OpinionDTO> selectOpinionList(String clientCompanyNo ) {
		
		return mapper.selectOpinionList(clientCompanyNo);
	}
	
	/* 의견 작성 */
	/* DB에 저장하는 과정이므로 트랜잭션 처리를 해줌 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertOpinion(String clientCompanyNo, String opinionContent, String id) {
		
		/* 로그인된 사용자의 번호, 이름 조회하여 저장 */
		OpinionDTO opinion = mapper.selectUserNoAndName(id);
		
		/* 고객사번호, 의견내용, 아이디를 저장 */
		opinion.setClientCompanyNo(clientCompanyNo);
		opinion.setOpinionContent(opinionContent);
		opinion.setId(id);

		return mapper.insertOpinion(opinion);
	}
	
	/* 의견 삭제 */
	/* DB에서 삭제하는 과정이므로 트랜잭션 처리를 해줌 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int deleteOpinion(String opinionNo) {

		return mapper.deleteOpinion(opinionNo);
	}
	
	/* 파일 조회 */
	@Override
	public ClientCompanyAttachmentDTO selectFile(String fileNo) {

		return mapper.selectFile(fileNo);
	}


	/* 고객사 등록 */
	/* DB에 저장하는 과정이므로 트랜잭션 처리를 해줌 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertClientComapany(ClientCompanyCollectionDTO clientCompanyCollection) {

		ClientCompanyDTO clientCompany = clientCompanyCollection.getClientCompany();
		List<ClientCompanyAttachmentDTO> files = clientCompanyCollection.getFiles();
		
		int result1 = mapper.insertClientCompany(clientCompany); //고객사 등록
		
		String progressStatus = clientCompany.getProgressStatus();	
		int result2 = mapper.insertClientCompanyChangeHistory(progressStatus); //고객사 진행상태 변경이력 등록
		
		int result3 = 0;	
		if(files != null) {
			for(ClientCompanyAttachmentDTO ClientCompanyAttachment : files) {
				result3 += mapper.insertClientCompanyAttachment(ClientCompanyAttachment);	//첨부파일 리스트 등록
			}	
		}
		
		int result4 = mapper.insertBoard();	//고객사별 보드 등록
		
		/* 모든 조건이 부합할 경우 result를 1로 반환 */
		int result = 0;
		if(result1 == 1 && result2 == 1 && result4 == 1 && (files == null || result3 == files.size())) {
			result = 1;
		}
		
		return result;
	}

	/* 고객사 수정페이지 조회 */
	@Override
	public Map<String, Object> seleteClientCompanyForUpdate(String clientCompanyNo) {
		
		ClientCompanyDTO clientCompany = mapper.selectClientCompany(clientCompanyNo);											//고객사 조회
		List<ClientCompanyManagerDTO> clientCompanyManager = mapper.selectClientCompanyManager(clientCompanyNo);				//고객사 담당자 리스트 조회
		List<ClientCompanyAttachmentDTO> clientCompanyAttachmentList = mapper.selectClientCompanyAttachment(clientCompanyNo);	//고객사 첨부파일 리스트 조회
		
		Map<String, Object> map = new HashMap<>();

		map.put("clientCompany", clientCompany);
		map.put("clientCompanyManager", clientCompanyManager);
		map.put("clientCompanyAttachmentList", clientCompanyAttachmentList);
		
		return map;
	}

	/* 첨부파일 삭제 */
	/* DB에서 삭제하는 과정이므로 트랜잭션 처리를 해줌 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public Map<String, Object> deleteFile(String fileNo) {
	
		ClientCompanyAttachmentDTO clientCompanyAttachment = mapper.selectFile(fileNo);		//첨부파일정보 조회			
		int result = mapper.deleteFile(fileNo);												//첨부파일 삭제
		
		Map<String, Object> map = new HashMap<>();
		
		map.put("clientCompanyAttachment", clientCompanyAttachment);
		map.put("result", result);
		
		return map;
	}
	
	/* 고객사 수정 */
	/* DB에 저장하는 과정이므로 트랜잭션 처리를 해줌 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int updateClientComapany(ClientCompanyCollectionDTO clientCompanyCollection) {

		ClientCompanyDTO clientCompany = clientCompanyCollection.getClientCompany();
		List<ClientCompanyAttachmentDTO> files = clientCompanyCollection.getFiles();
		
		int result1 = mapper.updateClientCompany(clientCompany); //고객사 수정		
		
		String clientCompanyNo = clientCompany.getClientCompanyNo();	//고객사 번호
		String progressStatus = clientCompany.getProgressStatus();		//진행상태
		
		Map<String, String> map = new HashMap<>();
		
		map.put("clientCompanyNo", clientCompanyNo);
		map.put("progressStatus", progressStatus);
		
		int result2 = mapper.insertClientCompanyChangeHistoryByUpdate(map);	//고객사 진행상태 변경이력 등록
		
		int result3 = 0;	
		if(files != null) {
			for(ClientCompanyAttachmentDTO clientCompanyAttachment : files) {
				clientCompanyAttachment.setClientCompanyNo(clientCompanyNo);
				result3 += mapper.insertClientCompanyAttachmentByUpdate(clientCompanyAttachment);	//첨부파일 등록
			}	
		}
		
		/* 모든 조건이 부합할 경우 result를 1로 반환 */
		int result = 0;
		if(result1 == 1 && result2 == 1 &&  (files == null || result3 == files.size())) {
			result = 1;
		}
		
		return result;
	}

	/* 고객사 삭제 */
	/* DB에 저장하는 과정이므로 트랜잭션 처리를 해줌 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int deleteClientCompany(String clientCompanyNo) {
		
		return mapper.deleteClientCompany(clientCompanyNo); 
	}

	
}
