package com.codefactory.mnm.clientcompany.model.dto;

import com.codefactory.mnm.common.SelectCriteria;

public class ClientCompanySearchDTO implements java.io.Serializable {

	private String clientCompanyName;			//회사명
	private String division;					//구분
	private String deptName;					//담당자 부서
	private String userNo;						//담당자번호
	private String currentPage;					//현재페이지
	private SelectCriteria selectCriteria;		//페이징처리
	
	public ClientCompanySearchDTO() {}

	public ClientCompanySearchDTO(String clientCompanyName, String division, String deptName, String userNo,
			String currentPage, SelectCriteria selectCriteria) {
		super();
		this.clientCompanyName = clientCompanyName;
		this.division = division;
		this.deptName = deptName;
		this.userNo = userNo;
		this.currentPage = currentPage;
		this.selectCriteria = selectCriteria;
	}

	public String getClientCompanyName() {
		return clientCompanyName;
	}

	public void setClientCompanyName(String clientCompanyName) {
		this.clientCompanyName = clientCompanyName;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(String currentPage) {
		this.currentPage = currentPage;
	}

	public SelectCriteria getSelectCriteria() {
		return selectCriteria;
	}

	public void setSelectCriteria(SelectCriteria selectCriteria) {
		this.selectCriteria = selectCriteria;
	}

	@Override
	public String toString() {
		return "ClientCompanySearchDTO [clientCompanyName=" + clientCompanyName + ", division=" + division
				+ ", deptName=" + deptName + ", userNo=" + userNo + ", currentPage=" + currentPage + ", selectCriteria="
				+ selectCriteria + "]";
	}

	

	


}
