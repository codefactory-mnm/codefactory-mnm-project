package com.codefactory.mnm.clientcompany.model.dto;

import java.sql.Date;

public class ClientCompanyListDTO implements java.io.Serializable {

	private String clientCompanyNo;			//고객사번호
	private String clientCompanyName;		//고객사명
	private String address;					//주소
	private String detailedAddress;			//상세주소
	private java.sql.Date writeDate;		//작성일자
	
	public ClientCompanyListDTO() {}

	public ClientCompanyListDTO(String clientCompanyNo, String clientCompanyName, String address,
			String detailedAddress, Date writeDate) {
		super();
		this.clientCompanyNo = clientCompanyNo;
		this.clientCompanyName = clientCompanyName;
		this.address = address;
		this.detailedAddress = detailedAddress;
		this.writeDate = writeDate;
	}

	public String getClientCompanyNo() {
		return clientCompanyNo;
	}

	public void setClientCompanyNo(String clientCompanyNo) {
		this.clientCompanyNo = clientCompanyNo;
	}

	public String getClientCompanyName() {
		return clientCompanyName;
	}

	public void setClientCompanyName(String clientCompanyName) {
		this.clientCompanyName = clientCompanyName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDetailedAddress() {
		return detailedAddress;
	}

	public void setDetailedAddress(String detailedAddress) {
		this.detailedAddress = detailedAddress;
	}

	public java.sql.Date getWriteDate() {
		return writeDate;
	}

	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}

	@Override
	public String toString() {
		return "ClientCompanyListDTO [clientCompanyNo=" + clientCompanyNo + ", clientCompanyName=" + clientCompanyName
				+ ", address=" + address + ", detailedAddress=" + detailedAddress + ", writeDate=" + writeDate + "]";
	}


}
