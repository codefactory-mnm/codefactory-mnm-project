package com.codefactory.mnm.clientcompany.model.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyAttachmentDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyCountDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyListDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyManagerDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanySalesDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanySearchDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientDTO;
import com.codefactory.mnm.clientcompany.model.dto.DepartmentDTO;
import com.codefactory.mnm.clientcompany.model.dto.OpinionDTO;
import com.codefactory.mnm.clientcompany.model.dto.UsersDTO;

@Mapper
public interface ClientCompanyDAO {

	List<DepartmentDTO> selectDepartmentList();																//부서 리스트 조회
	
	List<UsersDTO> selectUsersList();																		//담당자 리스트 조회
	
	List<ClientCompanyListDTO> selectClientCompanyList(ClientCompanySearchDTO clientCompanySearch);			//고객사 리스트 조회

	List<ClientCompanyManagerDTO> selectClientCompanyManager(String clientCompanyNo);						//고객사 담당자 리스트 조회

	List<ClientCompanySalesDTO> selectClientCompanySales();													//고객사 매출 리스트 조회

	int selectTotalCount(ClientCompanySearchDTO clientCompanySearch);										//게시물 수 조회

	List<ClientCompanyCountDTO> selectClientCompanyCount();													//고객사의 영업기회, 영업활동, 견적, 계약, 고객지원 횟수 리스트 조회

	ClientCompanyDTO selectClientCompany(String clientCompanyNo);											//고객사 조회

	List<ClientCompanyAttachmentDTO> selectClientCompanyAttachment(String clientCompanyNo);					//고객사 첨부파일 리스트 조회
	
	List<ClientDTO> selectClient(String clientCompanyNo);													//고객사에 해당하는 고객리스트 조회
	
	List<OpinionDTO> selectOpinionList(String clientCompanyNo);												//의견 리스트 조회

	OpinionDTO selectUserNoAndName(String id);																//로그인된 사용자의 번호, 이름 조회
	
	int insertOpinion(OpinionDTO opinion);																	//의견 등록
	
	int deleteOpinion(String opinionNo);																	//의견 삭제
	
	ClientCompanyAttachmentDTO selectFile(String fileNo);													//파일 조회
	
	int insertClientCompany(ClientCompanyDTO clientCompany);												//고객사 등록
	
	int insertClientCompanyChangeHistory(String progressStatus);											//고객사진행상태변경이력 등록
	
	int insertClientCompanyAttachment(ClientCompanyAttachmentDTO clientCompanyAttachment);					//첨부파일 등록

	int insertBoard();																						//고객사별 보드 등록
	
	int deleteFile(String fileNo);																			//첨부파일 삭제

	int updateClientCompany(ClientCompanyDTO clientCompany);												//고객사 수정

	int insertClientCompanyChangeHistoryByUpdate(Map<String, String> map);									//고객사 수정으로 인한 고객사진행상태변경이력 등록

	int insertClientCompanyAttachmentByUpdate(ClientCompanyAttachmentDTO clientCompanyAttachment);			//고객사 수정 시 첨부파일 등록

	int deleteClientCompany(String clientCompanyNo);														//고객사 삭제















}
