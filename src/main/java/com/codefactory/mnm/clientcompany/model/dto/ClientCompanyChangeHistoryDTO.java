package com.codefactory.mnm.clientcompany.model.dto;

import java.sql.Date;

public class ClientCompanyChangeHistoryDTO implements java.io.Serializable {
	
	private String clientCompanyHstNo;		//상태변경 이력번호
	private String progressStatus;			//진행상태
	private java.sql.Date changeDate;		//변경일자
	private String clientCompanyNo;			//고객사번호
	
	public ClientCompanyChangeHistoryDTO() {}

	public ClientCompanyChangeHistoryDTO(String clientCompanyHstNo, String progressStatus, Date changeDate,
			String clientCompanyNo) {
		super();
		this.clientCompanyHstNo = clientCompanyHstNo;
		this.progressStatus = progressStatus;
		this.changeDate = changeDate;
		this.clientCompanyNo = clientCompanyNo;
	}

	public String getClientCompanyHstNo() {
		return clientCompanyHstNo;
	}

	public void setClientCompanyHstNo(String clientCompanyHstNo) {
		this.clientCompanyHstNo = clientCompanyHstNo;
	}

	public String getProgressStatus() {
		return progressStatus;
	}

	public void setProgressStatus(String progressStatus) {
		this.progressStatus = progressStatus;
	}

	public java.sql.Date getChangeDate() {
		return changeDate;
	}

	public void setChangeDate(java.sql.Date changeDate) {
		this.changeDate = changeDate;
	}

	public String getClientCompanyNo() {
		return clientCompanyNo;
	}

	public void setClientCompanyNo(String clientCompanyNo) {
		this.clientCompanyNo = clientCompanyNo;
	}

	@Override
	public String toString() {
		return "ClientCompanyChangeHistoryDTO [clientCompanyHstNo=" + clientCompanyHstNo + ", progressStatus="
				+ progressStatus + ", changeDate=" + changeDate + ", clientCompanyNo=" + clientCompanyNo + "]";
	}

	
	
}
