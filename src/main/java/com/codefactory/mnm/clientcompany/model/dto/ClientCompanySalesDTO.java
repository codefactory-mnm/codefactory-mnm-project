package com.codefactory.mnm.clientcompany.model.dto;

public class ClientCompanySalesDTO implements java.io.Serializable {
	
	private String clientCompanyNo;			//고객사번호
	private Integer sum;					//매출
	
	public ClientCompanySalesDTO() {}

	public ClientCompanySalesDTO(String clientCompanyNo, Integer sum) {
		super();
		this.clientCompanyNo = clientCompanyNo;
		this.sum = sum;
	}

	public String getClientCompanyNo() {
		return clientCompanyNo;
	}

	public void setClientCompanyNo(String clientCompanyNo) {
		this.clientCompanyNo = clientCompanyNo;
	}

	public Integer getSum() {
		return sum;
	}

	public void setSum(Integer sum) {
		this.sum = sum;
	}

	@Override
	public String toString() {
		return "ClientCompanySalesDTO [clientCompanyNo=" + clientCompanyNo + ", sum=" + sum + "]";
	}

}
