package com.codefactory.mnm.clientcompany.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyAttachmentDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyCollectionDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyCountDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyListDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyManagerDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanySalesDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanySearchDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientDTO;
import com.codefactory.mnm.clientcompany.model.dto.DepartmentDTO;
import com.codefactory.mnm.clientcompany.model.dto.OpinionDTO;
import com.codefactory.mnm.clientcompany.model.dto.UsersDTO;
import com.codefactory.mnm.clientcompany.model.service.ClientCompanyService;
import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.member.model.dto.UserImpl;
import java.net.URLDecoder;

@Controller
@RequestMapping("/clientCompany")
public class ClientCompanyController {

	private ClientCompanyService clientCompanyService;

	@Autowired
	public ClientCompanyController(ClientCompanyService clientCompanyService) {
		this.clientCompanyService = clientCompanyService;
	}

	/* 부서 리스트 조회 */
	@GetMapping(value="dept", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<DepartmentDTO> selectDepartmentList() {

		return clientCompanyService.selectDepartmentList();

	}

	/* 담당자 리스트 조회*/
	@GetMapping(value="users", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<UsersDTO> selectUsersList() {

		return clientCompanyService.selectUsersList();

	}

	/* 고객사 리스트 조회 */
	@GetMapping("/list")
	public ModelAndView selectClientCompanyList(ModelAndView mv, 
			@RequestParam(defaultValue = "") String clientCompanyName,
			@RequestParam(defaultValue = "") String deptName,
			@RequestParam(defaultValue = "") String division,
			@RequestParam(defaultValue = "") String userNo,
			@RequestParam(defaultValue = "1") String currentPage) {

		/* 처음 고객사 리스트를 조회하거나 또는 검색조건을 설정하지 않았을 때를 대비 */
		if(clientCompanyName.equals("")) { // 검색조건 중 고객사를 입력하지 않았을 경우
			clientCompanyName = null;
		}

		if(deptName.equals("")) { // 검색조건 중 부서를 선택하지 않았을 경우
			deptName = null;
		}

		if(division.equals("")) { // 검색조건 중 구분을 선택하지 않았을 경우
			division = null;
		}

		if(userNo.equals("")) { // 검색조건 중 이름을 선택하지 않았을 경우
			userNo = null;
		}

		ClientCompanySearchDTO clientCompanySearch = new ClientCompanySearchDTO();
		clientCompanySearch.setClientCompanyName(clientCompanyName);
		clientCompanySearch.setDeptName(deptName);
		clientCompanySearch.setDivision(division);
		clientCompanySearch.setUserNo(userNo);
		clientCompanySearch.setCurrentPage(currentPage);

		Map<String, Object> map = clientCompanyService.selectClientCompanyList(clientCompanySearch);

		List<ClientCompanyListDTO> clientCompanyList = (List<ClientCompanyListDTO>) map.get("clientCompanyList");				//고객사 리스트 조회
		List<ClientCompanyManagerDTO> clientCompanyManager = (List<ClientCompanyManagerDTO>) map.get("clientCompanyManager");	//고객사 담당자 리스트 조회
		List<ClientCompanySalesDTO> clientCompanySales = (List<ClientCompanySalesDTO>) map.get("clientCompanySales");			//고객사 매출 리스트 조회
		List<ClientCompanyCountDTO> clientCompanyCount = (List<ClientCompanyCountDTO>) map.get("clientCompanyCount");			//고객사의 영업기회, 영업활동, 견적, 계약, 고객지원 횟수 리스트 조회
		SelectCriteria selectCriteria = (SelectCriteria) map.get("selectCriteria");												//페이징 처리		
		int totalCount = (int) map.get("totalCount");																			//게시물 수 조회

		mv.addObject("clientCompanyList", clientCompanyList);
		mv.addObject("clientCompanyManager", clientCompanyManager);
		mv.addObject("clientCompanySales", clientCompanySales);
		mv.addObject("clientCompanyCount", clientCompanyCount);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("clientCompanySearch", clientCompanySearch);
		mv.addObject("totalCount", totalCount);

		mv.setViewName("clientCompany/list");
		return mv;
	}

	/* 고객사 상세조회 */
	@GetMapping("/select")
	public ModelAndView selectClientCompany(ModelAndView mv, String clientCompanyNo) {

		Map<String, Object> map = clientCompanyService.selectClientCompany(clientCompanyNo);

		ClientCompanyDTO clientCompany = (ClientCompanyDTO) map.get("clientCompany");																//고객사 조회
		List<ClientCompanyManagerDTO> clientCompanyManager = (List<ClientCompanyManagerDTO>) map.get("clientCompanyManager");						//고객사 담당자 리스트 조회
		List<ClientCompanyAttachmentDTO> clientCompanyAttachmentList = (List<ClientCompanyAttachmentDTO>) map.get("clientCompanyAttachmentList");	//첨부파일 리스트 조회
		List<ClientDTO> clientList = (List<ClientDTO>) map.get("clientList");																		//고객사에 해당하는 고객리스트 조회

		/* 고객사 담당자 리스트를 한칸에 한줄로 표시하기 위해서 name변수에 담아서 view로 전달 */
		String name = "";

		for(int i = 0; i < clientCompanyManager.size(); i++) {
			name += clientCompanyManager.get(i).getName() + " ";
		}

		if(clientCompanyManager.size() < 1) {
			name = "담당없음";
		}

		mv.addObject("clientCompany", clientCompany);
		mv.addObject("name", name);		
		mv.addObject("clientCompanyAttachmentList", clientCompanyAttachmentList);		
		mv.addObject("clientList", clientList);		
		mv.setViewName("clientCompany/select");
		return mv;
	}

	/* 의견 리스트 조회 */
	@GetMapping(value="opinion/select", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<OpinionDTO> selectOpinionList(String clientCompanyNo, @AuthenticationPrincipal UserImpl customUser) {

		List<OpinionDTO> selectOpinionList = clientCompanyService.selectOpinionList(clientCompanyNo);

		/* 의견삭제시 작성자와 로그인된사용자가 같은지 확인하기 위해서 사용자번호를 담음 */
		for(int i = 0; i < selectOpinionList.size(); i++) {

			selectOpinionList.get(i).setLoginUserNo(customUser.getUserNo());

		}

		return selectOpinionList;
	}

	/* 의견 등록 */
	@PostMapping(value="opinion/insert", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int insertOpinion(String clientCompanyNo, String opinionContent, Principal pcp) {

		String id = pcp.getName();	//세션에 담겨있는 사용자 아이디

		return clientCompanyService.insertOpinion(clientCompanyNo, opinionContent, id);

	}

	/* 의견 삭제 */
	@PostMapping(value="opinion/delete", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int deleteOpinion(String opinionNo) {

		return clientCompanyService.deleteOpinion(opinionNo);		
	}

	/*  첨부파일 다운로드 */
	@GetMapping(value="fileDownload")
	@ResponseBody
	public void fileDownload(String fileNo, HttpServletResponse response) throws Exception {

		ClientCompanyAttachmentDTO clientCompanyAttachment = clientCompanyService.selectFile(fileNo);

		String fileName = clientCompanyAttachment.getFileName();					//저장파일명
		String fileOriginalName = clientCompanyAttachment.getFileOriginalName();	//원본파일명
		String filePath = clientCompanyAttachment.getFilePath();					//첨부파일 저장경로(최종경로)
		String saveFileName = filePath + fileName;

		File file = new File(saveFileName);
		long fileLength = file.length();

		/* 파일 정보들을 가지고 reponse의 Header에 세팅 */
		response.setHeader("Content-Disposition", "attachment; filename=\"" + URLEncoder.encode(fileOriginalName,"UTF-8") + "\";");
		response.setHeader("Content-Transfer-Encoding", "binary");
		response.setHeader("Content-Type", "application/octet-stream");
		response.setHeader("Content-Length", "" + fileLength);
		response.setHeader("Pragma", "no-cache;");
		response.setHeader("Expires", "-1;");

		/* saveFileName을 파라미터로 넣어 inputStream 객체를 만들고 
	        response에서 파일을 내보낼 OutputStream을 가져와서 */
		FileInputStream fis = new FileInputStream(saveFileName);
		OutputStream out = response.getOutputStream();

		/* 파일 읽을 만큼 크기의 buffer를 생성한 후 */
		int readCount = 0;
		byte[] buffer = new byte[1024];
		
		/* outputStream에 씌워준다 */
		while((readCount = fis.read(buffer)) != -1){
			out.write(buffer,0,readCount);
		}

		fis.close();
		out.close();

	}

	/* 고객사 작성화면으로 이동 */
	@GetMapping("/insert")
	public void insertClientCompanyPage() {}

	/* 고객사 등록 */
	@PostMapping("/insert")
	public ModelAndView insertClientComapany(ModelAndView mv, ClientCompanyDTO clientCompany,
			List<MultipartFile> multiFiles, HttpServletRequest request, RedirectAttributes rttr) {

		/* 고객사 등록과 동시에 파일첨부도 같이 등록한 경우 */
		if(multiFiles.size() > 0 && !multiFiles.get(0).getOriginalFilename().equals("")) {

			String root = request.getSession().getServletContext().getRealPath("/");	//첨부파일 저장경로(절대경로)
			String filePath = root + "static/upload/";									//첨부파일 저장경로(최종경로)

			/* 첨부파일 저장경로 (최종경로) 파일이 존재 하지 않을 경우 생성 */
			File mkdir = new File(filePath);
			if(!mkdir.exists()) {
				mkdir.mkdirs();
			}

			List<ClientCompanyAttachmentDTO> files = new ArrayList<>();

			/* 다중 첨부파일은 반복문을 통해서, 미리 선언한 files에 담음  */
			for(int i = 0; i < multiFiles.size(); i++) {
				String fileOriginalName = multiFiles.get(i).getOriginalFilename();			//원본파일명
				String ext = fileOriginalName.substring(fileOriginalName.lastIndexOf("."));	//확장자명
				String fileName = UUID.randomUUID().toString().replace("-", "") + ext;		//저장파일명

				/* 고객사파일첨부DTO인 file에 정보를 담은 후 리스트인 files에 담음 */
				ClientCompanyAttachmentDTO file = new ClientCompanyAttachmentDTO();			
				file.setFileName(fileName);
				file.setFileOriginalName(fileOriginalName);
				file.setFilePath(filePath);

				files.add(file);
			}

			/* 고객사 등록시 필요한 정보를 ClientCompanyCollectionDTO에 담음 */
			ClientCompanyCollectionDTO clientCompanyCollection = new ClientCompanyCollectionDTO();	
			clientCompanyCollection.setClientCompany(clientCompany);
			clientCompanyCollection.setFiles(files);

			int result = clientCompanyService.insertClientComapany(clientCompanyCollection);

			/* DB에 등록을 성공하였을 경우 */
			if(result > 0 ) {

				/* 파일 저장 */
				try {
					for(int i = 0; i < multiFiles.size(); i++) {
						ClientCompanyAttachmentDTO file = files.get(i);			
						multiFiles.get(i).transferTo(new File(filePath + "\\" + file.getFileName()));
					}

					rttr.addFlashAttribute("message", "고객사 등록을 성공하셨습니다.");

					/* 파일 저장을 실패하였을 경우, 저장했던 파일을 삭제
					 * 트랜젝션처리를 통해서 DB에 입력된 정보도 삭제됨 */
				} catch (Exception e) {
					e.printStackTrace();

					for(int i = 0; i < multiFiles.size(); i++) {

						ClientCompanyAttachmentDTO file = files.get(i); 
						new File(filePath + "\\" + file.getFileName()).delete(); 

					}
					rttr.addFlashAttribute("message", "고객사 등록 후 파일 업로드에 실패하셨습니다.");
				}

				/* DB에 고객사 등록을 실패하였을 경우 */
			} else {
				rttr.addFlashAttribute("message", "고객사 등록을 실패하셨습니다.");	
			}

			/* 파일첨부를 하지 않고, 고객사 등록을 하는 경우 */
		} else {
			ClientCompanyCollectionDTO clientCompanyCollection = new ClientCompanyCollectionDTO();
			clientCompanyCollection.setClientCompany(clientCompany);

			int result = clientCompanyService.insertClientComapany(clientCompanyCollection);

			if(result > 0) {
				rttr.addFlashAttribute("message", "고객사 등록을 성공하셨습니다.");
			} else {
				rttr.addFlashAttribute("message", "고객사 등록을 실패하셨습니다.");
			}

		}

		mv.setViewName("redirect:/clientCompany/list");
		return mv;
	}

	/* 고객사 수정페이지 조회 */
	@GetMapping("/update")
	public ModelAndView seleteClientCompanyForUpdate(ModelAndView mv, String clientCompanyNo) {

		Map<String, Object> map = clientCompanyService.seleteClientCompanyForUpdate(clientCompanyNo);

		ClientCompanyDTO clientCompany = (ClientCompanyDTO) map.get("clientCompany");																//고객사 조회
		List<ClientCompanyManagerDTO> clientCompanyManager = (List<ClientCompanyManagerDTO>) map.get("clientCompanyManager");						//고객사 담당자 리스트 조회
		List<ClientCompanyAttachmentDTO> clientCompanyAttachmentList = (List<ClientCompanyAttachmentDTO>) map.get("clientCompanyAttachmentList");	//고객사 첨부파일 리스트 조회

		String name = "";

		/* 고객사 담당자 리스트를 한칸에 한줄로 표시하기 위해서 name변수에 담아서 view로 전달 */
		for(int i = 0; i < clientCompanyManager.size(); i++) {
			name += clientCompanyManager.get(i).getName() + " ";
		}

		if(clientCompanyManager.size() < 1) {
			name = "담당없음";
		}

		/* 화면으로 전달 */
		mv.addObject("clientCompany", clientCompany);
		mv.addObject("name", name);
		mv.addObject("clientCompanyAttachmentList", clientCompanyAttachmentList);
		mv.setViewName("clientCompany/update");
		return mv;
	}

	/* 첨부파일 삭제 */
	@PostMapping(value="file/delete", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int deleteFile(String fileNo, HttpServletRequest request) {

		String root = request.getSession().getServletContext().getRealPath("/");	//첨부파일 저장경로(절대경로)
		String filePath = root + "static/upload/";									//첨부파일 저장경로(최종경로)

		Map<String, Object> map = clientCompanyService.deleteFile(fileNo);	

		ClientCompanyAttachmentDTO file = (ClientCompanyAttachmentDTO) map.get("clientCompanyAttachment");	//첨부파일정보
		int result = (int) map.get("result");																//첨부파일 DB에서 삭제 결과

		/* 첨부파일을 DB에서 성공적으로 삭제되었을 경우 저장된파일도 삭제 */
		if(result > 0 ) {
			new File(filePath + "\\" + file.getFileName()).delete(); 		
		}

		return result;
	}

	/* 고객사 수정 */
	@PostMapping("/update")
	public ModelAndView updateClientComapany(ModelAndView mv, ClientCompanyDTO clientCompany,
			List<MultipartFile> multiFiles, HttpServletRequest request, RedirectAttributes rttr) {

		/* 고객사 정보 수정과 동시에 파일첨부도 같이 등록한 경우 */
		if(multiFiles.size() > 0 && !multiFiles.get(0).getOriginalFilename().equals("")) {

			String root = request.getSession().getServletContext().getRealPath("/");	//첨부파일 저장경로(절대경로)
			String filePath = root + "static/upload/";									//첨부파일 저장경로(최종경로)

			/* 첨부파일 저장경로 (최종경로) 파일이 존재 하지 않을 경우 생성 */
			File mkdir = new File(filePath);
			if(!mkdir.exists()) {
				mkdir.mkdirs();
			}

			List<ClientCompanyAttachmentDTO> files = new ArrayList<>();

			/* 다중 첨부파일은 반복문을 통해서, 미리 선언한 files에 담음  */
			for(int i = 0; i < multiFiles.size(); i++) {
				String fileOriginalName = multiFiles.get(i).getOriginalFilename();			//원본파일명
				String ext = fileOriginalName.substring(fileOriginalName.lastIndexOf("."));	//확장자명
				String fileName = UUID.randomUUID().toString().replace("-", "") + ext;		//저장파일명

				/* 고객사파일첨부DTO인 file에 정보를 담은 후 리스트인 files에 담음 */
				ClientCompanyAttachmentDTO file = new ClientCompanyAttachmentDTO();			
				file.setFileName(fileName);
				file.setFileOriginalName(fileOriginalName);
				file.setFilePath(filePath);

				files.add(file);
			}

			/* 영업기회 수정시 필요한 정보를 ClientCompanyCollectionDTO에 담음 */
			ClientCompanyCollectionDTO clientCompanyCollection = new ClientCompanyCollectionDTO();	
			clientCompanyCollection.setClientCompany(clientCompany);
			clientCompanyCollection.setFiles(files);

			int result = clientCompanyService.updateClientComapany(clientCompanyCollection);

			/* DB에 등록을 성공하였을 경우 */
			if(result > 0 ) {

				/* 파일 저장 */
				try {
					for(int i = 0; i < multiFiles.size(); i++) {
						ClientCompanyAttachmentDTO file = files.get(i);			
						multiFiles.get(i).transferTo(new File(filePath + "\\" + file.getFileName()));
					}

					rttr.addFlashAttribute("message", "고객사 수정을 성공하셨습니다.");

					/* 파일 저장을 실패하였을 경우, 저장했던 파일을 삭제
					 * 트랜젝션처리를 통해서 DB에 입력된 정보도 삭제됨 */
				} catch (Exception e) {
					e.printStackTrace();

					for(int i = 0; i < multiFiles.size(); i++) {
						ClientCompanyAttachmentDTO file = files.get(i); 
						new File(filePath + "\\" + file.getFileName()).delete(); 
					}

					rttr.addFlashAttribute("message", "고객사 수정 후 파일 업로드에 실패하셨습니다.");
				}

				/* DB에 고객사 수정을 실패하였을 경우 */
			} else {
				rttr.addFlashAttribute("message", "고객사 수정을 실패하셨습니다.");	
			}

			/* 파일첨부를 하지 않고, 고객사 수정하는 경우 */
		} else {

			/* 고객사 수정시 필요한 정보를 clientCompanyCollection에 담음 */
			ClientCompanyCollectionDTO clientCompanyCollection = new ClientCompanyCollectionDTO();
			clientCompanyCollection.setClientCompany(clientCompany);

			int result = clientCompanyService.updateClientComapany(clientCompanyCollection);

			if(result > 0) {
				rttr.addFlashAttribute("message", "고객사 수정을 성공하셨습니다.");
			} else {
				rttr.addFlashAttribute("message", "고객사 수정을 실패하셨습니다.");
			}
			
		}

		mv.setViewName("redirect:/clientCompany/list");
		return mv;
	}

	/* 고객사 삭제 */
	@PostMapping(value="delete", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int deleteClientCompany(String clientCompanyNo) {

		return clientCompanyService.deleteClientCompany(clientCompanyNo);	

	}
}
