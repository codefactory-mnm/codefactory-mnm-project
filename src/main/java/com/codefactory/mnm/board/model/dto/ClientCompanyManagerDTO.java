package com.codefactory.mnm.board.model.dto;

public class ClientCompanyManagerDTO implements java.io.Serializable {
	
	private String clientCompanyNo;		//고객사번호
	private String name;				//담당자이름
	
	public ClientCompanyManagerDTO() {}

	public ClientCompanyManagerDTO(String clientCompanyNo, String name) {
		super();
		this.clientCompanyNo = clientCompanyNo;
		this.name = name;
	}

	public String getClientCompanyNo() {
		return clientCompanyNo;
	}

	public void setClientCompanyNo(String clientCompanyNo) {
		this.clientCompanyNo = clientCompanyNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "ClientCompanyManagerDTO [clientCompanyNo=" + clientCompanyNo + ", name=" + name + "]";
	}

	

	
}
