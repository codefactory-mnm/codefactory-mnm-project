package com.codefactory.mnm.board.model.dto;

import java.sql.Date;

public class ClientCompanyDTO implements java.io.Serializable {
		
	private String clientCompanyNo;			//고객사번호
	private String clientCompanyName;		//고객사명
	private String address;					//주소
	private String detailedAddress;			//상세주소
	private String color;					//색상
	private String boardName;				//보드명
	private String boardNo;					//보드번호
	private String division;				//구분
	private java.sql.Date writeDate;		//작성일자
	
	public ClientCompanyDTO() {}

	public ClientCompanyDTO(String clientCompanyNo, String clientCompanyName, String address, String detailedAddress,
			String color, String boardName, String boardNo, String division, Date writeDate) {
		super();
		this.clientCompanyNo = clientCompanyNo;
		this.clientCompanyName = clientCompanyName;
		this.address = address;
		this.detailedAddress = detailedAddress;
		this.color = color;
		this.boardName = boardName;
		this.boardNo = boardNo;
		this.division = division;
		this.writeDate = writeDate;
	}

	public String getClientCompanyNo() {
		return clientCompanyNo;
	}

	public void setClientCompanyNo(String clientCompanyNo) {
		this.clientCompanyNo = clientCompanyNo;
	}

	public String getClientCompanyName() {
		return clientCompanyName;
	}

	public void setClientCompanyName(String clientCompanyName) {
		this.clientCompanyName = clientCompanyName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDetailedAddress() {
		return detailedAddress;
	}

	public void setDetailedAddress(String detailedAddress) {
		this.detailedAddress = detailedAddress;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getBoardName() {
		return boardName;
	}

	public void setBoardName(String boardName) {
		this.boardName = boardName;
	}

	public String getBoardNo() {
		return boardNo;
	}

	public void setBoardNo(String boardNo) {
		this.boardNo = boardNo;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public java.sql.Date getWriteDate() {
		return writeDate;
	}

	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}

	@Override
	public String toString() {
		return "ClientCompanyDTO [clientCompanyNo=" + clientCompanyNo + ", clientCompanyName=" + clientCompanyName
				+ ", address=" + address + ", detailedAddress=" + detailedAddress + ", color=" + color + ", boardName="
				+ boardName + ", boardNo=" + boardNo + ", division=" + division + ", writeDate=" + writeDate + "]";
	}


}
