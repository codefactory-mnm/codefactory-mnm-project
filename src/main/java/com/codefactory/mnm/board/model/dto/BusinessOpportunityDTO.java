package com.codefactory.mnm.board.model.dto;

import java.sql.Date;

public class BusinessOpportunityDTO implements java.io.Serializable {
	
	private String businessOpportunityNo;  			 //영업기회번호
	private String businessOpportunityName; 		 //영업기회명
	private String clientNo;						 //고객번호
	private String clientName;					 	 //고객명
	private String clientCompanyNo;					 //고객사번호	
	private String clientCompanyName;				 //고객사명
	private java.sql.Date businessStartDate; 		 //영업시작일
	private java.sql.Date businessEndDate;			 //영업종료일
	private String userNo;						 	 //담당자번호
	private String name;							 //담당자이름		
	private String color;							 //색상
	private String boardName;						 //보드명
	private String boardNo;							 //보드번호
	private String division;						 //구분
	private java.sql.Date writeDate;				 //작성일자
	
	public BusinessOpportunityDTO() {}

	public BusinessOpportunityDTO(String businessOpportunityNo, String businessOpportunityName, String clientNo,
			String clientName, String clientCompanyNo, String clientCompanyName, Date businessStartDate,
			Date businessEndDate, String userNo, String name, String color, String boardName, String boardNo,
			String division, Date writeDate) {
		super();
		this.businessOpportunityNo = businessOpportunityNo;
		this.businessOpportunityName = businessOpportunityName;
		this.clientNo = clientNo;
		this.clientName = clientName;
		this.clientCompanyNo = clientCompanyNo;
		this.clientCompanyName = clientCompanyName;
		this.businessStartDate = businessStartDate;
		this.businessEndDate = businessEndDate;
		this.userNo = userNo;
		this.name = name;
		this.color = color;
		this.boardName = boardName;
		this.boardNo = boardNo;
		this.division = division;
		this.writeDate = writeDate;
	}

	public String getBusinessOpportunityNo() {
		return businessOpportunityNo;
	}

	public void setBusinessOpportunityNo(String businessOpportunityNo) {
		this.businessOpportunityNo = businessOpportunityNo;
	}

	public String getBusinessOpportunityName() {
		return businessOpportunityName;
	}

	public void setBusinessOpportunityName(String businessOpportunityName) {
		this.businessOpportunityName = businessOpportunityName;
	}

	public String getClientNo() {
		return clientNo;
	}

	public void setClientNo(String clientNo) {
		this.clientNo = clientNo;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getClientCompanyNo() {
		return clientCompanyNo;
	}

	public void setClientCompanyNo(String clientCompanyNo) {
		this.clientCompanyNo = clientCompanyNo;
	}

	public String getClientCompanyName() {
		return clientCompanyName;
	}

	public void setClientCompanyName(String clientCompanyName) {
		this.clientCompanyName = clientCompanyName;
	}

	public java.sql.Date getBusinessStartDate() {
		return businessStartDate;
	}

	public void setBusinessStartDate(java.sql.Date businessStartDate) {
		this.businessStartDate = businessStartDate;
	}

	public java.sql.Date getBusinessEndDate() {
		return businessEndDate;
	}

	public void setBusinessEndDate(java.sql.Date businessEndDate) {
		this.businessEndDate = businessEndDate;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getBoardName() {
		return boardName;
	}

	public void setBoardName(String boardName) {
		this.boardName = boardName;
	}

	public String getBoardNo() {
		return boardNo;
	}

	public void setBoardNo(String boardNo) {
		this.boardNo = boardNo;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public java.sql.Date getWriteDate() {
		return writeDate;
	}

	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}

	@Override
	public String toString() {
		return "BusinessOpportunityDTO [businessOpportunityNo=" + businessOpportunityNo + ", businessOpportunityName="
				+ businessOpportunityName + ", clientNo=" + clientNo + ", clientName=" + clientName
				+ ", clientCompanyNo=" + clientCompanyNo + ", clientCompanyName=" + clientCompanyName
				+ ", businessStartDate=" + businessStartDate + ", businessEndDate=" + businessEndDate + ", userNo="
				+ userNo + ", name=" + name + ", color=" + color + ", boardName=" + boardName + ", boardNo=" + boardNo
				+ ", division=" + division + ", writeDate=" + writeDate + "]";
	}

}
