package com.codefactory.mnm.board.model.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.codefactory.mnm.board.model.dto.BoardDTO;
import com.codefactory.mnm.board.model.dto.BusinessOpportunityDTO;
import com.codefactory.mnm.board.model.dto.ClientCompanyDTO;
import com.codefactory.mnm.board.model.dto.ClientCompanyManagerDTO;
import com.codefactory.mnm.board.model.dto.ClientDTO;
import com.codefactory.mnm.board.model.dto.CountDTO;

@Mapper
public interface BoardDAO {

	List<ClientCompanyDTO> selectClientCompany();											//고객사 리스트 조회

	List<ClientCompanyManagerDTO> selectClientCompanyManager();								//고객사 담당자 리스트 조회

	List<CountDTO> selectClientCompanyCount();												//고객사 별 보드 갯수 조회

	List<BoardDTO> selectBoardByClientCompany();											//고객사 보드 리스트 조회

	int copyBoardByClientCompany(ClientCompanyDTO clientCompany);							//보드별 고객사 등록

	int deleteBoardByClientCompany(ClientCompanyDTO clientCompany);							//보드별 고객사 삭제

	int insertBoardByClientCompany(String boardName);										//고객사 보드 등록

	int updateBoardName(ClientCompanyDTO clientCompany);									//보드명 수정

	int updateBoardColor(ClientCompanyDTO clientCompany);									//보드 색상 수정

	int deleteBoard(String boardNo);														//보드 삭제

	List<ClientDTO> selectClient();															//고객 리스트 조회

	List<CountDTO> selectClientCount();														//고객 별 보드 갯수 조회

	List<BoardDTO> selectBoardByClient();													//고객 보드 리스트 조회

	int copyBoardByClient(ClientDTO client);												//보드별 고객 등록 

	int deleteBoardByClient(ClientDTO client);												//보드별 고객 삭제
	
	int insertBoardByClient(String boardName);												//고객 보드 등록

	List<BusinessOpportunityDTO> selectBusinessOpportunity();								//영업기회 리스트 조회

	List<CountDTO> selectBusinessOpportunityCount();										//영업기회 별 보드 갯수 조회

	List<BoardDTO> selectBoardByBusinessOpportunity();										//영업기회 보드 리스트 조회
	
	int copyBoardByBusinessOpportunity(BusinessOpportunityDTO businessOpportunity);			//보드별 영업기회 등록

	int deleteBoardByBusinessOpportunity(BusinessOpportunityDTO businessOpportunity);		//보드별 영업기회 삭제

	int insertBoardByBusinessOpportunity(String boardName);									//영업기회 보드 등록


	
	
}
