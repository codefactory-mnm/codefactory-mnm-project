package com.codefactory.mnm.board.model.dto;

public class BoardDTO implements java.io.Serializable {

	private String boardNo;			//보드번호
	private String boardName;		//보드이름
	private String color;			//색상
	private String division;		//구분
	
	public BoardDTO() {}

	public BoardDTO(String boardNo, String boardName, String color, String division) {
		super();
		this.boardNo = boardNo;
		this.boardName = boardName;
		this.color = color;
		this.division = division;
	}

	public String getBoardNo() {
		return boardNo;
	}

	public void setBoardNo(String boardNo) {
		this.boardNo = boardNo;
	}

	public String getBoardName() {
		return boardName;
	}

	public void setBoardName(String boardName) {
		this.boardName = boardName;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	@Override
	public String toString() {
		return "BoardDTO [boardNo=" + boardNo + ", boardName=" + boardName + ", color=" + color + ", division="
				+ division + "]";
	}
	

}
