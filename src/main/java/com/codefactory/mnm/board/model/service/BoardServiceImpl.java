package com.codefactory.mnm.board.model.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.codefactory.mnm.board.model.dao.BoardDAO;
import com.codefactory.mnm.board.model.dto.BoardDTO;
import com.codefactory.mnm.board.model.dto.BusinessOpportunityDTO;
import com.codefactory.mnm.board.model.dto.ClientCompanyDTO;
import com.codefactory.mnm.board.model.dto.ClientCompanyManagerDTO;
import com.codefactory.mnm.board.model.dto.ClientDTO;
import com.codefactory.mnm.board.model.dto.CountDTO;

@Service
public class BoardServiceImpl implements BoardService {

	private BoardDAO mapper;
	
	@Autowired
	public BoardServiceImpl(BoardDAO mapper) {
		this.mapper = mapper;
	}
	
	/* 보드 별 고객사 리스트 조회 */
	@Override
	public Map<String, Object> selectClientCompany() {
		
		Map<String, Object> map = new HashMap<>();
		
		List<ClientCompanyDTO> clientCompany = mapper.selectClientCompany();			//고객사 리스트 조회
		List<ClientCompanyManagerDTO> manager = mapper.selectClientCompanyManager();	//고객사 담당자 리스트 조회
		List<CountDTO> clientCompanyCount = mapper.selectClientCompanyCount();			//고객사 별 보드 갯수 조회
		List<BoardDTO> board = mapper.selectBoardByClientCompany();						//고객사 보드 리스트 조회
		
		map.put("clientCompany", clientCompany);
		map.put("manager", manager);
		map.put("clientCompanyCount", clientCompanyCount);
		map.put("board", board);
		
		return map;
	}
	
	/* 고객사 보드명 조회 */
	@Override
	public List<BoardDTO> selectBoardByClientCompany() {
		
		return mapper.selectBoardByClientCompany();
	}
	
	/* 보드별 고객사 등록 */
	/* DB에 저장하는 과정이므로 트랜잭션 처리를 해줌 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int copyBoardByClientCompany(String boardNo, String clientCompanyNo) {
		
		ClientCompanyDTO clientCompany = new ClientCompanyDTO();
		clientCompany.setBoardNo(boardNo);
		clientCompany.setClientCompanyNo(clientCompanyNo);

		return mapper.copyBoardByClientCompany(clientCompany);
	}
	
	/*  보드별 고객사 삭제 */
	/* DB에 저장하는 과정이므로 트랜잭션 처리를 해줌 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int deleteBoardByClientCompany(String boardNo, String clientCompanyNo) {
		
		ClientCompanyDTO clientCompany = new ClientCompanyDTO();
		clientCompany.setBoardNo(boardNo);
		clientCompany.setClientCompanyNo(clientCompanyNo);
		
		return mapper.deleteBoardByClientCompany(clientCompany);

	}

	/* 고객사 보드 등록 */
	/* DB에 저장하는 과정이므로 트랜잭션 처리를 해줌 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertBoardByClientCompany(String boardName) {
		
		return mapper.insertBoardByClientCompany(boardName);
	}

	/* 보드명 수정 */
	/* DB에 저장하는 과정이므로 트랜잭션 처리를 해줌 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int updateBoardName(String boardNo, String boardName) {
		
		/* dto에 담아서 mapper에 전달 */
		ClientCompanyDTO clientCompany = new ClientCompanyDTO();
		clientCompany.setBoardNo(boardNo);
		clientCompany.setBoardName(boardName);
		
		return mapper.updateBoardName(clientCompany);
	}
	
	/* 보드 색상 수정 */
	/* DB에 저장하는 과정이므로 트랜잭션 처리를 해줌 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int updateBoardColor(String boardNo, String color) {
		
		ClientCompanyDTO clientCompany = new ClientCompanyDTO();
		clientCompany.setBoardNo(boardNo);
		clientCompany.setColor(color);
		
		return mapper.updateBoardColor(clientCompany);
	}
	
	/* 보드 삭제 */
	/* DB에 삭제하는 과정이므로 트랜잭션 처리를 해줌 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int deleteBoard(String boardNo) {
			
		return mapper.deleteBoard(boardNo);
	}

	/* 보드 별 고객 리스트 조회 */
	@Override
	public Map<String, Object> selectClient() {
		
		Map<String, Object> map = new HashMap<>();
		
		List<ClientDTO> client = mapper.selectClient();						//고객 리스트 조회
		List<CountDTO> clientCount = mapper.selectClientCount();			//고객 별 보드 갯수 조회
		List<BoardDTO> board = mapper.selectBoardByClient();				//고객 보드 리스트 조회
		
		map.put("client", client);
		map.put("clientCount", clientCount);
		map.put("board", board);
		
		return map;		
	}

	/* 고객 보드명 조회 */
	@Override
	public List<BoardDTO> selectBoardByClient() {
		
		return mapper.selectBoardByClient();
	}
	
	/* 보드별 고객 등록 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int copyBoardByClient(String boardNo, String clientNo) {
		
		ClientDTO client = new ClientDTO();
		client.setBoardNo(boardNo);
		client.setClientNo(clientNo);

		return mapper.copyBoardByClient(client);
	}
	
	/* 보드별 고객 삭제 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int deleteBoardByClient(String boardNo, String clientNo) {
		
		ClientDTO client = new ClientDTO();
		client.setBoardNo(boardNo);
		client.setClientNo(clientNo);

		return mapper.deleteBoardByClient(client);
	}
	
	/* 고객 보드 등록 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertBoardByClient(String boardName) {
		
		return mapper.insertBoardByClient(boardName);
	}

	/* 보드 별 영업기회 리스트 조회 */
	@Override
	public Map<String, Object> selectBusinessOpportunity() {
		
		Map<String, Object> map = new HashMap<>();
		
		List<BusinessOpportunityDTO> businessOpportunity = mapper.selectBusinessOpportunity();						//영업기회 리스트 조회
		List<CountDTO> businessOpportunityCount = mapper.selectBusinessOpportunityCount();							//영업기회 별 보드 갯수 조회
		List<BoardDTO> board = mapper.selectBoardByBusinessOpportunity();											//영업기회 보드 리스트 조회
		
		map.put("businessOpportunity", businessOpportunity);
		map.put("businessOpportunityCount", businessOpportunityCount);
		map.put("board", board);
		
		return map;	
	}

	/* 영업기회 보드명 조회  */
	@Override
	public List<BoardDTO> selectBoardByBusinessOpportunity() {
		
		return  mapper.selectBoardByBusinessOpportunity();
	}

	/* 보드별 영업기회 등록 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int copyBoardByBusinessOpportunity(String boardNo, String businessOpportunityNo) {
		
		BusinessOpportunityDTO businessOpportunity = new BusinessOpportunityDTO();
		businessOpportunity.setBoardNo(boardNo);
		businessOpportunity.setBusinessOpportunityNo(businessOpportunityNo);

		return mapper.copyBoardByBusinessOpportunity(businessOpportunity);
	}
	
	/* 보드별 영업기회 삭제 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int deleteBoardByBusinessOpportunity(String boardNo, String businessOpportunityNo) {
		
		BusinessOpportunityDTO businessOpportunity = new BusinessOpportunityDTO();
		businessOpportunity.setBoardNo(boardNo);
		businessOpportunity.setBusinessOpportunityNo(businessOpportunityNo);
	
		return mapper.deleteBoardByBusinessOpportunity(businessOpportunity);
	}

	/* 영업기회 보드 등록 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertBoardByBusinessOpportunity(String boardName) {
		
		return mapper.insertBoardByBusinessOpportunity(boardName);
	}



}
