package com.codefactory.mnm.board.model.service;

import java.util.List;
import java.util.Map;

import com.codefactory.mnm.board.model.dto.BoardDTO;

public interface BoardService {

	Map<String, Object> selectClientCompany();												//보드 별 고객사 리스트 조회

	List<BoardDTO> selectBoardByClientCompany();											//고객사 보드명 조회

	int copyBoardByClientCompany(String boardNo, String clientCompanyNo);					//보드별 고객사 등록

	int deleteBoardByClientCompany(String boardNo, String clientCompanyNo);					//보드별 고객사 삭제

	int insertBoardByClientCompany(String boardName);										//고객사 보드 등록

	int updateBoardName(String boardNo, String boardName);									//보드명 수정

	int updateBoardColor(String boardNo, String color);										//보드 색상 수정

	int deleteBoard(String boardNo);														//보드 삭제

	Map<String, Object> selectClient();														//보드 별 고객 리스트 조회

	List<BoardDTO> selectBoardByClient();													//고객 보드명 조회

	int copyBoardByClient(String boardNo, String clientNo);									//보드별 고객 등록
	
	int deleteBoardByClient(String boardNo, String clientNo);								//보드별 고객 삭제
	
	int insertBoardByClient(String boardName);												//고객 보드 등록

	Map<String, Object> selectBusinessOpportunity();										//보드 별 영업기회 리스트 조회

	List<BoardDTO> selectBoardByBusinessOpportunity();										//영업기회 보드명 조회 

	int copyBoardByBusinessOpportunity(String boardNo, String businessOpportunityNo);		//보드별 영업기회 등록

	int deleteBoardByBusinessOpportunity(String boardNo, String businessOpportunityNo);		//보드별 영업기회 삭제

	int insertBoardByBusinessOpportunity(String boardName);									//영업기회 보드 등록



}
