package com.codefactory.mnm.board.model.dto;

public class CountDTO implements java.io.Serializable {

	private String boardNo;			//보드번호
	private Integer boardCount;		//보드갯수
	
	public CountDTO() {}

	public CountDTO(String boardNo, Integer boardCount) {
		super();
		this.boardNo = boardNo;
		this.boardCount = boardCount;
	}

	public String getBoardNo() {
		return boardNo;
	}

	public void setBoardNo(String boardNo) {
		this.boardNo = boardNo;
	}

	public Integer getBoardCount() {
		return boardCount;
	}

	public void setBoardCount(Integer boardCount) {
		this.boardCount = boardCount;
	}

	@Override
	public String toString() {
		return "CountDTO [boardNo=" + boardNo + ", boardCount=" + boardCount + "]";
	}
	
	
}
