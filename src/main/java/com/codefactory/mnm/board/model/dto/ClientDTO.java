package com.codefactory.mnm.board.model.dto;

import java.sql.Date;

public class ClientDTO implements java.io.Serializable {
		
	private String clientNo;				//고객번호
	private String clientName;				//고객명
	private String clientCompanyNo;			//고객사번호	
	private String clientCompanyName;		//고객사명
	private String phone;					//휴대폰번호
	private String landlineTel;				//유선번호
	private String userNo;					//담당자번호
	private String name;					//담당자이름		
	private String color;					//색상
	private String boardName;				//보드명
	private String boardNo;					//보드번호
	private String division;				//구분
	private java.sql.Date writeDate;		//작성일자
	private String email;					//이메일
	
	public ClientDTO() {}

	public ClientDTO(String clientNo, String clientName, String clientCompanyNo, String clientCompanyName, String phone,
			String landlineTel, String userNo, String name, String color, String boardName, String boardNo,
			String division, Date writeDate, String email) {
		super();
		this.clientNo = clientNo;
		this.clientName = clientName;
		this.clientCompanyNo = clientCompanyNo;
		this.clientCompanyName = clientCompanyName;
		this.phone = phone;
		this.landlineTel = landlineTel;
		this.userNo = userNo;
		this.name = name;
		this.color = color;
		this.boardName = boardName;
		this.boardNo = boardNo;
		this.division = division;
		this.writeDate = writeDate;
		this.email = email;
	}

	public String getClientNo() {
		return clientNo;
	}

	public void setClientNo(String clientNo) {
		this.clientNo = clientNo;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getClientCompanyNo() {
		return clientCompanyNo;
	}

	public void setClientCompanyNo(String clientCompanyNo) {
		this.clientCompanyNo = clientCompanyNo;
	}

	public String getClientCompanyName() {
		return clientCompanyName;
	}

	public void setClientCompanyName(String clientCompanyName) {
		this.clientCompanyName = clientCompanyName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getLandlineTel() {
		return landlineTel;
	}

	public void setLandlineTel(String landlineTel) {
		this.landlineTel = landlineTel;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getBoardName() {
		return boardName;
	}

	public void setBoardName(String boardName) {
		this.boardName = boardName;
	}

	public String getBoardNo() {
		return boardNo;
	}

	public void setBoardNo(String boardNo) {
		this.boardNo = boardNo;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public java.sql.Date getWriteDate() {
		return writeDate;
	}

	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "ClientDTO [clientNo=" + clientNo + ", clientName=" + clientName + ", clientCompanyNo=" + clientCompanyNo
				+ ", clientCompanyName=" + clientCompanyName + ", phone=" + phone + ", landlineTel=" + landlineTel
				+ ", userNo=" + userNo + ", name=" + name + ", color=" + color + ", boardName=" + boardName
				+ ", boardNo=" + boardNo + ", division=" + division + ", writeDate=" + writeDate + ", email=" + email
				+ "]";
	}


}
