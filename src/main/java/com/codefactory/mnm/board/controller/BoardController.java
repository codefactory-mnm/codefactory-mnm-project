package com.codefactory.mnm.board.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.codefactory.mnm.board.model.dto.BoardDTO;
import com.codefactory.mnm.board.model.dto.BusinessOpportunityDTO;
import com.codefactory.mnm.board.model.dto.ClientCompanyDTO;
import com.codefactory.mnm.board.model.dto.ClientCompanyManagerDTO;
import com.codefactory.mnm.board.model.dto.ClientDTO;
import com.codefactory.mnm.board.model.dto.CountDTO;
import com.codefactory.mnm.board.model.service.BoardService;

@Controller
@RequestMapping("/board")
public class BoardController {

	private BoardService boardService;
	
	@Autowired
	public BoardController(BoardService boardService) {
		this.boardService = boardService;
	}
	
	/* 보드 별 고객사 리스트 조회 */
	@GetMapping("clientCompany/list")
	public ModelAndView selectClientCompanyList(ModelAndView mv) {
		
		Map<String, Object> map = boardService.selectClientCompany();
		
		List<ClientCompanyDTO> clientCompany = (List<ClientCompanyDTO>) map.get("clientCompany");		//고객사 리스트 조회
		List<ClientCompanyManagerDTO> manager = (List<ClientCompanyManagerDTO>) map.get("manager");		//고객사 담당자 리스트 조회
		List<CountDTO> clientCompanyCount = (List<CountDTO>) map.get("clientCompanyCount");				//고객사 별 보드 갯수 조회
		List<BoardDTO> board = (List<BoardDTO>) map.get("board");										//보드 리스트 조회
		
		mv.addObject("clientCompany", clientCompany);
		mv.addObject("manager", manager);
		mv.addObject("clientCompanyCount", clientCompanyCount);
		mv.addObject("board", board);
		mv.setViewName("board/clientCompany");
		
		return mv;
	}
	
	/* 고객사 보드명 조회 */
	@GetMapping(value="clientCompany/boardName", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<BoardDTO> selectBoardByClientCompany() {

		return boardService.selectBoardByClientCompany();
	}
	
	/* 보드별 고객사 등록 */
	@PostMapping(value="clientCompany/copy", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int copyBoardByClientCompany(String boardNo, String clientCompanyNo) {

		return boardService.copyBoardByClientCompany(boardNo, clientCompanyNo);
	}
	
	/* 보드별 고객사 삭제 */
	@PostMapping(value="clientCompany/delete", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int deleteBoardByClientCompany(String boardNo, String clientCompanyNo) {
		
		return boardService.deleteBoardByClientCompany(boardNo, clientCompanyNo);
	}
	
	/* 고객사 보드 등록 */
	@PostMapping(value="clientCompany/insert", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int insertBoardByClientCompany(String boardName) {

		return boardService.insertBoardByClientCompany(boardName);
	}
	
	/* 보드명 수정 */
	@PostMapping(value="updateName", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int updateBoardName(String boardNo, String boardName) {

		return boardService.updateBoardName(boardNo, boardName);
	}
	
	/* 보드 색상 수정 */
	@PostMapping(value="updateColor", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int updateBoardColor(String boardNo, String color) {

		return boardService.updateBoardColor(boardNo, color);
	}
	
	/* 보드 삭제 */
	@PostMapping(value="deleteBoard", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int deleteBoard(String boardNo) {

		return boardService.deleteBoard(boardNo);
	}
	
	/* 보드 별 고객 리스트 조회 */
	@GetMapping("client/list")
	public ModelAndView selectClientList(ModelAndView mv) {
		
		Map<String, Object> map = boardService.selectClient();
		
		List<ClientDTO> client= (List<ClientDTO>) map.get("client");						//고객 리스트 조회
		List<CountDTO> clientCount = (List<CountDTO>) map.get("clientCount");				//고객 별 보드 갯수 조회
		List<BoardDTO> board = (List<BoardDTO>) map.get("board");							//보드 리스트 조회
		
		mv.addObject("client", client);
		mv.addObject("clientCount", clientCount);
		mv.addObject("board", board);
		mv.setViewName("board/client");
		
		return mv;
	}
	
	/* 고객 보드명 조회 */
	@GetMapping(value="client/boardName", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<BoardDTO> selectBoardByClient() {

		return boardService.selectBoardByClient();
	}
	
	/* 보드별 고객 등록 */
	@PostMapping(value="client/copy", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int copyBoardByClient(String boardNo, String clientNo) {
		
		return boardService.copyBoardByClient(boardNo, clientNo);
	}
	
	/* 보드별 고객 삭제 */
	@PostMapping(value="client/delete", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int deleteBoardByClient(String boardNo, String clientNo) {

		return boardService.deleteBoardByClient(boardNo, clientNo);
	}
	
	/* 고객 보드 등록 */
	@PostMapping(value="client/insert", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int insertBoardByClient(String boardName) {

		return boardService.insertBoardByClient(boardName);
	}
	
	/* 보드 별 영업기회 리스트 조회 */
	@GetMapping("businessOpportunity/list")
	public ModelAndView selectBusinessOpportunityList(ModelAndView mv) {
		
		Map<String, Object> map = boardService.selectBusinessOpportunity();
		
		List<BusinessOpportunityDTO> businessOpportunity= (List<BusinessOpportunityDTO>) map.get("businessOpportunity");		//영업기회 리스트 조회
		List<CountDTO> businessOpportunityCount = (List<CountDTO>) map.get("businessOpportunityCount");							//영업기회 별 보드 갯수 조회
		List<BoardDTO> board = (List<BoardDTO>) map.get("board");																//보드 리스트 조회
		
		mv.addObject("businessOpportunity", businessOpportunity);
		mv.addObject("businessOpportunityCount", businessOpportunityCount);
		mv.addObject("board", board);
		mv.setViewName("board/businessOpportunity");
		
		return mv;
	}
	
	/* 영업기회 보드명 조회 */
	@GetMapping(value="businessOpportunity/boardName", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<BoardDTO> selectBoardByBusinessOpportunity() {

		return boardService.selectBoardByBusinessOpportunity();
	}
	
	/* 보드별 영업기회 등록 */
	@PostMapping(value="businessOpportunity/copy", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int copyBoardByBusinessOpportunity(String boardNo, String businessOpportunityNo) {
		
		return boardService.copyBoardByBusinessOpportunity(boardNo, businessOpportunityNo);
	}
	
	/* 보드별 영업기회 삭제 */
	@PostMapping(value="businessOpportunity/delete", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int deleteBoardByBusinessOpportunity(String boardNo, String businessOpportunityNo) {
		
		return boardService.deleteBoardByBusinessOpportunity(boardNo, businessOpportunityNo);
	}
	
	/* 영업기회 보드 등록 */
	@PostMapping(value="businessOpportunity/insert", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int insertBoardByBusinessOpportunity(String boardName) {

		return boardService.insertBoardByBusinessOpportunity(boardName);
	}
}
