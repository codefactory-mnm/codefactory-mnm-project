package com.codefactory.mnm.companyinfo.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.codefactory.mnm.companyinfo.model.dto.CompanyInfoDTO;
import com.codefactory.mnm.companyinfo.model.dto.UpdateCompanyInfoDTO;
import com.codefactory.mnm.companyinfo.model.service.CompanyInfoService;

@Controller
@RequestMapping("companyInfo")
public class CompanyInfoController {
	
	private CompanyInfoService companyInfoService;
	
	@Autowired
	public CompanyInfoController(CompanyInfoService companyInfoService) {
		this.companyInfoService = companyInfoService;
	}
	
	/* 회사 정보 조회 */
	@GetMapping
	public ModelAndView companyInfo(ModelAndView mv, Principal pcp) {
		
		String userId = pcp.getName();
		
		CompanyInfoDTO companyInfo = companyInfoService.companyInfo(userId);
		
		mv.addObject("companyInfo", companyInfo);
		mv.setViewName("companyInfo/companyInfo");
		
		return mv;
	}
	
	/* 회사 정보 수정 */
	@PostMapping("update")
	public ModelAndView UpdateCompanyInfo(ModelAndView mv, Principal pcp, UpdateCompanyInfoDTO companyInfo, RedirectAttributes rttr) {
		
		String userId = pcp.getName();
		
		int result = companyInfoService.updateCompanyInfo(userId, companyInfo);
		
		if(result > 0) {
			rttr.addFlashAttribute("successMessage", "회사 정보가 정상적으로 저장되었습니다.");
		} else {
			rttr.addFlashAttribute("failMessage", "회사 정보가 저장되지 않았습니다. 다시 시도해주세요.");
		}
		
		mv.setViewName("redirect:/companyInfo");
		
		return mv;
		
	}
	
}









