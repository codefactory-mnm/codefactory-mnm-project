package com.codefactory.mnm.companyinfo.model.service;

import com.codefactory.mnm.companyinfo.model.dto.CompanyInfoDTO;
import com.codefactory.mnm.companyinfo.model.dto.UpdateCompanyInfoDTO;

public interface CompanyInfoService {

	public CompanyInfoDTO companyInfo(String userId);								//회사정보 조회

	public int updateCompanyInfo(String userId, UpdateCompanyInfoDTO companyInfo);	//회사정보 수정
	
}
