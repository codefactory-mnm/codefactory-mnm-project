package com.codefactory.mnm.companyinfo.model.dto;

public class CompanyInfoDTO {
	
	private String companyNo;			//회사번호
	private String companyName;			//회사명
	private String companyId;			//회사아이디
	private String corporationNo;		//법인번호	
	private String businessNo;			//사업자번호
	private String ceo;					//대표이사
	private String tel;					//전화번호
	private String fax;					//팩스번호
	private String zipCode;				//우편번호
	private String address;				//주소
	private String detailedAddress;		//상세주소
	
	public CompanyInfoDTO() {}
	
	public CompanyInfoDTO(String companyNo, String companyName, String companyId, String corporationNo,
			String businessNo, String ceo, String tel, String fax, String zipCode, String address,
			String detailedAddress) {
		super();
		this.companyNo = companyNo;
		this.companyName = companyName;
		this.companyId = companyId;
		this.corporationNo = corporationNo;
		this.businessNo = businessNo;
		this.ceo = ceo;
		this.tel = tel;
		this.fax = fax;
		this.zipCode = zipCode;
		this.address = address;
		this.detailedAddress = detailedAddress;
	}

	public String getCompanyNo() {
		return companyNo;
	}

	public void setCompanyNo(String companyNo) {
		this.companyNo = companyNo;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getCorporationNo() {
		return corporationNo;
	}

	public void setCorporationNo(String corporationNo) {
		this.corporationNo = corporationNo;
	}

	public String getBusinessNo() {
		return businessNo;
	}

	public void setBusinessNo(String businessNo) {
		this.businessNo = businessNo;
	}

	public String getCeo() {
		return ceo;
	}

	public void setCeo(String ceo) {
		this.ceo = ceo;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDetailedAddress() {
		return detailedAddress;
	}

	public void setDetailedAddress(String detailedAddress) {
		this.detailedAddress = detailedAddress;
	}

	@Override
	public String toString() {
		return "CompanyInfoDTO [companyNo=" + companyNo + ", companyName=" + companyName + ", companyId=" + companyId
				+ ", corporationNo=" + corporationNo + ", businessNo=" + businessNo + ", ceo=" + ceo + ", tel=" + tel
				+ ", fax=" + fax + ", zipCode=" + zipCode + ", address=" + address + ", detailedAddress="
				+ detailedAddress + "]";
	}

}
