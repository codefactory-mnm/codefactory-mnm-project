package com.codefactory.mnm.companyinfo.model.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.codefactory.mnm.companyinfo.model.dto.CompanyInfoDTO;
import com.codefactory.mnm.companyinfo.model.dto.UpdateCompanyInfoDTO;

@Mapper
public interface CompanyInfoDAO {
	
	CompanyInfoDTO companyInfoById(String userId);												//회사정보 조회

	int updateCompanyInfo(@Param("userId") String userId, UpdateCompanyInfoDTO companyInfo);	//회사정보 수정
	
}
