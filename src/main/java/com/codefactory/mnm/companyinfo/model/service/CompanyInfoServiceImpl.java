package com.codefactory.mnm.companyinfo.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.codefactory.mnm.companyinfo.model.dao.CompanyInfoDAO;
import com.codefactory.mnm.companyinfo.model.dto.CompanyInfoDTO;
import com.codefactory.mnm.companyinfo.model.dto.UpdateCompanyInfoDTO;

@Service
public class CompanyInfoServiceImpl implements CompanyInfoService {
	
	private CompanyInfoDAO companyInfoDAO;
	
	@Autowired
	public CompanyInfoServiceImpl(CompanyInfoDAO companyInfoDAO) {
		this.companyInfoDAO = companyInfoDAO;
	}

	/* 회사정보 조회 */
	@Override
	public CompanyInfoDTO companyInfo(String userId) {
		
		CompanyInfoDTO companyInfo = companyInfoDAO.companyInfoById(userId);
		
		return companyInfo;
	}

	/* 회사정보 수정 */
	@Override
	public int updateCompanyInfo(String userId, UpdateCompanyInfoDTO companyInfo) {
		
		return companyInfoDAO.updateCompanyInfo(userId, companyInfo);
	}

}
