package com.codefactory.mnm.client.model.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.codefactory.mnm.client.model.dao.ClientDAO;
import com.codefactory.mnm.client.model.dto.BusinessActivityDTO;
import com.codefactory.mnm.client.model.dto.ClientAttachmentDTO;
import com.codefactory.mnm.client.model.dto.ClientCollectionDTO;
import com.codefactory.mnm.client.model.dto.ClientCompanyDTO;
import com.codefactory.mnm.client.model.dto.ClientCountDTO;
import com.codefactory.mnm.client.model.dto.ClientDTO;
import com.codefactory.mnm.client.model.dto.ClientListDTO;
import com.codefactory.mnm.client.model.dto.ClientSearchDTO;
import com.codefactory.mnm.client.model.dto.OpinionDTO;
import com.codefactory.mnm.client.model.dto.UsersDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyAttachmentDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyManagerDTO;
import com.codefactory.mnm.common.Pagenation;
import com.codefactory.mnm.common.SelectCriteria;

@Service
public class ClientServiceImpl implements ClientService {

	private final ClientDAO mapper;
	
	@Autowired
	public ClientServiceImpl(ClientDAO mapper) {
		this.mapper = mapper;
	}
	
	/* 고객 리스트 조회 */
	@Override
	public Map<String, Object> selectClientList(ClientSearchDTO clientSearch) {
		
		int pageNo = 1;		
		
		if(pageNo <= 0) {
			pageNo = 1;
		}
		
		int limit = 10;		
		int buttonAmount = 5;
		
		/* 전체 게시물 수, 검색조건이 있다면 검색조건에 맞는 게시물 수 */
		int totalCount = mapper.selectTotalCount(clientSearch); 
		
		/* view에서 전달받은 요청페이지*/
		String currentPage = clientSearch.getCurrentPage();
		
		/* view에서 전달받은 요청페이지를 pageNo에 저장 */
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		/* 설정한 변수에 따른 페이징조건 */
		SelectCriteria selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount);
		
		clientSearch.setSelectCriteria(selectCriteria);		
		
		List<ClientListDTO> clientList = mapper.selectClientList(clientSearch);					//검색조건에 따른 고객 리스트 조회			
		List<ClientCountDTO> clientCount = mapper.selectClientCount();							//고객의 영업활동 횟수 리스트 조회
		
		Map<String, Object> map = new HashMap<>();
		
		map.put("clientList", clientList);
		map.put("clientCount", clientCount);
		map.put("selectCriteria", selectCriteria);
		map.put("totalCount", totalCount);
		
		return map;
	}
	
	/* 담당자 리스트 조회 */
	@Override
	public List<UsersDTO> selectUserList() {
		
		return mapper.selectUserList();
	}
	
	/* 영업활동 리스트 조회 */
	@Override
	public List<BusinessActivityDTO> selectBusinessAcitivityList(String clientNo) {
		
		return mapper.selectBusinessAcitivityList(clientNo);
	}
	
	/* 고객 조회 */
	@Override
	public Map<String, Object> selectClient(String clientNo) {

		Map<String, Object> map = new HashMap<>();
		
		ClientDTO client = mapper.selectClient(clientNo);											//고객 조회									
		List<ClientAttachmentDTO> clientAttachmentList = mapper.selectClientAttachment(clientNo);	//첨부파일 리스트 조회

		map.put("client", client);
		map.put("clientAttachmentList", clientAttachmentList);
		
		return map;		
	}
	
	/* 의견 리스트 조회 */
	@Override
	public List<OpinionDTO> selectOpinionList(String clientNo) {

		return mapper.selectOpinionList(clientNo);
	}
	
	/* 의견 등록 */
	/* DB에 저장하는 과정이므로 트랜잭션 처리를 해줌 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertOpinion(String clientNo, String opinionContent, String id) {
		
		/* 로그인된 사용자의 번호, 이름 조회하여 저장 */
		OpinionDTO opinion = mapper.selectUserNoAndName(id);
		
		/* 고객사번호, 의견내용, 아이디를 저장 */
		opinion.setClientNo(clientNo);
		opinion.setOpinionContent(opinionContent);
		opinion.setId(id);
		
		return mapper.insertOpinion(opinion);
	}
	
	/* 의견 삭제 */
	/* DB에 저장하는 과정이므로 트랜잭션 처리를 해줌 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int deleteOpinion(String opinionNo) {
		
		return mapper.deleteOpinion(opinionNo);
	}
	
	/* 첨부파일 조회 */
	@Override
	public ClientAttachmentDTO selectFile(String fileNo) {
		
		return mapper.selectFile(fileNo);	
	}

	/* 고객사 리스트 조회 */
	@Override
	public List<ClientCompanyDTO> selectClientCompanyList() {
		
		return mapper.selectClientCompanyList();
	}
	
	
	/* 고객 등록 */
	/* DB에 저장하는 과정이므로 트랜잭션 처리를 해줌 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertClient(ClientCollectionDTO clientCollection) {

		ClientDTO client = clientCollection.getClient();
		List<ClientAttachmentDTO> files = clientCollection.getFiles();
		
		int result1 = mapper.insertClient(client);	//고객 등록 			
		
		int result2 = 0;	
		if(files != null) {
			for(ClientAttachmentDTO clientAttachment : files) {
				result2 += mapper.insertClientAttachment(clientAttachment);	//첨부파일 등록
			}	
		}
		
		int result3 = mapper.insertBoard();	//고객별 보드 등록
		
		/* 모든 조건이 부합할 경우 result를 1로 반환 */
		int result = 0;
		if(result1 == 1  && result3 == 1 &&  (files == null || result2 == files.size())) {
			result = 1;
		}
		
		return result;
	}

	/* 첨부파일 삭제 */
	/* DB에서 삭제하는 과정이므로 트랜잭션 처리를 해줌 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public Map<String, Object> deleteFile(String fileNo) {
	
		Map<String, Object> map = new HashMap<>();
		
		ClientAttachmentDTO clientAttachment = mapper.selectFile(fileNo);					//첨부파일정보 조회				
		int result = mapper.deleteFile(fileNo);												//첨부파일 DB에서 삭제 결과

		map.put("clientAttachment", clientAttachment);
		map.put("result", result);
		
		return map;
	}

	/* 고객 수정 */
	/* DB에 저장하는 과정이므로 트랜잭션 처리를 해줌 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int updateClient(ClientCollectionDTO clientCollection) {

		ClientDTO client = clientCollection.getClient();
		List<ClientAttachmentDTO> files = clientCollection.getFiles();
		
		String clientNo = client.getClientNo();
		
		int result1 = mapper.updateClient(client); 	//고객 수정
		
		int result2 = 0;	
		if(files != null) {
			for(ClientAttachmentDTO clientAttachment : files) {
				clientAttachment.setClientNo(clientNo);
				result2 += mapper.insertClientAttachmentByUpdate(clientAttachment);	//첨부파일 등록
			}	
		}
		
		/* 모든 조건이 부합할 경우 result를 1로 반환 */
		int result = 0;
		if(result1 == 1  &&  (files == null || result2 == files.size())) {
			result = 1;
		}
		
		return result;
	}

	/* 고객 삭제 */
	/* DB에 저장하는 과정이므로 트랜잭션 처리를 해줌 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int deleteClient(String clientNo) {
		
		return mapper.deleteClient(clientNo); 
	}

}
