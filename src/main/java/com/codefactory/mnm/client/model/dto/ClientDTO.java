package com.codefactory.mnm.client.model.dto;

import java.sql.Date;

public class ClientDTO implements java.io.Serializable {
	
	private String clientNo;					//고객번호
	private String clientName;					//고객명
	private String dept;						//부서
	private String job;							//직책
	private String phone;						//휴대폰번호
	private String landlineTel;					//유선전화번호
	private String email;						//이메일
	private String keyMan;						//keyMan
	private String note;						//비고
	private String userNo;						//담당자번호
	private String name;						//담당자이름
	private java.sql.Date writeDate;			//작성일자
	private String grade;						//등급
	private String delYn;						//삭제여부
	private String clientCompanyNo;				//고객사번호
	private String clientCompanyName;			//고객서명
	
	public ClientDTO() {}

	public ClientDTO(String clientNo, String clientName, String dept, String job, String phone, String landlineTel,
			String email, String keyMan, String note, String userNo, String name, Date writeDate, String grade,
			String delYn, String clientCompanyNo, String clientCompanyName) {
		super();
		this.clientNo = clientNo;
		this.clientName = clientName;
		this.dept = dept;
		this.job = job;
		this.phone = phone;
		this.landlineTel = landlineTel;
		this.email = email;
		this.keyMan = keyMan;
		this.note = note;
		this.userNo = userNo;
		this.name = name;
		this.writeDate = writeDate;
		this.grade = grade;
		this.delYn = delYn;
		this.clientCompanyNo = clientCompanyNo;
		this.clientCompanyName = clientCompanyName;
	}

	public String getClientNo() {
		return clientNo;
	}

	public void setClientNo(String clientNo) {
		this.clientNo = clientNo;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getLandlineTel() {
		return landlineTel;
	}

	public void setLandlineTel(String landlineTel) {
		this.landlineTel = landlineTel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getKeyMan() {
		return keyMan;
	}

	public void setKeyMan(String keyMan) {
		this.keyMan = keyMan;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public java.sql.Date getWriteDate() {
		return writeDate;
	}

	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getDelYn() {
		return delYn;
	}

	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}

	public String getClientCompanyNo() {
		return clientCompanyNo;
	}

	public void setClientCompanyNo(String clientCompanyNo) {
		this.clientCompanyNo = clientCompanyNo;
	}

	public String getClientCompanyName() {
		return clientCompanyName;
	}

	public void setClientCompanyName(String clientCompanyName) {
		this.clientCompanyName = clientCompanyName;
	}

	@Override
	public String toString() {
		return "ClientDTO [clientNo=" + clientNo + ", clientName=" + clientName + ", dept=" + dept + ", job=" + job
				+ ", phone=" + phone + ", landlineTel=" + landlineTel + ", email=" + email + ", keyMan=" + keyMan
				+ ", note=" + note + ", userNo=" + userNo + ", name=" + name + ", writeDate=" + writeDate + ", grade="
				+ grade + ", delYn=" + delYn + ", clientCompanyNo=" + clientCompanyNo + ", clientCompanyName="
				+ clientCompanyName + "]";
	}

	
		
}
