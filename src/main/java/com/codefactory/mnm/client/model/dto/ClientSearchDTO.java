package com.codefactory.mnm.client.model.dto;

import com.codefactory.mnm.common.SelectCriteria;

public class ClientSearchDTO implements java.io.Serializable {

	private String clientName;					//고객명
	private String clientCompanyName;			//고객사명
	private String phone;						//핸드폰번호
	private String email;						//이메일
	private String userNo;						//담당자번호
	private String currentPage;					//현재페이지
	private SelectCriteria selectCriteria;		//페이징처리
	
	public ClientSearchDTO() {}

	public ClientSearchDTO(String clientName, String clientCompanyName, String phone, String email, String userNo,
			String currentPage, SelectCriteria selectCriteria) {
		super();
		this.clientName = clientName;
		this.clientCompanyName = clientCompanyName;
		this.phone = phone;
		this.email = email;
		this.userNo = userNo;
		this.currentPage = currentPage;
		this.selectCriteria = selectCriteria;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getClientCompanyName() {
		return clientCompanyName;
	}

	public void setClientCompanyName(String clientCompanyName) {
		this.clientCompanyName = clientCompanyName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(String currentPage) {
		this.currentPage = currentPage;
	}

	public SelectCriteria getSelectCriteria() {
		return selectCriteria;
	}

	public void setSelectCriteria(SelectCriteria selectCriteria) {
		this.selectCriteria = selectCriteria;
	}

	@Override
	public String toString() {
		return "ClientSearchDTO [clientName=" + clientName + ", clientCompanyName=" + clientCompanyName + ", phone="
				+ phone + ", email=" + email + ", userNo=" + userNo + ", currentPage=" + currentPage
				+ ", selectCriteria=" + selectCriteria + "]";
	}

	

	

}
