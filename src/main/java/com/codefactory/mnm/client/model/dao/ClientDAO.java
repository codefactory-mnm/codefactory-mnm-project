package com.codefactory.mnm.client.model.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.codefactory.mnm.client.model.dto.BusinessActivityDTO;
import com.codefactory.mnm.client.model.dto.ClientAttachmentDTO;
import com.codefactory.mnm.client.model.dto.ClientCompanyDTO;
import com.codefactory.mnm.client.model.dto.ClientCountDTO;
import com.codefactory.mnm.client.model.dto.ClientDTO;
import com.codefactory.mnm.client.model.dto.ClientListDTO;
import com.codefactory.mnm.client.model.dto.ClientSearchDTO;
import com.codefactory.mnm.client.model.dto.OpinionDTO;
import com.codefactory.mnm.client.model.dto.UsersDTO;

@Mapper
public interface ClientDAO {
		
	public int selectTotalCount(ClientSearchDTO clientSearch);							//게시물 수 조회
	
	public List<ClientListDTO> selectClientList(ClientSearchDTO clientSearch);			//고객 리스트 조회
	
	public List<ClientCountDTO> selectClientCount();									//고객의 영업활동 횟수 리스트
	
	public List<UsersDTO> selectUserList();												//담당자 리스트 조회
	
	public List<BusinessActivityDTO> selectBusinessAcitivityList(String clientNo);		//영업활동 리스트 조회 
	
	public List<ClientCompanyDTO> selectClientCompanyList();							//고객사 리스트 조회

	public ClientDTO selectClient(String clientNo);										//고객 조회
	
	public List<ClientAttachmentDTO> selectClientAttachment(String clientNo);			//첨부파일 리스트 조회
	
	public List<OpinionDTO> selectOpinionList(String clientNo);							//의견 리스트 조회
	
	public OpinionDTO selectUserNoAndName(String id);									//로그인된 사용자의 번호, 이름 조회
	
	public int insertOpinion(OpinionDTO opinion);										//의견 등록
	
	public int deleteOpinion(String opinionNo);											//의견 삭제
	
	public ClientAttachmentDTO selectFile(String fileNo);								//첨부파일 조회
	
	public int insertClient(ClientDTO client);											//고객 등록
	
	public int insertClientAttachment(ClientAttachmentDTO clientAttachment); 			//첨부파일 등록

	public int insertBoard();															//고객별 보드 등록
	
	public int deleteFile(String fileNo);												//파일 삭제

	public int updateClient(ClientDTO client);											//고객 수정

	public int insertClientAttachmentByUpdate(ClientAttachmentDTO clientAttachment);	//고객 수정 시 첨부파일 등록

	public int deleteClient(String clientNo);											//고객 삭제









}
