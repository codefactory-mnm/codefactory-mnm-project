package com.codefactory.mnm.client.model.dto;

import java.sql.Date;

public class ClientListDTO implements java.io.Serializable{
	
	private String clientNo;				//고객번호
	private String clientName;				//고객명
	private String dept;					//부서
	private String job;						//직책
	private String phone;					//핸드폰번호
	private String landlineTel;				//유선번호
	private String email;					//이메일
	private String userNo;					//담당자번호
	private String name;					//담당자이름
	private String clientCompanyNo;			//고객사번호
	private String clientCompanyName;		//고객사명
	private java.sql.Date writeDate;		//작성일자
	
	public ClientListDTO() {}

	public ClientListDTO(String clientNo, String clientName, String dept, String job, String phone, String landlineTel,
			String email, String userNo, String name, String clientCompanyNo, String clientCompanyName,
			Date writeDate) {
		super();
		this.clientNo = clientNo;
		this.clientName = clientName;
		this.dept = dept;
		this.job = job;
		this.phone = phone;
		this.landlineTel = landlineTel;
		this.email = email;
		this.userNo = userNo;
		this.name = name;
		this.clientCompanyNo = clientCompanyNo;
		this.clientCompanyName = clientCompanyName;
		this.writeDate = writeDate;
	}

	public String getClientNo() {
		return clientNo;
	}

	public void setClientNo(String clientNo) {
		this.clientNo = clientNo;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getLandlineTel() {
		return landlineTel;
	}

	public void setLandlineTel(String landlineTel) {
		this.landlineTel = landlineTel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClientCompanyNo() {
		return clientCompanyNo;
	}

	public void setClientCompanyNo(String clientCompanyNo) {
		this.clientCompanyNo = clientCompanyNo;
	}

	public String getClientCompanyName() {
		return clientCompanyName;
	}

	public void setClientCompanyName(String clientCompanyName) {
		this.clientCompanyName = clientCompanyName;
	}

	public java.sql.Date getWriteDate() {
		return writeDate;
	}

	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}

	@Override
	public String toString() {
		return "ClientListDTO [clientNo=" + clientNo + ", clientName=" + clientName + ", dept=" + dept + ", job=" + job
				+ ", phone=" + phone + ", landlineTel=" + landlineTel + ", email=" + email + ", userNo=" + userNo
				+ ", name=" + name + ", clientCompanyNo=" + clientCompanyNo + ", clientCompanyName=" + clientCompanyName
				+ ", writeDate=" + writeDate + "]";
	}
	
}
