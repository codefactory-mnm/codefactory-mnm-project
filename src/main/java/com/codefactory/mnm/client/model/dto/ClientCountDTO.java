package com.codefactory.mnm.client.model.dto;

public class ClientCountDTO implements java.io.Serializable {

	private String clientNo;					//고객번호
	private int businessActivityCount;			//영업활동 횟수 

	public ClientCountDTO () {}

	public ClientCountDTO(String clientNo, int businessActivityCount) {
		super();
		this.clientNo = clientNo;
		this.businessActivityCount = businessActivityCount;
	}

	public String getClientNo() {
		return clientNo;
	}

	public void setClientNo(String clientNo) {
		this.clientNo = clientNo;
	}

	public int getBusinessActivityCount() {
		return businessActivityCount;
	}

	public void setBusinessActivityCount(int businessActivityCount) {
		this.businessActivityCount = businessActivityCount;
	}

	@Override
	public String toString() {
		return "ClientCountDTO [clientNo=" + clientNo + ", businessActivityCount=" + businessActivityCount + "]";
	}
	
	
	
}
