package com.codefactory.mnm.client.model.service;

import java.util.List;
import java.util.Map;

import com.codefactory.mnm.client.model.dto.BusinessActivityDTO;
import com.codefactory.mnm.client.model.dto.ClientAttachmentDTO;
import com.codefactory.mnm.client.model.dto.ClientCollectionDTO;
import com.codefactory.mnm.client.model.dto.ClientCompanyDTO;
import com.codefactory.mnm.client.model.dto.ClientSearchDTO;
import com.codefactory.mnm.client.model.dto.OpinionDTO;
import com.codefactory.mnm.client.model.dto.UsersDTO;

public interface ClientService {

	Map<String, Object> selectClientList(ClientSearchDTO clientSearch);					//고객 리스트 조회
	
	List<UsersDTO> selectUserList();													//담당자 리스트 조회
	
	List<BusinessActivityDTO> selectBusinessAcitivityList(String clientNo);				//영업활동 리스트 조회
	
	List<ClientCompanyDTO> selectClientCompanyList();									//고객사 리스트 조회

	Map<String, Object> selectClient(String clientNo);									//고객 조회
	
	List<OpinionDTO> selectOpinionList(String clientNo);								//의견 리스트 조회
	
	int insertOpinion(String clientNo, String opinionContent, String id);				//의견 등록		
	
	int deleteOpinion(String opinionNo);												//의견 삭제
	
	ClientAttachmentDTO selectFile(String fileNo);										//첨부파일 조회
	
	int insertClient(ClientCollectionDTO clientCollection);								//고객 등록

	Map<String, Object> deleteFile(String fileNo);										//첨부파일 삭제

	int updateClient(ClientCollectionDTO clientCollection);								//고객 수정

	int deleteClient(String clientNo);													//고객 삭제








}
