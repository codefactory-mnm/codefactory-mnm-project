package com.codefactory.mnm.client.model.dto;

import java.sql.Date;

public class BusinessActivityDTO implements java.io.Serializable {

	private String businessActivityNo;		//영업활동명
	private String activityClassification;	//분류
	private String activityPurpose;			//목적
	private java.sql.Date activityDate;		//영업활동일자
	private String businessOpportunityName;	//영업기회명
	
	public BusinessActivityDTO() {}

	public BusinessActivityDTO(String businessActivityNo, String activityClassification, String activityPurpose,
			Date activityDate, String businessOpportunityName) {
		super();
		this.businessActivityNo = businessActivityNo;
		this.activityClassification = activityClassification;
		this.activityPurpose = activityPurpose;
		this.activityDate = activityDate;
		this.businessOpportunityName = businessOpportunityName;
	}

	public String getBusinessActivityNo() {
		return businessActivityNo;
	}

	public void setBusinessActivityNo(String businessActivityNo) {
		this.businessActivityNo = businessActivityNo;
	}

	public String getActivityClassification() {
		return activityClassification;
	}

	public void setActivityClassification(String activityClassification) {
		this.activityClassification = activityClassification;
	}

	public String getActivityPurpose() {
		return activityPurpose;
	}

	public void setActivityPurpose(String activityPurpose) {
		this.activityPurpose = activityPurpose;
	}

	public java.sql.Date getActivityDate() {
		return activityDate;
	}

	public void setActivityDate(java.sql.Date activityDate) {
		this.activityDate = activityDate;
	}

	public String getBusinessOpportunityName() {
		return businessOpportunityName;
	}

	public void setBusinessOpportunityName(String businessOpportunityName) {
		this.businessOpportunityName = businessOpportunityName;
	}

	@Override
	public String toString() {
		return "BusinessActivityDTO [businessActivityNo=" + businessActivityNo + ", activityClassification="
				+ activityClassification + ", activityPurpose=" + activityPurpose + ", activityDate=" + activityDate
				+ ", businessOpportunityName=" + businessOpportunityName + "]";
	}
	
	
}
