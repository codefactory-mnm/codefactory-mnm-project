package com.codefactory.mnm.client.model.dto;

import java.util.List;

public class ClientCollectionDTO implements java.io.Serializable {
	
	public ClientDTO client;							//고객 DTO
	public List<ClientAttachmentDTO> files;		        //첨부파일DTO List
	
	public ClientCollectionDTO() {}

	public ClientCollectionDTO(ClientDTO client, List<ClientAttachmentDTO> files) {
		super();
		this.client = client;
		this.files = files;
	}

	public ClientDTO getClient() {
		return client;
	}

	public void setClient(ClientDTO client) {
		this.client = client;
	}

	public List<ClientAttachmentDTO> getFiles() {
		return files;
	}

	public void setFiles(List<ClientAttachmentDTO> files) {
		this.files = files;
	}

	@Override
	public String toString() {
		return "ClientCollectionDTO [client=" + client + ", files=" + files + "]";
	}

	
	
}
