package com.codefactory.mnm.client.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.codefactory.mnm.client.model.dto.BusinessActivityDTO;
import com.codefactory.mnm.client.model.dto.ClientAttachmentDTO;
import com.codefactory.mnm.client.model.dto.ClientCollectionDTO;
import com.codefactory.mnm.client.model.dto.ClientCompanyDTO;
import com.codefactory.mnm.client.model.dto.ClientCountDTO;
import com.codefactory.mnm.client.model.dto.ClientDTO;
import com.codefactory.mnm.client.model.dto.ClientListDTO;
import com.codefactory.mnm.client.model.dto.ClientSearchDTO;
import com.codefactory.mnm.client.model.dto.OpinionDTO;
import com.codefactory.mnm.client.model.dto.UsersDTO;
import com.codefactory.mnm.client.model.service.ClientService;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyAttachmentDTO;
import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.member.model.dto.UserImpl;

@Controller
@RequestMapping("/client")
public class ClientController {
	
	private ClientService clientService;
	
	@Autowired
	public ClientController(ClientService clientService) {
		this.clientService = clientService;
	}
	
	/* 고객 리스트 조회 */
	@GetMapping("/list")
	public ModelAndView selectClientList(ModelAndView mv,
			@RequestParam(defaultValue = "") String selectSearch,
			@RequestParam(defaultValue = "") String searchValue,
			@RequestParam(defaultValue = "") String userNo,
			@RequestParam(defaultValue = "1") String currentPage) {
		
		/* 검색조건에서 해당되지 않거나, 검색조건 없이 리스틀 조회할 시 null로 전달하기위해 null로 담는다. */
		String clientName = null;
		String clientCompanyName = null;
		String phone = null;
		String email = null;
		
		/* 검색 조건에 따라 값을 설정 */
		if(selectSearch.equals("고객")) { 			//검색조건 중 고객을 선택하였을 경우
			clientName = searchValue;
		} else if(selectSearch.equals("고객사")){		//검색조건 중 고객사를 선택하였을 경우 
			clientCompanyName = searchValue;
		} else if(selectSearch.equals("메일")) {		//검색조건 중 메일을 선택하였을 경우
			email = searchValue;
		} else if(selectSearch.equals("휴대폰번호")) {	//검색조건 중 휴대본번호를 선택하였을 경우
			phone = searchValue;
		}

		if(userNo.equals("")) { 						// 검색조건 중 이름을 선택하지 않았을 경우
			userNo = null;
		}

		ClientSearchDTO clientSearch = new ClientSearchDTO();
		clientSearch.setClientName(clientName);
		clientSearch.setClientCompanyName(clientCompanyName);
		clientSearch.setPhone(phone);
		clientSearch.setEmail(email);
		clientSearch.setUserNo(userNo);
		clientSearch.setCurrentPage(currentPage);

		Map<String, Object> map = clientService.selectClientList(clientSearch);

		List<ClientListDTO> clientList = (List<ClientListDTO>) map.get("clientList");				//고객 리스트 조회
		List<ClientCountDTO> clientCount = (List<ClientCountDTO>) map.get("clientCount");			//고객의 영업활동 횟수 리스트 조회
		SelectCriteria selectCriteria = (SelectCriteria) map.get("selectCriteria");					//페이징 처리		
		int totalCount = (int) map.get("totalCount");												//게시물 수 조회
		
		mv.addObject("clientList", clientList);
		mv.addObject("clientCount", clientCount);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("clientSearch", clientSearch);
		mv.addObject("totalCount", totalCount);
		mv.setViewName("client/list");
		
		return mv;
	}
	
	/* 담당자 리스트 조회 */
	@GetMapping(value="user", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<UsersDTO> selectUserList() {
		
		return clientService.selectUserList();
		
	}
	
	/* 영업활동 리스트 조회 */
	@GetMapping(value="businessAcitivity", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<BusinessActivityDTO> selectBusinessAcitivityList(String clientNo) {

		return clientService.selectBusinessAcitivityList(clientNo);
		
	}
	
	/* 고객 조회 */
	@GetMapping("/select")
	public ModelAndView selectClient(ModelAndView mv, String clientNo) {
			
		Map<String, Object> map = clientService.selectClient(clientNo);
			
		ClientDTO client = (ClientDTO) map.get("client");																//고객 조회								
		List<ClientAttachmentDTO> clientAttachmentList = (List<ClientAttachmentDTO>) map.get("clientAttachmentList");	//첨부파일 리스트 조회																	

		mv.addObject("client", client);
		mv.addObject("clientAttachmentList", clientAttachmentList);			
		
		mv.setViewName("client/select");
		return mv;
	}
	
	/* 의견 리스트 조회 */
	@GetMapping(value="opinion/select", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<OpinionDTO> selectOpinionList(String clientNo, @AuthenticationPrincipal UserImpl customUser) {

		List<OpinionDTO> selectOpinionList = clientService.selectOpinionList(clientNo);
		
		/* 의견삭제시 작성자와 로그인된사용자가 같은지 확인하기 위해서 사용자번호를 담음 */
		for(int i = 0; i < selectOpinionList.size(); i++) {

			selectOpinionList.get(i).setLoginUserNo(customUser.getUserNo());

		}

		return selectOpinionList;
	}
	
	/* 의견 등록 */
	@PostMapping(value="opinion/insert", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int insertOpinion(String clientNo, String opinionContent, Principal pcp) {
		
		String id = pcp.getName();	//세션에 담겨있는 사용자 아이디

		return clientService.insertOpinion(clientNo, opinionContent, id);
	}
	
	/* 의견 삭제 */
	@PostMapping(value="opinion/delete", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int deleteOpinion(String opinionNo) {

		return clientService.deleteOpinion(opinionNo);		
	}
	
	/* 첨부파일 다운로드 */
	@GetMapping(value="fileDownload")
	@ResponseBody
	public void fileDownload(String fileNo, HttpServletResponse response, HttpServletRequest request) throws Exception {
		
		ClientAttachmentDTO clientAttachment = clientService.selectFile(fileNo);
		
		String fileName = clientAttachment.getFileName();							//저장파일명
		String fileOriginalName = clientAttachment.getFileOriginalName();			//원본파일명
		String filePath = clientAttachment.getFilePath();							//첨부파일 저장경로(절대경로)
		String saveFileName = filePath + fileName;					
		
		File file = new File(saveFileName);
		long fileLength = file.length();

		/* 파일 정보들을 가지고 reponse의 Header에 세팅 */
		response.setHeader("Content-Disposition", "attachment; filename=\"" + URLEncoder.encode(fileOriginalName,"UTF-8") + "\";");
		response.setHeader("Content-Transfer-Encoding", "binary");
		response.setHeader("Content-Type", "application/octet-stream");
		response.setHeader("Content-Length", "" + fileLength);
		response.setHeader("Pragma", "no-cache;");
		response.setHeader("Expires", "-1;");

		/* saveFileName을 파라미터로 넣어 inputStream 객체를 만들고 
	        response에서 파일을 내보낼 OutputStream을 가져와서 */
		FileInputStream fis = new FileInputStream(saveFileName);
		OutputStream out = response.getOutputStream();

		/* 파일 읽을 만큼 크기의 buffer를 생성한 후 */
		int readCount = 0;
		byte[] buffer = new byte[1024];
		
		/* outputStream에 씌워준다 */
		while((readCount = fis.read(buffer)) != -1){
			out.write(buffer,0,readCount);
		}

		fis.close();
		out.close();
		
	}
	
	
	/* 고객사 리스트 조회 */
	@GetMapping(value="clientCompany", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<ClientCompanyDTO> selectClientCompanyList() {

		return clientService.selectClientCompanyList();
	}
	
	/* 고객 작성화면으로 이동 */
	@GetMapping("/insert")
	public void insertClientClientPage() {}
	
	/* 고객 등록 */
	@PostMapping("/insert")
	public ModelAndView insertClient(ModelAndView mv, ClientDTO client,
			List<MultipartFile> multiFiles, HttpServletRequest request, RedirectAttributes rttr) {

		/* 고객 정보 입력과 동시에 파일첨부도 같이 등록한 경우 */
		if(multiFiles.size() > 0 && !multiFiles.get(0).getOriginalFilename().equals("")) {

			String root = request.getSession().getServletContext().getRealPath("/");	//첨부파일 저장경로(절대경로)
			String filePath = root + "static/upload/";									//첨부파일 저장경로(최종경로)

			/* 첨부파일 저장경로 (최종경로) 파일이 존재 하지 않을 경우 생성 */
			File mkdir = new File(filePath);
			if(!mkdir.exists()) {
				mkdir.mkdirs();
			}

			List<ClientAttachmentDTO> files = new ArrayList<>();

			/* 다중 첨부파일은 반복문을 통해서, 미리 선언한 files에 담음  */
			for(int i = 0; i < multiFiles.size(); i++) {
				String fileOriginalName = multiFiles.get(i).getOriginalFilename();			//원본파일명
				String ext = fileOriginalName.substring(fileOriginalName.lastIndexOf("."));	//확장자명
				String fileName = UUID.randomUUID().toString().replace("-", "") + ext;		//저장파일명

				/* 고객파일첨부DTO인 file에 정보를 담은 후 리스트인 files에 담음 */
				ClientAttachmentDTO file = new ClientAttachmentDTO();			
				file.setFileName(fileName);
				file.setFileOriginalName(fileOriginalName);
				file.setFilePath(filePath);

				files.add(file);
			}

			/* 고객 등록시 필요한 정보를 ClientCollectionDTO에 담음 */
			ClientCollectionDTO clientCollection = new ClientCollectionDTO();	
			clientCollection.setClient(client);
			clientCollection.setFiles(files);

			int result = clientService.insertClient(clientCollection);

			/* DB에 등록을 성공하였을 경우 */
			if(result > 0 ) {

				/* 파일 저장 */
				try {
					for(int i = 0; i < multiFiles.size(); i++) {
						ClientAttachmentDTO file = files.get(i);			
						multiFiles.get(i).transferTo(new File(filePath + "\\" + file.getFileName()));
					}
					rttr.addFlashAttribute("message", "고객 등록을 성공하셨습니다.");

					/* 파일 저장을 실패하였을 경우, 저장했던 파일을 삭제
					 * 트랜젝션처리를 통해서 DB에 입력된 정보도 삭제됨 */
				} catch (Exception e) {
					e.printStackTrace();

					for(int i = 0; i < multiFiles.size(); i++) {
						ClientAttachmentDTO file = files.get(i); 
						new File(filePath + "\\" + file.getFileName()).delete(); 
					}
					rttr.addFlashAttribute("message", "고객 등록 후 파일 업로드에 실패하셨습니다.");
				}

				/* DB에 고객 등록을 실패하였을 경우 */
			} else {
				rttr.addFlashAttribute("message", "고객 등록을 실패하셨습니다.");	
			}

			/* 파일첨부를 하지 않고, 고객 등록을 하는 경우 */
		} else {

			/* 고객 등록시 필요한 정보를 ClientCollectionDTO에 담음  */
			ClientCollectionDTO clientCollection = new ClientCollectionDTO();
			clientCollection.setClient(client);

			int result = clientService.insertClient(clientCollection);

			if(result > 0) {
				rttr.addFlashAttribute("message", "고객 등록을 성공하셨습니다.");
			} else {
				rttr.addFlashAttribute("message", "고객 등록을 실패하셨습니다.");
			}

		}

		mv.setViewName("redirect:/client/list");
		return mv;
	}
	
	/* 고객 수정페이지 조회  */
	@GetMapping("/update")
	public ModelAndView selectClientForUpdate(ModelAndView mv, String clientNo) {
			
		Map<String, Object> map = clientService.selectClient(clientNo);
			
		ClientDTO client = (ClientDTO) map.get("client");																//고객 정보 조회								
		List<ClientAttachmentDTO> clientAttachmentList = (List<ClientAttachmentDTO>) map.get("clientAttachmentList");	//첨부파일 리스트 조회															

		mv.addObject("client", client);
		mv.addObject("clientAttachmentList", clientAttachmentList);			
		
		mv.setViewName("client/update");
		return mv;
	}
	
	/* 첨부파일 삭제 */
	@PostMapping(value="file/delete", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int deleteFile(String fileNo, HttpServletRequest request) {
		
		String root = request.getSession().getServletContext().getRealPath("/");	//첨부파일 저장경로(절대경로)
		String filePath = root + "static/upload/";									//첨부파일 저장경로(최종경로)
		
		/* 파일번호를 service에 전달하고 map으로 값을 전달받음 */
		Map<String, Object> map = clientService.deleteFile(fileNo);	
		
		ClientAttachmentDTO file = (ClientAttachmentDTO) map.get("clientAttachment");			//첨부파일정보
		int result = (int) map.get("result");													//첨부파일 DB에서 삭제 결과
		
		/* 첨부파일을 DB에서 성공적으로 삭제되었을 경우 저장된파일도 삭제 */
		if(result > 0 ) {		
			new File(filePath + "\\" + file.getFileName()).delete(); 					
		}
	
		return result;
	}
	
	/* 고객 수정 */
	@PostMapping("/update")
	public ModelAndView updateClient(ModelAndView mv, ClientDTO client,
			List<MultipartFile> multiFiles, HttpServletRequest request, RedirectAttributes rttr) {

		/* 고객 정보 수정과 동시에 파일첨부도 같이 등록한 경우 */
		if(multiFiles.size() > 0 && !multiFiles.get(0).getOriginalFilename().equals("")) {

			String root = request.getSession().getServletContext().getRealPath("/");	//첨부파일 저장경로(절대경로)
			String filePath = root + "static/upload/";									//첨부파일 저장경로(최종경로)

			/* 첨부파일 저장경로 (최종경로) 파일이 존재 하지 않을 경우 생성 */
			File mkdir = new File(filePath);
			if(!mkdir.exists()) {
				mkdir.mkdirs();
			}

			List<ClientAttachmentDTO> files = new ArrayList<>();

			/* 다중 첨부파일은 반복문을 통해서, 미리 선언한 files에 담음  */
			for(int i = 0; i < multiFiles.size(); i++) {
				String fileOriginalName = multiFiles.get(i).getOriginalFilename();			//원본파일명
				String ext = fileOriginalName.substring(fileOriginalName.lastIndexOf("."));	//확장자명
				String fileName = UUID.randomUUID().toString().replace("-", "") + ext;		//저장파일명

				/* 고객파일첨부DTO인 file에 정보를 담은 후 리스트인 files에 담음 */
				ClientAttachmentDTO file = new ClientAttachmentDTO();			
				file.setFileName(fileName);
				file.setFileOriginalName(fileOriginalName);
				file.setFilePath(filePath);

				files.add(file);
			}

			/* 고객 수정시 필요한 정보를 ClientCollectionDTO에 담음 */
			ClientCollectionDTO clientCollection = new ClientCollectionDTO();	
			clientCollection.setClient(client);
			clientCollection.setFiles(files);

			int result = clientService.updateClient(clientCollection);

			/* DB에 등록을 성공하였을 경우 */
			if(result > 0 ) {

				/* 파일 저장 */
				try {
					for(int i = 0; i < multiFiles.size(); i++) {
						ClientAttachmentDTO file = files.get(i);			
						multiFiles.get(i).transferTo(new File(filePath + "\\" + file.getFileName()));
					}

					rttr.addFlashAttribute("message", "고객 수정을 성공하셨습니다.");

					/* 파일 저장을 실패하였을 경우, 저장했던 파일을 삭제
					 * 트랜젝션처리를 통해서 DB에 입력된 정보도 삭제됨 */
				} catch (Exception e) {
					e.printStackTrace();

					for(int i = 0; i < multiFiles.size(); i++) {
						ClientAttachmentDTO file = files.get(i); 
						new File(filePath + "\\" + file.getFileName()).delete(); 
					}
					rttr.addFlashAttribute("message", "고객 수정 후 파일 업로드에 실패하셨습니다.");
				}

				/* DB에 고객 등록을 실패하였을 경우 */
			} else {
				rttr.addFlashAttribute("message", "고객 수정을 실패하셨습니다.");	
			}

		/* 파일첨부를 하지 않고, 고객 수정을 하는 경우 */
		} else {

			/* 고객 수정시 필요한 정보를 ClientCollectionDTO에 담음 */
			ClientCollectionDTO clientCollection = new ClientCollectionDTO();
			clientCollection.setClient(client);

			int result = clientService.updateClient(clientCollection);

			if(result > 0) {
				rttr.addFlashAttribute("message", "고객 수정을 성공하셨습니다.");
			} else {
				rttr.addFlashAttribute("message", "고객 수정을 실패하셨습니다.");
			}

		}

		mv.setViewName("redirect:/client/list");
		return mv;
	}
	
	/* 고객 삭제 */
	@PostMapping(value="delete", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int deleteClient(String clientNo) {

		return clientService.deleteClient(clientNo);		
	}
	
}
