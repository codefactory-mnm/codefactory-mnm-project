package com.codefactory.mnm.businesstool.model.dto;

import java.sql.Date;

public class BusinessNoticeListDTO implements java.io.Serializable {

	private String businessNoticeNo;			//영업공지번호
	private String title;						//제목
	private String mustReadYn;					//필독여부
	private String content;						//내용
	private int count;							//조회수
	private java.sql.Date writeDate;			//작성일
	private String userNo;						//작성자번호
	private String delYn;						//삭제여부
	private String name;						//작성자명
	private String fileName;					//파일명
	private String fileOriginalName;			//원본파일명
	private String fileNo;						//파일번호
	private String filePath;					//파일경로
	
	public BusinessNoticeListDTO() {}

	public BusinessNoticeListDTO(String businessNoticeNo, String title, String mustReadYn, String content, int count,
			Date writeDate, String userNo, String delYn, String name, String fileName, String fileOriginalName,
			String fileNo, String filePath) {
		super();
		this.businessNoticeNo = businessNoticeNo;
		this.title = title;
		this.mustReadYn = mustReadYn;
		this.content = content;
		this.count = count;
		this.writeDate = writeDate;
		this.userNo = userNo;
		this.delYn = delYn;
		this.name = name;
		this.fileName = fileName;
		this.fileOriginalName = fileOriginalName;
		this.fileNo = fileNo;
		this.filePath = filePath;
	}

	public String getBusinessNoticeNo() {
		return businessNoticeNo;
	}

	public void setBusinessNoticeNo(String businessNoticeNo) {
		this.businessNoticeNo = businessNoticeNo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMustReadYn() {
		return mustReadYn;
	}

	public void setMustReadYn(String mustReadYn) {
		this.mustReadYn = mustReadYn;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public java.sql.Date getWriteDate() {
		return writeDate;
	}

	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getDelYn() {
		return delYn;
	}

	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileOriginalName() {
		return fileOriginalName;
	}

	public void setFileOriginalName(String fileOriginalName) {
		this.fileOriginalName = fileOriginalName;
	}

	public String getFileNo() {
		return fileNo;
	}

	public void setFileNo(String fileNo) {
		this.fileNo = fileNo;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	@Override
	public String toString() {
		return "BusinessNoticeListDTO [businessNoticeNo=" + businessNoticeNo + ", title=" + title + ", mustReadYn="
				+ mustReadYn + ", content=" + content + ", count=" + count + ", writeDate=" + writeDate + ", userNo="
				+ userNo + ", delYn=" + delYn + ", name=" + name + ", fileName=" + fileName + ", fileOriginalName="
				+ fileOriginalName + ", fileNo=" + fileNo + ", filePath=" + filePath + "]";
	}

	
	
}
