package com.codefactory.mnm.businesstool.model.dto;

import java.sql.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class ProceedingsDTO implements java.io.Serializable{
	
	private String proceedingsNo;						//회의록번호
	private String proceedingsName;						//회의록명
	private java.sql.Date proceedingsDate;				//회의날짜
	private String startTime;							//회의시작시간
	private String endTime;								//회의종료시간
	private String location;							//장소
	private String content;								//내용
	private String clientCompanyNo;						//고객사 번호
	private String clientCompanyName;					//고객사 번호
	
	public ProceedingsDTO() {}

	public ProceedingsDTO(String proceedingsNo, String proceedingsName, Date proceedingsDate, String startTime,
			String endTime, String location, String content, String clientCompanyNo, String clientCompanyName) {
		super();
		this.proceedingsNo = proceedingsNo;
		this.proceedingsName = proceedingsName;
		this.proceedingsDate = proceedingsDate;
		this.startTime = startTime;
		this.endTime = endTime;
		this.location = location;
		this.content = content;
		this.clientCompanyNo = clientCompanyNo;
		this.clientCompanyName = clientCompanyName;
	}

	public String getProceedingsNo() {
		return proceedingsNo;
	}

	public void setProceedingsNo(String proceedingsNo) {
		this.proceedingsNo = proceedingsNo;
	}

	public String getProceedingsName() {
		return proceedingsName;
	}

	public void setProceedingsName(String proceedingsName) {
		this.proceedingsName = proceedingsName;
	}

	public java.sql.Date getProceedingsDate() {
		return proceedingsDate;
	}

	public void setProceedingsDate(java.sql.Date proceedingsDate) {
		this.proceedingsDate = proceedingsDate;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getClientCompanyNo() {
		return clientCompanyNo;
	}

	public void setClientCompanyNo(String clientCompanyNo) {
		this.clientCompanyNo = clientCompanyNo;
	}

	public String getClientCompanyName() {
		return clientCompanyName;
	}

	public void setClientCompanyName(String clientCompanyName) {
		this.clientCompanyName = clientCompanyName;
	}

	@Override
	public String toString() {
		return "ProceedingsDTO [proceedingsNo=" + proceedingsNo + ", proceedingsName=" + proceedingsName
				+ ", proceedingsDate=" + proceedingsDate + ", startTime=" + startTime + ", endTime=" + endTime
				+ ", location=" + location + ", content=" + content + ", clientCompanyNo=" + clientCompanyNo
				+ ", clientCompanyName=" + clientCompanyName + "]";
	}

}
