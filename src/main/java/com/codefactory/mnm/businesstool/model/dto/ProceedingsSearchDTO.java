package com.codefactory.mnm.businesstool.model.dto;

import com.codefactory.mnm.common.SelectCriteria;

public class ProceedingsSearchDTO implements java.io.Serializable {
	
	private String proceedingsName;				//회의록명
	private String clientName;					//고객명
	private String clientCompanyName;			//고객사명
	private String currentPage;					//현재페이지
	private SelectCriteria selectCriteria;		//페이징처리
	
	public ProceedingsSearchDTO() {}

	public ProceedingsSearchDTO(String proceedingsName, String clientName, String clientCompanyName, String currentPage,
			SelectCriteria selectCriteria) {
		super();
		this.proceedingsName = proceedingsName;
		this.clientName = clientName;
		this.clientCompanyName = clientCompanyName;
		this.currentPage = currentPage;
		this.selectCriteria = selectCriteria;
	}

	public String getProceedingsName() {
		return proceedingsName;
	}

	public void setProceedingsName(String proceedingsName) {
		this.proceedingsName = proceedingsName;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getClientCompanyName() {
		return clientCompanyName;
	}

	public void setClientCompanyName(String clientCompanyName) {
		this.clientCompanyName = clientCompanyName;
	}

	public String getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(String currentPage) {
		this.currentPage = currentPage;
	}

	public SelectCriteria getSelectCriteria() {
		return selectCriteria;
	}

	public void setSelectCriteria(SelectCriteria selectCriteria) {
		this.selectCriteria = selectCriteria;
	}

	@Override
	public String toString() {
		return "ProceedingsSearchDTO [proceedingsName=" + proceedingsName + ", clientName=" + clientName
				+ ", clientCompanyName=" + clientCompanyName + ", currentPage=" + currentPage + ", selectCriteria="
				+ selectCriteria + "]";
	}

	
}
