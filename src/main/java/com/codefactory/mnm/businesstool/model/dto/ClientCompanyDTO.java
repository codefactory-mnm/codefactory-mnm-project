package com.codefactory.mnm.businesstool.model.dto;

import java.sql.Date;

public class ClientCompanyDTO implements java.io.Serializable{

	private String clientCompanyNo;			//고객사번호
	private String clientCompanyName;		//고객사명
	private String zipCode;					//우편번호
	private String address;					//주소
	private String detailedAddress;			//상세주소
	private String division;				//구분
	private java.sql.Date writeDate;		//작성일자

	public ClientCompanyDTO() {}

	public ClientCompanyDTO(String clientCompanyNo, String clientCompanyName, String zipCode, String address,
			String detailedAddress, String division, Date writeDate) {
		super();
		this.clientCompanyNo = clientCompanyNo;
		this.clientCompanyName = clientCompanyName;
		this.zipCode = zipCode;
		this.address = address;
		this.detailedAddress = detailedAddress;
		this.division = division;
		this.writeDate = writeDate;
	}

	public String getClientCompanyNo() {
		return clientCompanyNo;
	}

	public void setClientCompanyNo(String clientCompanyNo) {
		this.clientCompanyNo = clientCompanyNo;
	}

	public String getClientCompanyName() {
		return clientCompanyName;
	}

	public void setClientCompanyName(String clientCompanyName) {
		this.clientCompanyName = clientCompanyName;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDetailedAddress() {
		return detailedAddress;
	}

	public void setDetailedAddress(String detailedAddress) {
		this.detailedAddress = detailedAddress;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public java.sql.Date getWriteDate() {
		return writeDate;
	}

	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}

	@Override
	public String toString() {
		return "ClientCompanyDTO [clientCompanyNo=" + clientCompanyNo + ", clientCompanyName=" + clientCompanyName
				+ ", zipCode=" + zipCode + ", address=" + address + ", detailedAddress=" + detailedAddress
				+ ", division=" + division + ", writeDate=" + writeDate + "]";
	}

	


}
