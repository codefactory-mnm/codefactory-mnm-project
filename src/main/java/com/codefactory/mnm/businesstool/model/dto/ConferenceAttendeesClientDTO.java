package com.codefactory.mnm.businesstool.model.dto;

public class ConferenceAttendeesClientDTO implements java.io.Serializable {

	private String proceedingsNo;		//회의록번호
	private String clientNo;			//회의참석자(고객)번호
	private String clientName;			//회의참석자(고객)명
	
	public ConferenceAttendeesClientDTO() {}

	public ConferenceAttendeesClientDTO(String proceedingsNo, String clientNo, String clientName) {
		super();
		this.proceedingsNo = proceedingsNo;
		this.clientNo = clientNo;
		this.clientName = clientName;
	}

	public String getProceedingsNo() {
		return proceedingsNo;
	}

	public void setProceedingsNo(String proceedingsNo) {
		this.proceedingsNo = proceedingsNo;
	}

	public String getClientNo() {
		return clientNo;
	}

	public void setClientNo(String clientNo) {
		this.clientNo = clientNo;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	@Override
	public String toString() {
		return "ConferenceAttendeesClientDTO [proceedingsNo=" + proceedingsNo + ", clientNo=" + clientNo
				+ ", clientName=" + clientName + "]";
	}

	
	
	
}
