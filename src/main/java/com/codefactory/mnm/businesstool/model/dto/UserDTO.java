package com.codefactory.mnm.businesstool.model.dto;

public class UserDTO implements java.io.Serializable {
	
	private String userNo;			//담당자 번호
	private String name;			//담당자 이름
	private String deptName;		//담당자 부서
	private String position;		//담당자 직위
	
	public UserDTO() {}

	public UserDTO(String userNo, String name, String deptName, String position) {
		super();
		this.userNo = userNo;
		this.name = name;
		this.deptName = deptName;
		this.position = position;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	@Override
	public String toString() {
		return "UserDTO [userNo=" + userNo + ", name=" + name + ", deptName=" + deptName + ", position=" + position
				+ "]";
	}

	
}
