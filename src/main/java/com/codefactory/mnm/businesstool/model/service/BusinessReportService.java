package com.codefactory.mnm.businesstool.model.service;

import java.util.List;
import java.util.Map;

import com.codefactory.mnm.businessactivity.model.dto.BusinessActivityListDTO;
import com.codefactory.mnm.businesstool.model.dto.BusinessReportCollectionDTO;
import com.codefactory.mnm.businesstool.model.dto.BusinessReportSearchDTO;
import com.codefactory.mnm.client.model.dto.UsersDTO;

public interface BusinessReportService {

	Map<String, Object> selectBusinessReportList(BusinessReportSearchDTO businessReportSearch);

	int insertBusinessReport(BusinessReportCollectionDTO businessReportCollection);

	UsersDTO selectUserNo(String id);

	Map<String, Object> selectBusinessReport(String businessReportNo);

	int deleteBusinessReport(String businessReportNo);

	List<BusinessActivityListDTO> selectBusinessActivityList();

}
