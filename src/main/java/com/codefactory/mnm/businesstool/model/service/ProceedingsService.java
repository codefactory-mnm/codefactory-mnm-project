package com.codefactory.mnm.businesstool.model.service;

import java.util.List;
import java.util.Map;

import com.codefactory.mnm.businesstool.model.dto.ClientCompanyDTO;
import com.codefactory.mnm.businesstool.model.dto.ClientDTO;
import com.codefactory.mnm.businesstool.model.dto.ProceedingsAttachmentDTO;
import com.codefactory.mnm.businesstool.model.dto.ProceedingsCollectionDTO;
import com.codefactory.mnm.businesstool.model.dto.ProceedingsSearchDTO;
import com.codefactory.mnm.businesstool.model.dto.UserDTO;

public interface ProceedingsService {

	Map<String, Object> selectProceedingsList(ProceedingsSearchDTO proceedingsSearch);		//회의록 리스트 조회
	
	Map<String, Object> selectProceedings(String proceedingsNo);							//회의록 조회
	
	ProceedingsAttachmentDTO selectFile(String fileNo);										//첨부파일 조회
	
	List<ClientCompanyDTO> selectClientCompanyList();										//고객사 리스트 조회
	
	List<ClientDTO> selectClientList(String clientCompanyNo);								//회의 참석자(고객) 리스트 조회

	List<UserDTO> selectUserList();															//회의 참석자(자사) 리스트 조회

	int insertProceedings(ProceedingsCollectionDTO proceedingsCollection);					//회의록 등록

	Map<String, Object> deleteFile(String fileNo);											//첨부파일 삭제

	int insertConferenceAttendeesUsers(String proceedingsNo, String userNo);				//회의 참석자(자사) 등록

	int deleteConferenceAttendeesUsers(String proceedingsNo, String userNo);				//회의 참석자(자사) 삭제

	int insertConferenceAttendeesClient(String proceedingsNo, String clientNo);				//회의 참석자(고객) 등록

	int deleteConferenceAttendeesClient(String proceedingsNo, String clientNo);				//회의 참석자(고객) 삭제

	int updateProceedings(ProceedingsCollectionDTO proceedingsCollection);					//회의록 수정

	int deleteProceedings(String proceedingsNo);											//회의록 삭제



}
