package com.codefactory.mnm.businesstool.model.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.codefactory.mnm.businessactivity.model.dto.BusinessActivityListDTO;
import com.codefactory.mnm.businesstool.model.dto.BusinessReportAttachmentsDTO;
import com.codefactory.mnm.businesstool.model.dto.BusinessReportListDTO;
import com.codefactory.mnm.businesstool.model.dto.BusinessReportSearchDTO;
import com.codefactory.mnm.client.model.dto.UsersDTO;

@Mapper
public interface BusinessReportDAO {

	int selectTotalCount(BusinessReportSearchDTO businessReportSearch);

	List<BusinessReportListDTO> selectBusinessReportList(BusinessReportSearchDTO businessReportSearch);

	int selectBusinessReportListCount();

	int insertBusinessReport(BusinessReportListDTO businessReport);

	int insertBusinessReportAttachment(BusinessReportAttachmentsDTO businessReportAttachment);

	UsersDTO selectUserNo(String id);

	BusinessReportListDTO selectBusinessReport(String businessReportNo);

	List<BusinessReportListDTO> selectFileList(String businessReportNo);

	int deleteBusinessReport(String businessReportNo);

	List<BusinessActivityListDTO> selectBusinessActivityList();

}
