package com.codefactory.mnm.businesstool.model.dto;

import java.util.List;

public class ProceedingsCollectionDTO implements java.io.Serializable {

	private ProceedingsDTO proceedings;				//회의록DTO
	private List<ProceedingsAttachmentDTO> files;	//첨부파일DTO List
	private List<ClientDTO> clientList;				//고객DTO List
	private List<UserDTO> userList;					//사용자DTO List
	
	public ProceedingsCollectionDTO() {}

	public ProceedingsCollectionDTO(ProceedingsDTO proceedings, List<ProceedingsAttachmentDTO> files,
			List<ClientDTO> clientList, List<UserDTO> userList) {
		super();
		this.proceedings = proceedings;
		this.files = files;
		this.clientList = clientList;
		this.userList = userList;
	}

	public ProceedingsDTO getProceedings() {
		return proceedings;
	}

	public void setProceedings(ProceedingsDTO proceedings) {
		this.proceedings = proceedings;
	}

	public List<ProceedingsAttachmentDTO> getFiles() {
		return files;
	}

	public void setFiles(List<ProceedingsAttachmentDTO> files) {
		this.files = files;
	}

	public List<ClientDTO> getClientList() {
		return clientList;
	}

	public void setClientList(List<ClientDTO> clientList) {
		this.clientList = clientList;
	}

	public List<UserDTO> getUserList() {
		return userList;
	}

	public void setUserList(List<UserDTO> userList) {
		this.userList = userList;
	}

	@Override
	public String toString() {
		return "ProceedingsCollectionDTO [proceedings=" + proceedings + ", files=" + files + ", clientList="
				+ clientList + ", userList=" + userList + "]";
	}
	
	
}
