package com.codefactory.mnm.businesstool.model.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.codefactory.mnm.businesstool.model.dto.BusinessNoticeAttachmentsDTO;
import com.codefactory.mnm.businesstool.model.dto.BusinessNoticeListDTO;
import com.codefactory.mnm.businesstool.model.dto.BusinessNoticeSearchDTO;
import com.codefactory.mnm.client.model.dto.UsersDTO;

@Mapper
public interface BusinessNoticeDAO {
 
	UsersDTO selectUserNo(String id);
	
	int insertBusinessNotice(BusinessNoticeListDTO businessNotice);

	int insertBusinessNoticeAttachment(BusinessNoticeAttachmentsDTO businessNoticeAttachment);

	int selectBusinessNoticeListCount();

	int selectTotalCount(BusinessNoticeSearchDTO businessNoticeSearchDTO);

	List<BusinessNoticeListDTO> selectBusinessNoticeList(BusinessNoticeSearchDTO businessNoticeSearchDTO);

	BusinessNoticeListDTO selectBusinessNotice(String businessNoticeNo);

	List<BusinessNoticeListDTO> selectFileList(String businessNoticeNo);

	int deleteBusinessNotice(String businessNoticeNo);

	int updateBusinessNotice(BusinessNoticeListDTO businessNotice);

	int updateBusinessNoticeAttachment(BusinessNoticeAttachmentsDTO businessNoticeAttachment);

	BusinessNoticeAttachmentsDTO selectFile(String businessFileNo);

	int deleteFile(String businessFileNo);

	int selectCount(BusinessNoticeListDTO oneBusinessNotice);



}
