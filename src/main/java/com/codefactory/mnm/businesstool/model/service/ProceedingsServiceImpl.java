package com.codefactory.mnm.businesstool.model.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.codefactory.mnm.businesstool.model.dao.ProceedingsDAO;
import com.codefactory.mnm.businesstool.model.dto.ClientCompanyDTO;
import com.codefactory.mnm.businesstool.model.dto.ClientDTO;
import com.codefactory.mnm.businesstool.model.dto.ConferenceAttendeesClientDTO;
import com.codefactory.mnm.businesstool.model.dto.ConferenceAttendeesUsersDTO;
import com.codefactory.mnm.businesstool.model.dto.ProceedingsAttachmentDTO;
import com.codefactory.mnm.businesstool.model.dto.ProceedingsCollectionDTO;
import com.codefactory.mnm.businesstool.model.dto.ProceedingsDTO;
import com.codefactory.mnm.businesstool.model.dto.ProceedingsSearchDTO;
import com.codefactory.mnm.businesstool.model.dto.UserDTO;
import com.codefactory.mnm.common.Pagenation;
import com.codefactory.mnm.common.SelectCriteria;

@Service
public class ProceedingsServiceImpl implements ProceedingsService{

	private final ProceedingsDAO mapper;
	
	@Autowired
	public ProceedingsServiceImpl(ProceedingsDAO mapper) {
		this.mapper = mapper;
	}
	
	/* 회의록 리스트 조회 */
	@Override
	public Map<String, Object> selectProceedingsList(ProceedingsSearchDTO proceedingsSearch) {
		
		int pageNo = 1;		
		
		if(pageNo <= 0) {
			pageNo = 1;
		}
		
		int limit = 10;		
		int buttonAmount = 5;
		
		/* 전체 게시물 수, 검색조건이 있다면 검색조건에 맞는 게시물 수 */
		int totalCount = mapper.selectTotalCount(proceedingsSearch); 
		
		/* view에서 전달받은 요청페이지*/
		String currentPage = proceedingsSearch.getCurrentPage();
		
		/* view에서 전달받은 요청페이지를 pageNo에 저장 */
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		/* 설정한 변수에 따른 페이징조건 */
		SelectCriteria selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount);
		
		Map<String, Object> map = new HashMap<>();

		proceedingsSearch.setSelectCriteria(selectCriteria);		
		
		List<ProceedingsDTO> proceedingsList = mapper.selectProceedingsList(proceedingsSearch);	 	//검색조건에 따른 회의록 리스트	조회			

		map.put("proceedingsList", proceedingsList);
		map.put("selectCriteria", selectCriteria);
		map.put("totalCount", totalCount);
		
		return map;
	}
	
	/* 회의록 상세 조회 */
	@Override
	public Map<String, Object> selectProceedings(String proceedingsNo) {
		
		Map<String, Object> map = new HashMap<>();
		
		ProceedingsDTO proceedings = mapper.selectProceedings(proceedingsNo);											//회의록 조회
		List<ConferenceAttendeesUsersDTO> userList = mapper.selectConferenceAttendeesUsers(proceedingsNo);				//회의 참가자(자사) 리스트 조회
		List<ConferenceAttendeesClientDTO> clientList = mapper.selectConferenceAttendeesClient(proceedingsNo);			//회의 참가자(고객) 리스트 조회
		List<ProceedingsAttachmentDTO> proceedingsAttachmentList = mapper.selectProceedingsAttachment(proceedingsNo);	//회의록 첨부파일 리스트 조회
		
		map.put("proceedings", proceedings);
		map.put("userList", userList);
		map.put("clientList", clientList);
		map.put("proceedingsAttachmentList", proceedingsAttachmentList);
		
		return map;
	}
	
	/* 첨부파일 조회 */
	@Override
	public ProceedingsAttachmentDTO selectFile(String fileNo) {
		
		return mapper.selectFile(fileNo);
	}

	
	/* 고객사 리스트 조회 */
	@Override
	public List<ClientCompanyDTO> selectClientCompanyList() {
		
		return mapper.selectClientCompanyList();
	}

	/* 회의 참석자(고객) 리스트 조회 */
	@Override
	public List<ClientDTO> selectClientList(String clientCompanyNo) {
		
		return mapper.selectClientList(clientCompanyNo);
	}

	/* 회의 참석자(자사) 리스트 조회 */
	@Override
	public List<UserDTO> selectUserList() {
		
		return mapper.selectUserList();
	}

	/* 회의록 등록 */
	/* DB에 저장하는 과정이므로 트랜잭션 처리를 해줌 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertProceedings(ProceedingsCollectionDTO proceedingsCollection) {
		
		ProceedingsDTO proceedings = proceedingsCollection.getProceedings();
		List<ProceedingsAttachmentDTO> files = proceedingsCollection.getFiles();
		List<ClientDTO> clientList = proceedingsCollection.getClientList();
		List<UserDTO> userList = proceedingsCollection.getUserList();
		
		int result1 = mapper.insertProceedings(proceedings);	//회의록 등록
		
		int result2 = 0;
		if(files != null) {
			for(ProceedingsAttachmentDTO proceedingsAttachment: files) {
				result2 += mapper.insertProceedingsAttachment(proceedingsAttachment);	//첨부파일 등록
			}	
		}

		int result3 = 0;
		if(clientList != null) {
			for(ClientDTO client : clientList) {
				result3 += mapper.insertConferenceClient(client);	//회의 참석자(고객) 등록
			}
		}

		int result4 = 0;
		if(userList != null) {
			for(UserDTO user : userList) {
				result4 += mapper.insertConferenceUser(user);	//회의 참석자(자사) 등록
			}
		}

		/* 모든 조건이 부합할 경우 result를 1로 반환 */
		int result = 0;		
		if(result1 == 1 && (files == null || result2 == files.size()) && (clientList == null || result3 == clientList.size()) && (userList == null || result4 == userList.size())) {
			result = 1;
		}
				
		return result;
	}

	/* 첨부파일 삭제 */
	/* DB에서 삭제하는 과정이므로 트랜잭션 처리를 해줌 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public Map<String, Object> deleteFile(String fileNo) {
	
		Map<String, Object> map = new HashMap<>();
		
		ProceedingsAttachmentDTO proceedingsAttachment = mapper.selectFile(fileNo);			//첨부파일 조회				
		int result = mapper.deleteFile(fileNo);												//첨부파일 DB에서 삭제 결과

		map.put("proceedingsAttachment", proceedingsAttachment);
		map.put("result", result);
		
		return map;
	}

	/* 회의 참석자(자사) 등록 */
	/* DB에 저장하는 과정이므로 트랜잭션 처리를 해줌 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertConferenceAttendeesUsers(String proceedingsNo, String userNo) {
		
		ConferenceAttendeesUsersDTO conferenceAttendeesUsers = new ConferenceAttendeesUsersDTO();
		conferenceAttendeesUsers.setProceedingsNo(proceedingsNo);
		conferenceAttendeesUsers.setUserNo(userNo);
		
		return mapper.insertConferenceAttendeesUsers(conferenceAttendeesUsers);
	}

	/* 회의 참석자(자사) 삭제 */
	/* DB에 삭제하는 과정이므로 트랜잭션 처리를 해줌 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int deleteConferenceAttendeesUsers(String proceedingsNo, String userNo) {

		ConferenceAttendeesUsersDTO conferenceAttendeesUsers = new ConferenceAttendeesUsersDTO();
		conferenceAttendeesUsers.setProceedingsNo(proceedingsNo);
		conferenceAttendeesUsers.setUserNo(userNo);
		
		return mapper.deleteConferenceAttendeesUsers(conferenceAttendeesUsers);
	}
	
	/* 회의 참석자(고객) 등록 */
	/* DB에 저장하는 과정이므로 트랜잭션 처리를 해줌 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertConferenceAttendeesClient(String proceedingsNo, String clientNo) {
		
		ConferenceAttendeesClientDTO conferenceAttendeesClient = new ConferenceAttendeesClientDTO();
		conferenceAttendeesClient.setClientNo(clientNo);
		conferenceAttendeesClient.setProceedingsNo(proceedingsNo);
		
		return mapper.insertConferenceAttendeesClient(conferenceAttendeesClient);
	}

	/* 회의 참석자(고객) 삭제 */
	/* DB에 저장하는 과정이므로 트랜잭션 처리를 해줌 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int deleteConferenceAttendeesClient(String proceedingsNo, String clientNo) {
		
		ConferenceAttendeesClientDTO conferenceAttendeesClient = new ConferenceAttendeesClientDTO();
		conferenceAttendeesClient.setClientNo(clientNo);
		conferenceAttendeesClient.setProceedingsNo(proceedingsNo);
		
		return mapper.deleteConferenceAttendeesClient(conferenceAttendeesClient);
	}

	/* 회의록 수정 */
	/* DB에 저장하는 과정이므로 트랜잭션 처리를 해줌 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int updateProceedings(ProceedingsCollectionDTO proceedingsCollection) {

		ProceedingsDTO proceedings = proceedingsCollection.getProceedings();
		
		List<ProceedingsAttachmentDTO> files = proceedingsCollection.getFiles();

		int result1 = mapper.updateProceedings(proceedings);	//회의록 수정

		String proceedingsNo = proceedings.getProceedingsNo();
		int result2 = 0;
		if(files != null) {
			for(ProceedingsAttachmentDTO proceedingsAttachment: files) {
				proceedingsAttachment.setProceedingsNo(proceedingsNo);
				result2 += mapper.insertProceedingsAttachmentByUpdate(proceedingsAttachment);	//첨부파일 등록
			}	
		}
		
		/* 모든 조건이 부합할 경우 result를 1로 반환 */
		int result = 0;		
		if(result1 == 1 && (files == null || result2 == files.size())) {
			result = 1;
		}
				
		return result;
	}

	/* 회의록 삭제 */
	/* DB에 저장하는 과정이므로 트랜잭션 처리를 해줌 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int deleteProceedings(String proceedingsNo) {
		
		return mapper.deleteProceedings(proceedingsNo);
	}
	
	
	
}
