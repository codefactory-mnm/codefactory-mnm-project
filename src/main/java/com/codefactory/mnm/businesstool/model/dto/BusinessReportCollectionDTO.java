package com.codefactory.mnm.businesstool.model.dto;

import java.io.Serializable;
import java.util.List;

public class BusinessReportCollectionDTO implements Serializable{

	private BusinessReportListDTO businessReport;
	private List<BusinessReportAttachmentsDTO> files;
	
	public BusinessReportCollectionDTO() {}

	public BusinessReportCollectionDTO(BusinessReportListDTO businessReport, List<BusinessReportAttachmentsDTO> files) {
		super();
		this.businessReport = businessReport;
		this.files = files;
	}

	public BusinessReportListDTO getBusinessReport() {
		return businessReport;
	}

	public void setBusinessReport(BusinessReportListDTO businessReport) {
		this.businessReport = businessReport;
	}

	public List<BusinessReportAttachmentsDTO> getFiles() {
		return files;
	}

	public void setFiles(List<BusinessReportAttachmentsDTO> files) {
		this.files = files;
	}

	@Override
	public String toString() {
		return "BusinessReportCollectionDTO [businessReport=" + businessReport + ", files=" + files + "]";
	}
	
	
}
