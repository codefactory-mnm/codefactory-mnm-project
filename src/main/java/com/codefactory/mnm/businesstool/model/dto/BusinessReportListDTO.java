package com.codefactory.mnm.businesstool.model.dto;

import java.sql.Date;

public class BusinessReportListDTO {
	
	private String businessReportNo;			//영업보고번호
	private java.sql.Date reportDate;			//보고일
	private java.sql.Date activityStartDate;	//시작일
	private java.sql.Date activityEndDate;		//종료일
	private String competitorTrend;				//시장경쟁사 동향
	private String etc;							//기타
	private java.sql.Date writeDate;			//작성일
	private String delYn;						//삭제 여부
	private String userNo;						//작성자번호
	private String name;						//작성자명
	private String fileName;					//파일명
	private String fileOriginalName;			//원본파일명
	private String fileNo;						//파일번호
	private String filePath;					//파일경로
	private String deptName;					//부서명
	private String planContent;				 	//영엉활동 계획
	private String activityClassification;		//활동 분류
	private String activityPurpose;				//활동 목적
	private String activityContent;				//활동 내용
	private String businessActivityNo;			//영업 활동 보고
	private String activityPerformanceText;		//영업활동 실적 내용
	private String planContentText;				//영업활동 계획 내용
	
	
	public BusinessReportListDTO() {}


	public BusinessReportListDTO(String businessReportNo, Date reportDate, Date activityStartDate, Date activityEndDate,
			String competitorTrend, String etc, Date writeDate, String delYn, String userNo, String name,
			String fileName, String fileOriginalName, String fileNo, String filePath, String deptName,
			String planContent, String activityClassification, String activityPurpose, String activityContent,
			String businessActivityNo, String activityPerformanceText, String planContentText) {
		super();
		this.businessReportNo = businessReportNo;
		this.reportDate = reportDate;
		this.activityStartDate = activityStartDate;
		this.activityEndDate = activityEndDate;
		this.competitorTrend = competitorTrend;
		this.etc = etc;
		this.writeDate = writeDate;
		this.delYn = delYn;
		this.userNo = userNo;
		this.name = name;
		this.fileName = fileName;
		this.fileOriginalName = fileOriginalName;
		this.fileNo = fileNo;
		this.filePath = filePath;
		this.deptName = deptName;
		this.planContent = planContent;
		this.activityClassification = activityClassification;
		this.activityPurpose = activityPurpose;
		this.activityContent = activityContent;
		this.businessActivityNo = businessActivityNo;
		this.activityPerformanceText = activityPerformanceText;
		this.planContentText = planContentText;
	}


	public String getBusinessReportNo() {
		return businessReportNo;
	}


	public void setBusinessReportNo(String businessReportNo) {
		this.businessReportNo = businessReportNo;
	}


	public java.sql.Date getReportDate() {
		return reportDate;
	}


	public void setReportDate(java.sql.Date reportDate) {
		this.reportDate = reportDate;
	}


	public java.sql.Date getActivityStartDate() {
		return activityStartDate;
	}


	public void setActivityStartDate(java.sql.Date activityStartDate) {
		this.activityStartDate = activityStartDate;
	}


	public java.sql.Date getActivityEndDate() {
		return activityEndDate;
	}


	public void setActivityEndDate(java.sql.Date activityEndDate) {
		this.activityEndDate = activityEndDate;
	}


	public String getCompetitorTrend() {
		return competitorTrend;
	}


	public void setCompetitorTrend(String competitorTrend) {
		this.competitorTrend = competitorTrend;
	}


	public String getEtc() {
		return etc;
	}


	public void setEtc(String etc) {
		this.etc = etc;
	}


	public java.sql.Date getWriteDate() {
		return writeDate;
	}


	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}


	public String getDelYn() {
		return delYn;
	}


	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}


	public String getUserNo() {
		return userNo;
	}


	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getFileName() {
		return fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public String getFileOriginalName() {
		return fileOriginalName;
	}


	public void setFileOriginalName(String fileOriginalName) {
		this.fileOriginalName = fileOriginalName;
	}


	public String getFileNo() {
		return fileNo;
	}


	public void setFileNo(String fileNo) {
		this.fileNo = fileNo;
	}


	public String getFilePath() {
		return filePath;
	}


	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}


	public String getDeptName() {
		return deptName;
	}


	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}


	public String getPlanContent() {
		return planContent;
	}


	public void setPlanContent(String planContent) {
		this.planContent = planContent;
	}


	public String getActivityClassification() {
		return activityClassification;
	}


	public void setActivityClassification(String activityClassification) {
		this.activityClassification = activityClassification;
	}


	public String getActivityPurpose() {
		return activityPurpose;
	}


	public void setActivityPurpose(String activityPurpose) {
		this.activityPurpose = activityPurpose;
	}


	public String getActivityContent() {
		return activityContent;
	}


	public void setActivityContent(String activityContent) {
		this.activityContent = activityContent;
	}


	public String getBusinessActivityNo() {
		return businessActivityNo;
	}


	public void setBusinessActivityNo(String businessActivityNo) {
		this.businessActivityNo = businessActivityNo;
	}


	public String getActivityPerformanceText() {
		return activityPerformanceText;
	}


	public void setActivityPerformanceText(String activityPerformanceText) {
		this.activityPerformanceText = activityPerformanceText;
	}


	public String getPlanContentText() {
		return planContentText;
	}


	public void setPlanContentText(String planContentText) {
		this.planContentText = planContentText;
	}

	@Override
	public String toString() {
		return "BusinessReportListDTO [businessReportNo=" + businessReportNo + ", reportDate=" + reportDate
				+ ", activityStartDate=" + activityStartDate + ", activityEndDate=" + activityEndDate
				+ ", competitorTrend=" + competitorTrend + ", etc=" + etc + ", writeDate=" + writeDate + ", delYn="
				+ delYn + ", userNo=" + userNo + ", name=" + name + ", fileName=" + fileName + ", fileOriginalName="
				+ fileOriginalName + ", fileNo=" + fileNo + ", filePath=" + filePath + ", deptName=" + deptName
				+ ", planContent=" + planContent + ", activityClassification=" + activityClassification
				+ ", activityPurpose=" + activityPurpose + ", activityContent=" + activityContent
				+ ", businessActivityNo=" + businessActivityNo + ", activityPerformanceText=" + activityPerformanceText
				+ ", planContentText=" + planContentText + "]";
	}

}
