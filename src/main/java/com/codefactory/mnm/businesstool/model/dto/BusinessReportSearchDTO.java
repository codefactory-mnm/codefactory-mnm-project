package com.codefactory.mnm.businesstool.model.dto;

import java.io.Serializable;

import com.codefactory.mnm.common.SelectCriteria;

public class BusinessReportSearchDTO implements Serializable{
	
	private String activityStartDate;			//시작 날짜
	private String activityEndDate;				//끝 날짜
	private String currentPage;					//현재페이지
	private SelectCriteria selectCriteria;		//페이징처리
	
	public BusinessReportSearchDTO() {}

	public BusinessReportSearchDTO(String activityStartDate, String activityEndDate, String currentPage,
			SelectCriteria selectCriteria) {
		super();
		this.activityStartDate = activityStartDate;
		this.activityEndDate = activityEndDate;
		this.currentPage = currentPage;
		this.selectCriteria = selectCriteria;
	}

	public String getActivityStartDate() {
		return activityStartDate;
	}

	public void setActivityStartDate(String activityStartDate) {
		this.activityStartDate = activityStartDate;
	}

	public String getActivityEndDate() {
		return activityEndDate;
	}

	public void setActivityEndDate(String activityEndDate) {
		this.activityEndDate = activityEndDate;
	}

	public String getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(String currentPage) {
		this.currentPage = currentPage;
	}

	public SelectCriteria getSelectCriteria() {
		return selectCriteria;
	}

	public void setSelectCriteria(SelectCriteria selectCriteria) {
		this.selectCriteria = selectCriteria;
	}

	@Override
	public String toString() {
		return "BusinessReportSearchDTO [activityStartDate=" + activityStartDate + ", activityEndDate="
				+ activityEndDate + ", currentPage=" + currentPage + ", selectCriteria=" + selectCriteria + "]";
	}
	
}
