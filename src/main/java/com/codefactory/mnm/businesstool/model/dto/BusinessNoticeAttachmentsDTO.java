package com.codefactory.mnm.businesstool.model.dto;

public class BusinessNoticeAttachmentsDTO implements java.io.Serializable{
	
	private String fileNo;				//파일번호
	private String fileName;			//파일명
	private String fileOriginalName;	//실제파일명
	private String filePath;			//경로
	private String division;			//구분
	private String businessReportNo;	//영업보고번호
	private String proceedingsNo;		//회으록번호
	private String businessNoticeNo;	//영업공지번호
	
	public BusinessNoticeAttachmentsDTO() {}

	public BusinessNoticeAttachmentsDTO(String fileNo, String fileName, String fileOriginalName, String filePath,
			String division, String businessReportNo, String proceedingsNo, String businessNoticeNo) {
		super();
		this.fileNo = fileNo;
		this.fileName = fileName;
		this.fileOriginalName = fileOriginalName;
		this.filePath = filePath;
		this.division = division;
		this.businessReportNo = businessReportNo;
		this.proceedingsNo = proceedingsNo;
		this.businessNoticeNo = businessNoticeNo;
	}

	public String getFileNo() {
		return fileNo;
	}

	public void setFileNo(String fileNo) {
		this.fileNo = fileNo;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileOriginalName() {
		return fileOriginalName;
	}

	public void setFileOriginalName(String fileOriginalName) {
		this.fileOriginalName = fileOriginalName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getBusinessReportNo() {
		return businessReportNo;
	}

	public void setBusinessReportNo(String businessReportNo) {
		this.businessReportNo = businessReportNo;
	}

	public String getProceedingsNo() {
		return proceedingsNo;
	}

	public void setProceedingsNo(String proceedingsNo) {
		this.proceedingsNo = proceedingsNo;
	}

	public String getBusinessNoticeNo() {
		return businessNoticeNo;
	}

	public void setBusinessNoticeNo(String businessNoticeNo) {
		this.businessNoticeNo = businessNoticeNo;
	}

	@Override
	public String toString() {
		return "BusinessNoticeAttachmentsDTO [fileNo=" + fileNo + ", fileName=" + fileName + ", fileOriginalName="
				+ fileOriginalName + ", filePath=" + filePath + ", division=" + division + ", businessReportNo="
				+ businessReportNo + ", proceedingsNo=" + proceedingsNo + ", businessNoticeNo=" + businessNoticeNo
				+ "]";
	}
	
}
