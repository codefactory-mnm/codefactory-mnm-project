package com.codefactory.mnm.businesstool.model.service;

import java.util.Map;

import com.codefactory.mnm.businesstool.model.dto.BusinessNoticeAttachmentsDTO;
import com.codefactory.mnm.businesstool.model.dto.BusinessNoticeCollectionDTO;
import com.codefactory.mnm.businesstool.model.dto.BusinessNoticeListDTO;
import com.codefactory.mnm.businesstool.model.dto.BusinessNoticeSearchDTO;

public interface BusinessNoticeService {
 
	int insertBusinessNotice(BusinessNoticeCollectionDTO businessNoticeCollection);

	String selectUserNo(String id);

	Map<String, Object> selectBusinessNoticeList(BusinessNoticeSearchDTO businessNoticeSearchDTO);

	Map<String, Object> selectBusinessNotice(String businessNoticeNo);

	int deleteBusinessNotice(String businessNoticeNo);

	int updateBusinessNotice(BusinessNoticeCollectionDTO businessNoticeCollection);

	Map<String, Object> deleteFile(String businessFileNo);

	BusinessNoticeAttachmentsDTO selectFile(String businessFileNo);

	int selectCount(BusinessNoticeListDTO oneBusinessNotice);






}
