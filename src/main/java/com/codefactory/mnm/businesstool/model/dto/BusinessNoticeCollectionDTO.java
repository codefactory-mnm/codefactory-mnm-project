package com.codefactory.mnm.businesstool.model.dto;

import java.io.Serializable;
import java.util.List;

public class BusinessNoticeCollectionDTO implements Serializable{

	private BusinessNoticeListDTO businessNotice;
	private List<BusinessNoticeAttachmentsDTO> files;

	public BusinessNoticeCollectionDTO() {}

	public BusinessNoticeCollectionDTO(BusinessNoticeListDTO businessNotice, List<BusinessNoticeAttachmentsDTO> files) {
		super();
		this.businessNotice = businessNotice;
		this.files = files;
	}

	public BusinessNoticeListDTO getBusinessNotice() {
		return businessNotice;
	}

	public void setBusinessNotice(BusinessNoticeListDTO businessNotice) {
		this.businessNotice = businessNotice;
	}

	public List<BusinessNoticeAttachmentsDTO> getFiles() {
		return files;
	}

	public void setFiles(List<BusinessNoticeAttachmentsDTO> files) {
		this.files = files;
	}

	@Override
	public String toString() {
		return "BusinessNoticeCollectionDTO [businessNotice=" + businessNotice + ", files=" + files + "]";
	}

}
