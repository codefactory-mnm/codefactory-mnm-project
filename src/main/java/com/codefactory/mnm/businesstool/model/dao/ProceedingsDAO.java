package com.codefactory.mnm.businesstool.model.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.codefactory.mnm.businesstool.model.dto.ClientCompanyDTO;
import com.codefactory.mnm.businesstool.model.dto.ClientDTO;
import com.codefactory.mnm.businesstool.model.dto.ConferenceAttendeesClientDTO;
import com.codefactory.mnm.businesstool.model.dto.ConferenceAttendeesUsersDTO;
import com.codefactory.mnm.businesstool.model.dto.ProceedingsAttachmentDTO;
import com.codefactory.mnm.businesstool.model.dto.ProceedingsDTO;
import com.codefactory.mnm.businesstool.model.dto.ProceedingsSearchDTO;
import com.codefactory.mnm.businesstool.model.dto.UserDTO;

@Mapper
public interface ProceedingsDAO {

	int selectTotalCount(ProceedingsSearchDTO proceedingsSearch);										//총 게시물 수 조회
	
	List<ProceedingsDTO> selectProceedingsList(ProceedingsSearchDTO proceedingsSearch);					//회의록 리스트 조회
	
	ProceedingsDTO selectProceedings(String proceedingsNo);												//회의록 조회
	
	List<ConferenceAttendeesUsersDTO> selectConferenceAttendeesUsers(String proceedingsNo);				//회의 참가자(자사) 리스트 조회
	
	List<ConferenceAttendeesClientDTO> selectConferenceAttendeesClient(String proceedingsNo);			//회의 참가자(고객) 리스트 조회
	
	List<ProceedingsAttachmentDTO> selectProceedingsAttachment(String proceedingsNo);					//회의록 첨부파일 리스트 조회
	
	List<ClientCompanyDTO> selectClientCompanyList();													//고객사 리스트 조회

	List<ClientDTO> selectClientList(String clientCompanyNo);											//회의 참석자(고객) 리스트 조회

	List<UserDTO> selectUserList();																		//회의 참석자(자사) 리스트 조회

	int insertProceedings(ProceedingsDTO proceedings);													//회의록 등록
	
	int insertProceedingsAttachment(ProceedingsAttachmentDTO proceedingsAttachment);					//회의록 첨부파일리스트 등록
	
	int insertConferenceClient(ClientDTO client);														//회의 참석자(고객) 등록

	int insertConferenceUser(UserDTO user);																//회의 참석자(자사) 등록

	ProceedingsAttachmentDTO selectFile(String fileNo);													//첨부파일 조회
	
	int deleteFile(String fileNo);																		//첨부파일 삭제
	
	int insertConferenceAttendeesUsers(ConferenceAttendeesUsersDTO conferenceAttendeesUsers);			//회의 참석자(자사) 등록
	
	int deleteConferenceAttendeesUsers(ConferenceAttendeesUsersDTO conferenceAttendeesUsers);			//회의 참석자(자사) 삭제
	
	int insertConferenceAttendeesClient(ConferenceAttendeesClientDTO conferenceAttendeesClient);		//회의 참석자(고객) 등록
	
	int deleteConferenceAttendeesClient(ConferenceAttendeesClientDTO conferenceAttendeesClient);		//회의 참석자(고객) 삭제

	int updateProceedings(ProceedingsDTO proceedings);													//회의록 수정

	int insertProceedingsAttachmentByUpdate(ProceedingsAttachmentDTO proceedingsAttachment);			//회의록 수정 시 파일첨부리스트 등록

	int deleteProceedings(String proceedingsNo);														//회의록 삭제











}
