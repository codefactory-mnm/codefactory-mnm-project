package com.codefactory.mnm.businesstool.model.dto;

import com.codefactory.mnm.common.SelectCriteria;

public class BusinessNoticeSearchDTO implements java.io.Serializable {
	
	private String startDate;					//시작 날짜
	private String endDate;						//끝 날짜
	private String currentPage;					//현재페이지
	private SelectCriteria selectCriteria;		//페이징처리
	
	public BusinessNoticeSearchDTO() {}

	public BusinessNoticeSearchDTO(String startDate, String endDate, String currentPage,
			SelectCriteria selectCriteria) {
		super();
		this.startDate = startDate;
		this.endDate = endDate;
		this.currentPage = currentPage;
		this.selectCriteria = selectCriteria;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(String currentPage) {
		this.currentPage = currentPage;
	}

	public SelectCriteria getSelectCriteria() {
		return selectCriteria;
	}

	public void setSelectCriteria(SelectCriteria selectCriteria) {
		this.selectCriteria = selectCriteria;
	}

	@Override
	public String toString() {
		return "BusinessNoticeSearchDTO [startDate=" + startDate + ", endDate=" + endDate + ", currentPage="
				+ currentPage + ", selectCriteria=" + selectCriteria + "]";
	}

	
}
