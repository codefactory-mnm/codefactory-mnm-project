package com.codefactory.mnm.businesstool.model.dto;

import java.sql.Date;

public class ClientDTO implements java.io.Serializable {

	private String clientNo;			//고객번호
	private String clientName;			//고객명
	private String clientCompanyName;	//고객사명
	private java.sql.Date writeDate;	//작성일자
	
	public ClientDTO() {}

	public ClientDTO(String clientNo, String clientName, String clientCompanyName, Date writeDate) {
		super();
		this.clientNo = clientNo;
		this.clientName = clientName;
		this.clientCompanyName = clientCompanyName;
		this.writeDate = writeDate;
	}

	public String getClientNo() {
		return clientNo;
	}

	public void setClientNo(String clientNo) {
		this.clientNo = clientNo;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getClientCompanyName() {
		return clientCompanyName;
	}

	public void setClientCompanyName(String clientCompanyName) {
		this.clientCompanyName = clientCompanyName;
	}

	public java.sql.Date getWriteDate() {
		return writeDate;
	}

	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}

	@Override
	public String toString() {
		return "ClientDTO [clientNo=" + clientNo + ", clientName=" + clientName + ", clientCompanyName="
				+ clientCompanyName + ", writeDate=" + writeDate + "]";
	}

}
