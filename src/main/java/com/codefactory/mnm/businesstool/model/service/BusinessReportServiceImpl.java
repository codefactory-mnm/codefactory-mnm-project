package com.codefactory.mnm.businesstool.model.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.codefactory.mnm.businessactivity.model.dto.BusinessActivityListDTO;
import com.codefactory.mnm.businesstool.model.dao.BusinessReportDAO;
import com.codefactory.mnm.businesstool.model.dto.BusinessReportAttachmentsDTO;
import com.codefactory.mnm.businesstool.model.dto.BusinessReportCollectionDTO;
import com.codefactory.mnm.businesstool.model.dto.BusinessReportListDTO;
import com.codefactory.mnm.businesstool.model.dto.BusinessReportSearchDTO;
import com.codefactory.mnm.client.model.dto.UsersDTO;
import com.codefactory.mnm.common.Pagenation;
import com.codefactory.mnm.common.SelectCriteria;

@Service
public class BusinessReportServiceImpl implements BusinessReportService{
	
	private final BusinessReportDAO businessReportDAO;
	
	@Autowired
	public BusinessReportServiceImpl(BusinessReportDAO businessReportDAO) {
		this.businessReportDAO = businessReportDAO;
	}
	
	/* 영업보고 목록 조회 */
	@Override
	public Map<String, Object> selectBusinessReportList(BusinessReportSearchDTO businessReportSearch) {
		
		/* 첫 요청페이지 */
		int pageNo = 1;		
		
		/* 요청 페이지가 0이하면 1로 바꿔줌 */
		if(pageNo <= 0) {
			pageNo = 1;
		}
		
		/* 최대 페이지 */
		int limit = 10;
		
		/* 최대 버튼 갯수 */
		int buttonAmount = 5;
		
		/* 전체 게시물 수, 검색조건이 있다면 검색조건에 맞는 게시물 수 */
		int totalCount = businessReportDAO.selectTotalCount(businessReportSearch);

		/* view에서 전달받은 요청페이지 */
		String currentPage = businessReportSearch.getCurrentPage();
		
		/* view에서 전달받은 요청페이지를 pageNo에 저장 */
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		/* 설정한 변수에 따른 페이징조건 */
		SelectCriteria selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount);

		/* 조회 결과를 담을 Map 생성 */
		Map<String, Object> map = new HashMap<>();
		
		businessReportSearch.setSelectCriteria(selectCriteria);
		
		/* 공지 조회 */
		List<BusinessReportListDTO> businessReportList = businessReportDAO.selectBusinessReportList(businessReportSearch);
		
		/* 공지 게시물 갯수 조회 */
		int businessReportListCount = businessReportDAO.selectBusinessReportListCount();
		
		map.put("businessReportList", businessReportList);
		map.put("businessReportListCount", businessReportListCount);
		map.put("selectCriteria", selectCriteria);
		map.put("totalCount", totalCount);
		
		return map;
	}
	
	/* 영업보고 등록 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertBusinessReport(BusinessReportCollectionDTO businessReportCollection) {

		/* controller에서 받아온 정보를 꺼낸다. */
		BusinessReportListDTO businessReport = businessReportCollection.getBusinessReport();
		List<BusinessReportAttachmentsDTO> files = businessReportCollection.getFiles();
		/* 공지 작성내용을 DB에 저장한다. */
		int result1 = businessReportDAO.insertBusinessReport(businessReport);
		
		/* 파일첨부정보를 DB에 저장한 횟수를 저장한다. */
		int result2 = 0;
		if(files != null) {
			for(BusinessReportAttachmentsDTO BusinessReportAttachment : files) {
				result2 += businessReportDAO.insertBusinessReportAttachment(BusinessReportAttachment);
			}	
		}
		
		/* 파일첨부가 없는 경우 : 공지를 정상적으로 등록하였을 경우 result = 1을 반환
		 * 파일첨부가 있는 경우 : 공지, 첨부파일을 정상적으로 등록하였을 경우 result = 1을 반환
		 */
		int result = 0;
		if(result1 == 1 &&  (files == null || result2 == files.size())) {
			result = 1;
		}
		
		return result;
	}

	@Override
	public UsersDTO selectUserNo(String id) {
		UsersDTO userDto = businessReportDAO.selectUserNo(id);
		
		return userDto;
	}
	/* 영업보고 상세 조회 */
	@Override
	public Map<String, Object> selectBusinessReport(String businessReportNo) {

		Map<String, Object> map = new HashMap<>();
		
		BusinessReportListDTO oneBusinessReport = businessReportDAO.selectBusinessReport(businessReportNo);
		List<BusinessReportListDTO> fileList = businessReportDAO.selectFileList(businessReportNo);
		
		map.put("oneBusinessReport", oneBusinessReport);
		map.put("fileList", fileList);
		
		return map;
	}
	
	/* 영업보고 삭제 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int deleteBusinessReport(String businessReportNo) {

		int result = businessReportDAO.deleteBusinessReport(businessReportNo);
		
		return result;
	}

	public List<BusinessActivityListDTO> selectBusinessActivityList() {
		
		return businessReportDAO.selectBusinessActivityList();
	}
}
