package com.codefactory.mnm.businesstool.model.dto;

public class ConferenceAttendeesUsersDTO implements java.io.Serializable {

	private String proceedingsNo;		//회의록번호
	private String userNo;				//회의참석자(자사)번호
	private String name;				//회의참석자(자사)명
		
	public ConferenceAttendeesUsersDTO() {}

	public ConferenceAttendeesUsersDTO(String proceedingsNo, String userNo, String name) {
		super();
		this.proceedingsNo = proceedingsNo;
		this.userNo = userNo;
		this.name = name;
	}

	public String getProceedingsNo() {
		return proceedingsNo;
	}

	public void setProceedingsNo(String proceedingsNo) {
		this.proceedingsNo = proceedingsNo;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "ConferenceAttendeesUsersDTO [proceedingsNo=" + proceedingsNo + ", userNo=" + userNo + ", name=" + name
				+ "]";
	}
	
	
}
