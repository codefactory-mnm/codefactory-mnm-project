package com.codefactory.mnm.businesstool.model.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.codefactory.mnm.businesstool.model.dao.BusinessNoticeDAO;
import com.codefactory.mnm.businesstool.model.dto.BusinessNoticeAttachmentsDTO;
import com.codefactory.mnm.businesstool.model.dto.BusinessNoticeCollectionDTO;
import com.codefactory.mnm.businesstool.model.dto.BusinessNoticeListDTO;
import com.codefactory.mnm.businesstool.model.dto.BusinessNoticeSearchDTO;
import com.codefactory.mnm.client.model.dto.UsersDTO;
import com.codefactory.mnm.common.Pagenation;
import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.proposal.model.dto.ProposalAttachmentDTO;

@Service
public class BusinessNoticeServiceImpl implements BusinessNoticeService{
 
	private final BusinessNoticeDAO businessNoticeDAO;
	
	@Autowired
	public BusinessNoticeServiceImpl(BusinessNoticeDAO businessNoticeDAO) {
		this.businessNoticeDAO = businessNoticeDAO;
	}
	
	/* 공지 등록 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertBusinessNotice(BusinessNoticeCollectionDTO businessNoticeCollection) {
		
		/* controller에서 받아온 정보를 꺼낸다. */
		BusinessNoticeListDTO businessNotice = businessNoticeCollection.getBusinessNotice();
		List<BusinessNoticeAttachmentsDTO> files = businessNoticeCollection.getFiles();
		
		/* 공지 작성내용을 DB에 저장한다. */
		int result1 = businessNoticeDAO.insertBusinessNotice(businessNotice);
		
		/* 파일첨부정보를 DB에 저장한 횟수를 저장한다. */
		int result2 = 0;
		if(files != null) {
			for(BusinessNoticeAttachmentsDTO BusinessNoticeAttachment : files) {
				result2 += businessNoticeDAO.insertBusinessNoticeAttachment(BusinessNoticeAttachment);
			}	
		}
		
		/* 파일첨부가 없는 경우 : 공지를 정상적으로 등록하였을 경우 result = 1을 반환
		 * 파일첨부가 있는 경우 : 공지, 첨부파일을 정상적으로 등록하였을 경우 result = 1을 반환
		 */
		int result = 0;
		if(result1 == 1 &&  (files == null || result2 == files.size())) {
			result = 1;
		}
		return result;
	}
	
	/* 공지 목록 조회 */
	@Override
	public Map<String, Object> selectBusinessNoticeList(BusinessNoticeSearchDTO businessNoticeSearchDTO) {
		
		/* 첫 요청페이지 */
		int pageNo = 1;		
		
		/* 요청 페이지가 0이하면 1로 바꿔줌 */
		if(pageNo <= 0) {
			pageNo = 1;
		}
		
		/* 최대 페이지 */
		int limit = 10;
		
		/* 최대 버튼 갯수 */
		int buttonAmount = 5;
		
		/* 전체 게시물 수, 검색조건이 있다면 검색조건에 맞는 게시물 수 */
		int totalCount = businessNoticeDAO.selectTotalCount(businessNoticeSearchDTO);

		/* view에서 전달받은 요청페이지 */
		String currentPage = businessNoticeSearchDTO.getCurrentPage();
		
		/* view에서 전달받은 요청페이지를 pageNo에 저장 */
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		/* 설정한 변수에 따른 페이징조건 */
		SelectCriteria selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount);

		/* 조회 결과를 담을 Map 생성 */
		Map<String, Object> map = new HashMap<>();
		
		businessNoticeSearchDTO.setSelectCriteria(selectCriteria);

		/* 공지 조회 */
		List<BusinessNoticeListDTO> businessNoticeList = businessNoticeDAO.selectBusinessNoticeList(businessNoticeSearchDTO);
		
		/* 공지 게시물 갯수 조회 */
		int businessNoticeListCount = businessNoticeDAO.selectBusinessNoticeListCount();
		
		map.put("businessNoticeList", businessNoticeList);
		map.put("businessNoticeListCount", businessNoticeListCount);
		map.put("selectCriteria", selectCriteria);
		map.put("totalCount", totalCount);
		
		return map;
	}

	@Override
	public String selectUserNo(String id) {
		
		UsersDTO userNo = businessNoticeDAO.selectUserNo(id);
			
		return userNo.getUserNo();
	}
	
	/* 공지 상세 조회 */
	@Override
	public Map<String, Object> selectBusinessNotice(String businessNoticeNo) {
		
		Map<String, Object> map = new HashMap<>();
		
		BusinessNoticeListDTO oneBusinessNotice = businessNoticeDAO.selectBusinessNotice(businessNoticeNo);
		List<BusinessNoticeListDTO> fileList = businessNoticeDAO.selectFileList(businessNoticeNo);
		
		map.put("oneBusinessNotice", oneBusinessNotice);
		map.put("fileList", fileList);
		
		return map;
	}
	
	/* 공지 삭제 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int deleteBusinessNotice(String businessNoticeNo) {
		
		int result = businessNoticeDAO.deleteBusinessNotice(businessNoticeNo);
		
		return result;
	}
	
	/* 공지 수정 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int updateBusinessNotice(BusinessNoticeCollectionDTO businessNoticeCollection) {
		
		/* controller에서 받아온 정보를 꺼낸다. */
		BusinessNoticeListDTO businessNotice = businessNoticeCollection.getBusinessNotice();
		
		List<BusinessNoticeAttachmentsDTO> files = businessNoticeCollection.getFiles();
		
		/* 영업 공지 작성내용을 DB에 저장한다. */
		int result1 = businessNoticeDAO.updateBusinessNotice(businessNotice);
		
		String businessNoticeNo = businessNotice.getBusinessNoticeNo();
		
		/* 파일첨부정보를 DB에 저장한 횟수를 저장한다. */
		int result2 = 0;
		if(files != null) {
			for(BusinessNoticeAttachmentsDTO BusinessNoticeAttachment : files) {
				BusinessNoticeAttachment.setBusinessNoticeNo(businessNoticeNo);
				result2 += businessNoticeDAO.updateBusinessNoticeAttachment(BusinessNoticeAttachment);
			}	
		}
		
		/* 파일첨부가 없는 경우 : 영업 공지를 정상적으로 등록하였을 경우 result = 1을 반환
		 * 파일첨부가 있는 경우 : 영업 공지, 첨부파일을 정상적으로 등록하였을 경우 result = 1을 반환
		 */
		int result = 0;
		if(result1 == 1 &&  (files == null || result2 == files.size())) {
			result = 1;
		}
		return result;
	}

	/* 파일 정보 조회 */
	@Override
	public BusinessNoticeAttachmentsDTO selectFile(String businessFileNo) {

		return businessNoticeDAO.selectFile(businessFileNo);
	}
	
	/* 첨부파일 삭제 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public Map<String, Object> deleteFile(String businessFileNo) {

		Map<String, Object> map = new HashMap<>();
		
		BusinessNoticeAttachmentsDTO File = businessNoticeDAO.selectFile(businessFileNo);		//첨부파일정보				
		int result = businessNoticeDAO.deleteFile(businessFileNo);								//첨부파일 DB에서 삭제 결과
		
		/* map에 담아서 controller에 전달 */
		map.put("File", File);
		map.put("result", result);
		
		return map;
	}

	@Override
	public int selectCount(BusinessNoticeListDTO oneBusinessNotice) {

		int selectCount = businessNoticeDAO.selectCount(oneBusinessNotice);
		
		return selectCount;
	}


}




