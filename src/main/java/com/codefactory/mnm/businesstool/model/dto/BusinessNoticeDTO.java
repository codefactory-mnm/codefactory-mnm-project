package com.codefactory.mnm.businesstool.model.dto;

import java.io.Serializable;
import java.util.Date;

public class BusinessNoticeDTO implements Serializable{
	
	private String businessNoticeNo;	//영업공지번호
	private String title;				//제목
	private String mustReadYn;			//필독여부
	private String content;				//내용
	private int count;					//조회수
	private java.sql.Date writeDate;	//작성일자
	private String userNo;				//작성자번호
	private String delYn;				//삭제여부
	
	private String name;				//작성자
	
	public BusinessNoticeDTO() {}

	public BusinessNoticeDTO(String businessNoticeNo, String title, String mustReadYn, String content, int count,
			java.sql.Date writeDate, String userNo, String delYn, String name) {
		super();
		this.businessNoticeNo = businessNoticeNo;
		this.title = title;
		this.mustReadYn = mustReadYn;
		this.content = content;
		this.count = count;
		this.writeDate = writeDate;
		this.userNo = userNo;
		this.delYn = delYn;
		this.name = name;
	}

	public String getBusinessNoticeNo() {
		return businessNoticeNo;
	}

	public void setBusinessNoticeNo(String businessNoticeNo) {
		this.businessNoticeNo = businessNoticeNo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMustReadYn() {
		return mustReadYn;
	}

	public void setMustReadYn(String mustReadYn) {
		this.mustReadYn = mustReadYn;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public java.sql.Date getWriteDate() {
		return writeDate;
	}

	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getDelYn() {
		return delYn;
	}

	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "BusinessNoticeDTO [businessNoticeNo=" + businessNoticeNo + ", title=" + title + ", mustReadYn="
				+ mustReadYn + ", content=" + content + ", count=" + count + ", writeDate=" + writeDate + ", userNo="
				+ userNo + ", delYn=" + delYn + ", name=" + name + "]";
	}


	
}
