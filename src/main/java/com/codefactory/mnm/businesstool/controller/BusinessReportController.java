package com.codefactory.mnm.businesstool.controller;

import java.io.File;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.codefactory.mnm.businessactivity.model.dto.BusinessActivityListDTO;
import com.codefactory.mnm.businesstool.model.dto.BusinessReportAttachmentsDTO;
import com.codefactory.mnm.businesstool.model.dto.BusinessReportCollectionDTO;
import com.codefactory.mnm.businesstool.model.dto.BusinessReportListDTO;
import com.codefactory.mnm.businesstool.model.dto.BusinessReportSearchDTO;
import com.codefactory.mnm.businesstool.model.service.BusinessReportService;
import com.codefactory.mnm.client.model.dto.UsersDTO;
import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.estimate.model.dto.BusinessOpportunityListDTO;

@Controller
@RequestMapping("/businessReport")
public class BusinessReportController {
	
	private BusinessReportService businessReportService;
	
	public BusinessReportController(BusinessReportService businessReportService) {
		this.businessReportService = businessReportService;
	}
	
	/* 목록 조회 (검색조건을 가져온다) */ 
	@GetMapping("/list")
	public ModelAndView selectBusinessReportList(ModelAndView mv,
			@RequestParam(defaultValue = "") @DateTimeFormat(pattern = "yyyyMMdd") String activityStartDate,
			@RequestParam(defaultValue = "") @DateTimeFormat(pattern = "yyyyMMdd") String activityEndDate,
			@RequestParam(defaultValue = "1") String currentPage, Principal pcp) {
		
		/* 리스트를 조회하거나 또는 검색조건을 설정하지 않았을 때를 대비 */
		if(activityStartDate.equals("")) { // 검색조건 중 activityStartDate을 입력하지 않았을 경우
			activityStartDate = null;
		}
		
		if(activityEndDate.equals("")) { // 검색조건 중 activityEndDate를 입력하지 않았을 경우
			activityEndDate = null;
		}
		
		/* 검색조건을 담는다. 검색조건이 없을 경우 null로 담김. */
		BusinessReportSearchDTO businessReportSearch = new BusinessReportSearchDTO();
		businessReportSearch.setActivityStartDate(activityStartDate);
		businessReportSearch.setActivityEndDate(activityEndDate);
		businessReportSearch.setCurrentPage(currentPage);
		
		/* 검색조건을 service로 보내주고, 값을 map으로 전달받음 */
		Map<String, Object> map = businessReportService.selectBusinessReportList(businessReportSearch);
		
		/* db에서 가져온 값을 꺼낸다. */
		List<BusinessReportListDTO> businessReportList = (List<BusinessReportListDTO>) map.get("businessReportList");
		int businessReportListCount = (int) map.get("businessReportListCount");
		SelectCriteria selectCriteria = (SelectCriteria) map.get("selectCriteria");
		int totalCount = (int) map.get("totalCount");
		
		/* db에서 조회해온 결과를 담는다 */
		mv.addObject("businessReportSearch", businessReportSearch);
		mv.addObject("businessReportList", businessReportList);
		mv.addObject("businessReportListCount", businessReportListCount);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("totalCount", totalCount);
		mv.setViewName("businessReport/list");
		
		return mv;
	}
	
	/* 상세조회 */
	@GetMapping("/detail")
	public ModelAndView selectBusinessNotice(ModelAndView mv, @RequestParam String businessReportNo){
		
		/* Map을 생성하여 서비스에서 값을 리턴 받는다*/
		Map<String, Object> map = businessReportService.selectBusinessReport(businessReportNo);
		
		/* map에 담은 값을 꺼낸다 */
		BusinessReportListDTO oneBusinessReport = (BusinessReportListDTO) map.get("oneBusinessReport");
		List<BusinessReportAttachmentsDTO> fileList = (List<BusinessReportAttachmentsDTO>) map.get("fileList");
		
		/* map에서 꺼낸 값들을 담는다 */
		mv.addObject("oneBusinessReport", oneBusinessReport);
		mv.addObject("fileList", fileList);
		mv.setViewName("businessReport/detail");
		
		return mv;
	}
	
	@GetMapping("/insert")
	public void insertBusinessReportPage() {}
	
	/* 영업보고 등록 */
	@PostMapping("/insert")
	public ModelAndView insertBusinessReport(ModelAndView mv, BusinessReportListDTO businessReport,
	List<MultipartFile> multiFiles, HttpServletRequest request, RedirectAttributes rttr, Principal pcp) {
		
			UsersDTO userDTO = businessReportService.selectUserNo(pcp.getName());
			businessReport.setName(userDTO.getName());
			businessReport.setDeptName(userDTO.getDeptName());
			
		/* 영업 보고 정보 입력과 동시에 파일첨부도 같이 등록한 경우 */
		if(multiFiles.size() > 0 && !multiFiles.get(0).getOriginalFilename().equals("")) {
			
			String root = request.getSession().getServletContext().getRealPath("/");		// 첨부파일 저장경로(절대경로) 
			String filePath = root + "static/upload/";										// 첨부파일 저장경로(최종경로)
			
			/* 첨부파일 저장경로 (최종경로) 파일이 존재 하지 않을 경우 생성 */
			File mkdir = new File(filePath);
			if(!mkdir.exists()) {
				mkdir.mkdirs();
			}
			
			/* 다중 첨부파일이기 때문에, 여러 개의 파일을 담기 위해 List<BusinessReportAttachmentsDTO> 타입으로 생성 */
			List<BusinessReportAttachmentsDTO> files = new ArrayList<>();
			
			/* 다중 첨부파일은 반복문을 통해서, 미리 선언한 files에 담음  */
			for(int i = 0; i < multiFiles.size(); i++) {
				String fileOriginalName = multiFiles.get(i).getOriginalFilename();				// 원본파일명
				String ext = fileOriginalName.substring(fileOriginalName.lastIndexOf("."));		// 확장자명
				String fileName = UUID.randomUUID().toString().replace("-", "") + ext;			// 저장파일명
				
				/* 영업보고 파일첨부DTO인 file에 정보를 담은 후 리스트인 files에 담음 */
				BusinessReportAttachmentsDTO file = new BusinessReportAttachmentsDTO();
				file.setFileName(fileName);
				file.setFileOriginalName(fileOriginalName);
				file.setFilePath(filePath);

				files.add(file);
			}
			
			/* 영업보고 등록시 필요한 정보를 businessNoticeCollection에 담는다 */
			BusinessReportCollectionDTO businessReportCollection = new BusinessReportCollectionDTO();
			businessReportCollection.setBusinessReport(businessReport);
			businessReportCollection.setFiles(files);
			
			/* 서비스로 businessReportCollection를 보낸다 */
			int result = businessReportService.insertBusinessReport(businessReportCollection);
			
			/* DB에 등록을 성공하였을 경우 */
			if(result > 0 ) {
			
			/* 파일 저장 */
			try {
				for(int i = 0; i < multiFiles.size(); i++) {

					BusinessReportAttachmentsDTO file = files.get(i);			
					multiFiles.get(i).transferTo(new File(filePath + "\\" + file.getFileName()));

				}

				rttr.addFlashAttribute("successMessage", "영업 보고 등록에 성공하셨습니다.");
				
				/* 파일 저장을 실패하였을 경우, 저장했던 파일을 삭제
				 * 트랜젝션처리를 통해서 DB에 입력된 정보도 삭제됨 */
			} catch (Exception e) {
				e.printStackTrace();

				/* 실패 시 파일 삭제 */
				for(int i = 0; i < multiFiles.size(); i++) {
					
					BusinessReportAttachmentsDTO file = files.get(i); 
					new File(filePath + "\\" + file.getFileName()).delete(); 
				}

				rttr.addFlashAttribute("successMessage", "영업 보고 등록후 파일 업로드에 실패하셨습니다.");
			}
			
			/* DB에 제안 등록을 실패하였을 경우 */
			} else {
			
			rttr.addAttribute("message", "영업 보고 등록에 실패하셨습니다.");
				
		} 
		
		/* 파일첨부를 하지 않고, 제안 등록을 하는 경우 */
		} else {
			
			/* 공지 등록시 필요한 정보를 businessReportCollection에 담는다 */
			BusinessReportCollectionDTO businessReportCollection = new BusinessReportCollectionDTO();
			businessReportCollection.setBusinessReport(businessReport);
			/* 서비스로 businessReportCollection를 전달한다 */
			int result = businessReportService.insertBusinessReport(businessReportCollection);

			if(result > 0) {
				rttr.addFlashAttribute("successMessage", "영업 보고 등록에 성공하셨습니다.");
			} else {
				rttr.addFlashAttribute("successMessage", "영업 보고 등록에 실패하셨습니다.");
			}
			
		}
		/* 공지 등록 후 연결할 페이지 */
		mv.setViewName("redirect:/businessReport/list");
		
		return mv;
	}
	
	/* 영업보고 삭제 */
	@GetMapping("/delete")
	public ModelAndView deleteBusinessNotice(ModelAndView mv, @RequestParam String businessReportNo,
			RedirectAttributes rttr) {
		 
		/* 공지 번호를 받아서 해당 게시글을 삭제 한다 */
		int result = businessReportService.deleteBusinessReport(businessReportNo);
		
		if(result > 0) {
			rttr.addFlashAttribute("message", "영업보고 삭제를 성공하였습니다.");
		} else {
			rttr.addFlashAttribute("message", "영업보고 삭제를 실패하였습니다.");
		}
		
		/* 삭제 후 연결할 페이지 */
		mv.setViewName("redirect:/businessReport/list");
		
		return mv;
	}
	
	/* 영업 보고 등록시 영업활동 실적 모달창에 띄울 정보 가져오기 */
	@GetMapping(value="/performance", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<BusinessActivityListDTO> selectBusinessActivityList(Model model) {
		
		List<BusinessActivityListDTO> businessActivityList = businessReportService.selectBusinessActivityList();
		
		model.addAttribute("businessActivityList", businessActivityList);
		
		return businessActivityList;
	}
	
}















