package com.codefactory.mnm.businesstool.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.codefactory.mnm.businesstool.model.dto.ClientCompanyDTO;
import com.codefactory.mnm.businesstool.model.dto.ClientDTO;
import com.codefactory.mnm.businesstool.model.dto.ConferenceAttendeesClientDTO;
import com.codefactory.mnm.businesstool.model.dto.ConferenceAttendeesUsersDTO;
import com.codefactory.mnm.businesstool.model.dto.ProceedingsAttachmentDTO;
import com.codefactory.mnm.businesstool.model.dto.ProceedingsCollectionDTO;
import com.codefactory.mnm.businesstool.model.dto.ProceedingsDTO;
import com.codefactory.mnm.businesstool.model.dto.ProceedingsSearchDTO;
import com.codefactory.mnm.businesstool.model.dto.UserDTO;
import com.codefactory.mnm.businesstool.model.service.ProceedingsService;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyAttachmentDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyManagerDTO;
import com.codefactory.mnm.common.SelectCriteria;

@Controller
@RequestMapping("/proceedings")
public class ProceedingsController {

	private ProceedingsService proceedingsService;
	
	@Autowired
	public ProceedingsController(ProceedingsService proceedingsService) {
		this.proceedingsService = proceedingsService;
	}
	
	/* 회의록 리스트 조회 */
	@GetMapping("list")
	public ModelAndView selectProceedingsList(ModelAndView mv,
			@RequestParam(defaultValue = "") String selectSearch,
			@RequestParam(defaultValue = "") String searchValue,
			@RequestParam(defaultValue = "1") String currentPage) {
		
		/* 처음 회의록 리스트를 조회하거나 또는 검색조건을 설정하지 않았을 때를 대비 */
		String proceedingsName = null;
		String clientCompanyName = null;
		String clientName = null;
		
		/* 검색 조건에 따라 값을 설정 */
		if(selectSearch.equals("회의록")) { 				//검색조건 중 회의록을 선택하였을 경우
			proceedingsName = searchValue;
		} else if(selectSearch.equals("고객사")){			//검색조건 중 고객사를 선택하였을 경우 
			clientCompanyName = searchValue;
		} else if(selectSearch.equals("고객")) {			//검색조건 중 고객을 선택하였을 경우
			clientName = searchValue;
		}		
		
		/* 검색조건을 담음. 검색조건이 없을 경우 null로 담김. */
		ProceedingsSearchDTO proceedingsSearch = new ProceedingsSearchDTO();
		proceedingsSearch.setClientCompanyName(clientCompanyName);
		proceedingsSearch.setClientName(clientName);
		proceedingsSearch.setCurrentPage(currentPage);
		proceedingsSearch.setProceedingsName(proceedingsName);
		
		Map<String, Object> map = proceedingsService.selectProceedingsList(proceedingsSearch);
		
		List<ProceedingsDTO> proceedingsList = (List<ProceedingsDTO>) map.get("proceedingsList"); //회의록 리스트 조회
		SelectCriteria selectCriteria = (SelectCriteria) map.get("selectCriteria");				  //페이징처리
		int totalCount = (int) map.get("totalCount");											  //총 게시물 수 조회
		
		mv.addObject("proceedingsList", proceedingsList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("totalCount", totalCount);
		mv.addObject("proceedingsSearch", proceedingsSearch);
		
		mv.setViewName("proceedings/list");
		return mv;
	}
	
	/* 회의록 상세조회 */
	@GetMapping("/select")
	public ModelAndView selectProceedings(ModelAndView mv, String proceedingsNo) {
		
		Map<String, Object> map = proceedingsService.selectProceedings(proceedingsNo);
		
		ProceedingsDTO proceedings = (ProceedingsDTO) map.get("proceedings");											//회의록 조회
		List<ConferenceAttendeesUsersDTO> userList = (List<ConferenceAttendeesUsersDTO>) map.get("userList");			//회의 참가자(자사) 리스트 조회
		List<ConferenceAttendeesClientDTO> clientList = (List<ConferenceAttendeesClientDTO>) map.get("clientList");		//회의 참가자(고객) 리스트 조회
		List<ProceedingsAttachmentDTO> files = (List<ProceedingsAttachmentDTO>) map.get("proceedingsAttachmentList");	//회의록 첨부파일 리스트 조회
		
		/* 화면으로 전달 */
		mv.addObject("proceedings", proceedings);
		mv.addObject("userList", userList);
		mv.addObject("clientList", clientList);
		mv.addObject("files", files);
		mv.setViewName("proceedings/select");
		
		return mv;
	}
	
	/*  첨부파일 다운로드 */
	@GetMapping(value="fileDownload")
	@ResponseBody
	public void fileDownload(String fileNo, HttpServletResponse response) throws Exception {

		ProceedingsAttachmentDTO proceedingsAttachment = proceedingsService.selectFile(fileNo);

		String fileName = proceedingsAttachment.getFileName();					//저장파일명
		String fileOriginalName = proceedingsAttachment.getFileOriginalName();	//원본파일명
		String filePath = proceedingsAttachment.getFilePath();					//첨부파일 저장경로(최종경로)
		String saveFileName = filePath + fileName;

		File file = new File(saveFileName);
		long fileLength = file.length();

		/* 파일 정보들을 가지고 reponse의 Header에 세팅 */
		response.setHeader("Content-Disposition", "attachment; filename=\"" + URLEncoder.encode(fileOriginalName,"UTF-8") + "\";");
		response.setHeader("Content-Transfer-Encoding", "binary");
		response.setHeader("Content-Type", "application/octet-stream");
		response.setHeader("Content-Length", "" + fileLength);
		response.setHeader("Pragma", "no-cache;");
		response.setHeader("Expires", "-1;");

		/* saveFileName을 파라미터로 넣어 inputStream 객체를 만들고 
	        response에서 파일을 내보낼 OutputStream을 가져와서 */
		FileInputStream fis = new FileInputStream(saveFileName);
		OutputStream out = response.getOutputStream();

		/* 파일 읽을 만큼 크기의 buffer를 생성한 후 */
		int readCount = 0;
		byte[] buffer = new byte[1024];
		
		/* outputStream에 씌워준다 */
		while((readCount = fis.read(buffer)) != -1){
			out.write(buffer,0,readCount);
		}

		fis.close();
		out.close();

	}
	
	/* 고객사 리스트 조회 */
	@GetMapping(value="clientCompany", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<ClientCompanyDTO> selectClientCompanyList() {
		
		return proceedingsService.selectClientCompanyList();
	}
	
	/* 회의 참석자(고객) 리스트 조회 */
	@GetMapping(value="client", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<ClientDTO> selectClientList(String clientCompanyNo) {
	
		return proceedingsService.selectClientList(clientCompanyNo);
	}
	
	/* 회의 참석자(자사) 리스트 조회 */
	@GetMapping(value="user", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<UserDTO> selectUserList() {
		
		return proceedingsService.selectUserList();
	}
	
	/* 회의록 작성화면으로 이동 */
	@GetMapping("/insert")
	public void insertProceedingsPage() {}
	
	/* 회의록 등록 */
	@PostMapping("/insert")
	public ModelAndView insertProceedings(ModelAndView mv, 
			String[] userNo,
			String[] clientNo, 
			ProceedingsDTO proceedings, 
			List<MultipartFile> multiFiles, 
			HttpServletRequest request, 
			RedirectAttributes rttr) {
		
		/* 회의 참석자(자사)를 등록하였을 경우 */
		List<UserDTO> userList = null;
		
		if(userNo != null ) {
			
			userList = new ArrayList<>();
			
			/* 반복문을 통해서 미리 선언한 userList에 당음 */
			for(int i = 0; i < userNo.length; i++) {
				
				UserDTO user = new UserDTO();
				user.setUserNo(userNo[i]);	
				
				userList.add(user);
			}
		}
		
		/* 회의 참석자(고객)를 등록하였을 경우 */
		List<ClientDTO> clientList = null;
		
		if(clientNo != null) {
			
			clientList = new ArrayList<>();
			
			/* 반복문을 통해서 미리 선언한 clientList에 당음 */
			for(int i = 0; i < clientNo.length; i++) {
				
				ClientDTO client = new ClientDTO();
				client.setClientNo(clientNo[i]);	
				
				clientList.add(client);
			}
			
		}
		
		/* 회의록 등록과 동시에 첨부파일도 같이 등록한 경우 */
		if(multiFiles.size() > 0 && !multiFiles.get(0).getOriginalFilename().equals("")) {

			String root = request.getSession().getServletContext().getRealPath("/");	// 첨부파일 저장경로(절대경로)
			String filePath = root + "static/upload/";									// 첨부파일 저장경로(최종경로)

			/* 첨부파일 저장경로 (최종경로) 파일이 존재 하지 않을 경우 생성 */
			File mkdir = new File(filePath);
			if(!mkdir.exists()) {
				mkdir.mkdirs();
			}

			List<ProceedingsAttachmentDTO> files = new ArrayList<>();

			/* 다중 첨부파일은 반복문을 통해서, 미리 선언한 files에 담음  */
			for(int i = 0; i < multiFiles.size(); i++) {
				String fileOriginalName = multiFiles.get(i).getOriginalFilename();			// 원본파일명
				String ext = fileOriginalName.substring(fileOriginalName.lastIndexOf("."));	// 확장자명
				String fileName = UUID.randomUUID().toString().replace("-", "") + ext;		// 저장파일명

				/* 회의록파일첨부DTO인 file에 정보를 담은 후 리스트인 files에 담음 */
				ProceedingsAttachmentDTO file = new ProceedingsAttachmentDTO();			
				file.setFileName(fileName);
				file.setFileOriginalName(fileOriginalName);
				file.setFilePath(filePath);

				files.add(file);
			}
			

			/* 회의록 등록시 필요한 정보를 ProceedingsCollectionDTO에 담음 */
			ProceedingsCollectionDTO proceedingsCollection = new ProceedingsCollectionDTO();
			proceedingsCollection.setProceedings(proceedings);
			proceedingsCollection.setFiles(files);
			
			/* 회의참석자(고객) 있을 시 저장 */
			if(clientNo != null) {
				proceedingsCollection.setClientList(clientList);				
			}
			
			/* 회의참석자(자사) 있을 시 저장 */
			if(userNo != null) {				
				proceedingsCollection.setUserList(userList);
			}
			
			int result = proceedingsService.insertProceedings(proceedingsCollection);

			/* DB에 등록을 성공하였을 경우 */
			if(result > 0 ) {

				/* 파일 저장 */
				try {
					for(int i = 0; i < multiFiles.size(); i++) {
						ProceedingsAttachmentDTO file = files.get(i);			
						multiFiles.get(i).transferTo(new File(filePath + "\\" + file.getFileName()));
					}
					rttr.addFlashAttribute("message", "회의록 등록을 성공하였습니다.");

					/* 파일 저장을 실패하였을 경우, 저장했던 파일을 삭제
					 * 트랜젝션처리를 통해서 DB에 입력된 정보도 삭제됨 */
				} catch (Exception e) {
					e.printStackTrace();

					for(int i = 0; i < multiFiles.size(); i++) {
						ProceedingsAttachmentDTO file = files.get(i); 
						new File(filePath + "\\" + file.getFileName()).delete(); 
					}
					rttr.addFlashAttribute("message", "회의록 등록 후 파일 업로드에 실패하였습니다.");
				}

			/* DB에 회의록 등록을 실패하였을 경우 */
			} else {
				rttr.addFlashAttribute("message", "회의록 등록을 실패하였습니다.");	
			}

		/* 파일첨부를 하지 않고, 회의록 등록을 하는 경우 */
		}  else {
			
			/* 회의록 등록시 필요한 정보를 ProceedingsCollectionDTO에 담음 */
			ProceedingsCollectionDTO proceedingsCollection = new ProceedingsCollectionDTO();
			proceedingsCollection.setProceedings(proceedings);
			
			/* 회의참석자(고객) 있을 시 저장 */
			if(clientNo != null) {
				proceedingsCollection.setClientList(clientList);				
			}
			
			/* 회의참석자(자사) 있을 시 저장 */
			if(userNo != null) {				
				proceedingsCollection.setUserList(userList);
			}
			
			int result = proceedingsService.insertProceedings(proceedingsCollection);

			if(result > 0) {				
					rttr.addFlashAttribute("message", "회의록 등록을 성공하였습니다.");
			} else {
				rttr.addFlashAttribute("message", "회의록 등록을 실패하였습니다.");
			}
		}

		mv.setViewName("redirect:/proceedings/list");
		return mv;		
	}
	
	/* 회의록 수정페이지 조회 */
	@GetMapping("/update")
	public ModelAndView selectProceedingsByUpdate(ModelAndView mv, String proceedingsNo) {
		
		Map<String, Object> map = proceedingsService.selectProceedings(proceedingsNo);
		
		ProceedingsDTO proceedings = (ProceedingsDTO) map.get("proceedings");											//회의록 조회
		List<ConferenceAttendeesUsersDTO> userList = (List<ConferenceAttendeesUsersDTO>) map.get("userList");			//회의 참가자(자사) 리스트 조회
		List<ConferenceAttendeesClientDTO> clientList = (List<ConferenceAttendeesClientDTO>) map.get("clientList");		//회의 참가자(고객) 리스트 조회
		List<ProceedingsAttachmentDTO> files = (List<ProceedingsAttachmentDTO>) map.get("proceedingsAttachmentList");	//회의록 첨부파일 리스트 조회
		
		mv.addObject("proceedings", proceedings);
		mv.addObject("userList", userList);
		mv.addObject("clientList", clientList);
		mv.addObject("files", files);
		mv.setViewName("proceedings/update");
		
		return mv;
	}
	
	/* 첨부파일 삭제 */
	@PostMapping(value="file/delete", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int deleteFile(String fileNo, HttpServletRequest request) {

		String root = request.getSession().getServletContext().getRealPath("/");						//첨부파일 저장경로(절대경로)
		String filePath = root + "static/upload/";														//첨부파일 저장경로(최종경로)

		Map<String, Object> map = proceedingsService.deleteFile(fileNo);	

		ProceedingsAttachmentDTO file = (ProceedingsAttachmentDTO) map.get("proceedingsAttachment");	//첨부파일정보
		int result = (int) map.get("result");															//첨부파일 DB에서 삭제 결과

		/* 첨부파일을 DB에서 성공적으로 삭제되었을 경우 저장된파일도 삭제 */
		if(result > 0 ) {
			new File(filePath + "\\" + file.getFileName()).delete(); 		
		}
	
		return result;
	}
	
	/* 회의 참석자(자사) 등록 */
	@PostMapping(value="conferenceAttendeesUsers/insert", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int insertConferenceAttendeesUsers(String proceedingsNo, String userNo) {
	
		return proceedingsService.insertConferenceAttendeesUsers(proceedingsNo, userNo);
	}
	
	/* 회의 참석자(자사) 삭제 */
	@PostMapping(value="conferenceAttendeesUsers/delete", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int deleteConferenceAttendeesUsers(String proceedingsNo, String userNo) {
	
		return proceedingsService.deleteConferenceAttendeesUsers(proceedingsNo, userNo);
	}
	
	/* 회의 참석자(고객) 등록 */
	@PostMapping(value="conferenceAttendeesClient/insert", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int insertConferenceAttendeesClient(String proceedingsNo, String clientNo) {

		return proceedingsService.insertConferenceAttendeesClient(proceedingsNo, clientNo);
	}
	
	/* 회의 참석자(고객) 삭제 */
	@PostMapping(value="conferenceAttendeesClient/delete", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int deleteConferenceAttendeesClient(String proceedingsNo, String clientNo) {
	
		return proceedingsService.deleteConferenceAttendeesClient(proceedingsNo, clientNo);
	}
	
	/* 회의록 수정 */
	@PostMapping("/update")
	public ModelAndView updateProceedings(ModelAndView mv, 
			ProceedingsDTO proceedings, 
			List<MultipartFile> multiFiles, 
			HttpServletRequest request, 
			RedirectAttributes rttr) { 
		
		/* 회의록 수정과 동시에 파일첨부도 같이 등록한 경우 */
		if(multiFiles.size() > 0 && !multiFiles.get(0).getOriginalFilename().equals("")) {

			String root = request.getSession().getServletContext().getRealPath("/");	// 첨부파일 저장경로(절대경로)
			String filePath = root + "static/upload/";									// 첨부파일 저장경로(최종경로)

			/* 첨부파일 저장경로 (최종경로) 파일이 존재 하지 않을 경우 생성 */
			File mkdir = new File(filePath);
			if(!mkdir.exists()) {
				mkdir.mkdirs();
			}

			List<ProceedingsAttachmentDTO> files = new ArrayList<>();

			/* 다중 첨부파일은 반복문을 통해서, 미리 선언한 files에 담음  */
			for(int i = 0; i < multiFiles.size(); i++) {
				String fileOriginalName = multiFiles.get(i).getOriginalFilename();			// 원본파일명
				String ext = fileOriginalName.substring(fileOriginalName.lastIndexOf("."));	// 확장자명
				String fileName = UUID.randomUUID().toString().replace("-", "") + ext;		// 저장파일명

				/* 회의록파일첨부DTO인 file에 정보를 담은 후 리스트인 files에 담음 */
				ProceedingsAttachmentDTO file = new ProceedingsAttachmentDTO();			
				file.setFileName(fileName);
				file.setFileOriginalName(fileOriginalName);
				file.setFilePath(filePath);

				files.add(file);
			}
			

			/* 회의록 수정시 필요한 정보를 ProceedingsCollectionDTO에 담음 */
			ProceedingsCollectionDTO proceedingsCollection = new ProceedingsCollectionDTO();
			proceedingsCollection.setProceedings(proceedings);
			proceedingsCollection.setFiles(files);

			int result = proceedingsService.updateProceedings(proceedingsCollection);

			/* DB에 등록을 성공하였을 경우 */
			if(result > 0 ) {

				/* 파일 저장 */
				try {
					for(int i = 0; i < multiFiles.size(); i++) {
						ProceedingsAttachmentDTO file = files.get(i);			
						multiFiles.get(i).transferTo(new File(filePath + "\\" + file.getFileName()));
					}

					rttr.addFlashAttribute("message", "회의록 수정을 성공하였습니다.");

					/* 파일 저장을 실패하였을 경우, 저장했던 파일을 삭제
					 * 트랜젝션처리를 통해서 DB에 입력된 정보도 삭제됨 */
				} catch (Exception e) {
					e.printStackTrace();

					for(int i = 0; i < multiFiles.size(); i++) {
						ProceedingsAttachmentDTO file = files.get(i); 
						new File(filePath + "\\" + file.getFileName()).delete(); 
					}
					rttr.addFlashAttribute("message", "회의록 수정 후 파일 업로드에 실패하였습니다.");
				}

			/* DB에 회의록 등록을 실패하였을 경우 */
			} else {
				rttr.addFlashAttribute("message", "회의록 수정을 실패하였습니다.");	
			}

		/* 파일첨부를 하지 않고, 회의록 수정을 하는 경우 */
		}  else {
			
			/* 회의록 수정시 필요한 정보를 ProceedingsCollectionDTO에 담음 */
			ProceedingsCollectionDTO proceedingsCollection = new ProceedingsCollectionDTO();
			proceedingsCollection.setProceedings(proceedings);
			
			int result = proceedingsService.updateProceedings(proceedingsCollection);

			if(result > 0) {				
					rttr.addFlashAttribute("message", "회의록 수정을 성공하였습니다.");
			} else {
				rttr.addFlashAttribute("message", "회의록 수정을 실패하였습니다.");
			}
		}

		mv.setViewName("redirect:/proceedings/list");
		return mv;		
	}
	
	/* 회의록 삭제 */
	@PostMapping(value="delete", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int deleteProceedings(String proceedingsNo) {

		return proceedingsService.deleteProceedings(proceedingsNo);
	}
}
