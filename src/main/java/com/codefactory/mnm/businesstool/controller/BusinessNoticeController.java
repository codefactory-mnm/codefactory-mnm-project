package com.codefactory.mnm.businesstool.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.codefactory.mnm.businesstool.model.dto.BusinessNoticeAttachmentsDTO;
import com.codefactory.mnm.businesstool.model.dto.BusinessNoticeCollectionDTO;
import com.codefactory.mnm.businesstool.model.dto.BusinessNoticeListDTO;
import com.codefactory.mnm.businesstool.model.dto.BusinessNoticeSearchDTO;
import com.codefactory.mnm.businesstool.model.service.BusinessNoticeService;
import com.codefactory.mnm.common.SelectCriteria;

@Controller
@RequestMapping("/businessNotice")
public class BusinessNoticeController {

	private BusinessNoticeService businessNoticeService;
	
	@Autowired
	public BusinessNoticeController(BusinessNoticeService businessNoticeService) {
		this.businessNoticeService = businessNoticeService;
	}
	
	/* 목록 조회 (검색조건을 가져온다) */ 
	@GetMapping("/list")
	public ModelAndView selectBusinessNoticeList(ModelAndView mv,
			@RequestParam(defaultValue = "") @DateTimeFormat(pattern = "yyyyMMdd") String startDate,
			@RequestParam(defaultValue = "") @DateTimeFormat(pattern = "yyyyMMdd") String endDate,
			@RequestParam(defaultValue = "1") String currentPage) {
		
		/* 리스트를 조회하거나 또는 검색조건을 설정하지 않았을 때를 대비 */
		if(startDate.equals("")) { // 검색조건 중 startDate을 입력하지 않았을 경우
			startDate = null;
		}
		
		if(endDate.equals("")) { // 검색조건 중 endDate를 입력하지 않았을 경우
			endDate = null;
		}
		
		/* 검색조건을 담는다. 검색조건이 없을 경우 null로 담김. */
		BusinessNoticeSearchDTO businessNoticeSearchDTO = new BusinessNoticeSearchDTO();
		businessNoticeSearchDTO.setStartDate(startDate);
		businessNoticeSearchDTO.setEndDate(endDate);
		businessNoticeSearchDTO.setCurrentPage(currentPage);
		
		/* 검색조건을 service로 보내주고, 값을 map으로 전달받음 */
		Map<String, Object> map = businessNoticeService.selectBusinessNoticeList(businessNoticeSearchDTO);
		
		/* db에서 가져온 값을 꺼낸다. */
		List<BusinessNoticeListDTO> businessNoticeList = (List<BusinessNoticeListDTO>) map.get("businessNoticeList");
		int businessNoticeListCount = (int) map.get("businessNoticeListCount");
		SelectCriteria selectCriteria = (SelectCriteria) map.get("selectCriteria");
		int totalCount = (int) map.get("totalCount");
		
		
		/* db에서 조회해온 결과를 담는다 */
		mv.addObject("businessNoticeList", businessNoticeList);
		mv.addObject("businessNoticeListCount", businessNoticeListCount);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("totalCount", totalCount);
		mv.addObject("businessNoticeSearchDTO", businessNoticeSearchDTO);
		mv.setViewName("businessNotice/list");
		
		return mv;
	}
	
	/* 상세조회 */
	@GetMapping("/detail")
	public ModelAndView selectBusinessNotice(ModelAndView mv, @RequestParam String businessNoticeNo){
		
		/* Map을 생성하여 서비스에서 값을 리턴 받는다*/
		Map<String, Object> map = businessNoticeService.selectBusinessNotice(businessNoticeNo);
		
		/* map에 담은 값을 꺼낸다 */
		BusinessNoticeListDTO oneBusinessNotice = (BusinessNoticeListDTO) map.get("oneBusinessNotice");
		List<BusinessNoticeAttachmentsDTO> fileList = (List<BusinessNoticeAttachmentsDTO>) map.get("fileList");
		
		int selectCount = businessNoticeService.selectCount(oneBusinessNotice);
		oneBusinessNotice.setCount(selectCount);
		
		/* map에서 꺼낸 값들을 담는다 */
		mv.addObject("oneBusinessNotice", oneBusinessNotice);
		mv.addObject("fileList", fileList);
		mv.setViewName("businessNotice/detail");
		
		return mv;
	}

	
	@GetMapping("/insert")
	public void insertBusinessNoticePage() {}
	
	/* 공지 등록 */
	@PostMapping("/insert")
	public ModelAndView insertBusinessNotice(ModelAndView mv, BusinessNoticeListDTO businessNotice,
			List<MultipartFile> multiFiles, HttpServletRequest request, RedirectAttributes rttr, Principal pcp) {
			
			String userNo = businessNoticeService.selectUserNo(pcp.getName());
			businessNotice.setUserNo(userNo);
		
		/* 영업 공지 정보 입력과 동시에 파일첨부도 같이 등록한 경우 */
		if(multiFiles.size() > 0 && !multiFiles.get(0).getOriginalFilename().equals("")) {
			
			String root = request.getSession().getServletContext().getRealPath("/");		// 첨부파일 저장경로(절대경로) 
			String filePath = root + "static/businessnotice/";								// 첨부파일 저장경로(최종경로)
			
			/* 첨부파일 저장경로 (최종경로) 파일이 존재 하지 않을 경우 생성 */
			File mkdir = new File(filePath);
			if(!mkdir.exists()) {
				mkdir.mkdirs();
			}
			
			/* 다중 첨부파일이기 때문에, 여러 개의 파일을 담기 위해 List<BusinessNoticeAttachmentsDTO> 타입으로 생성 */
			List<BusinessNoticeAttachmentsDTO> files = new ArrayList<>();

			/* 다중 첨부파일은 반복문을 통해서, 미리 선언한 files에 담음  */
			for(int i = 0; i < multiFiles.size(); i++) {
				String fileOriginalName = multiFiles.get(i).getOriginalFilename();				// 원본파일명
				String ext = fileOriginalName.substring(fileOriginalName.lastIndexOf("."));		// 확장자명
				String fileName = UUID.randomUUID().toString().replace("-", "") + ext;			// 저장파일명
				
				/* 제안 파일첨부DTO인 file에 정보를 담은 후 리스트인 files에 담음 */
				BusinessNoticeAttachmentsDTO file = new BusinessNoticeAttachmentsDTO();
				file.setFileName(fileName);
				file.setFileOriginalName(fileOriginalName);
				file.setFilePath(filePath);

				files.add(file);
			}
			
			/* 제안 등록시 필요한 정보를 businessNoticeCollection에 담는다 */
			BusinessNoticeCollectionDTO businessNoticeCollection = new BusinessNoticeCollectionDTO();
			businessNoticeCollection.setBusinessNotice(businessNotice);
			businessNoticeCollection.setFiles(files);
			
			/* 서비스로 businessNoticeCollection를 보낸다 */
			int result = businessNoticeService.insertBusinessNotice(businessNoticeCollection);
			
			/* DB에 등록을 성공하였을 경우 */
			if(result > 0 ) {
	
			/* 파일 저장 */
			try {
				for(int i = 0; i < multiFiles.size(); i++) {

					BusinessNoticeAttachmentsDTO file = files.get(i);			
					multiFiles.get(i).transferTo(new File(filePath + "\\" + file.getFileName()));

				}

				rttr.addFlashAttribute("successMessage", "영업 공지 등록에 성공하셨습니다.");
				
				/* 파일 저장을 실패하였을 경우, 저장했던 파일을 삭제
				 * 트랜젝션처리를 통해서 DB에 입력된 정보도 삭제됨 */
			} catch (Exception e) {
				e.printStackTrace();

				/* 실패 시 파일 삭제 */
				for(int i = 0; i < multiFiles.size(); i++) {
					
					BusinessNoticeAttachmentsDTO file = files.get(i); 
					new File(filePath + "\\" + file.getFileName()).delete(); 
				}

				rttr.addFlashAttribute("successMessage", "영업 공지 등록후 업로드에 실패하셨습니다.");
			}
			
			/* DB에 제안 등록을 실패하였을 경우 */
			} else {
			
			rttr.addAttribute("message", "영업 공지 등록에 실패하셨습니다.");
				
		} 
		
		/* 파일첨부를 하지 않고, 제안 등록을 하는 경우 */
		}else {
			
			/* 공지 등록시 필요한 정보를 businessNoticeCollection에 담는다 */
			BusinessNoticeCollectionDTO businessNoticeCollection = new BusinessNoticeCollectionDTO();
			businessNoticeCollection.setBusinessNotice(businessNotice);
			
			/* 서비스로 businessNoticeCollection를 전달한다 */
			int result = businessNoticeService.insertBusinessNotice(businessNoticeCollection);

			if(result > 0) {
				rttr.addFlashAttribute("successMessage", "영업 공지 등록에 성공하셨습니다.");
			} else {
				rttr.addFlashAttribute("successMessage", "영업 공지 등록에 실패하셨습니다.");
			}
			
		}
		
		/* 공지 등록 후 연결할 페이지 */
		mv.setViewName("redirect:/businessNotice/list");
		return mv;
	
	}
	
	/* 공지 삭제 */
	@GetMapping("/delete")
	public ModelAndView deleteBusinessNotice(ModelAndView mv, @RequestParam String businessNoticeNo,
			RedirectAttributes rttr) {
		 
		/* 공지 번호를 받아서 해당 게시글을 삭제 한다 */
		int result = businessNoticeService.deleteBusinessNotice(businessNoticeNo);
		
		if(result > 0) {
			rttr.addFlashAttribute("message", "공지 삭제를 성공하였습니다.");
		} else {
			rttr.addFlashAttribute("message", "공지 삭제를 실패하였습니다.");
		}
		
		/* 삭제 후 연결할 페이지 */
		mv.setViewName("redirect:/businessNotice/list");
		
		return mv;
	}
	
	/* 공지 수정페이지로 이동  */
	@GetMapping("/update")
	public ModelAndView selectBusinessNoticeUpdate(ModelAndView mv, @RequestParam String businessNoticeNo) {
		
		/* Map을 생성하여 서비스에서 값을 리턴 받는다 */
		Map<String, Object> map = businessNoticeService.selectBusinessNotice(businessNoticeNo);
		
		/* map에 담은 값을 꺼낸다 */
		BusinessNoticeListDTO oneBusinessNotice = (BusinessNoticeListDTO) map.get("oneBusinessNotice");
		
		List<BusinessNoticeAttachmentsDTO> fileList = (List<BusinessNoticeAttachmentsDTO>) map.get("fileList");
	   
		/* map에서 꺼낸 값들을 담는다 */
		mv.addObject("oneBusinessNotice", oneBusinessNotice);
		mv.addObject("fileList", fileList);
		mv.setViewName("businessNotice/update");
		
		return mv;
	}
	
	/* 공지 수정 */
	@PostMapping("/update")
	public ModelAndView updateBusinessNotice(ModelAndView mv, BusinessNoticeListDTO businessNotice,
			List<MultipartFile> multiFiles, HttpServletRequest request, RedirectAttributes rttr, Principal pcp) {

			String userNo = businessNoticeService.selectUserNo(pcp.getName());
			businessNotice.setUserNo(userNo);
		
			/* 공지 정보 입력과 동시에 파일첨부도 같이 등록한 경우 */
		if(multiFiles.size() > 0 && !multiFiles.get(0).getOriginalFilename().equals("")) {
			String root = request.getSession().getServletContext().getRealPath("/");		
			String filePath = root + "static/businessnotice/";
			
			/* 첨부파일 저장경로 (최종경로) 파일이 존재 하지 않을 경우 생성 */
			File mkdir = new File(filePath);
			if(!mkdir.exists()) {
				mkdir.mkdirs();
			}
			
			/* 다중 첨부파일이기 때문에, 여러 개의 파일을 담기 위해 List<BusinessNoticeAttachmentsDTO> 타입으로 생성 */
			List<BusinessNoticeAttachmentsDTO> files = new ArrayList<>();
			
			/* 다중 첨부파일은 반복문을 통해서, 미리 선언한 files에 담음  */
			for(int i = 0; i < multiFiles.size(); i++) {
				String fileOriginalName = multiFiles.get(i).getOriginalFilename();
				String ext = fileOriginalName.substring(fileOriginalName.lastIndexOf("."));
				String fileName = UUID.randomUUID().toString().replace("-", "") + ext;
				
				/* 공지 파일첨부DTO인 file에 정보를 담은 후 리스트인 files에 담음 */
				BusinessNoticeAttachmentsDTO file = new BusinessNoticeAttachmentsDTO();
				file.setFileName(fileName);
				file.setFileOriginalName(fileOriginalName);
				file.setFilePath(filePath);

				files.add(file);
			}

			
			/* 공지 등록시 필요한 정보를 BusinessNoticeCollectionDTO에 담는다 */
			BusinessNoticeCollectionDTO businessNoticeCollection = new BusinessNoticeCollectionDTO();
			businessNoticeCollection.setBusinessNotice(businessNotice);
			businessNoticeCollection.setFiles(files);
			
			/* 서비스로 businessNoticeCollection를 보낸다 */
			int result = businessNoticeService.updateBusinessNotice(businessNoticeCollection);
			
			/* DB에 등록을 성공하였을 경우 */
			if(result > 0 ) {
				
			/* 파일 저장 */
			try {
				for(int i = 0; i < multiFiles.size(); i++) {

					BusinessNoticeAttachmentsDTO file = files.get(i);			
					multiFiles.get(i).transferTo(new File(filePath + "\\" + file.getFileName()));

				}

				rttr.addFlashAttribute("successMessage", "영업 공지 등록에 성공하셨습니다.");
				
				/* 파일 저장을 실패하였을 경우, 저장했던 파일을 삭제
				 * 트랜젝션처리를 통해서 DB에 입력된 정보도 삭제됨 */
			} catch (Exception e) {
				e.printStackTrace();

				/* 실패 시 파일 삭제 */
				for(int i = 0; i < multiFiles.size(); i++) {
					
					BusinessNoticeAttachmentsDTO file = files.get(i); 
					new File(filePath + "\\" + file.getFileName()).delete(); 
				}

				rttr.addFlashAttribute("successMessage", "영업 공지 등록후 업로드에 실패하셨습니다.");
			}

		/* DB에 제안 등록을 실패하였을 경우 */	
		} else {
			
			rttr.addAttribute("message", "영업 공지 등록에 실패하셨습니다.");
				
		} 
			
		/* 파일첨부를 하지 않고, 영업 공지 등록을 하는 경우 */
		} else {
			/* 영업 공지 등록시 필요한 정보를 businessNoticeCollection에 담는다 */
			BusinessNoticeCollectionDTO businessNoticeCollection = new BusinessNoticeCollectionDTO();
			businessNoticeCollection.setBusinessNotice(businessNotice);
			
			/* 서비스로 businessNoticeCollection를 전달한다 */
			int result = businessNoticeService.updateBusinessNotice(businessNoticeCollection);

			if(result > 0) {
				rttr.addFlashAttribute("successMessage", "영업 공지 등록에 성공하셨습니다.");
			} else {
				rttr.addFlashAttribute("successMessage", "영업 공지 등록에 실패하셨습니다.");
			}
			
		}
		
		/* 제안 수정 후 연결할 페이지 */
		mv.setViewName("redirect:/businessNotice/list");
		return mv;
	
	}
	
	/* 첨부파일 다운로드 */
	@GetMapping("/download")
	@ResponseBody
	public void download(HttpServletResponse response, String businessFileNo) throws IOException {
		
		/* 전달받은 fileNo로 파일 정보 조회 */
		BusinessNoticeAttachmentsDTO BusinessNoticeAttachmentsDTO = businessNoticeService.selectFile(businessFileNo);
		String fileOriginalName = BusinessNoticeAttachmentsDTO.getFileOriginalName();
		String filePath = BusinessNoticeAttachmentsDTO.getFilePath();
		String fileName = BusinessNoticeAttachmentsDTO.getFileName();
		
		String saveFileName = filePath + fileName;
		
		File file = new File(saveFileName);
		long fileLength = file.length();
		
		/* 파일 정보들을 가지고 reponse의 Header에 세팅 */
		response.setHeader("Content-Disposition", "attachment; filename=\"" + URLEncoder.encode(fileOriginalName,"UTF-8") + "\";");
        response.setHeader("Content-Transfer-Encoding", "binary");
        response.setHeader("Content-Type", "application/octet-stream");
        response.setHeader("Content-Length", "" + fileLength);
        response.setHeader("Pragma", "no-cache;");
        response.setHeader("Expires", "-1;");

		/* saveFileName을 파라미터로 넣어 inputStream 객체를 만들고 
        response에서 파일을 내보낼 OutputStream을 가져와서 */
        FileInputStream fis = new FileInputStream(saveFileName);
        OutputStream out = response.getOutputStream();
        
    	/* 파일 읽을 만큼 크기의 buffer를 생성한 후 */
        int readCount = 0;
        byte[] buffer = new byte[1024];
        
        /* outputStream에 씌워준다 */
        while((readCount = fis.read(buffer)) != -1){
                out.write(buffer,0,readCount);
        }
        /* 스트림 close */
        fis.close();
        out.close();
        
	}
	
	/* ajax를 통해 파일 삭제 */
	@PostMapping(value="/file/delete", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public int deleteFile(String businessFileNo, HttpServletRequest request) {

		String root = request.getSession().getServletContext().getRealPath("/");			//첨부파일 저장경로(절대경로)
		String filePath = root + "static/businessnotice/";									//첨부파일 저장경로(최종경로)

		/* 파일번호를 service에 전달하고 map으로 값을 전달받음 */
		Map<String, Object> map = businessNoticeService.deleteFile(businessFileNo);	

		BusinessNoticeAttachmentsDTO File = (BusinessNoticeAttachmentsDTO) map.get("File");	//첨부파일정보
		int result = (int) map.get("result");												//첨부파일 DB에서 삭제 결과

		/* 첨부파일을 DB에서 성공적으로 삭제되었을 경우 저장된파일도 삭제 */
		if(result > 0 ) {

			new File(filePath + "\\" + File.getFileNo()).delete(); 		

		}

		/* 화면으로 전달 */
		return result;

	}
	
}












