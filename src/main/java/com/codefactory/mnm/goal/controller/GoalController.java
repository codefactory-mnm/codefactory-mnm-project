package com.codefactory.mnm.goal.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.codefactory.mnm.goal.model.dto.DepartmentDTO;
import com.codefactory.mnm.goal.model.dto.GoalInsertDTO;
import com.codefactory.mnm.goal.model.dto.GoalReturnDTO;
import com.codefactory.mnm.goal.model.dto.GoalUpdateDTO;
import com.codefactory.mnm.goal.model.dto.UserDTO;
import com.codefactory.mnm.goal.model.service.GoalService;
import com.codefactory.mnm.member.model.dto.UserImpl;

import ch.qos.logback.core.recovery.ResilientSyslogOutputStream;

@Controller
@RequestMapping("/goal")
public class GoalController {
	
	private final GoalService goalService;
	private MessageSource messageSource; 	//성공, 실패시 사용할 메세지 소스. 
	
	@Autowired
	public GoalController(GoalService goalService, MessageSource messageSource) {
		this.goalService = goalService;
		this.messageSource = messageSource;
	}
	
	/* 검색조건없이 제일 첫번째로 화면이동 할 때 */
	@GetMapping("/crew/select")
	public ModelAndView selectCrewGoal(ModelAndView mv, 
			@AuthenticationPrincipal UserImpl userImpl) {
		
		/* 오늘날짜의 연도 추출 */
		LocalDate currentDate = LocalDate.now();
		String todayDate = currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String[] todaySplit = todayDate.split("-");
		
		int todayYear = Integer.parseInt(todaySplit[0]);
		
		/* 3개년치 해를 리스트에 담기 */
		List<Integer> yearList = new ArrayList<>();
		for(int i = 0; i < 3; i++) {
			yearList.add(todayYear - i);
		}
		
		/* 현재 로그인한 유저의 userNo 받기 */
		String responsibility = userImpl.getUserNo(); 
		
		/* 검색조건을 입력하지 않았으므로 기본값으로 설정 */
		int yearInt = todayYear;			//오늘의 년도
		String measure = "상품매출";			//기본 검색조건은 매출액수
		
		/* 검색조건을 Map에 담기 */
		Map<String, Object> searchCriteria = new HashMap<>();
		searchCriteria.put("yearInt", yearInt);
		searchCriteria.put("responsibility", responsibility);
		searchCriteria.put("measure", measure);		
		
		/* 목표 List로 불러오기 */
		Map<String, Object> map = goalService.selectCrewGoal(searchCriteria);
		
		List<DepartmentDTO> deptList = (List<DepartmentDTO>)map.get("deptList"); 			//검색용으로 쓸 부서리스트
		List<UserDTO> userList = (List<UserDTO>)map.get("userList");						//검색용으로 쓸 직원리스트
		List<GoalReturnDTO> goalList = (List<GoalReturnDTO>)map.get("productReturnList");	//목표 리스트
		
		/* 상품별 합계 구하기 */
		for(int i = 0; i < goalList.size(); i++) {
			
			int productTotal = 0;
			if(goalList.get(i).getGoalScales() != null) {
				
				/* map형태로 들어있는 getGoalScales을 Iterator 목록으로 만들고, 
				 * while 반복문으로 반복하여 value값을 더해서 총합을 구한다. */
				Iterator<String> goalScaleIter = goalList.get(i).getGoalScales().keySet().iterator();
				while(goalScaleIter.hasNext()) {
					String key = goalScaleIter.next(); 
					String value = (String) goalList.get(i).getGoalScales().get(key);

					productTotal += Integer.parseInt(value);
				}
				goalList.get(i).setTotalCount(productTotal);
				
			} else {
				goalList.get(i).setTotalCount(productTotal);
			}
			
		}
				
		/* 넘길 값 add */		
		mv.addObject("yearList", yearList);
		mv.addObject("deptList", deptList);
		mv.addObject("userList", userList);
		mv.addObject("goalList", goalList);
		mv.setViewName("goal/crew/select");
		
		return mv;		
	}
	
	/* 검색조건을 설정하여 검색한 경우 */
	@PostMapping(value="/crew/selectAjax", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<GoalReturnDTO> selectCrewGoalList(
			@RequestParam String year, 
			@RequestParam String responsibility, 
			@RequestParam String measure, 
			@AuthenticationPrincipal UserImpl userImpl) {
		
		/* 검색조건으로 넘어온 연도가 없는 경우 */
		if(year.equals(" ")) {
			
			/* 오늘날짜의 연도 추출 */
			LocalDate currentDate = LocalDate.now();
			String todayDate = currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
			String[] todaySplit = todayDate.split("-");
			
			year = todaySplit[0];
		}
		
		/* 검색조건으로 넘어온 담당자 번호가 없는 경우 */
		if(responsibility.equals(" ")) {								
			
			responsibility = userImpl.getUserNo(); 							//현재 로그인한 유저의 userNo 받기
		}
		
		/* 검색조건 담기 */
		Map<String, Object> searchCriteria = new HashMap<>();
		searchCriteria.put("yearInt", Integer.parseInt(year));
		searchCriteria.put("responsibility", responsibility);
		searchCriteria.put("measure", measure);
		
		Map<String, Object> map = goalService.selectCrewGoal(searchCriteria);
		
		List<GoalReturnDTO> goalList = (List<GoalReturnDTO>)map.get("productReturnList");
		
		/* 상품별 합계 구하기 */
		for(int i = 0; i < goalList.size(); i++) {
			int productTotal = 0;
			if(goalList.get(i).getGoalScales() != null) {
				
				/* map형태로 들어있는 getGoalScales을 Iterator 목록으로 만들고, 
				 * while 반복문으로 반복하여 value값을 더해서 총합을 구한다. */
				Iterator<String> goalScaleIter = goalList.get(i).getGoalScales().keySet().iterator();
				while(goalScaleIter.hasNext()) {
					String key = goalScaleIter.next(); 
					String value = (String) goalList.get(i).getGoalScales().get(key);

					productTotal += Integer.parseInt(value);
				}
				goalList.get(i).setTotalCount(productTotal);
				
			} else {
				goalList.get(i).setTotalCount(productTotal);
			}
			
		}
		
		return goalList;
	}
	
	/* 목표 설정 */
	@PostMapping("/crew/insert")
	public ModelAndView insertCrewGoal(ModelAndView mv,
			@RequestParam Map<String, String> goalMap, 
			RedirectAttributes rttr,
			@AuthenticationPrincipal UserImpl customUser,
			Locale locale) {
		
		/* 월별 목표를 받아서 List에 add */
		List<String> goalMonthlyScales = new ArrayList<>();
		goalMonthlyScales.add(goalMap.get("jan"));
		goalMonthlyScales.add(goalMap.get("feb"));
		goalMonthlyScales.add(goalMap.get("mar"));
		goalMonthlyScales.add(goalMap.get("apr"));
		goalMonthlyScales.add(goalMap.get("may"));
		goalMonthlyScales.add(goalMap.get("jun"));
		goalMonthlyScales.add(goalMap.get("jul"));
		goalMonthlyScales.add(goalMap.get("aug"));
		goalMonthlyScales.add(goalMap.get("sep"));
		goalMonthlyScales.add(goalMap.get("oct"));
		goalMonthlyScales.add(goalMap.get("nov"));
		goalMonthlyScales.add(goalMap.get("dec"));
		
		List<Integer> month = new ArrayList<>();
		for(int i = 1; i < 13; i++) {
			month.add(i);
		}
		
		/* insert에 필요한 조건들을 DTO에 set */
		GoalInsertDTO goalInsertDTO = new GoalInsertDTO();
		goalInsertDTO.setUpdateProductName(goalMap.get("insertProductName"));
		goalInsertDTO.setUpdateProductNo(goalMap.get("insertProductNo"));
		goalInsertDTO.setYear(Integer.parseInt(goalMap.get("updateYear")));
		goalInsertDTO.setMonth(month);
		goalInsertDTO.setGoalMonthlyScales(goalMonthlyScales);
		goalInsertDTO.setDivision(goalMap.get("measure"));
		
		/* 현재 로그인한 유저의 userNo 받기 */
		if(goalMap.get("updateResponsibility").equals(" ")) {
			
			String responsibility = customUser.getUserNo(); 
			goalInsertDTO.setUserNo(responsibility);
			
		} else {
			goalInsertDTO.setUserNo(goalMap.get("updateResponsibility"));
		}
		
		/* 목표 삽입하고 결과 return 받기 */
		int result = goalService.insertCrewGoal(goalInsertDTO);
		
		if(result > 0) {
			rttr.addFlashAttribute("successMessage", "목표 등록 성공!");
		} else {
			rttr.addFlashAttribute("successMessage", "목표 등록 실패...");			
		}
		
		mv.setViewName("redirect:goal/crew/select");						//원래 페이지로 리다이렉트
		
		return mv;
	}
	
	/* 목표 수정 */
	@PostMapping("/crew/update")
	public ModelAndView updateCrewGoal(ModelAndView mv,
			@RequestParam Map<String, String> goalMap, 
			RedirectAttributes rttr, Locale locale) {
		
		/* 월별 목표(수정할 no와 목표값)를 받아서 List에 add */
		List<GoalUpdateDTO> updateGoalList = new ArrayList<>();
		for(int i = 1; i < 13; i++) {
			String getGoalNo = goalMap.get("GoalNoUpdate" + i);
			String getGoalScale = goalMap.get("scaleUpdate" + i);
			
			GoalUpdateDTO newGoal = new GoalUpdateDTO();
			newGoal.setGoalNo(getGoalNo);
			newGoal.setGoalScale(Integer.parseInt(getGoalScale));
			
			updateGoalList.add(newGoal);
		}
		
		/* 목표 수정하고 결과 return 받기 */
		int result = goalService.updateCrewGoal(updateGoalList);
		
		if(result > 0) {
			rttr.addFlashAttribute("successMessage", "목표 수정 성공!");
		} else {
			rttr.addFlashAttribute("successMessage", "목표 수정 실패...");			
		}
			
		mv.setViewName("redirect:goal/crew/select");						//원래 페이지로 리다이렉트
		
		return mv;
	}
	
	/* 검색조건없이 제일 첫번째로 화면이동 할 때 */
	@GetMapping("/team/select")
	public ModelAndView selectTeamGoal(ModelAndView mv, 
			@AuthenticationPrincipal UserImpl userImpl) {
		
		/* 오늘날짜의 연도 추출 */
		LocalDate currentDate = LocalDate.now();
		String todayDate = currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String[] todaySplit = todayDate.split("-");
		
		int todayYear = Integer.parseInt(todaySplit[0]);
		System.out.println("오늘의 해 :" + todayYear);
		
		/* 3개년치 해를 리스트에 담기 */
		List<Integer> yearList = new ArrayList<>();
		for(int i = 0; i < 3; i++) {
			yearList.add(todayYear - i);
		}
		
		/* 현재 로그인한 유저의 userNo 받기 */
		String deptNo = userImpl.getDeptNo();
		
		/* 검색조건을 입력하지 않았으므로 기본값으로 설정 */
		int yearInt = todayYear;			//오늘의 년도
		String measure = "상품매출";			//기본 검색조건은 매출액수
		
		/* 검색조건을 Map에 담기 */
		Map<String, Object> searchCriteria = new HashMap<>();
		searchCriteria.put("yearInt", yearInt);
		searchCriteria.put("deptNo", deptNo);
		searchCriteria.put("measure", measure);		
		
		/* 목표 List로 불러오기 */
		Map<String, Object> map = goalService.selectTeamGoal(searchCriteria);

		List<DepartmentDTO> deptList = (List<DepartmentDTO>)map.get("deptList"); 			//검색용으로 쓸 부서리스트
		List<GoalReturnDTO> goalList = (List<GoalReturnDTO>)map.get("productReturnList");	//검색용으로 쓸 직원리스트
		
		/* 상품별 합계 구하기 */
		for(int i = 0; i < goalList.size(); i++) {
			
			int productTotal = 0;
			if(goalList.get(i).getGoalScales() != null) {
				
				/* map형태로 들어있는 getGoalScales을 Iterator 목록으로 만들고, 
				 * while 반복문으로 반복하여 value값을 더해서 총합을 구한다. */
				Iterator<String> goalScaleIter = goalList.get(i).getGoalScales().keySet().iterator();
				while(goalScaleIter.hasNext()) {
					String key = goalScaleIter.next(); 
					String value = (String) goalList.get(i).getGoalScales().get(key);

					productTotal += Integer.parseInt(value);
				}
				
				goalList.get(i).setTotalCount(productTotal);
				System.out.println("상품별 합계 구하기 : " + productTotal);
				
			} else {
				goalList.get(i).setTotalCount(productTotal);
			}
			
		}
				
		/* 넘길 값 add */		
		mv.addObject("yearList", yearList);
		mv.addObject("deptList", deptList);
		mv.addObject("goalList", goalList);
		mv.setViewName("goal/team/select");
		
		return mv;		
	}
	
	/* 검색조건을 설정하여 검색한 경우 */
	@PostMapping(value="/team/selectAjax", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<GoalReturnDTO> selectTeamGoalList(
			@RequestParam String year, 
			@RequestParam String deptNo, 
			@RequestParam String measure, 
			@AuthenticationPrincipal UserImpl userImpl) {
		
		if(year.equals(" ")) {
			
			/* 오늘날짜의 연도 추출 */
			LocalDate currentDate = LocalDate.now();
			String todayDate = currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
			String[] todaySplit = todayDate.split("-");
			
			year = todaySplit[0];
		}
		
		if(deptNo.equals(" ")) {
			
			deptNo = userImpl.getDeptNo(); 							//현재 로그인한 유저의 userNo 받기
		}
		
		/* 검색조건 담기 */
		Map<String, Object> searchCriteria = new HashMap<>();
		searchCriteria.put("yearInt", Integer.parseInt(year));
		searchCriteria.put("deptNo", deptNo);
		searchCriteria.put("measure", measure);
		
		Map<String, Object> map = goalService.selectTeamGoal(searchCriteria);
		
		List<GoalReturnDTO> goalList = (List<GoalReturnDTO>)map.get("productReturnList");
		
		return goalList;
	}
	
	/* 목표 설정 */
	@PostMapping("/team/insert")
	public ModelAndView insertTeamGoal(ModelAndView mv,
			@RequestParam Map<String, String> goalMap, 
			RedirectAttributes rttr,
			@AuthenticationPrincipal UserImpl customUser,
			Locale locale) {
		
		/* 월별 목표를 받아서 List에 add */
		List<String> goalMonthlyScales = new ArrayList<>();
		goalMonthlyScales.add(goalMap.get("jan"));
		goalMonthlyScales.add(goalMap.get("feb"));
		goalMonthlyScales.add(goalMap.get("mar"));
		goalMonthlyScales.add(goalMap.get("apr"));
		goalMonthlyScales.add(goalMap.get("may"));
		goalMonthlyScales.add(goalMap.get("jun"));
		goalMonthlyScales.add(goalMap.get("jul"));
		goalMonthlyScales.add(goalMap.get("aug"));
		goalMonthlyScales.add(goalMap.get("sep"));
		goalMonthlyScales.add(goalMap.get("oct"));
		goalMonthlyScales.add(goalMap.get("nov"));
		goalMonthlyScales.add(goalMap.get("dec"));
		
		List<Integer> month = new ArrayList<>();
		for(int i = 1; i < 13; i++) {
			month.add(i);
		}
		
		/* insert에 필요한 조건들을 DTO에 set */
		GoalInsertDTO goalInsertDTO = new GoalInsertDTO();
		goalInsertDTO.setUpdateProductName(goalMap.get("updateProductName"));
		goalInsertDTO.setUpdateProductNo(goalMap.get("updateProductNo"));
		goalInsertDTO.setYear(Integer.parseInt(goalMap.get("updateYear")));
		goalInsertDTO.setMonth(month);
		goalInsertDTO.setGoalMonthlyScales(goalMonthlyScales);
		goalInsertDTO.setDivision(goalMap.get("measure"));
		
		if(goalMap.get("updateDeptNo").equals(" ") || goalMap.get("updateDeptNo").equals("")) {
			
			/* 현재 로그인한 유저의 deptNo 받기 */
			String deptNo = customUser.getDeptNo();
			goalInsertDTO.setDeptNo(deptNo);
			
		} else {
			goalInsertDTO.setDeptNo(goalMap.get("updateDeptNo"));
		}
		
		/* 목표 삽입하기 */
		int result = goalService.insertTeamGoal(goalInsertDTO);
		
		if(result > 0) {
			rttr.addFlashAttribute("successMessage", "목표 등록 성공!");
		} else {
			rttr.addFlashAttribute("successMessage", "목표 등록 실패...");			
		}
			
		mv.setViewName("redirect:goal/team/select");
		
		return mv;
	}
	
	/* 목표 수정 */
	@PostMapping("/team/update")
	public ModelAndView updateTeamGoal(ModelAndView mv,
			@RequestParam Map<String, String> goalMap, 
			RedirectAttributes rttr, Locale locale) {
		
		/* 월별 목표(수정할 no와 목표값)를 받아서 List에 add */
		List<GoalUpdateDTO> updateGoalList = new ArrayList<>();
		
		for(int i = 1; i < 13; i++) {
			String getGoalNo = goalMap.get("GoalNoUpdate" + i);
			String getGoalScale = goalMap.get("scaleUpdate" + i);
			
			GoalUpdateDTO newGoal = new GoalUpdateDTO();
			newGoal.setGoalNo(getGoalNo);
			newGoal.setGoalScale(Integer.parseInt(getGoalScale));
			
			updateGoalList.add(newGoal);
		}
		
		System.out.println("updateGoalList : " + updateGoalList);
		
		/* 목표 수정 */
		int result = goalService.updateCrewGoal(updateGoalList);
		
		if(result > 0) {
			rttr.addFlashAttribute("successMessage", "목표 수정 성공!");
		} else {
			rttr.addFlashAttribute("successMessage", "목표 수정 실패...");			
		}
			
		mv.setViewName("redirect:goal/team/select");
		
		return mv;
	}

}
