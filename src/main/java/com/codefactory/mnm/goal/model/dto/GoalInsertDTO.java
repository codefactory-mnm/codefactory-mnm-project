package com.codefactory.mnm.goal.model.dto;

import java.util.List;
import java.util.Map;

public class GoalInsertDTO {
	
	private String updateProductName;			//해당 상품명
	private String updateProductNo;				//해당 상품번호
	private Integer year;						//해당 연도
	private List<Integer> month;				//1~12까지 월 List
	private List<String> goalMonthlyScales;		//목표량 List
	private String division;					//구분
	private String userNo;						//사용자번호
	private String deptNo;						//부서번호

	private int monthInt;						//하나씩 꺼내 쓸 월 숫자
	private String goalScale;					//하나씩 꺼내 쓸 목표량
	
	public GoalInsertDTO() {}

	public GoalInsertDTO(String updateProductName, String updateProductNo, Integer year, List<Integer> month,
			List<String> goalMonthlyScales, String division, String userNo, String deptNo, int monthInt,
			String goalScale) {
		super();
		this.updateProductName = updateProductName;
		this.updateProductNo = updateProductNo;
		this.year = year;
		this.month = month;
		this.goalMonthlyScales = goalMonthlyScales;
		this.division = division;
		this.userNo = userNo;
		this.deptNo = deptNo;
		this.monthInt = monthInt;
		this.goalScale = goalScale;
	}

	public String getUpdateProductName() {
		return updateProductName;
	}

	public void setUpdateProductName(String updateProductName) {
		this.updateProductName = updateProductName;
	}

	public String getUpdateProductNo() {
		return updateProductNo;
	}

	public void setUpdateProductNo(String updateProductNo) {
		this.updateProductNo = updateProductNo;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public List<Integer> getMonth() {
		return month;
	}

	public void setMonth(List<Integer> month) {
		this.month = month;
	}

	public List<String> getGoalMonthlyScales() {
		return goalMonthlyScales;
	}

	public void setGoalMonthlyScales(List<String> goalMonthlyScales) {
		this.goalMonthlyScales = goalMonthlyScales;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getDeptNo() {
		return deptNo;
	}

	public void setDeptNo(String deptNo) {
		this.deptNo = deptNo;
	}

	public int getMonthInt() {
		return monthInt;
	}

	public void setMonthInt(int monthInt) {
		this.monthInt = monthInt;
	}

	public String getGoalScale() {
		return goalScale;
	}

	public void setGoalScale(String goalScale) {
		this.goalScale = goalScale;
	}

	@Override
	public String toString() {
		return "GoalInsertDTO [updateProductName=" + updateProductName + ", updateProductNo=" + updateProductNo
				+ ", year=" + year + ", month=" + month + ", goalMonthlyScales=" + goalMonthlyScales + ", division="
				+ division + ", userNo=" + userNo + ", deptNo=" + deptNo + ", monthInt=" + monthInt + ", goalScale="
				+ goalScale + "]";
	}

	
	
}
