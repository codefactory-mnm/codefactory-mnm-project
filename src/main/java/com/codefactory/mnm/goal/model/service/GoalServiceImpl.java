package com.codefactory.mnm.goal.model.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.codefactory.mnm.goal.model.dao.GoalDAO;
import com.codefactory.mnm.goal.model.dto.DepartmentDTO;
import com.codefactory.mnm.goal.model.dto.GoalDTO;
import com.codefactory.mnm.goal.model.dto.GoalInsertDTO;
import com.codefactory.mnm.goal.model.dto.GoalReturnDTO;
import com.codefactory.mnm.goal.model.dto.GoalUpdateDTO;
import com.codefactory.mnm.goal.model.dto.UserDTO;

@Service("goalService")
public class GoalServiceImpl implements GoalService {

	private final GoalDAO goalDAO;

	@Autowired
	public GoalServiceImpl(GoalDAO goalDAO) {
		this.goalDAO = goalDAO;
	}

	/* crew goal 가져오기 */
	@Override
	public Map<String, Object> selectCrewGoal(Map<String, Object> searchCriteria) {

		/* 부서 List로 불러오기 */
		List<DepartmentDTO> deptList = goalDAO.findAllDept();
		
		/* 직원 List로 불러오기 */
		List<UserDTO> userList = goalDAO.findAllUser();
		
		/* 상품 List 검색 (여기에 목표를 덧붙일 것) 
		 * 검색조건으로 받아왔던 것은 미리 set 해둔다 */
		List<GoalReturnDTO> productReturnList = goalDAO.findAllProductNo();
		for(int i = 0; i < productReturnList.size(); i++) {
			productReturnList.get(i).setYear((Integer) searchCriteria.get("yearInt"));
			productReturnList.get(i).setUserNo((String) searchCriteria.get("responsibility"));
			productReturnList.get(i).setDivision((String) searchCriteria.get("measure"));
		}

		/* cruw goal 검색 */
		List<GoalDTO> goalList = goalDAO.selectCrewGoal(searchCriteria);
		
		/* 상품기준으로 goalList 재조립 */
		for(int i = 0; i < productReturnList.size(); i++) {

			/* 해당 상품에 대한 목표가 있는 경우
			 * ProductNo와 ProductName은 검색해온 그대로 사용한다. 
			 * view에서는 1월부터 출력되야 하므로 LinkedHashMap을 사용해준다. 
			 * (일반 HashMap 사용시, 월 출력을 1월부터 할 수 없다.) */
			Map<String, String> goalScaleMap = new LinkedHashMap<>();			
			
			for(int j = 0; j < goalList.size(); j++) {

				if(goalList.get(j).getProductNo().equals(productReturnList.get(i).getProductNo())) {

					/* 월별로 목표번호와 목표매출을 Map에 넣어줌 */
					if(goalList.get(j).getMonth() == 1) {
						goalScaleMap.put(goalList.get(j).getGoalNo(), goalList.get(j).getGoalSales());
					} else if(goalList.get(j).getMonth() == 2) {
						goalScaleMap.put(goalList.get(j).getGoalNo(), goalList.get(j).getGoalSales());
					} else if(goalList.get(j).getMonth() == 3) {
						goalScaleMap.put(goalList.get(j).getGoalNo(), goalList.get(j).getGoalSales());
					} else if(goalList.get(j).getMonth() == 4) {
						goalScaleMap.put(goalList.get(j).getGoalNo(), goalList.get(j).getGoalSales());
					} else if(goalList.get(j).getMonth() == 5) {
						goalScaleMap.put(goalList.get(j).getGoalNo(), goalList.get(j).getGoalSales());
					} else if(goalList.get(j).getMonth() == 6) {
						goalScaleMap.put(goalList.get(j).getGoalNo(), goalList.get(j).getGoalSales());
					} else if(goalList.get(j).getMonth() == 7) {
						goalScaleMap.put(goalList.get(j).getGoalNo(), goalList.get(j).getGoalSales());
					} else if(goalList.get(j).getMonth() == 8) {
						goalScaleMap.put(goalList.get(j).getGoalNo(), goalList.get(j).getGoalSales());
					} else if(goalList.get(j).getMonth() == 9) {
						goalScaleMap.put(goalList.get(j).getGoalNo(), goalList.get(j).getGoalSales());
					} else if(goalList.get(j).getMonth() == 10) {
						goalScaleMap.put(goalList.get(j).getGoalNo(), goalList.get(j).getGoalSales());
					} else if(goalList.get(j).getMonth() == 11) {
						goalScaleMap.put(goalList.get(j).getGoalNo(), goalList.get(j).getGoalSales());
					} else if(goalList.get(j).getMonth() == 12) {
						goalScaleMap.put(goalList.get(j).getGoalNo(), goalList.get(j).getGoalSales());
					} 
					
					productReturnList.get(i).setGoalScales(goalScaleMap);
				} 
			}
		}

		/* Controller 로 보내는 map */
		Map<String, Object> map = new HashMap<>();
		map.put("deptList", deptList);
		map.put("userList", userList);
		
		map.put("productReturnList", productReturnList);
		
		return map;
	}

	/* DB에 저장하는 과정이므로 트랜잭션 처리를 해준다. */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertCrewGoal(GoalInsertDTO newGoal) {

		/* 월단위로 목표를 DB에 저장한다. */
		int result = 0;
		for(int i = 0; i < newGoal.getGoalMonthlyScales().size(); i++) {
			newGoal.setMonthInt(newGoal.getMonth().get(i));
			newGoal.setGoalScale(newGoal.getGoalMonthlyScales().get(i));
			
			result = goalDAO.insertCrewGoal(newGoal);			
		}
		
		return result;
	}

	/* DB에 저장하는 과정이므로 트랜잭션 처리를 해준다. */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int updateCrewGoal(List<GoalUpdateDTO> updateGoalList) {
		
		/* goalNo 단위로 DB에 저장한다. */
		int result = 0;
		for(int i = 0; i < updateGoalList.size(); i++) {
			
			result = goalDAO.updateCrewGoal(updateGoalList.get(i));
		}
		
		return result;
	}

	@Override
	public Map<String, Object> selectTeamGoal(Map<String, Object> searchCriteria) {
		/* 부서 List로 불러오기 */
		List<DepartmentDTO> deptList = goalDAO.findAllDept();
		
		/* 상품 List 검색 (여기에 목표를 덧붙일 것이다) 
		 * 검색조건으로 받아왔던 것은 미리 set 해둔다 */
		List<GoalReturnDTO> productReturnList = goalDAO.findAllProductNo();
		for(int i = 0; i < productReturnList.size(); i++) {
			productReturnList.get(i).setYear((Integer) searchCriteria.get("yearInt"));
			productReturnList.get(i).setDeptNo((String)searchCriteria.get("deptNo"));
			productReturnList.get(i).setDivision((String) searchCriteria.get("measure"));
		}

		/* cruw goal 검색 */
		List<GoalDTO> goalList = goalDAO.selectTeamGoal(searchCriteria);
		
		/* 상품기준으로 goalList 재조립 */
		for(int i = 0; i < productReturnList.size(); i++) {

			/* 해당 상품에 대한 목표가 있는 경우
			 * ProductNo와 ProductName은 검색해온 그대로 사용한다. 
			 * view에서는 1월부터 출력되야 하므로 LinkedHashMap을 사용해준다. 
			 * (일반 HashMap 사용시, 월 출력을 1월부터 할 수 없다.) */
			Map<String, String> goalScaleMap = new LinkedHashMap<>();			
			
			for(int j = 0; j < goalList.size(); j++) {

				if(goalList.get(j).getProductNo().equals(productReturnList.get(i).getProductNo())) {

					/* 월별로 목표번호와 목표매출을 Map에 넣어줌 */
					if(goalList.get(j).getMonth() == 1) {
						goalScaleMap.put(goalList.get(j).getGoalNo(), goalList.get(j).getGoalSales());
					} else if(goalList.get(j).getMonth() == 2) {
						goalScaleMap.put(goalList.get(j).getGoalNo(), goalList.get(j).getGoalSales());
					} else if(goalList.get(j).getMonth() == 3) {
						goalScaleMap.put(goalList.get(j).getGoalNo(), goalList.get(j).getGoalSales());
					} else if(goalList.get(j).getMonth() == 4) {
						goalScaleMap.put(goalList.get(j).getGoalNo(), goalList.get(j).getGoalSales());
					} else if(goalList.get(j).getMonth() == 5) {
						goalScaleMap.put(goalList.get(j).getGoalNo(), goalList.get(j).getGoalSales());
					} else if(goalList.get(j).getMonth() == 6) {
						goalScaleMap.put(goalList.get(j).getGoalNo(), goalList.get(j).getGoalSales());
					} else if(goalList.get(j).getMonth() == 7) {
						goalScaleMap.put(goalList.get(j).getGoalNo(), goalList.get(j).getGoalSales());
					} else if(goalList.get(j).getMonth() == 8) {
						goalScaleMap.put(goalList.get(j).getGoalNo(), goalList.get(j).getGoalSales());
					} else if(goalList.get(j).getMonth() == 9) {
						goalScaleMap.put(goalList.get(j).getGoalNo(), goalList.get(j).getGoalSales());
					} else if(goalList.get(j).getMonth() == 10) {
						goalScaleMap.put(goalList.get(j).getGoalNo(), goalList.get(j).getGoalSales());
					} else if(goalList.get(j).getMonth() == 11) {
						goalScaleMap.put(goalList.get(j).getGoalNo(), goalList.get(j).getGoalSales());
					} else if(goalList.get(j).getMonth() == 12) {
						goalScaleMap.put(goalList.get(j).getGoalNo(), goalList.get(j).getGoalSales());
					} 
					
					productReturnList.get(i).setGoalScales(goalScaleMap);
				} 
			}
		}

		/* Controller 로 보내는 map */
		Map<String, Object> map = new HashMap<>();
		map.put("deptList", deptList);
		map.put("productReturnList", productReturnList);
		
		return map;
	}
	
	/* DB에 저장하는 과정이므로 트랜잭션 처리를 해준다. */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int insertTeamGoal(GoalInsertDTO newGoal) {

		/* 월단위로 목표를 DB에 저장한다. */
		int result = 0;
		for(int i = 0; i < newGoal.getGoalMonthlyScales().size(); i++) {
			newGoal.setMonthInt(newGoal.getMonth().get(i));
			newGoal.setGoalScale(newGoal.getGoalMonthlyScales().get(i));
			result = goalDAO.insertTeamGoal(newGoal);			
		}
		
		return result;
	}
	
	/* DB에 저장하는 과정이므로 트랜잭션 처리를 해준다. */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = {Exception.class})
	public int updateTeamGoal(List<GoalUpdateDTO> updateGoalList) {
		
		/* goalNo 단위로 DB에 저장한다. */
		int result = 0;
		for(int i = 0; i < updateGoalList.size(); i++) {
			
			result = goalDAO.updateTeamGoal(updateGoalList.get(i));
		}
		
		return result;
	}

}
