package com.codefactory.mnm.goal.model.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.codefactory.mnm.goal.model.dto.DepartmentDTO;
import com.codefactory.mnm.goal.model.dto.GoalDTO;
import com.codefactory.mnm.goal.model.dto.GoalInsertDTO;
import com.codefactory.mnm.goal.model.dto.GoalReturnDTO;
import com.codefactory.mnm.goal.model.dto.GoalUpdateDTO;
import com.codefactory.mnm.goal.model.dto.UserDTO;

@Mapper
public interface GoalDAO {

	List<DepartmentDTO> findAllDept();

	List<UserDTO> findAllUser();

	List<GoalReturnDTO> findAllProductNo();
	
	List<GoalDTO> selectCrewGoal(Map<String, Object> searchCriteria);
	
	int insertCrewGoal(GoalInsertDTO newGoal);

	int updateCrewGoal(GoalUpdateDTO goalUpdateDTO);

	List<GoalDTO> selectTeamGoal(Map<String, Object> searchCriteria);

	int insertTeamGoal(GoalInsertDTO newGoal);

	int updateTeamGoal(GoalUpdateDTO goalUpdateDTO);

}
