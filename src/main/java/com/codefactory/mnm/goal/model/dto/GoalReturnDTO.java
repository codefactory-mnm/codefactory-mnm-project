package com.codefactory.mnm.goal.model.dto;

import java.util.Map;

public class GoalReturnDTO {
	
	private String productNo;					//상품번호
	private String productName;					//상품이름
	private Integer year;						//년
	private String division;					//구분
	private String userNo;						//사용자번호
	private String deptNo;						//부서번호
	private int totalCount;						//총합
	private Map<String, String> goalScales;		//월별 목표량
	
	public GoalReturnDTO() {}

	public GoalReturnDTO(String productNo, String productName, Integer year, String division, String userNo,
			String deptNo, int totalCount, Map<String, String> goalScales) {
		super();
		this.productNo = productNo;
		this.productName = productName;
		this.year = year;
		this.division = division;
		this.userNo = userNo;
		this.deptNo = deptNo;
		this.totalCount = totalCount;
		this.goalScales = goalScales;
	}

	public String getProductNo() {
		return productNo;
	}

	public void setProductNo(String productNo) {
		this.productNo = productNo;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getDeptNo() {
		return deptNo;
	}

	public void setDeptNo(String deptNo) {
		this.deptNo = deptNo;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public Map<String, String> getGoalScales() {
		return goalScales;
	}

	public void setGoalScales(Map<String, String> goalScales) {
		this.goalScales = goalScales;
	}

	@Override
	public String toString() {
		return "GoalReturnDTO [productNo=" + productNo + ", productName=" + productName + ", year=" + year
				+ ", division=" + division + ", userNo=" + userNo + ", deptNo=" + deptNo + ", totalCount=" + totalCount
				+ ", goalScales=" + goalScales + "]";
	}

	
	

}
