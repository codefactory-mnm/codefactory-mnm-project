package com.codefactory.mnm.goal.model.service;

import java.util.List;
import java.util.Map;

import com.codefactory.mnm.goal.model.dto.DepartmentDTO;
import com.codefactory.mnm.goal.model.dto.GoalDTO;
import com.codefactory.mnm.goal.model.dto.GoalInsertDTO;
import com.codefactory.mnm.goal.model.dto.GoalReturnDTO;
import com.codefactory.mnm.goal.model.dto.GoalUpdateDTO;
import com.codefactory.mnm.goal.model.dto.UserDTO;

public interface GoalService {

	Map<String, Object> selectCrewGoal(Map<String, Object> searchCriteria);

	int insertCrewGoal(GoalInsertDTO goalInsertDTO);
	
	int updateCrewGoal(List<GoalUpdateDTO> updateGoalList);

	Map<String, Object> selectTeamGoal(Map<String, Object> searchCriteria);

	int insertTeamGoal(GoalInsertDTO goalInsertDTO);
	
	int updateTeamGoal(List<GoalUpdateDTO> updateGoalList);

}
