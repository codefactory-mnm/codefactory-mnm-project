package com.codefactory.mnm.goal.model.dto;

import java.io.Serializable;

public class DepartmentDTO implements Serializable{
	
	private String deptNo;				//부서번호
	private String deptName;			//부서명
	private String refDeptNo;			//상위부서번호
	private int sortOrder;				//정렬순서
	private String deptHead;			//부서장
	
	public DepartmentDTO() {}

	public DepartmentDTO(String deptNo, String deptName, String refDeptNo, int sortOrder, String deptHead) {
		super();
		this.deptNo = deptNo;
		this.deptName = deptName;
		this.refDeptNo = refDeptNo;
		this.sortOrder = sortOrder;
		this.deptHead = deptHead;
	}

	public String getDeptNo() {
		return deptNo;
	}

	public void setDeptNo(String deptNo) {
		this.deptNo = deptNo;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getRefDeptNo() {
		return refDeptNo;
	}

	public void setRefDeptNo(String refDeptNo) {
		this.refDeptNo = refDeptNo;
	}

	public int getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getDeptHead() {
		return deptHead;
	}

	public void setDeptHead(String deptHead) {
		this.deptHead = deptHead;
	}

	@Override
	public String toString() {
		return "DepartmentDTO [deptNo=" + deptNo + ", deptName=" + deptName + ", refDeptNo=" + refDeptNo
				+ ", sortOrder=" + sortOrder + ", deptHead=" + deptHead + "]";
	}
	
	

}
