package com.codefactory.mnm.goal.model.dto;

import java.io.Serializable;

public class GoalDTO implements Serializable{
	
	private String goalNo;				//목표번호
	private String productNo;			//상품번호
	private String productName;			//상품이름
	private Integer year;				//년
	private Integer month;				//월
	private String goalSales;			//목표매출
	private String division;			//구분
	private String userNo;				//사용자번호
	private String deptNo;				//부서번호
	
	public GoalDTO() {}

	public GoalDTO(String goalNo, String productNo, String productName, Integer year, Integer month, String goalSales,
			String division, String userNo, String deptNo) {
		super();
		this.goalNo = goalNo;
		this.productNo = productNo;
		this.productName = productName;
		this.year = year;
		this.month = month;
		this.goalSales = goalSales;
		this.division = division;
		this.userNo = userNo;
		this.deptNo = deptNo;
	}

	public String getGoalNo() {
		return goalNo;
	}

	public void setGoalNo(String goalNo) {
		this.goalNo = goalNo;
	}

	public String getProductNo() {
		return productNo;
	}

	public void setProductNo(String productNo) {
		this.productNo = productNo;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public String getGoalSales() {
		return goalSales;
	}

	public void setGoalSales(String goalSales) {
		this.goalSales = goalSales;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getDeptNo() {
		return deptNo;
	}

	public void setDeptNo(String deptNo) {
		this.deptNo = deptNo;
	}

	@Override
	public String toString() {
		return "GoalDTO [goalNo=" + goalNo + ", productNo=" + productNo + ", productName=" + productName + ", year="
				+ year + ", month=" + month + ", goalSales=" + goalSales + ", division=" + division + ", userNo="
				+ userNo + ", deptNo=" + deptNo + "]";
	}

	
	
	

}
