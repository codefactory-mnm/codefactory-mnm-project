package com.codefactory.mnm.goal.model.dto;

public class GoalUpdateDTO {
	
	private String goalNo;			//목표번호
	private int goalScale;			//목표량
	
	public GoalUpdateDTO() {}

	public GoalUpdateDTO(String goalNo, int goalScale) {
		super();
		this.goalNo = goalNo;
		this.goalScale = goalScale;
	}

	public String getGoalNo() {
		return goalNo;
	}

	public void setGoalNo(String goalNo) {
		this.goalNo = goalNo;
	}

	public int getGoalScale() {
		return goalScale;
	}

	public void setGoalScale(int goalScale) {
		this.goalScale = goalScale;
	}

	@Override
	public String toString() {
		return "GoalUpdateDTO [goalNo=" + goalNo + ", goalScale=" + goalScale + "]";
	}
	
	

}
