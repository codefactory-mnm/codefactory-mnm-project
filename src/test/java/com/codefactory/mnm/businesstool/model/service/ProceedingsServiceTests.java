package com.codefactory.mnm.businesstool.model.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.codefactory.mnm.businesstool.model.dto.ClientCompanyDTO;
import com.codefactory.mnm.businesstool.model.dto.ClientDTO;
import com.codefactory.mnm.businesstool.model.dto.ProceedingsAttachmentDTO;
import com.codefactory.mnm.businesstool.model.dto.ProceedingsCollectionDTO;
import com.codefactory.mnm.businesstool.model.dto.ProceedingsDTO;
import com.codefactory.mnm.businesstool.model.dto.ProceedingsSearchDTO;
import com.codefactory.mnm.businesstool.model.dto.UserDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanySearchDTO;
import com.codefactory.mnm.common.Pagenation;
import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.configuration.CodefactoryMnmProjectApplication;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes= {CodefactoryMnmProjectApplication.class})
public class ProceedingsServiceTests {
	
	@Autowired
	private ProceedingsService proceedingsService;

	private ProceedingsDTO proceedings;
	private ProceedingsAttachmentDTO proceedingsAttachment;
	private ClientDTO client;
	private UserDTO user;
	private ProceedingsCollectionDTO proceedingsCollection;
	private ProceedingsSearchDTO proceedingsSearch;
	
	@BeforeEach
	public void init() {
		
		proceedings = new ProceedingsDTO();
		proceedings.setProceedingsNo("PD2");
		proceedings.setClientCompanyNo("CC1");
		proceedings.setContent("내용");
		proceedings.setEndTime("11:11");
		proceedings.setLocation("세미나실");
		proceedings.setProceedingsDate(java.sql.Date .valueOf("2021-06-22"));
		proceedings.setProceedingsName("회의명");
		proceedings.setStartTime("10:10");
		
		String filePath = "C:\\final\\codefactory-mnm-project\\src\\main\\webapp\\static/upload/";
		
		proceedingsAttachment = new ProceedingsAttachmentDTO();
		proceedingsAttachment.setFileName("FDSDFASDFASDFASDF213ASD.txt");
		proceedingsAttachment.setFileOriginalName("테스트.txt");
		proceedingsAttachment.setFilePath(filePath);
		
		List<ProceedingsAttachmentDTO> files = new ArrayList<>();
		files.add(proceedingsAttachment);
		
		client = new ClientDTO();
		client.setClientNo("C1");
		
		List<ClientDTO> clientList = new ArrayList<>();
		clientList.add(client);
		
		user = new UserDTO();
		user.setUserNo("U024");
		
		List<UserDTO> userList = new ArrayList<>();
		userList.add(user);
		
		proceedingsCollection = new ProceedingsCollectionDTO();
		proceedingsCollection.setClientList(clientList);
		proceedingsCollection.setFiles(files);
		proceedingsCollection.setProceedings(proceedings);
		proceedingsCollection.setUserList(userList);
	}
	
	/* 회의록 리스트 조회 */
	@Test
	@Disabled
	public void testSelectProceedingsList() {
		
		SelectCriteria selectCriteria = Pagenation.getSelectCriteria(1, 5, 10, 2);
		
		proceedingsSearch = new ProceedingsSearchDTO();
		proceedingsSearch.setClientCompanyName(null);
		proceedingsSearch.setClientName(null);
		proceedingsSearch.setCurrentPage("1");
		proceedingsSearch.setProceedingsName(null);
		proceedingsSearch.setSelectCriteria(selectCriteria);
				
		Map<String, Object> selectProceedingsList = proceedingsService.selectProceedingsList(proceedingsSearch);
				
		
		assertNotNull(selectProceedingsList);
	}
	
	/* 회의록 조회 */
	@Test
	@Disabled
	public void testSelectProceedings() {
		
		 Map<String, Object> proceedings = proceedingsService.selectProceedings("PD2");
		 
		 assertNotNull(proceedings);
		
	}
	
	/* 첨부파일 조회 */
	@Test
	@Disabled
	public void testSelectFile() {
		
		ProceedingsAttachmentDTO file = proceedingsService.selectFile("F183");
		 
		 assertNotNull(file);
		
	}
	
	/* 고객사 리스트 조회 */
	@Test
	@Disabled
	public void testSelectClientCompanyList() {
		
		 List<ClientCompanyDTO> clientCompanyList = proceedingsService.selectClientCompanyList();
		 
		 assertNotNull(clientCompanyList);
		
	}
	
	/* 회의 참석자(고객) 리스트 조회 */
	@Test
	@Disabled
	public void testSelectClientList() {
		
		 List<ClientDTO> clientList = proceedingsService.selectClientList("CC1");
		 
		 assertNotNull(clientList);
		
	}
	
	/* 회의 참석자(자사) 리스트 조회 */
	@Test
	@Disabled
	public void testSelectUserList() {
		
		 List<UserDTO> userList = proceedingsService.selectUserList();
		 
		 assertNotNull(userList);
		
	}
	
	/* 회의록 등록 */
	@Test
	@Disabled
	public void testInsertProceedings() {
		
		int result = proceedingsService.insertProceedings(proceedingsCollection);
		
		assertEquals(1, result);
	}
	
	/* 첨부파일 삭제 */
	@Test
	@Disabled
	public void testDeleteFile() {
		
		Map<String, Object> file = proceedingsService.deleteFile("F181");
		 
		 assertNotNull(file);
		
	}
	
	/* 회의 참석자(자사) 등록 */
	@Test
	@Disabled
	public void testInsertConferenceAttendeesUsers() {
		
		int result = proceedingsService.insertConferenceAttendeesUsers("PD2", "U1");
		
		assertEquals(1, result);
	}
	
	/* 회의 참석자(자사) 삭제 */
	@Test
	@Disabled
	public void testDeleteConferenceAttendeesUsers() {
		
		int result = proceedingsService.deleteConferenceAttendeesUsers("PD2", "U024");
		
		assertEquals(1, result);
	}
	
	/* 회의 참석자(고객) 등록 */
	@Test
	@Disabled
	public void testInsertConferenceAttendeesClient() {
		
		int result = proceedingsService.insertConferenceAttendeesClient("PD2", "C2");
		
		assertEquals(1, result);
	}
	
	/* 회의 참석자(고객) 삭제 */
	@Test
	@Disabled
	public void testDeleteConferenceAttendeesClient() {
		
		int result = proceedingsService.deleteConferenceAttendeesClient("PD2", "C2");
		
		assertEquals(1, result);
	}
	
	/* 회의록 수정 */
	@Test
	@Disabled
	public void testUpdateProceedings() {
		
		int result = proceedingsService.updateProceedings(proceedingsCollection);
		
		assertEquals(1, result);
	}
	
	/* 회의록 삭제 */
	@Test
	@Disabled
	public void testDeleteProceedings() {
		
		int result = proceedingsService.deleteProceedings("PD14");
		
		assertEquals(1, result);
	}
	
}
