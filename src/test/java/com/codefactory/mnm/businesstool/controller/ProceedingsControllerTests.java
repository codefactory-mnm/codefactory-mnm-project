package com.codefactory.mnm.businesstool.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.codefactory.mnm.configuration.CodefactoryMnmProjectApplication;
import com.codefactory.mnm.configuration.MybatisConfiguration;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {CodefactoryMnmProjectApplication.class,  
		MybatisConfiguration.class})
@SpringBootTest
public class ProceedingsControllerTests {
	
	@Autowired
	private ProceedingsController proceedingsController;
	
	private MockMvc mockMvc;
	
	@BeforeEach					
	public void setUp() {
		mockMvc = MockMvcBuilders.standaloneSetup(proceedingsController).build();			
	}
	
	/* 회의록 리스트 조회 */
	@Test
	@Disabled
	public void testSelectProceedingsList() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("selectSearch", "고객사");
		params.add("searchValue", "고객사");
		params.add("currentPage", "1");
		
		 mockMvc.perform(get("/proceedings/list").params(params))  					
		.andExpect(status().isOk())							
		.andExpect(forwardedUrl("proceedings/list"))				
		.andDo(print());
		
	}
	
	/* 회의록 상세조회 */
	@Test
	@Disabled
	public void testSelectProceedings() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("proceedingsNo", "PD2");
	
		 mockMvc.perform(get("/proceedings/select").params(params))  					
		.andExpect(status().isOk())							
		.andExpect(forwardedUrl("proceedings/select"))				
		.andDo(print());
		
	}
	
	/* 파일첨부 다운로드 */
	@Test
	@Disabled
	public void testFileDownload() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("fileNo", "F202");
		
		 mockMvc.perform(get("/proceedings/fileDownload").params(params))  					
		.andExpect(status().isOk())										
		.andDo(print());
		
	}
	
	/* 고객사 리스트 조회 */
	@Test
	@Disabled
	public void testSelectClientCompanyList() throws Exception {
		
		 mockMvc.perform(get("/proceedings/clientCompany"))  					
		.andExpect(status().isOk())										
		.andDo(print());
		
	}
	
	/* 회의 참석자(고객) 리스트 조회 */
	@Test
	@Disabled
	public void testSelectClientList() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("clientCompanyNo", "CC1");
		
		 mockMvc.perform(get("/proceedings/client").params(params))  					
		.andExpect(status().isOk())										
		.andDo(print());
		
	}
	
	/* 회의 참석자(자사) 리스트 조회 */
	@Test
	@Disabled
	public void testSelectUserList() throws Exception {
		
		 mockMvc.perform(get("/proceedings/user"))  					
		.andExpect(status().isOk())										
		.andDo(print());
		
	}
	
	/* 회의록 작성화면으로 이동 */
	@Test
	@Disabled
	public void testInsertProceedingsPage() throws Exception {
		
		 mockMvc.perform(get("/proceedings/insert"))  					
		.andExpect(status().isOk())							
		.andExpect(forwardedUrl("proceedings/insert"))				
		.andDo(print());
		
	}
	
	/* 회의록 등록 */
//	@Test
//	@Disabled
	public void testInsertProceedings() throws Exception {

		
	}
	
	/* 회의록 수정페이지 조회 */
	@Test
	@Disabled
	public void testSelectProceedingsByUpdate() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("proceedingsNo", "PD2");
	
		 mockMvc.perform(get("/proceedings/update").params(params))  					
		.andExpect(status().isOk())							
		.andExpect(forwardedUrl("proceedings/update"))				
		.andDo(print());
		
	}
	
	/* 첨부파일 삭제 */
	@Test
	@Disabled
	public void testDeleteFile() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("fileNo", "F184");
		
		 mockMvc.perform(post("/proceedings/file/delete").params(params))  					
		.andExpect(status().isOk())										
		.andDo(print());
		
	}
	
	/* 회의 참석자(자사) 등록 */
	@Test
	@Disabled
	public void testInsertConferenceAttendeesUsers() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("proceedingsNo", "PD15");
		params.add("userNo", "U024");
		
		 mockMvc.perform(post("/proceedings/conferenceAttendeesUsers/insert").params(params))  					
		.andExpect(status().isOk())										
		.andDo(print());
		
	}
	
	/* 회의 참석자(자사) 삭제 */
	@Test
	@Disabled
	public void testDeleteConferenceAttendeesUsers() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("proceedingsNo", "PD15");
		params.add("userNo", "U024");
		
		 mockMvc.perform(post("/proceedings/conferenceAttendeesUsers/delete").params(params))  					
		.andExpect(status().isOk())										
		.andDo(print());
		
	}
	
	/* 회의 참석자(고객) 등록 */
	@Test
	@Disabled
	public void testInsertConferenceAttendeesClient() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("proceedingsNo", "PD15");
		params.add("clientNo", "C2");
		
		 mockMvc.perform(post("/proceedings/conferenceAttendeesClient/insert").params(params))  					
		.andExpect(status().isOk())										
		.andDo(print());
		
	}
	
	/* 회의 참석자(자사) 삭제 */
	@Test
	@Disabled
	public void testDeleteConferenceAttendeesClient() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("proceedingsNo", "PD15");
		params.add("clientNo", "C2");
		
		 mockMvc.perform(post("/proceedings/conferenceAttendeesClient/delete").params(params))  					
		.andExpect(status().isOk())										
		.andDo(print());
		
	}
	
	/* 회의록 수정 */
//	@Test
//	@Disabled
	public void testUpdateProceedings() throws Exception {

		
	}
	
	/* 회의록 삭제 */
	@Test
	@Disabled
	public void testDeleteProceedings() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("proceedingsNo", "PD15");
		
		 mockMvc.perform(post("/proceedings/delete").params(params))  					
		.andExpect(status().isOk())										
		.andDo(print());
		
	}
}
