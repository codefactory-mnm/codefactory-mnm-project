package com.codefactory.mnm.board.model.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.codefactory.mnm.board.model.dto.BoardDTO;
import com.codefactory.mnm.configuration.CodefactoryMnmProjectApplication;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes= {CodefactoryMnmProjectApplication.class})
public class BoardServiceTests {
	
	@Autowired
	private BoardService boardService;

	/* 보드 별 고객사 리스트 조회 */
	@Test
	@Disabled
	public void testSelectClientCompany() {
		
		Map<String, Object> clientCompanyList = boardService.selectClientCompany();
		 
		 assertNotNull(clientCompanyList);
		
	}
	
	/*  고객사 보드명 조회 */
	@Test
	@Disabled
	public void testSelectBoardByClientCompany() {
		
		 List<BoardDTO> boardNameList = boardService.selectBoardByClientCompany();
		 
		 assertNotNull(boardNameList);
		
	}
	
	/*  보드별 고객사 등록 */
	@Test
	@Disabled
	public void testCopyBoardByClientCompany() {
		
		int result = boardService.copyBoardByClientCompany("BRD4", "CC126");
		 
		assertEquals(1, result);
		
	}
	
	/*  보드별 고객사 삭제 */
	@Test
	@Disabled
	public void testDeleteBoardByClientCompany() {
		
		int result = boardService.deleteBoardByClientCompany("BRD4", "CC126");
		 
		assertEquals(1, result);
		
	}
	
	/*  고객사 보드 등록 */
	@Test
	@Disabled
	public void testInsertBoardByClientCompany() {
		
		int result = boardService.insertBoardByClientCompany("방문필요");
		 
		assertEquals(1, result);
		
	}
	
	/*  보드명 수정 */
	@Test
	@Disabled
	public void testUpdateBoardName() {
		
		int result = boardService.updateBoardName("BRD5", "접촉금지");
		 
		assertEquals(1, result);
		
	}
	
	/*  보드 색상 수정 */
	@Test
	@Disabled
	public void testUpdateBoardColor() {
		
		int result = boardService.updateBoardColor("BRD5", "GRAY");
		 
		assertEquals(1, result);
		
	}
	
	/*  보드 삭제 */
	@Test
	@Disabled
	public void testDeleteBoard() {
		
		int result = boardService.deleteBoard("BRD23");
		 
		assertEquals(1, result);
		
	}
	
	/* 보드 별 고객 리스트 조회 */
	@Test
	@Disabled
	public void selectClient() {
		
		Map<String, Object> clientList = boardService.selectClient();
		 
		 assertNotNull(clientList);
		
	}
	
	/*  고객 보드명 조회 */
	@Test
	@Disabled
	public void testSelectBoardByClient() {
		
		 List<BoardDTO> boardNameList = boardService.selectBoardByClient();
		 
		 assertNotNull(boardNameList);
		
	}
	
	/*  보드별 고객 등록 */
	@Test
	@Disabled
	public void testCopyBoardByClient() {
		
		int result = boardService.copyBoardByClient("BRD32", "C1");
		 
		assertEquals(1, result);
		
	}
	
	/*  보드별 고객 삭제 */
	@Test
	@Disabled
	public void testDeleteBoardByClient() {
		
		int result = boardService.deleteBoardByClient("BRD32", "C1");
		 
		assertEquals(1, result);
		
	}
	
	/* 고객 보드 등록 */
	@Test
	@Disabled
	public void testInsertBoardByClient() {
		
		int result = boardService.insertBoardByClient("방문예정");
		 
		assertEquals(1, result);
		
	}
	
	/* 보드 별 영업기회 리스트 조회 */
	@Test
	@Disabled
	public void testSelectBusinessOpportunityList() {
		
		Map<String, Object> businessOpportunityList = boardService.selectBusinessOpportunity();
		 
		 assertNotNull(businessOpportunityList);
		
	}
	
	/*  영업기회 보드명 조회  */
	@Test
	@Disabled
	public void testSelectBoardByBusinessOpportunity() {
		
		 List<BoardDTO> boardNameList = boardService.selectBoardByBusinessOpportunity();
		 
		 assertNotNull(boardNameList);
		
	}
	
	/*  보드별 영업기회 등록 */
	@Test
	@Disabled
	public void testCopyBoardByBusinessOpportunity() {
		
		int result = boardService.copyBoardByBusinessOpportunity("BRD39", "BO1");
		 
		assertEquals(1, result);
		
	}
	
	/*  보드별 영업기회 삭제 */
	@Test
	@Disabled
	public void testDeleteBoardByBusinessOpportunity() {
		
		int result = boardService.deleteBoardByBusinessOpportunity("BRD39", "BO1");
		 
		assertEquals(1, result);
		
	}
	
	/*  영업기회 보드 등록 */
	@Test
	@Disabled
	public void testInsertBoardByBusinessOpportunity() {
		
		int result = boardService.insertBoardByBusinessOpportunity("방문예정");
		 
		assertEquals(1, result);
		
	}
	
}
