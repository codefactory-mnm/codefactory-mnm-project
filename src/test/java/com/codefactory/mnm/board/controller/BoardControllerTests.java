package com.codefactory.mnm.board.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.codefactory.mnm.configuration.CodefactoryMnmProjectApplication;
import com.codefactory.mnm.configuration.MybatisConfiguration;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {CodefactoryMnmProjectApplication.class,  
		MybatisConfiguration.class})
@SpringBootTest
public class BoardControllerTests {
	
	@Autowired
	private BoardController boardController;
	
	private MockMvc mockMvc;
	
	@BeforeEach					
	public void setUp() {
		mockMvc = MockMvcBuilders.standaloneSetup(boardController).build();			
	}
	
	/* 보드 별 고객사 리스트 조회 */
	@Test
	@Disabled
	public void testSelectClientCompanyList() throws Exception {

		 mockMvc.perform(get("/board/clientCompany/list"))  					
		.andExpect(status().isOk())							
		.andExpect(forwardedUrl("board/clientCompany"))				
		.andDo(print());
		
	}
	
	/* 고객사 보드명 조회 */
	@Test
	@Disabled
	public void testSelectBoardByClientCompany() throws Exception {
		
		 mockMvc.perform(get("/board/clientCompany/boardName"))  					
		.andExpect(status().isOk())										
		.andDo(print());
		
	}
	
	/* 보드별 고객사 등록 */
	@Test
	@Disabled
	public void testCopyBoardByClientCompany() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("boardNo", "BRD4");
		params.add("clientCompanyNo", "CC84");
		
		mockMvc.perform(post("/board/clientCompany/copy").params(params))				
		.andExpect(status().isOk())	 						
		.andDo(print());
		
	}
	
	/* 보드별 고객사 삭제 */
	@Test
	@Disabled
	public void testDeleteBoardByClientCompany() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("boardNo", "BRD4");
		params.add("clientCompanyNo", "CC84");
		
		mockMvc.perform(post("/board/clientCompany/delete").params(params))				
		.andExpect(status().isOk())	 						
		.andDo(print());
		
	}
	
	/* 고객사 보드 등록 */
	@Test
	@Disabled
	public void testInsertBoardByClientCompany() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("boardName", "대기");
		
		mockMvc.perform(post("/board/clientCompany/insert").params(params))				
		.andExpect(status().isOk())	 						
		.andDo(print());
		
	}
	
	/* 보드명 수정 */
	@Test
	@Disabled
	public void testUpdateBoardName() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("boardNo", "BRD4");
		params.add("boardName", "미팅예약필요");
		
		mockMvc.perform(post("/board/updateName").params(params))				
		.andExpect(status().isOk())	 						
		.andDo(print());
		
	}
	
	/* 보드 색상 수정 */
	@Test
	@Disabled
	public void testUpdateBoardColor() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("boardNo", "BRD4");
		params.add("color", "YELLOW");
		
		mockMvc.perform(post("/board/updateColor").params(params))				
		.andExpect(status().isOk())	 						
		.andDo(print());
		
	}
	
	/* 보드 삭제 */
	@Test
	@Disabled
	public void testDeleteBoard() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("boardNo", "BRD28");

		mockMvc.perform(post("/board/deleteBoard").params(params))				
		.andExpect(status().isOk())	 						
		.andDo(print());
		
	}
	
	/* 보드 별 고객 리스트 조회 */
	@Test
	@Disabled
	public void testSelectClientList() throws Exception {

		 mockMvc.perform(get("/board/client/list"))  					
		.andExpect(status().isOk())							
		.andExpect(forwardedUrl("board/client"))				
		.andDo(print());
		
	}
	
	/* 고객 보드명 조회 */
	@Test
	@Disabled
	public void testSelectBoardByClient() throws Exception {
		
		 mockMvc.perform(get("/board/client/boardName"))  					
		.andExpect(status().isOk())										
		.andDo(print());
		
	}
	
	/* 보드별 고객 등록 */
	@Test
	@Disabled
	public void testCopyBoardByClient() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("boardNo", "BRD34");
		params.add("clientNo", "C1");
		
		mockMvc.perform(post("/board/client/copy").params(params))				
		.andExpect(status().isOk())	 						
		.andDo(print());
		
	}
	
	/* 보드별 고객 삭제 */
	@Test
	@Disabled
	public void testDeleteBoardByClient() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("boardNo", "BRD34");
		params.add("clientNo", "C1");
		
		mockMvc.perform(post("/board/client/delete").params(params))				
		.andExpect(status().isOk())	 						
		.andDo(print());
		
	}
	
	/* 고객 보드 등록 */
	@Test
	@Disabled
	public void testInsertBoardByclient() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("boardName", "대기");
		
		mockMvc.perform(post("/board/client/insert").params(params))				
		.andExpect(status().isOk())	 						
		.andDo(print());
		
	}
	
	/* 보드 별 영업기회 리스트 조회 */
	@Test
	@Disabled
	public void testSelectBusinessOpportunityList() throws Exception {

		 mockMvc.perform(get("/board/businessOpportunity/list"))  					
		.andExpect(status().isOk())							
		.andExpect(forwardedUrl("board/businessOpportunity"))				
		.andDo(print());
		
	}
	
	/* 영업기회 보드명 조회 */
	@Test
	@Disabled
	public void testSelectBoardByBusinessOpportunity() throws Exception {
		
		 mockMvc.perform(get("/board/businessOpportunity/boardName"))  					
		.andExpect(status().isOk())										
		.andDo(print());
		
	}
	
	/* 보드별 영업기회 등록 */
	@Test
	@Disabled
	public void testCopyBoardByBusinessOpportunity() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("boardNo", "BRD39");
		params.add("businessOpportunityNo", "BO1");
		
		mockMvc.perform(post("/board/businessOpportunity/copy").params(params))				
		.andExpect(status().isOk())	 						
		.andDo(print());
		
	}
	
	/* 보드별 영업기회 삭제 */
	@Test
	@Disabled
	public void testDeleteBoardByBusinessOpportunity() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("boardNo", "BRD39");
		params.add("businessOpportunityNo", "BO1");
		
		mockMvc.perform(post("/board/businessOpportunity/delete").params(params))				
		.andExpect(status().isOk())	 						
		.andDo(print());
		
	}
	
	/* 영업기회 보드 등록 */
	@Test
	@Disabled
	public void testInsertBoardByBusinessOpportunity() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("boardName", "대기");
		
		mockMvc.perform(post("/board/businessOpportunity/insert").params(params))				
		.andExpect(status().isOk())	 						
		.andDo(print());
		
	}
	
}
