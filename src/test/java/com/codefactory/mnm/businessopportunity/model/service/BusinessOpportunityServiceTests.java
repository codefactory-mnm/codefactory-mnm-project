package com.codefactory.mnm.businessopportunity.model.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.codefactory.mnm.businessopportunity.model.dto.ApprovalDTO;
import com.codefactory.mnm.businessopportunity.model.dto.ApproverListDTO;
import com.codefactory.mnm.businessopportunity.model.dto.BusinessOpportunityAttachmentDTO;
import com.codefactory.mnm.businessopportunity.model.dto.BusinessOpportunityCollectionDTO;
import com.codefactory.mnm.businessopportunity.model.dto.BusinessOpportunityDTO;
import com.codefactory.mnm.businessopportunity.model.dto.BusinessOpportunitySearchDTO;
import com.codefactory.mnm.businessopportunity.model.dto.ClientDTO;
import com.codefactory.mnm.businessopportunity.model.dto.DepartmentDTO;
import com.codefactory.mnm.businessopportunity.model.dto.OpinionDTO;
import com.codefactory.mnm.businessopportunity.model.dto.ProductListDTO;
import com.codefactory.mnm.businessopportunity.model.dto.UserDTO;
import com.codefactory.mnm.common.Pagenation;
import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.configuration.CodefactoryMnmProjectApplication;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes= {CodefactoryMnmProjectApplication.class})
public class BusinessOpportunityServiceTests {
	
	@Autowired
	private BusinessOpportunityService businessOpportunityService;
	
	private BusinessOpportunityDTO businessOpportunity;
	private BusinessOpportunityAttachmentDTO businessOpportunityAttachment;
	private ProductListDTO product;
	private ApprovalDTO approval;
	private BusinessOpportunityCollectionDTO businessOpportunityCollection;
	private BusinessOpportunitySearchDTO businessOpportunitySearch;
	
	@BeforeEach
	public void init() {
		
		businessOpportunity = new BusinessOpportunityDTO();
		businessOpportunity.setBusinessOpportunityName("테스트");
		businessOpportunity.setExpectedSales("1000000");
		businessOpportunity.setSalesDivision("상품매출");
		businessOpportunity.setBusinessType("민수사업");
		businessOpportunity.setBusinessStartDate(java.sql.Date .valueOf("2021-06-22"));
		businessOpportunity.setBusinessEndDate(java.sql.Date .valueOf("2021-08-22"));
		businessOpportunity.setUserNo("U1");
		businessOpportunity.setClientNo("C1");
		businessOpportunity.setCognitivePathway("신문");
		businessOpportunity.setNote("안녕하세요");
		
		String filePath = "C:\\final\\codefactory-mnm-project\\src\\main\\webapp\\static/upload/";	
		
		businessOpportunityAttachment = new BusinessOpportunityAttachmentDTO();
		businessOpportunityAttachment.setFileName("FDSDFASDFASDFASDF213ASD.txt");
		businessOpportunityAttachment.setFileOriginalName("테스트.txt");
		businessOpportunityAttachment.setFilePath(filePath);
		
		List<BusinessOpportunityAttachmentDTO> files = new ArrayList<>();
		files.add(businessOpportunityAttachment);
		
		product = new ProductListDTO();
		product.setDiscount(10);
		product.setQuantity2(1);
		product.setProductNo("PT1");
		
		List<ProductListDTO> productList = new ArrayList<>();
		productList.add(product);
		
		approval = new ApprovalDTO();
		approval.setApproverNo("U1");
		
		businessOpportunityCollection = new BusinessOpportunityCollectionDTO();
		businessOpportunityCollection.setApproval(approval);
		businessOpportunityCollection.setBusinessOpportunity(businessOpportunity);
		businessOpportunityCollection.setFiles(files);
		businessOpportunityCollection.setProductList(productList);
	}
	
	/* 영업기회 리스트 조회 */
	@Test
	@Disabled
	public void testSelectBusinessOpportunityList() {
		
		SelectCriteria selectCriteria = Pagenation.getSelectCriteria(1, 5, 10, 2);
		
		businessOpportunitySearch = new BusinessOpportunitySearchDTO();
		businessOpportunitySearch.setBusinessOpportunityName(null);
		businessOpportunitySearch.setClientCompanyName(null);
		businessOpportunitySearch.setClientName(null);
		businessOpportunitySearch.setCurrentPage(null);
		businessOpportunitySearch.setDeptName(null);
		businessOpportunitySearch.setSelectCriteria(selectCriteria);
		businessOpportunitySearch.setUserNo(null);
		
		
		Map<String, Object> businessOpportunityList = businessOpportunityService.selectBusinessOpportunityList(businessOpportunitySearch);
		 
		 assertNotNull(businessOpportunityList);
		
	}
	
	/* 부서 리스트 조회 */
	@Test
	@Disabled
	public void testSelectDepartmentList() {
		
		 List<DepartmentDTO> departmentList = businessOpportunityService.selectDepartmentList();
		 
		 assertNotNull(departmentList);
		
	}
	
	/*  영업기회 상세조회 */
	@Test
	@Disabled
	public void testSelectBusinessOpportunity() {
		
		Map<String, Object> map = businessOpportunityService.selectBusinessOpportunity("BO1");
		
		assertNotNull(map);
		
	}
	
	/* 파일 조회 */
	@Test
	@Disabled
	public void testSelectFile() {
		
		BusinessOpportunityAttachmentDTO selectFile =  businessOpportunityService.selectFile("F121");
			
		assertNotNull(selectFile);
	}
	
	/*  의견 리스트 조회 */
	@Test
	@Disabled
	public void testSelectOpinionList() {
		
		List<OpinionDTO> opinionList = businessOpportunityService.selectOpinionList("BO42");
		
		assertNotNull(opinionList);
		
	}
	
	/* 의견 작성 */
	@Test
	@Disabled
	public void testInsertOpinion() {
		
		int result = businessOpportunityService.insertOpinion("BO42", "안녕하세요", "user01");
		
		assertEquals(1, result);
	}
	
	/* 의견 삭제 */
	@Test
	@Disabled
	public void testDeleteOpinion() {
		
		int result =  businessOpportunityService.deleteOpinion("O110");
		
		assertEquals(1, result);
	}
	
	/* 고객 리스트 조회 */
	@Test
	@Disabled
	public void testSelectClientList() {
		
		 List<ClientDTO> clientList = businessOpportunityService.selectClientList();
		 
		 assertNotNull(clientList);
		
	}
		
	/* 담당자 리스트 조회 */
	@Test
	@Disabled
	public void testSelectUserList() {
		
		 List<UserDTO> userList = businessOpportunityService.selectUserList();
		 
		 assertNotNull(userList);
		
	}
	
	/* 상품 리스트 조회 */
	@Test
	@Disabled
	public void testSelectProductList() {
		
		 List<ProductListDTO> productList = businessOpportunityService.selectProductList();
		 
		 assertNotNull(productList);
		
	}
	
	/* 결재권자 리스트 조회 */
	@Test
	@Disabled
	public void testSelectApproverList() {
		
		 List<ApproverListDTO> approverList = businessOpportunityService.selectApproverList();
		 
		 assertNotNull(approverList);
		
	}
	
	/* 영업기회 등록 */
	@Test
	@Disabled
	public void testInsertBusinessOpportunity() {
		
		int result = businessOpportunityService.insertBusinessOpportunity(businessOpportunityCollection);
		
		assertEquals(1, result);
	}
	
	/* 영업기회 삭제 */
	@Test
//	@Disabled
	public void testDeleteBusinessOpportunity() {
		
		int result = businessOpportunityService.deleteBusinessOpportunity("BO39");
		
		assertEquals(1, result);
	}
	
	
}
