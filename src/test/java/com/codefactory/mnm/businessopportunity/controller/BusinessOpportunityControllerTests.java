package com.codefactory.mnm.businessopportunity.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.codefactory.mnm.configuration.CodefactoryMnmProjectApplication;
import com.codefactory.mnm.configuration.MybatisConfiguration;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {CodefactoryMnmProjectApplication.class,  
		MybatisConfiguration.class})
@SpringBootTest
public class BusinessOpportunityControllerTests {

	@Autowired
	private BusinessOpportunityController businessOpportunityController;
	
	private MockMvc mockMvc;
	

	@BeforeEach					
	public void setUp() {
		mockMvc = MockMvcBuilders.standaloneSetup(businessOpportunityController).build();			
	}
	
	/* 영업기회 리스트 조회 */
	@Test
	@Disabled
	public void testSelectBusinessOpportunityList() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("selectSearch", "고객사");
		params.add("searchValue", "테스트");
		params.add("name", null);
		params.add("userNo", null);
		params.add("progressStatus", null);
		params.add("deptName", null);
		params.add("currentPage", "1");
		
		 mockMvc.perform(get("/businessOpportunity/list").params(params))  					
		.andExpect(status().isOk())							
		.andExpect(forwardedUrl("businessOpportunity/list"))				
		.andDo(print());
		
	}
	
	/* 부서 리스트 조회 */
	@Test
	@Disabled
	public void testSelectDepartmentList() throws Exception {
		
		 mockMvc.perform(get("/businessOpportunity/dept"))  					
		.andExpect(status().isOk())										
		.andDo(print());
		
	}
	
	/* 영업기회 상세조회 */
	@Test
//	@Disabled
	public void testSelectBusinessOpportunity() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("businessOpportunityNo", "C1");

		 mockMvc.perform(get("/businessOpportunity/select").params(params))  					
		.andExpect(status().isOk())							
		.andExpect(forwardedUrl("businessOpportunity/select"))				
		.andDo(print());
		
	}
	
	/* 첨부파일 다운로드 */
	@Test	
//	@Disabled
	public void testFileDownload() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("fileNo", "F123");
		
		mockMvc.perform(get("/businessOpportunity/fileDownload").params(params))				
		.andExpect(status().isOk())	  						
		.andDo(print());
		
	}
	
	/* 의견 리스트 조회 */
//	@Test	
//	@Disabled
	public void testSelectOpinionList() throws Exception {
		
	}
	
	/* 의견 등록 */
//	@Test	
//	@Disabled
	public void testInsertOpinion() throws Exception {
		
	}
	
	/* 의견 삭제 */
	@Test	
//	@Disabled
	public void testDeleteOpinion() throws Exception {
				
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("opinionNo", "O112");

		mockMvc.perform(post("/businessOpportunity/opinion/delete").params(params))				
		.andExpect(status().isOk())	  						
		.andDo(print());
		
	}
	
	/* 고객 리스트 조회 */
	@Test
	@Disabled
	public void testSelectClientList() throws Exception {
		
		 mockMvc.perform(get("/businessOpportunity/client"))  					
		.andExpect(status().isOk())										
		.andDo(print());
		
	}
	
	/* 담당자 리스트 조회 */
	@Test
//	@Disabled
	public void testSelectUserList() throws Exception {
		
		 mockMvc.perform(get("/businessOpportunity/user"))  					
		.andExpect(status().isOk())										
		.andDo(print());
		
	}
	
	/* 상품 리스트 조회 */
	@Test
	@Disabled
	public void testSelectProductList() throws Exception {
		
		 mockMvc.perform(get("/businessOpportunity/product"))  					
		.andExpect(status().isOk())										
		.andDo(print());
		
	}
	
	/* 결재권자 리스트 조회 */
	@Test
	@Disabled
	public void testSelectApproverList() throws Exception {
		
		 mockMvc.perform(get("/businessOpportunity/approver"))  					
		.andExpect(status().isOk())										
		.andDo(print());
		
	}
	
	/* 영업기회 작성화면으로 이동 */
	@Test
	@Disabled
	public void testInsertBusinessOpportunityPage() throws Exception {
		
		 mockMvc.perform(get("/businessOpportunity/insert"))  					
		.andExpect(status().isOk())
		.andExpect(forwardedUrl("businessOpportunity/insert"))	
		.andDo(print());
		
	}
	
	/* 영업기회 등록 */
//	@Test
//	@Disabled
	public void testInsertBusinessOpportunity() throws Exception {
		
	
		
	}
	
	/* ajax를 통해 영업기회 삭제 */
	@Test	
//	@Disabled
	public void testDeleteBusinessOpportunity() throws Exception {
				
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("businessOpportunityNo", "B38");

		mockMvc.perform(post("/businessOpportunity/delete").params(params))				
		.andExpect(status().isOk())	  						
		.andDo(print());
		
	}
	
	
}
