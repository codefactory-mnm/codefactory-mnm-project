package com.codefactory.mnm.client.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.codefactory.mnm.configuration.CodefactoryMnmProjectApplication;
import com.codefactory.mnm.configuration.MybatisConfiguration;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {CodefactoryMnmProjectApplication.class,  
		MybatisConfiguration.class})
@SpringBootTest
public class ClientControllerTests {

	@Autowired
	private ClientController clientController;
	
	private MockMvc mockMvc;
	
	@BeforeEach					
	public void setUp() {
		mockMvc = MockMvcBuilders.standaloneSetup(clientController).build();			
	}
	
	/* 고객 리스트 조회 */
	@Test
	@Disabled
	public void testSelectClientList() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("selectSearch", "고객사");
		params.add("searchValue", "테스트");
		params.add("name", null);
		params.add("currentPage", "1");
		
		 mockMvc.perform(get("/client/list").params(params))  					
		.andExpect(status().isOk())							
		.andExpect(forwardedUrl("client/list"))				
		.andDo(print());		
	}
	
	/* 담당자 리스트 조회 */
	@Test
	@Disabled
	public void testSelectUserList() throws Exception {
		
		mockMvc.perform(get("/client/user"))  					
		.andExpect(status().isOk())										
		.andDo(print());
		
	}
	
	/* 영업활동 리스트 조회 */
	@Test
	@Disabled
	public void testSelectBusinessAcitivityList() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("clientNo", "C1");
		
		mockMvc.perform(get("/client/businessAcitivity").params(params))  					
		.andExpect(status().isOk())										
		.andDo(print());
		
	}
	
	/* 고객 조회 */
	@Test
	@Disabled
	public void testSelectClient() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("clientNo", "C1");

		 mockMvc.perform(get("/client/select").params(params))  					
		.andExpect(status().isOk())							
		.andExpect(forwardedUrl("client/select"))				
		.andDo(print());
		
	}
	
	/* 의견 리스트 조회 */
//	@Test	
//	@Disabled
	public void testSelectOpinionList() throws Exception {
		
	}
	
	/* 의견 등록 */
//	@Test	
//	@Disabled
	public void testInsertOpinion() throws Exception {
		
	}
	
	/* 의견 삭제 */
	@Test	
	@Disabled
	public void testDeleteOpinion() throws Exception {
				
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("opinionNo", "O15");

		mockMvc.perform(post("/client/opinion/delete").params(params))				
		.andExpect(status().isOk())	  						
		.andDo(print());		
	}
	
	/* 첨부파일 다운로드 */
	@Test	
	@Disabled
	public void testFileDownload() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("fileNo", "F99");
		
		mockMvc.perform(get("/client/fileDownload").params(params))				
		.andExpect(status().isOk())	  						
		.andDo(print());		
	}
	
	/* 고객 작성화면으로 이동 */
	@Test
	@Disabled
	public void testInsertClientPage() throws Exception {
		
		mockMvc.perform(get("/client/insert"))  					
		.andExpect(status().isOk())							
		.andExpect(forwardedUrl("client/insert"))				
		.andDo(print());		
	}
	
	/* 고객사 리스트 조회 */
	@Test
	@Disabled
	public void testSelectClientCompanyList() throws Exception {
		
		 mockMvc.perform(get("/client/clientCompany"))  					
		.andExpect(status().isOk())										
		.andDo(print());		
	}
	
	
	/* 고객 등록 */
//	@Test
//	@Disabled
	public void testInsertClient() throws Exception {
		
		
		
	}
	
	/* 고객사 수정페이지 조회 */
	@Test
	@Disabled
	public void testSeleteClientForUpdate() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("clientNo", "C66");

		 mockMvc.perform(get("/client/update").params(params))  					
		.andExpect(status().isOk())							
		.andExpect(forwardedUrl("client/update"))				
		.andDo(print());		
	}
	
	/* 첨부파일 삭제 */
	@Test	
	@Disabled
	public void testDeleteFile() throws Exception {
				
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("fileNo", "F15");

		mockMvc.perform(post("/client/file/delete").params(params))				
		.andExpect(status().isOk())	  						
		.andDo(print());
		
	}
	
	/* 고객 수정 */
//	@Test
//	@Disabled
	public void testUpdateClient() throws Exception {
		

	}
	
	/* 고객 삭제 */
	@Test	
	@Disabled
	public void testDeleteClient() throws Exception {
				
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("clientNo", "C66");

		mockMvc.perform(post("/client/delete").params(params))				
		.andExpect(status().isOk())	  						
		.andDo(print());
		
	}
	
}
