package com.codefactory.mnm.client.model.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.codefactory.mnm.client.model.dto.BusinessActivityDTO;
import com.codefactory.mnm.client.model.dto.ClientAttachmentDTO;
import com.codefactory.mnm.client.model.dto.ClientCollectionDTO;
import com.codefactory.mnm.client.model.dto.ClientCompanyDTO;
import com.codefactory.mnm.client.model.dto.ClientDTO;
import com.codefactory.mnm.client.model.dto.ClientSearchDTO;
import com.codefactory.mnm.client.model.dto.OpinionDTO;
import com.codefactory.mnm.client.model.dto.UsersDTO;
import com.codefactory.mnm.common.Pagenation;
import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.configuration.CodefactoryMnmProjectApplication;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes= {CodefactoryMnmProjectApplication.class})
public class ClientServiceImplTests {

	@Autowired
	private ClientService clientService;
	
	private ClientDTO client;
	private ClientAttachmentDTO clientAttachment;
	private ClientCollectionDTO clientCollection;
	private ClientSearchDTO clientSearch;
	
	@BeforeEach
	public void init() {
		
		client = new ClientDTO();
		client.setClientNo("C61");
		client.setClientName("테스트11");
		client.setClientCompanyNo("CC1");
		client.setClientCompanyName("고객사1");
		client.setDept("개발팀");
		client.setJob("과장");
		client.setPhone("010-1234-5678");
		client.setLandlineTel("02-1234-5678");
		client.setEmail("abc@gmail.com");
		client.setGrade("S");
		client.setKeyMan("Y");
		client.setUserNo("U1");
		client.setNote("안녕하세요");
		
		String filePath = "C:\\final\\codefactory-mnm-project\\src\\main\\webapp\\static/upload/";	
		
		clientAttachment = new ClientAttachmentDTO();
		clientAttachment.setFileName("FDSDFASDFASDFASDF213ASD.txt");
		clientAttachment.setFileOriginalName("테스트.txt");
		clientAttachment.setFilePath(filePath);
		
		List<ClientAttachmentDTO> files = new ArrayList<>();
		files.add(clientAttachment);
		
		clientCollection = new ClientCollectionDTO();
		clientCollection.setClient(client);
		clientCollection.setFiles(files);
		
	}
	
	/* 고객 리스트 조회 */
	@Test
	@Disabled
	public void testSelectClientList() {
		
		SelectCriteria selectCriteria = Pagenation.getSelectCriteria(1, 5, 10, 2);
		
		clientSearch = new ClientSearchDTO();
		clientSearch.setClientName(null);
		clientSearch.setClientCompanyName(null);
		clientSearch.setCurrentPage(null);
		clientSearch.setEmail(null);
		clientSearch.setUserNo(null);
		clientSearch.setPhone(null);
		clientSearch.setSelectCriteria(selectCriteria);
		
		Map<String, Object> clientList = clientService.selectClientList(clientSearch);
		 
		 assertNotNull(clientList);		
	}
	
		
	/* 담당자 리스트 조회 */
	@Test
	@Disabled
	public void testSelectUserList() {
		
		 List<UsersDTO> userList = clientService.selectUserList();
		 
		 assertNotNull(userList);		
	}
	
	/* 영업활동 리스트 조회 */
	@Test
	@Disabled
	public void testSelectBusinessAcitivityList() {
		
		List<BusinessActivityDTO>  businessAcitivityList = clientService.selectBusinessAcitivityList("C1");
		
		assertNotNull(businessAcitivityList);		
	}
	
	/*  고객 조회 */
	@Test
	@Disabled
	public void testSelectClient() {
		
		Map<String, Object> map = clientService.selectClient("C1");
		
		assertNotNull(map);		
	}
	
	/*  의견 리스트 조회 */
	@Test
	@Disabled
	public void testSelectOpinionList() {
		
		List<OpinionDTO> opinionList = clientService.selectOpinionList("C1");
		
		assertNotNull(opinionList);		
	}
	
	/* 의견 작성 */
	@Test
	@Disabled
	public void testInsertOpinion() {
		
		int result = clientService.insertOpinion("C1", "안녕하세요", "user01");
		
		assertEquals(1, result);
	}
	
	/* 의견 삭제 */
	@Test
	@Disabled
	public void testDeleteOpinion() {
		
		int result =  clientService.deleteOpinion("O63");
			
		assertEquals(1, result);
	}
	
	/* 첨부파일 조회 */
	@Test
	@Disabled
	public void testSelectFile() {
		
		ClientAttachmentDTO selectFile =  clientService.selectFile("F98");
			
		assertNotNull(selectFile);
	}
	
	
	/* 고객사 리스트 조회 */
	@Test
	@Disabled
	public void testSelectClientCompanyList() {
		
		List<ClientCompanyDTO> cientCompanyList = clientService.selectClientCompanyList();
		
		assertNotNull(cientCompanyList);	
	}
	
	/* 고객 등록 */
	@Test
	@Disabled
	public void testInsertClient() {
		
		int result = clientService.insertClient(clientCollection);
		
		assertEquals(1, result);
	}
	
	/* 첨부파일 삭제 */
	@Test
	@Disabled
	public void testDeleteFile() {
		
		Map<String, Object> file = clientService.deleteFile("F1");
		
		assertNotNull(file);		
	}
	
	/* 고객 수정 */
	@Test
	@Disabled
	public void testUpdateClient() {
		
		int result = clientService.updateClient(clientCollection);
			
		assertEquals(1, result);
	}
	
	/* 고객 삭제 */
	@Test
	@Disabled
	public void testDeleteClient() {
		
		int result = clientService.deleteClient("C66");
			
		assertEquals(1, result);
	}
	
}
