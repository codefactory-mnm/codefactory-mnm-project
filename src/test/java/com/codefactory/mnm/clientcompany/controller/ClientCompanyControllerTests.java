package com.codefactory.mnm.clientcompany.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.codefactory.mnm.configuration.CodefactoryMnmProjectApplication;
import com.codefactory.mnm.configuration.MybatisConfiguration;
import com.codefactory.mnm.member.model.dto.UserDTO;
import com.codefactory.mnm.member.model.dto.UserImpl;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {CodefactoryMnmProjectApplication.class,  
		MybatisConfiguration.class})
@SpringBootTest
public class ClientCompanyControllerTests {
	
	@Autowired
	private ClientCompanyController clientCompanyController;
	
	private MockMvc mockMvc;
	
	@BeforeEach					
	public void setUp() {
		mockMvc = MockMvcBuilders.standaloneSetup(clientCompanyController).build();			
	}
	
	/* 부서 리스트 조회 */
	@Test
	@Disabled
	public void testSelectDepartmentList() throws Exception {
		
		 mockMvc.perform(get("/clientCompany/dept"))  					
		.andExpect(status().isOk())										
		.andDo(print());
		
	}
	
	/* 담당자 리스트 조회*/
	@Test
	@Disabled
	public void testSelectUsersList() throws Exception {
		
		 mockMvc.perform(get("/clientCompany/users"))  					
		.andExpect(status().isOk())										
		.andDo(print());
		
	}
	
	/* 고객사 리스트 조회 */
	@Test
	@Disabled
	public void testSelectClientCompanyList() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("clientCompanyName", "고객사");
		params.add("deptName", null);
		params.add("division", null);
		params.add("name", null);
		params.add("currentPage", "1");
		
		 mockMvc.perform(get("/clientCompany/list").params(params))  					
		.andExpect(status().isOk())							
		.andExpect(forwardedUrl("clientCompany/list"))				
		.andDo(print());
		
	}
	
	/* 고객사 상세 조회 */
	@Test
	@Disabled
	public void testSelectClientCompany() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("clientCompanyNo", "CC1");

		 mockMvc.perform(get("/clientCompany/select").params(params))  					
		.andExpect(status().isOk())							
		.andExpect(forwardedUrl("clientCompany/select"))				
		.andDo(print());
		
	}
	
	/* 의견 리스트 조회 */
//	@Test	
//	@Disabled
	public void testSelectOpinionList() throws Exception {
				
//		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
//		params.add("clientCompanyNo", "CC1");
//
//		 mockMvc.perform(get("/clientCompany/opinion/select").params(params))  					
//		.andExpect(status().isOk())							
//		.andExpect(forwardedUrl("clientCompany/select"))				
//		.andDo(print());
		
	}
	
	//* 의견 등록 */
//	@Test	
//	@Disabled
	public void testInsertOpinion() throws Exception {
				
//		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
//		params.add("clientCompanyNo", "CC1");
//		params.add("opinionContent", "안녕하세요");
//
//		mockMvc.perform(post("/clientCompany/opinion/insert").params(params))				
//		.andExpect(status().is3xxRedirection())  						
//		.andExpect(flash().attributeCount(1))
//		.andExpect(redirectedUrl("/clientCompany/insert"))
//		.andDo(print());
		
	}
	
	/* 의견 삭제 */
	@Test	
	@Disabled
	public void testDeleteOpinion() throws Exception {
				
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("opinionNo", "O15");

		mockMvc.perform(post("/clientCompany/opinion/delete").params(params))				
		.andExpect(status().isOk())	  						
		.andDo(print());
		
	}
	
	/* 첨부파일 다운로드 */
	@Test	
	@Disabled
	public void testFileDownload() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("fileNo", "F94");
		
		mockMvc.perform(get("/clientCompany/fileDownload").params(params))				
		.andExpect(status().isOk())	  						
		.andDo(print());
		
	}
	
	/* 고객사 작성화면으로 이동 */
	@Test
	@Disabled
	public void testInsertClientCompanyPage() throws Exception {
		
		 mockMvc.perform(get("/clientCompany/insert"))  					
		.andExpect(status().isOk())							
		.andExpect(forwardedUrl("clientCompany/insert"))				
		.andDo(print());
		
	}
	
	/* 고객사 등록 */
	@Test
	@Disabled
	public void testInsertClientComapany() throws Exception {

//		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
//
//		params.add("clientCompanyName", "테스트명");
//		params.add("grade", "S");
//		params.add("progressStatus", "접촉금지");
//		params.add("sales", "100억");
//		params.add("employeeNo", "5천명");
//		params.add("businessNo", "123-45-67890");
//		params.add("landlineTel", "02-1234-5678");
//		params.add("fax", "02-123-4567");
//		params.add("website", "www.abc.com");
//		params.add("zipCode", "12345");
//		params.add("address", "서울시 송파구");
//		params.add("detailedAddress", "3층");
//		params.add("division", "가망고객");
//		
////		mockMvc.perform(post("/clientCompany/insert").params(params))				
////		.andExpect(status().is3xxRedirection())  						
////		.andExpect(flash().attributeCount(1))
////		.andExpect(redirectedUrl("/clientCompany/list"))
////		.andDo(print());
//		
//		String fileName = "FDSDFASDFASDFASDF213ASD.txt";
//		String fileOriginalName = "test.txt";
//		String content = "hello";
//		String filePath = "C:\\final\\codefactory-mnm-project\\src\\main\\webapp\\static/upload/";	
//		
//		MockMultipartFile file = new MockMultipartFile(fileName, fileOriginalName, null, content.getBytes());
//			
//		this.mockMvc.perform(multipart("/clientCompany/insert").file(file))
//		.andDo(print())
//		.andExpect(status().is3xxRedirection());
//
//						
	}
	
	/* 고객사 수정페이지 조회 */
	@Test
	@Disabled
	public void testSeleteClientCompanyForUpdate() throws Exception {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("clientCompanyNo", "CC1");

		 mockMvc.perform(get("/clientCompany/update").params(params))  					
		.andExpect(status().isOk())							
		.andExpect(forwardedUrl("clientCompany/update"))				
		.andDo(print());
		
	}
	
	/* 첨부파일 삭제 */
	@Test	
	@Disabled
	public void testDeleteFile() throws Exception {
				
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("fileNo", "F15");

		mockMvc.perform(post("/clientCompany/file/delete").params(params))				
		.andExpect(status().isOk())	  						
		.andDo(print());
		
	}
	
	/* 고객사 수정 */
//	@Test
//	@Disabled
	public void testUpdateClientComapany() throws Exception {
		

	}
	
	/* 고객사 삭제 */
	@Test	
	@Disabled
	public void testDeleteClientCompany() throws Exception {
				
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("clientCompanyNo", "CC105");

		mockMvc.perform(post("/clientCompany/delete").params(params))				
		.andExpect(status().isOk())	  						
		.andDo(print());
		
	}
}
