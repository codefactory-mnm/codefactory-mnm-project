package com.codefactory.mnm.clientcompany.model.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyAttachmentDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyChangeHistoryDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyCollectionDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanyDTO;
import com.codefactory.mnm.clientcompany.model.dto.ClientCompanySearchDTO;
import com.codefactory.mnm.clientcompany.model.dto.DepartmentDTO;
import com.codefactory.mnm.clientcompany.model.dto.OpinionDTO;
import com.codefactory.mnm.clientcompany.model.dto.UsersDTO;
import com.codefactory.mnm.common.Pagenation;
import com.codefactory.mnm.common.SelectCriteria;
import com.codefactory.mnm.configuration.CodefactoryMnmProjectApplication;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes= {CodefactoryMnmProjectApplication.class})
public class ClientCompanyServiceImplTests {
	
	@Autowired
	private ClientCompanyService clientCompanyService;
	
	private ClientCompanyDTO clientCompany;
	private ClientCompanyAttachmentDTO clientCompanyAttachment;
	private ClientCompanyChangeHistoryDTO clientCompanyChangeHistory;
	private ClientCompanyCollectionDTO clientCompanyCollection;
	private ClientCompanySearchDTO clientCompanySearch;
	
	@BeforeEach
	public void init() {
		
		clientCompany = new ClientCompanyDTO();
		clientCompany.setClientCompanyNo("CC2");
		clientCompany.setClientCompanyName("테스트명7");
		clientCompany.setGrade("S");
		clientCompany.setProgressStatus("접촉금지");
		clientCompany.setSales("100억");
		clientCompany.setEmployeeNo("5천명");
		clientCompany.setBusinessNo("123-45-67890");
		clientCompany.setLandlineTel("02-1234-5678");
		clientCompany.setFax("02-123-4567");
		clientCompany.setWebsite("www.abc.com");
		clientCompany.setZipCode("12345");
		clientCompany.setAddress("서울시 송파구");
		clientCompany.setDetailedAddress("3층");
		clientCompany.setNote("안녕하세요");
		clientCompany.setDivision("가망고객");
		
		String filePath = "C:\\final\\codefactory-mnm-project\\src\\main\\webapp\\static/upload/";	
		
		clientCompanyAttachment = new ClientCompanyAttachmentDTO();
		clientCompanyAttachment.setFileName("FDSDFASDFASDFASDF213ASD.txt");
		clientCompanyAttachment.setFileOriginalName("테스트.txt");
		clientCompanyAttachment.setFilePath(filePath);
		
		List<ClientCompanyAttachmentDTO> files = new ArrayList<>();
		files.add(clientCompanyAttachment);

		clientCompanyChangeHistory = new ClientCompanyChangeHistoryDTO();
		clientCompanyChangeHistory.setProgressStatus(clientCompany.getProgressStatus());
		
		clientCompanyCollection = new ClientCompanyCollectionDTO();
		clientCompanyCollection.setClientCompany(clientCompany);
		clientCompanyCollection.setFiles(files);
		
		
	}
	
	/* 부서 리스트 조회 */
	@Test
	@Disabled
	public void testSelectDepartmentList() {
		
		 List<DepartmentDTO> departmentList = clientCompanyService.selectDepartmentList();
		 
		 assertNotNull(departmentList);
		
	}
	
	/* 담당자 리스트 조회
	 */
	@Test
	@Disabled
	public void testSelectUsersList() {
		
		List<UsersDTO> usersList = clientCompanyService.selectUsersList();
		 
		assertNotNull(usersList);
		
	}
	
	/* 고객사 리스트 조회 */
	@Test
	@Disabled
	public void testSelectClientCompanyList() {
		
		SelectCriteria selectCriteria = Pagenation.getSelectCriteria(1, 5, 10, 2);
		
		clientCompanySearch = new ClientCompanySearchDTO();
		clientCompanySearch.setClientCompanyName("고객사");
		clientCompanySearch.setDeptName("영업1팀");
		clientCompanySearch.setDivision("고객사");
		clientCompanySearch.setUserNo("U1");
		clientCompanySearch.setCurrentPage("1");
		clientCompanySearch.setSelectCriteria(selectCriteria);
				
		Map<String, Object> selectClientCompanyList = clientCompanyService.selectClientCompanyList(clientCompanySearch);
				
		
		assertNotNull(selectClientCompanyList);
	}
	
	/* 고객사 상세조회 */
	@Test
	@Disabled
	public void testSelectClientCompany() {
		
		Map<String, Object> selectClientCompany = clientCompanyService.selectClientCompany("CC1");	
			
		assertNotNull(selectClientCompany);
	}
	
	/* 의견 리스트 조회 */
	@Test
	@Disabled
	public void testSelectOpinionList() {
		
		 List<OpinionDTO> selectOpinionList = clientCompanyService.selectOpinionList("CC1");	
			
		assertNotNull(selectOpinionList);
	}
	
	/* 의견 등록 */
	@Test
	@Disabled
	public void testInsertOpinion() {
		
		int result =  clientCompanyService.insertOpinion("CC1", "안녕하세요", "user01");
			
		assertEquals(1, result);
	}
	
	/* 의견 삭제 */
	@Test
	@Disabled
	public void testDeleteOpinion() {
		
		int result =  clientCompanyService.deleteOpinion("O10");
			
		assertEquals(1, result);
	}
	
	/* 첨부파일 조회 */
	@Test
	@Disabled
	public void testSelectFile() {
		
		ClientCompanyAttachmentDTO selectFile =  clientCompanyService.selectFile("F10");
			
		assertNotNull(selectFile);
	}

	/* 고객사 등록 */
	@Test
	@Disabled
	public void testInsertClientCompany() {
		
		int result = clientCompanyService.insertClientComapany(clientCompanyCollection);
		
		assertEquals(1, result);
	}
	
	/* 고객사 수정페이지 조회 */
	@Test
	@Disabled
	public void testSeleteClientCompanyForUpdate() {
		
		Map<String, Object> clientCompany = clientCompanyService.seleteClientCompanyForUpdate("CC1");
			
		assertNotNull(clientCompany);
	}
	
	/* 첨부파일 삭제 */
	@Test
	@Disabled
	public void testDeleteFile() {
		
		Map<String, Object> file = clientCompanyService.deleteFile("F1");
			
		assertNotNull(file);
	}
	
	/* 고객사 수정 */
	@Test
	@Disabled
	public void testUpdateClientComapany() {
		
		int result = clientCompanyService.updateClientComapany(clientCompanyCollection);
			
		assertEquals(1, result);
	}
	
	/* 고객사 삭제 */
	@Test
	@Disabled
	public void testDeleteClientCompany() {
		
		int result = clientCompanyService.deleteClientCompany("CC102");
			
		assertEquals(1, result);
	}
}
