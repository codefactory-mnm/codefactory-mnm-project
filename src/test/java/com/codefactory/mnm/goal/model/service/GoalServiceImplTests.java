package com.codefactory.mnm.goal.model.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.codefactory.mnm.configuration.CodefactoryMnmProjectApplication;

@SpringBootTest
//@RunWith(SpringRunner.class)
@ContextConfiguration(classes= {CodefactoryMnmProjectApplication.class})
public class GoalServiceImplTests {
	
	@Autowired
	private GoalService goalService;
	
	private List<Map<String, String>> goalUpdateList;
	
	@Test
	public void testUpdateCrewGoal() {
		
		//List<Map<String, String>> goalUpdateList = new ArrayList<>();
		goalUpdateList = new ArrayList<>();
		
		for(int i = 13; i < 25; i++) {
			Map<String, String> goalUpdateMap = new HashMap<>();
			goalUpdateMap.put("goalNo", "GO" + i);
			goalUpdateMap.put("goalScale", "" + i);
			
			goalUpdateList.add(goalUpdateMap);
		}
		
		int result = goalService.updateCrewGoal(goalUpdateList);
		
		assertEquals(1, result);

		
	}

}
